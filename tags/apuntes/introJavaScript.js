// en js las funciones se declaran asi:
// function + el nombre que quieras darle a la funcion, con dos parentesis detras
// los parentesis, aunque esten vacios, son obligatorios
// se abren y cierran corchetes, entre los corchetes escribimos el codigo
function myFunction() {
    // alert() hace que aparezca una ventana con un mensaje en pantalla
    alert("Esto genera una ventana.")
    // document.write() escribe directamente en la pantalla, no hace aparecer ninguna ventana
    document.write("Esto se escribe en el body.")
}
// declarar una variable significa 'crearla'
// una variable se declara asi:
// var nombreDeLaVariable = valorDeLaVariable
// si en el valor de la variable ponemos un numero, la variable tomara el valor de ese numero
// si ponemos una frase, el de la frase: la frase debe ir entre comillas!!!! Asi:
var nombre = "Effy";
var numero = 7;
// al contrario que en java, no es necesario declarar que tipo de variable es, generalmente la interpreta como tipo String
// si queremos introducir un numero y que se interprete como un valor numerico y no como un String, tenemos que parsearlo
// probemos con 
numero = parseInt(numero);
// si se tratara de un decimal usariamos la misma estructura pero con parseFloat
numero = parseFloat(8.7);