(: Títulos en español de las películas de la Paramount :)

for $h in hitchcock/peliculas/pelicula/titulo
where $h/@lang = "es"
and $h/../estudios/@nombre = "paramount"
return $h/text()