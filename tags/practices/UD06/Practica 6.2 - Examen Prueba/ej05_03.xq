(: Número de nominaciones al óscar de las películas de James Stewart :)

for $h in hitchcock/peliculas/pelicula
let $a := $h/actores/actor
let $n := $h/oscar/@nominaciones
where $a = "James Stewart"
return $n