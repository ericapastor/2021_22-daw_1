(: Nombre completo del estudio de las películas con cuatro nominaciones :)

for $u in /hitchcock/estudios/estudio
for $d in /hitchcock/peliculas/pelicula
let $n:=$u/@id
where $d/oscar/@nominaciones="4"
and $d/estudios/@nombre=$u/@id
group by $n
return $u/nombre/text()