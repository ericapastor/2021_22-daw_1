(: Títulos en inglés de todas las películas :)

for $h in hitchcock/peliculas/pelicula/titulo
where $h/@lang = "en"
return $h/text()