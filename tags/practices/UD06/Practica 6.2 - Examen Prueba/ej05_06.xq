(: Año de nacimiento de la actriz de la película de 1951 :)

for $h in /hitchcock/actores/actor
for $d in /hitchcock/peliculas/pelicula
where $d/fecha="1951" and $d/actores/actriz=$h/@nombre
return $h/@nacimiento