(: 8. Mostrar la suma de los precios de los bailes de la sala 1. :)

let $b := /bailes/baile[sala=1]
return <total_sala_1>{sum($b/precio)}</total_sala_1>