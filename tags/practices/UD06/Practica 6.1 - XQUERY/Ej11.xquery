(: 11. Mostrar todos los datos de cada baile excepto la fecha de comienzo y de fin. :)

for $b in /bailes/baile
return <baile>
{$b/* except $b/comienzo except $b/fin}</baile>