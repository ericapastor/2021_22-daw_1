(: 1. Mostrar cada uno de los nombres de los bailes con la etiqueta "losbailes". :)

for $b in /bailes/baile
return
 <losbailes>
  {$b/nombre/text()}
 </losbailes>