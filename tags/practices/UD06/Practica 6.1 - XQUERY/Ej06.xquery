(: 6. Mostrar los nombres de los profesores y la sala en la que dan clase, ordénalos por sala. :)

for $b in bailes/baile
order by $b/sala
return
 <baile>
  {$b/profesor} 
  {$b/sala}
 </baile>