(: 9. Mostrar cuántas plazas en total oferta el profesor "Jesus Lozano". :)

let $b := bailes/baile[profesor='Jesus Lozano']
return
 <plazas>
  {sum($b/plazas)}
 </plazas>