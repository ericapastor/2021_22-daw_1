(: 3. Mostrar los nombres de los bailes cuyo precio sea mayor de 30. :)

for $b in /bailes/baile
where $b/precio > 30
return $b/(nombre, precio)/text()