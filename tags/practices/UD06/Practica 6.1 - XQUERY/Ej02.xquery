(: 2. Mostrar los nombres de los bailes seguidos con el número de plazas entre paréntesis, ambos dentro de la misma etiqueta "losbailes". :)

for $b in /bailes/baile
return
<losbailes>
 {$b/nombre/text()} ({$b/plazas/text()})
</losbailes>