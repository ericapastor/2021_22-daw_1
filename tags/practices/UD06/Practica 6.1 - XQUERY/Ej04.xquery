(: 4. Mostrar los nombres de los bailes cuyo precio sea mayor de 30 y la moneda "euro". :)

for $b in /bailes/baile
where $b/precio > 30 and $b/precio/@moneda = 'euro'
return $b/nombre/text()