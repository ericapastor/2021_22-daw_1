(: 5. Mostrar los nombres y la fecha de comienzo de los bailes que comiencen el mes de enero (utiliza para buscarlo la cadena de texto "/1/"). :)

for $b in bailes/baile
where contains ($b/comienzo, "/1/")
return 
 <baile>
  {$b/nombre}
  {$b/comienzo}
 </baile>
