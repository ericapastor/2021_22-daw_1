(: 7. Mostrar la media de los precios de todos los bailes. :)

let $b := avg(bailes/baile/precio)
return <media>$b</media>