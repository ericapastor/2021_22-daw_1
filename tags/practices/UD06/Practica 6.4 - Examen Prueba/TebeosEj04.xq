(:4. Nombre de los autores nacidos en Francia: (0.75p):)
for $t in /tebeos/autores/dibujantes/dibujante
for $g in /tebeos/autores/guionistas/guionista
where $t/nacimientoPais='Francia' and $g/nacimientoPais='Francia'
return ($t/nombre,$g/nombre)