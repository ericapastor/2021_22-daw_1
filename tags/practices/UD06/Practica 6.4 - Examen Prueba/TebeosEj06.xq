(:6. Nombre de los personajes que aparecen en "Astérix, el galo": (0.75p):)
for $t in tebeos/albumes/album
for $p in tebeos/personajes/personaje
where $t/titulo = 'AstÃ©rix, el galo'
and $p/@id = $t/personajes/personaje
return $p/text()