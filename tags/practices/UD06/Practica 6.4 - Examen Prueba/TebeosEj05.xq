(:5. Títulos de los álbumes publicados antes de 1970: (0.75p):)
for $t in tebeos/albumes/album
where $t/fechaPublicacion < '1970'
return $t/titulo/text()