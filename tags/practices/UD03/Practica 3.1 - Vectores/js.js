/* EJERCICIO 1 */
var numeros = new Array(5);
function rellenar() {
    for (var i = 0; i < numeros.length; i++) {
        numeros[i] = prompt("Dame un numero.");
    }
}
function mostrar() {
    for (var i = 0; i < numeros.length; i++){
        alert ("La posicion es "+i+" y el numero es "+numeros[i]);
    }
}

/* EJERCICIO 2 */
var numCien = new Array(100);
var acumulador=0;
for(var i=0;i<numCien.length;i++){
    numCien[i]=i+1;
}
function write100(){
    for (var i=0;i<numCien.length;i++){
        acumulador=acumulador+numCien[i];
    }
    var media=acumulador/100;
    document.write("La media es "+media)
}

/* EJERCICIO 3 */
var cienDesc = new Array(100);
for(var i=cienDesc.length-1;i>0;i--){
    cienDesc[i]=i;
}
function verCienDesc(){
    for (var i=0;i<cienDesc.length;i++){
        document.write(cienDesc[i]);
        if(cienDesc[i]%2==0){
            document.write(" es par.");
        }
        document.write("<br>");
    }
}

/* EJERCICIO 4 */
function numerosImpares(){
    for(var i=0;i<numCien.length;i++){
        if(numCien[i]%2==1){
            document.write(numCien[i]+"<br>");
        }
    }
}

/* EJERCICIO 5 */
function aleatorio(){
    var numRandom=Math.round(Math.random()*10);
    var adivina=prompt("Adivina un numero entre 1 y 10. Tienes 3 oportunidades.");
    for (var i=0;i<3;i++){
        if (adivina>numRandom){
            adivina=prompt("Demasiado grande.");
        } else if(adivina<numRandom){
            adivina=prompt("Demasiado pequeño.");
        } else if(adivina==numRandom){
            alert("Enhorabuena! Has adivinado el numero");
            break;
        }
    }
    if(adivina!=numRandom){
        document.write("Has perdido.");
    }
}