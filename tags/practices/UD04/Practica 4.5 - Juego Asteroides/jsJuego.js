var canvas, ctx;
var naveX = 0;
var naveY = 0;
var nave = new Image();
var fondoNave = new Image();
var contador = 100; // contador de movimientos
var tiempo = new Date(15000);
var stop;

function canvasTablero() {
    canvas = document.getElementById('midibujo');
    ctx = canvas.getContext('2d');
    // Llamar a pintar fondo con estrellas
    pintarFondo();
    // Llamar a pintar la nave azul
    pintarNave();
    // Llamar a pintar la base verde
    pintarBase();
    // Llamar a pintar los asteroides rojos
    pintarAsteroide();
    // escuchador de teclado
    window.addEventListener('keydown', moverNave, true);
    temporizador();
}

function pintarFondo() {
    // Pinto el fondo negro
    ctx.fillStyle = "black";
    ctx.beginPath();
    ctx.rect(0, 0, 450, 450); // posición x, posición y, ancho, alto (en píxeles)
    ctx.closePath();
    ctx.fill();
    // Pinto 100 estrellas
    for (var i = 0; i < 200; i++) {
        var x = Math.random() * 450;
        var y = Math.random() * 450;
        ctx.fillStyle = "white";
        ctx.beginPath();
        ctx.arc(x, y, 3, 0, Math.PI * 2);
        ctx.closePath();
        ctx.fill();
    }
    // guardo el fondo en la variable declarada arriba
    fondoNave = ctx.getImageData(0, 0, 30, 30);
}

function pintarNave() {
    ctx.fillStyle = "#3fafff";
    ctx.beginPath();
    ctx.rect(0, 0, 15, 15);
    ctx.closePath();
    ctx.fill();
    // guardo la nave en la variable declarada arriba 
    nave = ctx.getImageData(0, 0, 15, 15);
}

function pintarBase() {
    ctx.fillStyle = "rgb(0,200,100)";
    ctx.beginPath();
    ctx.rect(430, 420, 20, 30);
    ctx.closePath();
    ctx.fill();
}

function pintarAsteroide() {
    for (var i = 0; i < 30; i++) {
        var x = Math.random() * 420;
        var y = Math.random() * 420;
        if (x < 30 && y < 30) {
            x = x + 30;
            y = y + 30;
        }
        if (x > 420 && y > 420) {
            x = x - 30;
            y = y - 30;
        }
        // Pinto asteroide
        ctx.fillStyle = "#F15999";
        ctx.beginPath();
        ctx.rect(x, y, 20, 20);
        ctx.closePath();
        ctx.fill();
    }
}

function moverNave(evento) {
    // Detecto la tecla que pulsamos
    switch (evento.keyCode) {
        // la flecha izquierda es el 37
        // la letra a es el 65
        case 37:
        case 65:
        case 97:
            actualizarContador();
            if (naveX == 0) {
                detectarColision();
                break;
            }
            // borrar la nave (pintando fondo encima)
            ctx.putImageData(fondoNave, naveX, naveY);
            // actualizo la x
            naveX = naveX - 15;
            // capturar el fondo
            fondoNave = ctx.getImageData(naveX, naveY, 15, 15);
            // muevo la nave
            ctx.putImageData(nave, naveX, naveY);
            detectarColision();
            break;
            // la flecha de arriba es la 38
            // la letra w es el 87
        case 38:
        case 87:
        case 119:
            actualizarContador();
            if (naveY == 0) {
                detectarColision();
                break;
            }
            // borrar la nave (pintando fondo encima)
            ctx.putImageData(fondoNave, naveX, naveY);
            // actualizo la x
            naveY = naveY - 15;
            // capturar el fondo
            fondoNave = ctx.getImageData(naveX, naveY, 15, 15);
            // muevo la nave
            ctx.putImageData(nave, naveX, naveY);
            detectarColision();
            break;
            // la flecha derecha es el 39
            // la letra d es el 68
        case 39:
        case 68:
        case 100:
            actualizarContador();
            if (naveX == 435) {
                detectarColision();
                break;
            }
            // borrar la nave (pintando fondo encima)
            ctx.putImageData(fondoNave, naveX, naveY);
            // actualizo la x
            naveX = naveX + 15;
            // capturar el fondo
            fondoNave = ctx.getImageData(naveX, naveY, 15, 15);
            // muevo la nave
            ctx.putImageData(nave, naveX, naveY);
            detectarColision();
            break;
            // la flecha de arriba es la 40
            // la letra s es el 83
        case 40:
        case 83:
        case 115:
            actualizarContador();
            if (naveY == 435) {
                detectarColision();
                break;
            }
            // borrar la nave (pintando fondo encima)
            ctx.putImageData(fondoNave, naveX, naveY);
            // actualizo la x
            naveY = naveY + 15;
            // capturar el fondo
            fondoNave = ctx.getImageData(naveX, naveY, 15, 15);
            // muevo la nave
            ctx.putImageData(nave, naveX, naveY);
            detectarColision();
            break;
    }
}

function detectarColision() {
    var pixels = 900; // si el cuadrado es de 30x30
    var elementos = 900 * 4; // cada pixel tiene 4 bytes en RGB
    // busco el rosa(asteroide) o el verde(base)
    for (var i = 0; i < elementos; i += 4) {
        // asteroide (241, 89, 153)
        if (fondoNave.data[i] == 241 && fondoNave.data[i + 1] == 89 && fondoNave.data[i - 2] == 153) {
            var mensaje = "Has chocado con un asteroide. Pulsa AQUI para volver a jugar.";
            finalizar(mensaje);
            break;
        } // asteroide (241, 89, 153)
        if (fondoNave.data[i] == 0 && fondoNave.data[i + 1] == 200 && fondoNave.data[i - 2] == 100) {
            var mensaje = "Has llegado a base, BIEEEN!!! Pulsa AQUI para volver a jugar.";
            finalizar(mensaje);
            break;
        }

    }
}

function finalizar(mensaje) {
    var spanMensaje = document.getElementById("mensaje");
    spanMensaje.innerHTML = mensaje;
    window.removeEventListener("keydown", moverNave, true);
}

//reinicio el juego
function reiniciar() {
    window.location.reload();
}

function actualizarContador() {
    contador--;
    var spanPuntuacion = document.getElementById('puntuacion');
    spanPuntuacion.innerHTML = contador;
    if (contador == 0) {
        var mensaje = "Te has quedado sin movimientos. Pincha AQUI para volver a intentarlo.";
        finalizar(mensaje);
    } else if (contador <= 10) {
        puntuacion.style.color = 'red';

    } else if (contador <= 50) {
        puntuacion.style.color = 'yellow';
    }
}

function temporizador() {
    // quitamos de 500 en 500 milisegundos
    var ms = tiempo.getMilliseconds() - 500;
    tiempo.setMilliseconds(ms);
    var te
}