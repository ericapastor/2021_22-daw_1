
/*
*******************
*** Ejercicio 1 ***
*******************
*/

var n1 = 0
var n2 = 0
function pedirNumeros() {
    var condition = false;
    do {
        n1 = prompt("Dame el numero 1.")
        n1 = parseInt(n1)
        if (n1 > 0) {
            do {
                n2 = prompt("Dame el numero 2.")
                n2 = parseInt(n2)
                if (n2 > 0) {
                    if (n1 < n2) {
                        condition = true
                    } else {
                        alert("El segundo numero tiene que ser mayor que el primero.")
                    }
                } else {
                    alert("El numero no puede ser 0 o negativo.")
                }
            } while (n2 < 0 || n1 > n2)
        } else {
            alert("El numero no puede ser 0 o negativo.")
        }
    } while (condition == false)
}

function numerosPrimos() {
    var esPrimo
    var primos = ""
    for (var i = n1; i <= n2; i++) {
        esPrimo = true
        for (var j = 2; j < i; j++) {
            i = parseInt(i)
            j = parseInt(j)
            if (i % j == 0) {
                esPrimo = false
            }
        }
        if (esPrimo == true) {
            primos = primos + i + " "
        }
    }
    alert("Son primos:\n " + primos)
}
function numerosPares() {
    var pares = ""
    for (let i = n1; i <= n2; i++) {
        if (i % 2 == 0) {
            pares = pares + i + " "
        }
    }
    alert("Son pares:\n" + pares)
}


/*
*******************
*** Ejercicio 2 ***
*******************
*/

var alumnos = new Array(3)
for (let i = 0; i < alumnos.length; i++) {
    alumnos[i] = new Array(3)
}
var suma1 = 0
var suma2 = 0

function rellenarDatos() {
    suma1 = 0
    suma2 = 0
    for (let i = 0; i < alumnos.length; i++) {
        for (let j = 0; j < alumnos[i].length; j++) {
            if (j == 0) {
                alumnos[i][j] = prompt("Dime el nombre del alumno " + (i + 1))
            } else if (j == 1) {
                alumnos[i][j] = prompt("Dime la nota de Equipos de " + alumnos[i][j - 1])
            } else if (j == 2) {
                alumnos[i][j] = prompt("Dime la nota de Entornos de " + alumnos[i][j - 2])
            }
        }
    }
    for (let i = 0; i < alumnos.length; i++) {
        for (let j = 0; j < alumnos.length; j++) {
            if (j == 1 || j == 2) {
                alumnos[i][j] = parseFloat(alumnos[i][j])
                if (j == 1) {
                    suma1 = suma1 + alumnos[i][j]
                } else if (j == 2) {
                    suma2 = suma2 + alumnos[i][j]
                }
            }
        }
    }
}

function mediaEquipos() {
    var media1 = 0
    media1 = suma1 / 3
    alert("La media de notas en la asignatura Equipos es " + media1)
}

function mediaEntornos() {
    var media2 = 0
    media2 = suma2 / 3
    alert("La media de notas en la asignatura Equipos es " + media2)
}


/*
*******************
*** Ejercicio 3 ***
*******************
*/

const num1 = 0
const num2 = 0

function validar() {

    var losDaos = false;

    function validar2() {
        if (document.f1.input1.value == 0 && document.f1.input2.value == 0) {
            alert("Los campos estan vacios")
        } else {
            validar1()
        }
    }

    function validar1() {
        if (document.f1.input1.value == 0) {
            alert("Rellena el campo 1.")
        } else if (document.f1.input2.value == 0) {
            alert("Rellena el campo 2.")
        } else {
            a = true;
        }

        validar2();

        if (a == true) {
            var n1 = parseInt(document.f1.input1.value)
            var n2 = parseInt(document.f1.input2.value)
            var fact = 1
            for (var i = 1; i <= n1; i++) {
                fact *= i
            }
            document.write("El factorial de " + n1 + " es " + fact + ".")
            document.write("<br>La tabla de multiplicar de " + n2 + " es:<br>")
            for (var j = 0; j <= 10; j++) {
                document.write(n2 + " * " + j + " = " + (n2 * j) + "<br>")
            }
        }
    }