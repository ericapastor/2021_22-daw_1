(:Nombre del país cuya capital es Suecia:)
for $g in /geografia/paises/pais/nombre
where $g='Suecia'
return 
<pais> {$g/../capital/text()} ({$g/text()})</pais>