(: Visualiza los titulos :)
(: for $b in /bookstore/book/title
return $b/text() :)
(: Lo pongo entre comentarios para
que las demas funcionen :)

(:Titulo y precio:)
(: for $b in /bookstore/book
return ($b/title, $b/price)/text() :)
(: for $b in /bookstore/book
return $b(/title, /price)/text() :)
(: Las dos funcionan, tanto poniendo la
variable fuera como dentro :)

(: Mostrar los libros cuyo precio sea
<=30 :)
(: for $b in /bookstore/book
where $b/price <= 30
return $b :)
(: En este no se puede poner /text()
porque esta llamando un "objeto" entero :)

(: Titulo y autor del anyo 2005 :)
(: for $b in /bookstore/book
where $b/year = 2005
return $b/(title, author)/text() :)

(: Mostrar los libros ordenados por titulo :)
(: for $b in /bookstore/book
order by $b/title
return $b/(title, author)/text() :)

(: Mostrar cuantos libros hay :)
(: for $b in /bookstore/book
let $c:=count($b)
return $c :)
(: Si no le quito el for, cuenta
cada libro, mostrandolos por separado :)
(: let $c := count(/bookstore/book)
return $c :)

(: Mostrar el precio minimo
y maximo de los libros :)
(: let $min := min(/bookstore/book/price)
let $max := max(/bookstore/book/price)
return ($min, $max) :)
(: let $min := min(/bookstore/book/price)
let $max := max(/bookstore/book/price)
return
 <resultado>
  <maximo>{$max}</maximo>
  <minimo>{$min}</minimo>
 </resultado> :)

(: La suma total de los precios :)
(: let $suma := sum(/bookstore/book/price)
return $suma :)