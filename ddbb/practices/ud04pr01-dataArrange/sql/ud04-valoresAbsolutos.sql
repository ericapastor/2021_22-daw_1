/* Restablecer valores absolutos */
use witchcraft;
-- tabla PERSONAJE

update personaje
set nivel_medio_objeto = nivelMedioObj(id),
ataque_total = ataqueTotal(id),
defensa_total = defensaTotal(id),
velocidad_mov_total = velMovTotal(id),
estabilidad_total = estabilidadTotal(id),
parasitar_total = parasitarTotal(id),
celeridad_total = celeridadTotal(id),
num_NPC_asesinados = NPCsAsesinadosPorPj(id);


-- tabla personaje se relaciona con NPC
-- y tabla personaje roba a NPC

update personaje_se_relaciona_con_npc
set opinion_sumada = calcularOpinionPositiva(id_personaje, id_npc);
update personaje_roba_npc
set opinion_restada = calcularOpinionNegativa(id_personaje, id_npc);
update npc_opina_de_personaje
set puntuacion = calcularOpinionTotalNPC(id_personaje, id_npc);
update npc_opina_de_personaje
set opinion_personaje = estadoRelacionNpcPj(id_personaje, id_npc);