use witchcraft;

/*
	1. Funciones para mantener actualizados datos de la tabla personaje que deben ser
    calculados automaticamente. Este apartado cuenta con 7 funciones: una para calcular
    el nivel medio de objeto de las vestimentas que el personaje lleva equipadas, y seis
    mas para calcular su ataque, defensa, velocidad de movimiento, estabilidad, celeridad
    y parasitar totales. Estas funciones seran utilizadas en tres triggers (after insert,
    after update, after delete).
*/

	/*
		a) Calcular nivel medio de objeto
			- Comprueba que el id de personaje existe, si no existe,
				devuelve -1
            - Si existe: suma los niveles de objeto de las vestimentas que 
				el personaje lleva equipadas y los divide entre el numero
                de las mismas (sum/count)
	*/

drop function if exists nivelMedioObj;
delimiter ~
create function nivelMedioObj(idPersonaje int)
returns int
begin
	if (select count(*) from personaje where id = idPersonaje) < 1
		then return -1;
    else
		return
			(select sum(vestimenta.nivel_objeto)
			from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where personaje_se_equipa_vestimenta.id_personaje = idPersonaje)
            /
            (select count(*)
			from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where personaje_se_equipa_vestimenta.id_personaje = idPersonaje);
    end if;
end;
~
delimiter ;

	/*
		b) calcular ataque total
			- Devuelve la suma del ataque de las vestimentas que el personaje
				lleva equipadas y lo añade al ataque de la raza del PJ
			- Si el ID del personaje no exite, devuelve -1
	*/

drop function if exists ataqueTotal;
delimiter ~
create function ataqueTotal(idPersonaje int)
returns int
begin
    if (select count(*) from personaje where id = idPersonaje) < 1
		then return -1;
    else
		return (select raza.ataque_base from raza
			join personaje on personaje.id_raza = raza.id
			where personaje.id = idPersonaje)
		+ (select sum(vestimenta.ataque)
            from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where personaje_se_equipa_vestimenta.id_personaje = idPersonaje);
    end if;
end;
~
delimiter ;

	/*
		c) calcular defensa total
			- Devuelve la suma de la defensa base de la raza del personaje
				mas la suma de la defensa de cada una de las vestimentas que
                dicho PJ lleva equipadas
			- Si el ID del personaje introducido no existe, devuelve -1
	 */
 
drop function if exists defensaTotal;
delimiter ~
create function defensaTotal(idPersonaje int)
returns int
begin
    if (select count(*) from personaje where id = idPersonaje) < 1
		then return -1;
    else
		return (select raza.defensa_base from raza
			join personaje on personaje.id_raza = raza.id
			where personaje.id = idPersonaje)
		+ (select sum(vestimenta.defensa)
            from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where personaje_se_equipa_vestimenta.id_personaje = idPersonaje);
    end if;
end;
~
delimiter ;

	/*
		c) Calcular velocidad de movimiento total de un personaje:
			- Suma la velocidad de movimiento de la raza del personaje a
				la velocidad de movimiento de la clase del personaje y a
                la suma de cada velocidad que da cada una de las vestimentas
                que el personaje lleva equipadas.
			- Si el ID de personaje no existe, devuelve -1.
	*/

drop function if exists velMovTotal;
delimiter ~
create function velMovTotal(idPersonaje int)
returns int
begin
    if (select count(*) from personaje where id = idPersonaje) < 1
		then return -1;
    else
		return (select raza.velocidad_mov_base from raza
			join personaje on personaje.id_raza = raza.id
			where personaje.id = idPersonaje)
		+ (select clase.velocidad_movimiento_clase from clase
			join personaje on personaje.id_clase = clase.id
			where personaje.id = idPersonaje)
		+ (select sum(vestimenta.velocidad_movimiento)
            from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where personaje_se_equipa_vestimenta.id_personaje = idPersonaje);
    end if;
end;
~
delimiter ;

-- calcular estabilidad total

drop function if exists estabilidadTotal;
delimiter ~
create function estabilidadTotal(idPersonaje int)
returns int
begin
    if (select count(*) from personaje where id = idPersonaje) < 1
		then return -1;
    else
		return (select raza.estabilidad_base from raza
			join personaje on personaje.id_raza = raza.id
			where personaje.id = idPersonaje)
		+ (select sum(vestimenta.estabilidad)
            from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where personaje_se_equipa_vestimenta.id_personaje = idPersonaje);
    end if;
end;
~
delimiter ;

-- Calcular parasitar total

drop function if exists parasitarTotal;
delimiter ~
create function parasitarTotal(idPersonaje int)
returns int
begin
    if (select count(*) from personaje where id = idPersonaje) < 1
		then return -1;
	elseif (select sum(vestimenta.parasitar)
		from personaje_se_equipa_vestimenta
		join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
		where personaje_se_equipa_vestimenta.id_personaje = idPersonaje) is null
			then return 0;
    else
		return (select sum(vestimenta.parasitar)
			from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where personaje_se_equipa_vestimenta.id_personaje = idPersonaje);
    end if;
end;
~
delimiter ;

-- Calcular celeridad total

drop function if exists celeridadTotal;
delimiter ~
create function celeridadTotal(idPersonaje int)
returns int
begin
    if (select count(*) from personaje where id = idPersonaje) < 1
		then return -1;
	elseif (select sum(vestimenta.celeridad)
            from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where personaje_se_equipa_vestimenta.id_personaje = idPersonaje) is null
		then return 0;
    else
		return (select sum(vestimenta.celeridad)
            from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where personaje_se_equipa_vestimenta.id_personaje = idPersonaje);
    end if;
end;
~
delimiter ;
/*
	2. Funcion que devuelve el numero de NPCs asesinados por un personaje
*/

drop function if exists NPCsAsesinadosPorPj;
delimiter ~
create function NPCsAsesinadosPorPj(idPersonaje int)
returns int
begin
    if (select count(*) from personaje where id = idPersonaje) < 1
		then return -1;
    else
		return (select count(*) from personaje_roba_npc
			join personaje on personaje.id = personaje_roba_npc.id_personaje
			where personaje.id = idPersonaje
            and personaje_roba_npc.metodo = 'asesinato');
    end if;
end;
~
delimiter ;

/*
	3. Funcion que devuelve 0 si una fecha esta en formato incorrecto,
	o 1 si esta en formato correcto.
*/

drop function if exists comprobarFechaCorrecta;
delimiter ~
create function comprobarFechaCorrecta(fechaIns varchar(10))
returns int
begin
	declare fecha date;
    set fecha = fechaIns;
	if fecha = '0000-00-00' or fecha = 0 or fecha is null
		then return 0;
	else
		return 1;
    end if;
end;
~
delimiter ;

/*
	4. Funcion que recibe el ID de un perosnaje y el ID de una vestimenta,
		devuelviendo 1 si una vestimenta equipada es correcta para la clase
		del personaje que se la equipa, y si no lo es devuelve 0.
        * Devuelve -1 si el ID de personaje no existe y -2 si no existe el
        ID de vestimenta.
*/

drop function if exists materialCorrectoVestimentaClase;
delimiter ~
create function materialCorrectoVestimentaClase(idPJ int, idVest int)
returns int
begin
	if (select count(*) from personaje where id = idPJ) < 1 then
		return -1;
	elseif (select count(*) from vestimenta where id = idVest) < 1 then
		return -2;
	else
		if
			(select material
				from vestimenta
                where id = idVest)
		  = (select clase.material_armadura
				from clase
				join personaje on personaje.id_clase = clase.id
                where personaje.id = idPJ)
		then return 1;
		else return 0;
		end if;
	end if;
end;
~
delimiter ;

/*
	5. Funciones que calculan las variaciones en las opiniones que los NPCs
    tienen sobre los personajes:
		* opinionPositivaNPC --> Devuelve una puntuacion segun el
			tipo de relacion del PJ y el NPC. Si no existe relacion
            entre ese NPC y ese personaje, devuelve 0.Si el ID de
            personaje no existe, devuelve -1. Si no existe el ID del
            NPC, devuelve -2.
        * calcularOpinionPositiva --> Utiliza opinionPositivaNPC y
			la multiplica por la columna num_veces de la tabla
            personaje_se_relaciona_con_npc, devolviendo ese valor.
            Si opinionPositivaNPC le devolviese 0 o menor, devolvera
            lo mismo.
        * opinionNegativaNPC --> Devuelve una puntuacion segun el
			metodo que el personaje haya usado para robar al NPC.
            Si no existe relacion de robo entre ese personaje y
            ese NPC, devuelve 0. Si el ID de personaje no existe,
            devuelve -1. Si no existe el de NPC, devuelve -2.
        * calcularOpinionNegativa --> Utiliza opinionNegativaNPC,
			si esta devuelve un numero menor a 1, devuelve el mismo
            valor (0, -1, -2). Si no, devuelve la multiplicacion
            entre lo que opinionNegativaNPC le de, y la columna
            num_veces de la tabla personaje_roba_npc.
		* calcularOpinionTotalNPC --> Devuelve, segun el caso, una
			resta de la opinion positiva menos la opinion negativa,
            utilizando las funciones calcularOpinionPositiva y
            calcularOpinionNegativa. No permite que se devuelve un
            valor por encima de 20000 o menor a -20000.
        * estadoRelacionNpcPj --> Segun la puntuacion total en la
			tabla npc_opina_de_personaje, calculada con la funcion
            anterior, devolvera un varchar segun la puntuacion,
            pudiendo estos estar 'en guerra', 'enemistad', 'neutral',
            'amistad' o en 'alianza'.
*/

drop function if exists opinionPositivaNPC;
delimiter ~
create function opinionPositivaNPC(idPJ int, idNPC int, mot varchar(12))
returns int
begin
	declare puntuacion int;
	if (select count(*) from personaje where id = idPJ) < 1
		then return -1;
	elseif (select count(*) from npc where id = idNPC) < 1
		then return -2;
	elseif mot <> 'casarse' and mot <> 'vender' and mot <> 'comprar'
		then return -3;
	elseif (select count(*) from personaje_se_relaciona_con_npc
    where id_personaje = idPJ and id_npc = idNPC and motivo = mot) < 1
		then return 0;
    else
		if mot = 'casarse'
			then set puntuacion = 10000;
		elseif mot = 'comprar'
			then set puntuacion = 273;
		elseif mot = 'vender'
			then set puntuacion = 31;
        end if;
		return (select num_veces from personaje_se_relaciona_con_npc
		where id_personaje = idPJ and id_npc = idNPC and motivo = mot)
		* puntuacion;
	end if;
end;
~
delimiter ;

drop function if exists opinionNegativaNPC;
delimiter ~
create function opinionNegativaNPC(idPJ int, idNPC int, met varchar(12))
returns int
begin
	declare puntuacion int;
	if (select count(*) from personaje where id = idPJ) < 1
		then return -1;
	elseif (select count(*) from npc where id = idNPC) < 1
		then return -2;
	elseif met <> 'asesinato' and met <> 'hurto'
		then return -3;
    elseif (select count(*) from personaje_roba_npc
    where id_personaje = idPJ and id_npc = idNPC and metodo = met) < 1
		then return 0;
	else
		if met = 'asesinato'
			then set puntuacion = 1007;
		elseif met = 'hurto'
			then set puntuacion = 379;
        end if;
        return (select num_veces from personaje_roba_npc
		where id_personaje = idPJ and id_npc = idNPC)
        * puntuacion;
	end if;
end;
~
delimiter ;

drop function if exists opinionTotalNPC;
delimiter ~
create function opinionTotalNPC(idPJ int, idNPC int)
returns varchar(20)
begin
	declare puntP int;
	declare puntN int;
	if (select count(*) from personaje where id = idPJ) < 1
		then return 'PJ no existe';
	elseif (select count(*) from npc where id = idNPC) < 1
		then return 'NPC no existe';
	else
		set puntP = (select sum(opinion_sumada)
		from personaje_se_relaciona_con_npc
		where id_personaje = idPJ and id_npc = idNPC);
		set puntN = (select sum(opinion_restada)
		from personaje_roba_npc
		where id_personaje = idPJ and id_npc = idNPC);
		if puntP is null
			then set puntP = 0;
		end if;
		if puntN is null
			then set puntN = 0;
		end if;
		if puntP - puntN >= 50000
			then return 'alianza';
		elseif puntP - puntN >= 10000 and puntP - puntN < 50000
			then return 'amistad';
		elseif puntP - puntN > -10000 and puntP - puntN < 10000
			then return 'neutral';
		elseif puntP - puntN > -50000  and puntP - puntN <= -10000
			then return 'enemistad';
		elseif puntP - puntN <= -50000
			then return 'en guerra';
		end if;
	end if;
end;
~
delimiter ;

-- tabla personaje
update personaje
set nivel_medio_objeto = nivelMedioObj(id),
ataque_total = ataqueTotal(id),
defensa_total = defensaTotal(id),
velocidad_mov_total = velMovTotal(id),
estabilidad_total = estabilidadTotal(id),
parasitar_total = parasitarTotal(id),
celeridad_total = celeridadTotal(id),
num_NPC_asesinados = NPCsAsesinadosPorPj(id);

-- tabla personaje se relaciona con NPC
-- y tabla personaje roba a NPC
update personaje_se_relaciona_con_npc
set opinion_sumada = opinionPositivaNPC(id_personaje, id_npc, motivo);
update personaje_roba_npc
set opinion_restada = opinionNegativaNPC(id_personaje, id_npc, metodo);
update npc_opina_de_personaje
set opinion_personaje = opinionTotalNPC(id_personaje, id_npc);
