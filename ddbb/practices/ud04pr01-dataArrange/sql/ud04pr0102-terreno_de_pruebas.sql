use witchcraft;

-- funcion 1a
/* el PJ con id = 1 segun esta consulta tiene equipadas las vestimentas
	33, 65, y 72. Su nivel de objeto medio deberia ser: (27 + 299 + 47) / 3
    Lo cual es: 124 */
select nivel_objeto as nivel_vestimenta,
id_personaje, id_vestimenta
from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
where id_personaje = 1;
-- pero no es asi:
select nivel_medio_objeto
from personaje where id = 1;
-- usando la funcion nivelMedioObj(idPJ) podemos obtenerlo:
select nivelMedioObj(1);
-- seleccionar un ID inexistente
select nivelMedioObj(1234567890);
-- actualizar y volver a seleccionar el ID de personaje
update personaje
set nivel_medio_objeto = nivelMedioObj(id);
select nivel_medio_objeto
from personaje where id = 1;

-- funcion 1b
select ataque as ataque_vestimenta, ataque_base as ataque_raza,
raza.nombre as raza, id_personaje, id_vestimenta
from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
join personaje on personaje.id
= personaje_se_equipa_vestimenta.id_personaje
join raza on raza.id = personaje.id_raza
where id_personaje = 1;
-- seleccionar ataque total de PJ id = 1
select ataque_total
from personaje where id = 1;
-- seleccionarlo con el calculo de la funcion ataqueTotal:
select ataqueTotal(1);
-- seleccionar un ID inexistente
select ataqueTotal(-4);
-- actualizar y volver a seleccionar el ataque total del personaje
update personaje
set ataque_total = ataqueTotal(id);
select ataque_total
from personaje where id = 1;

-- funcion 1c
select defensa as defensa_vestimenta, defensa_base as defensa_raza,
raza.nombre as raza, id_personaje, id_vestimenta
from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
join personaje on personaje.id
= personaje_se_equipa_vestimenta.id_personaje
join raza on raza.id = personaje.id_raza
where id_personaje = 1;
-- seleccionar defensa total de PJ id = 1
select defensa_total
from personaje where id = 1;
-- seleccionarlo con el calculo de la funcion denfensaTotal:
select defensaTotal(1);
-- seleccionar un ID inexistente
select defensaTotal(504);
-- actualizar y volver a seleccionar la defensa total del personaje
update personaje
set defensa_total = defensaTotal(id);
select defensa_total
from personaje where id = 1;

-- funcion 1d
select velocidad_movimiento as vel_mov_vestimenta,
velocidad_mov_base as velocidad_raza,
velocidad_movimiento_clase, id_personaje, id_vestimenta
from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
join personaje on personaje.id
= personaje_se_equipa_vestimenta.id_personaje
join raza on raza.id = personaje.id_raza
join clase on clase.id = personaje.id_clase
where id_personaje = 1;
-- seleccionar velocidad de movimiento total de PJ id = 1
select velocidad_mov_total
from personaje where id = 1;
-- seleccionarlo con el calculo de la funcion velMovTotal:
select velMovTotal(1);
-- seleccionar un ID inexistente
select velMovTotal(102);
-- actualizar y volver a seleccionar la vel. de mov. total del personaje
update personaje
set velocidad_mov_total = velMovTotal(id);
select velocidad_mov_total
from personaje where id = 1;

-- funcion 1e
select estabilidad as estabilidad_vestimenta,
estabilidad_base as velocidad_raza, id_personaje, id_vestimenta
from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
join personaje on personaje.id
= personaje_se_equipa_vestimenta.id_personaje
join raza on raza.id = personaje.id_raza
join clase on clase.id = personaje.id_clase
where id_personaje = 1;
-- seleccionar estabilidad total de PJ id = 1
select estabilidad_total
from personaje where id = 1;
-- seleccionarlo con el calculo de la funcion estabilidadTotal:
select estabilidadTotal(1);
-- seleccionar un ID inexistente
select estabilidadTotal('zxcv12');
-- actualizar y volver a seleccionar la estabilidad total del personaje
update personaje
set estabilidad_total = estabilidadTotal(id);
select estabilidad_total
from personaje where id = 1;

-- funcion 1f
select parasitar as parasitar_vestimenta,
id_personaje, id_vestimenta
from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
join personaje on personaje.id
= personaje_se_equipa_vestimenta.id_personaje
join raza on raza.id = personaje.id_raza
join clase on clase.id = personaje.id_clase
where id_personaje = 1;
-- seleccionar parasitar total de PJ id = 1
select parasitar_total
from personaje where id = 1;
-- seleccionarlo con el calculo de la funcion parasitarTotal:
select parasitarTotal(1);
-- selecciono la funcion con el ID de un PJ que no lleva vestimentas con parasitar:
select parasitarTotal(3);
-- compruebo sus vestimentas
select parasitar as parasitar_vestimenta,
id_personaje, id_vestimenta
from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
join personaje on personaje.id
= personaje_se_equipa_vestimenta.id_personaje
join raza on raza.id = personaje.id_raza
join clase on clase.id = personaje.id_clase
where id_personaje = 3;
-- seleccionar un ID inexistente
select parasitarTotal(12341234);
-- actualizar y volver a seleccionar el parasitar total del personaje
update personaje
set parasitar_total = parasitarTotal(id);
select parasitar_total
from personaje where id = 1;

-- funcion 1g
select celeridad as celeridad_vestimenta,
id_personaje, id_vestimenta
from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
join personaje on personaje.id
= personaje_se_equipa_vestimenta.id_personaje
join raza on raza.id = personaje.id_raza
join clase on clase.id = personaje.id_clase
where id_personaje = 1;
-- busco un ID que si lleve equipadas vestimentas con celeridad:
select personaje.id from personaje_se_equipa_vestimenta
join personaje on personaje.id = personaje_se_equipa_vestimenta.id_personaje
join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
where vestimenta.celeridad is not null
group by personaje.id;
-- seleccionar parasitar total de PJ id = 1 y PJ id = 2
select celeridad_total, id
from personaje
where id = 1 or id = 2;
-- selecciono la funcion con los dos ID, uno detras de otro:
select celeridadTotal(1);
select celeridadTotal(2);
-- actualizar y volver a seleccionar la celeridad total de los personajes
update personaje 
set celeridad_total = celeridadTotal(id);
select celeridad_total, id
from personaje
where id = 1 or id = 2;

-- funcion 2
select id, num_NPC_asesinados from personaje;
-- compruebo las relaciones
select * from personaje_roba_npc;
-- uso la funcion NPCsAsesinadosPorPj
select NPCsAsesinadosPorPj(1);
-- actualizo tabla personaje y compruebo de nuevo
update personaje
set num_NPC_asesinados = NPCsAsesinadosPorPj(id);
select id, num_NPC_asesinados from personaje;

-- funcion 3
select comprobarFechaCorrecta('3500-16-87');

-- funcion 4
select personaje.id, tipo as clase, material_armadura as material_clase
from personaje join clase on clase.id = personaje.id_clase;
select vestimenta.id, material from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
join personaje on personaje.id = personaje_se_equipa_vestimenta.id_personaje
where personaje.id = 1;
select id from vestimenta where material = 'tela';
select materialCorrectoVestimentaClase(1,48596);

-- funcion 5a
-- uso la funcion con relaciones/IDs inexistentes
select opinionPositivaNPC(1,1, 'casarse');
select opinionPositivaNPC(80,1, 'comprar');
select opinionPositivaNPC(1,123, 'vender');
select opinionPositivaNPC(1,4,'asd');
-- veo las relaciones actuales
select * from personaje_se_relaciona_con_npc;
-- la uso con cada tipo de relacion
SELECT opinionPositivaNPC(1,19, 'comprar');
SELECT opinionPositivaNPC(1,29, 'casarse');
SELECT opinionPositivaNPC(5,2, 'vender');
-- actualizo y muestro
update personaje_se_relaciona_con_npc
set opinion_sumada = opinionPositivaNPC(id_personaje, id_npc, motivo);
select * from personaje_se_relaciona_con_npc;

-- funcion 5b
-- uso la funcion con relaciones/IDs inexistentes
select opinionNegativaNPC(1, 1, 'hurto');
select opinionNegativaNPC(76, 1, 'hurto');
select opinionNegativaNPC(1, 101, 'hurto');
select opinionNegativaNPC(1, 1, 'herramientas de ladron');
-- compruebo cuales existen
select * from personaje_roba_npc;
-- la uso con cada tipo de relacion
select opinionNegativaNPC(3, 35, 'hurto');
select opinionNegativaNPC(1, 25, 'asesinato');
update personaje_roba_npc
set opinion_restada = opinionNegativaNPC(id_personaje, id_npc, metodo);
select * from personaje_roba_npc;

-- funcion 5c
select * from npc_opina_de_personaje;
-- uso la funcion con IDs inexistentes
select opinionTotalNPC(155,1);
select opinionTotalNPC(1,1123);
-- consulta de un pj en ambas tablas de opinion
select * from personaje_se_relaciona_con_npc
join personaje_roba_npc 
on personaje_se_relaciona_con_npc.id_personaje
= personaje_roba_npc.id_personaje;
-- obtengo opinion con la funcion compruebo que es correcto
select opinionTotalNPC(3, 2);
select calcularOpinionTotalNPC(1, 1);
select calcularOpinionTotalNPC(2, 29);
select * from npc_opina_de_personaje where opinion_personaje <> 'neutral';
update npc_opina_de_personaje
set opinion_personaje = opinionTotalNPC(id_personaje, id_npc);
-- funcion 5c
select * from npc_opina_de_personaje;
-- pruebo con IDs inexistentes
select estadoRelacionNpcPj(123,12);
select estadoRelacionNpcPj(12,123);
-- pruebo con los mismos IDs que en la funcion anterior:
select estadoRelacionNpcPj(18, 77);
select estadoRelacionNpcPj(2, 46);
select estadoRelacionNpcPj(3, 2);
select estadoRelacionNpcPj(1, 1);
select estadoRelacionNpcPj(2, 29);
update npc_opina_de_personaje
set opinion_personaje = estadoRelacionNpcPj(id_personaje, id_npc);
select * from npc_opina_de_personaje;

-- procedimiento 1
/*
	insert into usuario(
	nombre, apellidos, fecha_nacimiento, fecha_alta_inscripcion,
    fecha_vencimiento_inscripcion, nickname, passw, metodo_pago,
    tarjeta_bancaria, paypal
	)
*/
call nuevoUser('nombre1', 'apellido1', '1990-07-30','2023-04-12', 'supermario', 'bross123','', '123412341234', '');
select * from usuario where nombre = 'nombre1';
call nuevoUser('nombre2', 'apellido2', '1998-12-07', '', 'suRealiza','passwd98','paypal','123456789101','sisoy@gmail.yeah');
select * from usuario where nombre = 'nombre2';
call nuevoUser('', '', '1998-12-07', '', 'Mr. Qwerty','qwerty','','','blank@mail.es');
select * from usuario where nickname = 'Mr. Qwerty';
update usuario
set id = 28  where nickname = 'Mr. Qwerty';

-- procedimiento 2
/*
	id_usuario, id_raza, id_clase, nombre, nivel, tatuajes, procedencia
*/
call nuevoPJ(28, 5, 3, 'Melina', 1, 1, '');
select * from personaje where nombre = 'Melina';
call nuevoPJ(1,3,4,'D', 50, 0, 'Elhaun`sharah');
select * from personaje where nombre = 'D';

-- procedimiento 3
/*

*/
call nuevaAdopcion(3, 'Comprobacion nombre no jaja', 45, '', 'aire');
select * from mascota where nombre like 'Comprobacion%';
delete from mascota where nombre like 'No hay%';
call nuevaAdopcion(67, 'Pistacho', 54, 'Peces electricos', 'fuego');
select * from mascota where nombre = 'Pistacho';
call nuevaAdopcion(43, 'Deathmark', 34, 'Oscuras aves','fantasma');
select * from mascota where nombre like 'Death%';

-- procedimiento 4
select * from personaje_posee_montura;
-- compruebo primero con relaciones ya existentes
call nuevaMontura(1,3);
call nuevaMontura(1,322);
call nuevaMontura(1,4);
select * from personaje_posee_montura
where id_personaje = 1 and id_montura = 4;

-- procedimiento 5a
select * from personaje_se_relaciona_con_npc where id_personaje = 1;
select * from npc_opina_de_personaje where id_personaje = 1 and opinion_personaje <> 'neutral';
call PJseRelacionaConNPC(123, 19123, 'asdf', 8);
call PJseRelacionaConNPC(1, 19123, 'asdf', 8);
call PJseRelacionaConNPC(1, 19, 'asdf', 8);
call PJseRelacionaConNPC(1, 19, 'vender', 8); 
call PJseRelacionaConNPC(1, 19, 'comprar', );
call PJseRelacionaConNPC(1, 29, 'casarse', 1);
call PJseRelacionaConNPC(1, 1, 'casarse', 9999);
select * from personaje_se_relaciona_con_npc where id_personaje = 1;
select * from npc_opina_de_personaje where id_personaje = 1 and id_npc = 19;
call PJseRelacionaConNPC(1, 19, 'comprar', 1600);

-- procedimiento 5b
select * from personaje_roba_npc where id_personaje = 1;
select * from npc_opina_de_personaje where id_personaje = 1 and id_npc = 25;
call PJrobaNPC(123, 253, 'asdf', 2);
call PJrobaNPC(1, 253, 'asdf', 2);
call PJrobaNPC(1, 25, 'asdf', 2);
call PJrobaNPC(1, 25, 'hurto', 2);
call PJrobaNPC(1, 25, 'asesinato', 2);
call PJrobaNPC(1, 1, 'hurto', 7);
select * from personaje_roba_npc where id_personaje = 1 and id_npc = 19;
select * from npc_opina_de_personaje where id_personaje = 1 and id_npc = 19;
call PJrobaNPC(1, 19, 'asesinato', 600);

-- procedimiento 6
select id_personaje as PJ, id_vestimenta as Vest,
vestimenta.tipo as tipoVest, clase.tipo as clase,
material_armadura as tipoArmadura, celeridad_total,
parasitar_total, estabilidad_total, velocidad_mov_total,
defensa_total, ataque_total, nivel_medio_objeto
from personaje_se_equipa_vestimenta
join personaje on personaje.id
= personaje_se_equipa_vestimenta.id_personaje
join clase on clase.id = personaje.id_clase
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
where id_personaje = 1;
select id, tipo from vestimenta where material = 'tela';
call equiparUnaVestimenta(123,123);
call equiparUnaVestimenta(1,123);
call equiparUnaVestimenta(1,1);
call equiparUnaVestimenta(1,33);
call equiparUnaVestimenta(1,5);
call equiparUnaVestimenta(1,8);
call equiparUnaVestimenta(1, 4);

-- trigger 1
insert into mascota(id_personaje,nombre,nivel,familia,tipo)
values(7, 'Panocho', 50, 'semihumano', 'metal');
select * from mascota where nombre = 'Panocho';
call nuevaAdopcion(4, 'Pinocho', 3, 'cortinas', 'fuego');
select * from mascota where nombre = 'Pinocho';

-- trigger 2
select id_personaje as PJ, id_vestimenta as Vest,
vestimenta.tipo as tipoVest, clase.tipo as clase,
material_armadura as tipoArmadura, celeridad_total,
parasitar_total, estabilidad_total, velocidad_mov_total,
defensa_total, ataque_total, nivel_medio_objeto
from personaje_se_equipa_vestimenta
join personaje on personaje.id
= personaje_se_equipa_vestimenta.id_personaje
join clase on clase.id = personaje.id_clase
join vestimenta on vestimenta.id
= personaje_se_equipa_vestimenta.id_vestimenta
where id_personaje = 1;
select id, tipo, nivel_objeto
from vestimenta where material = 'tela'
and celeridad is not null and parasitar is not null;
select * from vestimenta where id = 88;
insert into personaje_se_equipa_vestimenta
(id_personaje, id_vestimenta)
values(1, 96);
call equiparUnaVestimenta(1, 88);

-- trigger 3
select * from mascota where nombre = 'Panocho';
update mascota set id_personaje = 8
where nombre = 'Panocho';
select * from mascota where nombre = 'Pinocho';
update mascota set id_personaje = 8,
fecha_adopcion = '2022-04-14 12:00:01'
where nombre = 'Pinocho';
select * from mascota where nombre = 'Dori';
update mascota
set nombre = 'Dori' where nombre = 'Dora Reid';

-- trigger 4
select id_personaje, id_npc, metodo from personaje_roba_npc;
select num_npc_asesinados from personaje where id = 1 or id = 2 or id = 3;
update personaje_roba_npc set id_personaje = 1
where id_personaje = 2 and id_npc = 46;
select num_npc_asesinados from personaje where id = 1 or id = 2 or id = 3;
update personaje_roba_npc set id_personaje = 1
where id_personaje = 3 and id_npc = 35;
select num_npc_asesinados from personaje where id = 1 or id = 2 or id = 3;
select id_personaje, id_npc, metodo from personaje_roba_npc;

-- trigger 5
select * from montura where id = 7;
select nombre from npc where id = 12;
delete from montura where id = 7;
select * from montura;
select * from monturas_borradas where npc like 'Stacey%';

-- trigger 6
select id, nombre, celeridad_total as cele, parasitar_total as parasitar,
estabilidad_total as estabilidad, velocidad_mov_total as vel, defensa_total as def,
ataque_total as atk, nivel_medio_objeto as niv_obj
from personaje where id = 2;
select * from personaje_se_equipa_vestimenta where id_personaje = 2;
select id, nivel_objeto as nivelObj, ataque as atk, defensa as def,
celeridad as cele, estabilidad, parasitar, velocidad_movimiento as vel
from vestimenta where id = 7;
delete from personaje_se_equipa_vestimenta
where id_personaje = 2 and id_vestimenta = 7;
select * from personaje_se_equipa_vestimenta where id_personaje = 2;