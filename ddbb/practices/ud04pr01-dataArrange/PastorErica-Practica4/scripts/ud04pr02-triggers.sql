use witchcraft;

/*
	1. Trigger que le pone la fecha y hora a una adopcion de mascota
		si esta entra vacia a las actuales
*/

drop trigger if exists adopcionIns;
delimiter ~
create trigger adopcionIns before insert on mascota for each row
begin
	if new.fecha_adopcion is null or new.fecha_adopcion = ''
		then set new.fecha_adopcion = now();
    end if;
end;
~
delimiter ;

/*
	2. Trigger que calcula automaticamente el nivel medio de objeto de un personaje,
    asi como sus stats totales, usando las funciones nivelObjMedio, ataqueTotal
    defensaTotal, velMovTotal, estabilidadTotal, parasitarTotal y celeridadTotal
    tras una nueva insercion en la tabla personaje_se_equipa_vestimenta
*/

drop trigger if exists pjVestimentaIns;
delimiter ~
create trigger pjVestimentaIns after insert on personaje_se_equipa_vestimenta for each row
begin
	update personaje
	set nivel_medio_objeto = nivelMedioObj(new.id_personaje),
	ataque_total = ataqueTotal(new.id_personaje),
	defensa_total = defensaTotal(new.id_personaje),
	velocidad_mov_total = velMovTotal(new.id_personaje),
	estabilidad_total = estabilidadTotal(new.id_personaje),
	parasitar_total = parasitarTotal(new.id_personaje),
	celeridad_total = celeridadTotal(new.id_personaje)
	where id = new.id_personaje;
end;
~
delimiter ;

/*
	3. Trigger que pone la fecha y hora a una adopcion de mascota
		a las actuales si al cambiar de dueño no se modificase
        dicha fecha
*/

drop trigger if exists adopcionUp;
delimiter ~
create trigger adopcionUp before update on mascota for each row
	if old.id_personaje <> new.id_personaje then
		if new.fecha_adopcion = old.fecha_adopcion
			then set new.fecha_adopcion = now();
		end if;
    end if;
~
delimiter ;

/*
	4. Trigger que calculan y actualizan automaticamente el numero
    de NPCs asesinados por un personaje, en caso de haber actualizaciones
    en quien ha matado a quien.
*/

drop trigger if exists pjRobaNPCUp;
delimiter ~
create trigger pjRobaNPCUp after update on personaje_roba_npc for each row
begin
	if old.id_personaje <> new.id_personaje then
		if new.metodo = 'asesinato' then
			update personaje
			set num_NPC_asesinados = NPCsAsesinadosPorPj(new.id_personaje)
			where id = new.id_personaje;
			update personaje
			set num_NPC_asesinados = NPCsAsesinadosPorPj(old.id_personaje)
			where id = old.id_personaje;
		end if;
	end if;
end;
~
delimiter ;

/*
	5. Trigger que guarda los datos de una montura antes de borrarla
    y elimina sus apariciones en la tabla personaje_posee_montura
*/

drop trigger if exists borrarMonturas;
delimiter ~
create trigger borrarMonturas before delete on montura for each row
begin
	declare npcNombre varchar(50);
    set npcNombre = (select npc.nombre
    from npc join montura on montura.id_npc = npc.id 
    where npc.id = old.id_npc and montura.id = old.id);
	insert into monturas_borradas(npc, nombre,
    tipo_ambiente, tipo_forma, probabilidad_drop)
    values (npcNombre, old.nombre, old.tipo_ambiente,
    old.tipo_forma, old.probabilidad_drop);
	delete from personaje_posee_montura
    where id_montura = old.id;
end;
~
delimiter ;

/*
	6. Trigger que calcula automaticamente el nivel medio de objeto de un personaje,
    asi como sus stats totales, usando las funciones nivelObjMedio, ataqueTotal
    defensaTotal, velMovTotal, estabilidadTotal, parasitarTotal y celeridadTotal
    tras quitarle una vestimenta mediante el borrado de una linea en la tabla
    personaje_se_equipa_vestimenta
*/

drop trigger if exists pjVestimentaDel;
delimiter ~
create trigger pjVestimentaDel after delete on personaje_se_equipa_vestimenta for each row
begin
	update personaje
	set nivel_medio_objeto = nivelMedioObj(old.id_personaje),
	ataque_total = ataqueTotal(old.id_personaje),
	defensa_total = defensaTotal(old.id_personaje),
	velocidad_mov_total = velMovTotal(old.id_personaje),
	estabilidad_total = estabilidadTotal(old.id_personaje),
	parasitar_total = parasitarTotal(old.id_personaje),
	celeridad_total = celeridadTotal(old.id_personaje)
	where id = old.id_personaje;
end;
~
delimiter ;