use witchcraft;

/*
	1. Procedimiento que da de alta un usuario
		* Comprueba que la fecha de nacimiento este en un formato correcto
        * Comprueba si la fecha de vencimiento de inscripcion esta vacia,
			en caso de estarlo la calcula a un mes de distancia,
            y en caso no estarlo comprueba si el formato es correcto
		* Avisa si el nickname no este repetido, dado que es un dato UNIQUE
        * Comprueba que la password no este vacia
        * Comprueba que alguno de los metodos de pago esten rellenos
        * Ademas, calcula la fecha de inscripcion en la fecha actual (curdate())
	-- Para la comprobacion de formatos de fecha utiliza la funcion comprobarFechaCorrecta(fecha varchar(10))
*/

drop procedure if exists nuevoUser;
delimiter ~
create procedure nuevoUser(nombreUser varchar(25), apellidosUser varchar(25), fechaNac varchar(25),
	fechaVencInsc varchar(10), nickUser varchar(25), passwUser varchar(15), 
	metodoPago varchar(20), tarjetaBanc varchar(24), paypalUs varchar(40))
begin
	declare exit handler for sqlexception
    begin
		rollback;
        select 'Algo ha ido creando el usuario' as ooooooops_no_vuelvas_a_intentarlo;
	end;
    if (select comprobarFechaCorrecta(fechaNac)) = 0
		then select 'La fecha de nacimiento no es correcta' as fecha_nacimiento_incorrecta;
	else
		if (select count(*) from usuario where nickname = nickUser) > 0
			then select 'El nickname ya esta cogido.' as nickname_ya_existe;
        else
			if nickUser = '' or nickUser is null
				then select 'El nickname no puede estar vacio.' as rellena_tu_nick;
			elseif passwUser = ''
				then select 'La password no puede estar vacia' as passwd_vacia;
			else
				if tarjetaBanc = '' and paypalUs = ''
					then select 'Al menos un metodo de pago debe estar relleno' as falta_metodo_pago;
					else
					if metodoPago = '' then
						if tarjetaBanc <> ''
							then set metodoPago = 'tarjeta bancaria';
                        elseif paypalUs <> ''
							then set metodoPago = 'paypal';
                        end if;
					end if;
					if fechaVencInsc = '' or fechaVencInsc is null
						then set fechaVencInsc = date_add(curdate(), interval 1 month);
					end if;
					if (select comprobarFechaCorrecta(fechaVencInsc)) = 0
						then select 'La fecha de vencimiento no es correcta' as fecha_vencimiento_incorrecta;
					else
						start transaction;
						insert into usuario(nombre, apellidos, fecha_nacimiento, fecha_alta_inscripcion,
							fecha_vencimiento_inscripcion, nickname, passw, metodo_pago,
							tarjeta_bancaria, paypal)
						values (nombreUser, apellidosUser, fechaNac, curdate(),
							fechaVencInsc, nickUser, passwUser, 
							metodoPago, tarjetaBanc, paypalUs);
						commit;
					end if;
				end if;
            end if;
        end if;
	end if;
end;
~
delimiter ;

/*
	2. Procedimiento que da de alta un personaje
		* Comprueba que los ID de usuario, raza y clase existan
        * Comprueba que el nombre del personaje no este ya en la base de datos (dato UNIQUE)
        * Comprueba que el nivel del personaje este entre 1 y 120
        * Comprueba que el campo de tatuajes (boolean) no reciba otro dato que no sea 0 o 1
			Si lo recibe vacio tiene un valor DEFAULT de 0
		* Comprueba la procedencia (campo ENUM), si le lleva vacia, pone una por defecto
*/

drop procedure if exists nuevoPJ;
delimiter ~
create procedure nuevoPJ(idUser int, idRaza int, idClase int, nombrePJ varchar (25),
	nivelPJ int, tatus tinyint(1), procedenciaPJ varchar(25))
begin
	declare exit handler for sqlexception
    begin
		rollback;
        select 'Algo ha ido mal creando el personaje' as Oops;
	end;
    if (select count(*) from usuario where id = idUser) < 1
		then select 'El ID de usuario introducido no existe.' as usuario_no_existe;
	elseif (select count(*) from raza where id = idRaza) < 1
		then select 'El ID de raza introducido no existe.' as raza_no_existe;
	elseif (select count(*) from clase where id = idClase) < 1
		then select 'El ID de clase introducido no existe.' as clase_no_existe;
	elseif (select count(*) from personaje where nombre = nombrePJ) > 0
		then select 'El nombre de personaje ya esta cogido.' as nombre_de_PJ_ya_existe;
	elseif nivelPJ < 1 or nivelPJ > 120
		then select 'El nivel de personaje debe estar entre 1 y 120.' as nivel_fuera_de_rango;
	elseif tatus <> 1 and tatus <> 0
		then select 'Escribe 1 si lleva tatuajes o 0 si no los lleva.'
        as boolean_incorrecto;
	elseif nombrePJ = ''
		then select 'El nombre del personaje no puede estar vacio.' as rellena_el_nombre;
	elseif procedenciaPJ <> 'Jryushkp' and procedenciaPJ <> 'Elhaun`sharah'
	and procedenciaPJ <> 'Krrrprrgrrlkmpkah' and procedenciaPJ <> 'Ontaru'
	and procedenciaPJ <> 'Pyrokas' and procedenciaPJ <> 'Lykos' and procedenciaPJ <> 'Mo`onshar'
	and procedenciaPJ <> 'Tuyil' and procedenciaPJ <> 'Grupkyt' and procedenciaPJ <> 'Wqrsftmnvcxz'
	and procedenciaPJ <> 'Jiryosh`myan' and procedenciaPJ <> ''
		then select 'La procedencia no es correcta, consulta las opciones o deja el campo vacio.'
        as procedencia_incorrecta;
	else
		start transaction;
        if procedenciaPJ = '' then
			set procedenciaPJ = 'Krrrprrgrrlkmpkah';
        end if;
		insert into personaje(id_usuario, id_raza, id_clase, nombre, nivel, tatuajes, procedencia)
		values (idUser, idRaza, idClase, nombrePJ, nivelPJ, tatus, procedenciaPJ);
		commit;
	end if;
end;
~
delimiter ;

/*
	3. Procedimiento que para insertar nuevas mascotas que los personajes adopten:
		* Comprueba que el tipo de mascota sea correcto
        * Comprueba que el ID de personaje exista
        * Comprueba que el nivel de la mascota este dentro de los limites
        * Auto inserta la hora y fecha actuales en fecha de adopcion
*/

drop procedure if exists nuevaAdopcion;
delimiter ~
create procedure nuevaAdopcion(idPJ int, nombreMascota varchar(20), nivelMascota int,
	familiaMascota varchar(25), tipoMascota varchar(50))
begin
	declare exit handler for sqlexception
    begin
		rollback;
        select 'Algo ha ido mal adoptando a la mascota' as error_de_adopcion;
	end;
    if tipoMascota = 'fuego' or tipoMascota = 'tierra' or tipoMascota = 'aire'
		or tipoMascota = 'agua' or tipoMascota = 'arcano' or tipoMascota = 'electrico'
        or tipoMascota = 'hielo' or tipoMascota = 'metal' or tipoMascota = 'maquina'
		or tipoMascota = 'roca' or tipoMascota = 'hada' or tipoMascota = 'fantasma'
	then
		if (select count(*) from personaje where id = idPJ) > 0
        then
			if nivelMascota > 0 and nivelMascota < 61
            then
				start transaction;
				insert into mascota(id_personaje, nombre, nivel, familia, tipo, fecha_adopcion)
				values (idPJ, nombreMascota, nivelMascota, familiaMascota, tipoMascota, now());
				commit;
			else
				select 'El nivel de la mascota debe estar entre 1 y 60.' as nivel_fuera_de_limite;
			end if;
        else
			select 'El ID de personaje introducido no existe.' as id_pj_incorrecto;
        end if;
    else
		select 'El tipo no esta admitido' as tipo_incorrecto;
    end if;
end;
~
delimiter ;

/*
	4. Procedimiento que inserta una nueva obtencion de montura
		* Comprueba que el ID de personaje y de montura existan
        * Auto-inserta la fecha de obtencion en el dia actual
        * Si la relacion entre personaje y montura ya existe en
			la tabla personaje_posee_montura, avisa de que ya
            existe, y niega la nueva insercion.
*/

drop procedure if exists nuevaMontura;
delimiter ~
create procedure nuevaMontura(idPJ int, idMontura int)
begin
	declare exit handler for sqlexception
    begin
		rollback;
        select 'Algo ha ido mal obteniendo la montura' as Ooopsie;
	end;
	if (select count(*) from personaje where id = idPJ) < 1
		then select 'El ID del personaje introducido no existe' as id_pj_incorrecto;
    elseif (select count(*) from montura where id = idMontura) < 1
		then select 'El ID de la montura introducida no existe' as id_montura_incorrecto;
    elseif (select count(*) from personaje_posee_montura where id_personaje = idPJ and id_montura = idMontura) > 0
		then select 'Ya tienes esa montura' as montura_ya_obtenida;
    else
		start transaction;
        insert into personaje_posee_montura(id_personaje, id_montura, fecha_obtencion)
        values (idPJ, idMontura, curdate());
        commit;
	end if;
end;
~
delimiter ;

/*
	5. Procedimientos por los que insertamos nuevas relaciones o nuevos robos
		a) tabla personaje_se_relaciona_con_npc:
			* Si la relacion ya existe, actualiza el numero de veces que se han relacionado a +num
            * Si no existe, realiza un insert
        b) tabla personaje_roba_npc
			* Si la relacion ya existe, actualiza el numero de veces que el PJ ha robado al NPC a +num
            * Si no existe realiza un insert
	* Estos dos procedimientos, en cualquier caso afectan tambien a la tabla npc_opina_de_personaje,
	  dado que modifican la valoracion que los NPCs tienen sobre el PJ
*/

-- nuevas relaciones

drop procedure if exists PJseRelacionaConNPC;
delimiter ~
create procedure PJseRelacionaConNPC(idPJ int, idNPC int, mot varchar(8), num int)
begin
	declare exit handler
	for sqlexception
	begin
		rollback;
		select 'Algo ha ido mal.' as Oopsie;
	end;
	if (select count(*) from personaje where id = idPJ) < 1
		then select 'El ID del personaje no existe' as pj_no_existe;
	elseif (select count(*) from npc where id = idNPC) < 1
		then select 'El ID del NPC no existe' as npc_no_existe;
	elseif mot <> 'casarse' and mot <> 'comprar' and mot <> 'vender'
		then select 'Solo se admiten relaciones de comprar, vender o casarse'
		as motivo_incorrecto;
	else
		if (select count(*) from personaje_se_relaciona_con_npc
		where id_personaje = idPJ and id_npc = idNPC and motivo = mot) > 0
		then
			if mot = 'casarse' 
				then select 'No puedes casarte dos veces con el mismo.'
				as estas_borracho;
			else
				start transaction;
				update personaje_se_relaciona_con_npc
				set num_veces = num_veces + num
				where id_personaje = idPJ and id_npc = idNPC
				and motivo = mot;
				update personaje_se_relaciona_con_npc
				set opinion_sumada = opinionPositivaNPC(idPJ, idNPC, mot)
				where id_personaje = idPJ and id_npc = idNPC 
				and motivo = mot;
				update npc_opina_de_personaje
				set opinion_personaje = opinionTotalNPC(idPJ, idNPC)
                where id_personaje = idPJ and id_npc = idNPC;
                select 'Se ha modificado la relacion correctamente' as sucess;
				commit;
            end if;
		else
			start transaction;
				if mot = 'casarse'
					then set num = 1;
				end if;
				insert into personaje_se_relaciona_con_npc
				(id_personaje, id_npc, motivo, num_veces)
				values(idPJ, idNPC, mot, num);
				update personaje_se_relaciona_con_npc
				set opinion_sumada = opinionPositivaNPC(idPJ, idNPC, mot)
				where id_personaje = idPJ and id_npc = idNPC
				and motivo = mot;
				update npc_opina_de_personaje
				set opinion_personaje = opinionTotalNPC(idPJ, idNPC)
				where id_personaje = idPJ and id_npc = idNPC;
				select 'Se ha iniciado la relacion correctamente' as sucess;
			commit;
		end if;
	end if;
end;
~
delimiter ;

-- nuevos robos

drop procedure if exists PJrobaNPC;
delimiter ~
create procedure PJrobaNPC(idPJ int, idNPC int, met varchar(9), num int)
begin
	declare exit handler
	for sqlexception
	begin
		rollback;
		select 'Algo ha ido mal.' as Oopsie;
	end;
	if (select count(*) from personaje where id = idPJ) < 1
		then select 'El ID del personaje no existe' as pj_no_existe;
    elseif (select count(*) from npc where id = idNPC) < 1
		then select 'El ID del NPC no existe' as npc_no_existe;
	elseif met <> 'hurto' and met <> 'asesinato'
		then select 'Solo se admite hurto o asesinato' as metodo_incorrecto;
	else
		if (select count(*) from personaje_roba_npc
		where id_personaje = idPJ and id_npc = idNPC and metodo = met ) > 0
		then
			start transaction;
			update personaje_roba_npc
			set num_veces = num_veces + num
			where id_personaje = idPJ and id_npc = idNPC and metodo = met;
			update personaje_roba_npc
			set opinion_restada = opinionNegativaNPC(idPJ, idNPC, met)
			where id_personaje = idPJ and id_npc = idNPC and metodo = met;
			update npc_opina_de_personaje
			set opinion_personaje = opinionTotalNPC(idPJ, idNPC)
			where id_personaje = idPJ and id_npc = idNPC;
			select 'Se ha modificado el robo correctamente' as sucess;
			commit;
		else
			start transaction;
			insert into personaje_roba_npc(id_personaje, id_npc, metodo,
			num_veces)
			values(idPJ, idNPC, met, num);
			update personaje_roba_npc
			set opinion_restada = opinionNegativaNPC(idPJ, idNPC, met)
			where id_personaje = idPJ and id_npc = idNPC and metodo = met;
			update npc_opina_de_personaje
			set opinion_personaje = opinionTotalNPC(idPJ, idNPC)
			where id_personaje = idPJ and id_npc = idNPC;
			select 'Se ha completado el robo correctamente' as sucess;
            commit;
		end if;
	end if;
end;
~
delimiter ;

/*
	6. Insercion de una nueva vestimenta
    * Comprueba que el material sea correcto mediante la
		funcion materialCorrectoVestimentaClase
	* Comprueba que el ID de personaje y vestimenta existan
    * Actualiza las estadisticas del personaje mediante las
		funciones correspondientes
*/

drop procedure if exists equiparUnaVestimenta;
delimiter ~
create procedure equiparUnaVestimenta(idPJ int, idVest int)
begin
	declare exit handler for sqlexception
    begin
		rollback;
        select 'Algo ha ido mal equipando la vestimenta.' as Oooops;
	end;
	if (select count(*) from personaje where id = idPJ) < 1
		then select 'El ID de personaje seleccionado no existe' as PJ_no_existe;
	elseif (select count(*) from vestimenta where id = idVest) < 1
		then select 'El ID de vestimenta seleccionado no existe' as vestimenta_no_existe;
    elseif (select count(*) from personaje_se_equipa_vestimenta where id_personaje = idPJ and id_vestimenta = idVest) > 0
		then select 'Ya llevas equipada esa vestimenta' as eso_ya_lo_llevas;
	elseif (select materialCorrectoVestimentaClase(idPJ, idVest)) = 0
		then select 'El material no es adecuado para tu clase' as material_erroneo;
	else
		if (select count(*) from personaje_se_equipa_vestimenta
			join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
			where vestimenta.tipo = (select tipo from vestimenta where id = idVest)
            and id_personaje = idPJ) >= 1
			then select 'No puedes llevar mas un objeto del mismo tipo' as estas_borracho;
        else
			start transaction;
			insert into personaje_se_equipa_vestimenta(id_personaje,id_vestimenta)
            values(idPJ, idVest);
            update personaje
			set nivel_medio_objeto = nivelMedioObj(idPJ),
			ataque_total = ataqueTotal(idPJ),
			defensa_total = defensaTotal(idPJ),
			velocidad_mov_total = velMovTotal(idPJ),
			estabilidad_total = estabilidadTotal(idPJ),
			parasitar_total = parasitarTotal(idPJ),
			celeridad_total = celeridadTotal(idPJ)
            where id = idPJ;
			select 'Se ha equipado correctamente la vestimenta.' as vestimenta_equipada;
			commit;
		end if;
    end if;
end;
~
delimiter ;