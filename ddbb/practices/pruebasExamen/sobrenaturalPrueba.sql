use sobrenatural;


/*1.-Función que comprueba si una especie es vulnerable a cierta vulnerabilidad.
Devuelve 1 si la especie es vulnerable, 0 si no lo es y -1 si hay algún error.
La función recibirá el nombre de la vulnerabilidad y el nombre de la especie*/

drop function if exists especieDebilContra;
delimiter |~|
create function especieDebilContra(debilidad varchar(50), nombreEsp varchar(20))
returns int
begin
	if (select count(*) from vulnerabilidad where nombre like debilidad) > 0
		and (select count(*) from especie where nombre = nombreEsp) > 0
	then
		if (select count(*) from vulnerabilidad_especie
			join vulnerabilidad on vulnerabilidad.id = vulnerabilidad_especie.id_vulnerabilidad
            join especie on especie.id = vulnerabilidad_especie.id_especie
			where vulnerabilidad.nombre = debilidad and especie.nombre = nombreEsp) > 0
		then
			return 1;
		else
			return 0;
        end if;
    else
		return -1;
    end if;
end;
|~|
delimiter ;
-- pruebas mas adelante -- >>
-- -- >>
-- devuelve 0
select especieDebilContra('Puño de la verdad', 'Vampiro');
-- devuelve 1
select especieDebilContra('Corta alas', 'Vampiro');
-- devuelven -1
select especieDebilContra('qwerty', 'Vampiro');
select especieDebilContra('Puño de la verdad', 'qwerty');

/*2.-Función que usando la función anterior indica si un ser es vulnerable a cierta
vulnerabilidad*/

drop function if exists unSerDebilContra;
delimiter |~|
create function unSerDebilContra(debilidad varchar(50), nombreSer varchar(20))
returns int
begin
	if (select count(*) from vulnerabilidad where nombre like debilidad) > 0
		and (select count(*) from ser where nombre = nombreSer) > 0
	then
		if (select especieDebilContra(debilidad, (select especie.nombre
			from especie join ser on ser.id_especie = especie.id
            where ser.nombre = nombreSer))) = 1
		then
			return 1;
        else
			return 0;
        end if;
	else
		return -1;
	end if;
end;
|~|
delimiter ;

-- visiones de pruebas mas adelante... -- >>
-- -- >>
-- devuelve 0
select unSerDebilContra('Puño de la verdad', 'Jonah Miller');
-- devuelve 1
select unSerDebilContra('Corta alas', 'Jonah Miller');
-- devuelven -1
select unSerDebilContra('qwerty', 'Jonah Miller');
select unSerDebilContra('Puño de la verdad', 'qwerty');

/*3.-Procedimiento que registra una nueva vulnerabilidad para una especie conocida.
Si la vulnerabilidad ya existe en el registro, solo registrará su relación con la especie,
sino la creará primero y luego las vinculará*/

drop procedure if exists nuevaDebilidad;
delimiter |~|
create procedure nuevaDebilidad(debilidad varchar(50), nombreEsp varchar(20))
begin
	declare exit handler
		for sqlexception
		begin
		rollback;
		select 'Algo ha salido mal' as oopsie;
    end;
    if (select count(*) from especie where nombre = nombreEsp) > 0
    then
		if (select count(*) from vulnerabilidad where nombre like debilidad) > 0
		then
			if (select especieDebilContra(debilidad, nombreEsp)) < 1
			then
				start transaction;
					insert into vulnerabilidad_especie(id_vulnerabilidad, id_especie)
					values
                    (
						(select id from vulnerabilidad where nombre = debilidad)
                        ,
						(select id from especie where nombre = nombreEsp)
					);
					select 'La debilidad ha sido añadida' as operacion_exitosa;
				commit;
			else
				select 'Esa especie ya esta asociada a esa debilidad' as ya_existe;
			end if;
		else
			start transaction;
				insert into vulnerabilidad(nombre)
                values(debilidad);
                insert into vulnerabilidad_especie(id_vulnerabilidad, id_especie)
                values
                (
					(select last_insert_id())
					,
					(select id from especie where nombre = nombreEsp)
				);
                select 'La debilidad ha sido añadida' as operacion_exitosa;
			commit;
		end if;
	else
		select 'La especie introducida no existe.' as especie_no_existe;
    end if;
end;
|~|
delimiter ;

-- se requiere batalla a caballo mas adelante -- -- >>
-- -- >>
-- nos dira que la especie no existe
call nuevaDebilidad('estaca de fresno magico', 'Qwerty');
-- nos dira que ya esta asociada
call nuevaDebilidad('la mano derecha de gordon epperson', 'Licántropo');
-- inserta solamente en vulnerabilidad_especie
call nuevaDebilidad('la victoria de monty','Licántropo');
-- inserta en vulnerabilidad y en vulnerabilidad_especie
call nuevaDebilidad('Lapicero de un aprendiz en Raya Lucaria', 'Vampiro');

/*4.-Añade el campo 'disponible' a la tabla libro_base con valor por defecto 1.
Crea un procedimiento llamado 'prestar' que recibiendo el id de una base y de un libro
ponga disponible a 0. Si disponible ya valía 0 deberá devolver un mensaje diciendo
que el libro no está disponible.*/

-- caballo mas adelante
alter table libro_base
add column disponible tinyint(1) default 1;

-- cuidado con arriba
-- 	y entonces, pepinillo!
drop procedure if exists  prestarLibro;
delimiter |~|
create procedure prestarLibro(idBase int unsigned, idLibro int unsigned)
begin
	declare exit handler
		for sqlexception
		begin
		rollback;
		select 'Algo ha salido mal' as oopsie;
    end;
	if (select count(*) from base where id = idBase) > 0
	then
		if (select count(*) from libro where id = idLibro) > 0
        then
			if (select count(*) from libro_base where id_libro = idLibro and id_base = idBase) > 0
			then
				if (select disponible from libro_base where id_libro = idLibro and id_base = idBase) >= 1
				then start transaction;
						update libro_base
						set disponible = 0
						where id_libro = idLibro and id_base = idBase;
                        select 'Se ha prestado el caballo' as operacion_exitosa;
					commit;
                else
					select 'El caballo ya esta prestado' as no_disponible;
                end if;
            else
				select 'El caballo no esta en esa base' nope;
			end if;
		else
			select 'El ID del libro no existe' as libro_no_existe;
		end if;
	else
		select 'El ID de la base no existe' as base_no_existe;
    end if;
end;
|~|
delimiter ;
-- pepinillo! por lo tanto, cuidado con abajo
	-- devuelve que no existe la base:
	call prestarLibro(36, 72);
	-- devuelve que no existe el libro:
	call prestarLibro(35, 72);
    -- devuelve que el libro no esta en esa base
	call prestarLibro(35, 71);
    -- presta el libro
    call prestarLibro(35, 5);
    select * from libro_base where id_base = 35 and id_libro = 5;
    -- si lo intentas de nuevo, te dice que ya esta prestado
    call prestarLibro(35, 5);
    select * from libro_base where id_base = 35 and id_libro = 5;

/*5.-Crea el procedimiento 'devolver' complementario al anterior*/

-- perro mas adelante
-- 	y entonces, pepinillo...
drop procedure if exists  devolverLibro;
delimiter |~|
create procedure devolverLibro(idBase int unsigned, idLibro int unsigned)
begin
	declare exit handler
		for sqlexception
		begin
		rollback;
		select 'Algo ha salido mal' as oopsie;
    end;
	if (select count(*) from base where id = idBase) > 0
	then
		if (select count(*) from libro where id = idLibro) > 0
        then
			if (select count(*) from libro_base where id_libro = idLibro and id_base = idBase) > 0
			then
				if (select disponible from libro_base where id_libro = idLibro and id_base = idBase) = 0
				then start transaction;
						update libro_base
						set disponible = 1
						where id_libro = idLibro and id_base = idBase;
                        select 'Se ha devuelto el perro a la caseta' as operacion_exitosa;
					commit;
                else
					select 'No se puedo devolver al perro.' as operacion_cancelada,'El perro ya estaba en la caseta' as pues_eso;
                end if;
            else
				select 'El perro no esta en esa caseta' nope;
			end if;
		else
			select 'El ID del libro no existe' as libro_no_existe;
		end if;
	else
		select 'El ID de la base no existe' as base_no_existe;
    end if;
end;
|~|
delimiter ;

-- pepinillo! por lo tanto, cuidado con abajo

	-- devuelve que no existe la base:
	call devolverLibro(36, 72);
	-- devuelve que no existe el libro:
	call devolverLibro(35, 72);
    -- devuelve que el libro no esta en esa base
	call devolverLibro(35, 71);
    -- presta el libro
    call devolverLibro(35, 5);
    select * from libro_base where id_base = 35 and id_libro = 5;
    -- si lo intentas de nuevo, te dice que ya ha sido devuelto
    call devolverLibro(35, 5);
    select * from libro_base where id_base = 35 and id_libro = 5;
    
/*6.Añade el campo total ingredientes a la tabla hechizo y mantenlo actualizado mediante
triggers*/

-- 
alter table hechizo add column total_ingredientes int;
-- 
drop function if exists totalIngredientes;
delimiter |~|
create function totalIngredientes(idHechizo int unsigned)
returns int
begin
	if (select count(*) from hechizo where id = idHechizo) > 0
    then
		return (select count(*) from ingrediente_hechizo
			join ingrediente on ingrediente.id = ingrediente_hechizo.id_ingrediente
            join hechizo on hechizo.id = ingrediente_hechizo.id_hechizo
            where ingrediente_hechizo.id_hechizo = idHechizo);
    else
		return -1;
    end if;
end;
|~|
delimiter ;

drop trigger if exists totalIngredientesIns;
delimiter |~|
create trigger totalIngredientesIns after insert on ingrediente_hechizo for each row
begin
		update hechizo
        set total_ingredientes = totalIngredientes(new.id_hechizo)
        where id = new.id_hechizo;
end;
|~|
delimiter ;

drop trigger if exists totalIngredientesUp;
delimiter |~|
create trigger totalIngredientesUp after update on ingrediente_hechizo for each row
begin
		if (new.id_hechizo <> old.id_hechizo)
        then
			update hechizo
			set total_ingredientes = totalIngredientes(old.id_hechizo)
			where id = old.id_hechizo;
			update hechizo
			set total_ingredientes = totalIngredientes(new.id_hechizo)
			where id = new.id_hechizo;
        end if;
end;
|~|
delimiter ;

drop trigger if exists totalIngredientesDel;
delimiter |~|
create trigger totalIngredientesDel after delete on ingrediente_hechizo for each row
begin
		update hechizo
        set total_ingredientes = totalIngredientes(old.id_hechizo)
        where id = old.id_hechizo;
end;
|~|
delimiter ;

-- visiones de estrellas...
update hechizo
set total_ingredientes = totalIngredientes(id);
-- no hay camino oculto mas adelante
insert into ingrediente_hechizo(id_ingrediente, id_hechizo)
values(72, 1);
-- mentiroso mas adelante,
-- 	por tanto, cuidado con izquierda
update ingrediente_hechizo
set id_hechizo = 2
where id_hechizo = 1 and id_ingrediente = 72;
-- no tienes derecho, O' no tienes derecho
-- 	por tanto, bien hecho
delete from ingrediente_hechizo where id_hechizo = 2 and id_ingrediente = 72;

/*7.-Añade el campo 'probabilidad_de_victoria a misión que vale 0 por defecto.
El valor de este campo es igual al número de agentes dividido entre la suma
del número de agentes y el número de seres. Mantén el campo actualizado mediante
triggers*/

-- caballo mas adelante,
	-- por tanto, visiones de estrellas
alter table mision add column probabilidad_de_victoria float default 0.0;

drop function if exists calcularProbVictoria;
delimiter |~|
create function calcularProbVictoria(idMision int unsigned)
returns float
begin
	if (select count(*) from mision where id = idMision) > 0
    then
		return (select count(*) from agente_mision where id_mision = idMision)
			/
            ((select count(*) from agente_mision where id_mision = idMision)
            + (select count(*) from mision_ser where id_mision = idMision));
    else
		return -1.0;
	end if;
end;
|~|
delimiter ;

drop trigger if exists insMisionSer;
delimiter |~|
create trigger insMisionSer after insert on mision_ser for each row
begin
	update mision
    set probabilidad_de_victoria = calcularProbVictoria(new.id_mision)
    where id = new.id_mision;
end;
|~|
delimiter ;

drop trigger if exists insAgenteMision;
delimiter |~|
create trigger insAgenteMision after insert on agente_mision for each row
begin
	update mision
    set probabilidad_de_victoria = calcularProbVictoria(new.id_mision)
    where id = new.id_mision;
end;
|~|
delimiter ;


drop trigger if exists upMisionSer;
delimiter |~|
create trigger upMisionSer after update on mision_ser for each row
begin
	if new.id_mision <> old.id_mision
    then
		update mision
		set probabilidad_de_victoria = calcularProbVictoria(new.id_mision)
		where id = new.id_mision;
		update mision
		set probabilidad_de_victoria = calcularProbVictoria(old.id_mision)
		where id = old.id_mision;
	end if;
end;
|~|
delimiter ;

drop trigger if exists upAgenteMision;
delimiter |~|
create trigger upAgenteMision after update on agente_mision for each row
begin
	if new.id_mision <> old.id_mision
    then
		update mision
		set probabilidad_de_victoria = calcularProbVictoria(new.id_mision)
		where id = new.id_mision;
		update mision
		set probabilidad_de_victoria = calcularProbVictoria(old.id_mision)
		where id = old.id_mision;
	end if;
end;
|~|
delimiter ;

drop trigger if exists delMisionSer;
delimiter |~|
create trigger delMisionSer after delete on mision_ser for each row
begin
	update mision
    set probabilidad_de_victoria = calcularProbVictoria(old.id_mision)
    where id = old.id_mision;
end;
|~|
delimiter ;

drop trigger if exists delAgenteMision;
delimiter |~|
create trigger delAgenteMision after delete on agente_mision for each row
begin
	update mision
    set probabilidad_de_victoria = calcularProbVictoria(old.id_mision)
    where id = old.id_mision;
end;
|~|
delimiter ;

-- podria tratarse de... caballo?
	-- en resumen, prueba objeto mas adelante
select * from mision where id = 555 or id = 556;

update mision
set probabilidad_de_victoria = calcularProbVictoria(id);
insert into mision_ser(id_ser, id_mision)
values(1, 555);
update mision_ser
set id_mision = 556
where id_ser = 1 and id_mision = 555;
delete from mision_ser
where id_ser = 1 and id_mision = 556;