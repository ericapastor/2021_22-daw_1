use Stellar;

-- prueba examen

#1. (P)Nombre y cargos que ha ocupado una determinada persona
drop procedure if exists nombreCargosPersona;
delimiter |
create procedure nombreCargosPersona(idPersona int)
begin
	if (select count(*) from persona where persona.id = idPersona) < 1 then
    select 'El id introducido no corresponde a ninguna persona registrada.' as ningunaPersona;
    else
	select nombre, cargo from persona join tripulante on persona.id = tripulante.id_persona where persona.id = idPersona;
    end if;
end|
delimiter ;
#2. (P)Nombre de las personas originarias de cuerpos espaciales que orbitan estrellas
drop procedure if exists personasDeCuerpoQueOrbitaEstrella;
delimiter |
create procedure personasDeCuerpoQueOrbitaEstrella()
begin
	select persona.nombre
    from persona
    join cuerpo_celeste on cuerpo_celeste.id = persona.id_cuerpo_celeste
    join cuerpo_celeste_orbita_cuerpo_celeste on cuerpo_celeste.id = cuerpo_celeste_orbita_cuerpo_celeste.id_cuerpo_celeste_orbitador
    join estrella on estrella.id = cuerpo_celeste_orbita_cuerpo_celeste.id_cuerpo_celeste_orbitado;
end|
delimiter ;

#3.(F)Número de especies registradas con un tipo de alimentación determinado
drop function if exists numEspeciesSegunAlimentacion;
delimiter |
create function numEspeciesSegunAlimentacion(tipoAlimentacion varchar(20))
returns int
begin
	if (select count(*) from especie where tipoAlimentacion <> 'vampirismo' 
    and tipoAlimentacion <> 'herbívoro' and tipoAlimentacion <> 'carnívoro' 
    and tipoAlimentacion <> 'geovoro' and tipoAlimentacion <> 'omnívoro')
    then return -1;
    else return (select count(*) from especie where tipo_alimentacion = tipoAlimentacion group by tipo_alimentacion);
	end if;
end|
delimiter ;

#4.Función que dada una coalición nos dice si tiene más de X naves aparcadas en su territorio.
drop function if exists numNavesAparcadasDeUnaCoalicion;
delimiter |
create function numNavesAparcadasDeUnaCoalicion(idCoalicion int, numNaves int)
returns int
begin
	if (select count(*) from coalicion where coalicion.id = idCoalicion) < 1
    then return -1;
    elseif (select count(*) from coalicion
		join coalicion_cuerpo_celeste on coalicion_cuerpo_celeste.id_coalicion = coalicion.id
        join nave on coalicion_cuerpo_celeste.id_cuerpo_celeste = nave.id_cuerpo_celeste
        where coalicion.id = idCoalicion) >= numNaves
	then return 1;
    else return 0;
    end if;
end|
delimiter ;
#5.(P)Elimina una especie. Si ésta tiene personas asignadas no podrá ser eliminada.
drop procedure if exists eliminarEspecie;
delimiter |
create procedure eliminarEspecie(idEspecie int)
begin
	declare exit handler 
	for sqlexception
	begin
		rollback;
		select 'No se ha podido eliminar la especie', especie.nombre
        from especie
        where especie.id = idEspecie;
	end;
	if (select count(*) from especie where especie.id = idEspecie) < 1
    then select 'La especie introducida no existe' as EspecieNoExiste;
    else
		if (select count(*) from persona
        join especie_persona on persona.id = especie_persona.id_persona
        join especie on especie.id = especie_persona.id_especie
        where especie.id = idEspecie) >= 1
        then select 'La especie tiene personas en ella, no puede eliminarse' as especieConPersonas;
        else
        delete from especie where especie.id = idEspecie;
        end if;
    end if;
end|
delimiter ;

#6.(P)Elimina un tripulante. Si está asignado a alguna nave, se desvinculará antes de ésta
drop procedure if exists eliminarTripulante;
delimiter |
create procedure eliminarTripulante(idTripulante int)
begin
	declare exit handler 
	for sqlexception
	begin
		rollback;
		select 'No se ha podido eliminar el tripulante', persona.nombre
        from persona
        join tripulante on persona.id = tripulante.id_persona
        where tripulante.id = idTripulante;
	end;
	if (select count(*) from tripulante where idTripulante = tripulante.id) <1
    then select 'El id del tripulante no existe';
    else
		if (select count(*) from tripulante join nave_tripulante on tripulante.id = nave_tripulante.id_tripulante where tripulante.id = idTripulante) >=1
        then delete from nave_tripulante where nave_tripulante.id_tripulante = idTripulante;
        delete from tripulante where tripulante.id = idTripulante;
        end if;
    end if;
end|
delimiter ;

#7.(P)Modifica la coalicion de una nave. Comprueba que la coalición existe
drop procedure if exists modificarCoalicionDeNave;
delimiter |
create procedure modificarCoalicionDeNave(idNave int, idCoalicion int)
begin
	if (select count(*) from coalicion where coalicion.id = idCoalicion) < 1
    then select 'La coalicion introducida no existe';
    else
		if (select count(*) from nave where nave.id = idNave) < 1
		then select 'La nave introducida no existe';
        else
        update nave
			set id_coalicion = idCoalicion
            where nave.id = idNave;
		end if;
    end if;
end|
delimiter ;

#8.(P)Registra una nueva persona, comprobando que su especie está registrada.
drop procedure if exists registrarPersona;
delimiter |
create procedure registrarPersona(nombreP varchar(20), edadP int, idCuerpo int, idEspecie int)
begin
	declare exit handler 
	for sqlexception
	begin
		rollback;
		select 'No se ha podido registrar la persona';
	end;
	if (select count(*) from cuerpo_celeste where idCuerpo = cuerpo_celeste.id) < 1
    then select 'El cuerpo celeste introducido no existe';
	else
		if (select count(*) from especie where especie.id = idEspecie) < 1
        then select 'La especie introducida no existe';
        else
        start transaction;
        insert into persona(nombre,edad,id_cuerpo_celeste)
        values (nombreP, edadP, idCuerpo);
        insert into especie_persona(id_especie, id_persona)
        values (idEspecie, last_insert_id());
        commit;
        end if;
    end if;
end|
delimiter ;

call registrarPersona('Effy Stonem', 17, 2, 4);

#9.(P)Da de alta una nueva nave asignándola a una coalición determinada y a un
-- cuerpo celeste donde está estacionada
drop procedure if exists altaNave;
delimiter |
create procedure altaNave(nombreN varchar(50), identificadorN varchar(15), tipoN varchar(20), cargaN int, n_tripulantesN int unsigned, idCuerpo int unsigned, idCoalicion int unsigned)
begin
	declare exit handler 
	for sqlexception
	begin
		rollback;
		select 'No se ha podido dar de alta la nave' as naveNoExiste;
	end;
    if tipoN = 'carga' or tipoN = 'combate' or tipoN = 'exploración' or tipoN = 'pasajeros' then
		if (select count(*) from cuerpo_celeste where cuerpo_celeste.id = idCuerpo) < 1
		then select 'El cuerpo celeste seleccionado no existe' as cuerpoCelesteNoExiste;
		else
			if (select count(*) from coalicion where coalicion.id = idCoalicion) < 1
			then select 'La coalicion seleccionada no existe' as coalicionNoExiste;
			else
			start transaction;
				insert into nave(nombre, identificador, tipo, capacidad_carga, n_tripulantes, id_cuerpo_celeste, id_coalicion)
				values(nombreN, identificadorN, tipoN, cargaN, n_tripulantesN, idCuerpo, idCoalicion);
			commit;
			end if;
		end if;
        else
        select 'El tipo de nave no se admite';
    end if;
end|
delimiter ;

#10.(P)Elimina una coalición, asigna todos sus cuerpos celestes y naves a otra
-- coalición dada. Llama al procedimiento ‘Conquista’
drop procedure if exists eliminarCoalicion;
delimiter |
create procedure eliminarCoalicion(idCoalicionEliminar int, idCoalicionAsignar int)
begin
	declare exit handler 
	for sqlexception
	begin
		rollback;
		select 'No se ha podido eliminar la coalicion';
	end;
	if (select count(*) from coalicion where coalicion.id = idCoalicionAsignar) < 1
	then select 'La coalicion seleccionada para la nueva asignacion no existe';
    elseif (select count(*) from coalicion where coalicion.id = idCoalicionEliminar) < 1
	then select 'La coalicion seleccionada para eliminarse no existe' as coalicionNoExiste;
    else
		start transaction;
        update nave
        set id_coalicion = idCoalicionAsignar
        where id_coalicion = idCoalicionEliminar;
        update coalicion_cuerpo_celeste
        set id_coalicion = idCoalicionAsignar
        where id_coalicion = idCoalicionEliminar;
        delete from coalicion where coalicion.id = idCoalicionEliminar;
        commit;
    end if;
end|
delimiter ;

#11.(P)Elimina un cuerpo espacial, desvincúlalo de todas las especies, coaliciones y naves.
-- Elimina también la referencia de las personas nacidas en él. Llama al procedimiento’ DestruccionPlanetaria’
drop procedure if exists eliminarCuerpoEspacial;

#12.(P)Da de alta un nuevo cuerpo celeste y hazlo orbitar en torno a una estrella
-- (Comprueba que el cuerpo sobre el que va a orbitar es una estrella). Llámalo ‘NuevoHorizonte’.
drop procedure if exists nuevoHorizonte;
delimiter |
create procedure nuevoHorizonte(unaEstrella int)
begin
	
end;|
delimiter ;