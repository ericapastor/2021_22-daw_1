/*
	1. Nombres de todos los usuarios que contengan una 'e' o una 'i' mayores de 22 años,
	   y nombres de sus personajes, mostrando cuales de estos personajes son driadas
		* Condicional case
		* ILIKE (propio de postgre): para no diferenciar entre minus y mayus
		* Outer join
	   De los requisitos:
		* Datos estructurados/compuestos (nombre de usuario)
		* Funcion de fecha age(fecha, fecha)
*/
select (usuario.nombre).nombre as usuario, personaje.nombre as personaje,
	case when personaje.id_clase = pjs_driadas.id_driada then 'si' else 'no'
	end as es_driada
from usuario left join personaje on personaje.id_usuario = usuario.id
left join
	(select clase.id as id_driada
	from clase where clase.nombre = 'driada') as pjs_driadas
on personaje.id_clase = pjs_driadas.id_driada
where age(current_date, usuario.fecha_nacimiento) >= '22 years'
and ((usuario.nombre).nombre ilike '%e%' or (usuario.nombre).nombre ilike '%i%')
order by usuario.id;

/*
	2. Nombre de los personajes que adoptaron mascotas antes de 2021 cuyos
	   usuarios tengan mas de un telefono registrado en el juego
	   (Pueden repetirse personajes o las mascota dado que pueden haber
	   adoptado o sido adoptadas mas de una vez)
		* Subconsultas
	   De los requisitos:
		* Funcion de fecha extract (field from date)
		* Colecciones (array telefonos): funcion 'array_length (array, interval)'
*/
select masc_prev_21.nombrePJ as personaje, masc_prev_21.nombreMasc as mascota
from usuario
join (select mascota.nombre as nombreMasc, personaje.id_usuario as idUser,
	personaje.nombre as nombrePJ
	from personaje_mascota
	join mascota on mascota.id = personaje_mascota.id_mascota
	join personaje on personaje.id = personaje_mascota.id_personaje
	where extract (year from fecha_adopcion) < 2021) as masc_prev_21
on usuario.id = masc_prev_21.idUser
where array_length(usuario.telefono, 1) > 1;

/*
	3. Habilidades que no sean de ataque, ni defensivas ni curativas
	   y sean comunes a todas las clases
		* Subconsultas
	   De los requisitos:
		* Caracteristicas de herencia (only)
*/
select habilidad.nombre
from only habilidad
join (select num_clases.num_clases_por_hab, num_clases.idHab as id_hab
	from (select count(*) as num_clases_por_hab, clase_habilidad.id_habilidad as idHab
		from clase_habilidad
		join habilidad on habilidad.id = clase_habilidad.id_clase
		join clase on clase.id = clase_habilidad.id_clase
		group by clase_habilidad.id_habilidad) as num_clases
		where num_clases.num_clases_por_hab = (select count(*) from clase)) as habilidades_comunes
	on habilidad.id = habilidades_comunes.id_hab
group by habilidad.id;

/*
	4. Nombre de los personajes y nombre completo de sus usuarios que tengan en su
	   inventario mas de 5 vestimentas con calidad inferior a la normal las cuales
	   tengan mas de 15 de ataque o mas de 15 de defensa en las estadisticas y
	   cuyos usuarios hayan registrado como ultimo email una cuenta gmail
		* Subconsultas
	   De los requisitos:
		* Colecciones (array de emails): funcion 'array_length(array, interval)'
		* Datos estructurados/compuestos (estadisticas de vestimenta y nombre de usuario)
		* Funcion de enum, comparador '<' (calidad de vestimenta)
*/
select (usuario.nombre).*, personaje.nombre as personaje
from pj_tiene_vest
join personaje on personaje.id = pj_tiene_vest.id_personaje
join usuario on usuario.id = personaje.id_usuario
join (select vestimenta.id as idVest
	from vestimenta
	where vestimenta.calidad < 'normal'
	  group by vestimenta.id
	having (vestimenta.estadisticas).atk > 15
	  or (vestimenta.estadisticas).def > 15) as vest_cutre
on vest_cutre.idVest = pj_tiene_vest.id_vestimenta
where pj_tiene_vest.cantidad > 5
and email [array_length(email, 1)] like '%gmail%'
order by usuario.id desc;

/*
	5. Nombre de todos los personajes y si han obtenido en la
	   primavera de 2021 monturas con una probabilidad de drop menor
	   al 5% que puedan ser usadas en un ambiente acuatico y su forma
	   sea espiritual.
		* Mas de una tabla
		* Condicional CASE
	   De los requistitos:
		* Fechas
*/
select personaje.nombre as personaje,
case when monts_acuat_prim20.nMont is null
then '' else monts_acuat_prim20.nMont end
as montura_prim_2020
from personaje
left join raza on raza.id = personaje.id_raza
left join (select montura.nombre as nMont, montura.forma as fMont, personaje.id as idPJ
	from personaje_montura
	join montura on montura.id = personaje_montura.id_montura
	join personaje on personaje.id = personaje_montura.id_personaje
	where montura.probabilidad_drop < 5
	and montura.ambiente = 'acuatico'
	and fecha_obtencion between '2020-03-21' and '2020-06-20'
	and montura.forma = 'espiritual')
	as monts_acuat_prim20
on monts_acuat_prim20.idPJ = personaje.id;

/*
	6. Encuentra quien es el personaje mas odiado y y muestra su nombre
	   de usuario, si tiene o no una cuenta yahoo y cuantas horas jugadas
	   tiene.
		* Muchas subconsultas
		* Mas de una tabla
		* Conditional CASE
	   De los requisitos:
		* Colecciones (array email)
		* Funcion de enum 'enum_first'

*/
select (usuario.nombre).nombre,
case when '%yahoo%' ilike any(usuario.email) then 'si' else 'no' end
as tiene_cuenta_yahoo, horas_jugadas
from npc_opina_de_pj
join personaje on personaje.id = npc_opina_de_pj.id_personaje
join usuario on  usuario.id = personaje.id_usuario
join (select count(*) as num_npcs, personaje.id as idPJ
	from npc_opina_de_pj
	join personaje on personaje.id = npc_opina_de_pj.id_personaje
	where enum_first(opinion_personaje)= opinion_personaje
	group by personaje.id) as pjs_odiados
on personaje.id = pjs_odiados.idPJ
where pjs_odiados.num_npcs = (select max(pjs_odiados2.num_npcs) as pj_mas_odiado
	from (select count(*) as num_npcs
	from npc_opina_de_pj
	join personaje on personaje.id = npc_opina_de_pj.id_personaje
	where enum_first(opinion_personaje)= opinion_personaje
	group by personaje.id) as pjs_odiados2)
group by usuario.id;

/*
	7. NPC(s), clase y raza a la que pertenece(n) que puedan usar la
	   habilidad 'croqueta' y spawnee(n) en coordenadas X positivas
	   y coordenadas Y negativas.
		* Mas de una tabla
		* Subconsulta
	   De los requisitos:
		* Datos estructurados (aparicion)
*/
select npc.nombre as NPC, raza.nombre as raza, clase.nombre as clase
from npc
join clase on clase.id = npc.id_clase
join raza on raza.id = npc.id_raza
where npc.id in (select npc.id from clase_habilidad
	join npc on clase_habilidad.id_clase = npc.id_clase
	join habilidad on habilidad.id = clase_habilidad.id_habilidad
	where habilidad.nombre ilike 'croqueta'
	group by npc.id
	having (aparicion).x > 0 and (aparicion).y < 0);

/*
	8. Cuales son las habilidades defensivas que absorban o reduzcan
	   agravios magicos y no sean una inmunidad y de que clase son
		* Subconsulta
	   De los requisitos:
		* Datos estructurados (tipo, reduccion y absorcion)
		* Caracteristicas de herencia
*/
select habilidad.nombre as habilidad, clase.nombre as clase
from clase_habilidad
join clase on clase.id = clase_habilidad.id_clase
join habilidad on habilidad.id = clase_habilidad.id_habilidad
where clase_habilidad.id_habilidad
	in (select id
	from habilidad_def
	where ((tipo).absorcion).magico > 0
	or ((tipo).reduccion).magico > 0)
order by clase.id;

/*
	9. De los personajes cuyo nivel de objeto medio sea superior a 60,
	   muestra las habilidades de sanacion que utilicen con magia floral
	   (segun su clase)
		* Subconsultas
	   De los requisitos:
		* Caracteristicas de herencia (habilidad_sanacion)
	=====================================================================================
	NOTA:
	  He de clarificar que en la poblacion de datos he administrado las habilidades y
	las magias segun la clase. Esto significa que la magia que usan los personajes
	depende de la que puede usar su clase y no de la habilidad que usen, de manera que
	las mismas habilidades podrian ser usadas con distintos tipos de magia. Si quisiera
	limitar esto, haria un tabla relacion magia_habilidad. Por ejemplo, 'Flor De Vida'
	puede lanzarse con magia ignea, terrena, floral, acuifera, ventarrona o rocosa, pero
	solamente puede ser lanzada por driadas.
	  Por supuesto en un caso real utilizar una magia u otra favoreceria o penalizaria
	el uso de la habilidad, pero no llego a tanto en este trabajo.
	=====================================================================================
*/
select habilidad.nombre
from clase_habilidad
join habilidad on habilidad.id = clase_habilidad.id_habilidad
join personaje on personaje.id_clase = clase_habilidad.id_clase
join clase_magia on clase_habilidad.id_clase = clase_magia.id_clase
join magia on magia.id = clase_magia.id_magia
where personaje.id in (
	select personaje.id
	from personaje_vestimenta
	join personaje on personaje.id = personaje_vestimenta.id_personaje
	join (
		select niveles_objeto.suma/num_objetos.cuenta as nivel_medio, niveles_objeto.idPJSuma as idPJ
		from (select sum(nivel_objeto) as suma, personaje_vestimenta.id_personaje as idPJSuma
			from personaje_vestimenta
			join vestimenta on vestimenta.id = personaje_vestimenta.id_vestimenta
			group by personaje_vestimenta.id_personaje) as niveles_objeto
		join (select count(*) as cuenta, personaje_vestimenta.id_personaje as idPJCuenta
			from personaje_vestimenta
			group by personaje_vestimenta.id_personaje) as num_objetos
		on niveles_objeto.idPJSuma = num_objetos.idPJCuenta) as objetos
	on personaje.id = objetos.idPJ
	where objetos.nivel_medio > 60
	group by personaje.id)
and habilidad.id in (select id from habilidad_sanacion)
and magia.tipo = 'terrena'
group by habilidad.id;


/*
	10. Quien es el personaje que ha escogido la clase y la raza mas jugadas,
	cual es su nickname de usuario, su primer email registrado, sus horas jugadas,
	y el nivel medio de objeto de su personaje
		* Mucha paciencia
		* Coleciones / arrays (email)
*/

select personaje.nombre as personaje, usuario.nickname as usuario, usuario.horas_jugadas,
usuario.email[1], nivel_objeto.nivel_medio as nivel_medio_objeto
from personaje
join usuario on usuario.id = personaje.id_usuario
join (
	select niveles_objeto.suma/num_objetos.cuenta as nivel_medio, personaje.id as idPJ
	from (
		select sum(nivel_objeto) as suma, personaje_vestimenta.id_personaje as idPJSuma
		from personaje_vestimenta
		join vestimenta on vestimenta.id = personaje_vestimenta.id_vestimenta
		group by personaje_vestimenta.id_personaje
	) as niveles_objeto
	join (
		select count(*) as cuenta, personaje_vestimenta.id_personaje as idPJCuenta
		from personaje_vestimenta
		group by personaje_vestimenta.id_personaje
	) as num_objetos
	on niveles_objeto.idPJSuma = num_objetos.idPJCuenta
	join personaje on personaje.id = num_objetos.idPJCuenta
	where personaje.id_clase in (
		select clase.id
		from (
			select count(*) as num_pjs, clase.id idClase
			from clase
			join personaje on personaje.id_clase = clase.id
			group by clase.id
		) as num_clases
		join clase on num_clases.idClase = clase.id
		where num_clases.num_pjs = (
			select max(num_clases.num_pjs) from
			(select count(*) as num_pjs, clase.id idClase
			from clase
			join personaje on personaje.id_clase = clase.id
			group by clase.id
			) as num_clases
		)
	)
	and personaje.id_raza in (
		select raza.id
		from (
			select count(*) as num_pjs_por_raza, raza.id as idRaza
			from personaje
			join raza on raza.id = personaje.id_raza
			group by raza.id
		) as cant_razas
		join raza on raza.id = cant_razas.idRaza
		where cant_razas.num_pjs_por_raza = (
			select max(cant_razas2.num_pjs_por_raza)
			from (
				select count(*) as num_pjs_por_raza
				from personaje
				join raza on raza.id = personaje.id_raza
				group by raza.id
			) as cant_razas2
		)
	)
) as nivel_objeto
on personaje.id = nivel_objeto.idPJ
where personaje.id_clase in (
	select clase.id
	from (
		select count(*) as num_pjs, clase.id idClase
		from clase
		join personaje on personaje.id_clase = clase.id
		group by clase.id
	) as num_clases
	join clase on num_clases.idClase = clase.id
	where num_clases.num_pjs = (
		select max(num_clases.num_pjs) from (
			select count(*) as num_pjs, clase.id idClase
			from clase
			join personaje on personaje.id_clase = clase.id
			group by clase.id
		) as num_clases
	)
)
and personaje.id_raza in (
	select raza.id
	from (
		select count(*) as num_pjs_por_raza, raza.id as idRaza
		from personaje
		join raza on raza.id = personaje.id_raza
		group by raza.id
	) as cant_razas
	join raza on raza.id = cant_razas.idRaza
	where cant_razas.num_pjs_por_raza = (
		select max(cant_razas2.num_pjs_por_raza)
		from (
			select count(*) as num_pjs_por_raza
			from personaje
			join raza on raza.id = personaje.id_raza
			group by raza.id
		) as cant_razas2
	)
);