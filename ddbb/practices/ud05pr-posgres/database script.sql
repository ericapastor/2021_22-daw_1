-- crear base de datos
CREATE TYPE NOMBRE_COMPLETO AS (
	nombre VARCHAR(20),
	apellido1 VARCHAR(20),
	apellido2 VARCHAR(20)
);
CREATE TYPE TIPO_STAT AS (
	magico INT,
	fisico INT
);
CREATE TYPE STATS AS (
	atk INT,
	def INT,
	vel_mov INT,
	estabilidad INT,
	cele INT,
	leech INT
);
CREATE TYPE TIPO_DEF AS (
	absorcion TIPO_STAT,
	reduccion TIPO_STAT,
	inmunidad BOOLEAN
);
CREATE TYPE COORDENADAS AS (
	x INT,
	y INT
);
CREATE TYPE MAT_ARM AS ENUM ('placas','malla', 'cuero', 'tela');
CREATE TYPE AMBIENTE_MONTURA AS ENUM ('acuatico', 'aereo', 'terrenal');
CREATE TYPE TIPO_MONTURA AS ENUM ('animal', 'mecanica', 'espiritual');
CREATE TYPE MATERIAL AS ENUM('placas','malla', 'cuero', 'tela');
CREATE TYPE CALIDAD_VEST AS ENUM('cutre', 'normal', 'legendaria', 'mitica');
CREATE TYPE TIPO_VEST AS ENUM('cabeza', 'hombros', 'pecho', 'piernas', 'pies');
CREATE TYPE MOTREL_PJNPC AS ENUM ('hurto', 'asesinato', 'casarse', 'vender', 'comprar');
CREATE TYPE MOTREL_PJPJ AS ENUM ('escribirse', 'comprar', 'vender', 'pelearse', 'ayudarse', 'contratarse');
CREATE TYPE OPINION_NPC AS ENUM ('en guerra', 'enemistad', 'neutral', 'amistad', 'alianza');

CREATE TABLE usuario (
	id SERIAL PRIMARY KEY,
	nombre NOMBRE_COMPLETO,
	fecha_nacimiento DATE NOT NULL,
	fecha_alta_inscripcion DATE NOT NULL,
	nickname VARCHAR(15) UNIQUE,
	passw VARCHAR(15) NOT NULL,
	tarjeta_bancaria VARCHAR(24),
	telefono VARCHAR(20)[],
	email TEXT[],
	horas_jugadas INT DEFAULT 0
);

CREATE TABLE raza (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR(20),
	estadisticas STATS,
	origen_historia VARCHAR(400)
);

CREATE TABLE magia(
	id SERIAL PRIMARY KEY,
	antiguedad INT,
	tipo TEXT
);

CREATE TABLE clase (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR(20),
	material_armadura MAT_ARM
);

CREATE TABLE npc (
	id SERIAL PRIMARY KEY,
	id_clase INT  NOT NULL,
	id_raza INT  NOT NULL,
	FOREIGN KEY (id_clase) REFERENCES clase (id),
	FOREIGN KEY (id_raza) REFERENCES raza (id),
	nombre NOMBRE_COMPLETO,
	aparicion COORDENADAS,
	xp INT
);

CREATE TABLE montura (
	id SERIAL PRIMARY KEY,
	id_npc INT,
	FOREIGN KEY (id_npc) REFERENCES npc (id),
	nombre VARCHAR (20) UNIQUE,
	ambiente AMBIENTE_MONTURA,
	forma TIPO_MONTURA,
	probabilidad_drop INT DEFAULT 1
);

CREATE TABLE mascota (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR(20),
	tipo TEXT
);

CREATE TABLE personaje (
	id SERIAL PRIMARY KEY,
	id_usuario INT NOT NULL,
	id_raza INT NOT NULL,
	id_clase INT NOT NULL,
	FOREIGN KEY (id_usuario) REFERENCES usuario(id),
	FOREIGN KEY (id_raza) REFERENCES raza (id),
	FOREIGN KEY (id_clase) REFERENCES clase (id),
	nombre VARCHAR (25) UNIQUE,
	proc TEXT DEFAULT 'Krrrprrgrrlkmpkah',
	nivel INT DEFAULT 1,
	num_NPC_asesinados INT  DEFAULT 0,
	estadisticas STATS,
	nivel_objeto INT -- nivel medio de objeto
);

CREATE TABLE vestimenta (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR(40),
	color VARCHAR(10),
	material MATERIAL,
	nivel_objeto INT,
	estadisticas STATS,
	irrompibilidad BOOLEAN,
	calidad CALIDAD_VEST DEFAULT 'normal',
	tipo TIPO_VEST
);

CREATE TABLE habilidad (
	id SERIAL PRIMARY KEY,
	nombre VARCHAR(40) UNIQUE,
	reutilizacion INT,
	nivel_obtencion INT NOT NULL,
	afecta_gcd BOOLEAN
);

CREATE TABLE habilidad_atk (
	tipo TIPO_STAT
) INHERITS (habilidad);

CREATE TABLE habilidad_def (
	tipo TIPO_DEF,
	duracion INT
) INHERITS (habilidad);

CREATE TABLE habilidad_sanacion (
	cantidad TIPO_STAT
) INHERITS (habilidad);

CREATE TABLE clase_habilidad (
	id_clase INT NOT NULL REFERENCES clase (id),
	id_habilidad INT NOT NULL,
	PRIMARY KEY (id_clase, id_habilidad)
);

CREATE TABLE clase_magia (
	id_clase INT NOT NULL REFERENCES clase (id),
	id_magia INT NOT NULL REFERENCES magia (id),
	PRIMARY KEY (id_clase, id_magia)
);

CREATE TABLE pj_tiene_vest (
	id_personaje INT NOT NULL REFERENCES personaje (id),
	id_vestimenta INT NOT NULL REFERENCES vestimenta (id),
	cantidad INT,
	PRIMARY KEY (id_personaje,id_vestimenta)
);

CREATE TABLE personaje_vestimenta (
	id_personaje INT NOT NULL REFERENCES personaje (id),
	id_vestimenta INT NOT NULL REFERENCES vestimenta (id),
	PRIMARY KEY (id_personaje, id_vestimenta)
);

CREATE TABLE personaje_montura (
	id_personaje INT NOT NULL REFERENCES personaje (id),
	id_montura INT NOT NULL REFERENCES montura (id),
	fecha_obtencion DATE DEFAULT CURRENT_DATE,
	PRIMARY KEY (id_personaje, id_montura)
);

CREATE TABLE personaje_mascota (
	id_personaje INT NOT NULL REFERENCES personaje (id),
	id_mascota INT NOT NULL REFERENCES mascota (id),
	fecha_adopcion DATE DEFAULT CURRENT_DATE,
	PRIMARY KEY (id_personaje, id_mascota)
);

CREATE TABLE npc_dropea_vest (
	id_npc INT NOT NULL REFERENCES npc (id),
	id_vestimenta INT NOT NULL REFERENCES vestimenta (id),
	PRIMARY KEY (id_npc, id_vestimenta)
);

CREATE TABLE pj_se_relaciona_con_npc (
	id_personaje INT NOT NULL REFERENCES personaje (id),
	id_npc INT NOT NULL REFERENCES npc (id),
	motivo MOTREL_PJNPC,
	PRIMARY KEY (id_personaje, id_npc, motivo)
);

CREATE TABLE pj_se_relaciona_con_pj (
	id_personaje_activo INT  NOT NULL REFERENCES personaje (id),
	id_personaje_pasivo INT  NOT NULL REFERENCES personaje (id),
	motivo MOTREL_PJPJ,
	PRIMARY KEY (id_personaje_activo, id_personaje_pasivo, motivo)
);

CREATE TABLE npc_opina_de_pj(
	id_personaje INT NOT NULL REFERENCES personaje (id),
	id_npc INT NOT NULL REFERENCES npc(id),
	opinion_personaje OPINION_NPC DEFAULT 'neutral',
	PRIMARY KEY (id_personaje,id_npc)
);

/* Raza */
INSERT INTO raza (id, nombre, estadisticas, origen_historia)
VALUES
	(1, 'tortoliano',(5, 10, 1, 1, 2, 0),'Los tortolianos son una raza antigua, conocidos por sus largas historias y su lentitud, asi como por su resistente caparazon. Su origen es tan antiguo que ya ningun archivo lo recuerda, pero su alta velocidad de nado en el mar puede darnos alguna que otra pista...'),
	(2, 'dragonoide',(8, 6, 7, 8, 5, 5),'Los dragonoides son originarios del gran continente de fuego, fuertes y resilientes con grandes habilidades de combate cuerpo a cuerpo en tierra y en aire.'),
	(3, 'silfide',(10, 3, 8, 6, 10, 1),'Los bosques habitados por silfides siempre son los mas hermosos, dicen las leyendas. Provenientes de una perdida region de bosques conocido como Ashuklanlah, las silfides pueden ser estupendas curanderas, o peligrosas hechiceras, en busca de su tierra perdida.'),
	(4, 'gigante de llanura',(6, 8, 3, 10, 0, 3),'Peligrosos cuando tienen hambre y difilmente saciables, los gigantes de llanura poblan toda la tierra conocida. Se dice que nacieron de la tierra y por eso le tienen panico a las alturas.'),
	(5, 'centauro',(5, 5, 5, 5, 5, 5),'Una raza orgullosa, los centauros, y extremadamente rapidos. Estas criaturas nacidas de un peligroso experimento supuestamente terminado en la ciudad de Jryushkp son escasas, deseadas, cazadas, y vendidas como esclavos. Los centauros luchan por conseguir su libertad.');

/* Clase */
INSERT INTO clase (id, nombre, material_armadura)
VALUES
	(1, 'gladiadora','placas'),
	(2, 'arquera','malla'),
	(3, 'ninja', 'tela'),
	(4, 'driada', 'cuero'),
	(5, 'hechicera', 'tela');

/* Usuario */
INSERT INTO usuario (id, nombre, fecha_nacimiento, fecha_alta_inscripcion,
nickname, passw, tarjeta_bancaria, telefono, email, horas_jugadas)
VALUES
	(1, '("Juanito", "Perez", "Limones")', '1994-09-30', current_date, 'j94', 'passwd1', 'ES12 3456 7890 1234 5678', '{"+12 689402841", "+12 452897341"}', '{"juanesPL94@gmail.com"}', 100),
	(2, '("Maria", "La Portuguesa", "")', '2000-02-29', current_date, 'meriperi', 'passwd2', 'ES09 8765 4321 0987 6543', '{""}', '{"meriperi@hotmail.com", "marialaportuguesaoficial@mail.com"}', 67),
	(3, '("Francis", "Santo", "Domingo")', '2003-05-01', current_date, 'frasado2003', 'passwd3', 'BIC123 890 453 89012', '{"+34 123456780", "+43 897856123"}', '{"francis2003@gmail.com", "micorreo@gmail.com", "correodemispadres@gmail.com"}', 123),
	(4, '("Josuela", "Apellidofalso1", "Apellidofalso2")', '1978-01-16', current_date, 'miLadyUwu', 'passwd4', 'BIC666 666 666 69696', '{"+63 984512347", "+41 295620192"}', '{"nosoyunsenior@hotmail.com", "ladyuwu@mail.com"}', 1),
	(5, '("La Yoli", "", "")', '1998-10-03', current_date, 'layoliteb', 'passwd5', 'ES21 3243 5465 7687 9809', '{"+56 123456789"}', '{"yolanda98@gmail.com", "silayolitebasila@mail.com"}', 90),
	(6, '("Jorge", "Garcia", "Altus")', '2005-12-07', current_date, 'oneshot_guy', 'passwd6', 'BIC456 980 123 76859', '{"+33 666666666"}', '{"urwelcomeforcarry@gmail.com"}', 876),
	(7, '("Bea", "Horacia", "Gracia")', '1996-07-13', current_date, 'druidic-focus', 'passwd7', 'ES10 2938 4756 6574 8392', '{"+376 123456789", "+34 678452312"}', '{"begutgar@email.com", "beita98@mail.com"}', 539),
	(8, '("Timur", "Martinez", "")', '2002-04-26', current_date, 'tymy18', 'passwd8', 'ES56 4756 3847 2938 1029', '{"+43 781234560"}', '{"timurcasual@yahoomail.com"}', 76),
	(9, '("Gonzala", "Gonzalez", "")', '1999-03-20', current_date, 'ggwp', 'passwd9', 'ES63 7486 9708 3142 5364', '{"+44 908978675"}', '{"ggwp@gmail.com", "easyez@mail.com"}', 42),
	(10, '("Rosa Lucia", "Gamez", "Carrasco")', '1990-08-06', current_date, 'rosita22', 'passwd10', 'ES90 8978 6756 4534 2312', '{"+34 123456789"}', '{"rosales@mail.com", "floreslimoneras@gmail.com"}', 36);

/* Magia */
INSERT INTO magia (id, antiguedad, tipo)
VALUES
	(1, 50723215, 'arcana'),
	(2, 712656, 'ignea'),
	(3, 819672, 'terrena'),
	(4, 7127, 'helada'),
	(5, 493, 'floral'),
	(6, 629, 'acuifera'),
	(7, 3819691, 'ventarrona'),
	(8, 819667, 'electrica'),
	(9, 2427, 'fantasmal'),
	(10, 2961948, 'rocosa');
	
/* NPC */
INSERT INTO npc (id_clase, id_raza, nombre, aparicion, xp)
VALUES
  (5, 3, '("Josephine", "Dale", "Bradford")', '(78, 89)', 408),
  (3, 3, '("Teegan", "Griffin", "")', '(-13, 0)' ,86),
  (2, 4, '("Carla", "Crane", "Torres")', '(99, -89)', 301),
  (3, 3, '("Berk", "Mcknight", "Naida")', '(-43, -74)', 330),
  (1, 3, '("Garth", "Abbott", "Boone")', '(-22, 85)', 260),
  (2, 5, '("Gray", "Ortega", "")', '(-12, -64)', 51),
  (5, 4, '("Marvin", "Daphne", "Durham")', '(-67, 64)', 177),
  (4, 5, '("Chandler", "Eaton", "Howard")', '(84, 56)', 211),
  (2, 3, '("Emerald", "Stacy", "Vasquez")', '(16, 56)', 310),
  (3, 1, '("Rudyard", "Macdonald", "")', '(-37, -39)', 249),
  (1, 5, '("Jena", "Santana", "")', '(-46, -85)', 483),
  (2, 5, '("Cain", "Marks", "Booth")', '(86, -27)', 58),
  (1, 2, '("Elijah", "Nelson", "Levi")', '(19, -31)', 74),
  (4, 5, '("Jonah", "Wolf", "")', '(0, -78)', 29),
  (4, 4, '("Ivor", "Stuart", "Jana")', '(-99, -66)', 40),
  (5, 5, '("Wesley", "Villarreal", "Barton")', '(-42, 60)', 380),
  (3, 5, '("Ulysses", "Lynn", "Tatiana")', '(59, 7)', 206),
  (4, 5, '("Quamar", "Woodard", "Buckley")', '(-25, 9)', 383),
  (5, 3, '("Hyatt", "Key", "Dominic")', '(-11, -88)', 317),
  (2, 4, '("Bo", "Figueroa", "")', '(-80, -37)', 325);

/* Montura */
INSERT INTO montura (id_npc, nombre, ambiente, forma, probabilidad_drop)
VALUES
  (19, 'Abramita', 'terrenal', 'animal', 5),
  (2, 'Phoebina', 'acuatico', 'espiritual', 2),
  (17, 'Ignatiusmi', 'aereo', 'animal', 1),
  (13, 'Wallyomyng', 'aereo', 'mecanica', 7),
  (12, 'Camelon', 'terrenal', 'animal', 8),
  (14, 'Gloria', 'acuatico', 'mecanica', 6),
  (16, 'Harrison', 'aereo', 'espiritual', 9),
  (17, 'Felix', 'terrenal', 'mecanica', 9),
  (8, 'Taddy', 'acuatico', 'espiritual', 3),
  (20, 'Amethysta', 'acuatico', 'espiritual', 2);

/* Mascota */
INSERT INTO mascota (nombre, tipo)
VALUES
  ('Commodo Hendrerit', 'metal'),
  ('Non Sollicitudin', 'hada'),
  ('Eu Phasellus', 'agua'),
  ('Aliquet Proin', 'fuego'),
  ('Donec Fringilla', 'tierra'),
  ('NullaDignissim', 'hielo'),
  ('In Lorem.', 'fantasma'),
  ('Diam At', 'maquina'),
  ('Ut Velit', 'arcano'),
  ('Volutpat', 'aire');

/* Personaje */
INSERT INTO personaje (id_usuario, id_raza, id_clase, nombre,
	proc, nivel, num_NPC_asesinados, estadisticas, nivel_objeto)
VALUES
  (1, 4, 5, 'nerieri', 'Elhaun`sharah', 69, 74, '(10, 30, 20, 35, 12, 8)', 36),
  (5, 2, 5, 'colacao', 'Mo`onshar', 69, 80, '(7, 2, 12, 78, 4, 50)', 33),
  (9, 3, 5, 'zhyan', 'Mo`onshar', 74, 69, '(32, 27, 12, 9, 16, 40)', 11),
  (3, 5, 2, 'danytt', 'Jryushkp', 43, 91, '(67, 32, 1, 50, 2, 3)', 13),
  (9, 4, 5, 'anjan', 'Krrrprrgrrlkmpkah', 108, 31, '(20, 20, 20, 20, 20, 20)', 44),
  (6, 2, 2, 'eclipse', 'Tuyil', 116, 65, '(14, 20, 26, 30, 10, 20)', 43),
  (7, 3, 3, 'sabruxi', 'Mo`onshar', 119, 96, '(12, 28, 20, 2, 38, 20)', 42),
  (3, 5, 1, 'sora', 'Jiryosh`myan', 101, 60, '(7, 33, 17, 23, 0, 40)', 33),
  (2, 5, 3, 'edgarcito', 'Wqrsftmnvcxz', 25, 4, '(18, 18, 10, 10, 20, 44)', 36),
  (3, 5, 3, 'irusa', 'Mo`onshar', 25, 88, '(80, 5, 5, 10, 10, 10)', 17),
  (8, 4, 2, 'lordrop', 'Elhaun`sharah', 93, 2, '(40, 10, 0, 50, 10, 10)', 49),
  (3, 5, 1, 'jamondepony', 'Jryushkp', 117, 19, '(32, 28, 30, 12, 8, 10)', 36),
  (9, 1, 3, 'luterowally', 'Wqrsftmnvcxz', 43, 73, '(15, 15, 15, 23, 27, 26)', 30),
  (10, 1, 5, 'r3jy0', 'Pyrokas', 98, 74, '(17, 23, 7, 33, 0, 40)', 6),
  (9, 5, 4, 'sr.Fedora', 'Jiryosh`myan', 56, 67, '(17, 0, 7, 33, 23, 40)', 28),
  (4, 5, 5, 'scolopendra', 'Ontaru', 117, 90, '(26, 14, 20, 10, 20, 30)', 30),
  (5, 4, 2, 'mindthieff', 'Lykos', 89, 93, '(12, 14, 49, 7, 18, 20)', 19),
  (7, 4, 1, 'keinar', 'Jiryosh`myan', 98, 96, '(59, 11, 9, 8, 13, 20)', 35),
  (5, 3, 4, 'kareht', 'Grupkyt', 81, 40, '(10, 60, 15, 15, 10, 10)', 2),
  (3, 2, 2, 'blacky', 'Tuyil', 90, 38, '(15, 25, 15, 25, 15, 25)', 35);

/* Vestimenta */
INSERT INTO vestimenta (nombre, color, material, nivel_objeto, estadisticas, irrompibilidad, calidad, tipo)
VALUES
  ('Lorem Auctor Quis Foundation','#e55957','placas',25,'(15, 25, 15, 25, 15, 25)','no','cutre','cabeza'),
  ('Sed Est LLP','#d67011','cuero',22,'(10, 60, 15, 15, 10, 10)','yes','mitica','pecho'),
  ('Ullamcorper Velit In Inc.','#c7dd77','placas',53,'(59, 11, 9, 8, 13, 20)','yes','cutre','piernas'),
  ('At Associates','#45aa27','cuero',14,'(17, 23, 7, 33, 0, 40)','no','cutre','piernas'),
  ('Euismod Et LLP','#007753','tela',26,'(80, 5, 5, 10, 10, 10)','yes','normal','cabeza'),
  ('Ac Turpis Company','#29d129','tela',26,'(18, 18, 10, 10, 20, 44)','yes','mitica','pies'),
  ('Sed Eget Ltd','#1f2582','malla',83,'(26, 14, 20, 10, 20, 30)','yes','mitica','piernas'),
  ('Sollicitudin Orci Institute','#20c966','cuero',5,'(32, 28, 30, 12, 8, 10)','no','legendaria','pecho'),
  ('Nec Incorporated','#6492e0','cuero',120,'(10, 30, 20, 35, 12, 8)','no','normal','pies'),
  ('Eget Volutpat Ltd','#a0f7c1','tela',105,'(15, 15, 15, 23, 27, 26)','yes','cutre','hombros'),
  ('Aliquam Fringilla Institute','#5305c6','malla',27,'(32, 28, 30, 12, 8, 10)','yes','normal','cabeza'),
  ('At Inc.','#fc19c3','placas',41,'(67, 32, 1, 50, 2, 3)','no','normal','pies'),
  ('Odio Semper PC','#b2dcff','tela',55,'(15, 25, 15, 25, 15, 25)','yes','normal','pecho'),
  ('Fringilla Ornare Placerat Associates','#5ad14d','tela',102,'(40, 10, 0, 50, 10, 10)','yes','legendaria','hombros'),
  ('Imperdiet Non Vestibulum Ltd','#adffb5','placas',79,'(32, 27, 12, 9, 16, 40)','no','legendaria','piernas'),
  ('Ornare Sagittis Corp.','#2094e8','placas',51,'(67, 32, 1, 50, 2, 3)','no','normal','hombros'),
  ('Nostra Per Corporation','#81f4d2','cuero',44,'(40, 10, 0, 50, 10, 10)','yes','mitica','hombros'),
  ('Aliquam Adipiscing Ltd','#7d78e2','placas',76,'(32, 27, 12, 9, 16, 40)','yes','mitica','pies'),
  ('Egestas Ligula Nullam LLC','#d591e0','cuero',51,'(15, 25, 15, 25, 15, 25)','yes','mitica','cabeza'),
  ('Vel Mauris Limited','#20d800','cuero',77,'(40, 10, 0, 50, 10, 10)','yes','cutre','pecho');

/* Habilidades */
INSERT INTO habilidad_atk (nombre, reutilizacion, nivel_obtencion, afecta_gcd, tipo)
VALUES
  ('Cadena De Rayos',3,71,'no','(5,5)'),
  ('Ola Atronadora',8,83,'yes','(7,3)'),
  ('Marca Ignea',5,61,'yes','(1,19)'),
  ('Totem Electrico',4,35,'yes','(17,3)'),
  ('Patada Oscura',8,13,'yes','(10,10)'),
  ('Bola De Fuego',3,7,'yes','(0,10)'),
  ('Rayo',4,39,'yes','(8,12)'),
  ('Palabra De Las Sombras',8,5,'yes','(0,20)'),
  ('Terremoto',6,44,'no','(20,0)'),
  ('Patada Giratoria De La Grulla',1,38,'no','(18,2)');
INSERT INTO habilidad_def (nombre, reutilizacion, nivel_obtencion, afecta_gcd, duracion, tipo)
VALUES
  ('Croqueta',4,80,'no',5,'("(0,0)","(0,0)","yes")'),
  ('Brebaje reconstituyente',5,21,'yes',4,'("(0,0)","(7,13)","no")'),
  ('Cambio Astral',1,68,'yes',2,'("(0,0)","(0,20)","no")'),
  ('Bloqueo Con Escudo',10,44,'no',9,'("(15,0)","(3,2)","no")'),
  ('Ignorar Dolor',6,61,'yes',2,'("(0,0)","(0,0)","yes")'), --
  ('Pelaje Ferreo',5,88,'no',3,'("(0,0)","(20,0)","no")'), --
  ('Piel De Corteza',7,42,'yes',10,'("(0,0)","(12,8)","no")'), --
  ('Resolucion Inagotable',4,64,'no',4,'("(0,0)","(20,20)","no")'), --
  ('Aspecto De La Tortuga',8,59,'no',4,'("(0,0)","(0,0)","yes")'), --
  ('Metamorfosis',4,80,'yes',8,'("(10,10)","(10,10)","no")'); --
INSERT INTO habilidad_sanacion (nombre, reutilizacion, nivel_obtencion, afecta_gcd, cantidad)
VALUES
  ('Vivificar',2,66,'yes','(12,8)'),
  ('Palabra Curativa',6,6,'no','(4,6)'),
  ('Marea Sanadora',5,27,'yes','(7,13)'),
  ('Eflorescencia',3,25,'no','(1,9)'),
  ('Destello De Luz',5,11,'no','(8,2)'),
  ('Rejuvenecimiento',7,24,'yes','(10,0)'),
  ('Flor De Vida',8,91,'yes','(5,5)'),
  ('Crecimiento Salvaje',9,18,'no','(16,0)'),
  ('Alivio Presto',4,92,'yes','(10,10)'),
  ('Sanacion Relampago',3,6,'yes','(0,12)');
INSERT INTO habilidad (nombre, reutilizacion, nivel_obtencion, afecta_gcd)
VALUES
  ('Utilizar montura',0,60,'yes'),
  ('Decir piropo',2,120,'no'),
  ('Hacerse el muerto',60,30,'yes'),
  ('Crear vestimenta temporal',300,10,'yes'),
  ('Camuflaje total',600,60,'no');

/* Personaje tiene habilidad */

INSERT INTO clase_habilidad (id_clase, id_habilidad)
VALUES
  (1,31), (1,32), (1,34), (1,9), (1,11), (1,14), (1,15), (1,18), (1,30),
  (2,31), (2,32), (2,34), (2,33), (2,1), (2,4), (2,7), (2,19), (2,29),
  (3,31), (3,32), (3,34), (3,35), (3,5), (3,10), (3,12), (3,21), (3,23),
  (4,31), (4,32), (4,34), (4,2), (4,16), (4,17), (4,24), (4,26), (4,27), (4,28),
  (5,31), (5,32), (5,34), (5,3), (5,6), (5,8), (5,13), (5,20), (5,22), (5,25);
	
/* Clase usa magia */
INSERT INTO clase_magia (id_clase, id_magia)
VALUES
  (1,3), (1,10), (2,1), (2,4), (2,8),
  (3,6), (3,9), (4,2), (4,3), (4,5),
  (4,6), (4,7), (4,10), (5,1), (5,2),
  (5,4), (5,7), (5,8), (5,9);

/* Personaje tiene vestimenta (en inventario) */
INSERT INTO pj_tiene_vest (id_personaje, id_vestimenta, cantidad)
VALUES
  (3,6,9), (9,8,2), (11,5,8), (10,19,4), (8,18,9),
  (17,12,5), (19,14,3), (16,14,9), (2,11,7), (10,9,7),
  (13,17,3), (15,12,7), (3,18,4), (15,7,8), (13,7,8),
  (4,6,9), (17,10,2), (3,5,4), (17,13,7), (15,17,6),
  (7,18,3), (15,9,6), (4,16,9), (12,16,10), (10,10,9),
  (5,10,9), (19,19,6), (3,4,10), (2,17,1), (1,18,3),
  (7,7,8), (6,4,9), (5,3,3), (1,16,1), (2,2,2),
  (9,10,9), (12,15,6), (17,6,4), (15,5,3), (3,16,2),
  (4,8,9), (5,19,2), (13,18,9), (9,20,3), (15,6,8),
  (12,18,6), (10,13,4), (8,2,2), (11,6,3), (14,3,3),
  (11,4,3), (11,14,2), (3,7,8), (10,16,9), (12,4,3),
  (7,9,2), (18,2,7), (6,14,10), (12,20,6), (18,14,5),
  (9,11,7), (15,10,8), (8,15,4), (14,16,4), (15,20,6),
  (20,17,8), (11,15,7), (8,17,2), (10,12,4), (4,7,9),
  (5,6,5), (2,16,9), (13,19,7), (2,4,5), (19,17,3),
  (13,10,9), (2,3,3), (16,6,1), (5,7,8), (8,9,4),
  (11,13,8), (20,11,6), (14,6,4), (3,12,5), (12,6,7),
  (11,17,9), (17,17,4), (2,19,8), (12,11,2), (10,20,3),
  (8,13,4), (15,1,1), (11,8,7), (7,15,2), (16,12,1),
  (13,4,9), (12,19,3), (17,2,4), (4,13,8), (2,8,8);

/* Personaje se equipa vestimenta */
INSERT INTO personaje_vestimenta (id_personaje, id_vestimenta)
VALUES
  (1,5), (1,16), (1,2), (1,15), (1,6),
  (2,1), (2,16), (2,8), (2,3), (2,18),
  (3,11), (3,17), (3,20), (3,3), (3,12),
  (4,5), (4,10), (4,8), (4,4), (4,9),
  (5,11), (5,16), (5,2), (5,7), (5,6),
  (6,1), (6,17), (6,2), (6,15), (6,6),
  (7,1), (7,14), (7,13), (7,7), (7,18),
  (8,5), (8,10), (8,20), (8,4), (8,9),
  (9,19), (9,17), (9,13), (9,15), (9,12),
  (10,19), (10,14), (10,8), (10,3), (10,18),
  (11,5), (11,17), (11,2), (11,15), (11,6),
  (12,1), (12,16), (12,8), (12,15), (12,18),
  (13,11), (13,17), (13,13), (13,3), (13,12),
  (14,5), (14,16), (14,8), (14,4), (14,9),
  (15,11), (15,16), (15,2), (15,7), (15,18),
  (16,5), (16,17), (16,2), (16,15), (16,6),
  (17,1), (17,10), (17,13), (17,7), (17,18),
  (18,5), (18,10), (18,20), (18,3), (18,9),
  (19,19), (19,17), (19,13), (19,15), (19,9),
  (20,11), (20,14), (20,8), (20,3), (20,18);

/* Personaje posee montura */
INSERT INTO personaje_montura (id_personaje, id_montura, fecha_obtencion)
VALUES
  (10,2,'2020-05-18'),
  (11,2,'2019-07-03'),
  (5,6,'2020-02-06'),
  (14,7,'2021-10-07'),
  (14,6,'2021-09-27'),
  (10,1,'2020-03-23'),
  (6,5,'2020-10-09'),
  (19,3,'2019-06-04'),
  (15,8,'2020-12-22'),
  (14,10,'2020-05-26'),
  (6,4,'2020-04-11'),
  (14,3,'2020-05-22'),
  (11,1,'2021-05-27'),
  (14,2,'2020-11-15'),
  (13,6,'2020-04-18'),
  (15,5,'2022-04-08'),
  (18,8,'2020-12-04'),
  (12,5,'2021-05-25'),
  (6,9,'2019-06-25'),
  (12,3,'2021-06-27'),
  (7,2,'2021-09-10'),
  (8,2,'2019-10-03'),
  (20,6,'2020-02-20'),
  (19,8,'2019-09-19'),
  (2,3,'2019-08-26'),
  (19,6,'2020-04-14'),
  (3,8,'2022-02-11'),
  (11,9,'2020-09-03'),
  (12,6,'2022-04-28'),
  (19,9,'2020-08-16'),
  (10,10,'2019-08-31'),
  (5,3,'2021-07-06'),
  (16,9,'2019-10-19'),
  (2,7,'2021-02-11'),
  (9,9,'2021-10-23'),
  (7,3,'2020-05-10'),
  (17,5,'2021-03-11'),
  (18,5,'2021-12-18'),
  (10,6,'2019-11-06'),
  (4,9,'2020-04-22');

/* Personaje adopta mascota */
INSERT INTO personaje_mascota (id_personaje,id_mascota,fecha_adopcion)
VALUES
  (10,6,'2022-02-20'),
  (19,8,'2020-08-02'),
  (13,7,'2020-07-03'),
  (1,6,'2019-10-30'),
  (7,5,'2019-06-05'),
  (12,2,'2021-09-18'),
  (18,4,'2019-06-17'),
  (5,3,'2021-11-24'),
  (3,7,'2022-04-04'),
  (2,3,'2022-05-06'),
  (4,4,'2021-02-14'),
  (5,9,'2021-05-03'),
  (19,10,'2019-05-31'),
  (2,4,'2019-05-30'),
  (18,3,'2021-09-20'),
  (12,4,'2020-05-13'),
  (16,5,'2020-02-03'),
  (13,1,'2022-04-03'),
  (6,3,'2020-07-12'),
  (18,2,'2022-03-30');


/* NPC dropea vestimenta */
INSERT INTO npc_dropea_vest (id_npc, id_vestimenta)
VALUES
  (15,1), (20,2), (8,3), (13,4), (6,5),
  (5,6), (12,7), (3,8), (8,9), (16,10),
  (1,11), (4,12), (8,13), (18,14), (12,15),
  (4,16), (20,17), (14,18), (5,19), (4,20);

/* Personaje se relaciona con NPC */
INSERT INTO pj_se_relaciona_con_npc (id_personaje, id_npc, motivo)
VALUES
  (4,20,'asesinato'), (19,2,'casarse'), (16,10,'casarse'), (16,2,'casarse'),
  (13,18,'comprar'), (4,4,'vender'), (2,18,'comprar'), (3,19,'casarse'),
  (5,11,'casarse'), (8,11,'asesinato'), (18,20,'hurto'), (14,13,'hurto'),
  (20,16,'casarse'), (4,5,'asesinato'), (17,12,'casarse'), (17,2,'comprar'),
  (10,8,'vender'), (9,7,'casarse'), (7,3,'hurto'), (2,3,'asesinato');

/* Personaje se relaciona con personaje */
INSERT INTO pj_se_relaciona_con_pj (id_personaje_activo, id_personaje_pasivo, motivo)
VALUES
  (17,2,'ayudarse'), (17,4,'vender'), (18,11,'pelearse'), (2,2,'ayudarse'),
  (4,10,'vender'), (9,11,'ayudarse'), (7,16,'ayudarse'), (17,18,'ayudarse'),
  (9,6,'vender'), (14,19,'pelearse'), (11,15,'ayudarse'), (13,15,'escribirse'),
  (15,4,'pelearse'), (6,7,'pelearse'), (3,9,'escribirse'), (4,18,'comprar'),
  (13,16,'vender'), (10,6,'pelearse'), (15,16,'vender'), (12,19,'escribirse');

/* NPC opina de personaje */
INSERT INTO npc_opina_de_pj (id_personaje, id_npc, opinion_personaje)
VALUES
  (9,9,'enemistad'), (8,11,'amistad'), (10,12,'neutral'), (16,16,'en guerra'),
  (5,18,'enemistad'), (2,7,'en guerra'), (12,2,'en guerra'), (19,13,'neutral'),
  (4,2,'enemistad'), (15,16,'en guerra'), (9,14,'neutral'), (9,18,'alianza'),
  (12,11,'enemistad'), (2,10,'amistad'), (16,5,'alianza'), (13,12,'en guerra'),
  (3,19,'neutral'), (9,3,'alianza'), (8,4,'alianza'), (17,2,'alianza'),
  (9,1,'enemistad'), (17,18,'alianza'), (17,7,'alianza'), (7,11,'enemistad'),
  (11,2,'en guerra'), (4,16,'neutral'), (12,6,'alianza'), (9,11,'neutral'),
  (11,11,'alianza'), (13,4,'en guerra'), (10,3,'alianza'), (6,11,'amistad'),
  (2,1,'alianza'), (3,6,'amistad'), (10,15,'enemistad'), (16,9,'enemistad'),
  (15,1,'alianza'), (6,18,'enemistad'), (3,4,'enemistad'), (18,14,'alianza');