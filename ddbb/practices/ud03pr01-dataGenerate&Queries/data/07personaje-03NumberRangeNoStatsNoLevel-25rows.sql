INSERT INTO personaje (id_usuario,id_raza,id_clase,nombre,tatuajes,procedencia,num_NPC_asesinados,punta_nariz,curva_nariz,puente_nariz,tipo_barbilla,altura_pomulos,forma_mandibula,anchura_mandibula)
VALUES
  (12,3,5,"Branden Phelps","0","Pyrokas",129,"afilada","concava","alto","afilada","media","afilada","estrecha"),
  (18,1,4,"Brynn, Molina","1","Jiryosh`myan",410,"redonda","concava","bajo","redonda","alta","cuadrada","estrecha"),
  (22,1,1,"Otto","0","Lykos",286,"redonda","concava","bajo","afilada","media","cuadrada","ancha"),
  (21,4,3,"Quentin","0","Lykos",306,"afilada","concava","alto","cuadrada","media","cuadrada","ancha"),
  (14,3,5,"Anastasia, Jemima, Callie","0","Krrrprrgrrlkmpkah",83,"afilada","concava","medio","afilada","media","redonda","ancha"),
  (21,2,4,"Doyle Kennedy","1","Lykos",366,"redonda","concava","bajo","afilada","alta","redonda","mediana"),
  (14,2,3,"Mccormick Ferdinand","0","Mo`onshar",412,"afilada","convexa","bajo","redonda","alta","cuadrada","estrecha"),
  (17,4,2,"Sybil, Mays","0","Elhaun`sharah",67,"redonda","plana","bajo","afilada","media","afilada","ancha"),
  (21,5,4,"Susan Crane","1","Wqrsftmnvcxz",88,"afilada","convexa","alto","redonda","media","afilada","mediana"),
  (24,4,2,"Craig, Vance, Heidi","0","Wqrsftmnvcxz",497,"afilada","convexa","bajo","afilada","alta","afilada","mediana");
INSERT INTO personaje (id_usuario,id_raza,id_clase,nombre,tatuajes,procedencia,num_NPC_asesinados,punta_nariz,curva_nariz,puente_nariz,tipo_barbilla,altura_pomulos,forma_mandibula,anchura_mandibula)
VALUES
  (17,3,4,"Jarrod","0","Grupkyt",467,"afilada","convexa","medio","afilada","media","redonda","ancha"),
  (9,3,2,"Workman Amanda","1","Mo`onshar",452,"afilada","plana","medio","afilada","media","cuadrada","mediana"),
  (8,1,4,"Judah","1","Wqrsftmnvcxz",33,"redonda","concava","bajo","redonda","baja","redonda","mediana"),
  (1,3,3,"Amos, Desiree, Colt","0","Ontaru",373,"afilada","concava","medio","cuadrada","baja","afilada","mediana"),
  (4,3,5,"Cecilia, Linda, Tad","1","Jiryosh`myan",16,"afilada","plana","alto","afilada","media","redonda","mediana"),
  (16,4,4,"Alexis, Richard","0","Jiryosh`myan",12,"redonda","convexa","alto","afilada","media","redonda","estrecha"),
  (3,3,5,"Jasmine, Hoffman","1","Tuyil",66,"redonda","plana","medio","afilada","media","afilada","mediana"),
  (6,1,3,"Keelie, Lowery","0","Elhaun`sharah",329,"afilada","plana","bajo","cuadrada","media","redonda","mediana"),
  (7,2,4,"William Swanson","0","Lykos",213,"afilada","concava","bajo","cuadrada","baja","afilada","mediana"),
  (16,5,2,"Amber, Seth, Orson","0","Jryushkp",286,"afilada","convexa","medio","afilada","alta","redonda","ancha");
INSERT INTO personaje (id_usuario,id_raza,id_clase,nombre,tatuajes,procedencia,num_NPC_asesinados,punta_nariz,curva_nariz,puente_nariz,tipo_barbilla,altura_pomulos,forma_mandibula,anchura_mandibula)
VALUES
  (19,2,5,"Noelani, Bevis, Brooke","0","Tuyil",78,"redonda","plana","bajo","cuadrada","baja","afilada","estrecha"),
  (19,4,2,"Massey Buffy","1","Ontaru",140,"afilada","concava","bajo","afilada","media","afilada","estrecha"),
  (13,4,2,"Vincent, Lynn, Zeph","1","Tuyil",456,"redonda","concava","alto","cuadrada","baja","afilada","estrecha"),
  (19,2,4,"Tallulah","1","Grupkyt",462,"afilada","convexa","medio","cuadrada","baja","cuadrada","estrecha"),
  (6,3,1,"Keefe","1","Krrrprrgrrlkmpkah",396,"redonda","convexa","alto","cuadrada","alta","cuadrada","estrecha");
