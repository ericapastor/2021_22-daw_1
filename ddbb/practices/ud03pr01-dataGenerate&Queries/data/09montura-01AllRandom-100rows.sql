INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (93,"Mehta Micah","terrenal","espiritual",5),
  (45,"Jonkers Melanie","acuatica","espiritual",1),
  (57,"Schwarz Odette","acuatica","espiritual",5),
  (57,"Keefe Elijah","terrenal","animal",4),
  (52,"Soares Patience","terrenal","mecanica",8),
  (40,"Farrah Harrison","terrenal","animal",5),
  (12,"Odette Colt","terrenal","mecanica",9),
  (54,"Duran Jenette","aerea","mecanica",9),
  (71,"Tremblay Armand","acuatica","espiritual",4),
  (50,"Gage Lois","acuatica","espiritual",4);
INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (98,"Laouali Arthur","terrenal","espiritual",7),
  (86,"Vincent Rylee","aerea","animal",1),
  (50,"Gill Brody","acuatica","espiritual",3),
  (33,"Jones Coby","terrenal","espiritual",8),
  (80,"Moses Kane","aerea","mecanica",7),
  (82,"Justin Nathaniel","aerea","espiritual",3),
  (11,"Grant Cara","terrenal","animal",4),
  (79,"Hedley Isaiah","terrenal","animal",4),
  (87,"Parker Reagan","aerea","animal",5),
  (48,"Nguyễn Bethany","aerea","mecanica",4);
INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (61,"Sanz Tate","terrenal","espiritual",10),
  (46,"Dündar Meredith","acuatica","animal",10),
  (83,"Carrasco Caesar","acuatica","animal",6),
  (62,"Price Keegan","acuatica","espiritual",7),
  (55,"Robin Wynne","terrenal","animal",2),
  (62,"Steel Keith","aerea","animal",6),
  (95,"Candido Adrian","acuatica","mecanica",3),
  (63,"Mona Janna","aerea","espiritual",5),
  (81,"Groen Reuben","aerea","mecanica",7),
  (14,"Simon Baxter","terrenal","animal",0);
INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (92,"van Leeuwen Linus","terrenal","animal",4),
  (91,"Thompson Anjolie","acuatica","animal",8),
  (34,"Ryan Alea","aerea","mecanica",2),
  (3,"Wolff Vivian","aerea","animal",6),
  (84,"Murphy Quail","terrenal","animal",7),
  (26,"Jin Daquan","aerea","mecanica",2),
  (7,"Carter Vera","aerea","mecanica",6),
  (55,"Ivan Keegan","terrenal","espiritual",4),
  (14,"Perez Reuben","terrenal","animal",9),
  (36,"Berk Gray","acuatica","animal",3);
INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (65,"Raymond Octavius","terrenal","animal",2),
  (10,"Vivian Raymond","acuatica","animal",8),
  (24,"Miranda Zenia","acuatica","animal",3),
  (37,"Lee Meghan","aerea","animal",1),
  (18,"Huber Oleg","terrenal","espiritual",4),
  (62,"Barclay Glenna","acuatica","mecanica",4),
  (15,"Weber Rudyard","acuatica","espiritual",5),
  (65,"Joshi Denton","aerea","espiritual",8),
  (93,"Merkle Graiden","terrenal","mecanica",7),
  (63,"Kameko Rinah","acuatica","espiritual",9);
INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (8,"Penelope Zachary","aerea","animal",10),
  (29,"Castillo Paloma","aerea","espiritual",5),
  (48,"India Angelica","aerea","espiritual",0),
  (39,"Hamilton Ginger","terrenal","mecanica",8),
  (87,"Knight Emily","acuatica","espiritual",2),
  (5,"Deforest Eagan","aerea","animal",7),
  (50,"Karadeniz Brandon","aerea","animal",2),
  (9,"Dufour Sonya","acuatica","animal",3),
  (86,"Fritz Stephen","aerea","mecanica",5),
  (35,"Burger Sarah","aerea","animal",10);
INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (31,"Bruin Garrett","terrenal","mecanica",1),
  (49,"Cade Aurora","aerea","espiritual",5),
  (97,"Autumn Paki","aerea","espiritual",9),
  (43,"Ruby Mallory","terrenal","espiritual",4),
  (74,"Ignacio Hillary","aerea","mecanica",1),
  (72,"Velasco Scott","terrenal","animal",5),
  (80,"Nguyễn Aiko","terrenal","espiritual",7),
  (66,"Craig Kato","terrenal","mecanica",6),
  (7,"Colorado Hayes","terrenal","espiritual",8),
  (7,"Demetrius Lucas","acuatica","espiritual",10);
INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (33,"Nguyễn Reuben","aerea","espiritual",10),
  (64,"Graiden Oren","aerea","animal",4),
  (74,"Kibo Colton","acuatica","espiritual",2),
  (2,"Andrew Bertha","aerea","animal",8),
  (42,"Otto Branden","aerea","animal",3),
  (96,"Kylee Blaze","acuatica","mecanica",2),
  (94,"Perez Callum","terrenal","espiritual",4),
  (92,"Ahmed Kirsten","aerea","animal",5),
  (90,"Sade Justina","aerea","mecanica",0),
  (92,"Cameron Fletcher","aerea","animal",1);
INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (14,"Schneider Karyn","aerea","animal",7),
  (67,"Fraser Gavin","acuatica","espiritual",2),
  (95,"Shaine Gregory","acuatica","animal",1),
  (80,"Harper Chaim","terrenal","animal",0),
  (26,"Autumn Leila","acuatica","mecanica",1),
  (45,"Magalhaes Vivian","terrenal","animal",0),
  (42,"Nguyễn Shannon","acuatica","animal",4),
  (46,"Lawrence Kiayada","terrenal","animal",8),
  (91,"Denton Sharon","aerea","animal",2),
  (83,"Vermeer Thomas","aerea","espiritual",3);
INSERT INTO montura (id_npc,nombre,tipo_ambiente,tipo_forma,probabilidad_drop)
VALUES
  (57,"Josiah Brock","terrenal","animal",4),
  (99,"Franca Kiara","terrenal","mecanica",10),
  (94,"Zeus Martena","terrenal","animal",6),
  (7,"Koopman Quintessa","aerea","animal",0),
  (46,"Berry Neville","aerea","mecanica",9),
  (89,"Eker Jermaine","terrenal","animal",1),
  (15,"Jillian Ramona","acuatica","animal",5),
  (52,"David Jameson","aerea","mecanica",8),
  (92,"Constance Aaron","acuatica","mecanica",6),
  (17,"Klassen Elton","aerea","mecanica",2);
