INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (3,4,"Manohar, Nelle",2,"4.63","31.33","6.34",81535),
  (2,3,"Chadwick M, Ferdinand",8,"-11.06","6.26","-4.01",69598),
  (4,4,"Caesar",9,"29.51","27.91","-9.29",57716),
  (4,2,"Nguyễn, Hayes",2,"-12.77","-7.79","-8.99",50682),
  (3,4,"Trevor H, Elizabeth",3,"3.55","6.40","1.64",55421),
  (2,2,"Beck",7,"5.20","-16.68","-13.14",45084),
  (2,5,"Merrill B, Colleen",2,"6.86","3.98","23.10",71030),
  (1,3,"Veronica Charlotte",5,"15.92","-24.27","30.44",47454),
  (3,4,"Kitra",4,"-20.33","-32.94","-6.73",53773),
  (2,2,"Fuller Sandra",6,"-14.84","15.63","-17.96",58673);
