INSERT INTO habilidad (nombre,segundos_reutilizacion,nivel_obtencion,origen,nivel_evolucion,afecta_gcd,duracion,distancia_al_target,duracion_canalizacion,es_interrumpible,se_canaliza,canalizacion_movimiento)
VALUES
  ("ipsum dolor sit amet, consectetuer",68,85,"uno mismo",18,"1",23,11,7,"0","1","1"),
  ("consequat auctor, nunc nulla",198,60,"uno mismo",37,"0",29,10,1,"1","1","1"),
  ("semper egestas, urna justo",205,108,"uno mismo",48,"1",26,5,1,"0","1","1"),
  ("erat nonummy ultricies ornare,",82,83,"uno mismo",119,"1",30,11,3,"0","1","0"),
  ("posuere",193,93,"uno mismo",7,"1",22,12,4,"0","1","1"),
  ("luctus felis purus ac tellus.",35,79,"objetivo",37,"1",16,15,7,"1","1","0"),
  ("amet, consectetuer",298,49,"uno mismo",56,"1",29,7,4,"0","1","1"),
  ("et magnis dis parturient",185,98,"objetivo",78,"0",16,14,6,"0","1","0"),
  ("varius et, euismod et, commodo",16,10,"objetivo",33,"1",11,13,2,"1","1","1"),
  ("Vivamus non",191,70,"objetivo",2,"1",21,4,4,"0","1","0");
INSERT INTO habilidad (nombre,segundos_reutilizacion,nivel_obtencion,origen,nivel_evolucion,afecta_gcd,duracion,distancia_al_target,duracion_canalizacion,es_interrumpible,se_canaliza,canalizacion_movimiento)
VALUES
  ("non enim. Mauris quis",210,47,"objetivo",89,"0",22,1,4,"0","1","0"),
  ("erat volutpat.",281,5,"uno mismo",100,"1",18,12,6,"1","1","1"),
  ("dui nec urna suscipit",251,20,"uno mismo",110,"0",12,7,5,"1","1","1"),
  ("elit. Nulla facilisi. Sed",154,4,"uno mismo",36,"0",8,10,4,"1","1","0"),
  ("pharetra.",60,112,"uno mismo",19,"1",12,6,7,"1","1","0"),
  ("fermentum risus, at fringilla",291,83,"objetivo",94,"1",8,9,2,"0","1","1"),
  ("tempor augue ac",252,40,"objetivo",28,"0",19,2,1,"0","1","0"),
  ("dolor. Quisque tincidunt pede",1,92,"uno mismo",54,"1",4,3,1,"0","1","1"),
  ("penatibus",6,76,"objetivo",59,"1",2,12,3,"0","1","0"),
  ("justo sit",135,86,"objetivo",15,"1",7,14,2,"0","1","1");
INSERT INTO habilidad (nombre,segundos_reutilizacion,nivel_obtencion,origen,nivel_evolucion,afecta_gcd,duracion,distancia_al_target,duracion_canalizacion,es_interrumpible,se_canaliza,canalizacion_movimiento)
VALUES
  ("eleifend,",134,83,"uno mismo",101,"0",25,4,2,"1","1","0"),
  ("penatibus et magnis",153,4,"uno mismo",4,"0",21,6,2,"1","1","1"),
  ("augue porttitor interdum",263,15,"objetivo",102,"1",21,4,6,"0","1","1"),
  ("vestibulum",268,53,"uno mismo",80,"1",11,15,6,"1","1","1"),
  ("ornare. Fusce mollis.",7,35,"uno mismo",100,"0",4,10,5,"0","1","1"),
  ("vel arcu.",92,84,"objetivo",45,"0",16,15,1,"1","1","0"),
  ("In mi pede,",110,113,"objetivo",7,"1",8,6,3,"0","1","0"),
  ("dictum augue malesuada. Integer",61,28,"uno mismo",65,"0",13,7,6,"1","1","0"),
  ("ipsum dolor sit amet,",23,19,"uno mismo",96,"0",25,11,4,"0","1","1"),
  ("dolor dapibus gravida.",157,117,"uno mismo",81,"0",11,5,2,"0","1","0");
INSERT INTO habilidad (nombre,segundos_reutilizacion,nivel_obtencion,origen,nivel_evolucion,afecta_gcd,duracion,distancia_al_target,duracion_canalizacion,es_interrumpible,se_canaliza,canalizacion_movimiento)
VALUES
  ("non leo. Vivamus",3,41,"uno mismo",59,"0",30,7,5,"0","1","0"),
  ("Mauris blandit enim consequat",267,109,"objetivo",13,"1",7,12,3,"0","1","1"),
  ("nulla. Cras eu",126,20,"uno mismo",79,"0",12,4,2,"1","1","1"),
  ("malesuada vel, convallis",116,114,"uno mismo",46,"0",23,1,3,"0","1","0"),
  ("egestas blandit. Nam nulla magna,",245,67,"objetivo",26,"0",4,6,1,"0","1","1"),
  ("Aenean egestas",34,74,"uno mismo",48,"1",14,4,4,"0","1","1"),
  ("Phasellus elit",38,25,"uno mismo",100,"0",27,6,6,"0","1","0"),
  ("amet, faucibus ut, nulla.",213,102,"uno mismo",71,"1",16,11,1,"0","1","0"),
  ("molestie orci tincidunt adipiscing.",283,37,"uno mismo",88,"1",14,8,3,"0","1","0"),
  ("iaculis nec, eleifend non,",191,32,"uno mismo",100,"1",24,13,6,"0","1","1");
INSERT INTO habilidad (nombre,segundos_reutilizacion,nivel_obtencion,origen,nivel_evolucion,afecta_gcd,duracion,distancia_al_target,duracion_canalizacion,es_interrumpible,se_canaliza,canalizacion_movimiento)
VALUES
  ("amet nulla. Donec",272,110,"uno mismo",96,"0",16,6,4,"0","1","1"),
  ("ipsum. Suspendisse sagittis.",195,114,"uno mismo",80,"0",29,14,1,"0","1","1"),
  ("ultrices iaculis",113,51,"objetivo",107,"0",8,10,6,"0","1","1");
