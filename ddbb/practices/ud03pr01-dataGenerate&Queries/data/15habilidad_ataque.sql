INSERT INTO habilidad_ataque (id_habilidad,ataque_inmediato,tipo)
VALUES
  (31,28,"magico"),
  (32,14,"magico"),
  (33,27,"fisico"),
  (34,23,"magico"),
  (35,32,"fisico"),
  (36,32,"fisico"),
  (37,25,"magico"),
  (38,37,"magico"),
  (39,33,"fisico"),
  (40,28,"magico");
INSERT INTO habilidad_ataque (id_habilidad,ataque_en_el_tiempo,tipo)
VALUES
  (41,14,"fisico"),
  (42,16,"magico"),
  (43,35,"fisico"),
  (44,36,"magico"),
  (45,24,"fisico"),
  (46,37,"fisico"),
  (47,17,"magico"),
  (48,29,"magico"),
  (49,15,"fisico"),
  (50,31,"magico");
