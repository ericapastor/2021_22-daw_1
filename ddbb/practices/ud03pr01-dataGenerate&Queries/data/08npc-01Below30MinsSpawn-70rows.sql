INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (4,1,"Gisela H, Nola",10,"-30.31","-25.19","4.01",1261),
  (4,1,"Kylie W, Remedios",2,"34.75","50.83","-9.21",1615),
  (4,3,"Hendrix, Samantha",4,"18.76","-6.90","5.95",388),
  (5,1,"Baumgartner, Wing",0,"19.30","3.47","17.97",1103),
  (2,3,"Geoffrey J, Veda",10,"26.26","20.24","4.08",1794),
  (1,2,"Zenaida Hilary",1,"-0.08","-12.04","-10.48",502),
  (4,2,"Hofer, Jamal",4,"1.99","-21.56","-1.28",246),
  (1,2,"Lamar, Jackson",6,"8.85","14.50","-15.78",1684),
  (2,2,"Jaquelyn",3,"-32.69","-21.83","1.93",1473),
  (3,2,"Zijlstra, Adrian",3,"-23.25","-12.32","2.76",260);
INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (3,2,"Jamalia",7,"5.91","-4.76","7.56",551),
  (2,1,"Stacey Doris",2,"-4.80","-43.39","19.23",1799),
  (4,1,"Cody Tiger",7,"18.02","35.45","-7.88",1760),
  (3,4,"Leilani G, Audra",8,"-18.63","-35.62","9.96",610),
  (3,2,"Rafael Palmer",6,"2.99","13.74","-10.87",1056),
  (4,3,"Ergin, Kadeem",4,"-14.61","12.73","-6.16",1733),
  (4,3,"Heidi",2,"-10.21","10.51","35.14",901),
  (3,3,"Nascimento, Naida",1,"33.46","3.78","-2.16",283),
  (3,3,"Colby Darius",4,"-39.08","0.81","-4.88",1427),
  (1,2,"Laura U, Kenneth",3,"-26.78","-10.14","10.55",870);
INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (2,4,"Kevyn Idona",9,"-17.14","-12.80","-2.65",674),
  (4,4,"van Rijn, Lani",2,"23.13","69.19","7.10",597),
  (1,4,"Lopez, Ivan",4,"-24.27","-39.30","11.38",227),
  (2,5,"Lawrence I, Dale",8,"25.68","8.29","-3.39",485),
  (4,4,"Price D, Branden",1,"48.61","-59.40","4.48",1531),
  (4,3,"Nathan Hillary",6,"14.19","23.05","-8.98",1314),
  (3,2,"Shaine",1,"9.27","0.41","-10.47",255),
  (4,5,"Nina C, Dennis",8,"-42.61","-4.34","-17.95",1614),
  (5,3,"Abdul Ruby",8,"-34.55","48.89","-1.67",575),
  (3,3,"Guerrero, Nita",10,"-14.89","6.42","-11.21",341);
INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (5,5,"David",1,"-5.24","6.19","4.36",1148),
  (4,3,"Jelani O, Patience",5,"-57.56","33.48","3.62",1127),
  (4,2,"Oliver Regina",7,"-1.88","47.98","-9.22",953),
  (1,5,"Laura, Deirdre",2,"-25.21","8.85","23.30",1026),
  (5,2,"Ivan Q, Serina",1,"10.48","39.01","28.07",742),
  (2,5,"Kenneth Cameran",7,"-13.09","11.85","5.67",832),
  (2,4,"Strauß, Nehru",0,"28.68","4.29","7.44",908),
  (3,2,"Scholz, Cyrus",5,"11.11","-16.25","-4.35",575),
  (5,4,"Cadman",5,"13.12","23.84","-8.44",146),
  (2,3,"Quin X, Kalia",0,"-11.11","17.41","40.49",242);
INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (4,1,"MacKenzie",4,"-20.67","-31.46","-0.72",732),
  (3,1,"Destiny Elmo",2,"18.85","37.93","9.98",527),
  (2,3,"Hạ, Brynne",5,"6.28","-13.51","11.73",567),
  (1,2,"McKenzie Reece",3,"1.67","8.22","-1.75",845),
  (1,2,"Ursula Jordan",2,"-51.69","14.34","-6.18",378),
  (1,3,"Boger, Arthur",5,"-36.05","16.53","-38.22",204),
  (1,2,"Thompson, Axel",1,"27.13","-4.49","1.48",550),
  (5,3,"Finn J, Jermaine",7,"33.64","18.74","-0.58",1792),
  (3,4,"Lane O, Cullen",3,"45.77","-5.27","-9.35",633),
  (4,2,"Watkins, Sean",8,"-10.75","-13.61","-24.03",1020);
INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (5,1,"Boris",6,"4.77","-61.37","-18.04",730),
  (4,5,"Ursa Tad",7,"30.87","4.71","8.79",1584),
  (3,3,"Rhona Q, Daphne",9,"20.58","11.91","2.37",664),
  (1,2,"Maris",7,"-53.14","-11.86","9.05",1634),
  (2,1,"de Jesus, Kevyn",7,"-21.45","-37.76","13.07",1158),
  (1,2,"Siqueira, Myles",0,"8.27","4.02","23.58",1514),
  (1,2,"Dawn",8,"-24.05","-25.90","13.39",311),
  (4,3,"Nora",5,"-28.22","39.95","-11.17",1302),
  (2,1,"King, Vivien",5,"50.57","-2.30","-2.59",525),
  (1,4,"Deacon L, Kristen",2,"-15.99","11.64","8.31",1436);
INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (3,4,"Kato Melinda",1,"23.85","20.71","-13.61",1077),
  (4,5,"Reyes, Brendan",0,"-1.77","6.32","-40.87",1102),
  (2,2,"Rhoda Cadman",7,"2.15","-29.11","7.53",1127),
  (2,2,"Christen Melvin",4,"23.93","1.17","-6.35",1609),
  (2,4,"Cortes, Dorothy",7,"-11.61","-7.99","2.32",1058),
  (3,1,"Olympia",3,"9.60","-2.50","4.65",1307),
  (4,4,"Azalia P, Quentin",10,"-19.08","31.37","6.54",737),
  (3,4,"Dean",7,"-21.40","-17.63","-20.20",903),
  (4,4,"Yvonne Xenos",5,"10.43","-23.99","-10.96",210),
  (2,5,"Rai, Melvin",6,"41.59","40.55","-7.36",861);
