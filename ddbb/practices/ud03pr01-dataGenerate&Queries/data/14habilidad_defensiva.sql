INSERT INTO habilidad_defensiva (id_habilidad,reduccion_ataque_fisico,inmunidad)
VALUES
  (11,6,"0"),
  (12,10,"0"),
  (13,6,"0"),
  (14,22,"0"),
  (15,29,"0");
INSERT INTO habilidad_defensiva (id_habilidad,reduccion_ataque_magico,inmunidad)
VALUES
  (16,22,"0"),
  (17,10,"0"),
  (18,26,"0"),
  (19,20,"0"),
  (20,24,"0");
INSERT INTO habilidad_defensiva (id_habilidad,absorcion_ataque_fisico,inmunidad)
VALUES
  (21,29,"0"),
  (22,41,"0"),
  (23,36,"0"),
  (24,37,"0");
INSERT INTO habilidad_defensiva (id_habilidad,absorcion_ataque_magico,inmunidad)
VALUES
  (25,38,"0"),
  (26,34,"0"),
  (27,41,"0"),
  (28,30,"0");
INSERT INTO habilidad_defensiva (id_habilidad,inmunidad,duracion_inmunidad)
VALUES
  (29,"1",17),
  (30,"1",25);
