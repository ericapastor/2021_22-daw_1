INSERT INTO habilidad_sanacion (id_habilidad,sanacion_inmediata,afectada_por_venenos_o_sangrado)
VALUES
  (51,30,"0"),
  (52,30,"0"),
  (53,22,"0"),
  (54,35,"1"),
  (55,28,"1"),
  (56,39,"1"),
  (57,33,"0"),
  (58,29,"0"),
  (59,38,"0"),
  (60,16,"0");
INSERT INTO habilidad_sanacion (id_habilidad,sanacion_en_el_tiempo,afectada_por_venenos_o_sangrado)
VALUES
  (61,36,"1"),
  (62,28,"0"),
  (63,16,"1"),
  (64,20,"1"),
  (65,15,"1"),
  (66,32,"0"),
  (67,34,"0"),
  (68,34,"1"),
  (69,26,"0"),
  (70,23,"1");
