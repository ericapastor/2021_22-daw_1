
INSERT INTO habilidad_debilitadora (id_habilidad,control_masas,tiempo_cc)
VALUES
  (81,"encarcela",10),
  (82,"adormece",4),
  (83,"paraliza",3),
  (84,"congela",7),
  (85,"ralentiza",6);
INSERT INTO habilidad_debilitadora (id_habilidad,reduce_vel_ataque,aumenta_casteo)
VALUES
  (86,5,0),
  (87,0,3),
  (88,1,4),
  (89,8,0),
  (90,0,5);
INSERT INTO habilidad_debilitadora (id_habilidad,reduce_estadistica)
VALUES
  (91,5),
  (92,10),
  (93,3),
  (94,8),
  (95,10);