INSERT INTO clase_habilidad (id_personaje,nombre,nivel,familia,tipo,fecha_adopcion)
VALUES
  (38,"Seth Bauer",13,"mamiferos","metal","2015-02-04 13:00:10"),
  (41,"Amos van Gelder",3,"semipeces","arcano","2015-02-18 14:20:49"),
  (56,"Halee Schinacher",59,"peces","metal","2020-03-31 18:27:00"),
  (26,"Cameran Navarro",24,"humanoides","agua","2019-12-26 07:44:44"),
  (31,"Dora Reid",47,"reptiles","hada","2015-03-11 06:23:36"),
  (63,"Giacomo White",37,"semiaves","metal","2015-06-07 17:16:18"),
  (51,"Kessie Nath",38,"reptiles","fantasma","2016-11-30 04:24:47"),
  (45,"Deacon Vincent",40,"peces","fuego","2022-05-19 00:45:59"),
  (35,"Lionel Valenzuela",14,"semiaves","maquina","2020-10-28 04:34:02"),
  (51,"Catherine Hậ",12,"semiaves","tierra","2021-12-21 06:04:15"),
  (52,"Isaiah Fortier",42,"insectos","hielo","2023-02-08 16:07:10"),
  (3,"Malik Nijenhuis",26,"semipeces","maquina","2020-05-25 11:46:42"),
  (56,"Macaulay Srivastav",26,"insectos","arcano","2017-01-20 12:02:55"),
  (16,"Jenna Figueroa",57,"mamiferos","fantasma","2020-12-23 07:48:37"),
  (54,"Diana Schneider",45,"semiaves","maquina","2021-12-27 02:16:02"),
  (35,"Lavinia Stolk",12,"humanoides","arcano","2022-09-30 17:03:03"),
  (51,"Kasimir Paz",42,"peces","arcano","2022-02-16 03:04:14"),
  (35,"Basil Roggeveen",33,"aracnidos","agua","2019-01-17 22:21:54"),
  (26,"Hedda Monet",11,"aves","arcano","2018-01-19 01:55:30"),
  (44,"Remedios Burns",25,"moluscos","hielo","2022-05-20 04:59:44"),
  (22,"Bianca da Cunha",31,"reptiles","hielo","2021-08-30 18:03:55"),
  (42,"Karyn Karataş",59,"mamiferos","arcano","2019-03-20 14:14:06"),
  (41,"Imani Đào",46,"reptiles","arcano","2021-06-01 11:12:36"),
  (27,"Abbot Baumgartner",37,"reptiles","hada","2016-11-07 16:45:36"),
  (42,"Charde Mahajan",50,"semiaves","hada","2020-07-16 16:42:35"),
  (31,"Indigo Hill",12,"semiaves","maquina","2015-08-09 13:19:27"),
  (41,"Hashim Crespo",15,"reptimamiferos","roca","2019-05-28 12:24:24"),
  (42,"Scott Tuncer",24,"insectos","agua","2017-07-16 18:43:14"),
  (2,"Sade Tapia",11,"humanoides","fuego","2022-08-26 17:35:12"),
  (57,"Arsenio Versteeg",53,"mamiferos","metal","2022-11-19 15:02:01"),
  (44,"Quinn Bilgin",22,"aves","metal","2018-01-15 14:12:05"),
  (52,"Karen Brands",25,"peces","electrico","2019-12-20 19:02:14"),
  (52,"Stacy Neuville",4,"semipeces","hielo","2019-08-03 05:49:51"),
  (54,"Rina Durak",26,"semipeces","maquina","2021-03-02 18:48:14"),
  (35,"Thor Cabral",7,"humanoides","maquina","2019-12-29 04:05:46"),
  (61,"Francis Hamilton",28,"aracnidos","agua","2022-12-21 22:15:41"),
  (29,"Hayley Anjorin",3,"semipeces","maquina","2017-11-11 04:22:10"),
  (36,"Jada Meyer",12,"humanoides","tierra","2021-06-12 06:09:33"),
  (12,"Candice Boom",49,"insectos","metal","2016-06-30 01:26:22"),
  (43,"Kiayada Ajudua",15,"semipeces","hielo","2017-02-21 18:41:37"),
  (5,"Keaton Cardoso",38,"semipeces","fantasma","2018-05-10 07:15:08"),
  (52,"Lareina Contreras",37,"reptimamiferos","roca","2016-05-04 22:50:31"),
  (74,"Chaney Matilde",43,"peces","arcano","2016-05-19 08:09:30"),
  (56,"Stone Adewusi",1,"moluscos","metal","2023-01-04 00:24:00"),
  (52,"Maxwell Bentlee",39,"semipeces","aire","2017-11-01 22:16:35"),
  (26,"Laura Garcia",2,"moluscos","hada","2016-02-17 04:58:51"),
  (18,"Darius Dubois",4,"aves","hielo","2017-12-09 04:15:58"),
  (24,"Harding Post",52,"humanoides","electrico","2022-11-11 08:17:11"),
  (20,"Abra Uğurlu",12,"moluscos","agua","2016-07-25 12:51:45"),
  (45,"Donovan Moya",18,"insectos","electrico","2018-12-04 01:38:30");
INSERT INTO clase_habilidad (id_personaje,nombre,nivel,familia,tipo,fecha_adopcion)
VALUES
  (9,"Rhiannon Baker",22,"semipeces","fuego","2017-09-26 09:39:07"),
  (44,"Kessie Azevedo",4,"semiaves","maquina","2021-06-08 10:56:43"),
  (24,"Sean Flores",18,"humanoides","fantasma","2018-03-21 07:37:53"),
  (65,"Luke Adewumi",50,"semiaves","metal","2017-06-09 08:46:45"),
  (22,"Shoshana Gür",23,"insectos","agua","2020-02-18 05:50:57"),
  (12,"Salvador Nguyễn",57,"reptimamiferos","agua","2017-06-01 05:30:40"),
  (66,"Meredith Chavez",4,"humanoides","agua","2018-11-08 21:42:28"),
  (69,"Tyler Bradley",5,"reptimamiferos","aire","2016-10-23 22:52:34"),
  (30,"Maxine Makinde",8,"aracnidos","metal","2019-01-18 02:51:17"),
  (65,"Aimee Bell",36,"mamiferos","arcano","2021-04-07 10:55:21"),
  (1,"Cheryl Baran",30,"aracnidos","aire","2019-03-24 00:31:00"),
  (25,"Porter Ibañez",3,"mamiferos","maquina","2017-11-26 14:58:07"),
  (18,"Tanisha Jacobi",14,"insectos","electrico","2023-02-08 18:43:02"),
  (55,"Yuli Berger",37,"humanoides","hielo","2017-04-27 11:21:29"),
  (36,"Kiona Nguyễn",23,"insectos","maquina","2016-01-22 13:11:26"),
  (5,"Sydney Kahraman",17,"reptimamiferos","roca","2018-04-30 23:47:30"),
  (3,"Magee da Conceiçao",25,"mamiferos","electrico","2015-03-30 03:16:18"),
  (24,"Tashya Chavez",2,"moluscos","roca","2017-12-19 21:15:35"),
  (39,"Fletcher Gezer",9,"insectos","arcano","2017-05-31 02:32:59"),
  (61,"Isaiah Holmes",35,"mamiferos","fuego","2018-08-07 22:20:00"),
  (13,"Daquan Duarte",25,"peces","fuego","2021-10-26 21:13:12"),
  (56,"Brianna Elliott",47,"insectos","maquina","2022-03-18 06:24:41"),
  (61,"Freya Elzinga",12,"aracnidos","arcano","2017-11-04 08:34:13"),
  (47,"Haley Schulz",52,"aves","metal","2022-05-16 14:11:45"),
  (39,"Benjamin Altay",13,"semipeces","arcano","2021-10-12 16:04:50"),
  (8,"Caleb Paula",37,"aracnidos","fantasma","2022-03-14 05:54:07"),
  (51,"Hollee Barros",17,"reptiles","maquina","2021-09-08 08:16:35"),
  (64,"Grant Roberts",45,"peces","fantasma","2021-12-13 20:42:53"),
  (58,"Alden Nguyễn",17,"reptiles","metal","2017-05-29 07:30:05"),
  (75,"Mariam Karaca",40,"semiaves","hada","2020-09-20 14:01:17"),
  (69,"Reuben Carla",3,"humanoides","maquina","2022-11-21 05:55:11"),
  (11,"Omar Fonseca",38,"peces","hielo","2019-09-30 16:53:28"),
  (65,"Merrill Stephens",26,"semiaves","tierra","2017-09-04 08:00:22"),
  (19,"Jade Leroux",28,"humanoides","aire","2017-07-04 01:55:18"),
  (70,"Melodie Ward",20,"insectos","tierra","2018-03-01 03:32:18"),
  (29,"Thor Domingues",26,"humanoides","arcano","2017-07-10 06:04:38"),
  (1,"Regan Romeijnders",2,"humanoides","tierra","2016-07-19 11:11:04"),
  (7,"Maisie Dixon",8,"moluscos","hielo","2020-10-28 17:18:57"),
  (65,"Montana Gordon",12,"reptiles","fuego","2017-06-21 01:40:10"),
  (62,"Aline Bogner",23,"peces","electrico","2017-03-04 14:23:58"),
  (45,"Guy Dağ",21,"semiaves","aire","2018-05-19 13:56:09"),
  (44,"Ignatius Steiner",4,"reptimamiferos","roca","2021-07-03 23:10:59"),
  (74,"Mercedes Schneider",23,"aracnidos","metal","2020-02-26 04:22:25"),
  (36,"Olympia van der Heijden",31,"mamiferos","roca","2021-01-26 10:13:29"),
  (56,"Jermaine Braun",3,"reptimamiferos","maquina","2022-07-19 22:00:26"),
  (50,"Xantha Reyes",19,"semipeces","metal","2022-11-07 07:26:02"),
  (32,"Mollie Castro",45,"humanoides","fantasma","2017-03-26 08:42:16"),
  (63,"Shelby Nguyễn",41,"insectos","hada","2016-03-27 05:39:36"),
  (68,"Fuller Adejuyigbe",25,"mamiferos","hada","2018-01-26 05:04:23"),
  (14,"Talon Kuiper",36,"aves","aire","2018-05-27 06:29:49");
