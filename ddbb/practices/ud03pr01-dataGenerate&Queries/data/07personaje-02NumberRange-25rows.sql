INSERT INTO personaje (id_usuario,id_raza,id_clase,nombre,nivel,tatuajes,procedencia,num_NPC_asesinados,punta_nariz,curva_nariz,puente_nariz,tipo_barbilla,altura_pomulos,forma_mandibula,anchura_mandibula,celeridad_total,parasitar_total,estabilidad_total,velocidad_mov_total,defensa_total,ataque_total,nivel_medio_objeto)
VALUES
  (21,3,4,"Josephine Puckett",38,"0","Elhaun`sharah",146,"redonda","plana","alto","afilada","media","redonda","mediana",74,10,36,7,9,66,334),
  (14,4,5,"Gilliam Elijah",32,"1","Elhaun`sharah",481,"redonda","concava","bajo","cuadrada","media","afilada","mediana",5,6,14,26,74,20,111),
  (21,3,3,"Melanie, Sara, Austin",18,"0","Grupkyt",323,"redonda","plana","medio","afilada","media","afilada","mediana",10,7,41,41,89,15,151),
  (9,4,4,"Jamalia",60,"0","Krrrprrgrrlkmpkah",358,"afilada","convexa","medio","redonda","baja","afilada","estrecha",57,12,33,25,63,83,191),
  (9,2,4,"Victor, Clinton, Justina",114,"1","Elhaun`sharah",289,"redonda","convexa","bajo","cuadrada","baja","afilada","mediana",48,4,3,10,12,54,229),
  (2,3,2,"Derek Potts",73,"1","Lykos",2,"afilada","concava","bajo","cuadrada","alta","redonda","ancha",91,9,44,30,66,8,326),
  (11,3,3,"Kaye, Hyde",16,"1","Jryushkp",358,"redonda","concava","bajo","redonda","alta","redonda","estrecha",32,7,62,8,19,18,287),
  (9,2,2,"Pearl, Goodman",53,"0","Jryushkp",243,"afilada","concava","medio","redonda","baja","redonda","estrecha",64,11,86,1,67,21,418),
  (10,1,1,"Eaton, Spencer",69,"0","Mo`onshar",237,"redonda","concava","alto","cuadrada","baja","afilada","estrecha",36,6,75,15,41,62,123),
  (13,2,4,"Ishmael, Sweeney",114,"0","Jryushkp",434,"redonda","convexa","alto","cuadrada","baja","redonda","mediana",92,6,51,12,50,9,254);
INSERT INTO personaje (id_usuario,id_raza,id_clase,nombre,nivel,tatuajes,procedencia,num_NPC_asesinados,punta_nariz,curva_nariz,puente_nariz,tipo_barbilla,altura_pomulos,forma_mandibula,anchura_mandibula,celeridad_total,parasitar_total,estabilidad_total,velocidad_mov_total,defensa_total,ataque_total,nivel_medio_objeto)
VALUES
  (15,4,5,"Dieter, Hunter, Brody",43,"1","Pyrokas",303,"redonda","concava","medio","cuadrada","baja","cuadrada","mediana",22,6,54,32,41,12,11),
  (2,1,2,"Wendy Kimberly",40,"0","Elhaun`sharah",487,"redonda","convexa","bajo","redonda","baja","redonda","ancha",71,11,2,39,90,49,56),
  (22,1,4,"Kadeem, Patrick, Hannah",47,"0","Lykos",57,"redonda","plana","alto","afilada","baja","cuadrada","estrecha",68,10,43,28,5,60,252),
  (20,1,4,"Noelle",110,"0","Elhaun`sharah",296,"afilada","plana","alto","cuadrada","media","afilada","mediana",62,19,21,18,52,62,411),
  (20,4,2,"Macey Waters",18,"1","Ontaru",172,"afilada","plana","bajo","cuadrada","media","afilada","mediana",72,19,79,13,76,65,288),
  (20,4,5,"Mira Dustin",112,"0","Wqrsftmnvcxz",115,"redonda","concava","medio","cuadrada","alta","cuadrada","mediana",60,14,26,21,7,52,426),
  (9,2,4,"Clayton",90,"1","Mo`onshar",141,"afilada","plana","bajo","redonda","baja","redonda","ancha",3,14,95,44,40,98,439),
  (16,5,4,"Hodges Whitney",28,"0","Grupkyt",173,"redonda","convexa","medio","cuadrada","alta","afilada","estrecha",61,4,71,34,31,56,147),
  (4,3,1,"Bond Nicole",37,"0","Mo`onshar",170,"afilada","convexa","alto","redonda","baja","cuadrada","ancha",9,18,23,33,43,66,252),
  (25,4,2,"Edward Magee",51,"1","Jryushkp",272,"afilada","plana","medio","afilada","media","redonda","estrecha",68,11,30,23,55,58,391);
INSERT INTO personaje (id_usuario,id_raza,id_clase,nombre,nivel,tatuajes,procedencia,num_NPC_asesinados,punta_nariz,curva_nariz,puente_nariz,tipo_barbilla,altura_pomulos,forma_mandibula,anchura_mandibula,celeridad_total,parasitar_total,estabilidad_total,velocidad_mov_total,defensa_total,ataque_total,nivel_medio_objeto)
VALUES
  (15,1,2,"Mcdaniel Steel",19,"0","Jiryosh`myan",362,"redonda","plana","medio","afilada","baja","afilada","mediana",77,12,98,15,98,85,17),
  (9,3,1,"Sonia, Holcomb",97,"1","Jryushkp",119,"redonda","concava","alto","cuadrada","baja","cuadrada","ancha",61,1,7,31,98,7,353),
  (9,3,4,"Anne Jermaine",31,"0","Jryushkp",490,"redonda","plana","bajo","afilada","alta","afilada","mediana",61,13,27,21,13,42,170),
  (8,1,3,"Shepard Donovan",109,"1","Lykos",54,"afilada","convexa","bajo","afilada","media","afilada","mediana",74,6,78,15,30,60,127),
  (11,3,3,"Henry Tanner",95,"1","Krrrprrgrrlkmpkah",148,"afilada","concava","alto","afilada","media","afilada","estrecha",91,17,62,47,36,53,128);
