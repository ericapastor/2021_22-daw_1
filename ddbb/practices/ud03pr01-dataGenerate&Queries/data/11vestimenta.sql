INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,estabilidad,irrompibilidad,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("tempor","#a51310","malla",66,77,3,17,19,"0",6,"cutre","cabeza",-97),
  ("nunc id","#5eb71f","cuero",61,49,11,13,13,"0",3,"cutre","cabeza",105),
  ("malesuada ut,","#e2b383","placas",66,65,13,10,17,"0",1,"cutre","cabeza",265),
  ("tempor, est ac","#05485e","tela",16,42,2,15,11,"0",3,"cutre","cabeza",-21),
  ("posuere cubilia","#02a8e0","malla",37,87,17,3,8,"0",6,"cutre","cabeza",228),
  ("accumsan convallis,","#f9b7b6","malla",3,100,2,4,14,"0",4,"cutre","hombros",-291),
  ("dictum","#70efa9","placas",68,66,4,19,12,"0",7,"cutre","hombros",266),
  ("ut mi.","#96ce1c","tela",65,72,11,20,5,"0",7,"cutre","hombros",262),
  ("nascetur","#5c71ce","cuero",6,60,8,10,18,"0",5,"cutre","hombros",124),
  ("orci, adipiscing","#140de0","cuero",27,51,3,12,2,"0",5,"cutre","hombros",98);
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,estabilidad,irrompibilidad,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("magna. Praesent interdum","#a82c2a","malla",45,91,12,1,2,"0",7,"cutre","pecho",164),
  ("tortor. Integer","#7beddf","placas",39,38,17,5,1,"0",2,"cutre","pecho",56),
  ("eros nec tellus.","#88fca9","cuero",27,81,4,8,8,"0",3,"cutre","pecho",103),
  ("tortor,","#36bf11","placas",64,37,17,8,2,"0",6,"cutre","pecho",122),
  ("sit amet ultricies","#64e59e","tela",34,73,5,3,13,"0",4,"cutre","pecho",90),
  ("Mauris molestie","#e20804","placas",42,65,16,6,8,"0",5,"cutre","piernas",-282),
  ("Fusce mollis. Duis","#e0c04e","malla",47,41,10,7,14,"0",5,"cutre","piernas",-201),
  ("Sed","#7196d6","cuero",39,69,11,12,13,"0",1,"cutre","piernas",66),
  ("nunc, ullamcorper","#3051ad","cuero",14,22,9,14,10,"0",4,"cutre","piernas",95),
  ("elit pede,","#142dce","tela",65,86,10,17,15,"0",6,"cutre","piernas",256);
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,estabilidad,irrompibilidad,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("lectus","#f7aead","cuero",20,27,16,12,20,"0",3,"cutre","pies",-61),
  ("quis, pede.","#30fff1","cuero",28,37,9,6,9,"0",4,"cutre","pies",110),
  ("dolor. Fusce","#e0b20d","malla",34,37,13,9,6,"0",2,"cutre","pies",-197),
  ("mauris, aliquam","#2e5896","placas",55,70,13,15,8,"0",5,"cutre","pies",153),
  ("Integer id","#2c6889","tela",58,44,5,19,19,"0",6,"cutre","pies",-282),
  ("conubia nostra, per","#f2a6a4","placas",66,54,23,23,21,"0",19,"normal","cabeza",362),
  ("blandit at,","#94ea3f","tela",57,53,28,18,30,"0",31,"normal","cabeza",443),
  ("rhoncus id,","#d1be19","cuero",55,53,34,21,18,"0",30,"normal","cabeza",-303),
  ("sed dui.","#aaffde","placas",72,51,17,23,19,"0",25,"normal","cabeza",-138),
  ("elit erat vitae","#bfc8fc","tela",86,51,23,16,27,"0",31,"normal","cabeza",-445);
;
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,estabilidad,parasitar,irrompibilidad,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("Sed dictum. Proin","#db605e","malla",98,93,26,34,27,4,"0",30,"normal","hombros",469),
  ("sit amet","#62d1cf","placas",77,86,26,33,26,4,"0",21,"normal","hombros",-433),
  ("mi eleifend","#08b230","cuero",27,43,20,31,29,4,"0",10,"normal","hombros",-108),
  ("Pellentesque ultricies dignissim","#74e084","tela",47,79,30,21,15,1,"0",29,"normal","hombros",172),
  ("elementum,","#2f5bd6","tela",66,75,32,25,22,4,"0",20,"normal","hombros",-189),
  ("Nulla eu neque","#bc110f","placas",73,74,29,25,26,1,"0",28,"normal","pecho",-32),
  ("natoque","#208c0e","tela",93,84,28,23,25,4,"0",28,"normal","pecho",-287),
  ("Aliquam ornare,","#3c6dc1","cuero",19,69,33,34,24,3,"0",8,"normal","pecho",15),
  ("felis purus","#f97e54","placas",51,77,26,20,17,1,"0",32,"normal","pecho",303),
  ("nec","#47edea","malla",22,36,18,29,20,3,"0",33,"normal","pecho",-27);
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,estabilidad,parasitar,irrompibilidad,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("egestas lacinia. Sed","#ffc0bf","placas",31,64,28,21,33,3,"0",36,"normal","piernas",13),
  ("ante","#48f42e","tela",70,69,24,18,19,2,"0",25,"normal","piernas",112),
  ("viverra. Donec","#159e12","placas",16,83,16,22,27,4,"0",22,"normal","piernas",276),
  ("tellus.","#70cde0","malla",55,78,27,18,31,2,"0",21,"normal","piernas",113),
  ("urna justo","#3a06a3","cuero",62,48,27,25,28,5,"0",32,"normal","piernas",357),
  ("nulla","#f4a7a6","malla",46,43,29,25,26,3,"0",8,"normal","pies",-388),
  ("ipsum non","#e0600b","malla",97,78,34,32,32,2,"0",26,"normal","pies",-243),
  ("Donec vitae erat","#d3c60e","placas",99,68,30,34,35,2,"0",27,"normal","pies",-76),
  ("ultrices. Duis volutpat","#f2c098","cuero",67,51,30,16,28,4,"0",20,"normal","pies",329),
  ("tellus. Nunc lectus","#d669e0","tela",190,57,34,24,16,3,"0",17,"normal","pies",435);
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,estabilidad,parasitar,irrompibilidad,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("Lorem ipsum dolor","#f77471","tela",161,71,40,37,36,12,"1",42,"legendaria","cabeza",-290),
  ("penatibus","#76f29b","tela",103,99,41,37,45,14,"1",40,"legendaria","cabeza",572),
  ("rutrum","#3d8701","malla",272,69,41,42,35,12,"0",40,"legendaria","cabeza",-728),
  ("et netus","#cff492","placas",65,71,43,43,38,12,"1",41,"legendaria","cabeza",718),
  ("feugiat. Sed","#271396","placas",113,82,43,40,44,14,"0",41,"legendaria","cabeza",637),
  ("sed tortor.","#c42d2b","tela",233,69,44,42,35,14,"0",42,"legendaria","hombros",109),
  ("Nunc pulvinar arcu","#5ae89a","placas",179,97,36,44,43,10,"1",45,"legendaria","hombros",283),
  ("pellentesque.","#119186","placas",174,76,36,45,41,11,"1",41,"legendaria","hombros",866),
  ("facilisis","#94f2fc","cuero",66,100,35,44,35,11,"0",44,"legendaria","hombros",-864),
  ("sed consequat auctor,","#ed8b4e","malla",115,86,39,36,40,14,"1",41,"legendaria","hombros",-797);
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,estabilidad,parasitar,irrompibilidad,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("quis diam luctus","#ffcccc","cuero",147,68,37,39,44,14,"0",42,"legendaria","pecho",-546),
  ("malesuada malesuada.","#8bedb7","tela",222,92,38,37,35,13,"0",41,"legendaria","pecho",-945),
  ("nunc nulla","#5489d8","tela",165,94,41,42,41,10,"1",43,"legendaria","pecho",-31),
  ("elit elit","#aefcbc","malla",213,76,39,37,36,14,"0",43,"legendaria","pecho",611),
  ("imperdiet","#adedf7","placas",299,85,44,36,43,11,"0",45,"legendaria","pecho",-439),
  ("congue","#e87a78","placas",218,75,41,35,37,13,"1",41,"legendaria","piernas",439),
  ("arcu.","#2bad1d","tela",242,95,39,40,37,12,"1",41,"legendaria","piernas",382),
  ("vestibulum massa rutrum","#dcf9a4","cuero",104,80,41,44,42,13,"1",42,"legendaria","piernas",-309),
  ("natoque penatibus","#a6fbfc","cuero",139,96,39,38,41,12,"0",44,"legendaria","piernas",-205),
  ("dolor.","#d6a617","malla",95,89,45,36,37,13,"1",44,"legendaria","piernas",-676);
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,celeridad,estabilidad,parasitar,irrompibilidad,reduccion_golpe_caida,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("Fusce","#ea4644","cuero",116,95,41,38,38,12,"1",41,"legendaria","pies",-869),
  ("tincidunt vehicula risus.","#72f469","cuero",47,71,35,41,44,11,"0",41,"legendaria","pies",-354),
  ("Sed nec","#0052b7","placas",244,95,42,45,35,10,"0",42,"legendaria","pies",-602),
  ("fames ac","#8a5fe8","tela",250,90,35,42,44,14,"0",41,"legendaria","pies",93),
  ("auctor, velit","#57e057","malla",119,73,39,41,41,14,"0",41,"legendaria","pies",282);
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,celeridad,estabilidad,parasitar,irrompibilidad,reduccion_golpe_caida,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("eget","#e89f9d","tela",226,81,48,45,60,47,15,"1",48,44,"mitica","cabeza",-1848),
  ("enim. Nunc","#79ea38","placas",239,83,45,49,68,50,17,"1",32,45,"mitica","cabeza",772),
  ("elementum, dui","#70f488","placas",241,87,46,49,63,48,16,"1",40,41,"mitica","cabeza",-1160),
  ("sed dolor. Fusce","#e09479","cuero",231,84,49,50,55,47,18,"0",34,43,"mitica","cabeza",-767),
  ("aliquet nec, imperdiet","#57cc18","malla",226,82,46,47,58,49,17,"0",40,45,"mitica","cabeza",1403);
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,celeridad,estabilidad,parasitar,irrompibilidad,reduccion_golpe_caida,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("mollis","#c4120f","tela",237,87,48,48,66,47,18,"0",40,45,"mitica","hombros",-670),
  ("ligula. Aenean","#ff6207","malla",248,98,48,50,54,49,19,"0",21,41,"mitica","hombros",48),
  ("sodales elit","#2746dd","placas",215,90,45,48,53,49,20,"0",27,42,"mitica","hombros",-587),
  ("eu","#b8f731","cuero",232,94,45,47,57,48,17,"0",30,41,"mitica","hombros",1324),
  ("litora","#81dd75","malla",240,99,49,45,54,46,19,"1",21,43,"mitica","hombros",946),
  ("turpis","#d33532","tela",216,88,46,48,56,48,18,"0",48,40,"mitica","pecho",-197),
  ("Cras pellentesque.","#d89845","placas",238,95,46,47,69,46,17,"0",45,43,"mitica","pecho",1930),
  ("ornare, facilisis","#e58880","cuero",240,86,47,46,51,48,15,"0",23,44,"mitica","pecho",-207),
  ("Phasellus","#96b3e0","cuero",223,94,47,46,53,45,16,"1",47,40,"mitica","pecho",-1367),
  ("Phasellus nulla.","#7867f7","malla",219,90,49,45,66,47,19,"0",45,43,"mitica","pecho",-1305);
INSERT INTO vestimenta (nombre,color,material,nivel_objeto,durabilidad,ataque,defensa,celeridad,estabilidad,parasitar,irrompibilidad,reduccion_golpe_caida,velocidad_movimiento,calidad,tipo,influyen_opinionNPC)
VALUES
  ("pede, nonummy","#f4aead","placas",213,97,46,48,55,48,17,"1",26,42,"mitica","piernas",437),
  ("nulla. Integer vulputate,","#9df2c3","malla",214,93,46,50,60,45,15,"0",46,43,"mitica","piernas",330),
  ("Sed nulla ante,","#a9cbfc","tela",243,86,45,45,55,49,16,"0",47,40,"mitica","piernas",-1364),
  ("arcu vel quam","#92efd9","tela",249,99,48,46,57,50,19,"0",46,40,"mitica","piernas",772),
  ("feugiat nec,","#044fe5","cuero",233,80,48,47,53,46,20,"1",42,41,"mitica","piernas",1671),
  ("urna. Vivamus","#f78988","cuero",233,100,48,46,56,49,19,"1",27,41,"mitica","pies",50),
  ("amet, consectetuer adipiscing","#aef29d","malla",218,96,48,48,64,50,19,"1",38,43,"mitica","pies",1388),
  ("tincidunt orci","#6ba1c4","tela",204,83,50,48,52,49,15,"1",25,42,"mitica","pies",-963),
  ("mi. Aliquam","#efea8b","placas",227,89,49,50,60,49,19,"0",43,43,"mitica","pies",1444),
  ("Aliquam","#8aeab2","tela",239,97,50,45,56,45,16,"0",20,42,"mitica","pies",-298);
