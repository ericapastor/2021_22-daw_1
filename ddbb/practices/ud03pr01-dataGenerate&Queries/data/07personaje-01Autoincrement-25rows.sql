INSERT INTO personaje (id_usuario,id_raza,id_clase,nombre,nivel,tatuajes,procedencia,num_NPC_asesinados,punta_nariz,curva_nariz,puente_nariz,tipo_barbilla,altura_pomulos,forma_mandibula,anchura_mandibula,celeridad_total,parasitar_total,estabilidad_total,velocidad_mov_total,defensa_total,ataque_total,nivel_medio_objeto)
VALUES
  (1,5,3,"Carrillo Florence",61,"1","Elhaun`sharah",50,"redonda","concava","alto","cuadrada","baja","redonda","mediana",67,13,50,31,37,6,154),
  (2,2,2,"Glover Drake",112,"0","Tuyil",450,"afilada","concava","alto","redonda","baja","afilada","estrecha",6,4,87,40,5,97,325),
  (3,5,3,"Garth, Cooper",76,"0","Grupkyt",251,"afilada","concava","alto","cuadrada","baja","cuadrada","ancha",73,3,39,19,17,48,4),
  (4,3,4,"Sophia, Devin, Kessie",105,"0","Ontaru",237,"afilada","plana","alto","afilada","media","redonda","mediana",9,13,56,19,48,83,159),
  (5,3,1,"Melanie Beach",12,"0","Wqrsftmnvcxz",192,"afilada","convexa","bajo","cuadrada","baja","cuadrada","estrecha",67,17,74,47,87,30,164),
  (6,5,3,"Ivy, Unity, Tanya",57,"0","Krrrprrgrrlkmpkah",84,"afilada","concava","bajo","redonda","media","cuadrada","estrecha",26,8,91,45,65,42,181),
  (7,5,4,"Camden, Hayes, Eleanor",92,"1","Ontaru",67,"redonda","plana","bajo","afilada","alta","afilada","mediana",8,10,65,14,81,50,180),
  (8,3,1,"Berk, Brock, Kermit",42,"0","Ontaru",307,"redonda","concava","alto","redonda","media","redonda","ancha",63,13,51,14,7,96,387),
  (9,4,3,"Linus, Byron, Lucas",66,"0","Tuyil",311,"redonda","plana","medio","cuadrada","media","redonda","estrecha",7,7,63,27,23,74,30),
  (10,2,5,"Jenette, Quamar, Patrick",59,"0","Jryushkp",407,"afilada","concava","alto","cuadrada","baja","afilada","mediana",59,2,8,23,27,34,422);
INSERT INTO personaje (id_usuario,id_raza,id_clase,nombre,nivel,tatuajes,procedencia,num_NPC_asesinados,punta_nariz,curva_nariz,puente_nariz,tipo_barbilla,altura_pomulos,forma_mandibula,anchura_mandibula,celeridad_total,parasitar_total,estabilidad_total,velocidad_mov_total,defensa_total,ataque_total,nivel_medio_objeto)
VALUES
  (11,2,1,"French Martena",7,"1","Pyrokas",182,"afilada","concava","bajo","cuadrada","alta","cuadrada","mediana",83,18,19,3,50,37,5),
  (12,3,2,"Kiayada Edward",87,"1","Tuyil",308,"afilada","concava","medio","afilada","media","cuadrada","estrecha",24,13,74,7,29,96,260),
  (13,3,3,"Vasquez Ivan",6,"0","Ontaru",214,"afilada","convexa","medio","cuadrada","alta","afilada","estrecha",41,11,14,37,96,24,11),
  (14,4,2,"Leigh, Francesca, Shaine",28,"1","Lykos",500,"redonda","concava","medio","afilada","baja","afilada","ancha",88,6,67,43,77,87,93),
  (15,2,4,"Yoshio, Micah, Ciara",5,"1","Grupkyt",460,"afilada","convexa","alto","cuadrada","baja","afilada","estrecha",93,15,46,3,21,92,411),
  (16,2,2,"Felix Cline",31,"1","Jiryosh`myan",13,"afilada","plana","alto","redonda","baja","redonda","mediana",12,6,71,29,53,3,339),
  (17,3,5,"Nayda Germaine",68,"0","Ontaru",244,"afilada","concava","bajo","cuadrada","alta","cuadrada","ancha",35,4,46,39,4,24,347),
  (18,4,5,"Autumn Christian",103,"0","Krrrprrgrrlkmpkah",103,"afilada","plana","medio","afilada","baja","redonda","estrecha",11,5,19,42,88,52,210),
  (19,4,2,"Chanda",72,"1","Grupkyt",288,"afilada","concava","alto","redonda","alta","cuadrada","estrecha",69,9,6,2,53,62,299),
  (20,4,5,"Octavius",25,"1","Mo`onshar",95,"redonda","concava","medio","cuadrada","media","redonda","estrecha",95,9,81,32,78,13,48);
INSERT INTO personaje (id_usuario,id_raza,id_clase,nombre,nivel,tatuajes,procedencia,num_NPC_asesinados,punta_nariz,curva_nariz,puente_nariz,tipo_barbilla,altura_pomulos,forma_mandibula,anchura_mandibula,celeridad_total,parasitar_total,estabilidad_total,velocidad_mov_total,defensa_total,ataque_total,nivel_medio_objeto)
VALUES
  (21,2,4,"Savannah",99,"0","Jryushkp",14,"redonda","convexa","alto","cuadrada","media","afilada","ancha",99,5,95,6,74,82,335),
  (22,1,5,"Damon Ferguson",2,"1","Mo`onshar",437,"afilada","concava","alto","afilada","alta","redonda","mediana",54,7,92,49,20,26,153),
  (23,5,2,"Uriel April",20,"0","Mo`onshar",300,"redonda","plana","medio","cuadrada","media","redonda","estrecha",58,10,93,33,82,68,102),
  (24,3,3,"Xandra Adena",25,"0","Tuyil",219,"afilada","plana","alto","afilada","baja","afilada","mediana",1,16,25,33,77,21,172),
  (25,2,4,"Shea Quynn",82,"0","Grupkyt",355,"afilada","convexa","bajo","redonda","media","redonda","ancha",71,8,34,40,93,79,83);
