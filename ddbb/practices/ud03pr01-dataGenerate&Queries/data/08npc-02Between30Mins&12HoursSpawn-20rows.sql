INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (2,3,"Isaac S, Karyn",2,"-4.75","-2.09","-7.72",17275),
  (5,4,"Ethan",9,"-22.86","-32.64","22.50",30502),
  (2,3,"Mason, Cole",5,"14.15","-0.90","-3.68",21220),
  (5,4,"Iona M, Curran",8,"20.60","28.75","-2.49",26396),
  (5,3,"Walter Trevor",2,"-10.08","-2.05","-17.11",15433),
  (4,2,"Ray Natalie",3,"26.04","-35.03","16.98",16865),
  (5,4,"Guinevere A, Hayfa",5,"26.23","-39.84","9.42",40914),
  (2,2,"Wynne Christian",3,"43.61","18.90","6.23",29805),
  (2,4,"Ramos, Kevyn",0,"-18.13","15.62","4.18",20354),
  (4,5,"Erin I, Aspen",1,"-10.87","-16.22","-9.62",18876);
INSERT INTO npc (id_clase,id_raza,nombre,cantidad_xp,coordenadaX_aparicion,coordenadaZ_aparicion,coordenadaY_aparicion,frecuencia_reaparicion)
VALUES
  (3,2,"Myles P, Sonia",6,"-35.31","6.02","1.30",31905),
  (1,3,"Beauchene, Wilma",2,"5.76","-4.75","-2.25",30763),
  (2,2,"Thane R, Herman",1,"21.69","13.71","14.06",3654),
  (2,4,"Thaddeus",0,"15.02","-34.47","-9.22",7711),
  (4,3,"Nelle",8,"-2.61","-35.67","-6.27",26521),
  (3,3,"Dieter C, Moses",8,"-4.74","37.98","-9.51",22470),
  (4,4,"Nguyễn, Madison",3,"3.79","-3.59","-35.69",10631),
  (2,2,"Ignatius Winter",6,"-4.60","-0.49","-26.53",17998),
  (2,3,"Marcia D, Fitzgerald",1,"-5.48","8.48","-4.94",36348),
  (1,4,"Achterberg, David",5,"-7.33","-33.08","11.16",20878);
