use witchcraft;


/*
	1 - Todos los usuarios que tengan personajes que sean ninjas, y si han adoptado
    o no en el ultimo año, mostrando el nombre y familia de sus mascotas en caso de
    haberlo hecho.
*/
	-- 4 tablas
	-- funcion de fecha year()=year()
	-- subconsulta
	-- outer join
	-- funcion de caracteres

select usuario.nombre as usuario, personaje.nombre as personaje, clase.tipo, mascotas_ultimoyear.mascota, mascotas_ultimoyear.familia
from personaje
join usuario on personaje.id_usuario = usuario.id
join clase on personaje.id_clase = clase.id
left join ((select mascota.nombre as mascota, mascota.id_personaje as id_personaje, mascota.familia as familia
from mascota
where year(mascota.fecha_adopcion) = year(curdate())-1) as mascotas_ultimoyear) on personaje.id = mascotas_ultimoyear.id_personaje
where clase.tipo = 'ninja';


/* 
	2 - Nombre de las personas que tengan más de un personaje, que sean mayores de edad
    que lleven equipadas cinco o mas vestimentas por personaje.
*/
-- 2 tablas
-- una vista
-- un group by
-- funcion de fecha between date_sub() and ...
-- una subconsulta

drop view if exists numVestEquipadas;
create view numVestEquipadas as
select usuario.nombre as usuario, personaje.nombre as personaje,
	count(personaje_se_equipa_vestimenta.id_vestimenta) as numVest, usuario.id as id_usuario
from personaje_se_equipa_vestimenta
join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
join personaje on personaje.id = personaje_se_equipa_vestimenta.id_personaje
join usuario on usuario.id = personaje.id_usuario
group by personaje.id
having count(numVest)>=5;

select numVestEquipadas.usuario, numVestEquipadas.personaje, numVestEquipadas.numVest, usuario.fecha_nacimiento
from numVestEquipadas
join usuario on usuario.id = numVestEquipadas.id_usuario
where usuario.fecha_nacimiento not in
(select usuario.fecha_nacimiento from usuario
      where usuario.fecha_nacimiento between date_sub(curdate(), interval 18 year) and curdate());


/*
	3 - Comprueba si es verdad que solamente las arqueras llevan malla,
    el numero de personajes que llevan malla segun la clase a la que pertenecen
    (en caso de no llevar ninguna armadura de malla mostraria 0)
*/
-- 2 tablas
-- una vista
-- funcion de cadena de caracteres
-- group by

drop view if exists pjMalla;
create view pjMalla as
select personaje.id as idPj, clase.id as clase
from personaje
join clase on personaje.id_clase = clase.id
join personaje_se_equipa_vestimenta on personaje_se_equipa_vestimenta.id_personaje = personaje.id
join vestimenta on vestimenta.id = personaje_se_equipa_vestimenta.id_vestimenta
where vestimenta.material = 'malla'
group by personaje.id;

select count(pjMalla.idPj) as personaje, clase.tipo as clase
from pjMalla
join clase on pjMalla.clase = clase.id
group by clase.tipo;


/*
	4 - Todos los de personajes hechiceras que se hayan dado de alta entre el 2015 y el 2018 y si tienen una cuenta Google.
*/
-- 3 tablas
-- funcion de cadenas de caracteres
-- funciones de fecha year() <>=
-- una subconsulta
-- outer join

select usuario.nombre as usuario, usuariosGoogle.cuentaGoogle, usuario.fecha_alta_inscripcion as fecha_alta, clase.tipo, personaje.nombre
from ((select email.email as cuentaGoogle, email.id as email
	from email
	where email.email like '%@google%') as usuariosGoogle)
right join usuario on usuario.id = usuariosGoogle.email
join personaje on personaje.id_usuario = usuario.id
join clase on clase.id = personaje.id_clase
where clase.tipo = 'hechicera'
and year(usuario.fecha_alta_inscripcion) >= '2015'
and year(usuario.fecha_alta_inscripcion) <= '2018';


/*
	5 - Mostrar cuantas habilidades defensivas hay segun su tipo, y si reducen o absorben daño magico
*/
-- 5 tablas
-- group by

select COUNT(habilidad.id) as num_habilidades_defensivas, magia.tipo as tipo_magia, habilidad_defensiva.reduccion_ataque_magico, habilidad_defensiva.absorcion_ataque_magico
from habilidad
join habilidad_defensiva on habilidad.id = habilidad_defensiva.id_habilidad
join clase_habilidad on habilidad.id = clase_habilidad.id_habilidad
join clase_magia on clase_habilidad.id_clase = clase_magia.id_clase
join magia on magia.id = clase_magia.id_magia
group by magia.tipo;


/*
	6 - Cual de las monturas acuaticas espirituales ha sido conseguida mas veces por los personajes y que npc la dropea
*/
-- 3 tablas
-- funciones de cadena de caracteres
-- group by
-- outer join
-- una vista

drop view if exists monturasAcEsp;
create view monturasAcEsp as
select count(personaje.id) as personaje, montura.id as montura, npc.id as npc
from montura
join npc on npc.id = montura.id_npc
left join personaje_posee_montura on montura.id = personaje_posee_montura.id_montura
left join personaje on personaje.id = personaje_posee_montura.id_personaje
where montura.tipo_ambiente = 'acuatica' and montura.tipo_forma = 'espiritual'
group by montura.id;

select max(monturasAcEsp.personaje), montura.nombre as nombre_montura, npc.nombre as nombre_npc
from monturasAcEsp
join montura on monturasAcEsp.montura = montura.id
join npc on monturasAcEsp.npc = npc.id;


/*
	7 - Cuantos personajes hay en guerra contra NPCs dragonoides y de que clase son estos NPCs
*/
-- 5 tablas
-- funcion de cadena de caracteres
-- group by

select count(personaje.id), npc.nombre as NPC_en_guerra, clase.tipo
from personaje
join npc_opina_de_personaje on npc_opina_de_personaje.id_personaje = personaje.id
join npc on npc_opina_de_personaje.id_npc = npc.id
join clase on clase.id = npc.id_clase
join raza on npc.id_raza = raza.id
where npc_opina_de_personaje.opinion_personaje = 'en guerra'
and raza.nombre = 'dragonoide'
group by npc.id;


/*
	8 - Muestra el nombre de todas las mascotas, y en caso de que hayan sido adoptadas desde 2018 hasta la fecha,
    muestra la fecha de adopcion, y en caso de que en esa fecha de adopcion el tipo de la mascota sea electrico,
    muestra su tipo y familia.
    Dada la subida de la luz desde 2018, esta consulta sirve para detectar posibles estafas
    Ademas de para enrevesar una consulta simple y
    probar limites de lo que se puede hacer (renombrar objetos de vistas, y asi)
*/
-- 3 tablas
-- una subconsulta
-- una vista
-- funcion de fecha
-- funcion de cadena de caracteres

drop view if exists mascotas18;
create view mascotas18 as
select personaje.id as pj, mascota.familia as familia, mascota.tipo as tipo, mascota.fecha_adopcion as fecha, mascota.id as idMascota
from personaje
join mascota on mascota.id_personaje = personaje.id
where timestamp(fecha_adopcion) <=current_timestamp() and timestamp(fecha_adopcion) >= '2018-01-01 00:00:00';

select mascota.nombre as nombre_mascota, personaje.nombre as nombre_personaje, mascElectricas.familia, mascElectricas.tipo, mascotas18.fecha
from mascota
left join mascotas18 on mascota.id = mascotas18.idMascota
left join personaje on personaje.id = mascotas18.pj
left join ((select mascotas18.tipo as tipo, mascotas18.familia as familia, mascotas18.idMascota as id from mascotas18
where mascotas18.tipo = 'electrico') as mascElectricas) on mascElectricas.id = mascotas18.idMascota;


/*
	9 - Media de edad de los usuarios que utilizan tortolianos
*/
-- 3 tablas
-- funcion de cadena de caracteres
-- mucho esmero

select round(sum(floor(datediff(curdate(),tortolianosJugadores.fecha) / 365.225)) / count(tortolianosJugadores.tortUser))
as mediaDeEdadDeJugadoresTortolianos
from ((select raza.nombre as raza, personaje.id as tortPj, usuario.id as tortUser, usuario.fecha_nacimiento as fecha
	from raza
	join personaje on raza.id = personaje.id_raza
	join usuario on usuario.id = personaje.id_usuario
	where raza.nombre = 'tortoliano') as tortolianosJugadores);


/*
	10 - Que personajes tienen 4 o mas monturas, quien es su usuario y cuantas horas lleva jugadas en total
*/
-- una vista
-- 2 tablas
-- group by
-- having

drop view if exists totalMonturasPj;
create view totalMonturasPj as
select personaje.nombre as nombrePj, personaje.id_usuario as idUser, count(*) as numMonturas
from personaje
left join personaje_posee_montura on personaje_posee_montura.id_personaje = personaje.id
left join montura on personaje_posee_montura.id_montura = montura.id
group by personaje.id;

select totalMonturasPj.nombrePj, usuario.nombre as nombreUsuario, usuario.horas_jugadas
from totalMonturasPj
join usuario on usuario.id = totalMonturasPj.idUser
group by usuario.id
having count(totalMonturasPj.numMonturas)>=4;


/*
	11 - Telefono y email de todos los usuarios que tengan personajes gladiadoras y
    posean monturas de tipo terrestre conseguidas de 2015 a 2016
*/
-- 7 tablas
-- funciones de cadena de caracteres

select telefono.telefono, email.email
from personaje
join clase on clase.id = personaje.id_clase
join personaje_posee_montura on personaje.id = personaje_posee_montura.id_personaje
join montura on montura.id = personaje_posee_montura.id_montura
join usuario on usuario.id = personaje.id_usuario
join telefono on telefono.id_usuario = usuario.id
join email on email.id_usuario = usuario.id
where clase.tipo = 'gladiadora'
and montura.tipo_ambiente = 'terrenal'
and year(personaje_posee_montura.fecha_obtencion) >= '2015' 
and year(personaje_posee_montura.fecha_obtencion) >= '2016';


/*
	12 - Personajes con un nivel de objeto mayor a 126
*/
-- 3 tablas
-- group by
-- having

select personaje.nombre as nombrePj, round((sum(vestimenta.nivel_objeto) /(count(vestimenta.id)))) as nivelMedioObjeto
from personaje
left join personaje_se_equipa_vestimenta on personaje_se_equipa_vestimenta.id_personaje = personaje.id
join vestimenta on personaje_se_equipa_vestimenta.id_vestimenta = vestimenta.id
group by personaje.id
having round(nivelMedioObjeto)>126
order by nivelMedioObjeto asc;


/*
	13 - Muestra cuantas clases usa cada magia y su antigüedad
*/
-- 3 tablas
-- group by

select magia.tipo as magia, count(clase.id) as num_clases, magia.antiguedad as antigüedad
from clase_magia
join clase on clase.id = clase_magia.id_clase
join magia on magia.id = clase_magia.id_magia
group by magia.id;


/*
	14- Nombre e id de los personajes y sus usuarios que se han inciado peleas con otros personajes y que hayan robado a los NPC asesinandolos
*/
-- 4 tablas
-- funcion de cadenas de caracteres
-- group by

select usuario.nombre as usuario, personaje.nombre as personaje, personaje.id as id_de_personaje,
count(personaje_activo_personaje_pasivo.id_personaje_pasivo) as personas_con_las_que_se_han_peleado
from personaje
join usuario on usuario.id = personaje.id_usuario
join personaje_activo_personaje_pasivo on personaje_activo_personaje_pasivo.id_personaje_activo = personaje.id
join personaje_roba_npc on personaje.id = personaje_roba_npc.id_personaje
where personaje_roba_npc.metodo = 'asesinato'
group by personaje.id;

/*
	15 - Monturas obtenidas aun no recolectadas a dia de hoy, nombre del npc que las dropea,
    y nombre y telefono del usuario que las ha conseguido, asi como nombre de su personaje
*/
-- 6 tablas
-- funcion de fecha

select montura.nombre as nombre_montura_futura, npc.nombre as npc_que_dropea_la_montura,
usuario.nombre as nombre_usuario, telefono.telefono, personaje.nombre as nombre_personaje
from (select montura.id as idM
from montura
join personaje_posee_montura on personaje_posee_montura.id_montura = montura.id
where personaje_posee_montura.fecha_obtencion>curdate()) as monturasFuturas
join montura on montura.id = monturasFuturas.idM
join npc on npc.id = montura.id_npc
join personaje_posee_montura on monturasFuturas.idM = personaje_posee_montura.id_montura
join personaje on personaje.id = personaje_posee_montura.id_personaje
join usuario on usuario.id = personaje.id_usuario
join telefono on usuario.id = telefono.id_usuario;