DROP DATABASE IF EXISTS witchcraft;

DROP USER IF EXISTS 'erica';

CREATE DATABASE witchcraft;

USE witchcraft;

CREATE USER 'erica' IDENTIFIED BY 'nikonorcanon';

GRANT ALL PRIVILEGES ON witchcraft.*TO erica;

CREATE TABLE usuario(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(20),
apellido_1 VARCHAR(20),
apellido_2 VARCHAR(20),
nickname VARCHAR(15),
fecha_alta DATE NOT NULL,
fecha_fin_inscripcion DATE,
tarjeta_bancaria VARCHAR(16) NOT NULL,
paypal VARCHAR(40) UNIQUE,
fecha_nacimiento DATE,
email VARCHAR(50),
horas_jugadas INT UNSIGNED
);

CREATE TABLE telefono(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_usuario INT UNSIGNED NOT NULL,
FOREIGN KEY (id_usuario) REFERENCES usuario(id),
telefono VARCHAR(9)
);

CREATE TABLE raza(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(20),
altura INT UNSIGNED,
ataque_base INT UNSIGNED,
defensa_base INT UNSIGNED,
velocidad_mov_base INT UNSIGNED,
estabilidad_base INT UNSIGNED,
origen_historia VARCHAR(200)
);

CREATE TABLE clase(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
material_armadura ENUM ('placas','malla', 'cuero', 'tela'),
velocidad_movimiento_clase INT UNSIGNED,
tipo ENUM ('gladiadora', 'arquera', 'ninja', 'dríada', 'hechicera') NOT NULL
);

CREATE TABLE personaje(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_usuario INT UNSIGNED NOT NULL,
id_raza INT UNSIGNED NOT NULL,
id_clase INT UNSIGNED NOT NULL,
FOREIGN KEY (id_usuario) REFERENCES usuario(id),
FOREIGN KEY (id_raza) REFERENCES raza(id),
FOREIGN KEY (id_clase) REFERENCES clase(id),
nombre VARCHAR (25) UNIQUE,
nivel INT UNSIGNED DEFAULT 1,
tatuajes TINYINT(1),
procedencia ENUM('Jryushkp', 'Elhaun`sharah','Krrrprrgrrlkmpkah','Ontaru','Pyrokas','Lykos','Mo`onshar','Tuyil','Grupkyt','Wqrsftmnvcxz','Jiryosh`myan') NOT NULL,
direccion_postal VARCHAR(25),
num_NPC_asesinados INT UNSIGNED DEFAULT 0,
punta_nariz ENUM ('redonda', 'afilada'),
curva_nariz ENUM ('plana', 'concava', 'convexa'),
puente_nariz ENUM ('alto', 'medio', 'bajo'),
tipo_barbilla ENUM ('afilada', 'redonda', 'cuadrada'),
altura_pomulos ENUM ('alta', 'medio', 'baja'),
forma_mandibula ENUM ('afilada', 'redonda', 'cuadrada'),
anchura_mandibula ENUM ('ancha', 'estrecha', 'mediana'), 
celeridad_total INT UNSIGNED, 
parasitar_total INT UNSIGNED, 
estabilidad_total INT UNSIGNED, 
velocidad_movimiento_total INT UNSIGNED, 
defensa_total INT UNSIGNED, 
ataque_total INT UNSIGNED, 
nivel_medio_objeto INT UNSIGNED DEFAULT 1
);

CREATE TABLE npc(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_clase INT UNSIGNED NOT NULL,
id_raza INT UNSIGNED NOT NULL,
FOREIGN KEY (id_clase) REFERENCES clase(id),
FOREIGN KEY (id_raza) REFERENCES raza(id),
nombre VARCHAR(20),
cantidad_xp INT UNSIGNED,
coordenadaX_aparicion FLOAT,
coordenadaY_aparicion FLOAT,
coordenadaZ_aparicion FLOAT,
frecuencia_reaparicion TIME
);

CREATE TABLE montura(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_npc INT UNSIGNED,
FOREIGN KEY (id_npc) REFERENCES npc(id),
nombre VARCHAR (20),
tipo_ambiente ENUM ('acuatica', 'aerea', 'terrenal'),
tipo_forma ENUM ('animal', 'mecanica', 'espiritual'),
probabilidad_de_drop INT UNSIGNED DEFAULT 5
);

CREATE TABLE magia(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
antiguedad INT UNSIGNED,
tipo ENUM ('arcana', 'ignea', 'terrena', 'helada', 'floral', 'acuifera','ventarrona','electrica','rocosa')
);

CREATE TABLE mascota(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_personaje INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje) REFERENCES personaje(id),
nombre VARCHAR(20),
nivel INT UNSIGNED DEFAULT 1,
familia VARCHAR(20),
tipo ENUM('fuego','tierra','aire','agua','arcano','electrico','hielo','metal','maquina','roca','hada','fantasma')
);

CREATE TABLE vestimenta(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(20),
color VARCHAR(10),
material ENUM ('placas','malla', 'cuero', 'tela'),
nivel_objeto INT UNSIGNED,
durabilidad INT UNSIGNED,
ataque INT UNSIGNED,
defensa INT UNSIGNED,
origen VARCHAR(20),
estabilidad INT UNSIGNED,
parasitar INT UNSIGNED,
irrompibilidad TINYINT(1),
reduccion_golpe_caida INT UNSIGNED,
velocidad_movimiento INT UNSIGNED,
calidad ENUM ('cutre', 'normal', 'epica', 'legendaria', 'mitica') DEFAULT 'normal',
tipo ENUM ('cabeza', 'hombros', 'pecho', 'wrists', 'manos', 'piernas', 'pies', 'anillo', 'cuello', 'orejas'),
influyen_opinionNPC TINYINT(1)
);

CREATE TABLE habilidad(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR (20),
tiempo_reutilizacion TIME,
nivel_obtencion INT UNSIGNED default 1,
nivel_evolucion INT UNSIGNED,
distancia_area INT UNSIGNED,
epicentro_area ENUM('uno_mismo','objetivo'),
afecta_gcd TINYINT(1),
duracion TIME,
distancia_al_target INT UNSIGNED,
se_canaliza TINYINT(1),
es_interrumpible TINYINT(1),
duracion_canalizacion TIME,
canalizacion_movimiento TINYINT(1)
);

CREATE TABLE habilidad_movimiento(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_habilidad INT UNSIGNED NOT NULL,
FOREIGN KEY (id_habilidad) REFERENCES habilidad(id),
objeto_necesario VARCHAR(15),
cambia_anatomia TINYINT(1),
duracion_ilimitada TINYINT(1),
duracion INT UNSIGNED,
tipo ENUM ('montura_temporal', 'acometida', 'teletransporte', 'sprint', 'transmutación_animal'),
distancia INT UNSIGNED,
modifica_vel_mov_posterior TINYINT(1)
);

CREATE TABLE habilidad_defensiva(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_habilidad INT UNSIGNED NOT NULL,
FOREIGN KEY (id_habilidad) REFERENCES habilidad(id),
reduccion_ataque_fisico INT UNSIGNED,
reduccion_ataque_magico INT UNSIGNED,
absorcio_ataque_fisico INT UNSIGNED,
absorcion_ataque_magico INT UNSIGNED,
inmunidad TINYINT(1)
);

CREATE TABLE habilidad_ataque(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_habilidad INT UNSIGNED NOT NULL,
FOREIGN KEY (id_habilidad) REFERENCES habilidad(id),
ataque_inmediato INT UNSIGNED,
ataque_en_el_tiempo INT UNSIGNED,
tipo ENUM ('magico', 'fisico')
);

CREATE TABLE habilidad_sanacion(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_habilidad INT UNSIGNED NOT NULL,
FOREIGN KEY (id_habilidad) REFERENCES habilidad(id),
sanacion_inmediata INT UNSIGNED,
sanacion_en_el_tiempo INT UNSIGNED,
afectada_por_venenos_o_sangrado TINYINT(1)
);

CREATE TABLE habilidad_disipacion(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_habilidad INT UNSIGNED NOT NULL,
FOREIGN KEY (id_habilidad) REFERENCES habilidad(id),
disipa_aliado ENUM ('veneno', 'maldicion'),
disipa_enemigo ENUM ('potenciacion', 'enfurecimiento')
);

CREATE TABLE habilidad_potenciadora(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_habilidad INT UNSIGNED NOT NULL,
FOREIGN KEY (id_habilidad) REFERENCES habilidad(id),
aumenta_estadistica INT UNSIGNED,
aumenta_vel_ataque INT UNSIGNED,
reduce_casteo INT UNSIGNED
);

CREATE TABLE habilidad_debilitadora(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_habilidad INT UNSIGNED NOT NULL,
FOREIGN KEY (id_habilidad) REFERENCES habilidad(id),
reduce_estadistica INT UNSIGNED,
reduce_vel_ataqu INT UNSIGNED,
aumenta_casteo INT UNSIGNED,
control_masas ENUM ('encarcela', 'adormece', 'paraliza', 'congela')
);

CREATE TABLE personaje_posee_montura(
id_personaje INT UNSIGNED NOT NULL,
id_montura INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje) REFERENCES personaje(id),
FOREIGN KEY (id_montura) REFERENCES montura(id),
tiempo_canalizacion TIME,
PRIMARY KEY(id_personaje,id_montura)
);

CREATE TABLE personaje_utiliza_montura(
id_personaje INT UNSIGNED NOT NULL,
id_montura INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje) REFERENCES personaje(id),
FOREIGN KEY (id_montura) REFERENCES montura(id),
fecha_obtencion DATE,
cantidad INT UNSIGNED DEFAULT 0,
PRIMARY KEY(id_personaje,id_montura)
);
CREATE TABLE personaje_roba_NPC(
id_personaje INT UNSIGNED NOT NULL,
id_npc INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje) REFERENCES personaje(id),
FOREIGN KEY (id_npc) REFERENCES npc(id),
metodo ENUM ('hurto', 'asesinato'),
opnion_posteriorNPC ENUM ('en guerra', 'enemistad', 'neutral', 'amistad', 'en alianza') DEFAULT 'neutral',
PRIMARY KEY (id_personaje,id_npc)
);

CREATE TABLE personaje_se_relaciona_con_npc(
id_personaje INT UNSIGNED NOT NULL,
id_npc INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje) REFERENCES personaje(id),
FOREIGN KEY (id_npc) REFERENCES npc(id),
motivo ENUM ('comprar', 'vender', 'casarse'),
PRIMARY KEY (id_personaje,id_npc)
);

CREATE TABLE personaje_posee_vestimenta(
id_personaje INT UNSIGNED NOT NULL,
id_vestimenta INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje) REFERENCES personaje(id),
FOREIGN KEY (id_vestimenta) REFERENCES vestimenta(id),
cantidad INT UNSIGNED NOT NULL,
PRIMARY KEY (id_personaje,id_vestimenta)
);

CREATE TABLE personaje_se_equipa_vestimenta(
id_personaje INT UNSIGNED NOT NULL,
id_vestimenta INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje) REFERENCES personaje(id),
FOREIGN KEY (id_vestimenta) REFERENCES vestimenta(id),
PRIMARY KEY (id_personaje,id_vestimenta)
);

CREATE TABLE personaje_activo_personaje_pasivo(
id_personaje_activo INT UNSIGNED NOT NULL,
id_personaje_pasivo INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje_activo) REFERENCES personaje(id),
FOREIGN KEY (id_personaje_pasivo) REFERENCES vestimenta(id),
motivo ENUM('escribirse', 'comprar', 'vender', 'pelearse', 'ayudarse', 'contratarse'),
PRIMARY KEY (id_personaje_activo,id_personaje_pasivo)
);

CREATE TABLE personaje_mascota(
id_personaje INT UNSIGNED NOT NULL,
id_mascota INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje) REFERENCES personaje(id),
FOREIGN KEY (id_mascota) REFERENCES mascota(id),
fecha_adopcion DATE,
PRIMARY KEY (id_personaje,id_mascota)
);

CREATE TABLE habilidad_magia(
id_habilidad INT UNSIGNED NOT NULL,
id_magia INT UNSIGNED NOT NULL,
FOREIGN KEY (id_habilidad) REFERENCES habilidad(id),
FOREIGN KEY (id_magia) REFERENCES magia(id),
PRIMARY KEY (id_habilidad,id_magia)
);

CREATE TABLE clase_magia(
id_clase INT UNSIGNED NOT NULL,
id_magia INT UNSIGNED NOT NULL,
FOREIGN KEY (id_clase) REFERENCES clase(id),
FOREIGN KEY (id_magia) REFERENCES magia(id),
PRIMARY KEY (id_clase,id_magia)
);

CREATE TABLE clase_habilidad(
id_habilidad INT UNSIGNED NOT NULL,
id_clase INT UNSIGNED NOT NULL,
FOREIGN KEY (id_habilidad) REFERENCES habilidad(id),
FOREIGN KEY (id_clase) REFERENCES clase(id),
PRIMARY KEY(id_habilidad,id_clase)
);

CREATE TABLE vestimenta_NPC(
id_vestimenta INT UNSIGNED NOT NULL,
id_npc INT UNSIGNED NOT NULL,
FOREIGN KEY (id_vestimenta) REFERENCES vestimenta(id),
FOREIGN KEY (id_npc) REFERENCES npc(id),
num_limite_diario INT UNSIGNED,
PRIMARY KEY(id_vestimenta,id_npc)
);

CREATE TABLE npc_opina_de_personaje(
id_personaje INT UNSIGNED NOT NULL,
id_npc INT UNSIGNED NOT NULL,
FOREIGN KEY (id_personaje) REFERENCES personaje(id),
FOREIGN KEY (id_npc) REFERENCES npc(id),
opinion_personaje ENUM ('en guerra', 'enemistad', 'neutral', 'amistad', 'en alianza') DEFAULT 'neutral',
PRIMARY KEY (id_personaje,id_npc)
);