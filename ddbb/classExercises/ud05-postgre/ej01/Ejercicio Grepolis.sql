-- 1. Hacer el script para PostgreSQL

CREATE TABLE jugador (
  id serial PRIMARY KEY,
  nick text NOT NULL UNIQUE,
  password text NOT NULL,
  nombre text NOT NULL UNIQUE
);

CREATE TABLE ciudad (
  id serial PRIMARY KEY,
  nombre text NOT NULL UNIQUE,
  valor real DEFAULT 1,
  nhabitantes int DEFAULT 0,
  id_jugador int REFERENCES jugador (id)
);

CREATE TABLE tropa (
  id serial PRIMARY KEY,
  nombre text NOT NULL UNIQUE,
  defensa int DEFAULT 10,
  velocidad int DEFAULT 5,
  id_ciudad int REFERENCES ciudad (id)
);

CREATE TABLE barco (
  potencia int DEFAULT 10,
  canones int DEFAULT 1
) INHERITS (tropa);

CREATE TABLE caballeria (
  alcance int DEFAULT 10,
  ataque int DEFAULT 20
) INHERITS (tropa);

CREATE TABLE soldado (
  formacion text,
  armas text[],
  id_ciudad_defensa int REFERENCES ciudad (id)
) INHERITS (tropa);

CREATE TYPE tipo_material AS ENUM ('oro', 'plata', 'madera');

CREATE TABLE material (
  id serial PRIMARY KEY,
  nombre text NOT NULL,
  cantidad int DEFAULT 0,
  precio real DEFAULT 0,
  tipo tipo_material
);

CREATE TABLE edificio (
  id serial PRIMARY KEY,
  nombre text NOT NULL UNIQUE,
  nivel int DEFAULT 1,
  descripcion text,
  id_ciudad int REFERENCES ciudad (id),
  id_material_construido int REFERENCES material (id),
  cantidad_material int DEFAULT 0,
  id_material_generado int REFERENCES material (id)
);

CREATE TABLE edificio_defensa (
  valor_defensivo int DEFAULT 1,
  vida int DEFAULT 10
) INHERITS (edificio);

-- 2. Inserta dos filas más en cada tabla

INSERT INTO jugador (nick, password, nombre)
	VALUES ('superman', MD5('micontrasena'), 'Yo'),
  ('spiderman', MD5('aklsjdklas'), 'Otro'),
  ('sieso', MD5('qwerty'), 'Nombre1'),
  ('solecico', MD5('otrapwd'), 'Nombre2');

INSERT INTO ciudad (nombre, valor, nhabitantes, id_jugador)
    VALUES ('Zaragoza', 10, 700, 1),
      ('Calatayud', 2, 10283, 2),
	  ('Madrid', 5, 654, 4),
	  ('Barcelona', 3, 1200, 3);

INSERT INTO barco (nombre, defensa, velocidad, id_ciudad, potencia, canones)
  VALUES ('Bribón', 12, 12, 1, 10, 3),
    ('Venganza', 12, 12, 1, 2, 4),
	('Oleole', 21, 9, 2, 18, 4),
	('Nuse', 7, 17, 3, 5, 2);

INSERT INTO caballeria (nombre, defensa, velocidad, id_ciudad, alcance, ataque)
    VALUES ('Rocinante', 12, 12, 2, 1, 3),
      ('Veloz', 34, 3, 1, 3, 5),
	  ('Pikachu', 15, 90, 1, 2, 3);

INSERT INTO soldado (nombre, defensa, velocidad, id_ciudad, formacion, armas, id_ciudad_defensa)
    VALUES ('Tim', 12, 12, 1, 'formacion1', '{"espada", "escudo"}', 1),
      ('Joe', 12, 34, 2, 'formacion2', '{"espada", "garrote"}', 1),
	  ('Kim', 12, 12, 3, 'formacion1', '{"baston", "escudo"}', 2),
	  ('Ana', 34, 12, 4, 'formacion2', '{"katana", "katana"}', 1);

INSERT INTO material (nombre, cantidad, precio, tipo)
    VALUES ('madera de pino', 10, 10, 'madera'),
      ('oro', 10, 1000, 'oro'),
	  ('plata de rey', 20, 50, 'plata'),
	  ('bronce palo', 700, 5, 'madera');

INSERT INTO edificio(nombre, descripcion, id_ciudad, id_material_construido, cantidad_material, id_material_generado)
    VALUES ('Ayuntamiento', 'Un edificio', 2, 2, 20, NULL),
      ('Mina', 'Una mina', 1, 1, 20, 2),
	  ('Biblioteca', 'Una biblioteca', 3, 3, 50, null),
	  ('Mina2', 'Otra mina', 4, 4, 20, 3);

INSERT INTO edificio_defensa (nombre, nivel, descripcion, id_ciudad, id_material_construido, cantidad_material, id_material_generado, valor_defensivo, vida)
    VALUES ('Torre', 10, 'Una torre de defensa', 1, 1, 10, NULL, 100, 10),
      ('Muralla', 4, 'La muralla', 1, 1, 40, NULL, 200, 5),
	  ('Torreta', 8, 'Mas pequeña que las torres pero mas veloz', 2, 4, 50, 10, 20, 3),
	  ('Torre de magos', 9, 'Mejor no ponerse a tiro', 3, 2, 40, 1, 100, 7);

-- 3. ¿Cuántas tropas ha generado la ciudad de 'Zaragoza'?
select count(*)
from ciudad
join tropa on tropa.id_ciudad = ciudad.id
where ciudad.nombre = 'Zaragoza';


-- 4. ¿Cuántas armas tiene el soldado 'XXXXXX'?
select array_length(armas, 1) from soldado where soldado.nombre = 'Ana';


-- 5. Nombre de los soldados que tienen el arma 'XXXXXX'?
select nombre from soldado where 'escudo' = any (armas);


-- 6. ¿Cuántos edificios normales (no defensivos)
-- generan madera?
select count(*) from only edificio
join material on edificio.id_material_generado = material.id
where 'madera' = material.tipo;


-- 7. ¿Cuántos edificios defensivos necesitan oro para construirse?
select count(*) from edificio_defensa
join material on edificio_defensa.id_material_construido = material.id
where 'oro' = material.tipo;


-- 8. ¿Cuántos soldados defienden las ciudades del jugador 'XXXX'?
select count(*) from soldado
join ciudad on ciudad.id = soldado.id_ciudad_defensa
join jugador on jugador.id = ciudad.id_jugador
where 'superman' = jugador.nombre;


-- 9. ¿Cuántos edificios defensivos construye cada material?
select count(*) as num_edificios, material.nombre as material
from edificio_defensa
join material on material.id = edificio_defensa.id_material_construido
group by material.id;


-- 10. ¿Cuál es el arma principal de cada soldado? (El arma principal
-- es siempre la primera de su lista)
select soldado.nombre as soldado, armas [1] as arma_principal
from soldado;


-- 11. ¿Cuántas armas tiene cada soldado?
select soldado.nombre as soldado, array_length(armas, 1) as num_armas
from soldado;

