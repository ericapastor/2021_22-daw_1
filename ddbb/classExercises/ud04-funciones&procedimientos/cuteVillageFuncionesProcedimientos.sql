use cute_village;

/*Pag-35 Procedimiento que elimina un avatar y lo añade a la tabla fallecidos
donde aparecerá el nombre de su casa y un campo con la causa de la muerte y
la fecha de fallecimiento. La tabla  fallecidos tienes que crearla por tí mism@
antes de empezar a programar el procedimiento*/
drop procedure if exists crearTablaFallecidos;

delimiter |
create procedure crearTablaFallecidos()
begin
	drop table if exists fallecidos;
    create table fallecidos(
    id int unsigned primary key auto_increment,
    id_avatar int unsigned,
    nombre varchar(50),
    nombre_casa varchar(50),
    fecha_fallecimiento date,
    causa_muerte varchar(100)
    );
end|
delimiter ;

call crearTablaFallecidos();

drop procedure if exists avatarMuere;

delimiter |
create procedure avatarMuere(idAvatar int unsigned,causaMuerte varchar(100))
begin
	declare nombreCasa varchar(50);
    declare nombreAvatar varchar(50);
    set nombreCasa = (select casa.nombre from casa join avatar_casa on avatar_casa.id_casa = casa.id
    join avatar on avatar.id = avatar_casa.id_avatar where avatar.id = idAvatar);
    set nombreAvatar = (select avatar.nombre from avatar where avatar.id = idAvatar);
    
	insert into fallecidos(id_avatar,nombre,nombre_casa,fecha_fallecimiento,causa_muerte)
    values(idAvatar,nombreAvatar,nombreCasa,curdate(),causaMuerte);
    
    delete from avatar_casa where avatar_casa.id_avatar = idAvatar;
    delete from avatar_objeto where avatar_objeto.id_avatar = idAvatar;
    delete from avatar_relacion_avatar where avatar_relacion_avatar.id_avatar_ejerce = idAvatar;
	delete from avatar_relacion_avatar where avatar_relacion_avatar.id_avatar_recibe = idAvatar;
	delete from avatar where avatar.id = idAvatar;
end|
delimiter ;

call avatarMuere(197,'Asesinao');

/*Pag-36 Función que calcule el número de habitaciones de una casa*/
drop function if exists numHabitaciones;

delimiter |
create function numHabitaciones(idCasa int)
returns int
begin
	if(select count(*) from casa where idCasa=casa.id)<1 then
    return -1;
    else
    return (select count(*) from habitacion join casa on casa.id=habitacion.id_casa where idCasa=casa.id group by casa.id);
    end if;
end
|
delimiter ;

/*Pag-37 Utilizando la función anterior crea un procedimiento
que inserte una nueva habitación, la relacione con una casa y
actualice el número de habitaciones de la casa (Transacción)*/
drop procedure if exists insertarHabitacion;
delimiter |
create procedure insertarHabitacion(idCasa int, idHabitacion int, tipoHabitacion varchar(20))
begin
	declare exit handler 
	for sqlexception
		begin
		rollback;
		select 'No se ha podido insertar la habitacion en la casa: ',casa.nombre 
    from casa where casa.id=idCasa;
    end;
	if(select numHabitaciones(idCasa)) = -1 then
		select 'la casa introducida no existe' as casa;
	elseif(tipoHabitacion='habitación' or tipoHabitacion='cocina' or tipoHabitacion='baño' or tipoHabitacion='laboratorio' or tipoHabitacion='garaje' or tipoHabitacion='buhardilla')
    then
		start transaction;
			insert into habitacion(id_casa, tipo)
			values (idCasa,tipoHab);
		update casa
			set n_habitacion=numHabitaciones(idCasa)
			where casa.id=idCasa;
		commit;
    else
		select 'EL tipo de habitacion no esta admitido';
    end if;
end|
delimiter ;