use stellar;

/*Pag-17 Función que devuelve la densidad de un cuerpo celeste calculándola con su masa y radio. Recibe como parámetro su nombre*/
-- densidad = masa/volumen
-- volumen = 4/3 * PI * r^3
drop function if exists densidadCuerpoCeleste;
delimiter |
create function densidadCuerpoCeleste(planeta varchar(100))
returns float
begin
	-- declaro variables
	declare densidad float;
    declare pi float;
    declare volumen float;
    -- doy valor a las variables
    set pi = 3.14;
    set volumen = 4/3 * pi * (select cuerpo_celeste.radio from cuerpo_celeste where cuerpo_celeste.nombre=planeta)^3;
    set densidad = (select cuerpo_celeste.masa from cuerpo_celeste where cuerpo_celeste.nombre=planeta) / volumen;
    -- devuelvo el valor que busco
    return densidad;
end
|
delimiter ;

select densidadCuerpoCeleste('Oikecodi');

-- TRIGGERS

/*Pag-60_61 Triggers para tener actualizada la densidad de un cuerpo celeste*/
-- densidad = m/v

drop trigger if exists densidad_de_nuevo_cuerpo;
delimiter ~
create trigger densidad_de_nuevo_cuerpo before insert on cuerpo_celeste for each row
begin
	set new.densidad=(new.masa/(4/3*new.radio*new.radio*new.radio*3.14));
end;
~
delimiter ;

drop trigger if exists densidadU;
delimiter ~
create trigger densidadU before update on cuerpo_celeste for each row
begin
if(new.radio<>old.radio or new.masa<>old.masa<>new.densidad<>old.densidad)
then set new.densidad=(new.masa/(4/3*new.radio*new.radio*new.radio*3.14));
end if;
end;
~
delimiter ;

/*Pag-62 Haz las modificaciones pertinentes y crea los triggers necesarios para
que cada cuerpo celeste lleve la cuenta de cuántas naves hay aparcadas en él.*/
-- creo la columna de numero de naves
alter table cuerpo_celeste
add column num_naves int not null;
-- creo una funcion para contar el numero de naves segun el id de nave
drop function if exists contarNaves;
delimiter ~
create function contarNaves(idCuerpo int)
returns int
begin
	if(select count(*) from cuerpo_celeste where id = idCuerpo) < 1
	then return -1;
	else
		return (select count(*) from nave 
		where id_cuerpo_celeste = idCuerpo);
	end if;
end;
~
delimiter ;
-- actualizo toda la tabla de cuerpo_celeste para num_nave
update cuerpo_celeste set num_naves = contarNaves(id);
-- creo un trigger para la creacion de una nave
drop trigger if exists nuevaNave;
delimiter ~
create trigger nuevaNave after insert on nave for each row
begin
    update cuerpo_celeste
    set num_naves = (select contarNaves(new.id_cuerpo_celeste))
    where cuerpo_celeste.id = new.id_cuerpo_celeste;
end;
~
delimiter ;
-- trigger para la modificacion de naves
drop trigger if exists modificarNave;
delimiter ~
create trigger modificarNave after update on nave for each row
begin
	declare idCuerpoNuevo int;
	if new.id_cuerpo_celeste <> old.id_cuerpo_celeste then
		update cuerpo_celeste
		set num_naves = (select contarNaves(new.id_cuerpo_celeste))
		where cuerpo_celeste.id = new.id_cuerpo_celeste;
		update cuerpo_celeste
		set num_naves = (select contarNaves(old.id_cuerpo_celeste))
		where cuerpo_celeste.id = old.id_cuerpo_celeste;
    end if;
end;
~
delimiter ;
-- triger para el borrado de naves
drop trigger if exists borrarNave;
delimiter ~
create trigger borrarNave before delete on nave for each row
begin
	update cuerpo_celeste
    set num_naves = num_naves - 1
    where cuerpo_celeste.id = old.id_cuerpo_celeste;
end;
~
delimiter ;

-- insertar
insert into nave(nombre, identificador,id_cuerpo_celeste)
values
('siSoy','123asddasd123','1'),
('siSoy','asdfghjkl','3'),
('sisi','askdalfjkewg','4');
-- updatear
update nave
set nombre = 'siSoy'
where nombre = 'sisi';
update nave
set id_cuerpo_celeste = 2
where id = 3025;
-- borrar
delete from nave where nave.nombre = 'siSoy';

/* Pag-64 Hacer los triggers pertinentes para que cada nave lleve la cuenta de sus tripulantes*/

-- funcion para contar tripulantes
drop function if exists contarTripulantes;
delimiter ~
create function contarTripulantes(idNave int)
returns int
begin
	if (select count(*) from nave_tripulante where id_nave = idNave) < 1
		then return -1;
	else
		return (select count(*) from nave_tripulante where id_nave = idNave);
	end if;
end;
~
delimiter ;

-- actualizo el numero de tripulantes (todo el campo)
update nave set n_tripulantes = contarTripulantes(id);

-- trigger para el insert
drop trigger if exists nave_tripulante_ins;
delimiter ~
create trigger nave_tripulante_ins after insert on nave_tripulante for each row
begin
	update nave
    set n_tripulantes = contarTripulantes(id)
    where id = new.id_nave;
end;
~
delimiter ;

-- hago una prueba: la nave 1 tiene 13 tripulantes, inserto 1 deberia contar 14
insert into nave_tripulante(id_nave,id_tripulante) values (1,1);

-- trigger para el update
drop trigger if exists nave_tripulante_up;
delimiter ~
create trigger nave_tripulante_up after update on nave_tripulante for each row
begin
	if new.id_nave <> old.id_nave then
		update nave
		set n_tripulantes = contarTripulantes(id)
		where id = new.id_nave or id = old.id_nave;
	end if;
end;
~

-- hago una prueba, paso el tripulante 1 de la nave 1 a la 2
-- la nave 1 deberia volver a tener 13 tripulantes, y la 2 pasar de 16 a 17
update nave_tripulante set id_nave = 2 where id_nave = 1 and id_tripulante = 1;

-- trigger para el delete
drop trigger if exists nave_tripulante_del;
delimiter ~
create trigger nave_tripulante_del after delete on nave_tripulante for each row
begin
	update nave
    set n_tripulantes = contarTripulantes(id)
    where id = old.id_nave;
end;
~
delimiter ;

-- hago la prueba, borro el tripulante 1 de la nave 2, que volveria a tener 16 tripulantes
delete from nave_tripulante where id_nave = 2 and id_tripulante = 1;