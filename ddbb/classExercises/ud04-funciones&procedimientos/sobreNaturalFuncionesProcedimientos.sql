use sobrenatural;

/* Pag-53 Traspasar un agente de base y actualizar el número de agentes
en las dos bases involucradas (transacción) */
alter table base
add numAgentes int unsigned;

drop function if exists numAgEnBase;
delimiter |
create function numAgEnBase(idBase int)
returns int
begin
	if (select count(*) from base where base.id = idBase) < 1
		then return -1;
	else
		if (select count(*) from agente where agente.id_base = idBase) < 1
			then return 0;
        else
			return (select count(*) from agente where agente.id_base = idBase);
        end if;
	end if;
end;|
delimiter ;

drop procedure if exists contarAgentes;
delimiter |
create procedure contarAgentes()
begin
	declare exit handler
    for sqlexception
    begin
    rollback;
    end;
    start transaction;
		update base
		set numAgentes = numAgEnBase(id);
    commit;
end;
|
delimiter ;


drop procedure if exists traspasarAgenteDeBase;
delimiter |
create procedure traspasarAgenteDeBase(idAgente int unsigned, idNuevaBase int unsigned)
begin
/*
	declare exit handler
    for sqlexception
    begin
    rollback;
    select 'No se pudo traspasar el agente' as transaccionCancelada;
    end;
    */
    if (select count(*) from agente where agente.id = idAgente) < 1
		then select 'El agente introducido no existe' as agenteNoExiste;
    else
		if (select count(*) from base where base.id = idNuevaBase) < 1
			then select 'La base introducida no existe' as baseNoExiste;
        else
			start transaction;
				update agente
				set id_base = idNuevaBase
				where agente.id = idAgente;
                call contarAgentes();
			commit;
		
        end if;
    end if;
end;|
delimiter ;

call traspasarAgenteDeBase(1,10);

/* Pag-54 Insertar agente con fecha de nacimiento */

drop function if exists isdate;
delimiter |
create function isdate(fecha varchar(10))
returns int
begin
	if (select date(fecha)) is null
    then return 0;
    else return 1;
    end if;
end|;
delimiter ;

drop procedure if exists agenteConFecha;
delimiter |
create procedure agenteConFecha(nombreAgente varchar(20), fechaAgente varchar(20), espec varchar(20), idBase int)
begin
	if (select isdate(fechaAgente)) < 1
    then select 'El formato de fecha no es valido' as fechaNoValida;
    else
		if (select count(*) from base where base.id = idBase) < 1
		then select 'La base introducida no existe' as baseNoExiste;
            if espec <> 'agente de campo' and espec <> 'archivo' and espec <> 'administrativo' and espec <> 'diplomático'
            then select 'La opcion de espcializacion no esta contemplada' as especNoExiste;
			else
				start transaction;
                commit;
			end if;
		end if;
    end if;
end;|
delimiter ;

call agenteConFecha('hola','1999-02-02','asdasd',12);