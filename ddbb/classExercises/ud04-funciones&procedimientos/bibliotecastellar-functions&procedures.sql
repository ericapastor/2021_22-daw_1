use bibliotecastellar;

/* Pag-25 Procedimiento que recibe el id de un socio y
muestra los títulos de los libros que tiene en préstamo*/

drop procedure if exists librosPrestados;
delimiter //
create procedure librosPrestados(idSocio int unsigned)
begin
	select libro.titulo as librosPrestados
    from socio_libro
    join socio on socio.id = socio_libro.id_socio
    join libro on libro.id = socio_libro.id_libro
    where socio_libro.fecha_devolucion is null
    and socio.id = idSocio;
end//
delimiter ;
call librosPrestados(1);

/* Pag-26 Función que dado el nombre de un socio
devuelve cuántos libros tiene en préstamo*/

drop function if exists numLib;
delimiter //
create function numLib(socio varchar(30))
returns int
begin
	return (select count(*) as librosPrestados
		from socio_libro
		join socio on socio.id = socio_libro.id_socio
		join libro on libro.id = socio_libro.id_libro
		where socio_libro.fecha_devolucion is null
		and socio.nombre = socio
		group by socio.id);
end//
delimiter ;
select numLib('Fa  Baxo');

/* Pag-27 Procedimiento que devuelve la lista de títulos
prestados a un socio solo si este tiene más de 20 préstamos*/

drop procedure if exists masDe20Prestamos;
delimiter //
create procedure masDe20Prestamos(socio int unsigned)
begin
	if (select count(*) from socio_libro
			join libro on libro.id = socio_libro.id_libro
			join socio on socio.id = socio_libro.id_socio
			where socio.id = socio) >= 20
		then        
		select libro.titulo as tituloLibros
		from socio_libro
		join libro on libro.id = socio_libro.id_libro
		join socio on socio.id = socio_libro.id_socio
		where socio.id = socio;
    else
    select 'El socio no tiene mas de 20 libros prestados.' as tituloLibros;
    end if;
end//
delimiter ;

-- mas de 20 prestamos
call masDe20Prestamos(1);
-- menos de 20 prestamos
call masDe20Prestamos(23);

/* Pag-28 Procedimiento que recibe los datos de un nuevo socio y lo inserta en la tabla correspondiente*/
drop procedure if exists crearSocio;
delimiter //
create procedure crearSocio(idN int unsigned, nombreN varchar(20), n_prest int unsigned,
calleN varchar(50), puerta int, edadN int)
begin
	if idN not in (select socio.id from socio where socio.id = idN) then
		insert into socio(id, nombre, n_prestados, calle, numero, edad)
        values
        (idN, nombreN, n_prest, calleN, puerta, edadN);
    end if;
end//
delimiter ;

call crearSocio(1001, 'Effy', 78, 'Ribera del Jalon', 6, 15);

/*Pag-29 Función que dado el id de un socio devuelve cuántos libros tiene en préstamo*/
drop function if exists numeroPrestamos;

delimiter $
create function numeroPrestamos(ide int)
returns int
begin
if(select count(*) from socio  where id=ide)<1 
then
return -1;
else
return (select count(*) from libro
join socio_libro on socio_libro.id_libro=libro.id
join socio on socio.id = socio_libro.id_socio
where socio.id=ide and socio_libro.fecha_devolucion is NULL);
end if;
end$
delimiter ;

/*Pag-30 Procedimiento para insertar  un nuevo préstamo a partir de los ids de
socio y libro. Tras crear el préstamo actualizará el número de préstamos del socio
utilizando la función anterior*/
drop procedure if exists nuevoPrestamo;

delimiter |
create procedure nuevoPrestamo(idSocio int unsigned,idLibro int unsigned)
begin
	insert into socio_libro(id_socio,id_libro,fecha_prestamo)
    values(idSocio,idLibro,curdate());
    /* No sumamos ++ a n_prestados en caso de que alguien hubiera hecho un nuevo
    prestamo anteriormente sin comprobar que el campo quedara actualizado. Volvemos
    a contar los prestamos con la funcion anterior, que siempre nos dara un resultado
    correcto. */
    update socio
		set n_prestados = numeroPrestamos(idSocio) -- funcion anterior
        where socio.id = idSocio;
end|
delimiter ;

/*Pag-31 Modifica el procedimiento anterior para que compruebe que el socio y
el libro existen antes de crear el préstamo. Sino, deberá  dar el mensaje de
error más aproximado posible*/
drop procedure if exists nuevoPrestamo;

delimiter |
create procedure nuevoPrestamo(idSocio int unsigned,idLibro int unsigned)
begin
	if(select count(*) from socio where socio.id=idSocio) < 1 and (select count(*) from libro where libro.id=idLibro) < 1 then
		select 'Ninguno de los campos no existe' as noExisten;
        elseif(select count(*) from socio where socio.id=idSocio) < 1 then
        select 'El socio no existe' as noExisteSocio;
        elseif(select count(*) from libro where libro.id=idLibro) < 1 then
        select 'El libro no existe' as noExisteLibro;
    else
	insert into socio_libro(id_socio,id_libro,fecha_prestamo)
    values(idSocio,idLibro,curdate());
    /* No sumamos ++ a n_prestados en caso de que alguien hubiera hecho un nuevo
    prestamo anteriormente sin comprobar que el campo quedara actualizado. Volvemos
    a contar los prestamos con la funcion anterior, que siempre nos dara un resultado
    correcto. */
    update socio
		set n_prestados = numeroPrestamos(idSocio) -- funcion anterior
        where socio.id = idSocio;
	select 'Se ha añadido correctamente' as todoCorrecto;
	end if;
end|
delimiter ;

call nuevoPrestamo(888,3);

/*Pag-66 Hacer las modificaciones necesarias para que se rellene el campo
	edad a partir de la fecha de nacimiento de los socios*/
alter table socio add column fecha_nacimiento date;

drop function if exists fechaNacSocio;
delimiter ~
create function fechaNacSocio(idSocio int)
returns date
begin
	return date_sub(curdate(), interval (select edad from socio where id = idSocio) year);
end;
~
delimiter ;

update socio set fecha_nacimiento = fechaNacSocio(id);

drop function if exists calcularEdad;
delimiter ~
create function calcularEdad(fechaNacimiento date)
returns int
begin
	if (month(fechaNacimiento) = month(curdate())
		and day(fechaNacimiento) > day(curdate()))
		or
        month(fechaNacimiento) > month(curdate())
	then
		return year(curdate()) - year(fechaNacimiento) - 1;
	else
		return year(curdate()) - year(fechaNacimiento);
    end if;
end;
~
delimiter ;

update socio set edad = calcularEdad(fecha_nacimiento);

drop trigger if exists edadSocio;
delimiter ~
create trigger edadSocio before insert on socio for each row
begin
	if new.fecha_nacimiento is not null
			then set new.edad = calcularEdad(new.fecha_nacimiento);
    end if;
end;
~
delimiter ;

drop trigger if exists edadSocioUp;
delimiter ~
create trigger edadSocioUp before update on socio for each row
begin
	if new.fecha_nacimiento <> old.fecha_nacimiento
		then set new.edad = calcularEdad(new.fecha_nacimiento);
	end if;
end;
~
delimiter ;

insert into socio(nombre,fecha_nacimiento) values ('sisoy','1996-08-17');
update socio set fecha_nacimiento = '2015-03-29' where nombre = 'sisoy';
delete from socio where nombre = 'sisoy';
select * from socio;

/*Pag-67 */