use pokimon;

drop function if exists idEspecie;
delimiter |
create function idEspecie(nombreEspecie varchar(50))
-- tenemos que decir lo que devuelve antes de escribir nada mas
returns int 
begin
	return (select id from especie where nombre = nombreEspecie);
end;
|
delimiter ;

-- probamos la funcion:
select idEspecie('chorrimander');

drop procedure if exists motivadoresConEspecie;
delimiter |
create procedure motivadoresConEspecie(nombre varchar(50))
begin
	select motivador.nombre
    from motivador join pokimon
    on motivador.id=pokimon.id_motivador
    where pokimon.id_especie=idEspecie(nombre);
end;
|
delimiter ;

call motivadoresConEspecie('chorrimander');

/*Pag-21 Función que devuelve 1 si una especie dada es de tipo llama, sino devolverá 0. En caso de que la especie no exista devolverá -1*/
drop function if exists esTipoLlama;
delimiter |
create function esTipoLlama(especieElegida varchar(50))
returns int
begin
    if(select count(*) from especie where (especie.tipo1='llama' or especie.tipo2='llama') and especie.nombre=especieElegida)>0 then
    return 1;
    elseif especieElegida not in (select especie.nombre from especie) then
    return -1;
    else
    return 0;
    end if;
end
|
delimiter ;
-- una especie que es tipo llama
select esTipoLlama('chorrimander');
-- una que no es tipo llama
select esTipoLlama('bulbosaurio');
-- una que no existe
select esTipoLlama('cyndaquil');

/*Pag-22 Procedimiento que utilizando la función anterior nos dice mediante texto si la especie de pokimon elegida es tipo llama, no lo es o si no existe*/

drop procedure if exists unaLlamaLlama;
delimiter |
create procedure unaLlamaLlama (unaEspecie varchar(20))
begin
	if (select esTipoLlama(unaEspecie)) = 0 then
    select 'La especie no es tipo llama.' as es_tipo_llama_o_no;
    elseif (select esTipoLlama(unaEspecie)) = 1 then
    select 'La especie es tipo llama.' as es_tipo_llama_o_no;
    else
    select 'La especie no existe.' as es_tipo_llama_o_no;
    end if;
end |
delimiter ;

call unaLlamaLlama('squeeeero');

/* Pag-23 Función que recibe el nombre de un motivador y de una especie
y nos devuelve el número de pokimons que tiene de esa especie. Si el motivador
o la especie no existen devolverá -1*/

drop function if exists numPoki;
delimiter //
create function numPoki (moti varchar(20), esp varchar(20))
returns int
begin
	if moti not in (select motivador.nombre from motivador) or esp not in (select especie.nombre from especie) then
    return -1;
    else
    return (select count(*)
		from motivador
		join pokimon on pokimon.id_motivador = motivador.id
        join especie on pokimon.id_especie = especie.id
        where motivador.nombre = moti and especie.nombre = esp
        );
    end if;
end //
delimiter ;

select numPoki ('moradasdasdo','escuero');