/* Subconsultas */
USE bibliotecacastellar;

/*38-Préstamos realizados hace tres años*/
SELECT id_socio AS socio, id_libro AS libro, fecha_prestamo
FROM socio_libro
WHERE YEAR(fecha_prestamo)=YEAR(CURDATE())-3;

/*39-Libros prestados en los últimos 780 días*/
SELECT id_socio AS socio, id_libro AS libro, fecha_prestamo
FROM socio_libro
WHERE fecha_prestamo BETWEEN DATE_SUB(CURDATE(),INTERVAL 780 DAY) AND CURDATE();

/*40-Autores de los libros prestados desde hace 27 meses*/
SELECT autor.nombre AS autor, libro.titulo AS libro, socio_libro.fecha_prestamo
FROM autor_libro
JOIN libro ON libro.id=autor_libro.id_libro
JOIN autor ON autor.id=autor_libro.id_autor
JOIN socio_libro ON socio_libro.id_libro=libro.id
WHERE fecha_prestamo BETWEEN DATE_SUB(CURDATE(), INTERVAL 27 MONTH) AND CURDATE();

/*41-Socios que hayan tardado más de un año en devolver un préstamo*/
SELECT socio.nombre AS socio, socio_libro.fecha_prestamo, socio_libro.fecha_devolucion
FROM socio_libro
JOIN socio ON socio.id=socio_libro.id_socio
WHERE DATEDIFF(fecha_devolucion,fecha_prestamo)>365
GROUP BY socio.id;

/*44-Ejecutar una consulta dentro de otra, para poder ejecutar su resultado */
SELECT nombre,n_prestados
FROM socio
WHERE n_prestados=(SELECT MAX(n_prestados)
FROM socio);

/*Pag66 - TODOS los socios y el número de prestamos sin devolver*/
SELECT socio.nombre AS socio, COUNT(socio_libro.fecha_devolucion) AS libros_sin_devolver
FROM socio
LEFT JOIN (SELECT id_socio, socio.id AS socio, COUNT(socio_libro.fecha_devolucion) AS libros_no_dev
FROM socio_libro
JOIN socio ON socio.id = socio_libro.id_socio
WHERE socio_libro.fecha_devolucion IS NULL
GROUP BY socio_libro.id_socio) AS libros_no_devueltos
ON socio.id = libros_no_devueltos.id_socio
LEFT JOIN socio_libro ON socio.id = socio_libro.id_socio
GROUP BY socio.id;