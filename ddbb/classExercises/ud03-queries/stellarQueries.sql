
/*
SELECT columnas
FROM tablas
[ WHERE condiciones ]
[ GROUP BY columnas ]
[ HAVING condiciones_de_grupo ]
[ ORDER BY columnas_a_ordenar [ASC|DESC] ]
Utilizaremos ‘*’ para seleccionar todas las columnas de las tablas consultadas.
*/
USE stellar;

/*01-Toda la información sobre todas las naves*/
SELECT * FROM nave;

/*02-Nombre y edad de todas las personas*/
SELECT nombre, edad FROM persona;

/*03-Nombre de todas las especies y su tipo de alimentación*/
SELECT nombre, tipo_alimentacion FROM especie;

/*04-Nombre de todas las especies de vampiros*/
SELECT *
FROM especie
WHERE tipo_alimentacion='vampirismo';

/*05-Nombre y radio de los cuerpos celestes cuyo radio es menor de 200000km*/
SELECT nombre,radio
FROM cuerpo_celeste
WHERE radio <200000;

/*06-Especies que son bacterias y se alimentan de carne*/
SELECT nombre,tipo_alimentacion,reino
FROM especie
WHERE tipo_alimentacion='carnívoro' and reino='Bacteria';

/*07-Nombre de la nave con el id 2980*/
SELECT nombre
FROM nave
WHERE id=2980;

/*08-Nombre y edad de las personas mayores de 50 años(584)*/ 
select nombre,edad
from persona
where edad>50;

/*09-Naves de carga cuya capacidad está comprendida entre los 100 y los 1000kg*/
SELECT capacidad_carga, nombre
FROM nave
WHERE capacidad_carga > 100 AND capacidad_carga < 10000;

/*10-Número de personas nacidas en cada cuerpo celeste */
select persona.id_cuerpo_celeste, count(*)
from persona
group by persona.id_cuerpo_celeste;

/*11-¿Cuántos cuerpos celestes hay de cada tipo?*/
SELECT cuerpo_celeste.tipo, COUNT(*)
FROM cuerpo_celeste
GROUP BY cuerpo_celeste.tipo;

/*12-¿Cuántos cuerpos celestes hay de cada tipo con un radio menor que 1700000?*/
SELECT cuerpo_celeste.tipo, COUNT(*)
FROM cuerpo_celeste
WHERE cuerpo_celeste.radio<1700000
GROUP BY cuerpo_celeste.tipo;

/*13-Densidad máxima de cada tipo de cuerpo celeste*/
SELECT cuerpo_celeste.tipo, MAX(densidad)
FROM cuerpo_celeste
GROUP BY cuerpo_celeste.tipo;

/*14-Nombre de las personas y del cuerpo celeste donde han nacido*/
/*AS cambia el nombre bajo el que aparecera la informacion*/
SELECT persona.nombre AS persona, cuerpo_celeste.nombre AS cuerpo_celeste
FROM persona
JOIN cuerpo_celeste ON persona.id_cuerpo_celeste=cuerpo_celeste.id;

/*15-Número de personas nacidas en cada cuerpo celeste y el nombre del cuerpo celeste */
SELECT cuerpo_celeste.nombre, COUNT(*) AS numero_personas
FROM cuerpo_celeste
JOIN persona ON persona.id_cuerpo_celeste=cuerpo_celeste.id
GROUP BY cuerpo_celeste.id;

/*16-Número de especies registradas para cada tipo de alimentación*/
SELECT especie.id, especie.tipo_alimentacion, COUNT(*) AS numero_especies
FROM especie
GROUP BY especie.tipo_alimentacion;

/*17-Especie y nombre de cada persona*/
SELECT especie.nombre AS especie, persona.nombre AS nombre
FROM especie
JOIN especie_persona ON especie_persona.id_especie=especie.id
JOIN persona ON especie_persona.id_persona=persona.id;

/*18-Nombre de especie, alimentación y cuerpo espacial que habita cada especie*/
SELECT especie.nombre AS especie, especie.tipo_alimentacion AS tipo_de_alimentacion, cuerpo_celeste.nombre AS cuerpo_celeste
FROM especie_cuerpo_celeste
JOIN especie ON especie.id=especie_cuerpo_celeste.id_especie
JOIN cuerpo_celeste ON cuerpo_celeste.id=especie_cuerpo_celeste.id_cuerpo_celeste;

/*19-Direcciones .link de las personas que han tripulado la USS Butterfly*/
SELECT persona.nombre AS persona, direcciones.direccion AS link, nave.nombre AS nave
FROM direcciones
JOIN persona ON direcciones.id_persona=persona.id
JOIN tripulante ON tripulante.id_persona=persona.id
JOIN nave_tripulante ON tripulante.id=nave_tripulante.id_tripulante
JOIN nave ON nave.id=nave_tripulante.id_nave
WHERE nave.nombre='Butterfly';

/*20-Nombre y número de veces que cada persona ha formado parte de una tripulación*/
SELECT persona.nombre AS persona, COUNT(tripulante.id_persona) AS veces_tripulando
FROM tripulante
JOIN persona ON persona.id=tripulante.id_persona
GROUP BY tripulante.id_persona;

/*21-Número de naves estacionadas en territorio de cada coalición (nombre)*/
SELECT coalicion.nombre AS coalicion, COUNT(*) AS numero_naves
FROM coalicion_cuerpo_celeste
JOIN coalicion
JOIN cuerpo_celeste
JOIN nave ON nave.id_cuerpo_celeste=cuerpo_celeste.id
GROUP BY coalicion.nombre;

/*22-Número de personas nacidas en cada estrella*/
SELECT cuerpo_celeste.nombre AS estrella,COUNT(*) AS personas_nacidas
FROM cuerpo_celeste
JOIN estrella ON estrella.id_cuerpo_celeste=cuerpo_celeste.id
JOIN persona ON persona.id_cuerpo_celeste=cuerpo_celeste.id
GROUP BY estrella.id;

/*23-Coalicones con más de 23 naves estacionadas en ellas(en su territorio)*/
SELECT coalicion.nombre AS coalicion,COUNT(*) AS numero_naves
FROM coalicion_cuerpo_celeste
JOIN coalicion ON coalicion_cuerpo_celeste.id_coalicion=coalicion.id
JOIN cuerpo_celeste ON coalicion_cuerpo_celeste.id_cuerpo_celeste=cuerpo_celeste.id
JOIN nave ON nave.id_cuerpo_celeste=cuerpo_celeste.id
GROUP BY coalicion
HAVING COUNT(numero_naves)>23;

/*24-Nombre de especie y media de edad de las personas que pertenecen a esa especie,
solo si la media de edad es superior a 200 años*/
SELECT especie.nombre AS especie,AVG(persona.edad) AS edad_media
FROM especie
JOIN especie_persona ON especie.id=especie_persona.id_especie
JOIN persona ON persona.id=especie_persona.id_persona
GROUP BY especie
HAVING AVG(persona.edad)>200;

/*25-Naves de una coalición que están aparcadas en el territorio de otra coalición*/
SELECT coalicion.nombre AS coalicion,cuerpo_celeste.nombre AS territorio,nave.nombre AS nave
FROM coalicion_cuerpo_celeste
JOIN coalicion ON coalicion_cuerpo_celeste.id_coalicion=coalicion.id
JOIN cuerpo_celeste ON coalicion_cuerpo_celeste.id_cuerpo_celeste=cuerpo_celeste.id
JOIN nave ON cuerpo_celeste.id=nave.id_cuerpo_celeste
WHERE nave.id_coalicion<>coalicion_cuerpo_celeste.id_coalicion;

/*26-Número de tripulantes para las naves de la consulta anterior*/
SELECT nave.nombre AS nave,cuerpo_celeste.nombre AS territorio,coalicion.nombre AS coalicion,COUNT(*) AS tripulantes
FROM coalicion_cuerpo_celeste
JOIN coalicion ON coalicion_cuerpo_celeste.id_coalicion=coalicion.id
JOIN cuerpo_celeste ON coalicion_cuerpo_celeste.id_cuerpo_celeste=cuerpo_celeste.id
JOIN nave ON cuerpo_celeste.id=nave.id_cuerpo_celeste
JOIN nave_tripulante ON nave_tripulante.id_nave=nave.id
/* esta columna no es necesaria porque al solo necesitar contar los
tripulantes basta con unir a la tabla de union donde se halla su id.
JOIN tripulante ON tripulante.id=nave_tripulante.id_tripulante*/
WHERE coalicion_cuerpo_celeste.id_coalicion<>nave.id_coalicion
GROUP BY nave.id;

/*27-Para cada persona, especie a la que pertenece y cuerpo celeste de nacimiento*/
SELECT persona.nombre AS persona, especie.nombre AS especie, cuerpo_celeste.nombre AS cuerpo_celeste_de_nacimiento
FROM especie_persona
JOIN persona ON  persona.id=especie_persona.id_persona
JOIN especie ON especie.id=especie_persona.id_especie
JOIN cuerpo_celeste ON cuerpo_celeste.id=persona.id_cuerpo_celeste;

/*28-¿Cuántos tripulantes de cada especie tripulan las distintas naves?*/
SELECT especie.nombre AS especie,nave.nombre as nave,COUNT(*) AS numero_de_tripulantes
FROM nave
JOIN nave_tripulante ON nave.id=nave_tripulante.id_nave
JOIN tripulante ON tripulante.id=nave_tripulante.id_tripulante
JOIN persona ON persona.id=tripulante.id_persona
JOIN especie_persona ON persona.id=especie_persona.id_persona
JOIN especie ON  especie.id=especie_persona.id_especie
GROUP BY especie.id,nave.id;

/*29-Especies que no son herbívoro ni geovoro*/
SELECT especie.nombre AS especie,especie.tipo_alimentacion
FROM especie
WHERE tipo_alimentacion<>'herbívoro' AND tipo_alimentacion<>'geovoro';

/*30-Personas que pertenecen a las especies anteriores*/
SELECT especie.nombre AS especie,especie.tipo_alimentacion, persona.nombre AS persona
FROM especie
JOIN especie_persona ON especie.id=especie_persona.id_especie
JOIN persona ON persona.id=especie_persona.id_persona
WHERE tipo_alimentacion<>'herbívoro' AND tipo_alimentacion<>'geovoro';

/*31-Nombre y especie de las personas con un doctorado*/
SELECT persona.nombre AS persona,especie.nombre AS especie,tripulante.cargo AS cargo
FROM especie_persona
JOIN especie ON especie.id=especie_persona.id_especie
JOIN persona ON persona.id=especie_persona.id_persona
JOIN tripulante ON tripulante.id_persona=persona.id
WHERE tripulante.cargo IS NOT NULL;

/*32-Nombre y tipo de las naves que están en vuelo*/
SELECT nave.nombre,nave.tipo
FROM nave
WHERE nave.id_cuerpo_celeste IS NULL;

/*33-Número estimado de especímenes vivos del universo*/
SELECT SUM(especie.n_estimado)
FROM especie;

/*34-Nombre de las personas originarias de cuerpos espaciales que orbitan estrellas*/
SELECT persona.nombre as persona,cuerpo_celeste.nombre as cuerpo_orbitador,estrella.tipo AS estrella_orbitada
FROM persona
JOIN cuerpo_celeste ON cuerpo_celeste.id=persona.id_cuerpo_celeste
JOIN cuerpo_celeste_orbita_cuerpo_celeste ON cuerpo_celeste_orbita_cuerpo_celeste.id_cuerpo_celeste_orbitador=cuerpo_celeste.id
JOIN estrella ON cuerpo_celeste_orbita_cuerpo_celeste.id_cuerpo_celeste_orbitado=estrella.id_cuerpo_celeste;
/*
SELECT persona.nombre as persona,cuerpo_celeste.nombre as cuerpo_orbitador,estrella.tipo AS estrella_orbitada
from persona
JOIN cuerpo_celeste ON cuerpo_celeste.id=persona.id_cuerpo_celeste
JOIN cuerpo_celeste_orbita_cuerpo_celeste ON cuerpo_celeste_orbita_cuerpo_celeste.id_cuerpo_celeste_orbitador=cuerpo_celeste.id
JOIN estrella ON cuerpo_celeste_orbita_cuerpo_celeste.id_cuerpo_celeste_orbitado=estrella.id_cuerpo_celeste;
*/

/*35-Especies ordenadas por tipo de alimentación*/
SELECT especie.nombre, especie.tipo_alimentacion
FROM especie
ORDER BY especie.tipo_alimentacion ASC;

/*36-Media de edad de las personas registradas*/
SELECT AVG(edad)
FROM persona;

/*37-ID de la última persona registrada*/
SELECT MAX(id) AS ultimo_id
FROM persona;

/*38_41-En otro archivo, por ser de la bibliotecaestelar*/

/*42-Edad máxima de las personas de cada especie*/
select especie.nombre as nombre_especie, max(persona.edad) as edad_maxima
from especie
join especie_persona on especie.id=especie_persona.id_especie
join persona on especie_persona.id_persona=persona.id
group by especie.nombre;
/*Mi ejercicio*/
SELECT especie.nombre AS especie, persona.nombre AS persona, max(persona.edad)
FROM especie_persona
JOIN especie ON especie_persona.id_especie=especie.id
JOIN persona ON especie_persona.id_persona=persona.id
GROUP BY persona.edad;

/*43-Nombre y cuerpo celeste de nacimiento de los nobles*/
SELECT persona.nombre, cuerpo_celeste.nombre
	FROM persona
	JOIN cuerpo_celeste ON cuerpo_celeste.id=persona.id_cuerpo_celeste
	WHERE persona.nombre LIKE 'Lord%' 
    or persona.nombre like 'Sr%'
    or persona.nombre like 'Lady%'
    or persona.nombre like 'Sir%';

/*44-En otro archivo, por ser de la bibliotecaestelar*/

/*45-Nombre de la ultima persona registrada */
SELECT nombre, id
FROM persona
WHERE id=(SELECT MAX(id) FROM persona);

/*46-Nombre y edad de la persona más longeva*/
SELECT nombre, edad
FROM persona
WHERE edad=(SELECT MAX(edad) FROM persona);

/*47-Especies con menos sujetos vivos estimados
que la media de las especies*/
SELECT n_estimado AS sujetos_vivos, nombre AS especie
FROM especie
WHERE n_estimado<
(SELECT AVG(n_estimado)
FROM especie);

/*48-Cuerpos celestes habitados por especies con más
sujetos vivos que la media de las especies*/
SELECT cuerpo_celeste.nombre AS cuerpo_estelar, especie.n_estimado AS ejemplares_vivos, especie.nombre AS especie
FROM especie
JOIN especie_cuerpo_celeste ON especie.id=especie_cuerpo_celeste.id_especie
JOIN cuerpo_celeste ON cuerpo_celeste.id=especie_cuerpo_celeste.id_cuerpo_celeste
WHERE especie.n_estimado>(SELECT AVG(n_estimado) FROM especie);

/*49-Personas que nunca han formado parte
de una tripulación*/
SELECT nombre AS persona
FROM persona
WHERE persona.id NOT IN (SELECT tripulante.id_persona
FROM tripulante);

/*50-Especie con más personas*/
USE Stellar;
SELECT nombre,id
FROM persona
WHERE persona.id=(SELECT MAX(persona.id) FROM persona);

/*51-Nombre y edad de la persona más longeva*/
SELECT nombre,edad
FROM persona
WHERE persona.edad=(SELECT MAX(persona.edad) FROM persona);

/*52-Especies con menos sujetos vivos estimados que la media de las especies*/
USE Stellar;
SELECT especie.nombre,n_estimado
FROM especie
WHERE especie.n_estimado<(SELECT AVG(especie.n_estimado) FROM especie);

/*53-Cuerpos celestes habitados por especies con más sujetos vivos que la media de sujetos vivos de todas las especies*/
USE Stellar;
SELECT cuerpo_celeste.nombre AS cuerpo_celeste,especie.nombre AS especie
FROM especie_cuerpo_celeste
JOIN especie ON especie.id=especie_cuerpo_celeste.id_especie
JOIN cuerpo_celeste ON cuerpo_celeste.id=especie_cuerpo_celeste.id_cuerpo_celeste
WHERE especie.n_estimado>(SELECT AVG(n_estimado) FROM especie)
ORDER BY cuerpo_celeste;

/*54-Personas que nunca han formado parte de una tripulación*/
SELECT persona.nombre
FROM persona
WHERE persona.id NOT IN(
SELECT id_persona
FROM tripulante
JOIN persona ON persona.id=tripulante.id_persona)
ORDER BY persona.nombre ASC;

/*55-Especie con más personas*/
USE Stellar;
SELECT especie.nombre, COUNT(persona.id)
FROM especie_persona
JOIN especie ON especie.id=especie_persona.id_especie
JOIN persona ON persona.id=especie_persona.id_persona
GROUP BY especie.id
HAVING COUNT(persona.id)=
(SELECT MAX(total) 
FROM(
SELECT COUNT(persona.id) AS total
FROM especie_persona
JOIN especie ON especie.id=especie_persona.id_especie
JOIN persona ON persona.id=especie_persona.id_persona
GROUP BY especie.id) AS counting);

/*56-Nombre del último doctor o médico despedido de una tripulación (Sr. Qajixo)*/
SELECT persona.nombre
FROM persona
JOIN tripulante ON persona.id=tripulante.id_persona
WHERE tripulante.fecha_fin=(
SELECT MAX(tripulante.fecha_fin) 
FROM tripulante 
WHERE tripulante.cargo='Médico');

/*57-Nombre del cuerpo celeste con más personas nacidas en él, indicando el número de personas*/
SELECT cuerpo_celeste.nombre AS cuerpo_celeste, COUNT(persona.id) AS personas_nacidas
FROM persona
JOIN cuerpo_celeste ON cuerpo_celeste.id=persona.id_cuerpo_celeste
GROUP BY cuerpo_celeste.nombre
HAVING COUNT(persona.id)=(SELECT MAX(births) FROM
(SELECT COUNT(persona.id) AS births
FROM persona
JOIN cuerpo_celeste ON cuerpo_celeste.id=persona.id_cuerpo_celeste
GROUP BY cuerpo_celeste.nombre) AS counting);


/*58-Page_51Nombre y tipo de todos los cuerpos celestes y nombre de las personas nacidas en ellos*/
SELECT cuerpo_celeste.nombre AS cuerpo_celeste, cuerpo_celeste.tipo AS tipo, persona.nombre AS persona
FROM persona
RIGHT JOIN cuerpo_celeste ON cuerpo_celeste.id=persona.id_cuerpo_celeste;

/*59-Page_51-Nombre de TODAS las personas y cuerpo celeste en el que han nacido*/
SELECT cuerpo_celeste.nombre AS cuerpo_celeste, cuerpo_celeste.tipo AS tipo,persona.nombre AS persona
FROM persona
LEFT JOIN cuerpo_celeste ON cuerpo_celeste.id=persona.id_cuerpo_celeste;

/*60-Page_51-¿Cuántas personas han nacido en cada cuerpo celeste? (Incluidos aquellos en los que no ha nacido nadie)*/
SELECT cuerpo_celeste.nombre AS cuerpo_celeste, COUNT(persona.id) AS personas_nacidas
FROM cuerpo_celeste
LEFT JOIN persona ON persona.id_cuerpo_celeste=cuerpo_celeste.id
GROUP BY cuerpo_celeste.nombre;

/*61-Page_52-Todos los cuerpos (nombre) celestes e información de las especies (nombre, reino, alimentación) que viven en ellos*/
SELECT cuerpo_celeste.nombre AS cuerpo_celeste, especie.nombre AS especie, especie.reino AS reino_especie, especie.tipo_alimentacion AS alimentacion_especie
FROM cuerpo_celeste
LEFT JOIN especie_cuerpo_celeste ON especie_cuerpo_celeste.id_cuerpo_celeste=cuerpo_celeste.id
LEFT JOIN especie ON especie_cuerpo_celeste.id_especie=especie.id;

/*62-Page_53-Todos los cuerpos (nombre) celestes e información de las especies (nombre, reino) carnívoras que viven en ellos*/
select cuerpo_celeste.nombre as cuerpo_celeste, carnivoros.nombre, carnivoros.reino, carnivoros.alimentacion
    from cuerpo_celeste
    left join (select id_cuerpo_celeste, especie.id as id, especie.nombre as nombre, especie.reino as reino, especie.tipo_alimentacion as alimentacion 
			   from especie join especie_cuerpo_celeste
               on especie.id=especie_cuerpo_celeste.id_especie
			   where especie.tipo_alimentacion='carnívoro') as carnivoros
on cuerpo_celeste.id=carnivoros.id_cuerpo_celeste;


/*63-Page_56-Vista de todos los cuerpos celestes que orbitan estrellas */
CREATE VIEW CuerpoCelestes_Orbitan_Estrella
AS
SELECT cuerpo_celeste.nombre AS nombre, cuerpo_celeste_orbita_cuerpo_celeste.id_cuerpo_celeste_orbitador AS cuerpoCeleste
FROM cuerpo_celeste_orbita_cuerpo_celeste
JOIN estrella ON estrella.id_cuerpo_celeste=cuerpo_celeste_orbita_cuerpo_celeste.id_cuerpo_celeste_orbitado
JOIN cuerpo_celeste ON cuerpo_celeste.id=cuerpo_celeste_orbita_cuerpo_celeste.id_cuerpo_celeste_orbitador;

/*64-Page_56-Nombre de las personas que han nacido en cuerpos celestes que orbitan estrellas*/
SELECT CuerpoCelestes_Orbitan_Estrella.nombre AS cuerpo_celeste ,persona.nombre AS persona
FROM CuerpoCelestes_Orbitan_Estrella
JOIN persona ON CuerpoCelestes_Orbitan_Estrella.cuerpoCeleste=persona.id_cuerpo_celeste;

/*65.1-Page_58-Nombre del cuerpo_celeste con más personas nacidas en él*/
CREATE VIEW contarPersonas
AS 
SELECT cuerpo_celeste.nombre AS cuerpo, cuerpo_celeste.id AS id_cuerpo, COUNT(persona.id) AS personas
FROM persona
RIGHT JOIN cuerpo_celeste ON cuerpo_celeste.id=persona.id_cuerpo_celeste
GROUP BY cuerpo_celeste.id;

/*65.2*/
SELECT contarPersonas.cuerpo, contarPersonas.personas
FROM contarPersonas
WHERE contarPersonas.personas=(SELECT MAX(contarPersonas.personas) FROM contarPersonas);

/**/