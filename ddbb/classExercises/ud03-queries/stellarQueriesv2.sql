use stellar;

/*Número de personas nacidas en cada estrella*/
select count(*)
from persona
join cuerpo_celeste on cuerpo_celeste.id = persona.id_cuerpo_celeste
join estrella on estrella.id_cuerpo_celeste = cuerpo_celeste.id
group by estrella.id;

/*Coalicones con más de 23 naves estacionadas en ellas (en su territorio)*/
select coalicion.nombre
from coalicion_cuerpo_celeste
join coalicion on coalicion.id = coalicion_cuerpo_celeste.id_coalicion
join cuerpo_celeste on cuerpo_celeste.id = coalicion_cuerpo_celeste.id_cuerpo_celeste
join nave on nave.id_cuerpo_celeste = cuerpo_celeste.id
group by coalicion.id
having count(coalicion.id) > 23;

/*Nombre de especie y media de edad de las personas que pertenecen a esa especie, solo si la media de edad es superior a 200 años*/
select especie.nombre, avg(persona.edad)
from especie_persona
join especie on especie.id = especie_persona.id_especie
join persona on persona.id = especie_persona.id_persona
group by especie.id
having avg(persona.edad) > 200;

/*Naves de una coalición que están aparcadas en el territorio de otra coalición*/
select nave.nombre
from nave
join coalicion on coalicion.id = nave.id_coalicion
join cuerpo_celeste on cuerpo_celeste.id = nave.id_cuerpo_celeste
join coalicion_cuerpo_celeste on coalicion.id = coalicion_cuerpo_celeste.id_coalicion
where nave.id_cuerpo_celeste <> coalicion_cuerpo_celeste.id_cuerpo_celeste
group by nave.id;
	-- hecho en clase asi, dan resultados distintos:
	SELECT nave.nombre, coalicion.nombre AS terreno_aterrizado
	FROM coalicion_cuerpo_celeste
	JOIN coalicion ON coalicion_cuerpo_celeste.id_coalicion=coalicion.id
	JOIN cuerpo_celeste ON coalicion_cuerpo_celeste.id_cuerpo_celeste = cuerpo_celeste.id
	JOIN nave ON nave.id_cuerpo_celeste=cuerpo_celeste.id
	WHERE nave.id_coalicion<>coalicion_cuerpo_celeste.id_coalicion;
    
