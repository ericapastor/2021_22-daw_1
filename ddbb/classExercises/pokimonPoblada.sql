-- MySQL dump 10.16  Distrib 10.1.35-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: pokimon
-- ------------------------------------------------------
-- Server version	10.1.35-MariaDB
drop database if exists pokimon;
create database pokimon;
use pokimon;



/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ataque`
--

DROP TABLE IF EXISTS `ataque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ataque` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) DEFAULT NULL,
  `ataque` int(11) DEFAULT NULL,
  `usos` int(11) DEFAULT NULL,
  `estado` enum('siesta','despistao','chamuscado','resbaloso','asustado') DEFAULT NULL,
  `tipo` enum('espíritu','llama','líquido','césped','planeo','draco','hierro','unicornio','garrampa') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ataque`
--

LOCK TABLES `ataque` WRITE;
/*!40000 ALTER TABLE `ataque` DISABLE KEYS */;
INSERT INTO `ataque` VALUES (1,'rescoldos',9,25,NULL,'llama'),(2,'jabón a los ojos',5,25,'resbaloso','líquido'),(3,'lanza pipas',8,30,NULL,'césped'),(4,'a que te rajo',15,4,'asustado','espíritu'),(5,'frotar globo en el pelo',3,30,NULL,'garrampa'),(6,'golpe de remo',15,20,NULL,'líquido'),(8,'super tajo unicornio',145,3,'chamuscado','unicornio'),(9,'hiper tajo draco',50,28,'despistao','draco'),(10,'puño césped',31,7,'resbaloso','césped'),(11,'super puño',149,32,'asustado','garrampa'),(12,'baile líquido',32,2,'resbaloso','líquido'),(13,'mega rayo ',73,NULL,'resbaloso',NULL),(14,'super patada',99,8,'chamuscado','planeo'),(15,'hiper baile garrampa',63,6,'asustado','garrampa'),(16,'rayo',NULL,31,NULL,'draco'),(17,'despistao plus',133,24,'despistao','planeo'),(18,'mega cabezazo',32,11,'resbaloso','espíritu'),(19,'super patada unicornio',17,20,'siesta','unicornio'),(20,'super abrazo llama',33,5,'chamuscado','llama'),(21,'contoneo',82,4,'chamuscado','espíritu'),(22,'tajo hierro',29,28,'asustado','hierro'),(23,'cabezazo',49,20,'asustado','draco'),(24,'tajo planeo',150,3,'resbaloso','planeo'),(25,'abrazo',56,24,'chamuscado','unicornio'),(26,'patada',103,15,'resbaloso','planeo'),(27,'mega puño unicornio',43,15,'asustado','unicornio'),(28,'siesta',74,23,'asustado','líquido'),(29,'patada líquido',14,16,'asustado','líquido'),(30,'baile',137,35,'despistao','draco'),(31,'hiper cabezazo planeo',128,35,'despistao','planeo'),(32,'siesta unicornio',27,2,'chamuscado','unicornio'),(33,'mega tajo',21,23,'asustado','garrampa'),(34,'mega puño garrampa',NULL,31,NULL,'garrampa'),(35,'puño',11,19,'despistao','unicornio'),(36,'contoneo garrampa',20,30,'asustado','garrampa'),(37,'tajo líquido',83,20,'chamuscado','líquido'),(38,'puño unicornio',116,3,'asustado','unicornio'),(39,'',NULL,9,NULL,'garrampa'),(40,'super abrazo garrampa',49,18,'asustado','garrampa'),(41,'hiper contoneo',82,1,'resbaloso','líquido'),(42,'mega baile llama',73,24,'asustado','llama'),(43,'hiper abrazo',142,26,'asustado','draco'),(44,'super cabezazo',70,34,'siesta','garrampa'),(45,'hiper baile unicornio',140,30,'asustado','unicornio'),(46,'asustado',59,33,'asustado','unicornio'),(47,'hiper baile',109,32,'asustado','unicornio'),(48,'super baile líquido',67,11,'siesta','líquido'),(49,'super tajo',NULL,4,NULL,'llama'),(50,'mega siesta planeo',120,29,'resbaloso','planeo'),(51,'abrazo planeo',141,6,'resbaloso','planeo'),(52,'puño ',137,NULL,'chamuscado',NULL),(53,'hiper tajo',105,29,'asustado','hierro'),(54,'siesta ',102,NULL,'asustado',NULL),(55,'puño garrampa',18,18,'chamuscado','garrampa'),(56,'super contoneo',88,6,'resbaloso','hierro'),(57,'contoneo espíritu',122,20,'asustado','espíritu'),(58,'puño hierro',77,5,'asustado','hierro'),(59,'hiper cabezazo draco',NULL,2,NULL,'draco'),(60,'baile hierro',61,17,'resbaloso','hierro'),(61,'rayo unicornio',47,21,'chamuscado','unicornio'),(62,'hiper rayo hierro',139,34,'chamuscado','hierro'),(63,'rayo líquido',88,6,'resbaloso','líquido'),(64,'hiper puño',148,5,'despistao','garrampa'),(65,'mega patada ',126,NULL,'chamuscado',NULL),(66,'super patada hierro',69,7,'asustado','hierro'),(67,'chamuscado',140,10,'chamuscado','draco'),(68,'super patada líquido',87,10,'despistao','líquido'),(69,'super rayo',75,13,'asustado','draco'),(70,'mega baile líquido',150,24,'despistao','líquido'),(71,'mega patada',NULL,4,NULL,'planeo'),(72,'baile unicornio',31,10,'asustado','unicornio'),(73,'hiper siesta garrampa',NULL,19,NULL,'garrampa'),(74,'hiper siesta',46,NULL,'resbaloso',NULL),(75,'hiper patada',99,5,'resbaloso','draco'),(76,'tajo',142,31,'siesta','hierro'),(77,'mega puño',71,NULL,'despistao',NULL),(78,'mega abrazo',127,16,'chamuscado','líquido'),(79,'super cabezazo unicornio',NULL,1,NULL,'unicornio'),(80,'super rayo garrampa',20,11,'despistao','garrampa'),(81,'mega contoneo',NULL,17,NULL,'garrampa'),(82,'chamuscado plus',93,7,'chamuscado','césped'),(83,'siesta hierro',61,23,'resbaloso','hierro'),(84,'mega baile',97,NULL,'chamuscado',NULL),(85,'abrazo hierro',79,28,'asustado','hierro'),(86,'mega abrazo ',110,NULL,'asustado',NULL),(87,'cabezazo garrampa',63,5,'asustado','garrampa'),(88,'rayo ',68,NULL,'chamuscado',NULL),(89,'mega rayo hierro',50,9,'despistao','hierro'),(90,'super abrazo',10,3,'despistao','unicornio'),(91,'hiper rayo llama',NULL,28,NULL,'llama'),(92,'baile ',70,NULL,'resbaloso',NULL),(93,'tajo garrampa',23,4,'asustado','garrampa'),(94,'mega abrazo líquido',72,8,'resbaloso','líquido'),(95,'rayo garrampa',57,5,'asustado','garrampa'),(96,'super abrazo ',90,NULL,'asustado',NULL),(97,'despistao',41,NULL,'despistao',NULL),(98,'resbaloso',70,35,'resbaloso','unicornio'),(99,'patada draco',102,17,'chamuscado','draco'),(100,'patada ',19,NULL,'siesta',NULL),(101,'hiper abrazo unicornio',81,14,'resbaloso','unicornio'),(102,'super abrazo césped',81,22,'chamuscado','césped'),(103,'mega contoneo hierro',123,30,'chamuscado','hierro'),(104,'mega baile hierro',67,33,'chamuscado','hierro'),(105,'super patada ',36,NULL,'siesta',NULL),(106,'hiper patada llama',104,30,'resbaloso','llama'),(107,'hiper abrazo planeo',50,35,'asustado','planeo'),(108,'mega siesta',125,19,'asustado','garrampa'),(109,'super cabezazo césped',NULL,27,NULL,'césped'),(110,'hiper siesta draco',24,12,'asustado','draco'),(111,'hiper abrazo ',22,NULL,'despistao',NULL),(112,'mega tajo garrampa',106,32,'siesta','garrampa'),(113,'asustado plus',12,16,'asustado','unicornio'),(114,'patada planeo',30,1,'chamuscado','planeo'),(115,'super siesta',NULL,NULL,NULL,NULL),(116,'super contoneo unicornio',111,15,'asustado','unicornio'),(117,'super contoneo draco',25,24,'resbaloso','draco'),(118,'super cabezazo draco',19,19,'chamuscado','draco'),(119,'hiper tajo césped',16,33,'despistao','césped'),(120,'baile garrampa',62,1,'despistao','garrampa'),(121,'hiper tajo unicornio',128,31,'chamuscado','unicornio'),(122,'rayo draco',14,1,'resbaloso','draco'),(123,'mega patada césped',24,10,'chamuscado','césped'),(124,'super siesta unicornio',139,1,'resbaloso','unicornio'),(125,'super patada llama',112,11,'chamuscado','llama'),(126,'hiper tajo planeo',77,2,'despistao','planeo'),(127,'mega baile garrampa',75,15,'despistao','garrampa'),(128,'super contoneo garrampa',60,25,'chamuscado','garrampa'),(129,'super puño unicornio',129,33,'resbaloso','unicornio'),(130,'hiper baile líquido',113,2,'asustado','líquido'),(131,'mega patada draco',46,35,'despistao','draco'),(132,'super baile',115,26,'despistao','garrampa'),(133,'mega siesta unicornio',64,29,'asustado','unicornio'),(134,'super rayo draco',112,6,'asustado','draco'),(135,'mega patada planeo',12,26,'chamuscado','planeo'),(136,'tajo unicornio',NULL,35,NULL,'unicornio'),(137,'cabezazo unicornio',88,3,'asustado','unicornio'),(138,'patada hierro',63,25,'resbaloso','hierro'),(139,'super baile hierro',62,25,'asustado','hierro'),(140,'super abrazo planeo',10,23,'asustado','planeo'),(141,'contoneo unicornio',29,8,'asustado','unicornio'),(142,'mega tajo hierro',136,17,'resbaloso','hierro'),(143,'patada unicornio',61,8,'asustado','unicornio'),(144,'abrazo garrampa',103,21,'despistao','garrampa'),(145,'super rayo unicornio',43,1,'siesta','unicornio'),(146,'contoneo hierro',126,34,'siesta','hierro'),(147,'contoneo planeo',67,10,'despistao','planeo'),(148,'hiper cabezazo',75,5,'despistao','unicornio'),(149,'super puño draco',99,22,'chamuscado','draco'),(150,'puño planeo',88,13,'chamuscado','planeo'),(151,'tajo ',90,NULL,'despistao',NULL),(152,'mega siesta césped',55,2,'asustado','césped'),(153,'contoneo césped',92,7,'resbaloso','césped'),(154,'mega rayo garrampa',20,30,'asustado','garrampa'),(155,'mega patada garrampa',53,17,'asustado','garrampa'),(156,'super rayo ',36,NULL,'despistao',NULL),(157,'mega tajo ',25,NULL,'asustado',NULL),(158,'mega siesta draco',75,3,'asustado','draco'),(159,'contoneo draco',NULL,28,NULL,'draco'),(160,'rayo hierro',16,16,'asustado','hierro'),(161,'mega patada unicornio',87,14,'siesta','unicornio'),(162,'mega abrazo unicornio',10,20,'chamuscado','unicornio'),(163,'mega cabezazo ',NULL,NULL,NULL,NULL),(164,'hiper contoneo garrampa',75,12,'chamuscado','garrampa'),(165,'mega tajo unicornio',51,14,'asustado','unicornio'),(166,'hiper cabezazo unicornio',49,10,'despistao','unicornio'),(167,'baile draco',75,29,'despistao','draco'),(168,'siesta planeo',144,5,'asustado','planeo'),(169,'super puño hierro',NULL,10,NULL,'hierro'),(170,'puño draco',46,31,'resbaloso','draco'),(171,'hiper contoneo unicornio',44,3,'chamuscado','unicornio'),(172,'hiper baile hierro',59,25,'siesta','hierro'),(173,'hiper siesta hierro',25,1,'resbaloso','hierro'),(174,'super rayo hierro',36,23,'chamuscado','hierro'),(175,'mega siesta garrampa',40,7,'chamuscado','garrampa'),(176,'hiper rayo garrampa',93,20,'resbaloso','garrampa'),(177,'hiper puño garrampa',21,3,'resbaloso','garrampa'),(178,'siesta garrampa',40,1,'despistao','garrampa'),(179,'rayo césped',129,10,'resbaloso','césped'),(180,'cabezazo planeo',18,21,'asustado','planeo'),(181,'hiper siesta unicornio',31,12,'chamuscado','unicornio'),(182,'rayo planeo',58,11,'chamuscado','planeo'),(183,'hiper abrazo garrampa',114,23,'resbaloso','garrampa'),(184,'super abrazo hierro',NULL,17,NULL,'hierro'),(185,'super tajo llama',84,2,'despistao','llama'),(186,'contoneo llama',50,22,'asustado','llama'),(187,'mega abrazo hierro',129,22,'asustado','hierro'),(188,'tajo draco',53,15,'siesta','draco'),(189,'hiper tajo garrampa',91,6,'chamuscado','garrampa'),(190,'super baile planeo',143,6,'asustado','planeo'),(191,'hiper patada garrampa',131,26,'asustado','garrampa'),(192,'patada garrampa',72,35,'chamuscado','garrampa'),(193,'hiper contoneo espíritu',77,3,'asustado','espíritu'),(194,'super siesta garrampa',NULL,4,NULL,'garrampa'),(195,'hiper siesta líquido',103,1,'asustado','líquido'),(196,'tajo llama',93,28,'resbaloso','llama'),(197,'super baile unicornio',75,33,'chamuscado','unicornio'),(198,'super abrazo unicornio',120,5,'asustado','unicornio'),(199,'super contoneo hierro',44,9,'asustado','hierro'),(200,'abrazo espíritu',83,4,'chamuscado','espíritu'),(201,'mega patada líquido',130,21,'asustado','líquido'),(202,'super siesta hierro',136,35,'asustado','hierro'),(203,'mega abrazo draco',81,31,'resbaloso','draco'),(204,'mega puño draco',17,10,'despistao','draco'),(205,'mega contoneo garrampa',62,12,'chamuscado','garrampa'),(206,'abrazo unicornio',NULL,26,NULL,'unicornio'),(207,'mega puño planeo',NULL,33,NULL,'planeo');
/*!40000 ALTER TABLE `ataque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ataque_especie`
--

DROP TABLE IF EXISTS `ataque_especie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ataque_especie` (
  `id_ataque` int(11) NOT NULL,
  `id_especie` int(11) NOT NULL,
  PRIMARY KEY (`id_ataque`,`id_especie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ataque_especie`
--

LOCK TABLES `ataque_especie` WRITE;
/*!40000 ALTER TABLE `ataque_especie` DISABLE KEYS */;
INSERT INTO `ataque_especie` VALUES (1,1),(2,2),(3,3),(4,5),(5,4),(6,2),(7,6),(7,7),(7,8),(7,9),(7,10),(7,11),(7,12),(7,13),(7,15),(7,16),(7,17),(7,19),(7,20),(7,21),(7,22),(7,24),(7,25),(7,26),(7,27),(7,28),(7,29),(7,31),(7,32),(7,33),(7,34),(7,35),(7,36),(7,37),(7,38),(7,39),(7,40),(7,41),(7,42),(7,43),(7,44),(7,46),(7,47),(7,48),(7,49),(7,50),(7,51),(7,52),(7,53),(7,54),(7,56),(7,57),(7,58),(7,59),(7,60),(7,61),(7,62),(7,63),(7,64),(7,65),(7,66),(7,67),(7,68),(7,69),(7,71),(7,72),(7,73),(7,74),(7,75),(7,76),(7,77),(7,78),(7,79),(7,80),(7,81),(7,82),(7,83),(7,84),(7,85),(7,86),(7,87),(7,88),(7,89),(7,90),(7,91),(7,92),(7,93),(7,95),(7,96),(7,98),(7,99),(7,100),(8,6),(8,9),(8,10),(8,12),(8,14),(8,15),(8,16),(8,18),(8,19),(8,24),(8,25),(8,27),(8,29),(8,30),(8,31),(8,32),(8,33),(8,34),(8,36),(8,37),(8,38),(8,39),(8,41),(8,43),(8,44),(8,48),(8,49),(8,50),(8,51),(8,52),(8,53),(8,57),(8,58),(8,59),(8,61),(8,62),(8,63),(8,64),(8,65),(8,67),(8,68),(8,70),(8,71),(8,72),(8,73),(8,74),(8,75),(8,76),(8,77),(8,79),(8,80),(8,81),(8,82),(8,83),(8,84),(8,85),(8,86),(8,88),(8,89),(8,90),(8,91),(8,92),(8,93),(8,94),(8,95),(8,96),(8,97),(8,98),(8,99),(8,100),(9,6),(9,7),(9,10),(9,11),(9,14),(9,15),(9,16),(9,17),(9,18),(9,19),(9,21),(9,22),(9,24),(9,25),(9,29),(9,30),(9,31),(9,32),(9,34),(9,36),(9,37),(9,38),(9,39),(9,41),(9,43),(9,44),(9,47),(9,48),(9,49),(9,50),(9,51),(9,52),(9,53),(9,56),(9,57),(9,58),(9,61),(9,62),(9,63),(9,65),(9,66),(9,68),(9,69),(9,70),(9,71),(9,72),(9,73),(9,74),(9,75),(9,76),(9,77),(9,78),(9,79),(9,80),(9,81),(9,82),(9,83),(9,84),(9,85),(9,86),(9,87),(9,88),(9,89),(9,90),(9,91),(9,92),(9,93),(9,94),(9,95),(9,96),(9,97),(9,98),(9,99),(9,100),(10,14),(10,18),(10,24),(10,26),(10,29),(10,30),(10,31),(10,32),(10,36),(10,38),(10,39),(10,40),(10,41),(10,44),(10,49),(10,50),(10,51),(10,52),(10,53),(10,58),(10,61),(10,62),(10,63),(10,64),(10,65),(10,69),(10,70),(10,71),(10,72),(10,73),(10,74),(10,75),(10,76),(10,79),(10,80),(10,81),(10,82),(10,83),(10,84),(10,88),(10,89),(10,90),(10,91),(10,92),(10,93),(10,94),(10,97),(10,98),(10,100),(11,6),(11,7),(11,8),(11,10),(11,14),(11,15),(11,16),(11,18),(11,19),(11,21),(11,23),(11,24),(11,25),(11,29),(11,31),(11,32),(11,33),(11,34),(11,36),(11,37),(11,38),(11,39),(11,41),(11,43),(11,44),(11,45),(11,46),(11,47),(11,48),(11,49),(11,50),(11,51),(11,52),(11,53),(11,54),(11,55),(11,57),(11,58),(11,60),(11,61),(11,62),(11,63),(11,65),(11,66),(11,68),(11,69),(11,70),(11,71),(11,72),(11,73),(11,74),(11,75),(11,76),(11,77),(11,78),(11,79),(11,80),(11,81),(11,82),(11,83),(11,84),(11,85),(11,86),(11,87),(11,88),(11,89),(11,90),(11,91),(11,92),(11,93),(11,94),(11,95),(11,96),(11,97),(11,98),(11,99),(11,100),(12,6),(12,7),(12,10),(12,15),(12,16),(12,18),(12,19),(12,20),(12,25),(12,28),(12,29),(12,34),(12,36),(12,37),(12,38),(12,39),(12,43),(12,44),(12,48),(12,49),(12,50),(12,51),(12,52),(12,53),(12,57),(12,58),(12,61),(12,62),(12,63),(12,68),(12,69),(12,70),(12,71),(12,72),(12,73),(12,77),(12,78),(12,79),(12,80),(12,81),(12,82),(12,83),(12,85),(12,86),(12,87),(12,88),(12,89),(12,90),(12,91),(12,92),(12,95),(12,96),(12,97),(12,98),(12,99),(12,100),(13,8),(13,9),(13,10),(13,11),(13,12),(13,13),(13,14),(13,17),(13,18),(13,19),(13,20),(13,21),(13,22),(13,23),(13,24),(13,26),(13,27),(13,28),(13,29),(13,30),(13,31),(13,32),(13,35),(13,36),(13,37),(13,38),(13,39),(13,40),(13,41),(13,42),(13,44),(13,45),(13,49),(13,50),(13,51),(13,52),(13,53),(13,54),(13,55),(13,58),(13,59),(13,60),(13,61),(13,62),(13,63),(13,64),(13,65),(13,66),(13,69),(13,70),(13,71),(13,72),(13,73),(13,74),(13,75),(13,76),(13,79),(13,80),(13,81),(13,82),(13,83),(13,84),(13,88),(13,89),(13,90),(13,91),(13,92),(13,93),(13,94),(13,97),(13,98),(13,99),(13,100),(14,6),(14,10),(14,12),(14,14),(14,15),(14,16),(14,18),(14,19),(14,23),(14,25),(14,29),(14,30),(14,31),(14,32),(14,34),(14,37),(14,39),(14,41),(14,42),(14,43),(14,44),(14,48),(14,49),(14,50),(14,51),(14,53),(14,57),(14,61),(14,62),(14,63),(14,65),(14,68),(14,70),(14,71),(14,73),(14,74),(14,75),(14,76),(14,77),(14,78),(14,80),(14,83),(14,84),(14,85),(14,86),(14,88),(14,89),(14,93),(14,94),(14,95),(14,96),(14,97),(14,98),(14,99),(15,6),(15,7),(15,10),(15,13),(15,14),(15,15),(15,16),(15,19),(15,22),(15,23),(15,24),(15,25),(15,30),(15,31),(15,32),(15,33),(15,34),(15,37),(15,38),(15,41),(15,43),(15,45),(15,46),(15,47),(15,48),(15,52),(15,55),(15,57),(15,61),(15,62),(15,65),(15,66),(15,68),(15,69),(15,72),(15,74),(15,75),(15,76),(15,77),(15,78),(15,81),(15,82),(15,84),(15,85),(15,86),(15,87),(15,89),(15,90),(15,91),(15,92),(15,94),(15,95),(15,96),(15,98),(15,99),(15,100),(16,7),(16,10),(16,11),(16,16),(16,17),(16,18),(16,19),(16,21),(16,24),(16,29),(16,32),(16,36),(16,38),(16,44),(16,49),(16,50),(16,52),(16,53),(16,56),(16,58),(16,61),(16,62),(16,69),(16,70),(16,71),(16,72),(16,76),(16,78),(16,79),(16,80),(16,81),(16,82),(16,83),(16,87),(16,88),(16,90),(16,91),(16,92),(16,97),(16,100),(17,6),(17,7),(17,10),(17,12),(17,14),(17,15),(17,16),(17,18),(17,19),(17,23),(17,24),(17,25),(17,29),(17,30),(17,31),(17,32),(17,34),(17,36),(17,37),(17,39),(17,41),(17,42),(17,43),(17,44),(17,48),(17,49),(17,50),(17,51),(17,57),(17,58),(17,61),(17,63),(17,65),(17,68),(17,69),(17,70),(17,71),(17,73),(17,74),(17,75),(17,76),(17,77),(17,78),(17,79),(17,80),(17,81),(17,83),(17,84),(17,85),(17,86),(17,87),(17,88),(17,89),(17,90),(17,93),(17,94),(17,95),(17,96),(17,97),(17,98),(17,99),(17,100),(18,6),(18,7),(18,10),(18,14),(18,15),(18,16),(18,18),(18,19),(18,24),(18,25),(18,29),(18,30),(18,31),(18,32),(18,34),(18,36),(18,37),(18,38),(18,39),(18,41),(18,43),(18,44),(18,48),(18,49),(18,50),(18,51),(18,52),(18,53),(18,57),(18,58),(18,61),(18,62),(18,63),(18,65),(18,68),(18,69),(18,70),(18,71),(18,72),(18,73),(18,74),(18,75),(18,76),(18,77),(18,78),(18,79),(18,80),(18,81),(18,82),(18,83),(18,84),(18,85),(18,86),(18,87),(18,88),(18,89),(18,90),(18,91),(18,92),(18,93),(18,94),(18,95),(18,96),(18,97),(18,98),(18,99),(18,100),(19,9),(19,10),(19,12),(19,14),(19,15),(19,18),(19,19),(19,24),(19,25),(19,27),(19,29),(19,30),(19,31),(19,32),(19,33),(19,34),(19,36),(19,37),(19,38),(19,41),(19,43),(19,44),(19,49),(19,50),(19,51),(19,52),(19,53),(19,57),(19,58),(19,59),(19,61),(19,62),(19,64),(19,65),(19,67),(19,68),(19,70),(19,71),(19,72),(19,73),(19,74),(19,75),(19,76),(19,77),(19,79),(19,80),(19,81),(19,82),(19,83),(19,84),(19,85),(19,86),(19,88),(19,89),(19,90),(19,91),(19,92),(19,93),(19,94),(19,95),(19,97),(19,98),(19,99),(19,100),(20,6),(20,7),(20,15),(20,16),(20,18),(20,24),(20,25),(20,26),(20,31),(20,32),(20,34),(20,36),(20,40),(20,41),(20,42),(20,43),(20,48),(20,49),(20,50),(20,51),(20,57),(20,58),(20,65),(20,68),(20,69),(20,70),(20,71),(20,74),(20,75),(20,76),(20,77),(20,78),(20,79),(20,80),(20,85),(20,86),(20,87),(20,88),(20,95),(20,96),(20,97);
/*!40000 ALTER TABLE `ataque_especie` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 trigger borrar before insert on ataque_especie for each row
begin
if((select tipo from ataque where id=new.id_ataque)<>(select tipo1 from especie where id=new.id_especie)
and(select tipo from ataque where id=new.id_ataque)<>(select tipo2 from especie where id=new.id_especie))
then
signal sqlstate '45000';
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ataque_pokimon`
--

DROP TABLE IF EXISTS `ataque_pokimon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ataque_pokimon` (
  `id_ataque` int(11) NOT NULL,
  `id_pokimon` int(11) NOT NULL,
  PRIMARY KEY (`id_ataque`,`id_pokimon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ataque_pokimon`
--

LOCK TABLES `ataque_pokimon` WRITE;
/*!40000 ALTER TABLE `ataque_pokimon` DISABLE KEYS */;
INSERT INTO `ataque_pokimon` VALUES (1,1),(1,4),(2,2),(3,3),(4,4),(4,5),(5,6),(6,2),(7,7),(7,31),(7,32),(7,42),(7,47),(7,57),(7,58),(7,60),(7,66),(7,67),(7,76),(7,77),(7,78),(7,90),(7,91),(7,92),(7,98),(7,99),(8,7),(8,13),(8,32),(8,48),(8,60),(8,67),(8,68),(8,77),(8,78),(8,85),(8,87),(9,7),(9,9),(9,13),(9,14),(9,32),(9,33),(9,51),(9,61),(9,67),(9,68),(9,78),(9,85),(9,86),(9,87),(10,7),(10,8),(10,9),(10,10),(10,16),(10,23),(10,24),(10,52),(10,63),(10,71),(10,80),(10,81),(11,7),(11,9),(11,10),(11,11),(11,18),(11,19),(11,20),(11,27),(11,28),(11,29),(11,36),(11,37),(11,39),(11,40),(11,64),(12,7),(12,9),(12,10),(12,11),(13,7),(13,8),(13,9),(13,10),(13,12),(13,13),(14,9),(14,11),(14,14),(15,10),(15,11),(15,13),(16,7),(16,10),(16,13),(16,14),(16,16),(17,7),(17,9),(17,10),(17,11),(17,14),(18,7),(18,9),(18,10),(18,11),(18,13),(18,14),(18,16),(18,18),(19,7),(19,9),(19,10),(19,11),(19,13),(19,14),(19,16),(19,18),(19,19),(20,7),(20,8),(20,11),(20,14),(20,18),(20,19),(20,20);
/*!40000 ALTER TABLE `ataque_pokimon` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*//*!50003 trigger borrarpoki before insert on ataque_pokimon for each row
begin
if(select count(*) from ataque_especie where id_especie=(select id_especie from pokimon where id=new.id_pokimon) and id_ataque=new.id_ataque)=0

then
signal sqlstate '45000';
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `especie`
--

DROP TABLE IF EXISTS `especie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `especie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) DEFAULT NULL,
  `tipo1` enum('espíritu','llama','líquido','césped','planeo','draco','hierro','unicornio','garrampa') DEFAULT NULL,
  `tipo2` enum('espíritu','llama','líquido','césped','planeo','draco','hierro','unicornio','garrampa') DEFAULT NULL,
  `ataque_base` int(11) DEFAULT NULL,
  `defensa_base` int(11) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especie`
--

LOCK TABLES `especie` WRITE;
/*!40000 ALTER TABLE `especie` DISABLE KEYS */;
INSERT INTO `especie` VALUES (1,'chorrimander','llama',NULL,15,10,'Un lagartillo que la palma si se le moja la cola'),(2,'escuero','líquido',NULL,10,15,'Tortuga sin cuello y con unas gafas de sol molonas'),(3,'bulbosaurio','césped',NULL,12,13,'Rana con una cebolla a la espalda'),(4,'rascachú','garrampa',NULL,14,8,'Roedor que sifruta mordisqueando los cables de los transformadores de corriente alterna'),(5,'jengar','espíritu',NULL,18,20,'Bicho mu gracioso que disfruta dando sustos'),(7,'dogokude','draco',NULL,20,11,NULL),(8,'jega','garrampa','garrampa',7,12,NULL),(9,'sije','unicornio','césped',29,13,NULL),(10,'ipa\' kife','llama',NULL,3,6,NULL),(11,'ahe badu','draco','draco',2,22,NULL),(12,'doreve','unicornio','planeo',12,3,NULL),(13,'efeju bulazo','llama','garrampa',3,8,NULL),(14,'zisubele','garrampa',NULL,9,2,NULL),(15,'wuvaneqa','garrampa',NULL,14,23,NULL),(16,'e\' opemi','garrampa',NULL,22,21,NULL),(17,'ilirine','draco','hierro',30,10,NULL),(18,'ozufija','garrampa',NULL,22,27,NULL),(19,'kopehi','draco',NULL,30,6,NULL),(20,'lo','líquido','hierro',9,27,NULL),(21,'vubi','garrampa','draco',22,2,NULL),(22,'o nopecu','draco','garrampa',13,13,NULL),(23,'ejo\' ','garrampa','planeo',28,14,NULL),(24,'gofe','draco',NULL,24,7,NULL),(25,'ecani\' coxoke','garrampa',NULL,29,8,NULL),(26,'wu','llama','césped',22,30,NULL),(27,'api\' ubo','unicornio','unicornio',2,8,NULL),(28,'citu','hierro','líquido',6,11,NULL),(29,'carumiju','césped',NULL,30,14,NULL),(30,'ha','hierro',NULL,24,15,NULL),(31,'boqi','draco',NULL,9,22,NULL),(32,'witi','llama',NULL,2,4,NULL),(33,'bobi','garrampa','unicornio',12,22,NULL),(34,'oqi iwiqita','llama',NULL,15,27,NULL),(35,'fepedu','hierro','hierro',23,12,NULL),(36,'ce','unicornio',NULL,18,23,NULL),(37,'liba','unicornio',NULL,3,14,NULL),(38,'liha','planeo',NULL,28,14,NULL),(39,'u wo','líquido',NULL,25,6,NULL),(40,'ime\' la','césped','llama',4,28,NULL),(41,'jahaki','líquido',NULL,19,9,NULL),(42,'cize','llama','planeo',10,16,NULL),(43,'a u','garrampa',NULL,19,24,NULL),(44,'seba','césped',NULL,12,12,NULL),(45,'cuhapupisa','hierro','garrampa',1,1,NULL),(46,'axuki','garrampa','garrampa',9,28,NULL),(47,'ajalo','garrampa','draco',27,27,NULL),(48,'xo efo','unicornio',NULL,18,1,NULL),(49,'fogobu','líquido',NULL,16,24,NULL),(50,'dipu','llama',NULL,19,14,NULL),(51,'asu towo','hierro',NULL,20,30,NULL),(52,'uxeqe','garrampa',NULL,17,14,NULL),(53,'izo\' pogo','unicornio',NULL,5,3,NULL),(54,'okina','hierro','garrampa',20,20,NULL),(55,'icumu','garrampa','garrampa',25,9,NULL),(56,'ukinete','césped','draco',3,29,NULL),(57,'coli','planeo',NULL,23,19,NULL),(58,'pa','garrampa',NULL,3,28,NULL),(59,'tanocina','unicornio','garrampa',16,2,NULL),(60,'xoza','garrampa','hierro',5,21,NULL),(61,'gixi','hierro',NULL,3,25,NULL),(62,'efu\' ca','garrampa',NULL,1,1,NULL),(63,'olarafa','unicornio',NULL,29,16,NULL),(64,'oxe oguto','césped','unicornio',8,3,NULL),(65,'inoli\' axo','unicornio',NULL,17,10,NULL),(66,'ivoti xeha','draco','garrampa',29,2,NULL),(67,'jiqu','unicornio','unicornio',17,23,NULL),(68,'foze','espíritu',NULL,9,14,NULL),(69,'tuqa','hierro',NULL,8,8,NULL),(70,'rari','líquido',NULL,23,2,NULL),(71,'hewotu','líquido',NULL,18,1,NULL),(72,'keho','césped',NULL,10,9,NULL),(73,'owurijaka','garrampa',NULL,7,21,NULL),(74,'eto','planeo',NULL,7,26,NULL),(75,'safudi','llama',NULL,2,13,NULL),(76,'eka','garrampa',NULL,9,22,NULL),(77,'ruveji','hierro',NULL,24,11,NULL),(78,'ifelo kikomiqo','unicornio',NULL,4,6,NULL),(79,'uso\' ufakifi','unicornio',NULL,1,21,NULL),(80,'a\' aboxa','garrampa',NULL,4,29,NULL),(81,'e rejaxada','césped',NULL,4,30,NULL),(82,'kumo','líquido',NULL,16,7,NULL),(83,'jidozavo','draco',NULL,28,18,NULL),(84,'idutuve','líquido',NULL,23,9,NULL),(85,'a','garrampa',NULL,4,11,NULL),(86,'ubu jo','draco',NULL,1,3,NULL),(87,'imiza\' iwuquta','planeo',NULL,2,6,NULL),(88,'ejakitabe','draco',NULL,30,3,NULL),(89,'a xefi','césped',NULL,20,9,NULL),(90,'uzu bewodu','draco',NULL,27,17,NULL),(91,'omege','hierro',NULL,25,12,NULL),(92,'uce\' re','garrampa',NULL,30,17,NULL),(93,'jofigacu','hierro',NULL,16,20,NULL),(94,'ufe\' araso','césped',NULL,27,9,NULL),(95,'lufa','hierro',NULL,5,17,NULL),(96,'legu','planeo',NULL,15,17,NULL),(97,'qegafuvi','draco',NULL,26,24,NULL),(98,'pole','unicornio',NULL,8,2,NULL),(99,'uvogo ecevikazu','hierro',NULL,23,24,NULL);
/*!40000 ALTER TABLE `especie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intercambio`
--

DROP TABLE IF EXISTS `intercambio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intercambio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_motivador1` int(11) NOT NULL,
  `id_motivador2` int(11) NOT NULL,
  `id_pokimon1` int(11) NOT NULL,
  `id_pokimon2` int(11) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intercambio`
--

LOCK TABLES `intercambio` WRITE;
/*!40000 ALTER TABLE `intercambio` DISABLE KEYS */;
INSERT INTO `intercambio` VALUES (1,1,3,1,3,'2019-02-03'),(2,2,1,39,8,'2017-05-08'),(3,9,20,58,30,'2018-12-04'),(4,7,15,2,30,'2018-03-14'),(5,3,9,45,23,'2017-10-22'),(6,14,5,23,95,'2017-11-21'),(7,11,7,36,22,'2018-08-27'),(8,11,5,88,44,'2017-02-19'),(9,10,8,29,31,'2019-03-30'),(10,11,12,76,8,'2019-03-26'),(11,2,11,87,46,'2017-05-06'),(12,14,7,63,75,'2019-02-23'),(13,6,16,84,96,'2018-10-03'),(14,16,16,34,2,'2018-03-11'),(15,5,10,99,3,'2019-02-10'),(16,5,2,42,62,'2018-08-04'),(17,5,10,26,60,'2017-10-05'),(18,4,3,44,7,'2017-12-01'),(19,18,1,39,35,'2017-09-07'),(20,17,7,98,24,'2017-06-18'),(21,6,11,44,65,'2018-06-19'),(22,2,10,95,24,'2018-02-10'),(23,16,16,41,9,'2017-02-17'),(24,15,13,54,12,'2017-07-29'),(25,20,8,14,93,'2017-03-19'),(26,8,11,66,51,'2017-04-26'),(27,16,18,22,85,'2018-11-24'),(28,16,6,75,21,'2017-05-03'),(29,18,13,42,69,'2018-11-27'),(30,7,6,65,18,'2018-11-06'),(31,19,19,98,69,'2018-08-10'),(32,9,16,33,18,'2019-01-28'),(33,10,7,31,16,'2017-10-17'),(34,2,11,38,63,'2018-03-24'),(35,3,15,77,15,'2019-02-05'),(36,14,8,3,40,'2017-12-19'),(37,12,9,24,47,'2017-08-19'),(38,5,9,46,35,'2018-07-30'),(39,1,7,72,63,'2019-01-14'),(40,14,5,83,43,'2017-02-21'),(41,10,17,91,15,'2017-09-25'),(42,3,9,4,81,'2018-06-26'),(43,3,3,31,75,'2017-03-01'),(44,1,8,62,64,'2017-06-11'),(45,15,14,32,62,'2018-03-07'),(46,10,5,73,98,'2018-12-03'),(47,15,9,41,6,'2017-05-03'),(48,15,14,32,22,'2018-07-02'),(49,6,16,51,42,'2018-10-21'),(50,1,20,79,1,'2018-07-22'),(51,15,10,74,2,'2019-01-11'),(52,2,2,81,88,'2018-10-19'),(53,16,18,96,93,'2019-01-23'),(54,19,1,4,88,'2017-03-26'),(55,5,6,66,14,'2017-12-15'),(56,5,4,79,71,'2018-05-31'),(57,11,20,64,28,'2017-05-22'),(58,11,17,72,1,'2017-04-06'),(59,17,19,71,100,'2017-12-01'),(60,9,3,22,3,'2017-10-06'),(61,13,16,57,98,'2017-03-12'),(62,7,18,49,84,'2017-05-03'),(63,11,8,53,24,'2017-10-10'),(64,1,14,24,2,'2018-12-08'),(65,6,9,13,30,'2017-09-26'),(66,14,12,63,22,'2017-04-01'),(67,15,6,14,97,'2019-03-12'),(68,3,19,59,23,'2017-03-17'),(69,4,18,5,31,'2018-05-13'),(70,6,4,97,74,'2018-04-30'),(71,2,11,80,14,'2018-07-20'),(72,6,19,90,86,'2018-12-18'),(73,14,1,13,25,'2017-12-02'),(74,4,2,4,14,'2017-07-02'),(75,2,6,62,93,'2017-02-23'),(76,20,20,41,96,'2017-09-28'),(77,8,12,43,63,'2017-10-01'),(78,8,1,66,1,'2017-08-07'),(79,16,19,9,24,'2017-02-19'),(80,12,15,69,99,'2018-01-12'),(81,14,16,4,96,'2018-09-29'),(82,12,1,73,74,'2017-10-05'),(83,6,15,27,94,'2018-08-21'),(84,6,7,64,34,'2017-11-06'),(85,12,10,56,100,'2018-05-17'),(86,12,16,75,27,'2018-07-03'),(87,4,8,45,98,'2017-03-16'),(88,14,5,88,32,'2017-07-11'),(89,15,10,87,42,'2017-06-12'),(90,19,12,7,10,'2017-05-10'),(91,20,1,50,64,'2017-11-21'),(92,8,1,91,79,'2017-02-17'),(93,15,6,89,89,'2018-02-25'),(94,13,17,4,5,'2017-03-29'),(95,16,6,56,97,'2018-05-15'),(96,2,2,41,94,'2018-07-19'),(97,13,11,19,89,'2018-12-17'),(98,15,9,52,20,'2018-10-02'),(99,20,8,74,20,'2018-11-26'),(100,7,4,89,94,'2018-04-30'),(101,8,4,45,84,'2017-02-25'),(102,4,8,31,96,'2017-02-16'),(103,17,12,50,65,'2017-11-23'),(104,8,3,21,62,'2018-02-05'),(105,1,19,79,99,'2017-05-01'),(106,5,1,94,5,'2018-01-08'),(107,12,13,94,3,'2018-12-15'),(108,8,8,97,84,'2017-02-17'),(109,6,19,59,31,'2017-02-25'),(110,7,16,50,51,'2017-06-24'),(111,9,7,81,4,'2018-10-20'),(112,20,11,43,83,'2017-04-12'),(113,5,10,69,78,'2018-04-05'),(114,18,4,23,89,'2017-02-20'),(115,12,8,93,37,'2017-03-04'),(116,17,20,95,23,'2017-10-06'),(117,11,10,47,7,'2018-08-09'),(118,18,17,32,55,'2017-02-21'),(119,19,18,80,51,'2017-12-03'),(120,7,8,89,94,'2017-09-24'),(121,10,1,75,62,'2017-05-15'),(122,9,10,3,47,'2018-05-31'),(123,10,1,40,74,'2018-04-04'),(124,16,11,68,25,'2018-09-04'),(125,18,16,50,93,'2018-11-03'),(126,1,3,5,73,'2018-02-11'),(127,18,1,25,42,'2017-07-09'),(128,8,15,72,30,'2018-05-26'),(129,15,3,10,91,'2017-05-18'),(130,10,14,29,65,'2017-05-14'),(131,11,9,11,25,'2018-04-17'),(132,14,3,60,77,'2017-02-20'),(133,18,17,37,46,'2019-03-25'),(134,10,7,69,76,'2017-05-02'),(135,13,5,6,89,'2017-05-02'),(136,5,5,2,6,'2018-08-26'),(137,3,20,19,54,'2018-07-18'),(138,13,6,21,8,'2018-10-17'),(139,20,5,28,18,'2017-02-16'),(140,11,4,1,23,'2017-02-24'),(141,13,3,87,5,'2017-10-09'),(142,9,4,19,1,'2017-02-18'),(143,8,2,55,2,'2017-12-04'),(144,19,6,30,38,'2018-03-31'),(145,9,18,28,37,'2017-02-23'),(146,2,8,45,66,'2017-05-21'),(147,9,7,35,67,'2018-06-29'),(148,15,2,90,43,'2018-11-13'),(149,9,17,17,61,'2017-03-12'),(150,8,9,49,8,'2018-04-16'),(151,5,1,26,90,'2018-07-22'),(152,14,17,16,74,'2017-12-25'),(153,8,3,71,52,'2018-11-26'),(154,12,14,99,94,'2018-12-18'),(155,10,20,71,17,'2018-02-03'),(156,15,9,71,54,'2018-12-27'),(157,3,19,40,3,'2018-04-03'),(158,16,8,10,9,'2019-02-25'),(159,19,19,86,75,'2018-05-12'),(160,20,10,12,39,'2017-08-06'),(161,14,7,10,5,'2017-02-20'),(162,1,6,39,6,'2018-12-05'),(163,14,2,13,67,'2017-03-28'),(164,15,8,3,74,'2019-01-12'),(165,16,16,1,63,'2017-02-17'),(166,4,15,78,59,'2018-01-10'),(167,1,11,58,13,'2017-07-22'),(168,3,6,69,40,'2018-08-25'),(169,13,20,9,62,'2018-06-10'),(170,14,18,5,1,'2017-11-29'),(171,3,12,12,47,'2017-02-21'),(172,12,19,94,61,'2017-11-04'),(173,5,12,41,2,'2017-02-21'),(174,16,20,64,26,'2018-12-05'),(175,9,6,26,57,'2017-12-21'),(176,12,4,74,29,'2017-07-02'),(177,5,8,51,14,'2017-02-17'),(178,20,11,18,91,'2017-09-28'),(179,8,4,50,80,'2017-08-25'),(180,5,12,14,94,'2017-04-30'),(181,16,9,61,29,'2018-06-25'),(182,7,15,84,8,'2017-04-24'),(183,3,2,59,13,'2018-01-31'),(184,8,4,44,100,'2018-02-05'),(185,8,14,23,91,'2017-08-11'),(186,11,3,18,93,'2018-06-05'),(187,20,3,61,61,'2017-10-22'),(188,8,14,44,92,'2017-02-18'),(189,2,2,16,45,'2017-02-24'),(190,6,7,62,76,'2018-03-23'),(191,11,13,11,32,'2017-04-13'),(192,7,13,71,34,'2017-10-03'),(193,4,14,20,55,'2019-02-03'),(194,16,4,85,48,'2019-03-10'),(195,4,6,90,53,'2019-01-13'),(196,16,1,63,69,'2017-05-01'),(197,17,13,98,99,'2017-02-18'),(198,12,11,72,93,'2017-04-08'),(199,17,3,62,78,'2019-01-17'),(200,4,9,72,79,'2017-05-29'),(201,9,17,41,56,'2018-09-06'),(202,4,10,100,45,'2018-12-01'),(203,9,7,35,66,'2018-01-03'),(204,1,1,83,63,'2018-09-15'),(205,13,4,17,55,'2017-02-28'),(206,2,2,2,78,'2017-04-03'),(207,9,17,39,93,'2017-06-11'),(208,9,8,30,34,'2018-09-04'),(209,14,18,39,18,'2017-06-19'),(210,5,11,85,2,'2017-07-20'),(211,12,19,97,53,'2017-05-23'),(212,8,5,64,13,'2017-02-24'),(213,14,20,80,9,'2018-10-15'),(214,17,16,43,89,'2018-12-02'),(215,15,1,69,36,'2018-10-18'),(216,14,6,86,7,'2019-03-21'),(217,17,19,98,86,'2017-03-07'),(218,12,6,30,8,'2017-03-17'),(219,10,4,38,62,'2018-05-13'),(220,11,2,62,45,'2017-09-24'),(221,6,4,51,10,'2017-02-23'),(222,2,15,4,3,'2018-11-26'),(223,3,11,82,82,'2017-07-27'),(224,13,9,99,4,'2017-12-11'),(225,17,8,46,72,'2018-10-28'),(226,6,18,45,35,'2018-02-22'),(227,9,6,77,75,'2018-04-16'),(228,16,18,39,64,'2017-09-03'),(229,10,11,29,31,'2019-01-15'),(230,20,17,75,82,'2017-07-15'),(231,13,16,1,67,'2017-12-28'),(232,13,6,15,48,'2017-04-20'),(233,3,15,54,11,'2018-12-09'),(234,15,5,7,8,'2018-05-28'),(235,16,18,87,93,'2017-11-17'),(236,7,3,90,98,'2017-02-20'),(237,1,16,78,84,'2018-06-06'),(238,7,7,61,25,'2017-12-04'),(239,17,12,6,10,'2017-06-08'),(240,11,2,81,50,'2017-12-30'),(241,7,20,76,45,'2018-06-01'),(242,9,12,74,38,'2018-09-29'),(243,16,8,46,84,'2018-12-19'),(244,3,1,6,1,'2017-02-24'),(245,3,3,81,36,'2017-06-19'),(246,9,12,83,48,'2019-01-07'),(247,9,1,20,80,'2018-09-03'),(248,10,11,12,71,'2018-11-05'),(249,7,4,9,43,'2017-08-20'),(250,3,1,54,9,'2018-03-17'),(251,10,9,5,5,'2017-03-17'),(252,18,11,13,49,'2017-02-20'),(253,7,17,1,87,'2018-10-29'),(254,10,17,58,53,'2018-07-06'),(255,6,15,62,62,'2017-07-25'),(256,8,1,56,6,'2018-12-28'),(257,3,12,35,85,'2017-03-14'),(258,11,17,30,1,'2017-06-10'),(259,12,13,92,25,'2019-03-01'),(260,11,4,88,40,'2018-05-09'),(261,15,16,51,92,'2017-02-16'),(262,2,10,7,91,'2017-02-17'),(263,3,11,76,26,'2017-10-23'),(264,8,8,58,86,'2019-01-17'),(265,8,1,44,51,'2019-01-13'),(266,9,12,56,36,'2017-07-02'),(267,16,3,14,47,'2017-08-01'),(268,9,2,27,43,'2017-12-11'),(269,1,15,28,90,'2018-07-07'),(270,11,16,8,89,'2017-12-18'),(271,11,6,39,25,'2017-07-14'),(272,6,14,78,2,'2019-02-23'),(273,13,1,83,14,'2017-04-15'),(274,1,6,8,23,'2018-01-31'),(275,7,17,89,5,'2017-02-16'),(276,19,1,62,24,'2017-07-16'),(277,8,13,82,80,'2017-04-14'),(278,2,7,35,58,'2017-02-23'),(279,12,8,92,9,'2017-06-09'),(280,17,5,75,99,'2018-07-13'),(281,3,9,7,8,'2018-07-03'),(282,5,9,79,59,'2017-08-07'),(283,9,17,97,98,'2018-05-26'),(284,18,20,5,53,'2018-04-19'),(285,20,12,26,29,'2018-07-29'),(286,8,12,63,6,'2018-05-10'),(287,19,6,98,68,'2018-03-23'),(288,6,7,8,25,'2018-01-23'),(289,7,3,37,79,'2017-08-17'),(290,4,1,55,69,'2019-03-05'),(291,19,9,82,54,'2018-03-03'),(292,12,14,93,96,'2017-02-19'),(293,7,3,64,16,'2019-01-14'),(294,18,20,5,83,'2018-10-29'),(295,18,10,84,4,'2017-03-07'),(296,17,5,11,2,'2018-08-24'),(297,7,3,30,76,'2017-07-24'),(298,15,5,21,82,'2018-07-21'),(299,2,20,19,40,'2017-03-22'),(300,20,16,61,25,'2017-03-15');
/*!40000 ALTER TABLE `intercambio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `motivador`
--

DROP TABLE IF EXISTS `motivador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motivador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) DEFAULT NULL,
  `fecha_inscripcion` date DEFAULT NULL,
  `total_pokidex` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `motivador`
--

LOCK TABLES `motivador` WRITE;
/*!40000 ALTER TABLE `motivador` DISABLE KEYS */;
INSERT INTO `motivador` VALUES (1,'morado',NULL,1),(2,'burgundí',NULL,1),(3,'turquesa',NULL,1),(5,'PeachYellow','2018-07-17',73),(6,'MediumAquamarine','2017-02-19',48),(7,'Taupe','2018-11-04',49),(8,'Cinnabar','2017-07-22',90),(9,'Khaki','2017-03-24',4),(10,'DarkCoral','2019-03-31',12),(11,'YellowGreen','2017-08-10',67),(12,'MediumBlue','2017-02-02',31),(13,'Bone','2018-02-10',17),(14,'NavyBlue','2018-11-18',7),(15,'ZinnwalditeBrown','2017-04-20',60),(16,'TaupeGray','2018-03-27',57),(17,'LaSalleGreen','2017-04-04',52),(18,'Zaffre','2018-03-26',8),(19,'Cinnamon','2017-02-06',76),(20,'BottleGreen','2018-08-27',22),(21,'Citrine','2018-02-10',5),(22,'Tawny','2017-03-29',84),(23,'Pear','2017-12-05',4),(24,'RossoCorsa','2018-03-10',46);
/*!40000 ALTER TABLE `motivador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pokimon`
--

DROP TABLE IF EXISTS `pokimon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pokimon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) DEFAULT NULL,
  `nivel` int(11) DEFAULT '1',
  `ataque` int(11) DEFAULT NULL,
  `defensa` int(11) DEFAULT NULL,
  `id_especie` int(11) DEFAULT NULL,
  `id_motivador` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pokimon`
--

LOCK TABLES `pokimon` WRITE;
/*!40000 ALTER TABLE `pokimon` DISABLE KEYS */;
INSERT INTO `pokimon` VALUES (1,'juancho',1,5,5,1,3),(2,'donatello',1,5,5,2,2),(3,'goyo',1,5,5,3,1),(4,'miguelito',1,50,50,1,1),(5,'boo',1,30,40,5,1),(6,'bigotes',1,5,5,4,2),(7,'Allene Angel  ',54,972,1242,36,1),(8,'Lord Adalberto Oates',6,132,180,26,2),(9,'Claud Stuart  ',63,441,1323,73,3),(10,'Oaneqi   ',63,NULL,NULL,100,4),(11,'Sr. Hemidiba Liku  ',42,210,714,95,5),(12,'Sir Daqe   ',60,420,720,8,6),(13,'Hoti Hess  ',91,2548,1274,38,7),(14,'Lord Adela Woodley  ',7,28,203,80,8),(15,'Cleo   ',17,238,136,4,9),(16,'Blaine Ee  ',12,60,36,53,10),(17,'Sr. Lulu Lawler  ',56,1512,1512,47,11),(18,'Apryl Fe  ',72,1728,792,77,12),(19,'Don Nikobe Stubblefi',56,56,1176,79,13),(20,'Arlen   ',12,216,12,71,14),(21,'Vomado Iukawamozo  ',78,2340,1092,29,15),(22,'Iukiza   ',43,129,344,13,16),(23,'Doña Taso Hester  ',93,186,372,32,17),(24,'Sir Karleen   ',50,1450,800,63,18),(25,'Caroline Pekoko  ',95,2755,1235,9,19),(26,'Aleta   ',95,2090,2850,26,20),(27,'Dr. Germaine   ',58,58,174,86,21),(28,'Dr. Graig   ',9,225,81,55,22),(29,'Xusoheya   ',48,1056,1296,18,23),(30,'Juju   ',78,1794,1872,99,1),(31,'Queenie   ',14,98,168,8,2),(32,'Sr. Ya   ',46,690,1242,34,3),(33,'Lord Jame Yo  ',31,713,744,99,4),(34,'Doña Bruce Brumfield',3,9,18,10,5),(35,'Loraine Uatici  ',6,60,96,42,6),(36,'Sene Kucazoxa  ',23,NULL,NULL,6,7),(37,'Dr. Genaro   ',93,2139,1767,57,8),(38,'Nicolette Pucexa  ',3,12,84,40,9),(39,'Iigeme   ',76,1368,1748,36,10),(40,'Rene Anglin  ',27,594,54,21,11),(41,'Darcie Lala  ',50,500,450,72,12),(42,'Bose Rock  ',71,426,781,28,13),(43,'Lord Foge   ',7,210,70,17,14),(44,'Vigili   ',10,90,220,76,15),(45,'Hivapu   ',82,1394,820,65,16),(46,'Wulahu Lawless  ',44,176,1232,40,17),(47,'Jack   ',8,240,24,88,18),(48,'Kirstin Dehart  ',98,2352,1470,30,19),(49,'Roman Bruner  ',69,1035,690,1,20),(50,'Abigail   ',66,594,1782,20,21),(51,'Aeloki Bedi  ',29,348,348,44,22),(52,'Tinisha   ',24,NULL,NULL,100,23),(53,'Don Coralie   ',93,1116,1209,3,1),(54,'Iasu   ',49,833,686,52,2),(55,'Dr. Brigette   ',79,2370,790,17,3),(56,'Sir Pohome   ',8,16,32,32,4),(57,'Uasola Hewitt  ',80,2240,1440,83,5),(58,'Sra. Kake Pa  ',15,240,105,82,6),(59,'Sir Yawiqi   ',11,198,220,5,7),(60,'Don Reyo   ',8,136,184,67,8),(61,'Marcelo   ',76,228,1900,61,9),(62,'Miga Nazupi  ',4,60,68,96,10),(63,'Lady Zefezedi   ',56,224,1568,40,11),(64,'Lord Deanna Obrien  ',100,700,1200,8,12),(65,'Maboru   ',60,540,1320,76,13),(66,'Hegofiri   ',12,360,120,17,14),(67,'Oezuji   ',9,27,225,61,15),(68,'Abel   ',50,900,50,71,16),(69,'Don Qe Qoruvovu  ',91,2093,2184,99,17),(70,'Tommy   ',69,414,759,28,18),(71,'Armanda   ',15,120,120,69,19),(72,'Quniwu Romo  ',48,1440,480,17,20),(73,'Resu   ',81,2349,1053,9,21),(74,'Don Adolfo   ',7,168,105,30,22),(75,'Keila Loperexu  ',23,23,23,45,23),(76,'Comanipu   ',75,1275,750,65,1),(77,'Puta Gale  ',62,1054,620,65,2),(78,'Lady Rey   ',1,4,29,80,3),(79,'Kukadelo Angulo  ',3,6,66,11,4),(80,'Sir Tokunesu   ',92,2484,828,94,5),(81,'Albert   ',10,190,140,50,6),(82,'Cyril   ',52,936,1040,5,7),(83,'Bi   ',8,16,48,87,8),(84,'Lady Kocepi Rockwell',45,990,1350,26,9),(85,'Merlin   ',34,136,1020,81,10),(86,'Rosife   ',87,261,2523,56,11),(87,'Iojija Wetiya  ',64,192,1600,61,12),(88,'Fusuhuni Lawrence  ',85,1530,85,71,13),(89,'Lady Cibu   ',65,1040,455,82,14),(90,'Lady Trenton Obryan ',49,196,1470,81,15),(91,'Lady Kupaki   ',51,816,357,82,16),(92,'Caleb Hiatt  ',81,324,486,78,17),(93,'Sir Brady   ',95,380,2660,40,18),(94,'Don Michael Vokame  ',53,1166,1590,26,19),(95,'Reanna   ',80,640,640,69,20),(96,'Bruno   ',81,1539,1944,43,21),(97,'Toby Woodruff  ',42,126,1050,61,22),(98,'Lady Gege   ',89,2670,890,17,23),(99,'Doña Lorina   ',6,60,96,42,1);
/*!40000 ALTER TABLE `pokimon` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 trigger stats before insert on pokimon for each row
begin
set new.ataque=new.nivel*(select ataque_base from especie where new.id_especie=especie.id);
set new.defensa=new.nivel*(select defensa_base from especie where new.id_especie=especie.id);

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-01 23:19:11
