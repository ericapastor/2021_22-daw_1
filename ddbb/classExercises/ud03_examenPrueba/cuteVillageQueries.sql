use cute_village;

/*1 Nombre y color de pelo de todos los avatares con expresión 'pensativo' (0.5ptos)*/
select nombre as avatar, color
from avatar
where expresion_facial = 'pensativo'
order by nombre;

/*2 Nombre de los avatares que viven en 'El pequeño Jolgorio’ (1pto)*/
select avatar.nombre as avatar
from avatar_casa
join avatar on avatar.id=avatar_casa.id_avatar
join casa on casa.id=avatar_casa.id_casa
where casa.nombre = 'El pequeño Jolgorio';

/*3 Nombre de las prendas de la marca Gloom Butterfly (0.5ptos)*/
select nombre as nombre_prendas
from ropa
where ropa.descripcion like '%Gloom Butterfly%';

/*4 ¿Cuántos avatares entraron a vivir a una casa en cada mes durante 2019? (1pto)*/
select count(*), monthname(avatar_casa.fecha_entrada)
from avatar_casa
where year(avatar_casa.fecha_entrada) = 2019
group by month(avatar_casa.fecha_entrada);

/*5 Nombre de los avatares con más de 6 objetos (1pto)*/
select avatar.nombre as avatar, count(objeto.id) as objetos
from avatar_objeto
join avatar on avatar.id = avatar_objeto.id_avatar
join objeto on objeto.id = avatar_objeto.id_objeto
group by avatar.id
having count(objetos)>6;

/*6 ¿Quiénes fueron adoptados durante los últimos 19 meses? (1pto)*/
select avatar.nombre as avatar_adoptado, avatar_relacion_avatar.fecha
from avatar_relacion_avatar
join avatar on avatar.id = avatar_relacion_avatar.id_avatar_recibe
where avatar_relacion_avatar.tipo = 'adopta'
and avatar_relacion_avatar.fecha > date_sub(curdate(), interval 19 month);

/*7 ¿Cuántos objetos posee cada avatar? Ten en cuenta que el Sr. Rococi tiene 0 objetos (1pto)*/
select avatar.nombre, count(objeto.id)
from avatar_objeto
right join avatar on avatar.id = avatar_objeto.id_avatar
left join objeto on objeto.id = avatar_objeto.id_objeto
group by avatar.id;

/*8 Nombre de los avatares que no tienen ninguna herramienta (1pto)*/
select avatar.nombre
from avatar
where avatar.id not in (select avatar.id
 from avatar_objeto
join avatar on avatar_objeto.id_avatar=avatar.id
join objeto on avatar_objeto.id_objeto=objeto.id 
where objeto.tipo='herramienta');

/*9 Nombre y descripción del modelo de sombrero más usado por los avatares (1pto)*/
select ropa.nombre as sombrero, ropa.descripcion as descripcion, count(avatar.id) numSobr
from ropa
join sombrero on sombrero.id_ropa = ropa.id
join avatar on sombrero.id = avatar.id_sombrero
join ((select count(avatar.id) as numAvatares, ropa.id as idRopa, avatar.id as avatar
	from ropa
	join sombrero on sombrero.id_ropa = ropa.id
	join avatar on sombrero.id = avatar.id_sombrero
	group by sombrero.id) as sombreros)
on sombreros.idRopa = ropa.id
having count(numSobr) = max(sombreros.numAvatares);