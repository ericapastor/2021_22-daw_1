use sobrenatural;
/* 1 - Nombre de los seres con más de 200 años (0.5ptos)*/
select nombre from ser where edad > 200;

/* 2 - Número de misiones realizadas en los últimos 3 años (0.95ptos)*/
select count(*)
from mision
where mision.fecha_inicio > date_sub(curdate(),interval 4 year);

/* 3 - Edad media de los seres de cada especie(0.95ptos)*/
select round(avg(ser.edad))
from ser
join especie on especie.id = ser.id_especie
group by especie.id;

/* 4 - Especie, nombre y descripción del ser de mayor edad(0.95ptos)*/
select ser.nombre as ser, especie.nombre as especie, ser.descripcion_fisica as descripcion_del_ser, especie.descripcion as descripcion_de_su_especie
from ser
join especie on especie.id = ser.id_especie
where ser.edad = (select max(ser.edad) from ser);

/* 5 - Nombre de los seres y especie a la que pertenecen aquellos seres vulnerables al
hechizo 'símbolo arcano' (9 resultados) (0.95ptos)*/
select ser.nombre as ser, especie.nombre as espcie
from ser
join especie on especie.id = ser.id_especie
join vulnerabilidad_especie on vulnerabilidad_especie.id_especie = especie.id
join vulnerabilidad on vulnerabilidad.id = vulnerabilidad_especie.id_vulnerabilidad
where vulnerabilidad.nombre = 'símbolo arcano';

/* 6 - ¿Cuántos seres de cada especie fueron objetivo de una misión de caza durante el
año pasado? Las especies que no fueron objetivo no hace falta tenerlas en
cuenta(0.95ptos)*/
select count(ser.id) as num_de_seres, especie.nombre as especie
from ser
join especie on especie.id = ser.id_especie
join mision_ser on ser.id = mision_ser.id_ser
join mision on mision.id = mision_ser.id_mision
where mision.objetivo = 'caza'
and mision.fecha_inicio > date_sub(curdate(), interval 2 year)
group by especie.id;

/* 7 - Di la cantidad de hechizos que contiene cada libro, ten en cuenta que 'el arcano
conocimiento de los cielos' tiene 0 hechizos(0.95ptos)*/
select libro.titulo, count(hechizo.id)
from libro
left join hechizo on hechizo.id_libro = libro.id
group by libro.id;

/* 8 - Fecha de inicio de cada misión, objetivo y número de licántropos relacionados con
ella (Ten en cuenta que habrá muchas misiones con 0 licántropos)(0.95ptos)*/
select mision.fecha_inicio, mision.objetivo as objetivo,
count(especie.id) as numero_de_licantropos
from mision
left join mision_ser on mision.id = mision_ser.id_mision
left join ser on ser.id = mision_ser.id_ser
left join especie on ser.id_especie = especie.id
left join ((select especie.id as idEspecie from especie where especie.nombre = 'Licántropo') as licantropos)
	on licantropos.idEspecie = ser.id_especie
group by mision.id;

/* 9 - Nombre y especialidad de aquellos agentes que nunca han visto un Vampiro,
Licántropo o Profundo en una misión (0.95ptos)*/
select agente.nombre as agente, agente.especialidad as especialidad_del_agente
from agente_mision
join agente on agente.id = agente_mision.id_agente
join mision_ser on agente_mision.id_mision = mision_ser.id_mision
join ser on ser.id = mision_ser.id_ser
join especie on especie.id = ser.id_especie
where especie.nombre not in (select nombre
from especie
where nombre = 'Vampiro ' || nombre  = 'Licántropo' || nombre  = 'Profundo');

/* 10 - Nombre del agente que más misiones de 'información' ha realizado. (0.95ptos)*/
select agente.nombre
from agente_mision
join agente on agente.id = agente_mision.id_agente
join mision on mision.id = agente_mision.id_mision
where mision.objetivo = 'información'
group by agente.id
having count(*) = (select max(numMisionesInfo.misiones) from (select count(mision.id) as misiones, agente.id
	from mision
	join agente_mision on mision.id = agente_mision.id_mision
	join agente on agente.id = agente_mision.id_agente
	where mision.objetivo = 'información'
	group by agente.id) as numMisionesInfo);

/* 11 - Nombre del agente y nombre y especie del ser que más veces han coincidido en
una misión, así como el número de veces que han coincidido (Puede haber más de
un resultado)(0.95ptos)*/
select agente.nombre as agente, ser.nombre as nombre_del_ser,
especie.nombre as especie_del_ser
from agente
join agente_mision on agente.id = agente_mision.id_agente
join mision_ser on mision_ser.id_mision = agente_mision.id_mision
join ser on ser.id = mision_ser.id_ser
join especie on ser.id_especie = especie.id
group by agente.id
having count(*) = (select max(numEncuentros.encuentros)
	from (select count(*) as encuentros, ser.id as ser, agente.id as agente
	from agente_mision
	join agente on agente.id = agente_mision.id_agente
	join mision_ser on mision_ser.id_mision = agente_mision.id_mision
	join ser on ser.id = mision_ser.id_ser
	group by agente.id) as numEncuentros);


select count(*), ser.id, agente.id
from agente_mision
join agente on agente.id = agente_mision.id_agente
join mision_ser on mision_ser.id_mision = agente_mision.id_mision
join ser on ser.id = mision_ser.id_ser
group by agente.id;



