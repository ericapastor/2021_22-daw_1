use cazamonstruos;

/*1 Procedimiento que asigna un objeto como recompensa de una misión*/
drop procedure if exists asignarRecompensa;
delimiter |~|
create procedure asignarRecompensa(idObj int, idMision int)
begin
	declare exit handler for sqlexception
    begin
		rollback;
        select 'No se ha podido asignar el objeto' as algo_salio_mal;
    end;
	if (select count(*) from objeto where id = idObj) < 1
		then select 'El ID de objeto no existe' as obj_no_existe;
	elseif (select count(*) from mision where id = idMision) < 1
		then select 'El ID de mision no existe' as mision_no_existe;
	elseif (select count(*) from mision_objeto where id_mision = idMision and id_objeto = idObj) > 0
		then select 'Ese objeto ya esta asignado a esa mision' as estas_borracho;
    else
		start transaction;
		insert into mision_objeto(id_mision, id_objeto)
        values(idMision, idObj);
        select 'Se ha asignado el objeto correctamente' as ole_tu;
		commit;
    end if;
end;
|~|
delimiter ;

/*2 Función que comprueba si una misión da como recompensa un arma concreta. En
caso afirmativo devuelve 1, sino 0. Devolverá -1 si el arma o la misión no existen.*/
drop function if exists armamia;
delimiter |~|
create function armamia(idMision int, idArma int)
returns int
begin
	if (select count(*) from objeto where id = idArma) < 1 
    or (select count(*) from mision where id = idMision) < 1
		then return -1;
	elseif (select count(*) from mision_objeto
    join objeto on objeto.id = mision_objeto.id_objeto
    join arma on objeto.id = arma.id_objeto
    where arma.id = idArma and arma.id_objeto = mision_objeto.id_objeto
    and mision_objeto.id_mision = idMision) > 0
		then return 1;
	else
		return 0;
	end if;
end;
|~|
delimiter ;

/*3 Función que comprueba si el tamaño de un monstruo se encuentra entre el máximo
y el mínimo de su especie. Devuelve 1 en caso afirmativo, 0 si está fuera del rango.
Devolverá -1 si el monstruo o la especie no existen.*/
drop function if exists tamanoMonst;
delimiter |~|
create function tamanoMonst(idMonst int)
returns int
begin
	if (select count(*) from monstruo where id = idMonst) < 1
		then return -1;
	elseif ((select tamano from monstruo where id = idMonst) >=
    (select tamano_min from especie
    join monstruo on monstruo.id_especie = especie.id
    where monstruo.id = idMonst))
    and ((select tamano from monstruo where id = idMonst) <=
    (select tamano_max from especie
    join monstruo on monstruo.id_especie = especie.id
    where monstruo.id = idMonst))
		then return 1;
	else
		return 0;
    end if;
end;
|~|
delimiter ;
-- uno que devuelva 1:
select tamanoMonst(845);

/*4 Añade el campo total_objetivos a la tabla misión que lleve la cuenta del total de
monstruos que son objetivo de una misión. Programa los triggers que se encargan de
mantener el campo actualizado*/
alter table mision add column total_objetivos int default 0;
-- funcion que devuelve cuantos monstruos hay en una mision
drop function if exists totalObjetivos;
delimiter |~|
create function totalObjetivos(idMision int)
returns int
begin
	if (select count(*) from mision where id = idMision) < 1
		then return -1;
	else
		return (select count(*) from monstruo_mision
        where id_mision = idMision);
    end if;
end;
|~|
delimiter ;
-- trigger de insert
drop trigger if exists insMonsMision;
delimiter |~|
create trigger insMonsMision after insert on monstruo_mision for each row
begin
	update mision
    set total_objetivos = totalObjetivos(new.id_mision)
    where id = new.id_mision;
end;
|~|
delimiter ;
-- trigger para update
drop trigger if exists upMonsMision;
delimiter |~|
create trigger upMonsMision after update on monstruo_mision for each row
begin
	if old.id_mision <> new.id_mision
	then
		update mision
		set total_objetivos = totalObjetivos(new.id_mision)
		where id = new.id_mision;
		update mision
		set total_objetivos = totalObjetivos(old.id_mision)
		where id = old.id_mision;
    end if;
end;
|~|
delimiter ;
-- trigger para delete
drop trigger if exists delMonsMision;
delimiter |~|
create trigger delMonsMision after delete on monstruo_mision for each row
begin
	update mision
    set total_objetivos = totalObjetivos(old.id_mision)
    where id = old.id_mision;
end;
|~|
delimiter ;

/*5 Programa los triggers necesarios para que el tamaño de un monstruo esté
comprendido entre el tamaño min y el tamaño max de su especie. Si el valor es menor lo
sustituirá por tamaño min y si es mayor por tamaño max*/
-- trigger para insert
drop trigger if exists insMonstruo;
delimiter |~|
create trigger insMonstruo before insert on monstruo for each row
begin
	if new.tamano > (select tamano_max from especie
	where especie.id = new.id_especie)
	then
		set new.tamano = (select tamano_max from especie
		where especie.id = new.id_especie);
	elseif new.tamano < (select tamano_min from especie
	where especie.id = new.id_especie)
	then
		set new.tamano = (select tamano_min from especie
		where especie.id = new.id_especie);
	end if;
end;
|~|
delimiter ;

/*6 Programa la función que dado un objeto nos devuelva el total del coste de sus
materiales. Si el objeto no existe devolverá -1*/
drop function if exists costeObj;
delimiter |~|
create function costeObj(idObj int)
returns int
begin
	if (select count(*) from objeto where id = idObj) < 1
		then return -1;
	else return
		(select sum(material.precio_venta
        * material_objeto.cantidad)
        from material_objeto
        join material on material.id 
        = material_objeto.id_material
        where material_objeto.id_objeto = 52);
    end if;
end;
|~|
delimiter ;