DROP DATABASE if EXISTS cazamonstruos;

CREATE DATABASE cazamonstruos;

USE cazamonstruos;

CREATE TABLE especie(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(15) UNIQUE,
tamano_max FLOAT UNSIGNED,
tamano_min FLOAT UNSIGNED,
debilidad ENUM('Fuego', 'Agua', 'Veneno', 'Diplomacia', 'Abracitos'),
comportamiento ENUM('agresivo', 'neutral', 'huidizo')
);

CREATE TABLE mapa(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR (20) UNIQUE,
nivel_dificultad TINYINT UNSIGNED
);

CREATE TABLE clima(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_mapa INT UNSIGNED NOT NULL,
FOREIGN KEY (id_mapa) REFERENCES mapa(id),
clima VARCHAR(15)
);

CREATE TABLE monstruo(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_especie INT UNSIGNED,
id_mapa INT UNSIGNED,
FOREIGN KEY (id_especie) REFERENCES especie(id),
FOREIGN KEY (id_mapa) REFERENCES mapa(id),
color VARCHAR(10),
tamano INT UNSIGNED,
coordenadaX FLOAT,
coordenadaY FLOAT,
coordenadaZ FLOAT
);

CREATE TABLE material(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_especie INT UNSIGNED NOT NULL,
FOREIGN KEY (id_especie) REFERENCES especie(id),
nombre VARCHAR(16) UNIQUE,
precio_venta INT UNSIGNED,
rareza ENUM ('1', '2', '3', '4', '5') DEFAULT 1
);

CREATE TABLE mision(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_mapa INT UNSIGNED NOT NULL,
FOREIGN KEY (id_mapa) REFERENCES mapa(id),
nombre VARCHAR(15) UNIQUE,
monstruos_que_aparecen VARCHAR(300)
);

CREATE TABLE objeto(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(20),
descripcion VARCHAR(200)
);

CREATE TABLE arma(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_objeto INT UNSIGNED NOT NULL,
FOREIGN KEY (id_objeto) REFERENCES objeto(id),
elemento ENUM ('agua', 'fuego', 'veneno'),
valor_ataque TINYINT UNSIGNED,
alcance TINYINT UNSIGNED
);

CREATE TABLE armadura(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_objeto INT UNSIGNED NOT NULL,
FOREIGN KEY (id_objeto) REFERENCES objeto(id),
valor_defensa TINYINT UNSIGNED,
parte_cuerpo_protege ENUM('cabeza', 'torso', 'brazos', 'piernas')
);

CREATE TABLE consumible(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_objeto INT UNSIGNED NOT NULL,
FOREIGN KEY (id_objeto) REFERENCES objeto(id),
efecto VARCHAR(15),
duracion TIME
);

CREATE TABLE monstruo_mision(
id_monstruo INT UNSIGNED NOT NULL,
id_mision INT UNSIGNED NOT NULL,
FOREIGN KEY (id_monstruo) REFERENCES monstruo(id),
FOREIGN KEY (id_mision) REFERENCES mision(id),
PRIMARY KEY (id_monstruo, id_mision)
);

CREATE TABLE mision_objeto(
id_mision INT UNSIGNED NOT NULL,
id_objeto INT UNSIGNED NOT NULL,
FOREIGN KEY (id_mision) REFERENCES mision(id),
FOREIGN KEY (id_objeto) REFERENCES objeto(id),
PRIMARY KEY (id_mision, id_objeto)
);

CREATE TABLE objeto_material(
id_objeto INT UNSIGNED NOT NULL,
id_material INT UNSIGNED NOT NULL,
FOREIGN KEY (id_objeto) REFERENCES objeto(id),
FOREIGN KEY (id_material) REFERENCES material(id),
PRIMARY KEY (id_objeto, id_material),
materiales_necesarios VARCHAR(100),
suma_materiales TINYINT UNSIGNED
);

CREATE TABLE cazadora_cadaza(
id_cazadora INT UNSIGNED NOT NULL,
id_cazada INT UNSIGNED NOT NULL,
FOREIGN KEY (id_cazadora) REFERENCES especie(id),
FOREIGN KEY (id_cazada) REFERENCES especie(id)
);

