DROP DATABASE if EXISTS examen;

CREATE DATABASE examen;

USE examen;

CREATE TABLE usuario(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(15) NOT NULL,
nombre_real VARCHAR(15),
contrasenha VARCHAR(10) NOT NULL 
);

CREATE TABLE mecenas(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_usuario INT UNSIGNED NOT NULL,
FOREIGN KEY (id_usuario) REFERENCES usuario(id),
n_tarjeta_credito VARCHAR (24)
);

CREATE TABLE creador(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_usuario INT UNSIGNED NOT NULL,
FOREIGN KEY (id_usuario) REFERENCES usuario(id),
dni CHAR(9),
cuenta_bancaria VARCHAR(24) NOT NULL
);

CREATE TABLE campanha(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
id_creador INT UNSIGNED NOT NULL,
FOREIGN KEY (id_creador) REFERENCES creador(id),
objetivo INT UNSIGNED NOT NULL,
nombre VARCHAR(15) NOT NULL,
descripcion VARCHAR(200),
duracion INT DEFAULT 40
);

CREATE TABLE recompensa(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(15) NOT NULL,
cantidad INT UNSIGNED NOT NULL,
envio TINYINT(1) NOT NULL
);

CREATE TABLE mecenas_campanha(
id_mecenas INT UNSIGNED,
id_campanha INT UNSIGNED,
FOREIGN KEY (id_mecenas) REFERENCES mecenas(id),
FOREIGN KEY (id_campanha) REFERENCES campanha(id),
aportacion INT UNSIGNED NOT NULL,
tipo_aportacion ENUM('credito', 'debito', 'transferencia')
);