use cazamonstruos;

/*1 Nombre de todas las especies cuyo comportamiento sea
‘huidizo’ (0.5ptos)*/
select nombre
from especie
where comportamiento = 'huidizo';

/*2 Nombre y recompensa de las misiones que comenzaron el año
pasado (0.7ptos)*/
select nombre, recompensa
from mision
where year(fecha_inicio) = '2021';

/*3 Objetos que se obtenían como recompensa en las misiones que
empezaron durante los dos últimos años (0.7ptos)*/
select objeto.nombre
from objeto
join mision_objeto on mision_objeto.id_objeto = objeto.id
join mision on mision_objeto.id_mision = mision.id
where mision.fecha_inicio > date_sub(curdate(), interval 2 year)
group by objeto.id;

/*4 ¿Cuántos monstruos hay de cada especie? (0.7ptos)*/
select especie.nombre, count(*) as numero_de_monstruos
from especie
join monstruo on monstruo.id_especie = especie.id
group by especie.id;

/*5 ¿Cuántas misiones se desarrollan en mapas de nivel 15?
(0.7ptos)*/
select count(*) as numMisionesNivel15
from mapa
join mision on mision.id_mapa = mapa.id
where mapa.nivel = 15;

/*6 ¿Cuál es la media de tamaño de los monstruos de cada
especie? (0.7ptos)*/
select especie.nombre, avg(monstruo.tamano)
from especie
join monstruo on especie.id = monstruo.id_especie
group by especie.id;

/*7 Nombre de los distintos materiales necesarios para fabricar
cada arma (0.7ptos)*/
select material.nombre as material,
material_objeto.cantidad as cantidad, arma.id as id_arma
from arma
join objeto on objeto.id = arma.id_objeto
join material_objeto on objeto.id = material_objeto.id_objeto
join material on material.id = material_objeto.id_material
order by arma.id asc;

/*8 Suma de la cantidad de materiales que hacen falta para
realizar cada consumible (0.7ptos)*/
select consumible.id as id_consumible, objeto.nombre,
sum(material_objeto.cantidad) as cantidad_materiales
from consumible
join objeto on objeto.id = consumible.id_objeto
join material_objeto on objeto.id = material_objeto.id_objeto
join material on material.id = material_objeto.id_material
group by consumible.id;

/*9 ¿En qué mapas se realizan las misiones que pueden dar como
recompensa el arma ‘El temor de Gaia’? (0.7ptos)*/
select mapa.nombre as mapa
from mapa
join mision on mapa.id = mision.id_mapa
join mision_objeto on mision.id = mision_objeto.id_mision
join objeto on objeto.id = mision_objeto.id_objeto
where objeto.nombre = 'El temor de Gaia';


/*10 ¿Cuál es el material más útil? (Aquel que permite construir
mayor cantidad de objetos distintos) (0.8 ptos)*/
select material.nombre
from material_objeto
join objeto on objeto.id = material_objeto.id_objeto
join material on material.id = material_objeto.id_material
group by material.id
having count(*) = (select max(numObjetosPorMaterial.numObjetos) from (select count(*) as numObjetos
from material_objeto
join objeto on objeto.id = material_objeto.id_objeto
join material on material.id = material_objeto.id_material
group by material.id) as numObjetosPorMaterial);

/*11 ¿Cuál es la armadura más fácil de construir? (Aquella que
necesite menos componentes distintos par ser fabricada)
(0.8ptos)*/
select armadura.id as armadura, objeto.nombre
from armadura
join objeto on objeto.id = armadura.id_objeto
join material_objeto on objeto.id = material_objeto.id_objeto
join material on material.id = material_objeto.id_material
group by armadura.id
having count(*) = (select min(materialPorArmadura.numMateriales)
from (select armadura.id as armadura, count(*) as numMateriales
	from armadura
	join objeto on objeto.id = armadura.id_objeto
	join material_objeto on objeto.id = material_objeto.id_objeto
	join material on material.id = material_objeto.id_material
	group by armadura.id) as materialPorArmadura);

/*12 Lista de los materiales que no sirvan para fabricar
consumibles (0.8ptos)*/
select material.nombre
from material
where material.id not in (select material.id
	from material_objeto
    join material on material.id = material_objeto.id_material
	join objeto on objeto.id = material_objeto.id_objeto
	join consumible on objeto.id = consumible.id_objeto);
    
/*13 ¿A qué especie pertenece el monstruo más pequeño?
(0.8ptos)*/
select especie.nombre as especie
from especie
join monstruo on especie.id = monstruo.id_especie
where monstruo.tamano = (select min(pequenios.monstruosPeques)
	from (select monstruo.tamano as monstruosPeques from monstruo) as pequenios);

/*14 ¿Cuál es el material más común obtenido de especies
agresivas? (Aquel que más especies agresivas distintas dan)
(0.8ptos)*/
select material.nombre
from material_especie
join material on material.id = material_especie.id_material
join especie on especie.id = material_especie.id_especie
where especie.comportamiento = 'agresivo'
group by material.id
having count(*) = (select max(numAgros.numEspecies) from (select count(*) as numEspecies, material.nombre
	from material_especie
	join material on material.id = material_especie.id_material
	join especie on especie.id = material_especie.id_especie
	where especie.comportamiento = 'agresivo'
	group by material.id) as numAgros);