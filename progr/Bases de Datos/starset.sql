drop database if exists starset;
create database starset;
use starset;

create table producto(
  id varchar(6) primary key,
  nombre varchar(50),
  precio double
);

create table baraja(
  id_producto varchar(6) primary key,
  foreign key (id_producto) references producto (id),
  tipo enum ('tarot', 'oraculo')
);

create table cristal(
  id_producto varchar(6) primary key,
  foreign key (id_producto) references producto (id),
  pulido tinyint (1),
  forma varchar (20)
);

insert into producto(id, nombre, precio)
values
  ('BAR001', 'The Wild Unknown Animal Spirit Deck', 28.97),
  ('BAR002', 'Tarot OSHO Zen', 24.70),
  ('CRI001', 'Amatista', 5.00),
  ('CRI002', 'Ojo de Tigre', 2.00);

insert into baraja (id_producto, tipo)
values
  ('BAR001', 'Oraculo'),
  ('BAR002', 'Tarot');

insert into cristal (id_producto, pulido, forma)
values
  ('CRI001', 0, 'irregular'),
  ('CRI002', 1, 'corazon');

select id, nombre, precio, baraja.tipo, case
  when cristal.pulido = 0 then 'no'
  when cristal.pulido = 1 then 'si'
end as pulido, cristal.forma
from producto
left join baraja on baraja.id_producto = producto.id
left join cristal on cristal.id_producto = producto.id;
