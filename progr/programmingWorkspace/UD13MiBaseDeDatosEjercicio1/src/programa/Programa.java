package programa;

import clases.AppCompra;

public class Programa {

	public static void main(String[] args) {
		AppCompra app = new AppCompra();
		app.conectarBBDD();
		app.mostrarProductosBBDD();
	}

}
