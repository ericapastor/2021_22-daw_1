package clases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class AppCompra implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String datosPedidos = "src/datosPedidos.dat";
	private static final String datosProductos = "src/datosProductos.dat";
	private Connection conexion;

	private ArrayList<Pedido> pedidos;
	private ArrayList<Producto> productos;

	public AppCompra() {
		pedidos = new ArrayList<Pedido>();
		productos = new ArrayList<Producto>();
	}

	public ArrayList<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(ArrayList<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

	//
	// obtener
	//

	public Baraja obtenerBaraja(String id) {
		for (Producto p : productos) {
			if (p != null && p instanceof Baraja) {
				if (p.getId().equalsIgnoreCase(id)) {
					return (Baraja) p;
				}
			}
		}
		return null;
	}

	public Cristal obtenerCristal(String id) {
		for (Producto p : productos) {
			if (p != null && p instanceof Cristal) {
				if (p.getId().equalsIgnoreCase(id)) {
					return (Cristal) p;
				}
			}
		}
		return null;
	}

	public Pedido obtenerPedido(String id) {
		for (Pedido p : pedidos) {
			if (p != null && p.getId().equalsIgnoreCase(id)) {
				return p;
			}
		}
		return null;
	}

	//
	// altas
	//

	public void altaBaraja(String id, String nombre, double precio, TipoBaraja tipo) {
		if (obtenerBaraja(id) != null) {
			System.out.println("Ya existe una baraja con ese ID");
		} else {
			productos.add(new Baraja(id, nombre, precio, tipo));
			Collections.sort(productos);
		}
	}

	public void altaCristal(String id, String nombre, double precio, boolean pulido, String forma) {
		if (obtenerCristal(id) != null) {
			System.out.println("Ya existe un cristal con ese ID");
		} else {
			productos.add(new Cristal(id, nombre, precio, pulido, forma));
			Collections.sort(productos);
		}
	}

	public void altaPedido(String id, Baraja baraja, String fechaPedido) {
		if (obtenerPedido(id) != null) {
			System.out.println("Ya hay un pedido registrado con ese ID");
		} else {
			pedidos.add(new Pedido(id, baraja, fechaPedido));
			Collections.sort(pedidos);
		}
	}

	//
	// asignar cristales a pedido
	//

	public void asignarCristalesPedido(String idPedido, String idCristal) {
		if (obtenerPedido(idPedido) == null) {
			System.out.println("No existe ningun pedido con ese ID");
		} else if (obtenerCristal(idCristal) == null) {
			System.out.println("No existe ningun cristal con ese ID");
		} else {
			obtenerPedido(idPedido).getCristales().add(obtenerCristal(idCristal));
			Collections.sort(obtenerPedido(idPedido).getCristales());
		}
	}

	// guardar datos ficheros

	private void guardarPedidosFichero() throws FileNotFoundException, IOException {
		ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(datosPedidos));
		escritor.writeObject(pedidos);
		escritor.close();
	}

	private void guardarProductosFichero() throws FileNotFoundException, IOException {
		ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(datosProductos));
		escritor.writeObject(productos);
		escritor.close();

	}

	public void guardarDatosFichero() {
		try {
			guardarProductosFichero();
			guardarPedidosFichero();
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	// cargar datos de ficheros

	@SuppressWarnings("unchecked")
	private void cargarPedidosFichero() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream lector = new ObjectInputStream(new FileInputStream(datosPedidos));
		pedidos = (ArrayList<Pedido>) lector.readObject();
		lector.close();
	}

	@SuppressWarnings("unchecked")
	private void cargarProductosFichero() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream lector = new ObjectInputStream(new FileInputStream(datosProductos));
		productos = (ArrayList<Producto>) lector.readObject();
		lector.close();
	}

	public void cargarDatosFichero() {
		try {
			cargarPedidosFichero();
			cargarProductosFichero();
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	// base de datos

	public void conectarBBDD() {
		String server = "jdbc:mysql://localhost:3306/starset";
		try {
			conexion = DriverManager.getConnection(server, "root", "");
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void guardarBarajasBBDD() {
		String query1 = "insert into producto (id, nombre, precio) values (?, ?, ?);";
		String query2 = "insert into baraja (id_producto, tipo) values (?, ?);";
		try {
			PreparedStatement sentencia1 = conexion.prepareStatement(query1);
			PreparedStatement sentencia2 = conexion.prepareStatement(query2);
			for (Producto p : productos) {
				if (p != null && p instanceof Baraja) {
					sentencia1.setString(1, p.getId());
					sentencia1.setString(2, p.getNombre());
					sentencia1.setDouble(3, p.getPrecio());
					sentencia2.setString(1, p.getId());
					sentencia2.setString(2, ((Baraja) p).getTipo().toString());
					sentencia1.executeUpdate();
					sentencia2.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void guardarCristalesBBDD() {
		String query1 = "insert into producto (id, nombre, precio) values (?, ?, ?);";
		String query2 = "insert into cristal (id_producto, pulido, forma) values (?, ?, ?);";
		try {
			PreparedStatement sentencia1 = conexion.prepareStatement(query1);
			PreparedStatement sentencia2 = conexion.prepareStatement(query2);
			for (Producto p : productos) {
				if (p != null && p instanceof Cristal) {
					sentencia1.setString(1, p.getId());
					sentencia1.setString(2, p.getNombre());
					sentencia1.setDouble(3, p.getPrecio());
					sentencia2.setString(1, p.getId());
					sentencia2.setInt(2, (((Cristal) p).isPulido()) ? 1 : 0);
					sentencia2.setString(3, ((Cristal) p).getForma());
					sentencia1.executeUpdate();
					sentencia2.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void mostrarProductosBBDD() {
		String query = "select id, nombre, precio, baraja.tipo, case when cristal.pulido "
				+ "= 0 then 'no' when cristal.pulido = 1 then 'si' end as pulido, cristal.forma "
				+ "from producto left join baraja on baraja.id_producto = producto.id "
				+ "left join cristal on cristal.id_producto = producto.id;";
		try {
			PreparedStatement sen = conexion.prepareStatement(query);
			ResultSet res = sen.executeQuery();
			while (res.next()) {
				boolean baraja = res.getString(1).substring(0, 3).equals("BAR");
				System.out.println("ID de producto: " + res.getString(1) + ", nombre: " + res.getString(2)
						+ ", precio: " + res.getString(3)
						+ (baraja ? ", tipo de baraja: " + res.getString(4)
								: ", el cristal esta pulido: " + res.getString(5) + ", forma del cristal: "
										+ res.getString(6)));
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

}
