package clases;

import java.io.Serializable;

public abstract class Producto implements Comparable<Producto>, Serializable {

	private static final long serialVersionUID = 1L;
	
	String id;
	String nombre;
	double precio;
	
	public Producto(String id, String nombre, double precio) {
		this.id=id;
		this.nombre=nombre;
		this.precio=precio;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

}
