package clases;

import java.io.Serializable;

public class Cristal extends Producto implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean pulido;
	private String forma;

	public Cristal(String id, String nombre, double precio, boolean pulido, String forma) {
		super(id, nombre, precio);
		this.pulido = pulido;
		this.forma = forma;
	}

	@Override
	public int compareTo(Producto o) {
		return id.compareTo(o.id);
	}

	@Override
	public String toString() {
		return "Cristal [pulido=" + pulido + ", forma=" + forma + super.toString() + "]";
	}

	public boolean isPulido() {
		return pulido;
	}

	public void setPulido(boolean pulido) {
		this.pulido = pulido;
	}

	public String getForma() {
		return forma;
	}

	public void setForma(String forma) {
		this.forma = forma;
	}

}
