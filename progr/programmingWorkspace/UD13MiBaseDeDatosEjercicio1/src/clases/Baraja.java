package clases;

import java.io.Serializable;

public class Baraja extends Producto implements Serializable {

	private static final long serialVersionUID = 1L;

	private TipoBaraja tipo;

	public Baraja(String id, String nombre, double precio, TipoBaraja tipo) {
		super(id, nombre, precio);
		this.tipo = tipo;
	}

	@Override
	public int compareTo(Producto o) {
		return id.compareTo(o.id);
	}

	@Override
	public String toString() {
		return "Baraja [tipo=" + tipo + ", toString()=" + super.toString() + "]";
	}

	public TipoBaraja getTipo() {
		return tipo;
	}

	public void setTipo(TipoBaraja tipo) {
		this.tipo = tipo;
	}

}
