package clases;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

public class Pedido implements Comparable<Pedido>, Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private Baraja baraja;
	private Date fechaPedido;
	private ArrayList<Cristal> cristales;

	public Pedido(String id, Baraja baraja, String fechaPedido) {
		this.id = id;
		this.baraja = baraja;
		this.fechaPedido = Date.valueOf(fechaPedido);
		cristales = new ArrayList<Cristal>();
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "Pedido [id=" + id + ", cristales="
				+ (cristales != null ? cristales.subList(0, Math.min(cristales.size(), maxLen)) : null) + "]";
	}

	@Override
	public int compareTo(Pedido o) {
		return id.compareTo(o.id);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Baraja getBaraja() {
		return baraja;
	}

	public void setBaraja(Baraja baraja) {
		this.baraja = baraja;
	}

	public Date getFechaPedido() {
		return fechaPedido;
	}

	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

	public ArrayList<Cristal> getCristales() {
		return cristales;
	}

	public void setCristales(ArrayList<Cristal> cristales) {
		this.cristales = cristales;
	}

}
