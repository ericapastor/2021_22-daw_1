package clases;

public enum TipoBaraja {

	TAROT, ORACULO;

	public String toString() {
		switch (this) {
		case TAROT:
			return "tarot";
		case ORACULO:
			return "oraculo";
		default:
			return null;
		}
	}

}
