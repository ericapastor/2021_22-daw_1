package ej03ClasesAbstractas;

public abstract class FiguraGeometrica {
	// la abstraccion permite resaltar lo mas representativo de una clase sin dar
		// detalles
		// una clase abstracta es similar a una normal salvo que al menos uno de sus
		// metodos es abstracto
		// su uso depende de la aplicacion del concepto de herencia
		// aņadiremos la palabra reservada abstract
		// la abstraccion permite mantener las aplicaciones mas organizadas
		// si heredo de una clase abstracta estoy obligado a implementar sus metodos
		String nombre;
		
		public FiguraGeometrica() {
			this.nombre="";
		}

		public FiguraGeometrica(String nombre) {
			this.nombre = nombre;
		}
		
		//metodos abstractos area y perimetro
		// los metodos abstractos no llevan contenido (no llevan { } )
		public abstract double area();
		public abstract double perimetro();
		
		public String dimeMiNombre() {
			return nombre;
		}
}
