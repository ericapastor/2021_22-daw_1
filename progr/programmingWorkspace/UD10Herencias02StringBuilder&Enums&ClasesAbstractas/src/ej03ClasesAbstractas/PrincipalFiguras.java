package ej03ClasesAbstractas;

public class PrincipalFiguras {

	public static void main(String[] args) {
		// no puedo instancia la clase FiguraGeometrica
		Circulo circulito = new Circulo(3);
		Triangulo triangulito = new Triangulo(20, 12);

		System.out.println("Nombre " + circulito.dimeMiNombre());
		System.out.println("Area circulo " + circulito.area());
		System.out.println("Nombre " + triangulito.dimeMiNombre());
		System.out.println("Area triangulo " + triangulito.area());

	}

}
