package ej01StringBuilder;

public class PrincipalStringBuilder2 {
	
	public static void main(String[] args) {
		String cadena = "1234567890";
		System.out.println("Sin separar " + cadena);
		cadena = separarMiles(cadena);
		System.out.println("Separando " + cadena);
	}

	public static String separarMiles(String s) {
		// creamos un StringBuilder a partir del String
		StringBuilder aux = new StringBuilder(s);
		// le damos la vuelta
		aux.reverse();
		// variable que indica donde debe insertar el siguiente punto
		int posicion = 3;
		// mientras no lleguemos al final de numero
		while (posicion < aux.length()) {
			// insertamos un punto en la posicion
			aux.insert(posicion, '.');
			// colocamos la posicion siguiente
			posicion += 4;
		}
		// le damos la vuelta
		aux.reverse();
		// devolvemos el String
		return aux.toString();
	}

}
