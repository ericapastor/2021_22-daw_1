package ej01StringBuilder;

public class PrincipalStringBuilder {
	
	public static void main(String[] args) {
		// StringBuilder es dinamica
		// a�adir, insertar, transformar desde un String, invertir...
		// pesa mas
		StringBuilder texto = new StringBuilder("una prueba\n");
		System.out.println("Texto: "+texto);
		texto.append(" mas");
		System.out.println("A�adimos: "+texto);
		texto.insert(2, "xxx");
		System.out.println("Insertamos: "+texto);
		texto.reverse();
		System.out.println("Invertimos: "+texto);
		System.out.println("En mayusculas: "+texto.toString().toUpperCase());
		texto.append("jajasi\n");
		texto.append("Nose\n");
		texto.append("qwerty\n");
		String t = texto.toString();
		System.out.println(t);
	}

}
