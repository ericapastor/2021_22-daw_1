package ej02Enums;

public class DiaEnumTest {
	// usando el enumerado
	// Clase nombreEnumerado que voy a usar
	Dias dia;

	public DiaEnumTest(Dias dia) {
		this.dia = dia;
	}

	public void dimeSiTeGusta() {
		switch (dia) {
		case LUNES:
			System.out.println("Los lunes son malos");
			break;
		case VIERNES:
			System.out.println("Los viernes son mejores");
			break;
		case SABADO:
		case DOMINGO:
			System.out.println("Los fines de semana me encantan");
			break;
		default:
			System.out.println("El resto de dias me dan igual");
			break;

		}
	}
}
