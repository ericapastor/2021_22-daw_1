package ej02Enums;

public class Principal {

	public static void main(String[] args) {
		DiaEnumTest primerDia = new DiaEnumTest(Dias.LUNES);
		primerDia.dimeSiTeGusta();
		DiaEnumTest segundoDia = new DiaEnumTest(Dias.MIERCOLES);
		segundoDia.dimeSiTeGusta();
		DiaEnumTest tercerDia = new DiaEnumTest(Dias.VIERNES);
		tercerDia.dimeSiTeGusta();
		DiaEnumTest cuartoDia = new DiaEnumTest(Dias.SABADO);
		cuartoDia.dimeSiTeGusta();
		DiaEnumTest quintoDia = new DiaEnumTest(Dias.DOMINGO);
		quintoDia.dimeSiTeGusta();
	}

}
