package ficherosRAF;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class EscribirFicheroRAF {

	// RAF
	// Random Access File
	// no guarda texto plano, guarda registros
	// permite acceder a una posicion determinada
	// Me puedo desplazar por los registros
	// puedo leer y escribir a la vez
	// modo
	// r -> read
	// w -> write
	// metodo escribir (writeInt, writeLong, writeDouble, writeBytes)
	// getFilePointer devuelve la posicion actual donde se va a realizar la
	// operacion
	// seek colocar el fichero en una posicion determinada
	// length tama�o archivo

	public static void main(String[] args) {
		try {
			// 1.- abrir el fichero en acceso RAF
			RandomAccessFile f = new RandomAccessFile("ej1.txt", "rw");
			// 2.- nos posicionamos al final del fichero
			f.seek(f.length());
			// 3.- escribimos una cadena de texto
			f.writeBytes("");
			char l;
			// me posiciono al principio del fichero
			f.seek(0);
			// 4.- leemos byte a byte
			boolean fin = false;
			while (!fin) {
				try {
					// hay que leer del mismo modo en que se escribio:
					// si se escribe en bytes, se lee en bytes
					// si se escribe en utf, se lee en utf
					// si se escriben ints, se leen ints
					// pueden ALTERNARSE: escribir un int, un float, luego un char
					// pero deben leerse en el orden correcto
					l = (char) f.readByte();
					System.out.print(l);
				} catch (EOFException e) {
					// 3.- fin fichero
					System.out.println("\nFin fichero");
					fin = true;
				}
			}
			// 5.- cerramos el fichero
			f.close();
			System.out.println("Fichero actualizado correctamente");
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}
	}

}
