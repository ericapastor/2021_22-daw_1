package copiarVesctorRAF;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

public class ListaProductos {
	// arrayList productos
	private ArrayList<Producto> v;

	public ListaProductos() {
		v = new ArrayList<Producto>();
	}

	// rellenar la lista de productos
	public void rellenarLista() {
		Scanner input = new Scanner(System.in);
		String respuesta = "si";
		while (respuesta.equalsIgnoreCase("si")) {
			// creo un producto
			Producto unProducto = new Producto();
			// relleno un producto
			unProducto.rellenarProducto();
			// nuevo producto al vector
			v.add(unProducto);
			System.out.println("Desea continuar (si/no)");
			respuesta = input.nextLine();
		}
		input.close();
	}

	// visualizar productos
	public void visualizarLista() {
		for (Producto unProducto : v) {
			unProducto.visualizarProducto();
		}
	}

	// copiar lista a un archivo
	public void copiarListaAArchivo(String nombreArchivo) {
		try {
			// abrimos el archivo para escribir
			RandomAccessFile f = new RandomAccessFile(nombreArchivo, "rw");
			// posicionarse al final
			f.seek(f.length());
			// leo el vector y escribo en el archivo
			for (Producto unProducto : v) {
				f.writeInt(unProducto.getCodigo());
				f.writeDouble(unProducto.getPrecio());
			}
			// cierro el archivo
			f.close();
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

	// visualizar archivo
	public void visualizarArchivo(String archivo) {
		try {
			int codigo;
			double precio;
			// abrir el fichero
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			boolean finFichero = false;
			// recorre el archivo para mostrarlo
			do {
				try {
					codigo = (int) f.readInt();
					precio = (double) f.readDouble();
					System.out.println("Codigo= " + codigo);
					System.out.println("Precio= " + precio);
				} catch (EOFException e) {
					System.out.println("fin fichero");
					finFichero = true;
					f.close();
				}
			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

	// modificar precio archivo
	public void modificarPrecioArchivo(String archivo) {
		try {
			int codigo;
			double precio;
			// abrir el fichero
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			boolean finFichero = false;
			// recorre el archivo para cambiar precio
			do {
				try {
					codigo = (int) f.readInt();
					precio = (double) f.readDouble();
					// precio>100 -> precio-precio*50/100
					// sino -> precio+precio*500/100

					// posicionarme -> seek
					// llevar el puntero donde necesito getFilePointer
					// double ocupa 8 bytes
					if (precio > 100) {
						f.seek(f.getFilePointer() - 8);
						f.writeDouble(precio - precio * 50 / 100);
					} else {
						f.seek(f.getFilePointer() - 8);
						f.writeDouble(precio + precio * 500 / 100);
					}
				} catch (EOFException e) {
					System.out.println("fin fichero");
					finFichero = true;
					f.close();
				}
			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

}