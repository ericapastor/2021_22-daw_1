package copiarVesctorRAF;

public class Principal {

	public static void main(String[] args) {
		//crear el vector
		ListaProductos unaLista = new ListaProductos();
		//rellenar el vector
		System.out.println("Rellenamos el vector");
		unaLista.rellenarLista();
		System.out.println("Visualizar vector");
		unaLista.visualizarLista();
		//pasamos el vector al archivo
		System.out.println("Copiamos informacion al archivo");
		unaLista.copiarListaAArchivo("datos2");
		System.out.println("Mostramos datos del archivo");
		unaLista.visualizarArchivo("datos2");
		//modificar precios
		System.out.println("Modificando precios");
		unaLista.modificarPrecioArchivo("datos2");
		System.out.println("Mostrando datos del archivo modificado");
		unaLista.visualizarArchivo("datos2");
	}
	
}