package clases;

import java.time.LocalDate;

public class Laboratorio {

	private Medico[] medicos;
	private Paciente[] pacientes;

	public Laboratorio(int n, int l) {
		medicos = new Medico[n];
		pacientes = new Paciente[l];
	}

	public void altaPaciente(String nSS, String nombre, String apellidos, String resultado, Medico medico) {
		for (int i = 0; i < pacientes.length; i++) {
			if (pacientes[i] == null) {
				pacientes[i] = new Paciente(nSS, resultado);
				pacientes[i].setNombre(nombre);
				pacientes[i].setApellidos(apellidos);
				pacientes[i].setMedico(medico);
				pacientes[i].setFechaTest(LocalDate.now());
				break;
			}
		}
	}

	public void altaMedico(String nombre, String especialidad) {
		for (int i = 0; i < medicos.length; i++) {
			if (medicos[i] == null) {
				medicos[i] = new Medico(nombre, especialidad);
				break;
			}
		}
	}

	public Paciente buscarPaciente(String nSS) {
		for (int i = 0; i < pacientes.length; i++) {
			if (pacientes[i] != null) {
				if (pacientes[i].getnSS().equalsIgnoreCase(nSS)) {
					return pacientes[i];
				}
			}
		}
		return null;
	}

	public void eliminarPaciente(String nSS) {
		int count = 0;
		for (int i = 0; i < pacientes.length; i++) {
			if (pacientes[i] != null) {
				if (pacientes[i].getnSS().equalsIgnoreCase(nSS)) {
					pacientes[i] = null;
					count++;
					break;
				}
			}
		}
		if (count == 0) {
			System.out.println("  no se encontro un paciente con ese codigo");
		}
	}

	public void listarPacientes() {
		for (int i = 0; i < pacientes.length; i++) {
			if (pacientes[i] != null) {
				System.out.println(pacientes[i]);
			}
		}
	}

	public void cambiarResultadoPaciente(String nSS, String resultado) {
		for (int i = 0; i < pacientes.length; i++) {
			if (pacientes[i] != null) {
				if (pacientes[i].getnSS().equalsIgnoreCase(nSS)) {
					pacientes[i].setResultado(resultado);
				}
			}
		}
	}

	public void listarPacientePorResultado(String resultado) {
		System.out.println("  estos son los pacientes que han resultado " + resultado + ":");
		for (int i = 0; i < pacientes.length; i++) {
			if (pacientes[i] != null) {
				if (pacientes[i].getResultado().equalsIgnoreCase(resultado)) {
					System.out.println(pacientes[i]);
				}
			}
		}
	}

	public void contarPacientesPositivos() {
		float count = 0;
		float total = 0;
		for (int i = 0; i < pacientes.length; i++) {
			if (pacientes[i] != null) {
				total++;
				if (pacientes[i].getResultado().equalsIgnoreCase("positivo")) {
					count++;
				}
			}
		}
		if (count == 0) {
			System.out.println("  no hay positivos, el porcentaje es del 0%");
		} else {
			System.out.println(
					"  hay " + (int) count + " positivo(s), el porcentaje es del " + (count / total * 100) + "%");
		}
	}

	public void contarPacientesNegativos() {
		float count = 0;
		float total = 0;
		for (int i = 0; i < pacientes.length; i++) {
			if (pacientes[i] != null) {
				total++;
				if (pacientes[i].getResultado().equalsIgnoreCase("negativo")) {
					count++;
				}
			}
		}
		if (count == 0) {
			System.out.println("  no hay negativos, el porcentaje es del 0%");
		} else {
			System.out.println(
					"  hay " + (int) count + " negativo(s), el porcentaje es del " + (count / total * 100) + "%");
		}
	}
}
