package clases;

public class Medico {

	// atributos
	private String nombreMedico;
	private String especialidad;

	// constructor
	public Medico(String nombreMedico, String especialidad) {
		this.nombreMedico = nombreMedico;
		this.especialidad = especialidad;
	}

	// getters&setters
	public String getNombreMedico() {
		return nombreMedico;
	}

	public void setNombreMedico(String nombreMedico) {
		this.nombreMedico = nombreMedico;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	// toString
	@Override
	public String toString() {
		return "Medico [nombreMedico=" + nombreMedico + ", especialidad=" + especialidad + "]";
	}

}
