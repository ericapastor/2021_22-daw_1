package clases;

import java.time.LocalDate;

public class Paciente {

	// atributos
	private String nSS;
	private String nombre;
	private String apellidos;
	private String resultado;
	private LocalDate fechaTest;
	private Medico medico;

	// constructor
	public Paciente(String nSS, String resultado) {
		this.nSS = nSS;
		this.resultado = resultado;
	}

	// getters&setters
	public String getnSS() {
		return nSS;
	}

	public void setnSS(String nSS) {
		this.nSS = nSS;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public LocalDate getFechaTest() {
		return fechaTest;
	}

	public void setFechaTest(LocalDate fechaTest) {
		this.fechaTest = fechaTest;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	// toString
	@Override
	public String toString() {
		return "Paciente [nSS=" + nSS + ", nombre=" + nombre + ", apellidos=" + apellidos + ", resultado=" + resultado
				+ ", fechaTest=" + fechaTest + ", medico=" + medico + "]";
	}

}
