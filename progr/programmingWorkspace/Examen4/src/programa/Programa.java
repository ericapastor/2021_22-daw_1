package programa;

import clases.Laboratorio;
import clases.Medico;

public class Programa {

	public static void main(String[] args) {
		// 1 - crear instancia de laboratorio
		Laboratorio analisisClinico = new Laboratorio(3, 4);
		// 2 - dar de alta 2 medicos y 4 pacientes
		Medico alberto = new Medico("Alberto", "Osteopatia");
		Medico juanita = new Medico("Juana", "Hematologia");
		analisisClinico.altaMedico("Alberto", "Osteopatia");
		analisisClinico.altaMedico("Juana", "Hematologia");
		analisisClinico.altaPaciente("12312312F", "Jim", "Kimkun", "positivo", alberto);
		analisisClinico.altaPaciente("R12341234", "Quinto", "De Agosto", "negativo", juanita);
		analisisClinico.altaPaciente("K67823542", "Rua", "Qwerty", "negativo", juanita);
		analisisClinico.altaPaciente("Q384957H1", "Samuela", "Robinson", "positivo", alberto);
		// 3 - listar pacientes con sus medicos
		System.out.println("3 - Listar pacientes:");
		analisisClinico.listarPacientes();
		// 4 - buscar paciente por su nSS y mostrar sus datos por pantalla
		System.out.println("\n4 - Muestro el paciente por cuyo nSS es \"Q384957H1\":");
		System.out.println(analisisClinico.buscarPaciente("Q384957H1"));
		// 5 - eliminar un paciente diferente al anterior
		System.out.println("\n5 - Elimino el paciente cuyo nSS es \"R12341234\", y muestro todos de nuevo:");
		analisisClinico.eliminarPaciente("R12341234");
		analisisClinico.listarPacientes();
		// 6 - almacenar un nuevo medico y un nuevo paciente(con ese nuevo medico)
		System.out.println("\n6 - Almaceno un nuevo medico y un nuevo paciente y los muestro");
		Medico kim = new Medico("Yolanda", "Podologia");
		analisisClinico.altaMedico("Yolanda", "Podologia");
		analisisClinico.altaPaciente("P4567C123", "Kim", "Possible", "negativo", kim);
		System.out.println(analisisClinico.buscarPaciente("P4567C123"));
		// 7 - modificar el restultado de un paciente
		System.out.println("\n7 - Modifico el resultado de un paciente.");
		System.out.println("Lo muestro antes de modificar:");
		System.out.println(analisisClinico.buscarPaciente("K67823542"));
		// modifico
		analisisClinico.cambiarResultadoPaciente("K67823542", "positivo");
		System.out.println("Lo muestro despues de modificar:");
		System.out.println(analisisClinico.buscarPaciente("K67823542"));
		// 8 - listar solo pacientes con un resultado
		System.out.println("\n8 - Lito los pacientes con resultado positivo:");
		analisisClinico.listarPacientePorResultado("positivo");
		// 9 - mostrar cuantos pacientes positivos tengo
		// y su porcentaje respecto al total
		System.out.println("\n9 - Muestro numero y porcentaje de positivos:");
		analisisClinico.contarPacientesPositivos();
		// 10 - mostrar cuantos pacientes negativos tengo
		// y su porcentaje respecto al total
		System.out.println("\n10 - Muestro numero y porcentaje de negativos:");
		analisisClinico.contarPacientesNegativos();
	}

}
