package ej01CuentasBancarias;

public class CuentaCorriente extends Cuenta{
	
	public CuentaCorriente() {
		super();
	}
	
	public CuentaCorriente(String numero,double saldo, double interes) {
		super(numero,saldo,interes);
	}

	@Override
	public String toString() {
		return "CuentaCorriente [numero=" + numero + ", saldo=" + saldo + ", interes=" + interes + ", titular="
				+ titular + "]";
	}
	
	public double reintegro(int perras) {
		saldo-=perras;
		return saldo;
	}

}
