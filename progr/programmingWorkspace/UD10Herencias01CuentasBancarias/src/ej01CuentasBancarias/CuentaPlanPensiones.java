package ej01CuentasBancarias;

public class CuentaPlanPensiones extends Cuenta{

	double cotizacion;

	public CuentaPlanPensiones() {
		super();
		this.interes=3.22;
		this.cotizacion=6.5;
	}
	
	public CuentaPlanPensiones(String numero,double saldo,double interes,double cotizacion) {
		super(numero,saldo);
		this.interes=interes;
		this.cotizacion=cotizacion;
	}
	
	public double getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(double cotizacion) {
		this.cotizacion = cotizacion;
	}

	@Override
	public String toString() {
		return "CuentaPlanPensiones [numero=" + numero + ", saldo=" + saldo + ", interes=" + interes
				+ ", titular=" + titular + ", cotizacion=" + cotizacion + "]";
	}

}
