package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {
		// Creo y relleno una matriz
		int[][] a = new int[2][6];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = (int) (Math.round(Math.random() * 90) + 10);
			}
		}
		// La visualizo con un metodo
		visualizarMatrix(a);
		// Visualizo el maximo de los numeros pares
		visualizarMaximoPar(a);
		// Visualizo el minimo de los numeros pares
		visualizarMinimoImpar(a);
	}

	static void visualizarMatrix(int[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				System.out.print("| " + a[i][j] + " |");
			}
			System.out.println();
		}
	}

	static void visualizarMaximoPar(int[][] a) {
		int maxPar = a[0][0];
		int b = 0;
		int c = 0;
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				if (a[i][j] % 2 == 0) {
					if (a[i][j] > maxPar) {
						maxPar = a[i][j];
						b = i;
						c = j;
					}
				}
			}
		}
		System.out.println("El maximo de los numeros pares es " + maxPar);
		System.out.println("Su posicion es la (" + b + "," + c + ")");
	}

	static void visualizarMinimoImpar(int[][] a) {
		int minImpar = a[0][0];
		int b = 0;
		int c = 0;
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				if (a[i][j] % 2 != 0) {
					if (a[i][j] < minImpar) {
						minImpar = a[i][j];
						b = i;
						c = j;
					}
				}
			}
		}
		System.out.println("El minimo de los numeros impares es " + minImpar);
		System.out.println("Su posicion es la (" + b + "," + c + ")");

	}
}
