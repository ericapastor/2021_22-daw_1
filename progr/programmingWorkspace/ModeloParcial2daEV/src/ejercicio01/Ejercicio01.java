package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Create an array to fill it with sentences
		System.out.println("Dime la largura del array");
		int a = input.nextInt();
		input.nextLine();
		String[] b = new String[a];
		// Fill it using a method
		rellenarArray(b);
		// Cuento la cantidad de palabras de cada frase
		cantidadPalabras(b);
		// Pido una frase y le cuento las letras
		System.out.println();
		visualizarLetras(b);
		input.close();
	}

	static void rellenarArray(String[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.println("Dime la frase " + (i + 1) + ".");
			a[i] = input.nextLine();
		}
	}

	static void cantidadPalabras(String[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("Frase " + (i + 1) + ":\n" + array[i]);
			System.out.println("La cantidad de palabras de la frase "+(i+1)+" es "+array[i].split(" ").length);
		}
	}

	static void visualizarLetras(String[] array) {
		System.out.println("Indica la componente que quieras visualizar.");
		int a = input.nextInt();
		a -= 1;
		System.out.println("La frase de la componente " + (a + 1) + " es: " + array[a] + ".");
		System.out.println("Sus palabras y letras son:");
		String[] c = array[a].split(" ");
		for (int i = 0; i < c.length; i++) {
			System.out.println(c[i] + ", " + c[i].length() + " letras.");
		}
	}
}
