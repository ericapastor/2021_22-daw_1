package ejercicio03;

public class Ejercicio03 {

	public static void main(String[] args) {
		/*
		 * Realizar un programa que visualice el valor de una variable de tipo cadena en
		 * el mensaje siguiente: �El valor de la variable es: �
		 */
		String chain;
		chain = "Soy una cadena de texto";

		System.out.println("El valor del String es: " + chain);
	}

}
