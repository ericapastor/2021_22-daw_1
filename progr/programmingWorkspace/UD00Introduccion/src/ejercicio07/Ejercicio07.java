package ejercicio07;

public class Ejercicio07 {

	public static void main(String[] args) {

		// Realizar un programa que realice las siguientes operaciones.

		int num1;
		int num2;
		int num3;
		int num4;
		int num5;
		int num6;
		int num7;
		int num8;
		int num9;

		// Declarar numero1 entero y darle un valor

		num1 = 5;

		// Mostrar numero1

		System.out.println("Number 1 is " + num1);

		// Declarar numero2 entero, numero2 ser� numero1 + un numero entero cualquiera

		num2 = num1 + 2;

		// Mostrar numero2

		System.out.println("Number 2 is " + num2);

		// Declarar numero3 entero, numero3 ser� numero1 � un numero entero cualquiera

		num3 = num1 - 5;

		// Mostrar numero3

		System.out.println("Number 3 is " + num3);

		// Declarar numero4 entero y darle un valor

		num4 = 9;

		// Mostrar numero4

		System.out.println("Number 4 is " + num4);

		// Incrementar con el operador += un numero entero cualquiera

		num4 += 8;

		// Mostrar numero4

		System.out.println("Number 4 is now " + num4);

		// Declarar numero5 entero y darle un valor

		num5 = 6;

		// Mostrar numero5

		System.out.println("Number 5 is " + num5);

		// Decrementar con el operador -= un numero entero cualquiera

		num5 -= 7;

		// Mostrar numero5

		System.out.println("Number 5 is now " + num5);

		// Declarar numero6 entero y darle un valor

		num6 = 13;

		// Mostrar numero6

		System.out.println("Number 6 is " + num6);

		// Multiplicar con el operador *= un numero entero cualquiera

		num6 *= 3;

		// Mostrar numero6

		System.out.println("Number 6 is now " + num6);

		// Declarar numero7 entero y darle un valor

		num7 = 8;

		// Mostrar numero7

		System.out.println("Number 7 is " + num7);

		// Dividir con el operador /= un numero entero cualquiera

		num7 /= 2;

		// Mostrar numero7

		System.out.println("Number 7 is now " + num7);

		// Declarar numero8 entero y darle un valor

		num8 = 53;

		// Mostrar numero8

		System.out.println("Number 8 is " + num8);

		// Aumentar su valor con operador ++

		num8++;

		// Mostrar numero8

		System.out.println("Number 8 is now " + num8);

		// Declarar numero9 entero y darle un valor

		num9 = 23;

		// Mostrar numero9

		System.out.println("Number 9 is " + num9);

		// Disminuir su valor con operador --

		num9--;

		// Mostrar numero9

		System.out.println("Number 9 is now " + num9);

	}

}
