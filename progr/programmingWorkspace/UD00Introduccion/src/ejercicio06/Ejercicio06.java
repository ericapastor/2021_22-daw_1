package ejercicio06;

public class Ejercicio06 {

	static final double POUND = 0.91;
	static final double DOLAR = 1.17;

	public static void main(String[] args) {

		/*
		 * Realizar un programa que calcule declare una variable en euros, y la
		 * transforme a libras y visualice el resultado de las dos. Realizar el mismo
		 * proceso para el cambio euros, d�lares. 1 euro = 0.91 libras esterlinas 1 euro
		 * = 1.17 d�lares
		 */

		double euro;
		double pound;
		double dolar;

		euro = 8;
		pound = euro * POUND;
		dolar = euro * DOLAR;

		System.out.println(euro + " euros are:");
		System.out.println("� " + pound + " pounds.");
		System.out.println("� " + dolar + " dolars.");

	}

}
