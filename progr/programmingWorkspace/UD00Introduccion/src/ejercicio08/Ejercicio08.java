package ejercicio08;

public class Ejercicio08 {

	public static void main(String[] args) {

		// Realizar un programa que realice las siguientes operaciones.
		// Declarar num1 y darle un valor entero

		int num1 = 1;

		// Declarar num2 y darle un valor entero

		int num2 = 2;

		// Declarar num3 y darle un valor entero

		int num3 = 3;

		// Declarar un booleano resultado

		boolean condition;

		// Asignarle la comparación num1>num3

		condition = num1 > num3;

		// Mostrar su resultado

		System.out.println("Condition is " + condition);

		// A resultado, asignarle ahora la comparación num3>num2

		condition = num3 > num2;

		// Mostrar su resultado

		System.out.println("Condition is now " + condition);

		// A resultado, asignarle la operación AND de las dos anteriores

		condition = num1 > num3 && num3 > num2;

		// Mostrar su resultado

		System.out.println("Condition is now " + condition);

		// A resultado, asignarle la operación OR de las dos anteriores

		condition = num1 > num3 || num3 > num2;

		// Mostrar su resultado

		System.out.println("Condition is now " + condition);

		// A resultado, asignarle true

		condition = true;

		System.out.println("Condition is now " + condition);

		// Usar la operación NOT para invertir su valor

		condition = !condition;

		System.out.println("Condition is now " + condition);

		// Mostrar su resultado

	}

}
