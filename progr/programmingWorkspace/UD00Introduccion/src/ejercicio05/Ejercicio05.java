package ejercicio05;

public class Ejercicio05 {

	public static void main(String[] args) {

		/*
		 * Realizar un programa que visualice el resto de la divisi�n de dos variables
		 * enteras.
		 */

		int minuend;
		int subtrahend;
		int difference;

		minuend = 5;
		subtrahend = 3;
		difference = minuend - subtrahend;

		System.out.println("The difference between " + minuend + " and " + subtrahend + " is " + difference);

	}

}
