package ejercicio04;

public class Ejercicio04 {

	public static void main(String[] args) {

		/*
		 * Realizar un programa que cree una variable de cada tipo y visualice su
		 * resultado
		 */

		byte byteNumber; // 256 values
		short shortNumber; // 65000 values
		int normalNumber; // 4000 million values
		long longNumber; // above 4000 million values
		float tinyDecimal; // simple decimal number, needs indication 'F'
		double largeDecimal; // multiple decimal number
		char character; // characters with ASCII value
		boolean condition; // conditions that are 'true' o 'false'

		byteNumber = 127;
		shortNumber = -32000;
		normalNumber = 2000000000;
		longNumber = 2000000000000L;
		tinyDecimal = 785.2F;
		largeDecimal = 3465.54673465243;
		condition = false;
		character = 'a';

		System.out.println(byteNumber);
		System.out.println(shortNumber);
		System.out.println(normalNumber);
		System.out.println(longNumber);
		System.out.println(tinyDecimal);
		System.out.println(largeDecimal);
		System.out.println(condition);
		System.out.println(character);

		// i can make the ASCII value of a character behave like a number

		System.out.println("The byte plus the char equals " + (byteNumber + character));

	}

}
