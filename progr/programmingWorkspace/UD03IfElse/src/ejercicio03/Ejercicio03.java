package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {

		/*
		 * Programa que lea un car�cter por teclado e indique si es una letra may�scula
		 * (tipo char)
		 */

		Scanner program = new Scanner(System.in);

		char letter;
		boolean lowerCase;
		boolean upperCase;

		System.out.println("Type a character in the alphabet.");
		letter = program.nextLine().charAt(0);
		upperCase = letter >= 65 && letter <= 90;
		lowerCase = letter >= 97 && letter <= 122;

		if (upperCase) {
			System.out.println("Chacacter is in upper case.");
		} else if (lowerCase) {
			System.out.println("Character is in lower case.");
		} else {
			System.out.println("Character is not a letter.");
		}

		program.close();

	}

}
