package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {

		/*
		 * Programa que pide dos cadenas de texto por teclado. El programa nos indicar�
		 * si son exactamente iguales, si son iguales, aunque haya diferencia entre
		 * may�sculas y min�sculas, o si no son iguales, mostrando un �nico mensaje
		 * (M�todos equals y equalsIgnoreCase de la clase String)
		 */

		Scanner program = new Scanner(System.in);

		String chain1;
		String chain2;

		System.out.println("Type a sentence");
		chain1 = program.nextLine();
		System.out.println("Type another sentence");
		chain2 = program.nextLine();

		if (chain1.equalsIgnoreCase(chain2)) {
			if (chain1.equals(chain2)) {
				System.out.println("Sentences are exactly the same.");
			} else {
				System.out.println("Sentences are the same, but with different upper and lower cases.");
			}

		} else {
			System.out.println("Sentences are not the same.");
		}

		program.close();

	}

}
