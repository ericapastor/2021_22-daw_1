package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {

		/*
		 * Pedir dos fechas e indicar la cantidad de d�as que hay de diferencia entre
		 * ellos. Consideramos que todos los meses tienen 30 d�as. Las fechas se piden
		 * como se quiera.
		 */

		Scanner program = new Scanner(System.in);

		String date1;
		String date2;
		int day1;
		int day2;
		int month1;
		int month2;
		int year1;
		int year2;
		int totaldays1;
		int totaldays2;
		int difference;

		System.out.println("Type a date");
		date1 = program.nextLine();
		day1 = Integer.parseInt(date1.substring(0, date1.indexOf('/')));
		month1 = Integer.parseInt(date1.substring(date1.indexOf('/') + 1, date1.lastIndexOf('/')));
		year1 = Integer.parseInt(date1.substring(date1.lastIndexOf('/') + 1));
		if (day1 >= 1 && day1 <= 30) {
			if (month1 >= 1 && month1 <= 12) {
				if (year1 > 0) {
					System.out.println("\n" + "Date is valid" + "\n");
				} else {
					System.out.println("Year can't be 0 or inferior.");
				}
			} else {
				System.out.println("Month out of range. Must be between 1 and 12.");
			}
		} else {
			System.out.println("Day out of range. Must be between 1 and 30.");
		}

		System.out.println("Type another date");
		date2 = program.nextLine();
		day2 = Integer.parseInt(date2.substring(0, date2.indexOf('/')));
		month2 = Integer.parseInt(date2.substring(date2.indexOf('/') + 1, date2.lastIndexOf('/')));
		year2 = Integer.parseInt(date2.substring(date2.lastIndexOf('/') + 1));
		if (day2 >= 1 && day2 <= 30) {
			if (month2 >= 1 && month2 <= 12) {
				if (year2 > 0) {
					System.out.println("\n" + "Date is valid" + "\n");
				} else {
					System.out.println("Year can't be 0 or inferior.");
				}
			} else {
				System.out.println("Month out of range. Must be between 1 and 12.");
			}
		} else {
			System.out.println("Day out of range. Must be between 1 and 30.");
		}

		// i calculate the number of days for both dates
		totaldays1 = day1 + (month1 * 30) + (year1 * 365);
		totaldays2 = day2 + (month2 * 30) + (year2 * 365);
		difference = totaldays1 - totaldays2;

		if (difference < 0) {
			difference = -difference;
		}

		System.out.println("The difference between these two dates is of " + difference + " days.");

		program.close();

	}

}
