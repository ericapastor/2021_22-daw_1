package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {

		/*
		 * Programa que lee el mes de forma num�rica e indique si es un mes de 30, de 31
		 * o de 28 d�as. Se debe comprobar que el valor introducido est� comprendido
		 * entre 1 y 12 antes de hacer nada. Si no es un mes v�lido, lo debe indicar
		 */

		Scanner program = new Scanner(System.in);

		int month;
		boolean month31;
		boolean month30;
		boolean month28;

		System.out.println("What month are we in? Number, please.");
		month = program.nextInt();
		month31 = month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12;
		month30 = month == 4 || month == 6 || month == 9 || month == 11;
		month28 = month == 2;

		if (month31) {
			System.out.println("Month has 31 days.");
		} else if (month30) {
			System.out.println("Month has 30 days.");
		} else if (month28) {
			System.out.println("Month has 28 or 29 days, depending on the year");
		} else {
			System.out.println("Number out of range.");
		}

		System.out.println();

		program.close();

	}

}
