package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {

		/* Programa que lea un n�mero entero e indique si es par o impar */

		Scanner program = new Scanner(System.in);

		int number;
		boolean pair;

		System.out.println("Type a whole number.");
		number = program.nextInt();
		pair = number % 2 == 0;

		if (pair) {
			System.out.println("Number is pair.");
		} else {
			System.out.println("Number is impair.");
		}

		program.close();

	}

}
