package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {

		/*
		 * Construir un programa utilizando las sentencias if-else anidadas, que pide el
		 * peso y la altura, calcule el �ndice de masa corporal de una persona (IMC =
		 * peso [kg] / altura2 [m]) e indique el estado en el que se encuentra esa
		 * persona en funci�n del valor de IMC: (*Las magnitudes no son valores enteros)
		 */
		// Valor de IMC Diagn�stico
		// < 16 Criterio de ingreso en hospital
		// de 16 a 17 infrapeso
		// de 17 a 18 bajo peso
		// de 18 a 25 peso normal (saludable)
		// de 25 a 30 sobrepeso (obesidad de grado I)
		// de 30 a 35 sobrepeso cr�nico (obesidad de grado II)
		// de 35 a 40 obesidad prem�rbida (obesidad de grado III)
		// >40 obesidad m�rbida (obesidad de grado IV)

		Scanner program = new Scanner(System.in);

		double weight;
		double height;
		double BMI; // Body Mass Index
		boolean medWeight;
		boolean infraWeight;
		boolean lowWeight;
		boolean normalWeight;
		boolean overWeightI;
		boolean overWeightII;
		boolean overWeightIII;
		boolean overWeightIV;

		System.out.println("What is your weight?");
		weight = program.nextDouble();
		if (weight > 0) {
			System.out.println("What is your height?");
			height = program.nextDouble();
			if (height > 0) {
				BMI = weight / (height * height);
				medWeight = BMI <= 16 && BMI > 0;
				infraWeight = BMI > 16 && BMI <= 17;
				lowWeight = BMI > 17 && BMI <= 18;
				normalWeight = BMI > 18 && BMI <= 25;
				overWeightI = BMI > 25 && BMI <= 30;
				overWeightII = BMI > 30 && BMI <= 35;
				overWeightIII = BMI > 35 && BMI <= 40;
				overWeightIV = BMI > 40;

				System.out.println("\n" + "You Body Mass Index is: " + BMI + ", and your diagnosis is: " + "\n");
				if (medWeight) {
					System.out.println("Hospital admission.");
				} else if (infraWeight) {
					System.out.println("1.- Infraweight.");
				} else if (lowWeight) {
					System.out.println("2.- Low weight.");
				} else if (normalWeight) {
					System.out.println("3.- Normal weight.");
				} else if (overWeightI) {
					System.out.println("4.- Overweight grade I.");
				} else if (overWeightII) {
					System.out.println("4.- Overweight grade II.");
				} else if (overWeightIII) {
					System.out.println("4.- Overweight grade III.");
				} else if (overWeightIV) {
					System.out.println("4.- Overweight grade IV.");
				} else {
					System.out.println("Out of range.");
				}
			} else {
				System.out.println("Height can't be 0 or negative.");
			}
		} else {
			System.out.println("Weight can't be 0 or negative.");
		}

		program.close();

	}

}
