package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {

		/*
		 * Programa que lea un solo car�cter e indique si es una cifra o no lo es
		 * (Car�cter entre '0' y '9') (tipo char).
		 */

		Scanner program = new Scanner(System.in);

		char number;
		boolean digit;

		System.out.println("Type a digit.");
		number = program.nextLine().charAt(0);
		digit = number >= '0' && number <= '9';

		if (digit) {
			System.out.println("Conditions are met.");
		} else {
			System.out.println("Not a digit.");
		}

		program.close();

	}

}
