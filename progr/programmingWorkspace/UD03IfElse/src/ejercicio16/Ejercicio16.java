package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {

		/*
		 * Programa variaci�n del anterior que pida una hora al usuario en formato
		 * horas:minutos:segundos y posteriormente pida una fecha en formato dia/mes/a�o
		 * . El programa comprueba si la hora y fechas son v�lidas y muestra un mensaje
		 * al usuario indic�ndolo al usuario. Para separar los meses, d�as, a�os, horas,
		 * minutos y segundos debemos utilizar los m�todos de la clase String que
		 * creamos oportunos. Debemos atender a que los meses tienen los d�as indicados
		 * en la siguiente tabla: 31 dias Enero, Marzo, Mayo, Julio, Agosto, Octubre,
		 * Diciembre 30 dias Abril, Junio, Septiembre, Noviembre 28 dias Febrero (caso
		 * normal) 29 dias Febrero (Cada 4 a�os, o sea, todos los a�os m�ltiplos de 4)
		 */

		Scanner program = new Scanner(System.in);

		String time;
		String date;
		int hour;
		int mins;
		int secs;
		int day;
		int month;
		int year;

		System.out.println("Type the time in the following format-> hour:minutes:seconds.");
		time = program.nextLine();
		hour = Integer.parseInt(time.substring(0, time.indexOf(':')));
		mins = Integer.parseInt(time.substring(time.indexOf(':') + 1, time.lastIndexOf(':')));
		secs = Integer.parseInt(time.substring(time.lastIndexOf(':') + 1));
		if (hour >= 0 && hour <= 23) {
			if (mins >= 0 && mins <= 59) {
				if (secs >= 0 && secs <= 59) {
					System.out.println("Type the date in the following format-> day/month/year.");
					date = program.nextLine();
					day = Integer.parseInt(date.substring(0, date.indexOf('/')));
					month = Integer.parseInt(date.substring(date.indexOf('/') + 1, date.lastIndexOf('/')));
					year = Integer.parseInt(date.substring(date.lastIndexOf('/') + 1));
					if (day >= 1 && day <= 31) {
						if (month >= 1 && month <= 12) {
							if (year > 0) {
								System.out.println("Date and time are valid.");
								if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10
										|| month == 12) {
									System.out.println("Month has 31 days.");
								} else if (month == 4 || month == 6 || month == 9 || month == 11) {
									System.out.println("month has 30 days.");
								} else if (month == 2) {
									if (year % 4 == 0) {
										System.out.println("Februar has 29 days because it's a leap year.");
									} else {
										System.out.println("Februar has 28 days because it's not a leap year.");
									}
								}
								System.out.println("Time is " + hour + ":" + mins + ":" + secs + ".");
								System.out.println("Date is " + day + "/" + month + "/" + year + ".");
							} else {
								System.out.println("Year out of range. Must be bigger than 0.");
							}
						} else {
							System.out.println("Month out of range. Must be between 1 and 12.");
						}
					} else {
						System.out.println("Day out of range. Must be between 1 and 30.");
					}
				} else {
					System.out.println("Seconds out of range. Must be between 0 and 59.");
				}
			} else {
				System.out.println("Minutes out of range. Must be between 0 and 59.");
			}
		} else {
			System.out.println("Hour out of range. Must be between 0 and 23.");
		}

		program.close();

	}

}
