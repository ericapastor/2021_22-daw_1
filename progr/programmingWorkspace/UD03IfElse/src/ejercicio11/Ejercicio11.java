package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {

		/*
		 * Programa que lea tres n�meros enteros que contienen hora, minutos y segundos
		 * respectivamente, y comprueba si la hora que indican es una hora v�lida
		 * (formato 24h: Minutos y segundos de 0 a 59 y horas de 0 a 23)
		 */

		Scanner program = new Scanner(System.in);

		int hour;
		int mins;
		int secs;

		System.out.println("Type the hour.");
		hour = program.nextInt();
		if (hour >= 0 && hour <= 23) {
			System.out.println("Type the minutes.");
			mins = program.nextInt();
			if (mins >= 0 && mins <= 59) {
				System.out.println("Type the seconds.");
				secs = program.nextInt();
				if (secs >= 0 && secs <= 59) {
					System.out.println("The time is: " + hour + ":" + mins + ":" + secs);
				} else {
					System.out.println("Seconds are not valid. Must be between 0 and 59.");
				}
			} else {
				System.out.println("Minutes are not valid. Must be between 0 and 59.");
			}
		} else {
			System.out.println("Hours are not valid. Must be between 0 and 23.");
		}

		program.close();

	}

}
