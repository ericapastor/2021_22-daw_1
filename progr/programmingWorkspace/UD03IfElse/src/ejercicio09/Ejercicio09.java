package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {

		/*
		 * Pedir dos n�meros y decir cu�l es el mayor o si son iguales. Se debe mostrar
		 * un �nico mensaje.
		 */

		Scanner program = new Scanner(System.in);

		int num1;
		int num2;
		boolean equals;
		boolean bigger;

		System.out.println("Type a number");
		num1 = program.nextInt();

		System.out.println("Type another number");
		num2 = program.nextInt();

		equals = num1 == num2;
		bigger = num1 > num2;

		if (equals) {
			System.out.println("Numbers are equals.");
		} else if (bigger) {
			System.out.println(num1 + " is bigger.");
		} else {
			System.out.println(num2 + " is bigger.");
		}

		program.close();

	}

}
