package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {

		/*
		 * Programa que lea dos n�meros por teclado y muestre el resultado de la
		 * divisi�n del primero por el segundo. Antes de realizar la operaci�n, se debe
		 * comprobar que el divisor no es cero, y si es as�, indicarlo. Si no es cero
		 * realizo la operaci�n y muestro el resultado
		 */

		Scanner program = new Scanner(System.in);

		double dividend;
		double divisor;
		double quotient;

		System.out.println("Type the dividend.");
		dividend = program.nextDouble();

		System.out.println("Type the divisor.");
		divisor = program.nextDouble();

		if (divisor != 0) {
			quotient = dividend / divisor;
			System.out.println("Quotient is: " + quotient);
		} else {
			System.out.println("Divisor can't be 0.");
		}

		program.close();

	}

}
