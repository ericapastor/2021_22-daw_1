package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {

		/*
		 * Programa que lea dos caracteres e indique si las dos son letras minúsculas
		 * (tipo char).
		 */

		Scanner program = new Scanner(System.in);

		char letter1;
		char letter2;
		boolean upperCase1;
		boolean lowerCase1;
		boolean upperCase2;
		boolean lowerCase2;

		System.out.println("Type a character in the alphabet.");
		letter1 = program.nextLine().charAt(0);
		upperCase1 = letter1 >= 'A' && letter1 <= 'Z';
		lowerCase1 = letter1 >= 'a' && letter1 <= 'z';

		System.out.println("Type another character in the alphabet.");
		letter2 = program.nextLine().charAt(0);
		upperCase2 = letter2 >= 'A' && letter2 <= 'Z';
		lowerCase2 = letter2 >= 'a' && letter2 <= 'z';

		if (upperCase1 && upperCase2) {
			System.out.println("Characters are both in upper case.");
		} else if (lowerCase1 && lowerCase2) {
			System.out.println("Characters are both in lower case.");
		} else if (lowerCase1 || lowerCase2 || upperCase1 || upperCase2) {
			System.out.println("Characters are in different cases.");
		} else {
			System.out.println("Both or one of the characters is not in the alphabet.");
		}

		program.close();

	}

}
