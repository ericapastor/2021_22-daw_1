package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {

		/*
		 * Programa que lea dos caracteres por teclado e indique si son iguales (tipo
		 * char).
		 */

		Scanner program = new Scanner(System.in);

		char charac1;
		char charac2;
		boolean equals;

		System.out.println("Type a character.");
		charac1 = program.nextLine().charAt(0);

		System.out.println("Type another character.");
		charac2 = program.nextLine().charAt(0);

		equals = charac1 == charac2;

		if (equals) {
			System.out.println("Characters are the same.");
		} else {
			System.out.println("Conditions are not met.");
		}

		program.close();

	}

}
