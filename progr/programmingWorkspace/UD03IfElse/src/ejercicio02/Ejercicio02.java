package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {

		/*
		 * Programa que lea un n�mero entero e indique si el n�mero es m�ltiplo de 10.
		 */

		Scanner program = new Scanner(System.in);

		int number;
		boolean x10;

		System.out.println("Type a number.");
		number = program.nextInt();
		x10 = number % 10 == 0;

		if (x10) {
			System.out.println("Number is multiple of 10, well done!");
		} else {
			System.out.println("Conditions are not met.");
		}

		program.close();

	}

}
