package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {

		/*
		 * Lee un n�mero entero por teclado y muestra por consola el car�cter al que
		 * corresponde en la tabla ASCII.
		 */

		Scanner program = new Scanner(System.in);

		int number;
		char charac;

		System.out.println("Type a number." + "\n");
		number = program.nextInt();
		charac = (char) number;

		System.out.println("\n" + "The character is: " + charac);

		program.close();

	}

}
