package ejercicio10;

import java.util.Scanner;

public class Ejercicio10_02 {

	public static void main(String[] args) {

		/*
		 * Programa que pide 3 n�meros enteros por teclado, y me muestra solo el mayor
		 * de todos
		 */

		Scanner program = new Scanner(System.in);

		int num1;
		int num2;
		int num3;
		int bigger = 0;

		System.out.println("Type the first number.");
		num1 = program.nextInt();

		System.out.println("Type the second number.");
		num2 = program.nextInt();

		System.out.println("Type the third number.");
		num3 = program.nextInt();

		if (num1 == num2 && num2 == num3) {
			System.out.println("Numbers are equals.");
		} else {
			if (bigger < num1) {
				bigger = num1;
			}
			if (bigger < num2) {
				bigger = num2;
			}
			if (bigger < num3) {
				bigger = num3;
			}
			System.out.println("The bigger number is " + bigger + ".");
		}

		program.close();

	}

}
