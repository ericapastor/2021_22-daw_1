package ejercicio10;

import java.util.Scanner;

public class Ejercicio10_01 {

	public static void main(String[] args) {

		/*
		 * Programa que pide 3 n�meros enteros por teclado, y me muestra solo el mayor
		 * de todos
		 */

		Scanner program = new Scanner(System.in);

		int num1;
		int num2;
		int num3;

		System.out.println("Type the first number.");
		num1 = program.nextInt();

		System.out.println("Type the second number.");
		num2 = program.nextInt();

		System.out.println("Type the third number.");
		num3 = program.nextInt();

		if (num1 > num2 && num1 > 3) {
			System.out.println("The bigger number is " + num1 + ".");
		} else if (num2 > num1 && num2 > num3) {
			System.out.println("The bigger number is " + num2 + ".");
		} else if (num3 > num1 && num3 > num1) {
			System.out.println("The bigger number is " + num3 + ".");
		} else {
			System.out.println("Numbers are equals.");
		}

		program.close();

	}

}
