package ej01Clases;

public class Vehiculo {

	private static int vehiculosCreados;

	// atributos

	private String tipo;
	private String marca;
	private float consumo;
	private float kmTotales;
	private int numRuedas;
	private boolean funciona;

	// constructor sin parametros

	public Vehiculo() {
		kmTotales = 0;
		funciona = true;
		vehiculosCreados++;
	}

	// constructor con 2 parametros

	public Vehiculo(String marca, String tipo) {
		this.marca = marca;
		this.tipo = tipo;
		kmTotales = 0;
		funciona = true;
		vehiculosCreados++;
	}

	// constructor con 4 parametros

	public Vehiculo(String marca, String tipo, float consumo, int numRuedas) {
		this.marca = marca;
		this.tipo = tipo;
		this.consumo = consumo;
		this.numRuedas = numRuedas;
		kmTotales = 0;
		funciona = true;
		vehiculosCreados++;
	}

	// setter y getter

	public int getVehiculosCreados() {
		return vehiculosCreados;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public float getConsumo() {
		return consumo;
	}

	public void setConsumo(float consumo) {
		this.consumo = consumo;
	}

	public float getKmTotales() {
		return kmTotales;
	}

	public int getNumRuedas() {
		return numRuedas;
	}

	public void setNumRuedas(int numRuedas) {
		this.numRuedas = numRuedas;
	}

	public boolean getFunciona() {
		return funciona;
	}

	public void setFunciona(boolean funciona) {
		this.funciona = funciona;
	}

	// METODOS SOBRECARGADOS

	// metodo float combustibleConsumido()

	public float combustibleConsumido() {
		return (this.consumo * this.kmTotales) / 100;
	}

	// metodo float combustibleConsumido(float kmTotales)

	public float combustibleConsumido(float kmTotales) {
		return (this.consumo * kmTotales) / 100;
	}

	// metodo trucarCuentaKM()

	public float trucarCuentaKM() {
		return this.kmTotales = 0.0F;
	}

	// metodo trucarCuentaKM(float km)

	public float trucarCuentaKM(float km) {
		return this.kmTotales = km;
	}

}
