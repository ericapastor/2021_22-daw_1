package ej06;

public class Principal {

	public static void main(String[] args) {
		// persona con dos datos
		Persona persona1 = new Persona("Horacio", "Singed");
		System.out.println("Persona 1:");
		mostrarPersona(persona1);
		System.out.println();
		// persona con cuatro datos
		System.out.println("Persona 2:");
		Persona persona2 = new Persona("Oliver", "Benji", "1995-12-07", "she her");
		mostrarPersona(persona2);
		System.out.println();
		// persona con seis datos
		System.out.println("Persona 3:");
		Persona persona3 = new Persona("Kim", "Possible", "2007-05-31", "neutro", 1.65F, 58.7F);
		mostrarPersona(persona3);
		System.out.println();

	}

	public static void mostrarPersona(Persona persona) {
		System.out.println("Nombre: " + persona.getNombre());
		System.out.println("Apellidos: " + persona.getApellidos());
		if (persona.getSexo() != null) {
			System.out.println("Fecha de nacimiento: " + persona.getFechaNacimiento());
			System.out.println("Sexo: " + persona.getSexo());
			if (persona.getAltura() != 0) {
				System.out.println("Altura: " + persona.getAltura());
				System.out.println("Peso: " + persona.getPeso());
			}
		}
	}

}
