package ej06;

import java.time.LocalDate;

public class Persona {

	// atributes
	private String nombre;
	private String apellidos;
	private LocalDate fechaNacimiento;
	private String sexo;
	float altura;
	float peso;

	// constructor with 2 fields
	public Persona(String nombre, String apellidos) {
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	/**
	 * 
	 * @param nombre
	 * @param apellidos
	 * @param fechaNacimiento
	 * @param sexo
	 */
	// constructor with 4 fields
	public Persona(String nombre, String apellidos, String fechaNacimiento, String sexo) {
		this(nombre, apellidos);
		// recibo un parametro String
		// pero lo guardo en un LocalDate
		// todas las aplicaciones graficas reciben String
		// por tanto no va a venir como el parametro
		// en el que tu lo quieres guardar
		// tengo que parsearlo
		this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
		// normalmente incluso los numeros se reciben
		// inicialmente como Strings
		this.sexo = sexo;
	}

	// constructor with all 6 fields
	public Persona(String nombre, String apellidos, String fechaNacimiento, String sexo, float altura, float peso) {
		this(nombre, apellidos, fechaNacimiento, sexo);
		this.altura = altura;
		this.peso = peso;
	}

	// getters and setters

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

}
