package ej04;

import java.util.Scanner;

import ej01Clases.Vehiculo;
import ej02Main.Ej02;

public class Ej04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// creo un vector de vehiculos
		Vehiculo[] listaVehiculos = new Vehiculo[3];
		for (int i = 0; i < listaVehiculos.length; i++) {
			System.out.println("Dame la marca");
			String marca = input.nextLine();
			System.out.println("Dame el tipo");
			String tipo = input.nextLine();
			System.out.println("Dame el consumo");
			float consumo = input.nextFloat();
			System.out.println("Dame el numero de ruedas");
			int ruedas = input.nextInt();
			input.nextLine();

			listaVehiculos[i] = new Vehiculo(tipo, marca, consumo, ruedas);
		}
		input.close();

		for (int i = 0; i < listaVehiculos.length; i++) {
			Ej02.mostrarDatosVehiculo(listaVehiculos[i]);
			System.out.println("Vehiculos creados: " + listaVehiculos[i].getVehiculosCreados());
		}
	}

}
