package ej03;

import ej01Clases.Vehiculo;

public class Ej03 {

	public static void main(String[] args) {
		// crear vehiculo con constructor sin parametros
		Vehiculo coche1 = new Vehiculo();
		// mostrar vehiculos creados(vehiculo1)
		System.out.println(coche1.getVehiculosCreados());

		// crear vehiculo con constructor con 2 parametros
		Vehiculo coche2 = new Vehiculo("Opel", "Corsa");
		// mostrar Vehiculos creados (vehiculo1, vehiculo2)
		System.out.println(coche1.getVehiculosCreados());
		System.out.println(coche2.getVehiculosCreados());

		// crear vehiculo con constructor con 4 parametros
		Vehiculo coche3 = new Vehiculo("Skoda", "Octavia", 34000.8F, 4);
		// mostrar Vehiculos creados (vehiculo1, vehiculo2, vehiculo3)
		System.out.println(coche1.getVehiculosCreados());
		System.out.println(coche2.getVehiculosCreados());
		System.out.println(coche3.getVehiculosCreados());

	}

}
