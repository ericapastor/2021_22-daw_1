package ej02Main;

import ej01Clases.Vehiculo;
import java.util.Scanner;

public class Ej02 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// pedir datos
		System.out.println("Datos del segundo vehiculo:");
		System.out.println("Dame la marca");
		String marca = input.nextLine();
		System.out.println("Dame el tipo");
		String tipo = input.nextLine();
		System.out.println("Dame el consumo");
		float consumo = input.nextFloat();
		System.out.println("Dame el numero de ruedas");
		int ruedas = input.nextInt();
		input.nextLine();
		// creo vehiculo con 4 parametros
		Vehiculo vehiculo1 = new Vehiculo("Skoda", "Octavia", 340000.30F, 4);
		// pedir datos
		System.out.println("Datos del primer vehiculo:");
		System.out.println("Dame la marca");
		marca = input.nextLine();
		System.out.println("Dame el tipo");
		tipo = input.nextLine();
		// creo vehiculo con dos parametros
		Vehiculo vehiculo2 = new Vehiculo(marca, tipo);
		// creo vehiculo sin parametros
		// lo relleno usando setters
		Vehiculo vehiculo3 = new Vehiculo();
		System.out.println("Dame la marca");
		marca = input.nextLine();
		System.out.println("Dame el tipo");
		tipo = input.nextLine();
		System.out.println("Dame el consumo");
		consumo = input.nextFloat();
		System.out.println("Dame el numero de ruedas");
		ruedas = input.nextInt();
		vehiculo3.setConsumo(consumo);
		vehiculo3.setMarca(marca);
		vehiculo3.setNumRuedas(ruedas);
		vehiculo3.setTipo(tipo);

		mostrarDatosVehiculo(vehiculo1);
		mostrarDatosVehiculo(vehiculo2);
		mostrarDatosVehiculo(vehiculo3);

		System.out.println("Cambio los km del vehiculo.");
		vehiculo1.trucarCuentaKM(57000);
		System.out.println("Estimo el combustible consumido en el vehiculo1.");
		float estimacion = vehiculo1.combustibleConsumido();
		System.out.println("Estimacion: " + estimacion);
		System.out.println("Reseteo el cuenta km del vehiculo1.");
		vehiculo1.trucarCuentaKM();
		System.out.println("Estimo el combustible consumido en el vehiculo1.");
		estimacion = vehiculo1.combustibleConsumido(40);
		System.out.println("Estimacion: " + estimacion);
		System.out.println("\n**************\nVEHICULOS CREADOS:" + vehiculo1.getVehiculosCreados());

		input.close();
	}

	public static void mostrarDatosVehiculo(Vehiculo vehiculo) {
		System.out.println("Tipo: " + vehiculo.getTipo());
		System.out.println("Marca: " + vehiculo.getMarca());
		System.out.println("Consumo: " + vehiculo.getConsumo());
		System.out.println("Numero de ruedas: " + vehiculo.getNumRuedas());
		System.out.println("Funciona o no: " + vehiculo.getFunciona());
		System.out.println("Km: " + vehiculo.getKmTotales());
	}

}
