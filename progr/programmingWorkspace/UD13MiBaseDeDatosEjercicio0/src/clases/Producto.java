package clases;

public class Producto {

	int id;
	String nombre;
	float peso;
	float precioB;
	float iva = 21F;
	String codigo;
	String tipo;

	public Producto() {
	}

	public Producto(int id, String nombre, float peso, float precio_bruto, float iva, String codigo, String tipo) {
		this.id = id;
		this.nombre = nombre;
		this.peso = peso;
		this.precioB = precio_bruto;
		this.iva = iva;
		this.codigo = codigo;
		this.tipo = tipo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	public float getPrecioB() {
		return precioB;
	}

	public void setPrecioB(float precio_bruto) {
		this.precioB = precio_bruto;
	}

	public float getIva() {
		return iva;
	}

	public void setIva(float iva) {
		this.iva = iva;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Producto [nombre=" + nombre + ", peso=" + peso + ", precio_bruto=" + precioB + ", iva=" + iva
				+ ", codigo=" + codigo + ", tipo=" + tipo + "]";
	}

}
