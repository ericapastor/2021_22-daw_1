package clases;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) throws NullPointerException, SQLException {
		Scanner in = new Scanner(System.in);
		Datos bbdd = new Datos();
		// conectar
		System.out.println("Conectando...");
		bbdd.conectar();
		// mostrar
		System.out.println("Estos son los datos actuales:");
		bbdd.mostrarTodo();
		// insertar
		while (true) {
			System.out.println("Insertamos productos hasta que se confirme lo contrario");
			System.out.print("Dime el nombre del producto: ");
			String nombre = in.nextLine();
			System.out.print("Escribe el codigo del producto: ");
			String codigo = in.nextLine();
			System.out.print("Que tipo de producto es: ");
			String tipo = in.nextLine();
			System.out.print("Fecha de caducidad: ");
			String caducidad = in.nextLine();
			float peso;
			float precio;
			while (true) {
				try {
					System.out.print("Cuanto pesa el producto: ");
					peso = in.nextFloat();
					System.out.print("Escribe el precio en bruto: ");
					precio = in.nextFloat();
					in.nextLine();
					break;
				} catch (InputMismatchException e) {
					System.out.println("Has insertado un dato no numerico");
					System.out.println("Volvamos a empezar");
					in.nextLine();
				}
			}
			bbdd.insertar(nombre, peso, precio, caducidad, codigo, tipo);
			System.out.print("Quieres repetir la operacion [si/no] = ");
			String opcion = in.nextLine();
			if (opcion.equalsIgnoreCase("no")) {
				break;
			} else {
				System.out.println("Siguiente producto:");
			}
		}
		System.out.println("Actualizar datos: ");
		while (true) {
			System.out.println("Actualizamos productos hasta que se confirme lo contrario");
			System.out.print("Dime el nombre del producto: ");
			String nombre = in.nextLine();
			System.out.print("Escribe el codigo del producto: ");
			String codigo = in.nextLine();
			System.out.print("Que tipo de producto es: ");
			String tipo = in.nextLine();
			System.out.print("Fecha de caducidad: ");
			String caducidad = in.nextLine();
			float peso;
			float precio;
			while (true) {
				try {
					System.out.print("Cuanto pesa el producto: ");
					peso = in.nextFloat();
					System.out.print("Escribe el precio en bruto: ");
					precio = in.nextFloat();
					in.nextLine();
					break;
				} catch (InputMismatchException e) {
					System.out.println("Has insertado un dato no numerico");
					System.out.println("Volvamos a empezar");
					in.nextLine();
				}
			}
			bbdd.actualizar(nombre, peso, precio, caducidad, codigo, tipo);
			System.out.print("Quieres repetir la operacion [si/no] = ");
			String opcion = in.nextLine();
			if (opcion.equalsIgnoreCase("no")) {
				break;
			} else {
				System.out.println("Siguiente producto:");
			}
		}
		System.out.println("Borrar datos:");
		System.out.println("Dime el id del producto");
		while (true) {
			try {
				int id = in.nextInt();
				in.nextLine();
				bbdd.eliminar(id);
				System.out.println("Estos son los datos que quedan:");
				bbdd.mostrarTodo();
				break;
			} catch (InputMismatchException e) {
				System.out.println("Has insertado un dato no numerico");
				in.nextLine();
			}
		}
		System.out.println("Desconectando...");
		bbdd.desconectar();
		in.close();
	}
}
