package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Datos {

	private Connection con = null;
	PreparedStatement sen = null;

	public void conectar() {
		try {
			String server = "jdbc:mysql://localhost:3306/";
			String bbdd = "starset";
			String user = "root";
			String pwd = "";
			con = DriverManager.getConnection(server + bbdd, user, pwd);
		} catch (NullPointerException e) {
			System.out.println("La base no existe o no se ha podido conectar con ella.");
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		}
	}

	public void mostrarTodo() {
		String senSQL = "SELECT * FROM starset.producto";
		try {
			// la sentencia se prepara con la conexion anterior
			sen = con.prepareStatement(senSQL);
			// ejecutar RESULTADO con la sentencia
			// ejecutar NO es mostrar
			ResultSet res = sen.executeQuery();
			// mostrar con while resultado .next, ejecutamos todo en String
			// el 0 es el ID auto_increment, por eso se suele empezar en el 1
			while (res.next()) {
				System.out.println(res.getString(1) + ", " + res.getString(2));
				System.out.println("Peso: " + res.getString(3));
				System.out.println("Precio: " + res.getString(4));
				System.out.println("Caducidad: " + res.getString(5));
				System.out.println("Codigo: " + res.getString(6));
				System.out.println("Tipo: " + res.getString(7));
			}
		} catch (NullPointerException e) {
			System.out.println("La base no existe o no se ha podido conectar con ella.");
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		} finally {
			try {
				if (sen != null) {
					sen.close();
				}
			} catch (SQLException e) {
				System.out.println("No se ha podido inicializar la sentencia.");
			}
		}
	}

	public void insertar(String nombre, float peso, float precioB, String caducidad, String codigo, String tipo) {
		try {
			String senSQL = "insert into producto (nombre, peso, precio_bruto, iva, codigo, tipo)"
					+ "values (?, ?, ?, ?, ?, ?)";
			sen = con.prepareStatement(senSQL);
			sen.setString(2, nombre);
			sen.setFloat(3, peso);
			sen.setFloat(4, precioB);
			sen.setString(5, caducidad);
			sen.setString(6, codigo);
			sen.setString(7, tipo);
			sen.executeUpdate();
		} catch (NullPointerException e) {
			System.out.println("La base no existe o no se ha podido conectar con ella.");
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		} finally {
			try {
				if (sen != null) {
					sen.close();
				}
			} catch (SQLException e) {
				System.out.println("No se ha podido inicializar la sentencia.");
			}
		}
	}

	public void actualizar(String nombre, float peso, float precioB, String caducidad, String codigo, String tipo) {
		try {
			String senSQL = "update producto set nombre=?, peso=?, precio_bruto=?, iva=?, codigo=?, tipo=?";
			sen = con.prepareStatement(senSQL);
			sen.setString(1, nombre);
			sen.setFloat(2, peso);
			sen.setFloat(3, precioB);
			sen.setString(4, caducidad);
			sen.setString(5, codigo);
			sen.setString(6, tipo);
			sen.executeUpdate();
		} catch (NullPointerException e) {
			System.out.println("La base no existe o no se ha podido conectar con ella.");
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		} finally {
			try {
				if (sen != null) {
					sen.close();
				}
			} catch (SQLException e) {
				System.out.println("No se ha podido inicializar la sentencia.");
			}
		}
	}

	public void eliminar(int id) {
		try {
			String senSQL = "delete from producto where id=?";
			sen = con.prepareStatement(senSQL);
			sen.setInt(0, id);
			sen.executeUpdate();
		} catch (NullPointerException e) {
			System.out.println("La base no existe o no se ha podido conectar con ella.");
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		} finally {
			try {
				if (sen != null) {
					sen.close();
				}
			} catch (SQLException e) {
				System.out.println("No se ha podido inicializar la sentencia.");
			}
		}
	}

	public void desconectar() {
		try {
			sen.close();
		} catch (SQLException e) {
			System.out.println("No se ha podido inicializar la sentencia.");
		}
	}

}
