package ej05;

import java.util.Scanner;

public class Ej05SinExcepciones {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un numero");
		double x = input.nextDouble();
		System.out.println("Raiz cuadrada de " + x + " = " + Math.sqrt(x));
		input.close();

	}

}
