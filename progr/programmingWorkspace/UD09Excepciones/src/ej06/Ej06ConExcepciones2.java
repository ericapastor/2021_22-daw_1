package ej06;

public class Ej06ConExcepciones2 {
	
	public static int numerador = 10;
	public static Integer denominador = null;
	public static float division;

	public static void main(String[] args) {
		metodo1();
	}

	// capturo la excepcion
	public static void metodo1() {
		try {
			division = numerador / denominador;
			System.out.println(division);
		} catch (NullPointerException e) {
			division = 1;
			System.out.println("Traza de errores");
			e.printStackTrace();
		}
	}

}
