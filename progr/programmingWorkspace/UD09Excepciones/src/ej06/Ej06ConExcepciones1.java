package ej06;

public class Ej06ConExcepciones1 {

	public static int numerador = 10;
	public static Integer denominador = null;
	public static float division;

	// capturo la excepcion
	public static void main(String[] args) {
		try {
			metodo1();
		} catch (NullPointerException e) {
			division = 1;
			System.out.println("Mensaje de error "+e.getMessage());
		}
	}

	// throws lanzo la excepcion
	public static void metodo1() throws NullPointerException {
		division = numerador / denominador;
		System.out.println(division);
	}
	
}
