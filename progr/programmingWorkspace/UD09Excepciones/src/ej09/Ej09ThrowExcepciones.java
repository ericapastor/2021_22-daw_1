package ej09;

public class Ej09ThrowExcepciones {
	
	public static void main(String[] args) throws Exception {
		comprobarDiaMes(35);
	}

	public static void comprobarDiaMes(int dia) throws Exception {
		if (dia > 31 || dia < 1) {
			Exception excepcion = new Exception("Numero de dia fuera de rango");
			throw excepcion;
		}
	}

}
