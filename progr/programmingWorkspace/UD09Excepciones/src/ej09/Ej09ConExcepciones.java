package ej09;

public class Ej09ConExcepciones {
	
	//excepcion creada por el programador
		//permite lanzar tu propia excepcion y personalizar el mensaje
		
		public static void main(String[] args) {
			try {
				comprobarDiaMes(35);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		public static void comprobarDiaMes(int dia) throws Exception {
			if (dia > 31 || dia < 1) {
				Exception excepcion = new Exception("Numero de dia fuera de rango");
				throw excepcion;
			}
		}

}
