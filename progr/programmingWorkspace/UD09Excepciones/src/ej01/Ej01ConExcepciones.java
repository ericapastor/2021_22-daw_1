package ej01;

public class Ej01ConExcepciones {

	public static void main(String[] args) {
		int i;
		int valor;
		// cuando ocurre una excepcion dentro un metodo
		// Java crea ub objeto y se lo entrega al entorno de ejecucion
		// checked obligatorias
		// unchecked no obligatorias
		// forma de controlarlas
		// try catch o progandolas (aplica a metodos)

		try {
			i = 3;
			valor = i / 0;
			System.out.println(valor);
		} catch (ArithmeticException e) {
			System.out.println(e.toString());
		} finally {
			System.out.println("Esto se imprime siempre");
		}

		// bloque try
		// contiene el codigo regular del programa
		// el codigo que provoca la excepcion
		// catch
		// contiene el codigo con el que trataremos el error
		// finally
		// contiene el codigo que se ejecutara al final
		// tanto si se ha producido el error, como si no
	}

}
