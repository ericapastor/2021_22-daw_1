package ej07;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ej07ThrowExcepciones {

	public static void main(String[] args) throws IOException {
		lectura();
	}

	// leer datos usando BufferedReader
	// esta clase permite leer datos de teclado, de fichero, ...
	// BufferedReader es una clase para leer, pero requiere un constructor
	// que reciba un objeto indicando por donde voy a leer
	// InputStreamReader
	// FileInputStreamReader ... Writer .....
	public static void lectura() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Dame una frase");
		String frase = br.readLine();
		System.out.println("La frase es " + frase);
	}

}
