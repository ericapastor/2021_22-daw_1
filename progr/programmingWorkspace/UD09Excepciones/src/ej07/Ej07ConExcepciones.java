package ej07;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ej07ConExcepciones {
	
	//excepcion obligatoria de entrada salida (IOException)

		public static void main(String[] args) throws IOException {
			lectura();
		}

		public static void lectura() {

			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				System.out.println("Dame una frase");
				String frase;
				frase = br.readLine();
				System.out.println("La frase es " + frase);
			} catch (IOException e) {
				System.err.println("Se produjo un error de E/S");
			}
		}

}
