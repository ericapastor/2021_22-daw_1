package ej03;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ej03ConExcepciones {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int numero;
		try {
			System.out.println("Dame un numero");
			numero = input.nextInt();
			System.out.println("El numero es " + numero);
			input.close();
		} catch (InputMismatchException e) {
			System.out.println("Error de formato incorrecto");
		}
	}

}
