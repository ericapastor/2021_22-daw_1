package ejercicio02_menus;

import java.util.Scanner;

public class Ejercicio01 {

	static final String START = "PROGRAM STARTS";
	static final String END = "PROGRAM ENDS";

	public static void main(String[] args) {

		System.out.println(START);

		Scanner program = new Scanner(System.in);

		// variables are here

		int option;
		char optionab;

		// cases a
		long toDecimal = 0;
		int exponent;
		String fromBinary;
		String fromHex;
		String fromOctal;

		// cases b
		int fromDecimal;
		String toBinary;
		String toHex;
		String toOctal;

		do {
			System.out.println(
					"Options menu:\n\n1.-Binary conversion.\n2.-Hexadecimal conversion.\n3.-Octal conversion.\n4.-End program.\n");
			option = program.nextInt();
			switch (option) {

			default:
				System.out.println("\nOption not valid.\n");
				break;

			case 1:
				System.out.println("\n**************************");
				System.out.println("STARTING BINARY CONVERSION");
				System.out.println("**************************");
				program.nextLine();
				do {
					System.out.println(
							"\nWould you prefer:\na.-Convert from binary to decimal.\nb.-Convert from decimal to binary\nc.-Return to home screen.");
					optionab = program.nextLine().charAt(0);
					switch (optionab) {
					default:
						System.out.println("Option not valid\n");
						break;
					case 'a':
						toDecimal = 0;
						exponent = 0;
						System.out.println("Type the binary number");
						fromBinary = program.nextLine();
						for (int i = fromBinary.length() - 1; i >= 0; i--) {
							if (fromBinary.charAt(i) == '1') {
								toDecimal = toDecimal + (long) Math.pow(2, exponent);
							}
							exponent++;
						}
						System.out.println("The decimal conversion for " + fromBinary + " is: " + toDecimal);
						program.nextLine();
						break;

					case 'b':
						System.out.println("Type the decimal number");
						fromDecimal = program.nextInt();
						toBinary = Integer.toBinaryString(fromDecimal);
						System.out.println("The binary conversion for " + fromDecimal + " is: " + toBinary);
						program.nextLine();
						break;

					case 'c':
						System.out.println("\n************************");
						System.out.println("RETURNING TO HOME SCREEN");
						System.out.println("************************\n");
						break;
					}
				} while (optionab != 'c');
				break;

			case 2:
				System.out.println("\n*******************************");
				System.out.println("STARTING HEXADECIMAL CONVERSION");
				System.out.println("*******************************");
				program.nextLine();
				do {
					System.out.println(
							"\nWould you prefer:\na.-Convert from hexadecimal to decimal.\nb.-Convert from decimal to hexadecimal\nc.-Return to home screen.");
					optionab = program.nextLine().charAt(0);
					switch (optionab) {
					default:
						System.out.println("Option not valid\n");
						break;

					case 'a':
						toDecimal = 0;
						exponent = 0;
						System.out.println("Type the hexadecimal number");
						fromHex = program.nextLine();
						for (int i = fromHex.length() - 1; i >= 0; i--) {
							if (fromHex.charAt(i) >= '1' && fromHex.charAt(i) <= '9') {
								toDecimal = toDecimal + (fromHex.charAt(i) - 48) * (long) Math.pow(16, exponent);
							}
							if (fromHex.charAt(i) >= 'A' && fromHex.charAt(i) <= 'G') {
								toDecimal = toDecimal + (fromHex.charAt(i) - 55) * (long) Math.pow(16, exponent);

							}
							if (fromHex.charAt(i) >= 'a' && fromHex.charAt(i) <= 'g') {
								toDecimal = toDecimal + (fromHex.charAt(i) - 87) * (long) Math.pow(16, exponent);
							}
							exponent++;
						}
						System.out.println("The decimal conversion for " + fromHex + " is: " + toDecimal);
						break;

					case 'b':
						System.out.println("Type the decimal number");
						fromDecimal = program.nextInt();
						toHex = Integer.toHexString(fromDecimal);
						System.out.println("The hexadecimal conversion for " + fromDecimal + " is: " + toHex);
						break;

					case 'c':
						System.out.println("\n************************");
						System.out.println("RETURNING TO HOME SCREEN");
						System.out.println("************************\n");
						break;
					}
				} while (optionab != 'c');
				break;

			case 3:
				System.out.println("\n*************************");
				System.out.println("STARTING OCTAL CONVERSION");
				System.out.println("*************************");
				program.nextLine();
				do {
					System.out.println(
							"\nWould you prefer:\na.-Convert from octal to decimal.\nb.-Convert from decimal to octal\nc.-Return to home screen.");
					optionab = program.nextLine().charAt(0);
					switch (optionab) {
					default:
						System.out.println("Option not valid");
						break;

					case 'a':
						toDecimal = 0;
						exponent = 0;
						System.out.println("Type the octal number");
						fromOctal = program.nextLine();
						System.out.println(fromOctal);
						for (int i = fromOctal.length() - 1; i >= 0; i--) {
							if (fromOctal.charAt(i) >= '1' && fromOctal.charAt(i) <= '8') {
								toDecimal = toDecimal + (fromOctal.charAt(i) - 48) * (long) Math.pow(8, exponent);
							}
							exponent++;
						}
						System.out.println("The decimal conversion for " + fromOctal + " is: " + toDecimal + ".");
						break;

					case 'b':
						System.out.println("Type the decimal number");
						fromDecimal = program.nextInt();
						toOctal = Integer.toOctalString(fromDecimal);
						System.out.println("The octal conversion for " + fromDecimal + " is: " + toOctal);
						break;

					case 'c':
						System.out.println("\n************************");
						System.out.println("RETURNING TO HOME SCREEN");
						System.out.println("************************\n");
						break;
					}
				} while (optionab != 'c');
				break;

			case 4:
				System.out.println("\nThanks for using effsConverters. See you soon.\n");
				break;
			}

		} while (option != 4);

		program.close();

		System.out.println(END);

	}

}
