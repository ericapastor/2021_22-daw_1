package ejercicio02_menus;

import java.util.Scanner;

public class Ejercicio03 {

	static final String START = "PROGRAM STARTS\n";
	static final String END = "\nPROGRAM ENDS";

	public static void main(String[] args) {

		System.out.println(START);

		/*
		 * 3) El programa al ejecutarse debe mostrar el siguiente men� por pantalla Men�
		 * de opciones:
		 */
		// 1.- Volumen esfera
		// 2.- Numero de tres cifras con cifras por separado
		// 3.- Letras min�sculas
		// 4.- Pares entre dos n�meros"
		// 5.- Salir
		/*
		 * Despu�s de que aparezcan los mensajes que conforman el men�, el programa
		 * esperar� a que introduzcamos un n�mero entero. Despu�s de seleccionar una
		 * opci�n y ejecutar el c�digo referente a la opci�n debe volverse a mostrar el
		 * men� por pantalla. Solo termina el programa al introducir 5.
		 */

		Scanner program = new Scanner(System.in);

		int option;
		int number;
		int number2;
		double radio;
		char leter1;
		char leter2;
		boolean bothLow;
		boolean bothUpper;
		boolean firstUpper;
		String acumulator = "";

		do {
			System.out.println(
					"\n********************\n1 - Sphere volume.\n2 - Three digit number, separate.\n3 - Low case leters.\n4 - Pairs between two numbers.\n5 - Exit program.\n\n********************\n");
			option = program.nextInt();
			switch (option) {
			default:
				System.out.println("Option not valid.");
				break;
			case 1:

				// Opci�n 1 Esta opci�n calcula el volumen de una esfera
				do {
					System.out.println("Input the radio from the sphere to know its volume.");
					radio = program.nextDouble();
					System.out.println(
							"Volume is:\n" + "4/3*PI*radio3 = " + (((double) 4 / 3) * Math.PI * Math.pow(radio, 3)));
					System.out.println("Repeat the process?\n1 - Yes.\n2 - No.");
					option = program.nextInt();
				} while (option != 2);
				break;

			case 2:
				// Opci�n 2 Esta opci�n solicita un numero de tres cifras y las muestra por
				// separado
				// we could do this by a String using the charAt() method
				// I am going to use an int
				do {
					System.out.println("Type a three digit number.");
					number = program.nextInt();
					System.out.println("Digit 1: " + (number / 100));
					System.out.println("Digit 2: " + ((number / 10) % 10));
					System.out.println("Digit 3: " + (number % 10));
					System.out.println("Repeat the process?\n1 - Yes.\n2 - No.");
					option = program.nextInt();
				} while (option != 2);
				break;

			case 3:
				// Opci�n 3 Esta opci�n solicita dos caracteres y comprueba si los dos son
				// letras min�sculas
				program.nextLine();
				do {
					System.out.println("Type a character.");
					leter1 = program.nextLine().charAt(0);
					// first we check the first character is a leter, and not any other kind of
					// character
					if (leter1 >= 'A' && leter1 <= 'Z' || leter1 >= 'a' && leter1 <= 'z') {
						System.out.println("Type another character.");
						leter2 = program.nextLine().charAt(0);
						// we check if its a leter again
						if (leter2 >= 'A' && leter2 <= 'Z' || leter2 >= 'a' && leter2 <= 'z') {
							// check if they are low or upper case
							bothUpper = leter1 >= 'A' && leter1 <= 'Z' && leter2 >= 'A' && leter2 <= 'Z';
							bothLow = leter1 >= 'a' && leter1 <= 'z' && leter2 >= 'a' && leter2 <= 'z';
							firstUpper = leter1 >= 'A' && leter1 <= 'Z' && leter2 >= 'a' && leter2 <= 'z';
							if (bothLow) {
								System.out.println("Both are lower case.");
							} else if (bothUpper) {
								System.out.println("Both are upper case.");
							} else if (firstUpper) {
								System.out.println("First one is un upper case and the second one in lower case.");
							} else {
								System.out.println("First one is un lower case and the second one in upper case.");
							}
						} else {
							System.out.println("Character must be a leter.");
						}
					} else {
						System.out.println("Character must be a leter.");
					}
					System.out.println("Repeat the process?\n1 - Yes.\n2 - No.");
					option = program.nextInt();
					// we need to clean the buffer
					program.nextLine();
				} while (option != 2);
				break;

			case 4:
				// Opci�n 4 Esta opci�n solicita dos n�meros y muestra los n�meros pares entre
				// ellos
				do {
					System.out.println("Type the first number.");
					number = program.nextInt();
					System.out.println("Type the second number.");
					number2 = program.nextInt();
					for (int i = number + 1; i < number2; i++) {
						// i need to check all the numbers in between, dividing by 2
						if (i % 2 == 0) {
							acumulator = acumulator + i + " ";
						}
					}
					// check if there were any pairs
					if (acumulator.length() >= 1) {
						System.out.println("Pairs between those numbers are: \n" + acumulator);
					} else {
						System.out.println("There aren't any pairs in between.");
					}
					System.out.println("Repeat the process?\n1 - Yes.\n2 - No.");
					option = program.nextInt();
				} while (option != 2);
				break;

			case 5:
				System.out.println("Thanks.\nProgram will end now.");
				break;

			}
		} while (option != 5);
		
		// that's it!!!!
		// gona go make lunch, thanks for watching...

		program.close();

		System.out.println(END);

	}

}
