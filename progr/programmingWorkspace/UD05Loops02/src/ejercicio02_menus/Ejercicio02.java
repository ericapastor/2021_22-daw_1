package ejercicio02_menus;

import java.util.Scanner;

public class Ejercicio02 {

	static final String START = "PROGRAM STARTS";
	static final String END = "PROGRAM ENDS";

	public static void main(String[] args) {

		System.out.println(START);

		Scanner program = new Scanner(System.in);

		// variables are here

		int option;
		int number;

		do {
			System.out.println(
					"\n****************************************\nOptions menu:\n\n1 - Numbers from 1 to 100.\n2 - Positive, negative, pair or impair.\n3 - End program.\n\n****************************************\n");
			option = program.nextInt();
			switch (option) {

			default:
				System.out.println("\nOption not valid.\n");
				break;

			case 1:
				System.out.println("\n**********************");
				System.out.println("NUMBERS FROM 1 TO 100");
				System.out.println("**********************");
				number = 1;
				for (int i = 0; i < 100; i++) {
					System.out.println(number);
					number++;
				}
				break;

			case 2:
				do {
					System.out.println("\n************************");
					System.out.println("TYPE NUMBER TO IDENTIFY");
					System.out.println("************************");
					number = program.nextInt();
					if (number > 0) {
						System.out.print("Number is positive");
						if (number % 2 == 0) {
							System.out.println(", and pair.");
						} else if (number % 2 != 0) {
							System.out.println(", and impair.");
						}
					} else if (number < 0) {
						System.out.print("Number is negative");
						if (number % 2 == 0) {
							System.out.println(", and pair.");
						} else if (number % 2 != 0) {
							System.out.println(", and impair.");
						}
					} else {
						System.out.println("Number is zero.");
					}
					System.out.println("Do you want to read another number?\n1 - Yes.\n2 - No.");
					option = program.nextInt();

				} while (option != 2);
				break;

			case 3:
				System.out.println("\nThanks for using effsConverters. See you soon.\n");
				break;
			}

		} while (option != 3);

		program.close();

		System.out.println(END);

	}

}
