package ejercicio01_practicaexamen;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {

		/*
		 * Ejercicio 3 (3 ptos) Crear un programa que me muestre un men� y me pida
		 * seleccionar una opci�n. El men� tendr� 3 opciones:
		 */

		// 1 � calcular importe
		// 2 � contrase�a correcta
		// 3 - salir

		/*
		 * a) (1 pto) Si selecciono otra opci�n no contemplada el programa me avisar�.
		 * Debo usar un switch para ejecutar la opci�n pedida. El men� se repite despu�s
		 * de cada opci�n (1 o 2) hasta pulsar 3-salir.
		 */

		Scanner program = new Scanner(System.in);

		int option;
		double number;

		String day;
		String password;

		boolean tuesday;
		boolean thursday;
		boolean hasVowel;
		boolean hasNotLeter;

		do {
			System.out.println("Select an option.");
			System.out.println("**********************");
			System.out.println("1 - Calculate amount.");
			System.out.println("2 - Correct password.");
			System.out.println("3 - Exit.");
			System.out.println("**********************");

			option = program.nextInt();
			switch (option) {

			default:
				System.out.println("Option not valid.");
				break;

			case 1:
				/*
				 * b) (1 pto) Opci�n 1: El programa pedir� introducir una cantidad por teclado
				 * correspondiente al total de una compra y un d�a de la semana. Si el d�a es
				 * martes o jueves, se realizar� un descuento del 15% por la compra. Visualizar
				 * el descuento y el total a pagar por la compra.
				 */
				System.out.println("Introduce an amount.");
				number = program.nextDouble();
				System.out.println("What day is it today?");
				program.nextLine();
				day = program.nextLine();
				tuesday = day.equalsIgnoreCase("tuesday");
				thursday = day.equalsIgnoreCase("thursday");
				if (tuesday == true || thursday == true) {
					System.out.println("There is a discount of 15%.");
					number *= 0.85;
				} else {
					System.out.println("There is no discount today.");
				}
				System.out.println("The total amount will be: " + number);
				break;

			case 2:
				/*
				 * c) (1 pto) Opci�n 2: El programa me pide una contrase�a y muestra un �nico
				 * mensaje indicando solamente si es v�lida o no. Para que sea v�lida debe
				 * contener al menos una vocal, y solo puede contener letras. Ni cifras, ni
				 * otros caracteres. (facilita las cosas, convertir la cadena a may�sculas o
				 * min�sculas, con alg�n m�todo String).
				 */
				System.out.println("Type ur password.");
				program.nextLine();
				password = program.nextLine().toLowerCase();
				hasVowel = false;
				hasNotLeter = true;
				for (int i = 0; i < password.length(); i++) {
					if (password.charAt(i) >= 97 && password.charAt(i) <= 122) {
						if (password.charAt(i) == 'a' || password.charAt(i) == 'e' || password.charAt(i) == 'i'
								|| password.charAt(i) == 'o' || password.charAt(i) == 'u') {
							hasVowel = true;
						}
					} else {
						hasNotLeter = false;
						System.out.println("Password cannot have special characters, only leters.");
						break;
					}
				}
				if (hasVowel == true && hasNotLeter == true) {
					System.out.println("Password is correct.");
				}
				break;

			case 3:
				System.out.println("Thanks. Program will close.");
				break;

			}

		} while (option != 3);

		program.close();

	}

}
