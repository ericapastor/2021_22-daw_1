package ejercicio01_practicaexamen;

import java.util.Scanner;

public class Ejercicio04 {

	static final String START = "PROGRAM STARTS HERE\n";
	static final String END = "\nEND OF THE PROGRAM";

	public static void main(String[] args) {

		/*
		 * Ejercicio 4 (3 ptos) Crear un programa que me pide un n�mero positivo
		 * (veces). Despu�s me pide y lee tantos n�meros como me indica ese n�mero
		 * inicial (veces). (1 pto). Por cada n�mero le�do el programa me dice si es par
		 * o impar. (1 pto) Al terminar de leer todos los n�meros (cuando para el bucle)
		 * me calcula la media de todos ellos. (1 pto)
		 */

		System.out.println(START);

		Scanner program = new Scanner(System.in);

		int cycle;
		int counter = 0;
		int acumulator = 0;
		int number;
		double media;

		System.out.println("Tell me how many numbers you wish to sum.");
		cycle = program.nextInt();

		for (int i = 0; i < cycle; i++) {
			System.out.println("\n************\nType number " + (counter + 1) + ".");
			number = program.nextInt();
			if (number > 0) {
				counter++;
				acumulator += number;
				if (number % 2 == 0) {
					System.out.println("The number is a pair.");
				} else {
					System.out.println("The number is impair.");
				}
			} else {
				System.out.println("Number cannot be 0 or negative.");
				break;
			}
		}

		media = (double) acumulator / (double) counter;
		
		System.out.println("Media of numbers is: " + media + ".");

		program.close();

		System.out.println(END);

	}

}
