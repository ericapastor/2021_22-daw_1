package ejercicio01_practicaexamen;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {

		/*
		 * Ejercicio 2 (2 ptos) Pedir un n�mero entero que representa el tama�o del
		 * lado. Dibujar y rellenar un cuadrado con el car�cter �X�, con tantas filas y
		 * columnas como se ha indicado por teclado.
		 */

		Scanner program = new Scanner(System.in);

		int number;

		System.out.println("Type a number for the side of a square.");
		number = program.nextInt();

		for (int i = 0; i < number; i++) {
			for (int j = 0; j < number; j++) {
				System.out.print("x ");
			}
			System.out.println();
		}

		program.close();

	}

}
