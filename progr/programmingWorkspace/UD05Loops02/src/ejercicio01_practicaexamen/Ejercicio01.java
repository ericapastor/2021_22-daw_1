package ejercicio01_practicaexamen;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {

		Scanner program = new Scanner(System.in);

		double number;
		int acumulator;
		int counter;

		/*
		 * Ejercicio 1 (2 ptos) Pedir 10 n�meros reales por teclado y despu�s escribir
		 * la suma total
		 */

		acumulator = 0;
		counter = 1;
		for (int i = 0; i < 10; i++) {
			System.out.println("Type number " + counter + ".");
			counter++;
			number = program.nextDouble();
			acumulator += number;
		}
		System.out.println("Total is: " + acumulator);

		program.close();

	}

}
