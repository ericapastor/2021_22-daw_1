package ejercicio03_formas;

import java.util.Scanner;

public class Ejercicio05_02 {

	public static void main(String[] args) {

		// ask for the side of a perfect square and draw it void

		Scanner program = new Scanner(System.in);

		int side;

		do {
			System.out.println("Type the side of the square.");
			side = program.nextInt();

			for (int i = 1; i <= side; i++) {
				for (int j = 1; j <= side; j++) {
					if (i == 1 || i == side) {
						System.out.print("* ");
					} else {
						if (j == 1 || j == side) {
							System.out.print("* ");
						} else {
							System.out.print("  ");
						}
					}
				}
				System.out.println();
			}

			System.out.println();

		} while (side > 0);

		program.close();

	}

}
