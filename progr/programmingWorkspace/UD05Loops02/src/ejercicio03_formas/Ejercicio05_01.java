package ejercicio03_formas;

import java.util.Scanner;

public class Ejercicio05_01 {

	public static void main(String[] args) {

		// ask for the side of a perfect square and draw it void

		Scanner program = new Scanner(System.in);

		int x;

		do {
			System.out.println("Type the side of the square.");
			x = program.nextInt();

			for (int i = 0; i < x; i++) {
				System.out.print("* ");
			}
			System.out.println();
			for (int i = 0; i < (x - 2); i++) {
				for (int j = 0; j < 1; j++) {
					System.out.print("* ");
				}
				for (int j = 0; j < (x - 2); j++) {
					System.out.print("  ");
				}
				for (int j = 0; j < 1; j++) {
					System.out.print("* ");
				}
				System.out.println();
			}
			for (int i = 0; i < x; i++) {
				System.out.print("* ");
			}
			System.out.println();

		} while (x > 0);

		program.close();

	}

}
