package ejercicio03_formas;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {

		// ask for width and height to draw a square

		Scanner program = new Scanner(System.in);

		int width;
		int height;

		System.out.println("Type the width of the square.");
		width = program.nextInt();
		System.out.println("Type the height of the square.");
		height = program.nextInt();

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}

		program.close();

	}

}
