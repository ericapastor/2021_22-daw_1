package ejercicio03_formas;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {

		// ask for the height to draw a stair
		// which adds 1 at each height

		Scanner program = new Scanner(System.in);

		int height;
		System.out.println("Type the height.");
		height = program.nextInt();

		for (int i = 0; i < height; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}

		program.close();

	}

}
