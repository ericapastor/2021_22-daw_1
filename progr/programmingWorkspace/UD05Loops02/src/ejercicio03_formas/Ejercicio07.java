package ejercicio03_formas;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {

		Scanner program = new Scanner(System.in);

		// draw a tree

		int height;
		do {

			System.out.println("Type the height.");
			height = program.nextInt();

			for (int i = 0; i <= height; i++) {
				for (int j = height * 3; j >= i; j--) {
					System.out.print(" ");
				}
				for (int j = 0; j < i * 2 - 1; j++) {
					System.out.print("*");
				}
				System.out.println();
			}
			for (int i = 0; i < height * height / 3; i++) {
				for (int j = height * 3 - 1; j >= i; j--) {
					System.out.print(" ");
				}
				for (int j = 0; j < i * 2 + 1; j++) {
					System.out.print("*");
				}
				System.out.println();
			}
			for (int i = 0; i <= height; i++) {
				for (int j = height * 2 + 2; j >= 0; j--) {
					System.out.print(" ");
				}
				for (int j = 0; j <= height * height / 6; j++) {
					System.out.print("*");
				}
				System.out.println();
			}
		} while (height != 0);

		program.close();

	}

}