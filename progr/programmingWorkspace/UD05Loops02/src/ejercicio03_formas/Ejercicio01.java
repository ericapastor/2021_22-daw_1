package ejercicio03_formas;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {

		// ask for a positive number to draw a perfect square with *

		Scanner program = new Scanner(System.in);

		int number;

		System.out.println("Type the number of the side to draw a square.");
		number = program.nextInt();
		for (int i = 0; i < number; i++) {
			for (int j = 0; j < number; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}

		program.close();

	}

}
