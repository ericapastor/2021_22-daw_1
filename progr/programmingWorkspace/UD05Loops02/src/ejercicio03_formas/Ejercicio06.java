package ejercicio03_formas;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {

		// ask for a height to get an inverted pyramid

		Scanner program = new Scanner(System.in);

		int height;

		System.out.println("Type the height.");
		height = program.nextInt();
		for (int i = height; i > 0; i--) {
			for (int j = 0; j <= (height - i); j++) {
				System.out.print(" ");
			}
			for (int j = (i * 2 - 1); j > 0; j--) {
				System.out.print("*");
			}
			System.out.println();
		}

		program.close();

	}

}
