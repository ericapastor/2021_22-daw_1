package ejercicio03_formas;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {

		// ask for a height to draw a pyramid

		Scanner program = new Scanner(System.in);

		int number;

		do {
			System.out.println("Type the height of the pyramid.");
			number = program.nextInt();
			for (int i = 0; i < number; i++) {
				for (int j = number; j >= i; j--) {
					System.out.print(" ");
				}
				for (int k = 0; k < (i * 2 - 1); k++) {
					System.out.print("*");
				}
				System.out.println();
			}
			System.out.println();
		} while (number > 0);

		program.close();

	}

}
