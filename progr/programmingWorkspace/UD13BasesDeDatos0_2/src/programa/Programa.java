package programa;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import clases.Conexion;
import clases.Persona;

public class Programa {

	public static void main(String[] args) {
		Persona persona1 = new Persona(1, "Maria", "Martin", "maria@persona.com", new java.util.Date());
		Persona persona2 = new Persona(2, "Carlos", "Garcia", "carlos@persona.com", new java.util.Date());
		try {
			// CREATE
			insertamosPersona(persona1);
			insertamosPersona(persona2);
			System.out.println("Personas insertadas en la BD");
			System.out.println("-----------------------------------------------------------------------");

			// READ
			List<Persona> personas = recuperarPersonas();
			System.out.println("Recuperamos personas de la BD");
			for (Persona pers : personas) {
				System.out.println(pers);
			}
			System.out.println("-----------------------------------------------------------------------");
			// UPDATE
			System.out.println("Actualizamos primer apellido de la persona ");
			actualizarPrimerApellidoPersona(persona1);
			persona1.setPrimerApellido("Apellido nuevo");
			System.out.println("Mostramos el apellido actualizado " + persona1);
			System.out.println("-----------------------------------------------------------------------");
			// READ (SOLO UNO)
			Persona tempPersona2 = recuperamosPersona(2);
			System.out.println("Recuperamos persona2 de la BD " + tempPersona2);
			System.out.println("-----------------------------------------------------------------------");
			// DELETE
			borramosPersona(2);
			System.out.println("Borramos persona2 de la BD ");
			System.out.println("-----------------------------------------------------------------------");
			// READ
			List<Persona> tempPersonas = recuperarPersonas();
			System.out.println("Recuperamos personas de la BD. Persona2 no esta");
			for (Persona pers : tempPersonas) {
				System.out.println(pers);
			}
			System.out.println("-----------------------------------------------------------------------");
			// DELETE
			borramosTodosRegistros();
			System.out.println("Borramos todos los registros de la BD. Tabla Personas");
		} catch (SQLException e) {
			System.out.println("Ha ocurrido una excepcion " + e.getMessage());
		} catch (Exception e) {
			System.out.println("Ha ocurrido una excepcion " + e.getMessage());
		}
	}

	// METODO BORRAR TODOS LOS REGISTROS
	private static void borramosTodosRegistros() throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = Conexion.getConnection();
			if (con == null) {
				System.out.println("Error de conexion, comprueba si el servidor esta en marcha");
				return;
			}
			ps = con.prepareStatement("DELETE FROM PERSONA");
			ps.execute();
		} catch (SQLException e) {
			throw e;
		} finally {
			ps.close();
			con.close();
		}
	}

	// METODO BORRAR PERSONA PARTIENDO DE UN ID
	private static void borramosPersona(int id) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = Conexion.getConnection();
			ps = con.prepareStatement("DELETE FROM PERSONA WHERE ID=?");
			ps.setLong(1, id);
			ps.execute();
		} catch (SQLException e) {
			throw e;
		} finally {
			ps.close();
			con.close();
		}
	}

	// METODO RECUPERAR UNA PERSONA PARTIENDO DE UN ID
	private static Persona recuperamosPersona(int id) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Persona persona = new Persona();
		try {
			con = Conexion.getConnection();
			if (con == null) {
				System.out.println("Error de conexion, comprueba si el servidor esta en marcha");
				return persona;
			}
			ps = con.prepareStatement(
					"SELECT ID,PRIMER_APELLIDO,SEGUNDO_APELLIDO,EMAIL,FECHA_NACIMIENTO FROM PERSONA WHERE ID=?");
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				persona.setId(rs.getInt("ID"));
				persona.setPrimerApellido(rs.getString("PRIMER_APELLIDO"));
				persona.setSegundoApellido(rs.getString("SEGUNDO_APELLIDO"));
				persona.setEmail(rs.getString(4)); // recuperamos usando el indice de la columna
				persona.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			ps.close();
			con.close();
		}
		return persona;
	}

	// METODO ACTUALIZAR PRIMER APELLIDO PERSONA
	private static void actualizarPrimerApellidoPersona(Persona persona) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = Conexion.getConnection();
			if (con == null) {
				System.out.println("Error de conexion, comprueba si el servidor esta en marcha");
				return;
			}
			con.setAutoCommit(false);
			ps = con.prepareStatement("UPDATE PERSONA SET PRIMER_APELLIDO=? WHERE ID=?");
			ps.setString(1, persona.getPrimerApellido());
			ps.setLong(2, persona.getId());
			ps.execute();
			con.commit();
		} catch (SQLException e) {
			try {
				if (con != null) {
					con.rollback();
					throw e;
				}
			} catch (SQLException e1) {
				throw e1;
			}
		} finally {
			ps.close();
			con.close();
		}
	}

	// METODO RECUPERAR PERSONAS
	private static List<Persona> recuperarPersonas() throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Persona> personas = new ArrayList<Persona>();
		try {
			con = Conexion.getConnection();
			if (con == null) {
				System.out.println("Error de conexion, comprueba si el servidor esta en marcha");
				return personas;
			}
			ps = con.prepareStatement("SELECT ID,PRIMER_APELLIDO,SEGUNDO_APELLIDO,EMAIL,FECHA_NACIMIENTO FROM PERSONA");
			rs = ps.executeQuery();
			while (rs.next()) {
				Persona pers = new Persona();
				pers.setId(rs.getInt("ID"));
				pers.setPrimerApellido(rs.getString("PRIMER_APELLIDO"));
				pers.setSegundoApellido(rs.getString("SEGUNDO_APELLIDO"));
				pers.setEmail(rs.getString(4)); // recuperamos usando el indice de la columna
				pers.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
				personas.add(pers);
			}
		} catch (SQLException e) {
			throw e;
		} finally {
			rs.close();
			ps.close();
			con.close();
		}
		return personas;
	}

	// INSERTAR UNA PERSONA
	private static void insertamosPersona(Persona persona) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = Conexion.getConnection();
			if (con == null) {
				System.out.println("Error de conexion, comprueba si el servidor esta en marcha");
				return;
			}
			con.setAutoCommit(false);
			ps = con.prepareStatement(
					"INSERT INTO PERSONA(ID,PRIMER_APELLIDO,SEGUNDO_APELLIDO,EMAIL,FECHA_NACIMIENTO) VALUES(?,?,?,?,?)");
			ps.setInt(1, persona.getId());
			ps.setString(2, persona.getPrimerApellido());
			ps.setString(3, persona.getSegundoApellido());
			ps.setString(4, persona.getEmail());
			ps.setDate(5, new Date(persona.getFechaNacimiento().getTime()));
			ps.execute();
			con.commit();
		} catch (SQLException e) {
			try {
				if (con != null) {
					con.rollback();
				}
			} catch (SQLException e1) {
				throw e1;
			}
			throw e;
		} finally {
			ps.close();
			con.close();
		}
	}
}