package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	static final String START = "PROGRAM STARTS\n";
	static final String END = "\nPROGRAM ENDS";

	public static void main(String[] args) {

		System.out.println(START);

		Scanner program = new Scanner(System.in);

		int numberofSales;
		int counter = 0;
		double price;
		double totalAmount = 0;

		System.out.println("Cuantas ventas quieres introducir?");
		numberofSales = program.nextInt();

		for (int i = 0; i < numberofSales; i++) {
			System.out.println("Venta numero: " + (counter + 1));
			price = program.nextDouble();
			if (price >= 0) {
				counter++;
				totalAmount = totalAmount + price;
			} else {
				System.out.println("Sale can't be negative.");
			}
		}

		System.out.println(
				"Number of sales: " + counter + "\nTotal amount without VAT: " + totalAmount + "�\nPrice of VAT: "
						+ (totalAmount * 0.21) + "�\nTotal amount with VAT: " + (totalAmount * 1.21) + "�");

		program.close();

		System.out.println(END);

	}

}
