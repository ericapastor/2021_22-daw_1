package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	static final String START = "PROGRAM STARTS\n";
	static final String END = "\nPROGRAM ENDS";

	public static void main(String[] args) {

		System.out.println(START);

		Scanner program = new Scanner(System.in);

		// variables here -->>>
		int option;
		int numberOfGrades;
		int counter = 0;
		int width;
		int height;
		double grades;
		double lowestGrade = 9999999;
		boolean itsNumber = false;
		boolean itsLeter = false;
		String course;

		do {
			System.out.println("*********\nMain menu:");
			System.out.println("1 - Lowest grade.");
			System.out.println("2 - Correct course.");
			System.out.println("3 - Multiplication tables.");
			System.out.println("4 - Draw a square.");
			System.out.println("5 - Exit program.");
			System.out.println("*********\n\nChoose an option.\n");
			option = program.nextInt();

			switch (option) {
			default:
				System.out.println("\nOption not valid.\n");
				break;

			case 1:
				do {
					System.out.println("How many grades you wish to input?");
					numberOfGrades = program.nextInt();
					for (int i = 0; i < numberOfGrades; i++) {
						System.out.println("Grade number " + (counter + 1) + ":");
						grades = program.nextDouble();
						if (grades >= 0) {
							if (grades < lowestGrade) {
								lowestGrade = grades;
							}
						} else {
							System.out.println("Grade is negative so it won't count as valid.");
						}
						counter++;
					}
					System.out.println("\nLowest grade is: " + lowestGrade + "\n");
					System.out.println("Repeat the process?\n1 - Yes.\n2 - No.\n");
					option = program.nextInt();
				} while (option != 2);
				break;

			case 2:
				do {
					program.nextLine();
					System.out.println("Type the course of the student. Must be 2 numbers, followed by 5 leters.");
					course = program.nextLine().toLowerCase();
					if (course.length() == 7) {
						for (int i = 0; i < (course.length() - 5); i++) {
							if (course.charAt(i) >= '0' && course.charAt(i) <= '9') {
								itsNumber = true;
							} else {
								System.out.println("\nFirst two digits must be numbers.\n");
								break;
							}
						}
						for (int i = 2; i < (course.length() - 2); i++) {
							if (course.charAt(i) >= 97 && course.charAt(i) <= 122) {
								itsLeter = true;
							} else {
								System.out.println("\nFive last digits must be leters.\n");
								break;
							}
						}
						if (itsNumber == true && itsLeter == true) {
							System.out.println("\nCourse is valid.\n");
						} else {
							System.out.println("\nCourse is not valid.\n");
						}
					} else {
						System.out.println(
								"\nCourse must be 2 numbers, followerd by 5 leters. Must be a 7 digit String.\n");
					}
					System.out.println("Repeat the process?\n1 - Yes.\n2 - No.\n");
					option = program.nextInt();
				} while (option != 2);
				break;

			case 3:
				do {
					for (int i = 1; i <= 10; i++) {
						for (int j = 1; j <= 10; j++) {
							System.out.println(i + "*" + j + " = " + (i * j));
						}
					}
					System.out.println("Repeat the process?\n1 - Yes.\n2 - No.\n");
					option = program.nextInt();
				} while (option != 2);
				break;

			case 4:
				do {
					System.out.println("Type the width of the square.");
					width = program.nextInt();
					System.out.println("Type the height of the square.");
					height = program.nextInt();
					for (int i = 0; i < height; i++) {
						for (int j = 0; j < width; j++) {
							System.out.print(" *");
						}
						System.out.println();
					}
					System.out.println("Repeat the process?\n1 - Yes.\n2 - No.\n");
					option = program.nextInt();
				} while (option != 2);
				System.out.println("\n");
				break;

			case 5:
				System.out.println("Thank you. Program will close.");
				break;

			}

		} while (option != 5);

		program.close();

		System.out.println(END);

	}
}
