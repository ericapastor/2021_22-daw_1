package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	static final String START = "PROGRAM STARTS\n";
	static final String END = "\nPROGRAM ENDS";

	public static void main(String[] args) {

		System.out.println(START);

		Scanner program = new Scanner(System.in);

		int number;
		int multiple;
		int sum = 0;
		int counter = 0;

		System.out.println("Type a number of times.\n");
		number = program.nextInt();

		for (int i = 0; i < number; i++) {
			System.out.println("\nType number " + (counter + 1));
			multiple = program.nextInt();
			if (multiple % 5 == 0) {
				System.out.println("It's multiple of 5");
			} else {
				System.out.println("It's not multiple of 5");
			}
			sum = sum + multiple;
			counter++;
		}

		System.out.println("\nTotal sum is: " + sum);

		program.close();

		System.out.println(END);

	}

}
