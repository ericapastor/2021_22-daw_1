package programa;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PedirDatos {

	public static Scanner in = new Scanner(System.in);

	public static String texto() {
		String texto = "";
		while (texto.equals("")) {
			texto = in.nextLine();
		}
		return texto;
	}

	public static int enteros() {
		while (true) {
			try {
				int enteros = in.nextInt();
				in.nextLine();
				if (enteros < 0) {
					System.out.println("No puedes introducir numeros negativos.");
				} else {
					return enteros;
				}
			} catch (InputMismatchException e) {
				System.out.println("Introduce solamente numeros enteros, por favor.");
				in.nextLine();
			}
		}
	}

	public static double decimales() {
		while (true) {
			try {
				double decimal = in.nextDouble();
				in.nextLine();
				if (decimal < 0) {
					System.out.println("No puedes introducir numeros negativos.");
				} else {
					return decimal;
				}
			} catch (InputMismatchException e) {
				System.out.println("Introduce solamente numeros con decimales, por favor.");
				in.nextLine();
			}
		}
	}

	public static boolean bool() {
		String bool = "";
		boolean b = !bool.equalsIgnoreCase("si") && (!bool.equalsIgnoreCase("no"));
		while (b) {
			bool = in.nextLine();
			if (b) {
				if (bool.equalsIgnoreCase("si")) {
					return true;
				} else {
					return false;
				}
			} else {
				System.out.println("Escribe solamente [si] o [no]");
			}
		}
		return b;
	}

}
