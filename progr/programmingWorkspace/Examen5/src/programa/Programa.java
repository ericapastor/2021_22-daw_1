package programa;

import java.io.Serializable;

import clases.Supermercado;

public class Programa implements Serializable {

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		Supermercado gestion = new Supermercado();
		// cargar datos descomentar cuando haya datos
		gestion.cargarDatosProductos();
		gestion.cargarDatosClientes();
		int opcion = 0;
		do {
			System.out.println("Bienvenido a nuestro supermercado");
			System.out.println("Quieres:");
			System.out.println("1.-Darte de alta como cliente");
			System.out.println("2.-Dar de alta productos");
			System.out.println("3.-Ver nuestros productos");
			System.out.println("4.-Realizar un pedido");
			System.out.println("5.-Listar todas las ventas");
			System.out.println("6.-Ver tus ventas");
			System.out.println("7.-Guardar datos");
			System.out.println("8.-Salir");
			opcion = PedirDatos.enteros();
			switch (opcion) {
			case 1:
				gestion.altaCliente();
				break;
			case 2:
				gestion.altaProducto();
			case 3:
				gestion.mostrarProductos();
				break;
			case 4:
				// por hacer
				gestion.realizarPedido();
				break;
			case 5:
				gestion.listarVentasTotales();
				break;
			case 6:
				gestion.mostrarVentasACliente();
				break;
			case 7:
				gestion.guardarDatos();
			default:
				break;
			}
		} while (opcion != 8);
	}
}
