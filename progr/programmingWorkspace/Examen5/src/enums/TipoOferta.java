package enums;

import java.io.Serializable;

public enum TipoOferta implements Serializable {

	NINGUNA, SEMANAL, DIARIA;

	public String tipoOferta() {
		switch (this) {
		case SEMANAL:
			return "semanal";
		case DIARIA:
			return "diaria";
		default:
			return "ninguna";
		}
	}

}
