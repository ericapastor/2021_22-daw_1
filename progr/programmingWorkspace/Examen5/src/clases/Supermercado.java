package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

import enums.TipoOferta;
import programa.PedirDatos;

public class Supermercado implements Serializable {

	private static String datosProductos = "src/datosProductos.dat";
	private static String datosClientes = "src/datosClientes.dat";
	private static String datosVentas = "src/datosVentas.dat";

	private static final long serialVersionUID = 1L;

	private ArrayList<Productos> productos = new ArrayList<Productos>();
	private ArrayList<Clientes> clientes = new ArrayList<Clientes>();
	private ArrayList<Ventas> ventas = new ArrayList<Ventas>();

	public Supermercado() {
	}

	public ArrayList<Productos> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Productos> productos) {
		this.productos = productos;
	}

	public ArrayList<Clientes> getClientes() {
		return clientes;
	}

	public void setClientes(ArrayList<Clientes> clientes) {
		this.clientes = clientes;
	}

	public ArrayList<Ventas> getVentas() {
		return ventas;
	}

	public void setVentas(ArrayList<Ventas> ventas) {
		this.ventas = ventas;
	}

	// ************************************************
	// por definir
	// osea poner bonito
	// ************************************************
	@Override
	public String toString() {
		return "Supermercado [productos=" + productos + ", clientes=" + clientes + ", ventas=" + ventas + "]";
	}

	private void altaCliente(String nombre) {
		clientes.add(new Clientes("C" + clientes.size(), nombre, LocalDate.now()));
		Collections.sort(clientes);
	}

	private void altaProducto(String nombreProducto, double precio, boolean fragil) {
		Productos p = new ProductosEspeciales(nombreProducto, precio, fragil);
		p.calcularPrecio(precio);
		productos.add(p);
		Collections.sort(productos);
	}

	private void altaProducto(String nombreProducto, double precio, TipoOferta tipoOferta) {
		Productos p = new ProductosOferta(nombreProducto, precio, tipoOferta);
		p.calcularPrecio(precio);
		productos.add(p);
		Collections.sort(productos);
	}

	public void altaCliente() {
		boolean conf = true;
		while (conf) {
			System.out.println("Dime el nombre del cliente para darlo de alta.");
			String nombre = PedirDatos.texto();
			altaCliente(nombre);
			System.out.println("Quieres dar de alta otro cliente: [si/no]");
			conf = PedirDatos.bool();
			if (!conf) {
				System.out.println("Volviendo hacia atras...");
			}
		}
	}

	public void altaProducto() {
		boolean conf = true;
		while (conf) {
			System.out.println("Quieres dar de alta:\n1.-Productos especiales\n2.-Productos en oferta");
			int opcion = PedirDatos.enteros();
			switch (opcion) {
			case 1:
				altaProductoEspecial();
				break;
			case 2:
				altaProductoOferta();
				break;
			default:
				System.out.println("Opcion no contemplada.");
			}
			System.out.println("Quieres dar de alta otro producto: [si/no]");
			conf = PedirDatos.bool();
			if (!conf) {
				System.out.println("Volviendo a hacia atras.");
			}
		}
	}

	private void altaProductoEspecial() {
		boolean conf;
		System.out.println("Dame el nombre del producto");
		String nombreProducto = PedirDatos.texto();
		System.out.println("Que precio base tiene?");
		double precio = PedirDatos.decimales();
		System.out.println("Dime si es fragil o no");
		boolean fragil = PedirDatos.bool();
		System.out.println("Estos son los datos:");
		System.out.println("Nombre: " + nombreProducto);
		System.out.println("Precio base: " + precio);
		System.out.println("Es fragil: " + (fragil ? "Si" : "No"));
		System.out.println("Estas de acuerdo [si/no]");
		conf = PedirDatos.bool();
		if (!conf) {
			conf = true;
			System.out.println("Volvamos a empezar");
		} else {
			altaProducto(nombreProducto, precio, fragil);
		}
	}

	private void altaProductoOferta() {
		boolean conf;
		System.out.println("Dame el nombre del producto");
		String nombreProducto = PedirDatos.texto();
		System.out.println("Que precio base tiene?");
		double precio = PedirDatos.decimales();
		System.out.println("Dime el tipo de oferta");
		String oferta = "";
		boolean ofertaCorr = !oferta.equalsIgnoreCase("diaria") && !oferta.equals("semanal")
				&& !oferta.equalsIgnoreCase("ninguna");
		do {
			oferta = PedirDatos.texto();
			if (!ofertaCorr) {
				System.out.println("Di solamente \"diaria\", \"semanal\" o \"ninguna\"");
			}
		} while (!ofertaCorr);
		oferta = oferta.toUpperCase();
		TipoOferta tipoOferta = TipoOferta.NINGUNA;
		switch (oferta) {
		case "DIARIA":
			tipoOferta = TipoOferta.DIARIA;
		case "SEMANAL":
			tipoOferta = TipoOferta.SEMANAL;
		}
		System.out.println("Estos son los datos:");
		System.out.println("Nombre: " + nombreProducto);
		System.out.println("Precio base: " + precio);
		System.out.println("Tipo de oferta: " + oferta);
		System.out.println("Estas de acuerdo [si/no]");
		conf = PedirDatos.bool();
		if (!conf) {
			conf = true;
			System.out.println("Volvamos a empezar");
		} else {
			altaProducto(nombreProducto, precio, tipoOferta);
		}
	}

	void addPtoVenta(Ventas venta, String nombreProducto) {
		for (Ventas v : ventas) {
			if (v.getCodigoVenta().equals(venta.getCodigoVenta())) {
				if (!venta.getProductos().isEmpty()) {
					for (Productos p : venta.getProductos()) {
						if (p.getNombreProducto().equalsIgnoreCase(nombreProducto)) {
							System.out.println("Ya tienes ese producto en la cesta.");
						} else {
							v.getProductos().add(p);
							System.out.println("Producto a�adido.");
							Collections.sort(v.getProductos());
						}
					}
				}
			}
		}
	}

	public void realizarPedido() {
		Clientes cliente = buscarCliente();
		Ventas venta = new Ventas("V" + ventas.size(), LocalDate.now(), cliente);
		boolean conf = true;
		while (conf) {
			System.out.println("Dime el nombre del producto que quieres incluir en la cesta");
			String nombreProducto = PedirDatos.texto();
			addPtoVenta(venta, nombreProducto);
			System.out.println("Quieres incluir otro producto [si/no]");
			conf = PedirDatos.bool();
		}
		System.out.println("Tu venta ha sido realizada");
		for (Ventas v : ventas) {
			if (v.getCodigoVenta().equals(venta.getCodigoVenta())) {
				System.out.println("El precio total es: " + v.calcularPrecioTotal());
			}
		}

	}

	private Clientes buscarCliente() {
		System.out.println("Dime el nombre del cliente:");
		String nombre = PedirDatos.texto();
		for (Clientes c : clientes) {
			if (c.getNombre().equalsIgnoreCase(nombre)) {
				return c;
			}
		}
		return null;
	}

	public void mostrarVentasACliente() {
		if (buscarCliente() == null) {
			System.out.println("El cliente no existe.");
		} else {
			String nombre = PedirDatos.texto();
			int c = 1;
			for (Ventas v : ventas) {
				if (v.getCliente().getNombre().equalsIgnoreCase(nombre)) {
					System.out.println("Venta " + c);
					System.out.println(v);
					System.out.println("Precio total: " + v.calcularPrecioTotal());
					c++;
				}
			}
		}
	}

	public void listarVentasTotales() {
		// datos del cliente y los productos
		// precio total en cada venta
		for (Ventas v : ventas) {
			System.out.println("Cliente: ");
			System.out.println(v.getCliente());
			System.out.println("Precio total: " + v.calcularPrecioTotal());
			System.out.println("Productos: ");
			System.out.println(v.getProductos());
		}
	}

	public void mostrarProductos() {
		for (Productos p : productos) {
			System.out.println(p);
		}
	}

	private void guardarDatosProductos() {
		try {
			ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(new File(datosProductos)));
			f.writeObject(productos);
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fallo al buscar el archivo");
		} catch (IOException e) {
			System.out.println("Fallo de entrada/salida de datos");
			System.exit(0);
		}
	}

	private void guardarDatosClientes() {
		try {
			ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(new File(datosClientes)));
			f.writeObject(clientes);
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fallo al buscar el archivo");
		} catch (IOException e) {
			System.out.println("Fallo de entrada/salida de datos");
			System.exit(0);
		}

	}

	private void guardarDatosVentas() {
		try {
			ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(new File(datosVentas)));
			f.writeObject(ventas);
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fallo al buscar el archivo");
		} catch (IOException e) {
			System.out.println("Fallo de entrada/salida de datos");
			System.exit(0);
		}
	}

	@SuppressWarnings("unchecked")
	public void cargarDatosProductos() {
		try {
			ObjectInputStream f = new ObjectInputStream(new FileInputStream(new File(datosProductos)));
			productos = (ArrayList<Productos>) f.readObject();
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fallo al buscar el archivo");
		} catch (IOException e) {
			System.out.println("Fallo de entrada/salida de datos");
			System.exit(0);
		} catch (ClassNotFoundException e) {
			System.out.println("Te has dejado alguna clase sin implementar, melona.");
		}
	}

	@SuppressWarnings("unchecked")
	public void cargarDatosClientes() {
		try {
			ObjectInputStream f = new ObjectInputStream(new FileInputStream(new File(datosClientes)));
			clientes = (ArrayList<Clientes>) f.readObject();
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fallo al buscar el archivo");
		} catch (IOException e) {
			System.out.println("Fallo de entrada/salida de datos");
			System.exit(0);
		} catch (ClassNotFoundException e) {
			System.out.println("Te has dejado alguna clase sin implementar, melona.");
		}
	}

	@SuppressWarnings("unchecked")
	private void cargarDatosVentas() {
		try {
			ObjectInputStream f = new ObjectInputStream(new FileInputStream(new File(datosVentas)));
			ventas = (ArrayList<Ventas>) f.readObject();
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fallo al buscar el archivo");
		} catch (IOException e) {
			System.out.println("Fallo de entrada/salida de datos");
			System.exit(0);
		} catch (ClassNotFoundException e) {
			System.out.println("Te has dejado alguna clase sin implementar, melona.");
		}
	}

	public void guardarDatos() {
		if (!productos.isEmpty()) {
			guardarDatosProductos();
		}
		if (!clientes.isEmpty()) {
			guardarDatosClientes();
		}
		if (!ventas.isEmpty()) {
			guardarDatosVentas();
		}
	}

	public void cargarDatos() {
		cargarDatosProductos();
		cargarDatosClientes();
		cargarDatosVentas();
	}

}
