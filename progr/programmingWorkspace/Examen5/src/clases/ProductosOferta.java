package clases;

import java.io.Serializable;

import enums.TipoOferta;

public final class ProductosOferta extends Productos implements Serializable {

	private static final long serialVersionUID = 1L;

	private TipoOferta tipoOferta;

	public ProductosOferta() {
	}

	public ProductosOferta(String nombreProducto, double precio, TipoOferta tipoOferta) {
		super(nombreProducto, precio);
		this.tipoOferta = tipoOferta;
	}

	public TipoOferta getTipoOferta() {
		return tipoOferta;
	}

	public void setTipoOferta(TipoOferta tipoOferta) {
		this.tipoOferta = tipoOferta;
	}

	@Override
	public String toString() {
		return super.toString() + "Tipo de oferta: " + tipoOferta.tipoOferta();
	}

	@Override
	public int compareTo(Productos o) {
		return nombreProducto.compareTo(o.getNombreProducto());
	}

	@Override
	double calcularPrecio(double precio) {
		if (tipoOferta == TipoOferta.SEMANAL) {
			return precio * 0.9;
		} else if (tipoOferta == TipoOferta.DIARIA) {
			return precio * 0.85;
		}
		return precio;
	}

}
