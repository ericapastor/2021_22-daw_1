package clases;

import java.io.Serializable;

public final class ProductosEspeciales extends Productos implements Serializable {

	private static final long serialVersionUID = 1L;

	boolean fragil;

	public ProductosEspeciales() {
	}

	public ProductosEspeciales(String nombreProducto, double precio, boolean fragil) {
		super(nombreProducto, precio);
		this.fragil = fragil;
	}

	public boolean isFragil() {
		return fragil;
	}

	public void setFragil(boolean fragil) {
		this.fragil = fragil;
	}

	@Override
	public String toString() {
		return super.toString() + "Es fragil: " + (fragil ? "si" : "no");
	}

	@Override
	public int compareTo(Productos o) {
		return nombreProducto.compareTo(o.getNombreProducto());
	}

	@Override
	double calcularPrecio(double precio) {
		if (fragil) {
			return precio + 10;
		} else {
			return precio;
		}
	}

}
