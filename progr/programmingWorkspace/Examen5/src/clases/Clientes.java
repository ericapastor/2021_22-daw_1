package clases;

import java.io.Serializable;
import java.time.LocalDate;

public class Clientes implements Comparable<Clientes>, Serializable {

	private static final long serialVersionUID = 1L;

	private String codigoCliente;
	private String nombre;
	private LocalDate fechaAlta;

	public Clientes() {
	}

	public Clientes(String codigoCliente, String nombre, LocalDate fechaAlta) {
		this.codigoCliente = codigoCliente;
		this.nombre = nombre;
		this.fechaAlta = fechaAlta;
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Override
	public String toString() {
		return "Codigo de cliente: " + codigoCliente + "\nNombre: " + nombre + "\nFechaAlta: " + fechaAlta;
	}

	@Override
	public int compareTo(Clientes o) {
		return codigoCliente.compareTo(codigoCliente);
	}

}
