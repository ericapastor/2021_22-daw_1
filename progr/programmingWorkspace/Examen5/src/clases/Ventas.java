package clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Ventas implements Comparable<Clientes>, Serializable {

	private static final long serialVersionUID = 1L;

	private String codigoVenta;
	private LocalDate fechaVenta;
	private Clientes cliente;

	private ArrayList<Productos> productos;

	public Ventas() {
	}

	public Ventas(String codigoVenta, LocalDate fechaVenta, Clientes cliente) {
		this.codigoVenta = codigoVenta;
		this.fechaVenta = fechaVenta;
		this.cliente = cliente;
		productos = new ArrayList<Productos>();
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public String getCodigoVenta() {
		return codigoVenta;
	}

	public void setCodigoVenta(String codigoVenta) {
		this.codigoVenta = codigoVenta;
	}

	public LocalDate getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(LocalDate fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public ArrayList<Productos> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Productos> productos) {
		this.productos = productos;
	}

	@Override
	public String toString() {
		System.out.println("Codigo de venta: " + codigoVenta + "\nFecha de venta: " + fechaVenta + "\nCliente: "
				+ cliente + "\nProductos:\n");
		mostrarProductos();
		return "";
	}

	public void mostrarProductos() {
		for (Productos p : productos) {
			System.out.println(p);
		}
	}

	public int calcularPrecioTotal() {
		double sumaPrecio = 0;
		for (Productos p : productos) {
			sumaPrecio = sumaPrecio + p.getPrecio();
		}
		return 0;
	}

	@Override
	public int compareTo(Clientes o) {
		return codigoVenta.compareTo(codigoVenta);
	}

}
