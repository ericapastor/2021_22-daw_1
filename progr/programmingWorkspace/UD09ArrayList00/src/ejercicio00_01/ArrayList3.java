package ejercicio00_01;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayList3 {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// creo un arraylist
		ArrayList<String> lista = new ArrayList<String>();
		// lo relleno
		lista.add("Elemento1");
		lista.add("Elemento2");
		lista.add("Elemento3");
		lista.add("Elemento4");
		System.out.println("Mostramos con un for");
		// mostrar con for
		for (int i = 0; i < lista.size(); i++) {
			System.out.println(lista.get(i));
		}
		System.out.println("\nMostramos con iterator");
		// mostrar con iterador
		// obtengo el iterador
		// importamos desde java.util
		Iterator<String> iterador = lista.iterator();
		// se repite mientras que haya un elemento a continuacion (hasNext())
		while (iterador.hasNext()) {
			// iterador.next() permite acceder a cada elemento
			String elemento = iterador.next();
			System.out.println(elemento);
		}
		// mostrar con for each
		System.out.println("\nMostramos con foreach");
		for (String elemento : lista) {
			System.out.println(elemento);
		}
		System.out.println("\nIncluimos un elemento en la posicion 1");
		lista.add(1, "Elemento insertado");
		for (String elemento : lista) {
			System.out.println(elemento);
		}
		System.out.println("\nDevolvemos el numero de elementos del ArrayList.");
		System.out.println(lista.size());
		System.out.println("\nDevolvemos el elemento en la posicion 2 del ArrayList.");
		System.out.println(lista.get(2));
		System.out.println("\nComprobamos si existe el elemento 1 del ArrayList.");
		System.out.println(lista.contains("Elemento1"));
		System.out.println("\nDevolvemos la posicion de la primera ocurrencia de un elemento");
		System.out.println(lista.indexOf("Elemento5"));
		System.out.println("\nDevolvemos la ultima ocurrencia de \"Elemento1\"");
		System.out.println(lista.lastIndexOf("Elemento1"));
		System.out.println("\nBorramos el elemento de la posicion 5.");
		lista.remove(4);
		for (String elemento : lista) {
			System.out.println(elemento);
		}
		System.out.println("\nBorramos la primera ocurrencia de \"Elemento2\"");
		lista.remove("Elemento2");
		for (String elemento : lista) {
			System.out.println(elemento);
		}
		System.out.println("\nCopiamos el ArrayList");
		ArrayList<String> arrayListCopia = (ArrayList<String>) lista.clone();
		for (String elemento : arrayListCopia) {
			System.out.println(elemento);
		}
		System.out.println("\nConvertimos el ArrayList en un array.");
		String[] array = new String[lista.size()];
		array = lista.toArray(array);
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.println("\nBorramos todos los elementos del ArrayList");
		lista.clear();
		for (String elemento : lista) {
			System.out.println(elemento);
		}
		System.out.println("\nComprobamos si el ArrayList esta vacio.");
		System.out.println(lista.isEmpty());
	}

}
