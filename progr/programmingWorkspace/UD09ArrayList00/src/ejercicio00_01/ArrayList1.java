package ejercicio00_01;

import java.util.ArrayList;

public class ArrayList1 {

	public static void main(String[] args) {
		System.out.println("Creamos un ArrayList");
		ArrayList<String> nombres = new ArrayList<String>();
		System.out.println("Le introducimos datos");
		nombres.add("Ana");
		nombres.add("Luisa");
		nombres.add("Felipe");
		System.out.println("Mostramos");
		System.out.println(nombres);
		System.out.println("Uno mas");
		nombres.add(1, "Pablo");
		System.out.println(nombres);
		System.out.println("Borramos el 0");
		nombres.remove(0);
		System.out.println(nombres);
		System.out.println("Ponemos en la posicion 0 Alfonso");
		nombres.set(0, "Alfonso");
		System.out.println(nombres);
		System.out.println("Mostramos la parte final del ArrayList");
		String s = nombres.get(1);
		String ultimo = nombres.get(nombres.size() - 1);
		System.out.println(s + " " + ultimo);
	}

}