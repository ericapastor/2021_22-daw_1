package ejercicio00_01;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayList2 {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// ArrayList -> objetos
		// clases envoltorio -> wrappers -> Integer, Double, Float, ...
		ArrayList<Integer> numeros = new ArrayList<Integer>();
		int n;
		do {
			System.out.println("Introduce numeros enteros, 0 para acabar");
			System.out.println("Numero");
			n = input.nextInt();
			if (n != 0) {
				numeros.add(n);
			}
		} while (n != 0);
		System.out.println("Has introducido " + numeros.size() + " numeros");
		System.out.println("Mostramos el ArrayList");
		System.out.println(numeros);
		System.out.println("Recorrido con foreach para sumar los elementos");
		double suma = 0;
		for (Integer i : numeros) {
			suma = suma + i;
		}
		System.out.println("La suma es " + suma);
		System.out.println("La media es " + suma / numeros.size());
		input.close();

	}

}
