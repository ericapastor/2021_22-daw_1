package ejercicio00_02;

public class Persona {
	private int id;
	private String nombre;
	private double altura;
	public Persona(int id, String nombre, double altura) {
		this.id = id;
		this.nombre = nombre;
		this.altura = altura;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	@Override
	public String toString() {
		return "Persona [id=" + id + ", nombre=" + nombre + ", altura=" + altura + "]";
	}
	
}
