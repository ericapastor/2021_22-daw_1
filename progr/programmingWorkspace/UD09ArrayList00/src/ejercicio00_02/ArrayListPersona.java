package ejercicio00_02;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListPersona {

	public static void main(String[] args) {
		// creamos objetos y atributos
		String nombre;
		double altura;
		Scanner leer = new Scanner(System.in);
		// ArrayList de personas
		ArrayList<Persona> personitas = new ArrayList<Persona>();
		System.out.println("Pedimos datos:");
		System.out.println("\nDime el numero de personas a introducir.");
		int n = leer.nextInt();
		for (int i = 1; i <= n; i++) {
			System.out.println("Dame el nombre");
			nombre = leer.next();
			System.out.println("Dame la altura");
			altura = leer.nextDouble();
			// para crear las personas al ArrayList,
			// tengo que crear personas, con un constructor
			personitas.add(new Persona(i, nombre, altura));
		}
		System.out.println("\nMuestro el ArrayList");
		System.out.println(personitas);
		System.out.println("\nCalculo la media de la altura");
		double suma = 0;
		for (Persona i : personitas) {
			suma = suma + i.getAltura();
		}
		System.out.println("Suma "+suma);
		System.out.println("Media "+(suma/personitas.size()));
		leer.close();
	}

}
