package ejercicio0304;

import java.util.ArrayList;
import java.util.Iterator;

public class Ej03y04 {

	public static void main(String[] args) {
		/*
		 * Crear un arrayList de elementos tipo Integer con n�meros enteros generados
		 * aleatoriamente a partir de Math.random(). Los n�meros aleatorios debes estar
		 * entre 1 y 20.
		 */
		ArrayList<Integer> lista = new ArrayList<Integer>();
		System.out.println("Relleno y muestro:");
		rellenarLista(lista);
		mostrarArray(lista);
		System.out.println("Borro y muestro:");
		borrarConIterator(lista);
		mostrarArray(lista);
	}

	static void rellenarLista(ArrayList<Integer> lista) {
		for (int i = 0; i < 20; i++) {
			lista.add((int) Math.floor(Math.random() * 19) + 1);
		}
	}

	static void mostrarArray(ArrayList<Integer> lista) {
		if (lista.isEmpty() == false) {
			for (int n : lista) {
				System.out.print(n+" ");
			}
		}
	}

	static void borrarNumeros10y15(ArrayList<Integer> lista) {
		for (int i = lista.size() - 1; i >= 0; i--) {
			if (lista.get(i) > 9 && lista.get(i) < 16) {
				lista.remove(i);
			}
		}
	}

	static void borrarConWhile(ArrayList<Integer> lista) {
		int i = 0;
		while (i < lista.size()) {
			if (lista.get(i) >= 10 && lista.get(i) <= 15) {
				lista.remove(i);
			} else {
				i++; // se pone en el else porque si borra, salta una linea de todas maneras
			}
		}
	}

	// borrar con iterador es preferente porque da mas control sobre el ArrayList
	static void borrarConIterator(ArrayList<Integer> lista) {
		Iterator<Integer> iterador = lista.iterator();
		while (iterador.hasNext()) {
			Integer numero = iterador.next();
			if (numero >= 10 && numero <= 15) {
				iterador.remove();
			}
		}
	}

}
