package ejercicio05;

import java.util.ArrayList;
import java.util.Iterator;

public class Ejercicio05 {

	/*
	 * Crear un ArrayList de Strings, y a�adir varias cadenas directamente para
	 * probar el siguiente m�todo. M�todo est�tico borrarCadenas(String cadena,
	 * ArrayList lista). Debo borrar del arraylist toda aparici�n de la cadena
	 * recibida como par�metro. Revisar la incidencia de las distintas formas de
	 * iterar la lista a la hora de borrar elementos.
	 */

	public static void main(String[] args) {
		ArrayList<String> listae = new ArrayList<String>();
		System.out.println("Primeroooooooooooooo");
		rellenar(listae);
		mostrar(listae);
		System.out.println("\nSegundoooooooooooooo");
		borrar(listae);
		mostrar(listae);
	}

	static void rellenar(ArrayList<String> listae) {
		listae.add("Hola");
		listae.add("Hey");
		listae.add("Sioooo");
		listae.add("SOL");
		listae.add("aham");
		listae.add("qwerty");
		listae.add("CVNsdfkj");
		listae.add("KAdmakfAWDQqed");
		listae.add("Qwerty");
	}

	static void mostrar(ArrayList<String> listae) {
		for (String elem : listae) {
			System.out.println(elem);
		}
	}

	static void borrar(ArrayList<String> listae) {
		Iterator<String> it = listae.iterator();
		while (it.hasNext()) {
			if (it.next().contains("H")) {
				it.remove();
			}
		}
	}

}
