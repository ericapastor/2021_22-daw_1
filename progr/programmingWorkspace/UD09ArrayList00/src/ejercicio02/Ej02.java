package ejercicio02;

import java.util.ArrayList;
import java.util.Scanner;

public class Ej02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		/*
		 * Crear un paquete ejercicio2 con una clase Ejercicio2 que tenga m�todo main.
		 * En �l creamos un ArrayList de Strings. Por consola iremos indicando que se
		 * introduzca un string y se almacenar� en el arraylist. Se pedir�n Strings
		 * hasta introducir la cadena �fin�. Posteriormente se mostrar�n todos los
		 * Strings del array list. Crearemos un m�todo est�tico borrarCadena(String
		 * cadena, ArrayList lista) que borra dicha cadena del arraylist. Si la cadena
		 * no existe no se podr� borrar nada. Llamaremos a ese m�todo para borrar una
		 * cadena. Finalmente volvemos a mostrar el arraylist.
		 * 
		 */
		// creo arraylist
		ArrayList<String> lista = new ArrayList<String>();
		// voy pidiendo datos
		String dato = "";
		while (!dato.equalsIgnoreCase("fin")) {
			System.out.println("Introduce un dato. Para terminar escribe \"FIN\"");
			dato = input.nextLine();
			if (!dato.equalsIgnoreCase("fin")) {
				lista.add(dato);
			} else {
				System.out.println("Hemos terminado de rellenar datos.");
			}
		}
		// muestro los datos
		System.out.println("\nEsto es lo que hay hasta ahora:");
		listar(lista);
		// al terminar pido que dato se quiere borrar, lo borro, y vuelvo a mostrar
		System.out.println("\nQue quieres borrar?");
		dato = input.nextLine();
		if (!dato.equals("") && !dato.equals("fin")) {
			borrarCadena(dato, lista);
		} else {
			System.out.println("No es valido");
		}
		System.out.println("La lista actual:");
		listar(lista);
		input.close();
	}

	static void listar(ArrayList<String> lista) {
		for (String elemento : lista) {
			System.out.println(elemento);
		}
	}

	static void borrarCadena(String cadena, ArrayList<String> lista) {
		for (String elemento : lista) {
			if (elemento.equalsIgnoreCase(cadena)) {
				lista.remove(cadena);
				break;
			}
		}
	}

}
