package ejercicio01;

import java.util.ArrayList;
import java.util.Scanner;

public class Ej01 {

	/*
	 * Crear un paquete ejercicio1 con una clase Ejercicio1 con m�todo main.
	 * Crearemos un arrayList de Strings. Pediremos por teclado la cantidad de
	 * Strings que nos solicitar� e ira pidi�ndonos cada String y a�adi�ndolo al
	 * arraylist. Despu�s pediremos la posici�n del elemento a borrar, y lo
	 * borramos. Volvemos a mostrar el contenido del arraylist.
	 */

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// creo arraylist
		ArrayList<String> cadena = new ArrayList<String>();
		// pido la largura
		System.out.println("Dime cuantos datos quieres introducir");
		int n = input.nextInt();
		// relleno el arraylist
		for (int i = 0; i < n; i++) {
			System.out.println("Dato " + i);
			cadena.add(input.next());
		}
		// muestro
		System.out.println("\nEste es el ArraList relleno:");
		for (String e : cadena) {
			System.out.println(e);
		}
		// pido el elemento a borrar
		System.out.println("\nQue elemento quieres borrar");
		n = input.nextInt();
		// lo borro y muestro de nuevo
		if (n < cadena.size() && n > -1) {
			cadena.remove(n);
			System.out.println("\nEste es el ArrayList, de nuevo:");
			for (String e : cadena) {
				System.out.println(e);
			}
		} else {
			System.out.println("Has introducido una posicion superior o inferior de los elementos existentes.");
		}
		input.close();
	}

}
