package clases;

import java.time.LocalDate;
import java.time.LocalTime;

import programa.Programa;

public class Cliente {

	// variables
	private static int centenasCliente = 0;
	private static int decenasCliente = 0;

	// atributos
	private String nombre;
	private String apellido;
	private String dni;
	private String codigo;
	private LocalDate fecha;
	private LocalTime hora;
	private Signo signo;

	// constructores
	public Cliente() {
		this.signo = new Signo();
	}

	public Cliente(String nombre, String apellido, String dni) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.signo = new Signo();
	}

	// getters & setters
	public int getCentenasCliente() {
		return centenasCliente;
	}

	// el siguiente setter metodo recibe un numero
	// si el numero es positivo lo sumara a las centenas, si las centenas valian 9
	// pondra las centanas a 0 y sumara el numero a las decenas
	// si el numero es negativo hara la contrario
	public void setCentenasCliente(int n) {
		if (centenasCliente + n > 9 && decenasCliente + n > 9) {
			centenasCliente = 0;
			decenasCliente = 0;
		} else if (centenasCliente + n < 0) {
			centenasCliente = 9;
			decenasCliente--;
		} else if (centenasCliente + n > 0) {
			centenasCliente += n;
		} else if (centenasCliente + n > 9) {
			centenasCliente = 0;
			decenasCliente++;
		}
	}

	public int getDecenasCliente() {
		return decenasCliente;
	}

	public void setDecenasCliente(int n) {
		centenasCliente = n;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = LocalDate.parse(fecha);
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = LocalTime.parse(hora);
	}

	public Signo getSigno() {
		return signo;
	}

	public void setSigno(Signo signo) {
		this.signo = signo;
	}

	// to String
	@Override
	public String toString() {
		return "##  cliente: " + apellido + ", " + nombre + "\n##  DNI: " + dni + "\n##  codigo de cliente: " + "\""
				+ codigo + "\"";
	}

	/**
	 * 
	 * a continuacion entraran CINCO metodos que piden un dato, lo comprueban y/o lo
	 * cambian:
	 * 
	 * pedirNombre y pedirApellidos comprueban que una sola palabra haya sido
	 * introducida y convierten el nombre a un caso UpLowCase (Nombre Apellidos)
	 * 
	 * pedirDNI comprueba que el DNI introducido sea correcto, si es asi calcula su
	 * letra, y si se introduce 0 se cancela la accion
	 * 
	 * Con estos tres datos se crea un objeto cliente
	 * 
	 * pedirFecha comprueba que la fecha este en formato correcto para un parseo
	 * posterior en el setter, y si se introduce 0 se cancela la accion
	 * 
	 * pedirHora comprueba que la hora este en formato correcto para un parseo
	 * posterior en el setter, y si se introduce 0 se cancela la accion
	 * 
	 * si la fecha y/u hora estuvieran vacias, no se incluiran en el objeto del
	 * array Cliente[]cliente, pudiendo rellenarlas mas adelante en el metodo
	 * modificar datos de cliente
	 * 
	 * en cualquier momento se puede cancelar el alta de un cliente introduciendo 0,
	 * en cualquiera de las cinco fases
	 * 
	 */

	String pedirNombre() {
		boolean correcto = false;
		String[] nombres;
		System.out.println("##\n##  introduce un nombre");
		do {
			String nombre = Programa.input.nextLine();
			if (!nombre.equals("")) {
				nombre = nombre.trim();
				nombres = nombre.split(" ");
				nombre = "";
				for (int i = 0; i < nombres.length; i++) {
					nombres[i] = nombres[i].trim();
					nombre = nombre + nombres[i].toUpperCase().charAt(0) + nombres[i].toLowerCase().substring(1) + " ";
				}
				nombre = nombre.trim();
				return nombre;
			} else {
				System.out.println("##  no has escrito nada, vuelve a intentarlo");
			}
		} while (correcto == false);
		return "0";
	}

	String pedirApellido() {
		boolean correcto = false;
		String[] apellidos;
		System.out.println("##\n##  introduce un apellido");
		do {
			String apellido = Programa.input.nextLine();
			if (!apellido.equals("")) {
				apellido = apellido.trim();
				apellidos = apellido.split(" ");
				apellido = "";
				for (int i = 0; i < apellidos.length; i++) {
					apellido = apellido + apellidos[i].toUpperCase().charAt(0) + apellidos[i].toLowerCase().substring(1)
							+ " ";
				}
				apellido = apellido.trim();
				return apellido;
			} else {
				System.out.println("##  no has escrito nada, vuelve a intentarlo");
			}
		} while (correcto == false);
		return "0";
	}

	String pedirDNI() {
		boolean dniCorrecto = false;
		System.out.println("##\n##  introduce el DNI, sin letra");
		do {
			String dni = Programa.input.nextLine();
			if (!dni.equals("")) {
				if (dni.equals("0")) {
					return dni;
				} else {
					dniCorrecto = Calculos.comprobarDNI(dni);
					if (dniCorrecto == true) {
						return Calculos.calcularLetraDNI(dni);
					}
				}
			} else {
				System.out.println("##  no has escrito nada, vuelve a intentarlo");
			}
		} while (dniCorrecto == false);
		return "0";

	}

	String pedirFecha() {
		boolean correcto = false;
		System.out.println("##\n##  introduce la fecha de nacimiento,");
		System.out.println("##  si quieres rellenar tus datos de nacimiento");
		System.out.println("##  en este momento, escribe \"no\"");
		do {
			String fecha = Programa.input.nextLine();
			if (!fecha.equals("")) {
				if (fecha.equals("0") || fecha.equalsIgnoreCase("no")) {
					return fecha;
				} else {
					correcto = Calculos.comprobarFecha(fecha);
					if (correcto == true) {
						return fecha;
					}
				}
			} else {
				System.out.println("##  no has escrito nada, vuelve a intentarlo");
			}
		} while (correcto == false);
		return "0";
	}

	String pedirHora() {
		boolean correcto = false;
		System.out.println("##\n##  introduce la hora de nacimiento, ");
		System.out.println("##  si no la sabes escribe \"no\"");
		do {
			String hora = Programa.input.nextLine();
			if (!hora.equals("")) {
				if (hora.equals("0") || hora.equalsIgnoreCase("no")) {
					return hora;
				} else {
					correcto = Calculos.comprobarHora(hora);
					if (correcto == true) {
						return Calculos.comprobarFranjaHoraria(hora);
					}
				}
			} else {
				System.out.println("##  no has escrito nada, vuelve a intentarlo");
			}
		} while (correcto == false);
		return "0";
	}

	/**
	 * pedirDatos usa los cinco metodos anteriores y tras una serie de
	 * comprobaciones rellenara o el objeto desde el que se lo llame
	 */

	void pedirDatos() {
		String nombre;
		String apellido;
		String dni;
		String fecha;
		String hora;
		String opcion = "n";
		// boolean que comprueban que no se ha cancelado el alta de cliente, lo
		// inicializo tras cada vez que rellene un solo dato basico a causa del break
		// (si no lo hago se queda con el valor true o false anterior)
		boolean datosBasicosRellenos = false;
		/*
		 * a continuacion utilizo tres do{}while():
		 * 
		 * el PRIMERO (el de afuera) me permitira volver a introducir los datos desde el
		 * principio si no estoy conforme tras haberlos escrito, interrumpiendose solo
		 * si estoy conforme, o si he cancelado la operacion a mitad de rellenar los
		 * datos basicos con la tecla 0
		 * 
		 * el SEGUNDO me sirve simplemente para poder parar de pedir datos si se
		 * introduce la tecla 0. No tiene mas funcionalidad.
		 * 
		 * el TERCERO se ocupara de asegurarse que a la hora de escribir [s/n]
		 * (Conforme/No conforme) no se introduzca por teclado cualquier otro caracter o
		 * cadena de texto que no sea 's' o 'n'
		 * 
		 */
		// primer do{}while()
		do {
			// por si vuelvo a pedir los datos mas tarde, empiezo vaciandolos
			nombre = "";
			apellido = "";
			dni = "";
			fecha = "";
			hora = "";
			datosBasicosRellenos = !nombre.equals("") && !apellido.equals("") && !dni.equals("");
			// segundo do{}while()
			do {
				opcion = "n";
				// introducir nombre
				nombre = pedirNombre();
				if (nombre.equals("0")) {
					nombre = "";
					break;
				} else {
					this.setNombre(nombre);
				}
				// introducir apellido
				apellido = pedirApellido();
				if (apellido.equals("0")) {
					nombre = "";
					break;
				} else {
					this.setApellido(apellido);
				}
				// introducir dni, cancelo pedir datos
				dni = pedirDNI();
				if (dni.equals("0")) {
					nombre = "";
					break;
				} else {
					this.setDni(dni);
				}
				datosBasicosRellenos = !nombre.equals("") && !apellido.equals("") && !dni.equals("");
				// introducir fecha de nacimiento
				fecha = pedirFecha();
				if (fecha.equals("0")) {
					nombre = "";
					break;
				} else if (fecha.equals("no")) {
					fecha = "";
					break;
				} else {
					this.setFecha(fecha);
				}
				// introducir hora de nacimiento
				hora = pedirHora();
				if (hora.equals("0")) {
					nombre = "";
					break;
				} else if (hora.equals("no")) {
					hora = "";
					break;
				} else {
					this.setHora(hora);
				}
			} while (datosBasicosRellenos == false);
			// compruebo la conformidad de los datos para crear un objeto, pero solo si se
			// han introducido los datos basicos
			datosBasicosRellenos = !nombre.equals("") && !apellido.equals("") && !dni.equals("");
			if (datosBasicosRellenos == true) {
				// mostrar los datos
				opcion = pedirValidacionDatos(nombre, apellido, dni, fecha, hora, opcion);
			} else {
				Pintar.retrocediendo();
				break;
			}
		} while (opcion.equalsIgnoreCase("n") && !opcion.equalsIgnoreCase("s"));
		// borro el nombre para borrar posteriormente el objeto en caso de que no se
		// este de acuerdo con los datos
		if (opcion.equalsIgnoreCase("n")) {
			this.setNombre("");
		} else if (opcion.equalsIgnoreCase("s")) {
			// si el objeto va a ser creado genero codigo de cliente
			this.generarCodigoCliente();
		}
	}

	/**
	 * pedirValidacionesDatos
	 * 
	 * @param nombre
	 * @param apellido
	 * @param dni
	 * @param fecha
	 * @param hora
	 * 
	 *                 este metodo me sirve para dar visibilidad de los datos en el
	 *                 metodo anterior y despues pedir confirmacion por parte del
	 *                 que los ha rellenado
	 */
	String pedirValidacionDatos(String nombre, String apellido, String dni, String fecha, String hora, String opcion) {
		if (!nombre.equals("") || !apellido.equals("") || !dni.equals("") || !fecha.equals("") || !hora.equals("")) {
			System.out.println("##\n##  estos son los datos introducidos:");
			if (!nombre.equals("")) {
				System.out.println("##  nombre: " + nombre);
			}
			if (!apellido.equals("")) {
				System.out.println("##  apellido: " + apellido);
			}
			if (!dni.equals("")) {
				System.out.println("##  DNI: " + dni);
			}
			if (!fecha.equals("")) {
				System.out.println("##  fecha de nacimiento: " + fecha);
			}
			if (!hora.equals("")) {
				System.out.println("##  hora de nacimiento: " + hora);
			}
			System.out.println("##  diga si esta: conforme / no conforme [s/n]");
			// tercer do{}while(), pido la respuesta [s/n]
			do {
				opcion = Programa.input.nextLine();
				switch (opcion) {
				case "":
					System.out.println("##  no has escrito nada, vuelve a intentarlo");
					break;
				case "s":
				case "S":
					return "s";
				case "n":
				case "N":
					System.out.print("##\n##  volvamos a empezar: ");
					return "n";
				default:
					System.out.println("##\n##  opcion no contemplada");
					System.out.println("##  escribe [s/n] (conforme / no conforme)");
					break;
				}
			} while (!opcion.equalsIgnoreCase("n") && !opcion.equalsIgnoreCase("s"));
		} else {
			System.out.println("##  ningun dato ha sido introducido");
		}
		return "-1";
	}

	void mostrarCliente(Cliente cliente) {
		System.out.println(cliente);
		this.mostrarFechaHoraSignos();
		System.out.println("##\n##\n##  *********************************************\n##");
	}

	/**
	 * 
	 * @param signo
	 */
	private void caracteristicasSolar(Signo signo) {
		for (int i = 0; i < 12; i++) {
			if (signo.getSolar().equalsIgnoreCase(signo.getSIGNO()[i])) {
				System.out.println("##                                                 ##");
				System.out.println("##  tu signo solar es: " + signo.getSolar());
				System.out.println("##  elemento: " + signo.getELEMENTO()[i]);
				System.out.println("##  modalidad: " + signo.getMODALIDAD()[i]);
				System.out.println("##  planeta que lo rige: " + signo.getPLANETAREGENTE()[i]);
				System.out.println("##                                                 ##");
				System.out.println("##  interpretacion:                                ##");
				System.out.println("##                                                 ##");
				System.out.println(signo.getINTERPRETACIONSOLAR()[i]);
				System.out.println("##                                                 ##");
				break;
			}
		}
	}

	/**
	 * 
	 * @param signo
	 */
	private void caracteristicasLunar(Signo signo) {
		for (int i = 0; i < 12; i++) {
			if (signo.getLunar().equalsIgnoreCase(signo.getSIGNO()[i])) {
				System.out.println("##                                                 ##");
				System.out.println("##  tu signo lunar es: " + signo.getLunar());
				System.out.println("##  elemento: " + signo.getELEMENTO()[i]);
				System.out.println("##  modalidad: " + signo.getMODALIDAD()[i]);
				System.out.println("##  planeta que lo rige: " + signo.getPLANETAREGENTE()[i]);
				System.out.println("##                                                 ##");
				System.out.println("##  interpretacion:                                ##");
				System.out.println(signo.getINTERPRETACIONLUNAR()[i]);
				System.out.println("##                                                 ##");
				break;
			}
		}
	}

	/**
	 * 
	 * @param cliente
	 */
	void mostrarCartaAstralCompleta(Cliente cliente) {
		System.out.println("##  fecha de nacimiento: " + cliente.getFecha());
		System.out.println("##  hora de nacimiento: " + cliente.getHora());
		System.out.println("##                                                 ##");
		System.out.println("##  esta es tu carta astral en detalle:            ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		this.caracteristicasSolar(cliente.getSigno());
		this.caracteristicasLunar(cliente.getSigno());

	}

	/**
	 * generarCodigoCliente generara un codigo de cliente
	 */
	void generarCodigoCliente() {
		this.setCodigo("CLI-" + this.getDecenasCliente() + this.getCentenasCliente());
		this.setCentenasCliente(1);
	}

	/**
	 * mostrarFechaHoraSignos es un metodo que llamare en listarClientes tras llamar
	 * al toString
	 * 
	 * 
	 */

	void mostrarFechaHoraSignos() {
		if (this.getSigno().getSolar() != null && this.getSigno().getAscendente() == null) {
			System.out.println("##  fecha de nacimiento: " + this.getFecha());
			System.out.println("##  carta astral:");
			System.out.println("##  - signo solar: " + signo.getSolar());
			System.out.println("##  - signo lunar: " + signo.getLunar());
		} else if (this.getSigno().getSolar() != null && this.getSigno().getAscendente() != null) {
			System.out.println("##  fecha de nacimiento: " + this.getFecha());
			System.out.println("##  hora de nacimiento: " + this.getHora());
			System.out.println("##  carta astral:");
			System.out.println("##  - signo solar: " + signo.getSolar());
			System.out.println("##  - signo lunar: " + signo.getLunar());
			System.out.println("##  - signo ascendente: " + signo.getAscendente());
		}

	}

	/**
	 * calcularCartaAstral calcula la carta astral de un cliente, en base a si
	 * tenemos sus datos de nacimiento, llamando a los respectivos metodos de la
	 * clase signo y rellenando el objeto signo de esta misma clase cliente
	 */
	void calcularCartaAstral() {
		if (this.getFecha() != null) {
			this.signo.calcularSolar(this.getFecha());
			this.signo.calcularLunar(this.getFecha());
			if (this.getHora() != null) {
				this.signo.calcularAscendente(this.getHora());

			}
		}
	}

	void modificarNombreApellidosCliente(Cliente cliente) {
		boolean datosRellenos = false;
		String nombre = "";
		String apellido = "";
		String opcion = "n";
		System.out.println("##  vamos con los  nuevos datos");
		do {
			nombre = "";
			apellido = "";
			opcion = "n";
			// introducir nombre
			nombre = this.pedirNombre();
			if (nombre.equals("0")) {
				nombre = "";
				break;
			}
			// introducir apellido
			apellido = this.pedirApellido();
			if (apellido.equals("0")) {
				apellido = "";
				break;
			}
			datosRellenos = !nombre.equals("") && !apellido.equals("");
			if (datosRellenos) {
				opcion = this.pedirValidacionDatos(nombre, apellido, "", "", "", opcion);
				if (opcion.equalsIgnoreCase("s")) {
					cliente.setNombre(nombre);
					cliente.setApellido(apellido);
				}
			}
		} while (opcion.equalsIgnoreCase("n"));
	}

	void modificarDNICliente(Cliente cliente) {
		boolean datosRellenos = false;
		String dni = "";
		String opcion = "n";
		System.out.println("##  vamos con los  nuevos datos");
		do {
			dni = "";
			opcion = "n";
			// introducir nombre
			dni = this.pedirDNI();
			if (dni.equals("0")) {
				dni = "";
				break;
			}
			datosRellenos = !dni.equals("");
			if (datosRellenos) {
				opcion = this.pedirValidacionDatos("", "", dni, "", "", opcion);
				if (opcion.equalsIgnoreCase("s")) {
					cliente.setDni(dni);
				}
			}
		} while (opcion.equalsIgnoreCase("n"));
	}

	void modificarFechaCliente(Cliente cliente) {
		boolean datosRellenos = false;
		String fecha = "";
		String opcion = "n";
		System.out.println("##  vamos con los nuevos datos");
		do {
			fecha = "";
			opcion = "n";
			fecha = this.pedirFecha();
			if (fecha.equals("0")) {
				fecha = "";
				break;
			}
			datosRellenos = !fecha.equals("");
			if (datosRellenos) {
				opcion = this.pedirValidacionDatos("", "", "", fecha, "", opcion);
				if (opcion.equalsIgnoreCase("s")) {
					cliente.setFecha(fecha);
					cliente.calcularCartaAstral();
				}
			}
		} while (opcion == "n");
	}

	void modificarHoraCliente(Cliente cliente) {
		boolean datosRellenos = false;
		String hora = "";
		String opcion = "n";
		System.out.println("##  vamos con los nuevos datos");
		do {
			hora = "";
			opcion = "n";
			hora = this.pedirHora();
			if (hora.equals("0")) {
				hora = "";
				break;
			}
			datosRellenos = !hora.equals("");
			if (datosRellenos) {
				opcion = this.pedirValidacionDatos("", "", "", hora, "", opcion);
				if (opcion.equalsIgnoreCase("s")) {
					cliente.setHora(hora);
					cliente.calcularCartaAstral();
				}
			}
		} while (opcion == "n");
	}

}