package clases;

public class Pintar {
	/**
	 * Pintar inicio
	 */

	public static void inicio() {
		System.out.println("\n\n");
		System.out.println("                          *                       *  ");
		System.out.println("        *                        *             *     ");
		System.out.println("                 *                      *            ");
		System.out.println("           *                               *         ");
		System.out.println("                    *                             *  ");
		System.out.println("  *       *                             *            ");
		System.out.println("                       #  #  #                       ");
		System.out.println("                   #  # #  #     #             *    *");
		System.out.println("    *           #  #  #  #         #                 ");
		System.out.println("              #  #  #  #             #               ");
		System.out.println("             #  #  #  #               #              ");
		System.out.println("            #  #  #  #   bienvenido    #            *");
		System.out.println("            #  #  #  #   a starset*    #             ");
		System.out.println(" *          #  #  #  #                 #      *      ");
		System.out.println("             #  #  #  #               #          *   ");
		System.out.println("              #  #  #  #             #       *       ");
		System.out.println("                #  #  #  #         #                 ");
		System.out.println("        *          #  # #  #     #             *     ");
		System.out.println("     *                 #  #  #           *        *  ");
		System.out.println("                                                     ");
		System.out.println("     *      *                          *             ");
		System.out.println("         *        *                  *      *    **  ");
		System.out.println("                     *        **                *    ");
		System.out.println("       *                             *             * ");

		// explico regla de navegacion
		regla0();
	}

	static void regla0() {
		System.out.println("\n\n#####################################################");
		System.out.println("##        *                               *      * ##");
		System.out.println("##          *         IMPORTANTE:            *     ##");
		System.out.println("##  **                                         *   ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
		System.out.println("##  si en cualquier momento introduces el numero   ##");
		System.out.println("##  0, el programa te llevara a la opcion          ##");
		System.out.println("##  anterior, cancelara lo que estes haciendo, o,  ##");
		System.out.println("##  en su defecto, si estas en el menu de inicio   ##");
		System.out.println("##  de sesion, terminara el programa               ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
	}

	/**
	 * Pintar despedida
	 */
	public static void despedida() {
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
		System.out.println("#####################################################\n\n");
		System.out.println("                          *                       *  ");
		System.out.println("        *                        *             *     ");
		System.out.println("                 *                      *            ");
		System.out.println("           *                               *         ");
		System.out.println("                    *                             *  ");
		System.out.println("  *       *                             *            ");
		System.out.println("                       #  #  #                       ");
		System.out.println("                   #  # #  #     #             *    *");
		System.out.println("    *           #  #  #  #         #                 ");
		System.out.println("              #  #  #  #             #               ");
		System.out.println("             #  #  #  #               #              ");
		System.out.println("            #  #  #  #    gracias*     #            *");
		System.out.println("            #  #  #  #    hasta        #             ");
		System.out.println(" *          #  #  #  #    pronto       #      *      ");
		System.out.println("             #  #  #  #               #          *   ");
		System.out.println("              #  #  #  #             #       *       ");
		System.out.println("                #  #  #  #         #                 ");
		System.out.println("        *          #  # #  #     #             *     ");
		System.out.println("     *                 #  #  #           *        *  ");
		System.out.println("                                                     ");
		System.out.println("     *      *                          *             ");
		System.out.println("         *        *                  *      *    **  ");
		System.out.println("                     *        **                *    ");
		System.out.println("       *                             *             * ");
	}

	/**
	 * Mostrar menu de loggeo
	 */
	public static void loggin() {
		System.out.println("## *            *                   *              ##");
		System.out.println("##      *             *                       *    ##");
		System.out.println("##       *         INICIAR SESION            *     ##");
		System.out.println("## *                          **                 * ##");
		System.out.println("## *            *                   *              ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                     *      *    ##");
		System.out.println("##                                      *          ##");
		System.out.println("##    1 - iniciar sesion como invitado      *      ##");
		System.out.println("##    2 - iniciar sesion como administrador        ##");
		System.out.println("##                                                 ##");
		System.out.println("##   *  *                                          ##");
		System.out.println("##  *                                              ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##         *                     *            *    ##");
		System.out.println("## *            *                   *              ##");
	}

	/**
	 * Mostrar menu usuario
	 */
	public static void menuInvitado() {
		System.out.println("##                                                 ##");
		System.out.println("## *                          **                 * ##");
		System.out.println("##       *                                 *       ##");
		System.out.println("##     *           MENU DE INVITADO            *   ##");
		System.out.println("##         **                                 *    ##");
		System.out.println("## *            *                   *              ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
		System.out.println("##  elige una opcion:                              ##");
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
		System.out.println("##  1 - darte de alta como cliente                 ##");
		System.out.println("##  2 - ver tu carta astral                        ##");
		System.out.println("##  3 - modificar tus datos                        ##");
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
	}

	/**
	 * Mostrar menu administrador
	 */
	public static void menuAdministrador() {
		System.out.println("##                                                 ##");
		System.out.println("## *                          **                 * ##");
		System.out.println("##       *                                 *       ##");
		System.out.println("##     *        MENU DE ADMINISTRADOR          *   ##");
		System.out.println("##         **                                 *    ##");
		System.out.println("## *            *                   *              ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
		System.out.println("##  elige una opcion:                              ##");
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
		System.out.println("##  1 - dar de alta una o varios clientes          ##");
		System.out.println("##  2 - lista de clientes                          ##");
		System.out.println("##  3 - buscar un cliente en especifico            ##");
		System.out.println("##  4 - modificar datos de un cliente              ##");
		System.out.println("##  5 - eliminar un cliente                        ##");
		System.out.println("##  6 - listar clientes segun su signo             ##");
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
	}

	/**
	 * Mostrar mensaje de retrocediendo
	 */
	public static void retrocediendo() {
		System.out.println("##                                          *      ##");
		System.out.println("##       *                                    *    ##");
		System.out.println("##   *                                  *          ##");
		System.out.println("##                 retrocediendo...                ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
	}

	static void tramandoEstrellas() {
		System.out.println("##    *     *          *           **         *    ##");
		System.out.println("##        *                    *         *   *     ##");
		System.out.println("##                                             *   ##");
		System.out.println("##   *         un momento, por favor,              ##");
		System.out.println("##      *   estamos tramando estrellas...   *      ##");
		System.out.println("##    *        *                                   ##");
		System.out.println("##  *                                   *   *      ##");
		System.out.println("##   *         *           *        **         *   ##");
		System.out.println("##        **                                  *    ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
	}

	static void todosLosClientes() {
		System.out.println("##                                         * *     ##");
		System.out.println("##     **            *          *                  ##");
		System.out.println("##                             *   *        *      ##");
		System.out.println("##          *                                      ##");
		System.out.println("##   *       estos son todos los clientes *        ##");
		System.out.println("##             almacenados hasta ahora      *      ##");
		System.out.println("##      *                        *                 ##");
		System.out.println("##           *           *                *        ##");
		System.out.println("##         *                                       ##");
	}

	static void ningunDato() {
		System.out.println("##     ***************************************     ##");
		System.out.println("##                                                 ##");
		System.out.println("##     vaya, parece que aun no has introducido     ##");
		System.out.println("##              ningun dato todavia                ##");
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
	}

	static void ningunCliente() {
		System.out.println("##     ***************************************     ##");
		System.out.println("##                                                 ##");
		System.out.println("##         oops! parece que el codigo/dni          ##");
		System.out.println("##   introducido no corresponde a ningun cliente   ##");
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
	}

	static void algoHaIdoMal() {
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
		System.out.println("##              vaya, algo ha ido mal              ##");
		System.out.println("##                                                 ##");
		System.out.println("##                    :D                           ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
	}

	static void dniYaExiste() {
		System.out.println("##             *");
		System.out.println("##  *");
		System.out.println("##  vaya, parece que el dni introducido ya existe");
		System.out.println("##  en nuestra base de datos, intentalo de nuevo:");
		System.out.println("##             *");
		System.out.println("##  *              *  *");
	}

	static void opcionClienteCodigo() {
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
		System.out.println("##  quieres buscar el cliente por:                 ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
		System.out.println("##  1 - codigo de cliente                          ##");
		System.out.println("##  2 - DNI                                        ##");
	}

	static void opcionModificarClienteAdmin() {
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
		System.out.println("##  quieres modificar:                             ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
		System.out.println("##  1 - nombre y apellidos del cliente             ##");
		System.out.println("##  2 - DNI del cliente                            ##");
		System.out.println("##  3 - fecha de nacimiento                        ##");
		System.out.println("##  4 - hora de nacimiento                         ##");
	}

	static void opcionModificarClienteInvitado() {
		System.out.println("##                                                 ##");
		System.out.println("##                                                 ##");
		System.out.println("##  elige una opcion:                              ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
		System.out.println("##  1 - cambiar nombre y apellidos                 ##");
		System.out.println("##  2 - iniciar una solicitud para cambiar DNI     ##");
		System.out.println("##  3 - cambiar fecha de nacimiento                ##");
		System.out.println("##  4 - cambiar hora de nacimiento                 ##");
		System.out.println("##  5 - eliminar tu perfil                         ##");
		System.out.println("##                                                 ##");

	}

	static void opcionListarClientePorSigno() {
		System.out.println("##                                                 ##");
		System.out.println("##  quieres buscar por:                            ##");
		System.out.println("##                                                 ##");
		System.out.println("##  *********************************************  ##");
		System.out.println("##                                                 ##");
		System.out.println("##  1 - signo solar                                ##");
		System.out.println("##  2 - signo lunar                                ##");
		System.out.println("##  3 - signo ascendente                           ##");
	}

	static void signoNoExiste() {
		System.out.println("##                                                 ##");
		System.out.println("##  el signo introducido no existe                 ##");
		System.out.println("##  estos son los signos:                          ##");
		System.out.println("##  1  - aries                                     ##");
		System.out.println("##  2  - tauro                                     ##");
		System.out.println("##  3  - geminis                                   ##");
		System.out.println("##  4  - cancer                                    ##");
		System.out.println("##  5  - leo                                       ##");
		System.out.println("##  6  - virgo                                     ##");
		System.out.println("##  7  - libra                                     ##");
		System.out.println("##  8  - escorpio                                  ##");
		System.out.println("##  9  - sagitario                                 ##");
		System.out.println("##  10 - capricornio                               ##");
		System.out.println("##  11 - acuario                                   ##");
		System.out.println("##  12 - piscis                                    ##");
		System.out.println("##  prueba de nuevo, o pulsa 0 para retroceder     ##");
		System.out.println("##                                                 ##");
	}

	public static void dateDeAlta() {
		System.out.println("##         *                             *    *    ##");
		System.out.println("##  *                        *              *      ##");
		System.out.println("##        tienes que darte de alta primero         ##");
		System.out.println("##                                           *     ##");
		System.out.println("##       *                     *                   ##");
	}

	private static String significadoSol() {
		return "##  El Sol determina tu ego, identidad, tu \"rol\"   ##\n"
				+ "##  en la vida. Es el nucleo de quien eres.        ##";
	}

	private static String significadoLuna() {
		return "##  La luna pone las normas a tus emociones,         ##\n"
				+ "##  estados de animo y sentimientos. Este el       ##\n"
				+ "##  signo con el que posiblemente te identifiques  ##\n"
				+ "##  mas, dado que refleja tu personalidad cuando   ##\n"
				+ "##  estas a solas o te encuentras muy comodo.      ##";
	}

	static String significadoAsc() {
		return "##  Tu ascendente es la \"mascara\" que presentas    ##\n"
				+ "##  al mundo. Se puede ver en tu estilo personal   ##\n"
				+ "##  y en en la impresion que das cuando conoces    ##\n"
				+ "##  por primera vez a alguien. Algunos dicen que   ##\n"
				+ "##  el ascendente se va volviendo menos relevante  ##\n"
				+ "##  cuanto mas mayor te haces.                     ##";
	}

	static String interpretacionSolar(int n) {
		String interpretacion = "";
		switch (n) {
		case 0:
			return significadoSol() + "##  Tu Sol esta en Aries,  lo que significa que    ##\n"
					+ "##  eres fundamentalmente asertivo, persistente,   ##\n"
					+ "##  y valeroso. Naturalmente competitivo y         ##\n"
					+ "##  salvajemente independiente, sacas adelante     ##\n"
					+ "##  todo con energia y entusiasmo, perseverante.   ##\n"
					+ "##  Necesitas entender que los demas son un todo   ##\n"
					+ "##  complejo.                                      ##";
		case 1:
			return significadoSol() + "##  Tu Sol esta en Tauro,  lo que significa que    ##\n"
					+ "##  eres fundamentalmente estable, deliberado,     ##\n"
					+ "##  practico, aunque de alguna manera obstinado.   ##\n"
					+ "##  Tu lado sensual se toma la comidad y el        ##\n"
					+ "##  placer muy en serio, aprecias las cosas        ##\n"
					+ "##  buenas cuando tienen utilidad y significado.   ##\n"
					+ "##  La gente de tu alrededor te aprecia porque     ##\n"
					+ "##  sabe que puede confiar en ti.                  ##";
		case 2:
			return significadoSol() + "##  Tu Sol esta en Geminis, lo que significa que   ##\n"
					+ "##  eres fundamentalmente dinamico, ingenioso,     ##\n"
					+ "##  eclectico, y curioso. Te fascina todo, y tu    ##\n"
					+ "##  energia ani�ada a menudo se dispersa en        ##\n"
					+ "##  muchas direcciones. En el nivel social, esto   ##\n"
					+ "##  quiza haga que te veas cotilla o excentrico.   ##";
		case 3:
			return significadoSol() + "##  Tu Sol esta en Cancer, lo que significa que    ##\n"
					+ "##  eres fundamentalmente sensible y amable.       ##\n"
					+ "##  Nutres a los que te rodean, y quiza sientas    ##\n"
					+ "##  que tus emociones son una carga para ti.       ##\n"
					+ "##  Estas profundamente comprometido con tus       ##\n"
					+ "##  relaciones cercanas y en ser bueno con la      ##\n"
					+ "##  gente a la que aprecias. Quiza tengas algunos  ##\n"
					+ "##  problemas con la auto-disciplina.              ##";
		case 4:
			return significadoSol() + "##  Tu Sol esta en Leo, lo que significa que       ##\n"
					+ "##  eres fundamentalmente audaz y orgulloso. Te    ##\n"
					+ "##  encanta que te presen atencion y que te        ##\n"
					+ "##  cuiden. Lo devuelves con creces con amabilidad ##\n"
					+ "##  y con tu capacidad de hacer sentir mejor a la  ##\n"
					+ "##  gente cuando tienen un mal dia. Sabes cuales   ##\n"
					+ "##  son habilidades y como usarlas.                ##";
		case 5:
			return significadoSol() + "##  Tu Sol esta en Virgo,  lo que significa que    ##\n"
					+ "##  eres fundamentalmente astuto, responsable,     ##\n"
					+ "##  trabajador y abnegado. Eres exhaustivo,        ##\n"
					+ "##  meticuloso, e intencional en todo lo que       ##\n"
					+ "##  haces. Puedes lograr cosas que poca gente      ##\n"
					+ "##  puede lograr, pero tambien puedes quedarte     ##\n"
					+ "##  empantanado en los detalles de tu dia a dia.   ##\n"
					+ "##  Le das mucha importancia a sentirte sano.      ##";
		case 6:
			return significadoSol() + "##  Tu Sol esta en Libra,  lo que significa que    ##\n"
					+ "##  te sientes fundamentalmente atraido hacia el   ##\n"
					+ "##  concepto de justicia. Tu vision relativa de    ##\n"
					+ "##  las cosas te permite ver las dos versiones     ##\n"
					+ "##  de cada situacion, aunque esto a veces se ve   ##\n"
					+ "##  como que eres indeciso e inseguro. No te atas  ##\n"
					+ "##  a una sola manera de ver las cosas, frecuen-   ##\n"
					+ "##  temente te cuestionas todo, y eres mas         ##\n"
					+ "##  impresionable de lo que aparentas. Haces lo    ##\n"
					+ "##  que dices que vas a hacer y puedes ser muy     ##\n"
					+ "##  productivo. Normalmente vistes bien.           ##";
		case 7:
			return significadoSol() + "##  Tu Sol esta en Escorpio, lo que significa que  ##\n"
					+ "##  tienes una necesidad fundamental de llegar al  ##\n"
					+ "##  fondo de las cosas, lo que a veces puede       ##\n"
					+ "##  llevarte a ser manipulador o a estar hambri-   ##\n"
					+ "##  ento de poder, pero se reduce a una intensa    ##\n"
					+ "##  pasion por la autenticidad, la intimidad real  ##\n"
					+ "##  y la verdad.                                   ##";
		case 8:
			return significadoSol() + "##  Tu Sol esta en Sagitario, asi que eres         ##\n"
					+ "##  fundamentalmente curioso, inquieto, e          ##\n"
					+ "##  independiente. Tienes ideas fuertes y bien     ##\n"
					+ "##  pensadas sobre el significado de las cosas,    ##\n"
					+ "##  lo que esta bien y lo que esta mal. Te         ##\n"
					+ "##  interesa descubrir los limites de la existen-  ##\n"
					+ "##  cia, vives para ser libre y aprender.          ##";
		case 9:
			return significadoSol() + "##  Tu Sol esta en Capricornio, por lo que eres    ##\n"
					+ "##  fundamentalmente responsable, serio, eficiente ##\n"
					+ "##  y racional. Esto puede llevarte: bien a gran   ##\n"
					+ "##  ambicion y hambre de poder (tienes tendencia   ##\n"
					+ "##  a volverte adicto al trabajo y/o al exito),    ##\n"
					+ "##  o bien apoyar a la gente cercana a ti en sus   ##\n"
					+ "##  metas y sue�os. Emocionalmente eres reserva-   ##\n"
					+ "##  do, tienes que aprender a expresar tu mundo    ##\n"
					+ "##  interno y a divertirte.                        ##";
		case 10:
			return significadoSol() + "##  Tu Sol esta en Acuario, por lo que eres        ##\n"
					+ "##  fundamentalmente poco convencional y vas en    ##\n"
					+ "##  en contra de la corriente.                     ##\n"
					+ "##  Te sientes comodo disidiendo o alejandote de   ##\n"
					+ "##  de la norma, tu talento de abstraccion esta    ##\n"
					+ "##  orientado a sobrepasar los limites de lo que   ##\n"
					+ "##  parece razonable. Sueles llevar mucho peso     ##\n"
					+ "##  en los hombros y tienes la necesidad de        ##\n"
					+ "##  luchar por el desfavorecido o el               ##\n"
					+ "##  tratado injustamente.                   *      ##";
		case 11:
			return significadoSol() + "##  Tu Sol esta en Piscis, lo que significa que    ##\n"
					+ "##  eres fundamentalmente so�ador, perspicaz, y    ##\n"
					+ "##  vives en tu mundo. Existes en un plano         ##\n"
					+ "##  caotico fuera del mundo material. Tu rica      ##\n"
					+ "##  imaginacion te dota de una fuerte intuicion    ##\n"
					+ "##  hacia las corrientes emocionales ocultas.      ##\n"
					+ "##  Cuando te sientes ofendido, lo sientes         ##\n"
					+ "##  profundamente, y no estas necesariamnete       ##\n"
					+ "##  interesado en una reconciliacion.              ##";
		default:
			return interpretacion;
		}
	}

	static String interpretacionLunar(int n) {
		switch (n) {
		case 0:
			return significadoLuna() + "##  Tu Luna esta en Aries, lo que significa que    ##\n"
					+ "##  tu yo emocional  es independiente, energico    ##\n"
					+ "##  y entusiasta. Tienes la tendencia de sentirte  ##\n"
					+ "##  inadecuado e intentas sobrecompensar a los     ##\n"
					+ "##  demas, intentando preveer la posibilidad de    ##\n"
					+ "##  que cometas un fallo.                          ##";
		case 1:
			return significadoLuna() + "##  Tu Luna esta en Tauro, lo que significa que    ##\n"
					+ "##  tu yo emocional es muy romantico y senti-      ##\n"
					+ "##  mental. Eres muy leal a la gente cercana a ti, ##\n"
					+ "##  e intentas mantener seguridad y estabilidad    ##\n"
					+ "##  en tus relaciones. A menudo te sientes         ##\n"
					+ "##  impotente frente a situaciones confusas,       ##\n"
					+ "##  pero tu manera de afrontarlas es sensible y    ##\n"
					+ "##  pragmatica.                                    ##";
		case 2:
			return significadoLuna() + "##  Tu Luna esta en Geminis, por lo que tu yo      ##\n"
					+ "##  emocional es a menudo inquieto e inestable.    ##\n"
					+ "##  Te adaptas extremadamente bien, lo cual a      ##\n"
					+ "##  veces te hara sentir como que tiran de ti en   ##\n"
					+ "##  todas las direcciones. Te aburres con facili-  ##\n"
					+ "##  dad y necesitas sentir que eres libre para     ##\n"
					+ "##  ser creativo, y crear significado, lo cual     ##\n"
					+ "##  puede hacerte sentir que estas en una crisis   ##\n"
					+ "##  constante de identidad.                        ##";
		case 3:
			return significadoLuna() + "##  Tu Luna esta en Cancer, lo que significa que   ##\n"
					+ "##  tu yo emocional es sensible, piensa mucho en   ##\n"
					+ "##  los demas, y es empatico. Tienes la tendencia  ##\n"
					+ "##  de sentirte un martir, y secretamente tienes   ##\n"
					+ "##  miedo de ser abandonado por aquellos que amas. ##\n"
					+ "##  A menudo tienes problemas dejando ir las cosas ##\n"
					+ "##  y te sientes como una naufragio emocional.    ##";
		case 4:
			return significadoLuna() + "##  Tu Luna esta en Leo, lo que quiere decir       ##\n"
					+ "##  que tu yo interno es dramatico, orgulloso,     ##\n"
					+ "##  expresivo, idealista, y de alguna manera       ##\n"
					+ "##  centrado en si mismo. Necesitas mucho amor,    ##\n"
					+ "##  cuidad, y validacion por parte de las          ##\n"
					+ "##  personas cercanas a ti.                        ##";
		case 5:
			return significadoLuna() + "##  Tu Luna esta en Virgo, lo que significa que    ##\n"
					+ "##  tu yo emocional es analitico, responsable,     ##\n"
					+ "##  Te adaptas extremadamente bien, lo cual a      ##\n"
					+ "##  y pacificador. Un manojo de nervios, lidias    ##\n"
					+ "##  con la necesidad de ser 100% honesto, lo cual  ##\n"
					+ "##  puede desembocar en auto-sacrificio o tenden-  ##\n"
					+ "##  cia a ser muy quisquilloso.                    ##";
		case 6:
			return significadoLuna() + "##  Tu Luna esta en Libra, por lo que tu yo        ##\n"
					+ "##  emocional es equilibrado, armonioso, ve las    ##\n"
					+ "##  dos caras de cada situacion, es indeciso,      ##\n"
					+ "##  orientado a las relaciones romanticas, y       ##\n"
					+ "##  se desvive por complacer a los demas. Puedes   ##\n"
					+ "##  ser algo obsesivo, pero sueles esconder tus    ##\n"
					+ "##  sentimientos y tus necesidades.                ##";
		case 7:
			return significadoLuna() + "##  Tu Luna esta en Escorpio, por lo que tu yo     ##\n"
					+ "##  emocional es intenso, apasionado, y algo       ##\n"
					+ "##  dramatico. Te cuesta abrirte y dejar que la    ##\n"
					+ "##  gente de tu alrededor de conozca, intentando   ##\n"
					+ "##  guardar privadas tus emociones mas oscuras.    ##\n"
					+ "##  Encuentras dificil confiar en los demas, lo    ##\n"
					+ "##  cual puede manifestarse en tendencias contro-  ##\n"
					+ "##  ladoras.                                       ##";
		case 8:
			return significadoLuna() + "##  Tu Luna esta en Sagitario, por lo que tu yo    ##\n"
					+ "##  emocional es inquieto, se cuestiona a si       ##\n"
					+ "##  mismo, es optimista e independiente. Tal vez   ##\n"
					+ "##  escondas tus sensibilidades detras de un       ##\n"
					+ "##  interes en encontrar nuevas formas de sentir.  ##";
		case 9:
			return significadoLuna() + "##  Tu Luna esta en Capricornio, por lo que tu yo  ##\n"
					+ "##  emocional esta de alguna manera reprimido en   ##\n"
					+ "##  nombre de la responsabilidad, la seriedad y    ##\n"
					+ "##  el raciocinio. Anhelas la comidad que ofrece   ##\n"
					+ "##  la guia de un padre o un maestro.              ##";
		case 10:
			return significadoLuna() + "##  Tu Luna esta en Acuario, por lo que tu yo      ##\n"
					+ "##  emocional es intuitivo, observador, desape-    ##\n"
					+ "##  gado y racional. A menudo estas en tu mundo,   ##\n"
					+ "##  pero te asustas como te sientes realmente.     ##";

		case 11:
			return significadoLuna() + "##  Tu Luna esta en Piscis, por lo que tu yo emo-  ##\n"
					+ "##  cional es es empatico, so�ador, sensible, y    ##\n"
					+ "##  amable. Te sientes vulnerable la mayor parte   ##\n"
					+ "##  del tiempo, y deseas desesperadamente que la   ##\n"
					+ "##  gente mas cercana a ti te comprenda en un      ##\n"
					+ "##  emocional profundo, espcialmente tu pareja o   ##\n"
					+ "##  parejas romantica(s).                         ##";
		default:
			return "";
		}
	}

}
