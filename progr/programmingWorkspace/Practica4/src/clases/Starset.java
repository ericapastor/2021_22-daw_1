package clases;

import programa.Programa;

public class Starset {

	/**
	 * ##########################################################################
	 * ####### Esta clase va a funcionar como base de datos de la tienda ########
	 * ##########################################################################
	 */

	// atributos
	private Cliente[] cliente;

	// constructor
	public Starset(int n) {
		this.cliente = new Cliente[n];
	}

	// getter & setter
	public Cliente[] getCliente() {
		return cliente;
	}

	public void setCliente(Cliente[] cliente) {
		this.cliente = cliente;
	}

	// metodos

	public void datosIniciales(String nombre, String apellido, String dni, String fecha, String hora) {
		for (int i = 0; i < cliente.length; i++) {
			if (cliente[i] == null) {
				cliente[i] = new Cliente(nombre, apellido, dni);
				cliente[i].setFecha(fecha);
				cliente[i].setHora(hora);
				cliente[i].generarCodigoCliente();
				cliente[i].calcularCartaAstral();
				break;
			}
		}
	}

	public String altaCliente() {
		String codigo = "";
		boolean dniNoRepetido = true;
		for (int i = 0; i < cliente.length; i++) {
			if (cliente[i] == null) {
				cliente[i] = new Cliente();
				do {
					cliente[i].pedirDatos();
					if (cliente[i].getNombre().equals("")) {
						cliente[i] = null;
					} else {
						for (int j = 0; j < i; j++) {
							if (cliente[j] != null && cliente[i] != null) {
								if (cliente[j].getDni().equalsIgnoreCase(cliente[i].getDni())) {
									dniNoRepetido = false;
									cliente[i] = null;
									break;
								}
							}
						}
						if (dniNoRepetido == false) {
							Pintar.dniYaExiste();
						} else {
							cliente[i].calcularCartaAstral();
							codigo = cliente[i].getCodigo();
							System.out.println("##  se han guardado los datos\n##");
						}
					}
				} while (dniNoRepetido == false);
				break;
			}
		}
		return codigo;
	}

	public void altaCliente(int n) {
		int count = 0;
		boolean dniNoRepetido = true;
		do {
			System.out.println("##  escribe cuantos clientes quieres dar de alta");
			n = Calculos.elegirOpcion();
			if (n == 0) {
				Pintar.retrocediendo();
				break;
			}
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < cliente.length; j++) {
					if (cliente[j] == null) {
						cliente[j] = new Cliente();
						do {
							cliente[j].pedirDatos();
							if (cliente[j].getNombre().equals("")) {
								cliente[j] = null;
							} else {
								for (int k = 0; k < j; k++) {
									if (cliente[k] != null && cliente[j] != null) {
										if (cliente[k].getDni().equalsIgnoreCase(cliente[j].getDni())) {
											dniNoRepetido = false;
											break;
										}
									}
								}
								if (dniNoRepetido == false) {
									Pintar.dniYaExiste();
								} else {
									cliente[j].calcularCartaAstral();
									count++;
									System.out.println("##  se han guardado los datos\n##");
								}
							}
						} while (dniNoRepetido == false);
						break;
					}
				}
				// este count me sirve para saber si las altas de cliente se han cancelado
				if (count == i) {
					break;
				}
			}
			if (count == n) {
				break;
			}
		} while (n != 0);

	}

	/**
	 * comprobarCliente
	 * 
	 * @param cliente
	 * @return boolean
	 * 
	 *         este metodo me devolvera un boolean, segun si el cliente buscado
	 *         existe o no en la base de datos
	 */
	boolean comprobarCliente(Cliente cliente) {
		for (int i = 0; i < this.cliente.length; i++) {
			if (this.cliente[i] == cliente) {
				return true;
			}
		}
		return false;
	}

	/**
	 * comprobarClientes
	 * 
	 * @return boolean
	 * 
	 *         este metodo me devolvera un boolean, segun si la lista de clientes
	 *         esta vacia o no
	 */
	boolean comprobarClientes() {
		for (int i = 0; i < cliente.length; i++) {
			if (cliente[i] != null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * listarClientes es un metodo que combina el toString con
	 * mostrarFechaHoraSignos para mostrar todos los datos basicos de un cliente si
	 * todos estan rellenos, o en su defecto, mostrar solamente los tres mas basicos
	 * (nombre, apellidos, dni y codigo de cliente)
	 */
	public void listarClientes() {
		Pintar.tramandoEstrellas();
		if (comprobarClientes()) {
			for (int i = 0; i < cliente.length; i++) {
				if (cliente[i] != null) {
					cliente[i].mostrarCliente(cliente[i]);
				}
			}
			Pintar.todosLosClientes();
		} else {
			Pintar.ningunDato();
		}
	}

	/**
	 * buscarCliente
	 * 
	 * @param codigo
	 * @return Cliente
	 */
	private Cliente buscarClienteCodigo(String codigo) {
		for (int i = 0; i < cliente.length; i++) {
			if (cliente[i] != null) {
				if (cliente[i].getCodigo().equals(codigo)) {
					return cliente[i];
				}
			}
		}
		Pintar.ningunCliente();
		return null;
	}

	/**
	 * buscarCliente
	 * 
	 * @param dni
	 * @param dniCorrecto
	 * @return Cliente
	 */

	private Cliente buscarClienteDNI(String dni) {
		dni = Calculos.calcularLetraDNI(dni);
		for (int i = 0; i < cliente.length; i++) {
			if (cliente[i] != null) {
				if (cliente[i].getDni().equalsIgnoreCase(dni)) {
					return cliente[i];
				}
			}
		}
		Pintar.ningunCliente();
		return null;
	}

	/**
	 * mostrarClientePorCodigo
	 */
	private void mostrarClientePorCodigo() {
		String codigo = "";
		System.out.println("##  escribe el codigo del cliente que buscas");
		System.out.println("##  si no lo sabes, vuelve atras con la tecla 0");
		do {
			codigo = Programa.input.nextLine();
			if (codigo.equals("0")) {
				Pintar.retrocediendo();
				break;
			} else if (!codigo.equals("")) {
				if (buscarClienteCodigo(codigo) != null) {
					System.out.println("##");
					System.out.println("##  la busqueda ha sido fructifera:");
					System.out.println("##");
					this.buscarClienteCodigo(codigo).mostrarCliente(this.buscarClienteCodigo(codigo));
				}
			} else {
				System.out.println("##  no has escrito nada, vuelve a intentarlo");
			}
		} while (codigo.equals(""));
	}

	/**
	 * mostrarClientePorDNI
	 */
	private void mostrarClientePorDNI() {
		String dni = "";
		boolean dniCorrecto = false;
		do {
			System.out.println("##  escribe el dni del cliente que buscas");
			System.out.println("##  si no lo sabes, vuelve atras con la tecla 0");
			do {
				dni = Programa.input.nextLine();
				if (dni.equals("0")) {
					break;
				} else if (!dni.equals("")) {
					dniCorrecto = Calculos.comprobarDNI(dni);
					if (dniCorrecto) {
						if (this.buscarClienteDNI(dni) != null) {
							System.out.println("##");
							System.out.println("##  la busqueda ha sido fructifera:");
							System.out.println("##");
							this.buscarClienteDNI(dni).mostrarCliente(this.buscarClienteDNI(dni));
						}
					}
				} else {
					System.out.println("##  no has escrito nada, vuelve a intentarlo");
				}
			} while (dni.equals(""));
			if (dni.equals("0")) {
				break;
			}
		} while (dniCorrecto == false);
	}

	private void mostrarUnClienteInvitado(String codigo) {
		this.buscarClienteCodigo(codigo).mostrarCartaAstralCompleta(this.buscarClienteCodigo(codigo));
	}

	public void mostrarUnCliente(String codigo) {
		if (!codigo.equals("")) {
			if (buscarClienteCodigo(codigo) != null) {
				boolean datos = buscarClienteCodigo(codigo).getSigno().getSolar() != null;
				if (datos == true) {
					this.mostrarUnClienteInvitado(codigo);
				} else {
					System.out.println("##  para ver tu carta completa, rellena tus datos  ##");
					System.out.println("##  de fecha y hora de nacimiento en la opcion:    ##");
					System.out.println("##  \"modificar tus datos\" en el menu anterior      ##");
				}
			}
		} else {
			Pintar.dateDeAlta();
		}
	}

	public void mostrarUnCliente(int n) {
		do {
			Pintar.opcionClienteCodigo();
			n = Calculos.elegirOpcion();
			switch (n) {
			case 0:
				Pintar.retrocediendo();
				break;
			case 1:
				this.mostrarClientePorCodigo();
				break;
			case 2:
				this.mostrarClientePorDNI();
				break;
			default:
				System.out.println("##  opcion no contemplada");
				break;
			}
		} while (n != 0);
	}

	private void modificarPorCodigo() {
		int n = -1;
		String codigo = "";
		System.out.println("##  escribe el codigo del cliente que buscas");
		System.out.println("##  si no lo sabes, vuelve atras con la tecla 0");
		do {
			codigo = Programa.input.nextLine();
			if (codigo.equals("0")) {
				Pintar.retrocediendo();
				break;
			} else if (!codigo.equals("")) {
				if (buscarClienteCodigo(codigo) != null) {
					do {
						Pintar.opcionModificarClienteAdmin();
						n = Calculos.elegirOpcion();
						switch (n) {
						case 0:
							Pintar.retrocediendo();
							break;
						case 1:
							buscarClienteCodigo(codigo)
									.modificarNombreApellidosCliente(this.buscarClienteCodigo(codigo));
							break;
						case 2:
							buscarClienteCodigo(codigo).modificarDNICliente(buscarClienteCodigo(codigo));
							break;
						case 3:
							buscarClienteCodigo(codigo).modificarFechaCliente(this.buscarClienteCodigo(codigo));
							break;
						case 4:
							buscarClienteCodigo(codigo).modificarHoraCliente(this.buscarClienteCodigo(codigo));
							break;
						default:
							System.out.println("##  opcion no contemplada");
							break;
						}
					} while (n != 0);
				}
			} else {
				System.out.println("##  no has escrito nada, vuelve a intentarlo");
			}
		} while (codigo.equals(""));

	}

	private void modificarPorDNI() {
		int n = -1;
		String dni = "";
		boolean dniCorrecto = false;
		do {
			System.out.println("##  escribe el dni del cliente que buscas");
			System.out.println("##  si no lo sabes, vuelve atras con la tecla 0");
			do {
				dni = Programa.input.nextLine();
				if (dni.equals("0")) {
					Pintar.retrocediendo();
					break;
				} else if (!dni.equals("")) {
					dniCorrecto = Calculos.comprobarDNI(dni);
					if (dniCorrecto) {
						if (this.buscarClienteDNI(dni) != null) {
							do {
								Pintar.opcionModificarClienteAdmin();
								n = Calculos.elegirOpcion();
								switch (n) {
								case 0:
									Pintar.retrocediendo();
									break;
								case 1:
									this.buscarClienteDNI(dni)
											.modificarNombreApellidosCliente(this.buscarClienteDNI(dni));
									break;
								case 2:
									this.buscarClienteDNI(dni).modificarDNICliente(this.buscarClienteDNI(dni));
									break;
								case 3:
									this.buscarClienteDNI(dni).modificarFechaCliente(this.buscarClienteDNI(dni));
									break;
								case 4:
									this.buscarClienteDNI(dni).modificarHoraCliente(this.buscarClienteDNI(dni));
									break;
								default:
									System.out.println("##  opcion no contemplada");
									break;
								}
							} while (n == -1);
						}
					}
				} else {
					System.out.println("##  no has escrito nada, vuelve a intentarlo");
				}
			} while (dni.equals(""));
			if (dni.equals("0")) {
				break;
			}
		} while (dniCorrecto == false);
	}

	// este recibira los private modificar nombre y apellidos,
	// modificar dni, modificar fecha y modificar hora
	public void modificarClienteAdmin(int n) {
		do {
			Pintar.opcionClienteCodigo();
			n = Calculos.elegirOpcion();
			switch (n) {
			case 0:
				Pintar.retrocediendo();
				break;
			case 1:
				this.modificarPorCodigo();
				break;
			case 2:
				this.modificarPorDNI();
				break;
			default:
				System.out.println("##  opcion no contemplada");
				break;
			}
		} while (n != 0);
	}

	public String modificarClienteInvitado(int n, String codigo) {
		String texto = "";
		if (!codigo.equals("")) {
			do {
				Pintar.opcionModificarClienteInvitado();
				n = Calculos.elegirOpcion();
				switch (n) {
				case 0:
					Pintar.retrocediendo();
					break;
				case 1:
					this.buscarClienteCodigo(codigo).modificarNombreApellidosCliente(this.buscarClienteCodigo(codigo));
					break;
				case 2:
					do {
						System.out.println("##                                                 ##");
						System.out.println("##  escribe los motivos por los que necesitar      ##");
						System.out.println("##  hacer un cambio en tu DNI                      ##");
						if (texto.equals("")) {
							System.out.println("##  no has escrito nada, vuelve a intentarlo       ##");
						} else {
							texto = Programa.input.nextLine();
							System.out.println("##  hemos recibido tu solicitud                    ##");
							System.out.println("##  en un plazo de 24 horas un miembro del equipo  ##");
							System.out.println("##  de Starset se pondra en contacto con usted     ##");
							System.out.println("##  para la validacion de datos                    ##");
						}
					} while (texto.equals(""));
					break;
				case 3:
					this.buscarClienteCodigo(codigo).modificarFechaCliente(this.buscarClienteCodigo(codigo));
					break;
				case 4:
					this.buscarClienteCodigo(codigo).modificarHoraCliente(this.buscarClienteCodigo(codigo));
					break;
				case 5:
					codigo = this.eliminarPorCodigo(codigo);
					return codigo;
				default:
					System.out.println("##  opcion no contemplada");
					break;
				}
			} while (n != 0);
		} else {
			Pintar.dateDeAlta();
		}
		return codigo;
	}

	private String eliminarPorCodigo(String codigo) {
		String opcion = "n";
		if (buscarClienteCodigo(codigo) != null) {
			System.out.println("##  esta seguro de borrar este usuario [s/n]");
			do {
				opcion = Programa.input.nextLine();
				switch (opcion) {
				case "":
					System.out.println("##  no has escrito nada, vuelve a intentarlo");
					break;
				case "s":
				case "S":
					for (int i = 0; i < cliente.length; i++) {
						if (cliente[i] == buscarClienteCodigo(codigo)) {
							cliente[i] = null;
							codigo = "";
							System.out.println("##\n##  datos eliminados correctamente\n##");
							break;
						}
					}
					break;
				case "n":
				case "N":
					System.out.print("##\n##  transaccion cancelada\n##");
					break;
				default:
					System.out.println("##\n##  opcion no contemplada\n##");
					System.out.println("##  escribe [s/n] (conforme / no conforme)");
					break;
				}
			} while (!opcion.equalsIgnoreCase("n") && !opcion.equalsIgnoreCase("s"));
		}
		return codigo;
	}

	private void eliminarPorDNI(String dni) {
		boolean dniCorrecto = false;
		String opcion = "n";
		dniCorrecto = Calculos.comprobarDNI(dni);
		if (dniCorrecto) {
			if (buscarClienteDNI(dni) != null) {
				System.out.println("##  esta seguro de borrar este usuario [s/n]");
				do {
					opcion = Programa.input.nextLine();
					switch (opcion) {
					case "":
						System.out.println("##  no has escrito nada, vuelve a intentarlo");
						break;
					case "s":
					case "S":
						for (int i = 0; i < cliente.length; i++) {
							if (cliente[i] == buscarClienteDNI(dni)) {
								cliente[i] = null;
								System.out.println("##\n##  datos eliminados correctamente\n##");
								break;
							}
						}
						break;
					case "n":
					case "N":
						System.out.print("##\n##  transaccion cancelada\n##");
						break;
					default:
						System.out.println("##\n##  opcion no contemplada\n##");
						System.out.println("##  escribe [s/n] (conforme / no conforme)");
						break;
					}
				} while (!opcion.equalsIgnoreCase("n") && !opcion.equalsIgnoreCase("s"));
			}
		} else {
			dni = "";
		}
	}

	// menu administrador
	public void eliminarCliente(int n) {
		do {
			Pintar.opcionClienteCodigo();
			n = Calculos.elegirOpcion();
			switch (n) {
			case 0:
				Pintar.retrocediendo();
				break;
			case 1:
				System.out.println("##  escribe el codigo del cliente que quieres borrar");
				System.out.println("##  si no lo sabes, vuelve atras con la tecla 0");
				String codigo = "";
				do {
					codigo = Programa.input.nextLine();
					if (codigo.equals("0")) {
						Pintar.retrocediendo();
						break;
					} else if (!codigo.equals("")) {
						this.eliminarPorCodigo(codigo);
					} else {
						System.out.println("##  no has escrito nada, vuelve a intentarlo");
					}
				} while (codigo.equals(""));
				break;
			case 2:
				System.out.println("##  escribe el DNI del cliente que quieres borrar");
				System.out.println("##  si no lo sabes, vuelve atras con la tecla 0");
				String dni = "";
				do {
					dni = Programa.input.nextLine();
					if (dni.equals("0")) {
						Pintar.retrocediendo();
						break;
					} else if (!dni.equals("")) {
						this.eliminarPorDNI(dni);
					} else

					{
						System.out.println("##  no has escrito nada, vuelve a intentarlo");
					}
				} while (dni.equals(""));
				break;
			}
		} while (n < 0 && n > 2);
	}

	private void listarClientesPorSolar() {
		int counter = 0;
		String signo = "";
		boolean signoExiste = false;
		do {
			System.out.println("##  escribe que signo quieres buscar");
			signo = Programa.input.nextLine();
			if (!signo.equals("0")) {
				for (int i = 0; i < signo.length(); i++) {
					if (cliente[i] != null) {
						if (cliente[i].getSigno().getSolar() != null) {
							signoExiste = cliente[i].getSigno().signoExiste(signo);
							if (signoExiste == true) {
								if (cliente[i].getSigno().getSolar().equalsIgnoreCase(signo)) {
									cliente[i].mostrarCliente(cliente[i]);
									counter++;
								}
							} else {
								Pintar.signoNoExiste();
								signo = "";
								break;
							}
						}
					}
				}
				if (counter == 0 && signoExiste == true) {
					System.out.println("##\n##  no se encontraron coincidencias");
				}
			} else {
				Pintar.retrocediendo();
			}
		} while (signo.equals(""));
	}

	private void listarClientesPorLunar() {
		int counter = 0;
		String signo = "";
		boolean signoExiste = false;
		do {
			System.out.println("##  escribe que signo quieres buscar");
			signo = Programa.input.nextLine();
			if (!signo.equals("0")) {
				for (int i = 0; i < signo.length(); i++) {
					if (cliente[i] != null) {
						if (cliente[i].getSigno().getLunar() != null) {
							signoExiste = cliente[i].getSigno().signoExiste(signo);
							if (signoExiste == true) {
								if (cliente[i].getSigno().getLunar().equalsIgnoreCase(signo)) {
									cliente[i].mostrarCliente(cliente[i]);
									counter++;
								}
							} else {
								Pintar.signoNoExiste();
								signo = "";
								break;
							}
						}
					}
				}
				if (counter == 0 && signoExiste == true) {
					System.out.println("##\n##  no se encontraron coincidencias");
				}
			} else {
				Pintar.retrocediendo();
			}

		} while (signo.equals(""));
	}

	private void listarClientesPorAscendente() {
		int counter = 0;
		String signo = "";
		boolean signoExiste = false;
		do {
			System.out.println("##  escribe que signo quieres buscar");
			signo = Programa.input.nextLine();
			if (!signo.equals("0")) {
				for (int i = 0; i < signo.length(); i++) {
					if (cliente[i] != null) {
						if (cliente[i].getSigno().getAscendente() != null) {
							signoExiste = cliente[i].getSigno().signoExiste(signo);
							if (signoExiste == true) {
								if (cliente[i].getSigno().getAscendente().equalsIgnoreCase(signo)) {
									cliente[i].mostrarCliente(cliente[i]);
									counter++;
								}
							} else {
								Pintar.signoNoExiste();
								signo = "";
								break;
							}
						}
					}
				}
				if (counter == 0 && signoExiste == true) {
					System.out.println("##\n##  no se encontraron coincidencias");
				}
			} else {
				Pintar.retrocediendo();
			}
		} while (signo.equals(""));
	}

	// aqui entrara un atributo que yo escriba y no uno introducido por teclado
	public void listarClientesSegunAtributo() {
	}

	/**
	 * listarClientesSegunAtributo
	 * 
	 * @param n
	 * 
	 *          con este metodo llamare a los tres anteriores, y en el doy la opcion
	 *          de buscar clientes por signo solar, lunar, o ascendente
	 */
	public void listarClientesSegunAtributo(int n) {
		do {
			Pintar.opcionListarClientePorSigno();
			n = Calculos.elegirOpcion();
			switch (n) {
			case 0:
				Pintar.retrocediendo();
				break;
			case 1:
				this.listarClientesPorSolar();
				break;
			case 2:
				this.listarClientesPorLunar();
				break;
			case 3:
				this.listarClientesPorAscendente();
				break;
			default:
				break;
			}
		} while (n != 0);
	}

}
