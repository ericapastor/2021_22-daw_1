package clases;

import programa.Programa;

public class Calculos {

	/**
	 * ##############################################################################
	 * 
	 * La clase CalculoFechas no tiene atributos. Tiene metodos estaticos para hacer
	 * comprobaciones tales como asegurar que un String son todo numeros antes de
	 * parsear, comprobar la validez del dni, etc
	 * 
	 * ##############################################################################
	 */

	/**
	 * comprobarNumeros
	 * 
	 * @param numero
	 * @return boolean
	 * 
	 *         Este metodo comprueba si una cadena String introducida es un numero.
	 */
	public static boolean comprobarNumeros(String numero) {
		for (int i = 0; i < numero.length(); i++) {
			if (numero.charAt(i) < '0' || numero.charAt(i) > '9') {
				return false;
			}
		}
		return true;
	}

	/**
	 * comprobarDNI
	 * 
	 * @param dni
	 * @return String
	 * 
	 *         Este metodo comprueba si un dni introducido por teclado tiene la
	 *         largura correcta y si solamente tiene numeros.
	 */
	public static boolean comprobarDNI(String dni) {
		if (dni.length() == 8) {
			if (comprobarNumeros(dni) == true) {
				return true;
			} else {
				System.out.println("##  el DNI solo debe contener numeros");
				return false;
			}
		} else {
			System.out.println("##  formato incorrecto: el DNI debe tener\n##  ocho digitos, escribelo sin letra\n##");
			return false;
		}
	}

	/**
	 * 
	 * @param dni
	 * @return String
	 * 
	 *         Aqui calculamos la letra para el dni introducido por teclado.
	 */
	public static String calcularLetraDNI(String dni) {
		int dniNum = Integer.parseInt(dni);
		char[] letrasDNI = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V',
				'H', 'L', 'C', 'K', 'E' };
		dniNum %= 23;
		for (int i = 0; i < 23; i++) {
			if (dniNum == i) {
				dni = dni + letrasDNI[i];
			}
		}
		return dni;
	}

	/**
	 * comprobarFecha
	 * 
	 * @param fecha
	 * @return boolean
	 * 
	 *         En este metodo vemos si una fecha introducida por teclado es correcta
	 *         antes de parsearla a LocalDate.
	 */
	public static boolean comprobarFecha(String fecha) {
		boolean formatoCorrecto = fecha.length() == 10 && fecha.indexOf('-') == 4 && fecha.lastIndexOf('-') == 7;
		boolean fechaCorrecta = false;
		// compruebo el formato
		if (formatoCorrecto == true) {
			String anyoString = fecha.substring(0, 4);
			String mesString = fecha.substring(5, 7);
			String diaString = fecha.substring(8);
			// compruebo que los numeros sean numeros
			if (Calculos.comprobarNumeros(anyoString) == true) {
				if (Calculos.comprobarNumeros(mesString) == true) {
					if (Calculos.comprobarNumeros(diaString) == true) {
						// parseo
						int anyo = Integer.parseInt(anyoString);
						int mes = Integer.parseInt(mesString);
						int dia = Integer.parseInt(diaString);
						// compruebo que los numeros esten en el rango adecuado
						boolean anyoCorrecto = anyo >= 1927 && anyo <= 2010;
						boolean mesCorrecto = mes >= 1 && mes <= 12;
						boolean diaCorrecto = ((dia >= 1 && dia <= 31)
								&& (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12))
								|| ((dia >= 1 && dia <= 30) && (mes == 4 || mes == 6 || mes == 9 || mes == 11))
								|| ((dia >= 1 && dia <= 28 && mes == 2 && anyo % 4 != 0)
										|| (dia >= 1 && dia <= 29 && mes == 2 && anyo % 4 == 0));
						System.out
								.println(anyoCorrecto
										? (mesCorrecto
												? (diaCorrecto ? ""
														: "##  el dato del dia no es correcto.\nVuelve a intentarlo:")
												: "##  el dato de mes no es correcto.\nVuelve a intentarlo:")
										: "##  el a�o debe estar entre 1927 y 2010.\nVuelve a intentarlo:");
						fechaCorrecta = anyoCorrecto && mesCorrecto && diaCorrecto;
						// si todo esta bien devuelvo true
						if (fechaCorrecta == true) {
							return true;
						}
					} else {
						System.out.println("##  algun digito del dia no es un numero");
					}

				} else {
					System.out.println("##  algun digito del mes no es un numero");
				}
			} else {
				System.out.println("##  algun digito del a�o no es un numero");
			}
		} else {
			System.out.println("##  el formato de fecha no es correcto, recuerda:");
			System.out.println("##  el formato debe ser: yyyy-MM-dd.");
			System.out.println("##  vuelve a intentarlo:");
		}
		return false;
	}

	/**
	 * comprobarHora
	 * 
	 * @param hora
	 * @return boolean
	 * 
	 *         En este metodo vemos si una hora introducida por teclado es correcta
	 *         antes de parsearla a LocalTime.
	 */
	public static boolean comprobarHora(String hora) {
		boolean horaCorrecta = false;
		boolean formatoCorrecto = hora.length() == 5 && hora.indexOf(':') == 2;
		// compruebo formato
		if (formatoCorrecto == true) {
			String horaString = hora.substring(0, 2);
			String minutoString = hora.substring(3);
			// compruebo que sean numeros lo que hay en las posiciones de horas
			if (Calculos.comprobarNumeros(horaString) == true) {
				// compruebo que sean numeros lo que hay en las posiciones de minutos
				if (Calculos.comprobarNumeros(minutoString) == true) {
					int horas = Integer.parseInt(horaString);
					int minutos = Integer.parseInt(minutoString);
					boolean horasCorrectas = horas >= 0 && horas <= 23;
					boolean minutosCorrectos = minutos >= 0 && minutos <= 59;
					System.out
							.println(horasCorrectas ? (minutosCorrectos ? "" : "##  los minutos deben estar entre 0 y 59")
									: "##  la hora debe estar entre 0 y 23");
					horaCorrecta = horasCorrectas && minutosCorrectos;
					if (horaCorrecta == true) {
						return true;
					}

				} else {
					System.out.println("##  algun digito de los minutos no es un numero");
				}
			} else {
				System.out.println("##  algun digito de las horas no es un numero");
			}
		} else {
			System.out.println("##  recuerda que el formato debe ser hh:MM");
			System.out.println("##  por ejemplo:  07:34");
		}
		return false;
	}

	public static String comprobarFranjaHoraria(String hora) {
		String horaString = hora.substring(0, 2);
		String minutoString = hora.substring(3);
		int horaInt = Integer.parseInt(horaString);
		if (horaInt >= 1 && horaInt <= 12) {
			System.out.println("##  escribe AM o PM para indicar la franja horaria");
			String franja = "";
			boolean franjaPM = false;
			boolean fanjaAM = false;
			boolean franjaCorrecta = false;
			do {
				franja = Programa.input.nextLine();
				fanjaAM = franja.equalsIgnoreCase("am");
				franjaPM = franja.equalsIgnoreCase("pm");
				franjaCorrecta = franjaPM || fanjaAM;
				if (franjaCorrecta == true) {
					if (horaInt == 12) {
						if (fanjaAM == true) {
							hora = "00:" + minutoString;
						}
					} else {
						if (franjaPM == true) {
							hora = (horaInt + 12) + ":" + minutoString;
						}
					}
				} else {
					System.out.println("##  recuerda, escribe AM o PM para indicar la franja horaria");
				}
			} while (franjaCorrecta == false);
		}
		return hora;
	}

	public static int elegirOpcion() {
		String numero = "";
		int n = -1;
		boolean esNumero = false;
		do {
			numero = "";
			numero = Programa.input.nextLine();
			if (!numero.equals("")) {
				esNumero = Calculos.comprobarNumeros(numero);
				if (esNumero == true) {
					n = Integer.parseInt(numero);
					return n;
				} else {
					System.out.println("##  la opcion introducida no es un numero");
				}
			} else {
				System.out.println("##  no has escrito nada, vuelve a intentarlo");
			}
		} while (numero.equals(""));
		return n;
	}
}
