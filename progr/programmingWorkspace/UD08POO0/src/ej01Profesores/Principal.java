package ej01Profesores;

import java.util.Scanner;

public class Principal {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		// creamos profesores
		// si no he creado un constructor, uso el nombre de la clase
		Profesor objProfe1 = new Profesor();
		Profesor objProfe2 = new Profesor();
		Profesor objProfe3 = new Profesor();
		Profesor objProfe4 = new Profesor();

		// rellenar profesor
		// el metodo estatico no requiere objeto
		// si no fuera estatico si lo requeriria
		rellenarProfesor(objProfe1);
		rellenarProfesor(objProfe2);
		rellenarProfesor(objProfe3);
		rellenarProfesor(objProfe4);
		
		// muestro profes
		System.out.println("Profe1:");
		mostrarProfesor(objProfe1);
		System.out.println("Profe2:");
		mostrarProfesor(objProfe2);
		System.out.println("Profe3:");
		mostrarProfesor(objProfe3);
		System.out.println("Profe4:");
		mostrarProfesor(objProfe4);

		input.close();
	}

	public static void rellenarProfesor(Profesor objProfesor) {
		System.out.println("Dime el nombre");
		String nombre = input.nextLine();
		System.out.println("Dime los apellidos");
		String apellidos = input.nextLine();
		System.out.println("Dame el ciclo");
		String ciclo = input.nextLine();
		// guardamos los datos
		// necesitamos el set
		objProfesor.setNombre(nombre);
		objProfesor.setApellidos(apellidos);
		objProfesor.setCiclo(ciclo);
	}

	public static void mostrarProfesor(Profesor objProfesor) {
		System.out.println("Su nombre es " + objProfesor.getNombre() + ", sus apellidos son "
				+ objProfesor.getApellidos() + ". Y el ciclo que imparte es " + objProfesor.getCiclo() + ".");
	}

}
