package ej01Profesores;

public class Profesor {
	// atributos
	private String nombre;
	private String apellidos;
	private String ciclo;
	
	// se recomienda atributos private
	// con metodos setter y getter public
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
}
