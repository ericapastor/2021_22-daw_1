package ej05Vehiculos;

public class Vehiculo {

	// atributos

	private String marca;
	private String modelo;
	private float autonomia;
	private float kilometraje;

	// constructores

	public Vehiculo() {
		marca = "";
		modelo = "";
		autonomia = 0.0F;
		kilometraje = 0.0F;
	}

	public Vehiculo(String marca, String modelo, float autonomia, float kilometraje) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.autonomia = autonomia;
		this.kilometraje = kilometraje;
	}

	// setters and getters

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public float getAutonomia() {
		return autonomia;
	}

	public void setAutonomia(float autonomia) {
		this.autonomia = autonomia;
	}

	public float getKilometraje() {
		return kilometraje;
	}

	public void setKilometraje(float kilometraje) {
		this.kilometraje = kilometraje;
	}

	// metodos

	public boolean esSeguro(int km) {
		if (km < this.autonomia) {
			return true;
		}
		return false;
	}

}
