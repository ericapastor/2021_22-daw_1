package ej05Vehiculos;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Vehiculo mioCasa = new Vehiculo("Skoda", "Octavia", 600, 340000.0F);
		System.out.println("Dime cuantos km");
		int km = input.nextInt();
		if (mioCasa.esSeguro(km)) {
			System.out.println("El coche es seguro.");
		} else {
			System.out.println("El coche no es seguro.");
		}
		input.close();
	}

}
