package ej04Libros;

public class Principal {

	public static void main(String[] args) {
		// creo 3 libros
		Libros libro1 = new Libros("titulo1", "autor1", 2002, "editorial1", 23.23F);
		Libros libro2 = new Libros("titulo2", "autor2", 2003, "editorial2", 24.44F);
		Libros libro3 = new Libros("titulo3", "autor3", 2004, "editorial3", 26.99F);
		// muestro los datos de cada libro
		// tengo que usar el get y repetir por cada libro porque no tengo un metodo
		System.out.println("Datos de libro1");
		System.out.println("Titulo " + libro1.getTitulo());
		System.out.println("Autor " + libro1.getAutor());
		System.out.println("A�o de publicacion " + libro1.getFecha());
		System.out.println("Editorial " + libro1.getEditorial());
		System.out.println("Precio " + libro1.getPrecio());

		System.out.println("Datos de libro2");
		System.out.println("Titulo " + libro2.getTitulo());
		System.out.println("Precio " + libro2.getPrecio());
		System.out.println("A�o de publicacion " + libro2.getFecha());
		System.out.println("Editorial " + libro2.getEditorial());
		System.out.println("Precio " + libro2.getPrecio());

		System.out.println("Datos de libro3");
		System.out.println("Titulo " + libro3.getTitulo());
		System.out.println("Autor " + libro3.getAutor());
		System.out.println("A�o de publicacion " + libro3.getFecha());
		System.out.println("Editorial " + libro3.getEditorial());
		System.out.println("Precio " + libro3.getPrecio());

		// mostramos el precio con iva
		final float IVA = 21F;
		System.out.println("Mostrarmos precios con iva del libro1");
		// LLamo a precioConIva usando la clase Libro porque es static
		System.out.println("Precio con IVA " + Libros.precioConIVA((float) libro1.getPrecio(), IVA));
	}

}
