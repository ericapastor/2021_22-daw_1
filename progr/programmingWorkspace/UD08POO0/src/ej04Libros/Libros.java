package ej04Libros;

public class Libros {
	// atributos
	private String titulo;
	private String autor;
	private int fecha;
	private String editorial;
	private float precio;

	// constructor sin parametros

	public Libros() {
		titulo = "";
		autor = "";
		fecha = 0;
		editorial = "";
		precio = 0.0F;
	}

	// constructor con parametros
	public Libros(String titulo, String autor, int fecha, String editorial, float precio) {
		this.titulo = titulo;
		this.autor = autor;
		this.fecha = fecha;
		this.editorial = editorial;
		this.precio = precio;
	}

	// setters and getters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getFecha() {
		return fecha;
	}

	public void setFecha(int fecha) {
		this.fecha = fecha;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	// metodo
	static float precioConIVA(float precio, float iva) {
		return (precio + (precio * iva / 100));
	}
}
