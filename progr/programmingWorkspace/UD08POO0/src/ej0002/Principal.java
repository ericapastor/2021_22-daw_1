package ej0002;

public class Principal {

	public void imprimirInfo(Naranja oNaranja) {
		// el metodo imprimirInfo recibe una naranja
		// oNaranja es un objeto naranja
		// String cadena -> cadena es un objeto String
		System.out.println("Fruta Naranja");
		System.out.println("Nombre " + oNaranja.nombre);
		System.out.println("Caracteristicas " + oNaranja.caracteristicas);
	}

	public void imprimirInfo(Manzana oManzana) {
		// el metodo imprimirInfo recibe una manzana
		// oManzana es un objeto manzana
		System.out.println("Fruta Manzana");
		System.out.println("Nombre " + oManzana.nombre);
		System.out.println("Caracteristicas " + oManzana.caracteristicas);
	}

	public static void main(String[] args) {
		// creo un objeto Naranja
		Naranja oNaranja = new Naranja();
		Manzana oManzana = new Manzana();
		// Naranja() es un constructor por defecto cuando no he definido yo uno
		// creo un objeto Principal
		Principal miObjeto = new Principal();
		// cuando un metodo no es static, necesita crear un objeto de la clase
		// llamo al metodo imprimirInfo (recibe una naranja)
		miObjeto.imprimirInfo(oNaranja);
		miObjeto.imprimirInfo(oManzana);

	}
	
}
