package ej03Numeros;

public class Numero {
	private int numero;

	public Numero() {
		numero = 0;
	}

	public Numero(int numero) {
		this.numero = numero;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	boolean primo() {
		for (int i = 2; i < this.numero; i++) {
			if (this.numero % i == 0) {
				return false;
			}
		}
		return true;
	}

	long factorial() {
		long m = (long) this.numero;
		long fac = 1;
		for (long i = 1; i <= m; i++) {
			fac *= i;
		}
		return fac;
	}

	void piramide1() {
		int n = this.numero;
		for (int i = 0; i < this.numero; i++) {
			for (int j = n; j > 0; j--) {
				System.out.print(" ");
			}
			for (int j = 0; j <= i * 2; j++) {
				System.out.print("*");
			}
			System.out.println();
			n -= 1;
		}
	}

	void piramide2() {
		int n = this.numero * 2;
		for (int i = 0; i < this.numero * 2; i++) {
			for (int j = n; j > 0; j--) {
				System.out.print(" ");
			}
			for (int j = 0; j <= i * 2; j++) {
				System.out.print("*");
			}
			System.out.println();
			n -= 1;
		}
	}
}
