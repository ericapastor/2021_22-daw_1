package ej03Numeros;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// pedir numero
		System.out.println("Dame un numero");
		int n = input.nextInt();
		// cambiar su valor
		Numero numero = new Numero(n);
		// comprobar si es primo
		if (numero.primo() == true) {
			System.out.println("El numero " + numero.getNumero() + " es primo.");
		} else {
			System.out.println("El numero " + numero.getNumero() + " no es primo.");
		}
		// mostrar su factorial
		System.out.println("Y su factorial es: " + numero.factorial());
		// mostrar piramide 1
		System.out.println("\n*****************");
		System.out.println("Primera piramide:");
		System.out.println("*****************\n");
		numero.piramide1();
		// mostrar piramide 2
		System.out.println("\n*****************");
		System.out.println("Segunda piramide:");
		System.out.println("*****************\n");
		numero.piramide2();
		input.close();
	}

}
