package ej02Alumnos1;

public class Alumno {
	
	// atributos
	private String nombre;
	private String apellidos;
	private double notaMedia;

	// constructor
	public Alumno() {
	}

	// setters and getters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public double getNotaMedia() {
		return notaMedia;
	}

	public void setNotaMedia(double notaMedia) {
		this.notaMedia = notaMedia;
	}
}
