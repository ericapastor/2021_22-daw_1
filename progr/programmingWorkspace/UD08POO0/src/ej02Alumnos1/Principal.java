package ej02Alumnos1;

import java.util.Scanner;

public class Principal {

	static Scanner input = new Scanner(System.in);

	public Principal() {
	}

	public static void main(String[] args) {
		Alumno alumno1 = new Alumno();
		Alumno alumno2 = new Alumno();
		Principal main = new Principal();
		main.rellenarAlumno(alumno1);
		main.rellenarAlumno(alumno2);
		main.mostrarAlumno(alumno1);
		main.mostrarAlumno(alumno2);
		input.close();
	}

	public void rellenarAlumno(Alumno alumno) {
		System.out.println("Nombre:");
		alumno.setNombre(input.nextLine());
		System.out.println("Apellidos:");
		alumno.setApellidos(input.nextLine());
		double nota;
		do {
			System.out.println("Nota media:");
			nota = input.nextDouble();
			if(nota < 0 || nota > 10) {
				System.out.println("La nota media no es valida. Vuelve a intentarlo.");
			}
		} while (nota < 0 || nota > 10);
		input.nextLine();
		alumno.setNotaMedia(nota);
	}

	public void mostrarAlumno(Alumno alumno) {
		System.out.println("El alumno " + alumno.getNombre() + " \nCon apellidos " + alumno.getApellidos()
				+ "\nTiene una nota media de " + alumno.getNotaMedia() + ".");
		if (alumno.getNotaMedia() >= 5 && alumno.getNotaMedia() <= 10) {
			System.out.println("Promociona al siguiente ciclo.");
		} else if (alumno.getNotaMedia() < 5 && alumno.getNotaMedia() >= 0) {
			System.out.println("No promociona al siguiente ciclo.");
		} else {
			System.out.println("La nota media introducida no es valida.");
		}
	}

}
