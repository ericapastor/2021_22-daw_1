package ej0007;

public class Fruta {

	String nombre;
	String caracteristica;
	
	public Fruta(String nombre, String caracteristica) {
		this.nombre = nombre;
		this.caracteristica = caracteristica;
	}

	 public void imprimirInfo(Fruta fr) {
		    System.out.println("Nombre "+fr.nombre);
		    System.out.println("Caracteristicas "+fr.caracteristica);
		  }
	 
}
