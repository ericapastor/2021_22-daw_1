package ej0007;

public class Principal {
	
	public static void main(String[]args) {
		Fruta unaFruta=new Fruta("Fruta","madura");
		unaFruta.imprimirInfo(unaFruta);
		Naranja unaNaranja=new Naranja("Naranjita", "dulce");
		unaNaranja.imprimirInfo(unaNaranja);
		Manzana unaManzana=new Manzana("Manzanita","picante");
		unaManzana.imprimirInfo(unaManzana);
	}

}
