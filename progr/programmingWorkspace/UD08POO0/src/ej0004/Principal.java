package ej0004;

public class Principal {

	public static void main(String[] args) {
		// creamos un ordenador
		Ordenador miOrdenador=new Ordenador();
		//mostramos los valores de los atributos
		System.out.println("Tama�o memoria: "+miOrdenador.tamMemoria);
		System.out.println("Tipo memoria: "+miOrdenador.tipoMemoria);
		System.out.println("Tama�o disco duro: "+miOrdenador.tamDiscoDuro);
		System.out.println("Modelo procesador: "+miOrdenador.modeloProcesador);
		//cambiamos los valores de los atributos
		System.out.print("Cambio tama�o memoria: ");
		miOrdenador.cambiarTamMemoria(25);
		System.out.print("Cambio tipo memoria: ");
		miOrdenador.cambiarTipoMemoria("QWERTY");
		System.out.print("Cambio tama�o disco duro: ");
		miOrdenador.cambiarTamDiscoDuro(12354);
		System.out.print("Cambio modelo procesador: ");
		miOrdenador.cambiarModeloProcesador("%$#@!56");
	}

}
