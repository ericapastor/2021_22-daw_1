package ej0004;

public class Ordenador {
	// atributos
	int tamMemoria;
	String tipoMemoria;
	int tamDiscoDuro;
	String modeloProcesador;

	// constructor
	public Ordenador() {
		tamMemoria = 63;
		tipoMemoria = "AHTG";
		tamDiscoDuro = 5230;
		modeloProcesador = "SFI1234";
	}

	// metodos
	public void cambiarTamMemoria(int tamMemoria1) {
		tamMemoria = tamMemoria1;
		System.out.println(tamMemoria);
	}

	public void cambiarTipoMemoria(String tipoMemoria1) {
		tipoMemoria = tipoMemoria1;
		System.out.println(tipoMemoria);
	}

	public void cambiarTamDiscoDuro(int tamDiscoDuro1) {
		tamDiscoDuro = tamDiscoDuro1;
		System.out.println(tamDiscoDuro);
	}

	public void cambiarModeloProcesador(String modeloProcesador1) {
		modeloProcesador = modeloProcesador1;
		System.out.println(modeloProcesador);
	}
}
