package ej0003;

public class Principal {

	public static void main(String[] args) {
		//creo una naranja
		Naranja oNaranja = new Naranja();
		//muestro sus datos
		oNaranja.imprimirInfo(oNaranja);
		//creo una manzana
		Manzana oManzana = new Manzana();
		//muestro sus datos
		oManzana.imprimirInfo(oManzana);
	}

}