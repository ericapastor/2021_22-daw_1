package ej0003;

public class Naranja {
	//atributos
	String nombre;
	String caracteristicas;
	// constructor
	public Naranja() {
		nombre = "Naranja";
		caracteristicas = "Redonda, piel rugosa, parte interior dividida en gajos";
	}
	//metodos
	public void imprimirInfo(Naranja oNaranja) {
		System.out.println("Fruta Naranja");
		System.out.println("Nombre "+oNaranja.nombre);
		System.out.println("Caracteristicas "+oNaranja.caracteristicas);
	}
}
