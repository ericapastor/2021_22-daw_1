package ej0003;

public class Manzana {
	// atributos
	String nombre;
	String caracteristicas;

	// constructor
	public Manzana() {
		nombre = "Manzana";
		caracteristicas = "Piel suave, en diversos colores";
	}

	// metodos
	public void imprimirInfo(Manzana oManzana) {
		System.out.println("Fruta Manzana");
		System.out.println("Nombre " + oManzana.nombre);
		System.out.println("Caracteristicas " + oManzana.caracteristicas);
	}
}
