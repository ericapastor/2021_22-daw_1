package ej02Alumnos2;

public class Principal {

	public static void main(String[] args) {
		// creamos alumnos
		Alumno obAlumno1 = new Alumno();
		Alumno obAlumno2 = new Alumno();
		// rellenar alumnos
		obAlumno1.rellenarAlumno();
		obAlumno2.rellenarAlumno();
		// visualizar alumnos
		obAlumno1.visualizarAlumno(obAlumno1);
		obAlumno2.visualizarAlumno(obAlumno2);
		// promociona
		System.out.println("Promocion alumno1");
		if (obAlumno1.promociona(obAlumno1.getNotaMedia())) {
			System.out.println("Si promociona");
		} else {
			System.out.println("No promociona");
		}
		System.out.println("Promocion alumno2");
		if (obAlumno2.promociona(obAlumno2.getNotaMedia())) {
			System.out.println("Si promociona");
		} else {
			System.out.println("No promociona");
		}
	}

}
