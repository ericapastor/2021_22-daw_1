package ej02Alumnos2;

import java.util.Scanner;

public class Alumno {

	static Scanner input = new Scanner(System.in);

	// atributos
	private String nombre;
	private String apellidos;
	private double notaMedia;

	// constructor sin parametros
	// this -> esta clase
	public Alumno() {
		this.nombre = "";
		this.apellidos = "";
		this.notaMedia = 0.0;
	}

	// setter y getter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public double getNotaMedia() {
		return notaMedia;
	}

	public void setNotaMedia(double notaMedia) {
		this.notaMedia = notaMedia;
	}

	// metodos
	public boolean promociona(double notaMedia) {
		if (notaMedia >= 5) {
			return true;
		}
		return false;
	}

	public void rellenarAlumno() {
		System.out.println("Dame el nombre");
		nombre = input.nextLine();
		// guardo el nombre del alumno
		this.setNombre(nombre);
		System.out.println("Dime los apellidos");
		apellidos = input.nextLine();
		// guardo los apellidos del alumno
		this.setApellidos(apellidos);
		System.out.println("Dime la nota media");
		notaMedia = input.nextDouble();
		// guardo la nota media
		this.setNotaMedia(notaMedia);
		input.nextLine();
	}

	public void visualizarAlumno(Alumno alumno) {
		System.out.println("El nombre es " + nombre);
		System.out.println("Los apellidos son " + apellidos);
		System.out.println("La nota media es " + notaMedia);
	}
}
