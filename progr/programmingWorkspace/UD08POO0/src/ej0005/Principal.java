package ej0005;

import java.util.Scanner;

public class Principal {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// creo un objeto de la clase Coche
		Coche coche123 = new Coche();
		// muestro sus atributos iniciales
		System.out.println("Atributos iniciales del coche-123:");
		System.out.println("Marca: " + coche123.marca);
		System.out.println("Modelo: " + coche123.modelo);
		System.out.println("Velocidad: " + coche123.velocidad + " km/h");
		System.out.println("Tam de rueda: " + coche123.tamRueda + " cm");
		// cambio sus atributos
		System.out.println("Marca?");
		String marca123 = input.nextLine();
		System.out.println("Modelo?");
		String modelo123 = input.nextLine();
		System.out.println("Velocidad?");
		int velocidad123 = input.nextInt();
		System.out.println("Tam de rueda?");
		double tamRueda123 = input.nextDouble();
		coche123.cambiarMarca(marca123);
		coche123.cambiarModelo(modelo123);
		coche123.cambiarVelocidad(velocidad123);
		coche123.cambiarTamRueda(tamRueda123);
		System.out.println("\nNuevos atributos:");
		System.out.println("Nueva marca: " + coche123.marca);
		System.out.println("Nuevo modelo: " + coche123.modelo);
		System.out.println("Nueva velocidad: " + coche123.velocidad + " km/h");
		System.out.println("Nuevo tam de rueda: " + coche123.tamRueda + " cm");
		// cierro Scanner
		input.close();
	}

}
