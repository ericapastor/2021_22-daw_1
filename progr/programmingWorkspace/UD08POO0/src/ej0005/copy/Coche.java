package ej0005.copy;

public class Coche {
	// atributos:
	String marca;
	String modelo;
	int velocidad;
	double tamRueda;

	// constructor:
	public Coche() {
		marca = "Mercedes";
		modelo = "NoSeDeCochesJaja";
		velocidad = 100;
		tamRueda = 22.5;
	}

	public Coche(String marca, String modelo, int velocidad, double tamRueda) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.velocidad = velocidad;
		this.tamRueda = tamRueda;
	}

	// metodos para cambiar los atributos
	public String cambiarMarca(String a) {
		marca = a;
		return marca;
	}

	public String cambiarModelo(String a) {
		modelo = a;
		return modelo;
	}

	public int cambiarVelocidad(int a) {
		velocidad = a;
		return velocidad;
	}

	public double cambiarTamRueda(double a) {
		tamRueda = a;
		return tamRueda;
	}
}
