package juego01;

import java.util.Scanner;

public class Adivina_Edades {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		// variables
		presentacion();
		int daniel = -1;
		do {
			daniel = edad();
		} while (daniel <= 0);
		input.close();
		int luis = diviLuis(daniel);
		int carlos = edadCarlos(daniel, luis);
		int maria = edadMaria(daniel, luis);
		suma(daniel, luis, carlos, maria);
	}

	public static void presentacion() {
		System.out.println("===================");
		System.out.println("Adivino la edad de Luis, Carlos y Maria.");
		System.out.println("===================\n");
	}

	public static int edad() {
		System.out.println("�Que edad tiene Daniel?");
		int edadDaniel = input.nextInt();
		if (edadDaniel <= 0) {
			System.out.println("La edad no puede ser negativa ni cero.");
		}
		return edadDaniel;
	}

	public static int diviLuis(int num1) {
		int luis = num1 / 3;
		System.out.println("\nLa edad de Luis es " + luis + " a�os.");
		return luis;
	}

	public static int edadCarlos(int num1, int num2) {
		int suma = num1 + num2;
		System.out.println("\nLa edad de Carlos es: " + suma + " a�os.");
		return suma;
	}

	public static int edadMaria(int num1, int num2) {
		int maria = num1 - num2;
		System.out.println("\nLa edad de Maria es " + maria + " a�os.");
		return maria;
	}

	public static int suma(int num1, int num2, int num3, int num4) {
		int total = num1 + num2 + num3 + num4;
		System.out.println("\nLa suma total de su edades es: " + total + " a�os.");
		return total;
	}
}
