package juego02;

import java.util.Scanner;

public class Tragaperras {

	static final String CAMPANA = "Campana";
	static final String CORAZON = "Corazon";
	static final String DIAMANTE = "Diamante";
	static final String HERRADURA = "Herradura";
	static final String LIMON = "Limon";
	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		int num1;
		int num2;
		int num3;
		int monedas = 4;
		String opcion = "";

		presentacion();

		do {
			monedas -= 1;
			num1 = resultado();
			num2 = resultado();
			num3 = resultado();
			if (num1 == num2 && num1 == num3 && num2 == num3) {
				System.out.println("\n-------------------------------------------------");
				System.out.print(" | ");
				casilla1(num1);
				System.out.print(" |");
				casilla2(num2);
				System.out.print(" |");
				casilla3(num3);
				System.out.print(" |");
				System.out.println("\n-------------------------------------------------\n");
				System.out.println("�Enhorabuena, ha ganado el premio! Aqui tiene sus diez puntos.");
				monedas += 10;
			} else if ((num1 == num2 && num1 != num3 && num3 != num2)
					|| (num1 == num3 && num2 != num3 && num1 != num3)) {
				System.out.println("\n-------------------------------------------------");
				System.out.print(" |");
				casilla1(num1);
				System.out.print(" |");
				casilla2(num2);
				System.out.print(" |");
				casilla3(num3);
				System.out.println("\n-------------------------------------------------\n");
				System.out.println(
						"�Uy! Ha estado cerquita, aqui tiene su moneda de nuevo, en la siguiente tendra mas suerte.");
				monedas += 1;
			} else if (num1 != num2 && num1 != num3 && num2 != num3) {
				System.out.println("\n-------------------------------------------------");
				casilla1(num1);
				System.out.print(" |");
				casilla2(num2);
				System.out.print(" |");
				casilla3(num3);
				System.out.print(" |");
				System.out.println("\n-------------------------------------------------\n");
				System.out.println("Vaya, ha perdido... No se desanime, en la siguiente tendra mas suerte.");
			}
			if (monedas <= 0) {
				System.out.println("\nLo siento, se ha quedado sin monedas.\n");
				break;
			} else {
				System.out.println("\nLe quedan " + monedas + " monedas.\n");
			}
			System.out.println("�Quiere volver a lanzar la moneda?\nSI\nNO");
			opcion = program.nextLine();
		} while (!opcion.equalsIgnoreCase("No"));
		System.out.println("\nGracias por su visita.");

	}

	public static void presentacion() {
		System.out.println("Bienvenido a la maquina tragaperras.");
		System.out.println("Si consigue dos figuras iguales le devolveremos su dinero.");
		System.out.println("Si consigue tres figuras iguales, habra ganado el premio.");
		System.out.println("�Buena suerte!\n");
	}

	public static int resultado() {
		int numero = (int) (Math.round(Math.random() * 5));
		if (numero == 0) {
			numero += 1;
		}
		return numero;
	}

	public static void casilla1(int numero1) {
		if (numero1 == 1) {
			System.out.print(" | " + CAMPANA);
		} else if (numero1 == 2) {
			System.out.print(" | " + CORAZON);
		} else if (numero1 == 3) {
			System.out.print(" | " + DIAMANTE);
		} else if (numero1 == 4) {
			System.out.print(" | " + HERRADURA);
		} else if (numero1 == 5) {
			System.out.print(" | " + LIMON);
		}
	}

	public static void casilla2(int numero2) {
		if (numero2 == 1) {
			System.out.print(" | " + CAMPANA);
		} else if (numero2 == 2) {
			System.out.print(" | " + CORAZON);
		} else if (numero2 == 3) {
			System.out.print(" | " + DIAMANTE);
		} else if (numero2 == 4) {
			System.out.print(" | " + HERRADURA);
		} else if (numero2 == 5) {
			System.out.print(" | " + LIMON);
		}
	}

	public static void casilla3(int numero3) {
		if (numero3 == 1) {
			System.out.print(" | " + CAMPANA);
		} else if (numero3 == 2) {
			System.out.print(" | " + CORAZON);
		} else if (numero3 == 3) {
			System.out.print(" | " + DIAMANTE);
		} else if (numero3 == 4) {
			System.out.print(" | " + HERRADURA);
		} else if (numero3 == 5) {
			System.out.print(" | " + LIMON);
		}
	}

	public static void resultado1(int num1, int num2, int num3) {
		System.out.println("--------------------------------------");
		System.out.println("num1");
		System.out.println("Vaya, has perdido, no se desanime, a la siguiente tendra mas suerte...");
	}

}
