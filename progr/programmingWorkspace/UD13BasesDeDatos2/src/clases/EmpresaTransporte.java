package clases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class EmpresaTransporte implements Serializable {

	private static final long serialVersionUID = 1L;
	private static String datosTransporte = "src/datosTransporte.dat";
	private static String datosPersonal = "src/datosPersonal.dat";
	private Connection conexion;

	private ArrayList<Transporte> listaTransportes;
	private ArrayList<Personal> listaPersonal;

	public EmpresaTransporte() {
		listaTransportes = new ArrayList<Transporte>();
		listaPersonal = new ArrayList<Personal>();
	}

	public ArrayList<Transporte> getListaTransportes() {
		return listaTransportes;
	}

	public void setListaTransportes(ArrayList<Transporte> listaTransportes) {
		this.listaTransportes = listaTransportes;
	}

	public ArrayList<Personal> getListaPersonal() {
		return listaPersonal;
	}

	public void setListaPersonal(ArrayList<Personal> listaPersonal) {
		this.listaPersonal = listaPersonal;
	}

	/*
	 * #############################################################################
	 * ################################# FICHEROS ##################################
	 * #############################################################################
	 */

	private void guardarDatosTransporte() throws FileNotFoundException, IOException, NullPointerException {
		ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(datosTransporte));
		f.writeObject(listaTransportes);
		f.close();
	}

	private void guardarDatosPersonal() throws FileNotFoundException, IOException, NullPointerException {
		ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(datosPersonal));
		f.writeObject(listaPersonal);
		f.close();
	}

	public void guardarDatos() {
		try {
			guardarDatosTransporte();
			guardarDatosPersonal();
		} catch (NullPointerException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	@SuppressWarnings({ "unchecked" })
	private void cargarDatosTransporte() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream f = new ObjectInputStream(new FileInputStream(datosTransporte));
		listaTransportes = (ArrayList<Transporte>) f.readObject();
		f.close();
	}

	@SuppressWarnings("unchecked")
	private void cargarDatosPersonal() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream f = new ObjectInputStream(new FileInputStream(datosPersonal));
		listaPersonal = (ArrayList<Personal>) f.readObject();
		f.close();
	}

	public void cargarDatos() {
		try {
			cargarDatosTransporte();
			cargarDatosPersonal();
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	/*
	 * #############################################################################
	 * ################################# PERSONAL ##################################
	 * #############################################################################
	 */

	public void altaCarretillero(String dni, String nombre, double costeHora) {
		if (!existePersonal(dni)) {
			listaPersonal.add(new Carretillero(dni, nombre, costeHora));
			Collections.sort(listaPersonal);
		} else {
			System.out.println("Ya existe un carretillero con ese DNI.");
		}
	}

	public void altaConductor(String dni, String nombre, String carnet) {
		if (!existePersonal(dni)) {
			listaPersonal.add(new Conductor(dni, nombre, carnet));
			Collections.sort(listaPersonal);
		} else {
			System.out.println("Ya existe un conductor con ese DNI.");
		}
	}

	public boolean existePersonal(String dni) {
		for (Personal p : listaPersonal) {
			if (p.getDni() != null && p.getDni().equalsIgnoreCase(dni)) {
				return true;
			}
		}
		return false;
	}

	public Personal buscarPersonal(String dni) {
		for (Personal p : listaPersonal) {
			if (p.getDni() != null && p.getDni().equalsIgnoreCase(dni)) {
				return p;
			}
		}
		return null;
	}

	public void listarCarretilleros() {
		System.out.println("Carretilleros existentes:");
		for (Personal p : listaPersonal) {
			if (p != null && p instanceof Carretillero) {
				System.out.println(p);
			}
		}
	}

	public void listarConductores() {
		System.out.println("Conductores existentes:");
		for (Personal p : listaPersonal) {
			if (p != null && p instanceof Conductor) {
				System.out.println(p);
			}
		}
	}

	/*
	 * #############################################################################
	 * ################################ TRANSPORTE #################################
	 * #############################################################################
	 */

	public void altaTransporte(String codigo, String empresa, String fechaTransporte, String dni) {
		if (!existeTransporte(codigo)) {
			if (existePersonal(dni) && buscarPersonal(dni) instanceof Conductor) {
				listaTransportes.add(new Transporte(codigo, empresa, fechaTransporte, (Conductor) buscarPersonal(dni)));
				Collections.sort(listaTransportes);
			} else {
				System.out.println("El DNI proporcionado no existe o no corresponde a un conductor.");
			}
		} else {
			System.out.println("Ya existe un transporte con ese codigo registrado.");
		}
	}

	public boolean existeTransporte(String codigo) {
		for (Transporte t : listaTransportes) {
			if (t != null && t.getCodigo().equalsIgnoreCase(codigo)) {
				return true;
			}
		}
		return false;
	}

	public Transporte buscarTransporte(String codigo) {
		for (Transporte t : listaTransportes) {
			if (t != null && t.getCodigo().equalsIgnoreCase(codigo)) {
				return t;
			}
		}
		return null;
	}

	public void asignarCarretilleroTransporte(String codigo, String dni) {
		if (existeTransporte(codigo)) {
			if (existePersonal(dni) && buscarPersonal(dni) instanceof Carretillero) {
				buscarTransporte(codigo).getCarretilleros().add((Carretillero) buscarPersonal(dni));
			} else {
				System.out.println("El DNI proporcionado no existe o no corresponde a un carretillero.");
			}
		} else {
			System.out.println("No existe ningun transporte con el codigo indicado.");
		}
	}

	/*
	 * #############################################################################
	 * ############################## BASE DE DATOS ################################
	 * #############################################################################
	 */

	public void conectarBBDD() {
		String servidor = "jdbc:mysql://localhost:3306/transporte3ev";
		try {
			conexion = DriverManager.getConnection(servidor, "root", "");

		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void guardarConductoresBBDD(String carnet) {
		String query = "INSERT INTO conductores (dni, nombre, carnet) VALUES (?, ?, ?)";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			for (Personal p : listaPersonal) {
				if (p instanceof Conductor) {
					if (((Conductor) p).getCarnet().equalsIgnoreCase(carnet)) {
						sentencia.setString(1, p.getDni());
						sentencia.setString(2, p.getNombre());
						sentencia.setString(3, ((Conductor) p).getCarnet());
						sentencia.executeUpdate();
					}
				}
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void cargarConductoresBBDD() {
		String query = "SELECT * FROM conductores";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				System.out.println(
						resultado.getString(2) + ", " + resultado.getString(3) + ", " + resultado.getString(4) + ".");
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

}
