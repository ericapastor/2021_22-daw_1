package clases;

import java.io.Serializable;

public class Carretillero extends Personal implements Serializable {

	private static final long serialVersionUID = 1L;
	private double costeHora;

	public Carretillero(String dni, String nombre, double costeHora) {
		super(dni, nombre);
		this.costeHora = costeHora;
	}

	public double getCosteHora() {
		return costeHora;
	}

	public void setCosteHora(double costeHora) {
		this.costeHora = costeHora;
	}

	@Override
	public String toString() {
		return "Carretillero [costeHora=" + costeHora + ", toString()=" + super.toString() + "]";
	}

	@Override
	public int compareTo(Personal o) {
		return dni.compareTo(o.dni);
	}

}
