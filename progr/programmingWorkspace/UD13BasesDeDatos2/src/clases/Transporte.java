package clases;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

public class Transporte implements Comparable<Transporte>, Serializable {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private String empresa;
	private Date fechaTransporte;
	private Conductor conductor;
	private ArrayList<Carretillero> carretilleros;

	public Transporte(String codigo, String empresa, String fechaTransporte, Conductor conductor) {
		super();
		this.codigo = codigo;
		this.empresa = empresa;
		this.fechaTransporte = Date.valueOf(fechaTransporte);
		this.conductor = conductor;
		carretilleros = new ArrayList<Carretillero>();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Date getFechaTransporte() {
		return fechaTransporte;
	}

	public void setFechaTransporte(Date fechaTransporte) {
		this.fechaTransporte = fechaTransporte;
	}

	public Conductor getConductor() {
		return conductor;
	}

	public void setConductor(Conductor conductor) {
		this.conductor = conductor;
	}

	public ArrayList<Carretillero> getCarretilleros() {
		return carretilleros;
	}

	public void setCarretilleros(ArrayList<Carretillero> carretilleros) {
		this.carretilleros = carretilleros;
	}

	@Override
	public int compareTo(Transporte o) {
		return codigo.compareTo(o.codigo);
	}

}
