package programa;

import clases.EmpresaTransporte;

public class Programa {

	public static void main(String[] args) {
		System.out.println("Bienvenido a este programa de transporte.");
		System.out.println(
				"\n1. Creamos un objeto de EmpresaTransporte: \"EmpresaTransporte et = new EmpresaTransporte()\"");
		EmpresaTransporte et = new EmpresaTransporte();
		System.out.println("\n2. Conectamos con la base de datos");
		et.conectarBBDD();
		System.out.println("\n3. Mostramos los conductores existentes de la base de datos:");
		et.cargarConductoresBBDD();
		System.out.println("\n4. Damos de alta los carretilleros 000005, 000006, 000007 y 000008.");
		et.altaCarretillero("000005", "Rosa", 25);
		et.altaCarretillero("000006", "Luis", 30);
		et.altaCarretillero("000007", "Alex", 20);
		et.altaCarretillero("000008", "Samira", 35);
		System.out.println("Los listamos:");
		et.listarCarretilleros();
		System.out.println("\n5. Damos de alta los conductores 000009, 000010, 000011 y 000012.");
		et.altaConductor("000009", "Ana", "C1");
		et.altaConductor("000010", "Karen", "C2");
		et.altaConductor("000011", "Manuel", "C1");
		et.altaConductor("000012", "Timoteo", "C2");
		System.out.println("Los listamos:");
		et.listarConductores();
		System.out.println("\n6. Intentamos dar de alta un carretillero y un conductor con DNIs ya existentes:");
		et.altaCarretillero("000005", "Quiensea", 0);
		et.altaConductor("000009", "Quiensea", "C500");
		System.out.println("\n7. Damos de alta 2 transportes con codigo T1 y T2 respectivamente.");
		et.altaTransporte("T1", "empresa1", "2022-06-30", "000010");
		et.altaTransporte("T2", "empresa2", "2022-05-27", "000011");
		System.out.println("\n8. Intentamos de alta un transporte con un codigo anterior:");
		et.altaTransporte("T1", "empresa3", "2020-02-02", "000012");
		System.out.println("\n9. Intentamos dar de alta un transporte con DNI de carretillero:");
		et.altaTransporte("T3", "empresa3", "2020-02-02", "000008");
		System.out.println("\n10. Asignamos un carretillero a un transporte.");
		et.asignarCarretilleroTransporte("T1", "000005");
		System.out.println("\n11. Intentamos asignar un conductor a un transporte:");
		et.asignarCarretilleroTransporte("T1", "000009");
		System.out.println("\n12. Intentamos asignar un caretillero a un transporte que no existe:");
		et.asignarCarretilleroTransporte("T3", "000005");
		System.out.println("\n13. Guardamos datos en ficheros.");
		et.guardarDatos();
		System.out.println("\n14. Cargamos datos desde ficheros.");
		et.cargarDatos();
		System.out.println("\n15. Guardamos conductores con carnet C1.");
		et.guardarConductoresBBDD("C1");
		System.out.println("\n16. Cargamos de nuevo los conductores desde la base de datos:");
		et.cargarConductoresBBDD();
		System.out.println("\n17. Guardamos conductores con carnet C2.");
		et.guardarConductoresBBDD("C2");
		System.out.println("\n18. Mostramos de nuevo los conductores de la base de datos:");
		et.cargarConductoresBBDD();
	}

}
