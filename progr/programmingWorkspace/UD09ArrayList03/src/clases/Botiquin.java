package clases;

import java.util.ArrayList;

public class Botiquin {

	// atributos

	private String nombre;
	private String ubicacion;
	private ArrayList<Producto> productos = new ArrayList<Producto>();

	// constructores

	public Botiquin() {
		this.nombre = "";
		this.ubicacion = "";
	}

	public Botiquin(String nombre, String ubicacion, ArrayList<Producto> productos) {
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		this.productos = productos;
	}

	// getters&setters

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

	// toString

	@Override
	public String toString() {
		final int maxLen = 10;
		return "Botiquin [nombre=" + nombre + ", ubicacion=" + ubicacion + ", productos="
				+ (productos != null ? productos.subList(0, Math.min(productos.size(), maxLen)) : null) + "]";
	}

	// rellenar productos
	void rellenarBotiquin() {
		System.out.println("Introduzca el nombre del botiquin");
		this.setNombre(Producto.input.nextLine());
		System.out.println("Introduzca la ubicacion del botiquin");
		this.setUbicacion(Producto.input.nextLine());
		while (true) {
			Producto producto = new Producto();
			producto.rellenarProducto();
			if (producto.getNombre().equalsIgnoreCase("*")) {
				break;
			} else {
				this.productos.add(producto);
			}
		}
	}

}
