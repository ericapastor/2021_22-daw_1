package clases;

import java.util.ArrayList;

public class Colegio {

	// atributos

	private String nombre;
	private ArrayList<Botiquin> botiquines = new ArrayList<Botiquin>();

	// constructores

	public Colegio() {
		this.nombre = "";
	}

	public Colegio(String nombre, ArrayList<Botiquin> botiquines) {
		this.nombre = nombre;
		this.botiquines = botiquines;
	}

	// getters&setters

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Botiquin> getBotiquines() {
		return botiquines;
	}

	public void setBotiquines(ArrayList<Botiquin> botiquines) {
		this.botiquines = botiquines;
	}

	// toString

	@Override
	public String toString() {
		final int maxLen = 10;
		return "Colegio [nombre=" + nombre + ", botiquines="
				+ (botiquines != null ? botiquines.subList(0, Math.min(botiquines.size(), maxLen)) : null) + "]";
	}

	// rellenar colegio

	public void rellenarColegio() {
		System.out.println("Dime el nombre del colegio");
		this.setNombre(Producto.input.nextLine());
		while (true) {
			Botiquin botiquin = new Botiquin();
			botiquin.rellenarBotiquin();
			this.botiquines.add(botiquin);
			System.out.println("Quieres rellenar otro botiquin: si/no");
			String opcion = Producto.input.nextLine();
			if (opcion.equalsIgnoreCase("no")) {
				break;
			}
		}
	}

}
