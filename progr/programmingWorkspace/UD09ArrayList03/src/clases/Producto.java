package clases;

import java.util.Scanner;

public class Producto {

	public static Scanner input = new Scanner(System.in);

	// atributos

	private String nombre;
	private float precio;

	// constructores

	public Producto() {
		this.nombre = "";
		this.precio = 0.0F;
	}

	public Producto(String nombre, float precio) {
		this.nombre = nombre;
		this.precio = precio;
	}

	// getters&setters

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	// toString

	@Override
	public String toString() {
		return "Producto [nombre=" + nombre + ", precio=" + precio + "]";
	}

	// rellenar productos

	void rellenarProducto() {
		System.out.println("____________");
		System.out.println("Intoruzca el nombre del producto");
		String nombre = input.nextLine();
		this.setNombre(nombre);
		if (!nombre.equalsIgnoreCase("*")) {
			System.out.println("Introduzca el precio.");
			try {
				float precio = input.nextFloat();
				this.setPrecio(precio);
				input.nextLine();
			} catch (Exception InputMismatchException) {
				System.out.println("Se ha producido un error al introducir el precio.");
				float precio = input.nextFloat();
				this.setPrecio(precio);
				input.nextLine();
			}
		} else {
			System.out.println("Hemos terminado de rellenar este botiquin");
		}

	}
}
