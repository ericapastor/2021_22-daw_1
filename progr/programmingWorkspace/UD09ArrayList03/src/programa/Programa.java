package programa;

import java.util.ArrayList;

import clases.Colegio;

public class Programa {

	public static void main(String[] args) {
		menu();
	}

	public static void menu() {
		Colegio colegio = new Colegio();
		ArrayList<Colegio> colegios = new ArrayList<Colegio>();
		colegio.rellenarColegio();
		colegios.add(colegio);
		for (Colegio cole : colegios) {
			System.out.println(cole);
		}
	}

}
