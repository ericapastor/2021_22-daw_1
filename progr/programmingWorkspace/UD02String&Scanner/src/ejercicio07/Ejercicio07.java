package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {

		/*
		 * Programa que pide y lee un n�mero de 3 cifras y muestra cada cifra en una
		 * l�nea distinta. Primero vamos a leer el numero como si fuera una cadena de
		 * texto (String, m�todo nextLine()) y mostramos el resultado. Despu�s volvemos
		 * a pedir un n�mero pero los leemos como un int (m�todo nextInt()). Pista: para
		 * decomponer en cifras String -> m�todos de String, int -> divisi�n y resto de
		 * enteros.
		 */

		Scanner program = new Scanner(System.in);

		String number;
		int num;

		System.out.println("Type a three digit number.");
		number = program.nextLine();

		System.out.println(number.charAt(0));
		System.out.println(number.charAt(1));
		System.out.println(number.charAt(2));

		System.out.println("\n" + "Another one");

		num = program.nextInt();
		System.out.println(num / 100);
		System.out.println((num % 100) / 10);
		System.out.println(num % 10);

		program.close();

	}

}
