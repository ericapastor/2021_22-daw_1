package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {

		/*
		 * Programa lea la longitud de los catetos de un tri�ngulo rect�ngulo y calcule
		 * la longitud de la hipotenusa seg�n el teorema de Pit�goras. (Hipotenusa2 =
		 * cateto12 + cateto22 ) Es necesario usar el m�todo Math.sqrt() para calcular
		 * la ra�z cuadrada.
		 */

		Scanner program = new Scanner(System.in);

		double leg1;
		double leg2;
		double hypotenuse;

		System.out.println("Say side one");
		leg1 = program.nextDouble();

		System.out.println("Say side two");
		leg2 = program.nextDouble();

		hypotenuse = Math.sqrt((leg1 * leg1) + (leg2 * leg2));

		System.out.println("The hypotenuse is " + hypotenuse);

		program.close();

	}

}
