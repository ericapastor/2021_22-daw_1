package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {

		/*
		 * Programa que lea un n�mero entero de 5 cifras y muestre sus cifras desde el
		 * principio como en el ejemplo. Utiliza la divisi�n de enteros y posteriormente
		 * realiza lo mismo pero trat�ndolo como un String, y aplicando m�todos de
		 * String. 1 12 123 1234 12345
		 */

		Scanner program = new Scanner(System.in);

		int num;
		String number;

		System.out.println("Type a five digit number");
		num = program.nextInt();

		System.out.println(num / 10000);
		System.out.println(num / 1000);
		System.out.println(num / 100);
		System.out.println(num / 10);
		System.out.println(num);

		System.out.println("Type another five digit number");
		program.nextLine();
		number = program.nextLine();

		System.out.println(number.substring(0, 1)); // == number.charAt(0) el ultimo digito del substring no es incluido
		System.out.println(number.substring(0, 2));
		System.out.println(number.substring(0, 3));
		System.out.println(number.substring(0, 4));
		System.out.println(number); // == (number.substring(0, 4)+1)

		program.close();

	}

}
