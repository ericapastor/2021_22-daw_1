package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {

		/*
		 * Programa Java que lee un n�mero entero por teclado y obtiene y muestra por
		 * pantalla el doble y el triple de ese n�mero
		 */

		Scanner program = new Scanner(System.in);

		int number;
		System.out.println("Type a number");

		number = program.nextInt();

		System.out.println("Double is: " + (number *= 2));

		number /= 2;

		System.out.println("Triple is: " + (number *= 3));

		program.close();

	}
}
