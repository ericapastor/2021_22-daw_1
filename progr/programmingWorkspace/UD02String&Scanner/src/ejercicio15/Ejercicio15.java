package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {

		/*
		 * Programa que me pide una cadena por teclado, compuesta de dos palabras
		 * separadas por un espacio, y me muestra por consola la misma cadena, pero
		 * mostrando primero la segunda palabra y luego la primera, separadas por un
		 * espacio. Pista, hay un m�todo que me indica la posici�n (n�mero) de un
		 * car�cter en una cadena de texto. Necesito encontrar el car�cter � � (espacio)
		 */

		Scanner program = new Scanner(System.in);

		String chain;
		int space;

		System.out.println("Type a sentece of 2 words.");
		chain = program.nextLine();
		space = chain.indexOf(' ');

		System.out.println(chain.substring(space+1) + " " + chain.substring(0, space+2));

		program.close();

	}

}
