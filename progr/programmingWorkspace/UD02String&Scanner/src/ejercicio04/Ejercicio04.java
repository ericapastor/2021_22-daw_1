package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {

		/*
		 * Programa que lee por teclado el valor del radio de una circunferencia y
		 * calcula y muestra por pantalla la longitud y el �rea de la circunferencia.
		 * Longitud de la circunferencia = 2*PI*Radio, �rea de la circunferencia =
		 * PI*Radio2
		 */

		Scanner program = new Scanner(System.in);

		double radius;
		double length;
		double area;

		System.out.println("Type the radius.");

		radius = program.nextDouble();

		length = 2 * Math.PI * radius;
		area = Math.PI * radius * radius;

		System.out.println("For " + radius + "cm of radius, the length of the circumference is " + length
				+ "cm, and the area is " + area + "cm2.");

		program.close();

	}

}
