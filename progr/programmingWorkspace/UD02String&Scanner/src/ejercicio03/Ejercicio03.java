package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {

		/*
		 * Programa que pida la fecha de nacimiento. El programa pedir� primero el
		 * n�mero, luego el mes escrito (Enero, Febrero, etc.) y finalmente el a�o en
		 * n�mero. Debe mostrar la fecha completa en una sola l�nea con formato:
		 * dia/mes/a�o. Al leer el mes quiz�s tengamos un problema -> Revisar apuntes
		 * sobre vaciar el buffer de lectura, en la clase Scanner.
		 */

		Scanner program = new Scanner(System.in);

		int day;
		String month;
		int year;

		System.out.println("Input day of birth");
		day = program.nextInt();

		System.out.println("Input month, in words please");
		program.nextLine();
		month = program.nextLine();

		System.out.println("Input year");
		year = program.nextInt();

		System.out.println("Date of birth is: " + day + "/" + month + "/" + year + ".");

		program.close();

	}

}
