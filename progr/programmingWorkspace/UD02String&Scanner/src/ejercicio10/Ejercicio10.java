package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {

		/*
		 * Programa que pida por teclado la fecha de nacimiento de una persona (d�a,
		 * mes, a�o) y calcule su n�mero de la suerte. La fecha se pide de forma
		 * separada, primero d�as, luego meses, etc. El n�mero de la suerte se calcula
		 * sumando el d�a, mes y a�o de la fecha de nacimiento y a continuaci�n sumando
		 * las cifras obtenidas en la suma. Si la fecha de nacimiento es 12/07/1980,
		 * calculamos el n�mero de la suerte as�: 12+7+1980 = 1999 -> 1+9+9+9 = 28
		 */

		Scanner program = new Scanner(System.in);

		int day;
		int month;
		int year;
		int number;

		System.out.println("Day of birth:");
		day = program.nextInt();

		System.out.println("Month of birth:");
		month = program.nextInt();

		System.out.println("Year of birth:");
		year = program.nextInt();

		number = day + month + year;
		number = ((number / 1000) + ((number % 1000) / 100) + ((number % 100) / 10) + (number % 10));

		System.out.println("Your lucky number is: " + number + ".");

		program.close();

	}
}
