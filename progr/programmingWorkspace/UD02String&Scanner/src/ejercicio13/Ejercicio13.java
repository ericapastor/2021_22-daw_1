package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {

		/*- Programa que lee dos cadenas por teclado y me dice si la primera est� contenida en la
		segunda. (Ej: la cadena �hola papa� contiene la cadena �papa�)
		*/

		Scanner program = new Scanner(System.in);

		String chain1;
		String chain2;

		System.out.println("Type a sentence or word");
		chain1 = program.nextLine();

		System.out.println("Type yet another sentence or word");
		chain2 = program.nextLine();

		System.out.println(chain1.contains(chain2) ? "First chain contains second chain."
				: (chain2.contains(chain1) ? "Second chain contains first chain."
						: "None of the chains contain the oher"));

		program.close();

	}

}
