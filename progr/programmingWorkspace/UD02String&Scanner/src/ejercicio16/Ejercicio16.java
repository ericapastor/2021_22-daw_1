package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {

		/*- Programa que lee una cadena de texto por teclado y me muestra la segunda mitad de
		la cadena. Por ejemplo si una cadena contiene 15 caracteres, me mostrar� los 7 �ltimos.*/

		Scanner program = new Scanner(System.in);

		String chain;
		int halfN;

		System.out.println("Type a sentence.");
		chain = program.nextLine();
		halfN = chain.length() / 2;

		System.out.println(chain.substring(halfN));

		program.close();

	}

}
