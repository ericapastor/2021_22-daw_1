package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {

		/*
		 * Programa que lee una cadena de texto y posteriormente lee un car�cter, y me
		 * dice si la cadena comienza por ese car�cter.
		 */

		Scanner program = new Scanner(System.in);

		String chain;
		String charac;
		boolean startsW;

		System.out.println("Type a sentence");
		chain = program.nextLine();

		System.out.println("Now type a character");
		charac = program.nextLine();

		startsW = chain.startsWith(charac);

		System.out.println(startsW ? "The chain starts with that character" : "Conditions are not met");

		// it'd be the same to say:

		// startsW = chain==chain.charAt(0);

		program.close();

	}

}
