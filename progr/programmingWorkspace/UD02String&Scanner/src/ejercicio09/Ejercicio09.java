package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {

		/*
		 * Programa que lea un n�mero entero de 5 cifras y muestre sus cifras desde el
		 * final igual que en el ejemplo. Utiliza la divisi�n de enteros. 5 45 345 2345
		 * 12345
		 */
		Scanner program = new Scanner(System.in);

		int num;
		String number;

		System.out.println("Type a five digit number");
		num = program.nextInt();

		System.out.println(num % 10);
		System.out.println(num % 100);
		System.out.println(num % 1000);
		System.out.println(num % 10000);
		System.out.println(num);

		program.nextLine();
		System.out.println("Type yet another five digit number");
		number = program.nextLine();

		System.out.println(number.substring(4)); // == number.charAt(4)
		System.out.println(number.substring(3));
		System.out.println(number.substring(2));
		System.out.println(number.substring(1));
		System.out.println(number);

		program.close();

	}

}
