package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {

		/*
		 * Programa que pide y lee 2 cadenas de texto por teclado y me dice si son
		 * iguales o no lo son (Operador ?).
		 */

		Scanner program = new Scanner(System.in);

		String chain1;
		String chain2;

		System.out.println("Give me a sentence");
		chain1 = program.nextLine();

		System.out.println("Give me another sentence");
		chain2 = program.nextLine();

		System.out.println((chain1.equals(chain2) ? "Sentences are the same" : "Sentences are not the same"));

		program.close();

	}

}
