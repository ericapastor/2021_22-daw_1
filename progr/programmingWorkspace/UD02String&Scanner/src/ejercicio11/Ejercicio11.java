package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {

		// Programa que lee una cadena por teclado y me indica lo siguiente mediante 3
		// mensajes diferentes: (Operador ? y M�todos clase String)
		// Si tiene alguna vocal o no la tiene
		// Si comienza por vocal o no.
		// Si termina con vocal o no.

		Scanner program = new Scanner(System.in);

		String chain;
		boolean startsV;
		boolean endsV;
		boolean containsV;

		System.out.println("Type a String");
		chain = program.nextLine();
		startsV = chain.startsWith("a") || chain.startsWith("e") || chain.startsWith("i") || chain.startsWith("o")
				|| chain.startsWith("u") || chain.startsWith("A") || chain.startsWith("E") || chain.startsWith("I")
				|| chain.startsWith("O") || chain.startsWith("U");
		endsV = chain.endsWith("a") || chain.endsWith("e") || chain.endsWith("i") || chain.endsWith("o")
				|| chain.endsWith("u") || chain.endsWith("A") || chain.endsWith("E") || chain.endsWith("I")
				|| chain.endsWith("O") || chain.endsWith("U");
		containsV = chain.contains("a") || chain.contains("e") || chain.contains("i") || chain.contains("o")
				|| chain.contains("u") || chain.contains("A") || chain.contains("E") || chain.contains("I")
				|| chain.contains("O") || chain.contains("U");
		
		System.out.println(startsV?"Starts with a vowel":"It does not start with a vowel");
		System.out.println(endsV?"Ends with a vowel":"It does not end with a vowel");
		System.out.println(containsV?"Contains a vowel":"It does not contain with a vowel");

		program.close();

	}

}
