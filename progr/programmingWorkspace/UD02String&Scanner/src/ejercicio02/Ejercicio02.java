package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {

		/*
		 * Programa que lee una cadena compuesta de varias palabras por teclado y la
		 * muestra quit�ndole los espacios. Emplear m�todos de String (reemplazar
		 * caracteres).
		 */

		Scanner program = new Scanner(System.in);

		String chain;

		System.out.println("Type a chain of words");

		chain = program.nextLine();
		chain = chain.replaceAll(" ", "");

		System.out.println(chain);

		program.close();

	}
}
