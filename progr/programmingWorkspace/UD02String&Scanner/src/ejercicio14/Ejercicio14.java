package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {

		/*- Programa que lee una cadena por teclado y posteriormente nos pregunta cuantos
		caracteres del inicio de la cadena queremos mostrar. Despu�s el programa muestra el
		n�mero de caracteres indicado con los que comienza la cadena.*/

		Scanner program = new Scanner(System.in);

		String chain;
		int num;

		System.out.println("Type a sentence");
		chain = program.nextLine();

		System.out.println("How many characters would you wish to show?");
		num = program.nextInt();

		System.out.println(chain.substring(0, num));

		program.close();
	}

}
