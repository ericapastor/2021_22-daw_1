package ejerciciobasedatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TrabajandoConDatos {

	private Connection con = null;
	PreparedStatement sen = null;

	public void conectar() {
		try {
			String servidor = "jdbc:mysql://localhost:3306/";
			String bbdd = "videojuegos";
			String user = "root";
			String password = "";
			con = DriverManager.getConnection(servidor + bbdd, user, password);
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		}
	}

	public void seleccionar() {
		String sentenciaSql = "SELECT * FROM videojuegos";
		try {
			// preparo la sentencia
			sen = con.prepareStatement(sentenciaSql);
			// ejecuto la query
			ResultSet resultado = sen.executeQuery();
			// mostramos los datos
			while (resultado.next()) {
				System.out.println(resultado.getString(1) + ", " + resultado.getString(2) + ", "
						+ resultado.getString(3) + ", " + resultado.getString(4) + ", " + resultado.getFloat(5));
			}
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		} catch (NullPointerException e) {
			System.out.println("La base no existe o no se ha podido conectar con ella.");
		} finally {
			// nos aseguramos que cerramos los recursos
			if (sen != null) {
				try {
					sen.close();
				} catch (SQLException e) {
					System.out.println("No se ha podido inicializar la sentencia.");
				}
			}
		}
	}

	public void insertar(String nombre, String plataforma, String genero, float precio) {
		try {
			// cuando vamos a insertar debemos de decirle cuales son (parametros)
			// vamos a poner interrogaciones tantas datos vaya a darle
			String sentenciaSql = "INSERT INTO videojuegos(nombre, plataforma, genero, precio)" + "values(?,?,?,?)";
			PreparedStatement sentencia;
			sentencia = con.prepareStatement(sentenciaSql);
			sentencia.setString(1, nombre);
			sentencia.setString(2, plataforma);
			sentencia.setString(3, genero);
			sentencia.setFloat(4, precio);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		} catch (NullPointerException e) {
			System.out.println("La base no existe o no se ha podido conectar con ella.");
		} finally {
			// nos aseguramos que cerramos los recursos
			if (sen != null) {
				try {
					sen.close();
				} catch (SQLException e) {
					System.out.println("No se ha podido inicializar la sentencia.");
				}
			}
		}
	}

	public void actualizar(String nombre, String plataforma, String genero, float precio) {
		try {
			String sentenciaSql = "UPDATE videojuegos set plataforma=?,genero=?,precio=? WHERE nombre=?";
			PreparedStatement sentencia;
			sentencia = con.prepareStatement(sentenciaSql);
			sentencia.setString(1, plataforma);
			sentencia.setString(2, genero);
			sentencia.setFloat(3, precio);
			sentencia.setString(4, nombre);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		} catch (NullPointerException e) {
			System.out.println("La base no existe o no se ha podido conectar con ella.");
		} finally {
			// nos aseguramos que cerramos los recursos
			if (sen != null) {
				try {
					sen.close();
				} catch (SQLException e) {
					System.out.println("No se ha podido inicializar la sentencia.");
				}
			}
		}
	}

	public void eliminar(String nombre) {
		try {
			String sentenciaSql = "DELETE FROM videojuegos WHERE nombre=?";
			PreparedStatement sentencia;
			sentencia = con.prepareStatement(sentenciaSql);
			sentencia.setString(1, nombre);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error de conexion con la base de datos.");
		} catch (NullPointerException e) {
			System.out.println("La base no existe o no se ha podido conectar con ella.");
		} finally {
			// nos aseguramos que cerramos los recursos
			if (sen != null) {
				try {
					sen.close();
				} catch (SQLException e) {
					System.out.println("No se ha podido inicializar la sentencia.");
				}
			}
		}
	}

	public void desconectar() {
		try {
			sen.close();
		} catch (SQLException e) {
			System.out.println("Error al cerrar la base de datos.");
		} catch (NullPointerException e) {
			System.out.println("La sentencia no se ha podido cerrar.");
		}
	}
}
