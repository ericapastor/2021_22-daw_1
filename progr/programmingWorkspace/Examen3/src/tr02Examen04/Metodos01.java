package tr02Examen04;

public class Metodos01 {
	
	public static void mostrarMenu() {
		System.out.println("\nSelecciona una opcion: ");
		System.out.println("1 - Contar vocales.");
		System.out.println("2 - Invertir vector.");
		System.out.println("3 - Visualizar palarbras.");
		System.out.println("4 - Salir.");
	}

	public static void contarVocales(String[] array) {
		int count;
		for (int i = 0; i < array.length; i++) {
			count = 0;
			for (int j = 0; j < array[i].length(); j++) {
				switch (array[i].toLowerCase().charAt(j)) {
				case 'a':
				case 'e':
				case 'i':
				case 'o':
				case 'u':
					count++;
					break;
				default:
					break;

				}
			}
			System.out.println("La frase " + array[i] + " tiene " + count + " vocal(es).");
		}
	}

	public static void invertirVector(String[] array) {
		String[] invertido = new String[5];
		for (int i = 0, j = array.length - 1; i < array.length; i++, j--) {
			invertido[j] = array[i];
		}
		System.out.println("Nuevo orden de las frases:");
		for (int i = 0; i < array.length; i++) {
			System.out.println("Frase " + (i + 1) + ":");
			System.out.println(invertido[i]);
		}
	}

	public static void visualizarPalabras(String[] array, int n) {
		n -= 1;
		String[] palabras = array[n].split(" ");
		System.out.println("Las palabras de la frase " + (n + 1) + " son:");
		for (int i = 0; i < palabras.length; i++) {
			System.out.println("Palabra " + (i + 1) + ": " + palabras[i]);
		}
	}

}
