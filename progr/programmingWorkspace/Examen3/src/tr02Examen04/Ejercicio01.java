package tr02Examen04;

import java.util.Scanner;

public class Ejercicio01 {

	public static final String FIN = "Fin del programa, hasta luego.";

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String[] frases = new String[5];
		frases = rellenarVector();
		int option;
		do {
			Metodos01.mostrarMenu();
			option = input.nextInt();
			input.nextLine();
			switch (option) {
			case 1:
				Metodos01.contarVocales(frases);
				break;
			case 2:
				Metodos01.invertirVector(frases);
				break;
			case 3:
				System.out.println("De que componente deseas visualizar las palabras?");
				int n = input.nextInt();
				Metodos01.visualizarPalabras(frases, n);
				break;
			case 4:
				System.out.println(FIN);
				break;
			default:
				System.out.println("Opcion no contemplada.");
				break;
			}
		} while (option != 4);

		input.close();
	}

	static String[] rellenarVector() {
		String[] array = new String[5];
		for (int i = 0; i < array.length; i++) {
			System.out.println("Dame la frase del componente " + (i + 1));
			array[i] = input.nextLine();
		}
		return array;
	}

}
