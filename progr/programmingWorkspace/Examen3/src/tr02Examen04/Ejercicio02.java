package tr02Examen04;

import java.util.Scanner;

public class Ejercicio02 {

	/*
	 * Creo que el programa es totalmente funcional, pero no se por que me devuelve
	 * ceros, osea no lee los doubles.
	 */

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		double[] precios = new double[5];
		precios = rellenarVector();
		int option;
		do {
			Metodos02.mostrarMenu();
			option = program.nextInt();
			program.nextLine();
			switch (option) {
			case 1:
				Metodos02.visualizarPreciosBajos(precios);
				break;
			case 2:
				Metodos02.ordenarPrecios(precios);
				break;
			case 3:
				Metodos02.visualizarPrecioMinimoPar(precios);
				break;
			case 4:
				System.out.println(Ejercicio01.FIN);
				break;
			default:
				System.out.println("Opcion no contemplada.");
				break;
			}
		} while (option != 4);
		program.close();

	}

	public static double[] rellenarVector() {
		double[] array = new double[5];
		for (int i = 0; i < array.length; i++) {
			System.out.println("Dame el precio " + (i + 1) + ":");
			array[i] = program.nextDouble();
		}
		return array;
	}

}
