
package tr02Examen04;

public class Metodos02 {

	public static void mostrarMenu() {
		System.out.println("\nMenu, elige una de estas opciones:");
		System.out.println("1 - Visualizar precios bajos de ant�genos.");
		System.out.println("2 - Ordenar precios de ant�genos de menor a mayor.");
		System.out.println("3 - Visualizar precio m�nimo par.");
		System.out.println("4 - Salir.");
	}

	public static void visualizarPreciosBajos(double[] array) {
		System.out.println("Precios inferiores a 2 euros: ");
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] < 2.00) {
				System.out.println(" - " + array[i] + ".");
				count++;
			}
		}
		if (count == 0) {
			System.out.println("No habia ningun precio menor que dos euros.");
		}
	}

	public static void ordenarPrecios(double[] array) {
		double[] ordenado = new double[array.length];
		System.out.println("Vector sin ordenar: ");
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.println("Vector ordenado: ");
		// devuelve ceros
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				if (array[j] < array[j]) {
					ordenado[j] = array[j];
				}
			}
			System.out.println(ordenado[i]);
		}
	}

	public static void visualizarPrecioMinimoPar(double[] array) {
		int[] enteros = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			enteros[i] = (int) Math.round(array[i]);
		}
		System.out.println("Vector de enteros: ");
		int menorPar = 9999;
		int posicion = 0;
		for (int i = 0; i < enteros.length; i++) {
			System.out.println(enteros[i]);
			if (enteros[i] % 2 == 0) {
				if (enteros[i] < menorPar) {
					posicion = i;
					menorPar = enteros[i];
				}
			}
		}
		System.out.println("El menor numero par es " + menorPar + ".");
		System.out.println("Su posicion es [" + posicion + "].");
	}
}
