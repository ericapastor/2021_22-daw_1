package programa;

import clases.Festival;

public class Programa {

	public static void main(String[] args) {

		// crear objeto festival
		Festival festival = new Festival();
		// cargar datos de ficheros
		// festival.cargarDatosFicheros();
		// conectar con la base de datos
		festival.conectarBBDD();
		// dar de alta artistas
		festival.altaArtista("12345678R", "Pepito", "Rock", "9");
		festival.altaArtista("0987654W", "Grillo", "Indie", "6");
		festival.altaArtista("34512378P", "Juana", "Blues", "8");
		// mostrar los artistas
		festival.listarArtistas();
		// dar de alta asistentes
		festival.altaAsistente("45667812L", "Luis", "1996-05-23", "colombiana");
		festival.altaAsistente("12312312Z", "Helena", "1994-12-07", "americana");
		festival.altaAsistente("84019564T", "Garub", "1990-09-15", "filipina");
		festival.listarAsistentes();
		// dar de alta conciertos
		System.out.println("");
		festival.altaConcierto("C001", "Viva La Pepa", "17:00:00", "12345678R");
		festival.altaConcierto("C002", "Kim Possible", "19:00:00", "0987654W");
		festival.altaConcierto("C003", "Jaja xd salu2s", "11:00:00", "34512378P");
		// registrar asistentes en un concierto
		festival.registrarAsistenteConcierto("C001", "45667812L");
		festival.registrarAsistenteConcierto("C002", "12312312Z");
		festival.registrarAsistenteConcierto("C003", "84019564T");
		System.out.println("Mostrar conciertos");
		festival.listarConciertos();
		// compruebo metodos
		System.out.println("Artista con dni ya existente");
		festival.altaArtista("12345678R", "Pepito", "Rock", "9");
		System.out.println("Asistente con dni ya existente");
		festival.altaAsistente("45667812L", "Luis", "1996-05-23", "colombiana");
		System.out.println("Concierto con codigo ya existente");
		festival.altaConcierto("C001", "Viva la Pepa", "17:00", "13563476354");
		System.out.println("Concierto con dni no existente");
		festival.altaConcierto("C004", "Viva la Pepa", "17:00", "13563476354");
		System.out.println("Concierto con dni de un asistente");
		festival.altaConcierto("C004", "Viva la Pepa", "17:00", "45667812L");
		System.out.println("Registrar en un concierto que no existe");
		festival.registrarAsistenteConcierto("C004", "13563476354");
		System.out.println("Registrar asistente que no existe");
		festival.registrarAsistenteConcierto("C003", "13563476354");
		System.out.println("Registrar un artista en un concierto");
		festival.registrarAsistenteConcierto("C003", "12345678R");
		// guardar en base de datos
		System.out.println("Guardo en la base de datos");
		festival.guardarArtistasBBDD();
		System.out.println("Cargo artistas de la base de datos");
		festival.cargarArtistasBBDD();
		// guardar datos en ficheros
		festival.guardarDatosFicheros();
	}

}
