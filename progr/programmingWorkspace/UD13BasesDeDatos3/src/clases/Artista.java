package clases;

import java.io.Serializable;

public class Artista extends Personal implements Serializable {
	private static final long serialVersionUID = 1L;

	private String estilo;
	private Float cache;

	public Artista(String dni, String nombre, String estilo, Float cache) {
		super(dni, nombre);
		this.estilo = estilo;
		this.cache = cache;
	}

	@Override
	public int compareTo(Personal o) {
		return dni.compareTo(o.dni);
	}

	@Override
	public String toString() {
		return "Artista [estilo=" + estilo + ", cache=" + cache + super.toString() + "]";
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public Float getCache() {
		return cache;
	}

	public void setCache(Float cache) {
		this.cache = cache;
	}

}
