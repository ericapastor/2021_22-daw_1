package clases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class Festival implements Serializable {
	private static final long serialVersionUID = 1L;

	private Connection conexion;
	private final String datosPersonal = "src/datosPersonal.dat";
	private final String datosConciertos = "src/datosConciertos.dat";

	private ArrayList<Personal> personal;
	private ArrayList<Concierto> conciertos;

	public Festival() {
		personal = new ArrayList<Personal>();
		conciertos = new ArrayList<Concierto>();
	}

	public ArrayList<Personal> getPersonal() {
		return personal;
	}

	public void setPersonal(ArrayList<Personal> personal) {
		this.personal = personal;
	}

	public ArrayList<Concierto> getConciertos() {
		return conciertos;
	}

	public void setConciertos(ArrayList<Concierto> conciertos) {
		this.conciertos = conciertos;
	}

	// ficheros

	private void guardarFicheroPersonal() throws FileNotFoundException, IOException {
		ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(datosPersonal));
		escritor.writeObject(personal);
		escritor.close();
	}

	private void guardarFicheroConciertos() throws FileNotFoundException, IOException {
		ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(datosConciertos));
		escritor.writeObject(conciertos);
		escritor.close();
	}

	public void guardarDatosFicheros() {
		try {
			guardarFicheroPersonal();
			guardarFicheroConciertos();
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	private void cargarFicheroPersonal() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream lector = new ObjectInputStream(new FileInputStream(datosPersonal));
		personal = (ArrayList<Personal>) lector.readObject();
		lector.close();
	}

	@SuppressWarnings("unchecked")
	private void cargarFicheroConciertos() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream lector = new ObjectInputStream(new FileInputStream(datosConciertos));
		conciertos = (ArrayList<Concierto>) lector.readObject();
		lector.close();
	}

	public void cargarDatosFicheros() {
		try {
			cargarFicheroPersonal();
			cargarFicheroConciertos();
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	// base de datos

	// conectar

	public void conectarBBDD() {
		String server = "jdbc:mysql://localhost:3306/festival3ev";
		try {
			conexion = DriverManager.getConnection(server);
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	// guardar artistas en tabla artistas

	public void guardarArtistasBBDD() {
		String query = "INSERT INTO artistas VALUES (?,?,?,?)";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			for (Personal p : personal) {
				if (p != null && p instanceof Artista) {
					sentencia.setString(1, p.getDni());
					sentencia.setString(2, p.getNombre());
					sentencia.setString(3, ((Artista) p).getEstilo());
					sentencia.setFloat(4, ((Artista) p).getCache());
					sentencia.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	// cargar artistas de la tabla artistas

	public void cargarArtistasBBDD() {
		String query = "SELECT * FROM artistas";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				System.out.println("DNI: " + resultado.getString(1) + ", nombre: " + resultado.getString(2)
						+ ", estilo: " + resultado.getString(3) + ", cache: " + resultado.getFloat(4) + ".");
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	// cargar asistentes de la tabla asistentes

	public void cargarAsistentesBBDD() {
		String query = "SELECT * FROM asistentes;";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				System.out.println("DNI: " + resultado.getString(1) + ", nombre: " + resultado.getString(2)
						+ ", fecha de nacimiento: " + resultado.getString(3) + ", nacionalidad: "
						+ resultado.getString(4) + ".");
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	// personal alta

	public void altaAsistente(String dni, String nombre, String fechaNacimiento, String nacionalidad) {
		if (buscarPersonal(dni) != null) {
			System.out.println("Ya existe alguien con ese dni");
		} else {
			personal.add(new Asistente(dni, nombre, fechaNacimiento, nacionalidad));
			Collections.sort(personal);
		}
	}

	public void altaArtista(String dni, String nombre, String estilo, String cache) {
		if (buscarPersonal(dni) != null) {
			System.out.println("Ya existe alguien con ese dni");
		} else {
			personal.add(new Artista(dni, nombre, estilo, Float.valueOf(cache)));
			Collections.sort(personal);
		}
	}

	// personal listar

	public void listarAsistentes() {
		for (Personal p : personal) {
			if (p != null && p instanceof Asistente) {
				System.out.println(p);
			}
		}
	}

	public void listarArtistas() {
		for (Personal p : personal) {
			if (p != null && p instanceof Artista) {
				System.out.println(p);
			}
		}
	}

	// concierto alta

	public void altaConcierto(String codigo, String nombre, String horaInicio, String dni) {
		if (buscarConcierto(codigo) != null) {
			System.out.println("Ya existe un concierto con ese codigo");
		} else if (buscarPersonal(dni) == null) {
			System.out.println("No hay nadie con ese dni");
		} else if (!(buscarPersonal(dni) instanceof Artista)) {
			System.out.println("El dni introducido tiene que ser de un artista");
		} else {
			conciertos.add(new Concierto(codigo, nombre, horaInicio, (Artista) buscarPersonal(dni)));
			Collections.sort(conciertos);
		}
	}

	// concierto listar

	public void listarConciertos() {
		for (Concierto c : conciertos) {
			if (c != null) {
				System.out.println(c);
			}
		}
	}

	// registrar asistentes en concierto

	public void registrarAsistenteConcierto(String codigo, String dni) {
		if (buscarConcierto(codigo) == null) {
			System.out.println("No existe ningun concierto con ese codigo");
		} else if (buscarPersonal(dni) == null) {
			System.out.println("No existe nadie registrado con ese dni");
		} else if (!(buscarPersonal(dni) instanceof Asistente)) {
			System.out.println("El dni proporcionado no es de un asistente");
		} else {
			buscarConcierto(codigo).getAsistentes().add((Asistente) buscarPersonal(dni));
			Collections.sort(buscarConcierto(codigo).getAsistentes());
		}
	}

	// buscar

	private Personal buscarPersonal(String dni) {
		for (Personal p : personal) {
			if (p.getDni() != null && p.getDni().equalsIgnoreCase(dni)) {
				return p;
			}
		}
		return null;
	}

	private Concierto buscarConcierto(String codigo) {
		for (Concierto c : conciertos) {
			if (c.getCodigo() != null && c.getCodigo().equalsIgnoreCase(codigo)) {
				return c;
			}
		}
		return null;
	}

}
