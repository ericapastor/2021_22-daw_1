package enums;

/**
 * @author erica
 *
 */

public enum Descuento {
	NINGUNO, PRIMAVERA, NAVIDAD, SAN_VALENTIN
}
