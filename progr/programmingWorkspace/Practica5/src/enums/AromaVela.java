package enums;

/**
 * @author erica
 *
 */

public enum AromaVela {
	NINGUNO, VAINILLA, LIMON, FRUTOS_ROJOS, CANELA
}
