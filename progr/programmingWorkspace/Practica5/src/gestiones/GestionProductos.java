package gestiones;

import java.util.ArrayList;
import java.util.Iterator;

import enums.AromaVela;
import productos.Baraja;
import productos.Cristal;
import productos.Vela;
import programa.Pintar;
import programa.Programa;

/**
 * La clase GestionProductos tiene como atributos ArrayLists de productos. Esta
 * en un paquete diferente porque si en un futuro quiero crear mas clases de
 * gestiones, iran a este paquete.
 * 
 * Tiene un solo constructor vacio.
 * 
 * Tiene setters y getters.
 * 
 * El toString llama a metodos de abajo.
 * 
 * Tiene metodos de gestion: altas, bajas, modificaciones, visualizaciones y
 * busquedas.
 * 
 * @author erica
 * 
 * @see public final class Baraja
 * @see public final class Vela
 * @see public final class Cristal
 *
 */

public class GestionProductos {

	private ArrayList<Baraja> barajas = new ArrayList<Baraja>();
	private ArrayList<Vela> velas = new ArrayList<Vela>();
	private ArrayList<Cristal> cristales = new ArrayList<Cristal>();

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Un solo constructor vacio
	 */
	public GestionProductos() {
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return ArrayList<Baraja> barajas
	 */
	public ArrayList<Baraja> getBarajas() {
		return barajas;
	}

	/**
	 * @param ArrayList<Baraja> barajas
	 */
	public void setBarajas(ArrayList<Baraja> barajas) {
		this.barajas = barajas;
	}

	/**
	 * @return ArrayList<Vela> velas
	 */
	public ArrayList<Vela> getVelas() {
		return velas;
	}

	/**
	 * @param ArrayList<Vela> velas
	 */
	public void setVelas(ArrayList<Vela> velas) {
		this.velas = velas;
	}

	/**
	 * @return ArrayList<Cristal> cristales
	 */
	public ArrayList<Cristal> getCristales() {
		return cristales;
	}

	/**
	 * @param ArrayList<Cristal> cristales
	 */
	public void setCristales(ArrayList<Cristal> cristales) {
		this.cristales = cristales;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	/**
	 * toString
	 * 
	 * @see this.listarBarajas()
	 * @see this.listarVelas()
	 * @see this.listarCristales()
	 */
	@Override
	public String toString() {
		System.out.println("|           **********************************");
		System.out.println("|           BARAJAS:\n|");
		System.out.println("|           **********************************");
		this.listarBarajas();
		System.out.println("|           **********************************");
		System.out.println("|           VELAS:\n|");
		System.out.println("|           **********************************");
		this.listarVelas();
		System.out.println("|           **********************************");
		System.out.println("|           CRISTALES:\n|");
		System.out.println("|           **********************************");
		this.listarCristales();
		return "|           **********************\n|\n|           Esos son todos nuestros productos.";
	}

	/**
	 * No recibe ni devuelve nada. Da de alta las barajas ya escritas a mano usando
	 * el metodo altaProducto sobreescito en la clase Baraja. Define el tipo de
	 * producto (enum) a baraja.
	 * 
	 * @see Baraja.altaProducto()
	 * @see Baraja.controlContadorProductos()
	 */
	private void altaBarajas() {
		for (int i = 0; i < Baraja.NUM_BAR_BASE; i++) {
			Baraja baraja = new Baraja();
			baraja.altaProducto(i);
			this.barajas.add(baraja);
			baraja.controlContadorProductos(1);
		}
	}

	/**
	 * Este es el metodo que se utiliza para dar de alta las velas pre-escritas.
	 * 
	 * No recibe ni devuelve nada. Da de alta unas velas base usando un bucle for
	 * que da tantas vueltas como elementos en el Enum AromaVela hay.
	 * 
	 * Todas las velas son iguales excepto por el aroma, y utilizan el constructor
	 * de Producto sin nombre, dado que lo recibiran junto al color en su metodo
	 * sobreescrito altaProducto.
	 * 
	 * Con la variable local AromaVela aroma, obtengo el valor del array del Enum
	 * AromaVela que corresponde al int i del bucle for, y asi cada vela tiene un
	 * aroma distinto.
	 * 
	 * Llama a calcular el precio neto, calcular el codigo y el control de contador
	 * de productos de Producto sobreescritos en la clase Vela.
	 * 
	 * @see Vela.altaProducto()
	 * @see Vela.calcularPrecioNeto()
	 * @see Vela.calcularCodigo()
	 * @see Vela.controlContadorProductos()
	 */
	private void altaVelasBase() {
		for (int i = 0; i < AromaVela.values().length; i++) {
			AromaVela aroma = AromaVela.values()[i];
			Vela vela = new Vela(2.12F, 5.00F, 2.5F, 7.0F, 1, aroma);
			vela.altaProducto(i);
			vela.calcularPrecioNeto();
			vela.calcularCodigo();
			this.velas.add(vela);
			vela.controlContadorProductos(1);
		}
	}

	/**
	 * Este es el metodo que se utilizara en el alta de velas por teclado.
	 * 
	 * Dar de alta velas con sus parametros. Llama al metodo altaProducto
	 * sobreescrito en la clase Vela para asignar nombre y color, buscando
	 * coincidencias a traves del .values() en un bucle for. Tras encontrar la
	 * coincidencia, se rompe el bucle.
	 * 
	 * Llama a calcular el precio neto, calcular el codigo y el control de contador
	 * de productos de Producto sobreescritos en la clase Vela.
	 * 
	 * @param float     peso
	 * @param float     precioBruto
	 * @param String    codigo
	 * @param float     diametro
	 * @param float     altura
	 * @param int       unidades
	 * @param AromaVela aroma
	 * 
	 * @see Vela.altaProducto()
	 * @see Vela.calcularPrecioNeto()
	 * @see Vela.calcularCodigo()
	 * @see Vela.controlContadorProductos()
	 */
	private void altaVelas(float peso, float precioBruto, float diametro, float altura, int unidades, AromaVela aroma) {
		Vela vela = new Vela(peso, precioBruto, diametro, altura, unidades, aroma);
		for (int i = 0; i < AromaVela.values().length; i++) {
			if (aroma == AromaVela.values()[i]) {
				vela.altaProducto(i);
				break;
			}
		}
		vela.calcularPrecioNeto();
		vela.calcularCodigo();
		this.velas.add(vela);
		vela.controlContadorProductos(1);
	}

	/**
	 * Comprueba si el valor de un String esta admitido en los Enum de AromaVela,
	 * devolviendo false por defecto, y true si encuentra coincidencia.
	 * 
	 * @param String aroma
	 * @return boolean
	 */
	private boolean comprobarAroma(String aroma) {
		switch (aroma) {
		case "NINGUNO":
			return true;
		case "VAINILLA":
			return true;
		case "LIMON":
			return true;
		case "FRUTOS ROJOS":
			return true;
		case "CANELA":
			return true;
		default:
			Pintar.valorIncorrecto();
			Pintar.opcionesAromas();
			return false;
		}
	}

	/**
	 * Devuelve un valor del Enum AromaVela segun el String entrante. No he
	 * conseguido juntar el metodo anterior y este de manera efectiva y por eso los
	 * he separado en dos.
	 * 
	 * @param String a
	 * @return AromaVela aroma
	 */
	private AromaVela rellenarAroma(String a) {
		switch (a) {
		case "NINGUNO":
			return AromaVela.NINGUNO;
		case "VAINILLA":
			return AromaVela.VAINILLA;
		case "LIMON":
			return AromaVela.LIMON;
		case "FRUTOS ROJOS":
			return AromaVela.FRUTOS_ROJOS;
		case "CANELA":
			return AromaVela.CANELA;
		default:
			return AromaVela.NINGUNO;
		}
	}

	/**
	 * Pide cada dato necesario para el alta de una vela y los va comprobando uno
	 * por uno. Al final del todo pide confirmacion, y en caso de ser positiva da el
	 * alta de la vela.
	 * 
	 * Se repite hasta que el usario responda "NO" cuando se le pregunta si quiere
	 * insertar mas velas.
	 * 
	 * Si en cualquier momento durante el proceso se escribe 0, el proceso se
	 * detiene.
	 * 
	 * @see Pintar.datosAltas()
	 * @see Pintar.altaVelaNum(int n)
	 * @see Pintar.pedirAromaVela()
	 * @see Pintar.pedirUnidadesVela()
	 * @see Pintar.pedirPrecio()
	 * @see Pintar.pedirPeso()
	 * @see Pintar.pedirDiametro()
	 * @see Pintar.pedirAltura()
	 * @see Pintar.altaCancelada()
	 * @see Pintar.confirmacionVela(String a, int unidades, float precio, flaot
	 *      peso, float diametro, float altura)
	 * @see Pintar.intentaloDeNuevo()
	 * @see Pintar.repetirAltaVela()
	 * @see Pintar.altasFinalizadas()
	 * @see Programa.toSentenceCase(String s)
	 * @see Programa.pedirDatosString()
	 * @see Programa.pedirDatosInt()
	 * @see Programa.pedirDatosFloat()
	 * @see Programa.pedirDatosBoolean()
	 * @see this.comprobarAroma(String a)
	 * @see this.rellenarAroma(String a)
	 * @see this.altaVelas(float peso, float precio, float diametro, float altura,
	 *      int unidades, AraomaVela aroma)
	 */
	public void pedirDatosVelas() {
		boolean confirmacion = true;
		int count = 0;
		Pintar.datosAltas();
		do {
			Pintar.altaVelaNum(count);
			Pintar.pedirAromaVela();
			boolean aromaCorrecto = false;
			String a = "-1";
			AromaVela aroma = AromaVela.NINGUNO;
			while (!aromaCorrecto) {
				a = Programa.pedirDatosString();
				if (a.equals("0")) {
					break;
				}
				a = a.toUpperCase();
				aromaCorrecto = this.comprobarAroma(a);
				if (aromaCorrecto) {
					aroma = this.rellenarAroma(a);
					a = Programa.toSentenceCase(a);
				}
			}
			if (a.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirUnidadesVela();
			int unidades = Programa.pedirDatosInt();
			if (unidades == 0) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPrecio();
			float precio = Programa.pedirDatosFloat();
			if (precio == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPeso();
			float peso = Programa.pedirDatosFloat();
			if (peso == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirDiametro();
			float diametro = Programa.pedirDatosFloat();
			if (diametro == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirAltura();
			float altura = Programa.pedirDatosFloat();
			if (altura == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.confirmacionVela(a, unidades, precio, peso, diametro, altura);
			confirmacion = Programa.pedirDatosBoolean();
			if (!confirmacion) {
				Pintar.altaCancelada();
				Pintar.intentaloDeNuevo();
				confirmacion = true;
			} else {
				this.altaVelas(peso, precio, diametro, altura, unidades, aroma);
				count++;
				Pintar.repetirAltaVela();
				confirmacion = Programa.pedirDatosBoolean();
				if (!confirmacion) {
					Pintar.altasFinalizadas();
				}
			}
		} while (confirmacion == true);

	}

	/**
	 * No recibe ni devuelve nada. Da de alta los Cristales base escritos en la
	 * clase Cristal en el metodo sobreescrito altaProducto, de la misma manera que
	 * lo hice en la clase Baraja.
	 * 
	 * Llama a calcular el precio neto, calcular el codigo y el control de contador
	 * de productos de Producto sobreescritos en la clase Vela.
	 * 
	 * @see Cristal.altaProducto()
	 * @see Cristal.calcularPrecioNeto()
	 * @see Cristal.calcularCodigo()
	 * @see Cristal.controlContadorProductos()
	 */
	private void altaCristalesBase() {
		for (int i = 0; i < Cristal.NUM_CRI_BASE; i++) {
			Cristal cristal = new Cristal();
			cristal.altaProducto(i);
			cristal.calcularPrecioNeto();
			cristal.calcularCodigo();
			this.cristales.add(cristal);
			cristal.controlContadorProductos(1);
		}
	}

	/**
	 * Recibe los seis atributos necesarios para dar de alta un cristal, y despues
	 * llama a calcular el precio neto, calcular el codigo y el control de contador
	 * de productos de Producto sobreescritos en la clase Cristal.
	 * 
	 * @param String  nombre
	 * @param float   peso
	 * @param float   precioBruto
	 * @param String  codigo
	 * @param boolean pulido
	 * @param String  forma
	 * 
	 * @see Cristal.calcularPrecioNeto()
	 * @see Cristal.calcularCodigo()
	 * @see Cristal.controlContadorProductos()
	 */
	private void altaCristales(String nombre, float peso, float precio, boolean pulido, String forma) {
		Cristal cristal = new Cristal(nombre, peso, precio, pulido, forma);
		cristal.calcularPrecioNeto();
		cristal.calcularCodigo();
		this.cristales.add(cristal);
		cristal.controlContadorProductos(1);
	}

	/**
	 * Pide cada dato necesario para el alta de un cristal y comprueba cada uno.
	 * 
	 * Al final del todo pide confirmacion, y en caso de ser positiva da el alta del
	 * cristal. Se repite hasta que el usario responda "NO" cuando se le pregunta si
	 * quiere insertar mas velas.
	 * 
	 * Si en cualquier momento durante el proceso se escribe 0, el proceso se
	 * detiene.
	 * 
	 * @see Pintar.datosAltas()
	 * @see Pintar.altaCristalNum(int n)
	 * @see Pintar.pedirNombreCristal()
	 * @see Pintar.pedirPeso()
	 * @see Pintar.pedirPrecio()
	 * @see Pintar.pedirPulido()
	 * @see Pintar.altaCancelada()
	 * @see Pintar.confirmacionCristal(String a, int unidades, float precio, flaot
	 *      peso, float diametro, float altura)
	 * @see Pintar.intentaloDeNuevo()
	 * @see Pintar.repetirAltaCristal()
	 * @see Pintar.altasFinalizadas()
	 * @see Programa.pedirDatosString()
	 * @see Programa.pedirDatosFloat()
	 * @see Programa.pedirDatosBoolean()
	 * @see this.altaCristales(String nombre, float peso, float precio, boolean
	 *      pulido, String forma)
	 */
	public void pedirDatosCristal() {
		boolean confirmacion = true;
		int count = 0;
		Pintar.datosAltas();
		do {
			Pintar.altaCristalNum(count);
			Pintar.pedirNombreCristal();
			String nombre = Programa.pedirDatosString();
			if (nombre.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPeso();
			float peso = Programa.pedirDatosFloat();
			if (peso == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPrecio();
			float precio = Programa.pedirDatosFloat();
			if (precio == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPulido();
			boolean isPulido = Programa.pedirDatosBoolean();
			Pintar.pedirForma();
			String forma = Programa.pedirDatosString();
			Pintar.confirmacionCristal(nombre, peso, precio, isPulido, forma);
			confirmacion = Programa.pedirDatosBoolean();
			if (!confirmacion) {
				Pintar.altaCancelada();
				Pintar.intentaloDeNuevo();
				confirmacion = true;
			} else {
				this.altaCristales(nombre, peso, precio, isPulido, forma);
				count++;
				Pintar.repetirAltaCristal();
				confirmacion = Programa.pedirDatosBoolean();
				if (!confirmacion) {
					Pintar.altasFinalizadas();
				}
			}
		} while (confirmacion);
	}

	/**
	 * No recibe ni devuelve nada. Da de alta unos productos base usando los metodos
	 * escritos anteriormente.
	 * 
	 * @see this.altaBarajas()
	 * @see this.altaVelasBase()
	 * @see this.altaCristalesBase()
	 */
	public void altaProductosBase() {
		this.altaBarajas();
		this.altaVelasBase();
		this.altaCristalesBase();
	}

	/**
	 * Recibe una cadena de texto y por cada baraja en un bucle for recorre otro
	 * bucle for dependiendo de la largura del array de cartas.
	 * 
	 * Compara el nombre de cada carta con el String recibido. Si encuentra una
	 * coincidencia, muestra la carta.
	 * 
	 * Si no encuentra coincidencia, muestra un mensaje de que no se ha encontrado
	 * dicha carta.
	 * 
	 * No se detendra con un break tras encontrarla puesto que podria haber mas de
	 * una con el mismo nombre (no las hay, porque solo tengo dos barajas, pero
	 * podria ser).
	 * 
	 * @see Carta.toString()
	 */
	public void buscarCarta() {
		Pintar.pedirNombreCarta();
		String nombre = Programa.pedirDatosString();
		if (nombre.equals("0")) {
			Pintar.retrocediendo();
		} else {
			boolean cExiste = false;
			for (int i = 0; i < Baraja.NUM_BAR_BASE; i++) {
				for (int j = 0; j < this.barajas.get(i).getCartas().length; j++) {
					if (this.barajas.get(i).getCartas()[j] != null) {
						if (this.barajas.get(i).getCartas()[j].getNombre().equalsIgnoreCase(nombre)) {
							cExiste = true;
							System.out.println(this.barajas.get(i).getCartas()[j]);
						}
					}
				}
			}
			if (!cExiste) {
				Pintar.cartaNoExiste();
			} else {
				Pintar.esoEsTodo();
			}
		}
	}

	/**
	 * Recibe un Enum AromaVela y lo compara con el atributo de las velas
	 * existentes. Si encuentra concidencias, las va mostrando por pantalla, y si
	 * no, muestra un mensaje de que no hay coincidencias.
	 * 
	 * @param AromaVela aroma
	 * 
	 * @see Vela.toString()
	 * 
	 */
	public void buscarVela() {
		Pintar.pedirAromaVela();
		boolean aromaCorrecto = false;
		String a = "-1";
		AromaVela aroma = AromaVela.NINGUNO;
		while (!aromaCorrecto) {
			a = Programa.pedirDatosString();
			if (a.equals("0")) {
				break;
			}
			a = a.toUpperCase();
			aromaCorrecto = this.comprobarAroma(a);
			if (aromaCorrecto) {
				aroma = rellenarAroma(a);
				a = Programa.toSentenceCase(a);
			}
		}
		if (a.equals("0")) {
			Pintar.retrocediendo();
		} else {
			boolean vExiste = false;
			for (Vela v : velas) {
				if (v.getAroma() == aroma) {
					System.out.println(v);
					vExiste = true;
				}
			}
			if (!vExiste) {
				Pintar.noHayCoincidencias();
			} else {
				Pintar.esoEsTodo();
			}
		}
	}

	/**
	 * Recibe un String y lo compara con cada forma de cada cristal existente. Si
	 * encuentra coincidencias, muestra el/los cristal/es por pantalla, y si no
	 * encuentra ninguna, muestra un mensaje de que no hay coincidencias.
	 * 
	 * @param String forma
	 * 
	 * @see Cristal.toString()
	 * 
	 */
	public void buscarCristal() {
		Pintar.pedirForma();
		String forma = Programa.pedirDatosString();
		if (forma.equals("0")) {
			Pintar.retrocediendo();
		} else {
			boolean cExiste = false;
			for (Cristal c : cristales) {
				if (c.getForma().equalsIgnoreCase(forma)) {
					System.out.println(c);
					cExiste = true;
				}
			}
			if (!cExiste) {
				Pintar.noHayCoincidencias();
			} else {
				Pintar.esoEsTodo();
			}
		}
	}

	/**
	 * Recibe una cadena de texto y recorre cada ArrayList de esta clase Gestion con
	 * bucles for each comparando si el codigo de los objetos que va recorriendo
	 * coincide con la cadena de texto. Va comprobando si ha encontrado o no el
	 * producto con un boolean, de manera que si lo encuentra al principio no tenga
	 * necesidad de recorrer los siguientes bucles. Si no se encuentra el producto,
	 * muestra un mensaje de no se encontraron coincidencias.
	 * 
	 * @param String codigo
	 * 
	 * @see Baraja.toString()
	 * @see Vela.toString()
	 * @see Cristal.toString()
	 * 
	 */
	public void buscarProducto() {
		Pintar.pedirCodigo();
		String codigo = Programa.pedirDatosString();
		if (codigo.equals("0")) {
			Pintar.retrocediendo();
		} else {
			boolean pExiste = false;
			for (Baraja b : barajas) {
				if (codigo.equalsIgnoreCase(b.getCodigo())) {
					System.out.println("|           Este es el producto que has buscado:");
					System.out.println(b);
					pExiste = true;
					break;
				}
			}
			if (!pExiste) {
				for (Vela v : velas) {
					if (codigo.equalsIgnoreCase(v.getCodigo())) {
						System.out.println("|           Este es el producto que has buscado:");
						System.out.println(v);
						pExiste = true;
						break;
					}
				}
				if (!pExiste) {
					for (Cristal c : cristales) {
						if (codigo.equalsIgnoreCase(c.getCodigo())) {
							System.out.println("|           Este es el producto que has buscado:");
							System.out.println(c);
							pExiste = true;
							break;
						}
					}
				}
			}
			if (!pExiste) {
				Pintar.productoNoExiste();
			}
		}
	}

	/**
	 * No recibe ni devuelve nada. Muestra las barajas almacenadas.
	 * 
	 * @see Baraja.toString()
	 * 
	 */
	public void listarBarajas() {
		int count = 1;
		for (Baraja b : this.barajas) {
			if (b != null) {
				System.out.println("|           Baraja " + count + ":");
				System.out.println("|" + b);
				count++;
			}
		}
	}

	/**
	 * No recibe ni devuelve nada. Muestra las velas almacenadas.
	 * 
	 * @see Vela.toString()
	 * 
	 */
	public void listarVelas() {
		int count = 1;
		for (Vela v : this.velas) {
			if (v != null) {
				System.out.println("|           Vela " + count + ":");
				System.out.println("|" + v);
				count++;
			}
		}
	}

	/**
	 * No recibe ni devuelve nada. Muestra los cristales almacenados.
	 * 
	 * @see Cristal.toString()
	 * 
	 */
	public void listarCristales() {
		int count = 1;
		for (Cristal c : this.cristales) {
			if (c != null) {
				System.out.println("|           Cristal " + count + ":");
				System.out.println("|" + c);
				count++;
			}
		}
	}

	/**
	 * Pide un String, y, recorriendo todos los productos existentes, lo compara con
	 * los codigos de producto. Si encuentra una coincidencia, pide otro String y lo
	 * intercambia por el nombre anterior del producto.
	 * 
	 * Comprueba si ha encontrado el producto o no con un boolean. Si no encuentra
	 * coincidencias, muestra un mensaje avisando de ello. Si las encuentra, no
	 * recorre los siguientes bucles y deja de buscar.
	 * 
	 * @catch NullPointerException
	 * 
	 * @param String codigo
	 * @param String nombre
	 * 
	 * @see Pintar.retrocediendo()
	 * @see Pintar.nuevoNombre()
	 * @see Pintar.noHayCoincidencias()
	 * @see Pintar.nombreModificado()
	 * @see Programa.pedirDatosString()
	 * 
	 */
	public void modificarProducto() {
		String codigo = Programa.pedirDatosString();
		if (codigo.equals("0")) {
			Pintar.retrocediendo();
		} else {
			try {
				boolean pExiste = false;
				Iterator<Baraja> itB = barajas.iterator();
				while (itB.hasNext()) {
					Baraja b = itB.next();
					if (b.getCodigo() != null) {
						if (b.getCodigo().equalsIgnoreCase(codigo)) {
							Pintar.nuevoNombre();
							String nombre = Programa.pedirDatosString();
							if (nombre.equals("0")) {
								Pintar.retrocediendo();
								break;
							} else {
								b.setNombre(nombre);
								Pintar.nombreModificado();
								pExiste = true;
								break;
							}
						}
					}
				}
				if (!pExiste) {
					Iterator<Vela> itV = velas.iterator();
					while (itV.hasNext()) {
						Vela v = itV.next();
						if (v.getCodigo() != null) {
							if (v.getCodigo().equalsIgnoreCase(codigo)) {
								Pintar.nuevoNombre();
								String nombre = Programa.pedirDatosString();
								if (nombre.equals("0")) {
									Pintar.retrocediendo();
									break;
								} else {
									v.setNombre(nombre);
									Pintar.nombreModificado();
									pExiste = true;
									break;
								}
							}
						}
					}
					if (!pExiste) {
						Iterator<Cristal> itC = cristales.iterator();
						while (itC.hasNext()) {
							Cristal c = itC.next();
							if (c.getCodigo() != null) {
								if (c.getCodigo().equalsIgnoreCase(codigo)) {
									Pintar.nuevoNombre();
									String nombre = Programa.pedirDatosString();
									if (nombre.equals("0")) {
										Pintar.retrocediendo();
										break;
									} else {
										c.setNombre(nombre);
										Pintar.nombreModificado();
										pExiste = true;
										break;
									}
								}
							}
						}
					}
					if (!pExiste) {
						Pintar.noHayCoincidencias();
					}
				}
			} catch (NullPointerException e) {
				Pintar.error();
			}
		}
	}

	/**
	 * Pide un String y lo va comparando con el codigo de todos los productos
	 * almacenados.
	 * 
	 * Va comprobando si ha encontrado o no el producto con un boolean, de manera
	 * que si lo encuentra no tenga necesidad de recorrer los siguientes bucles. Si
	 * no lo encuentra, se muestra un mensaje de no se encontraron coincidencias.
	 * 
	 * El bucle para borrar barajas existe pero no es accesible. En caso de que en
	 * un futuro cambiase de idea sobre poder borrarlas, el codigo ya esta escrito,
	 * solamente habria que eliminar una clausula else if().
	 * 
	 * @catch NullPointerException
	 * 
	 * @param String codigo
	 * 
	 * @see Pintar.pedirCodigo()
	 * @see Pintar.retrocediendo()
	 * @see Pintar.noSePuedenBorrarBarajas()
	 * @see Programa.pedirDatosString()
	 * @see Pintar.borradoCorrecto()
	 * @see Pintar.noHayCoincidencias()
	 * @see Pintar.error()
	 * 
	 */
	public void borrarProducto() {
		Pintar.pedirCodigo();
		String codigo = Programa.pedirDatosString();
		if (codigo.equals("0")) {
			Pintar.retrocediendo();
		} else if (codigo.equalsIgnoreCase("BAR000") || codigo.equalsIgnoreCase("BAR001")) {
			Pintar.noSePuedenBorrarBarajas(); // borrar barajas inaccesible aqui
		} else {
			try {
				boolean pExiste = false;
				for (int i = 0; i < barajas.size(); i++) {
					if (barajas.get(i) != null) {
						if (barajas.get(i).getCodigo().equalsIgnoreCase(codigo)) {
							barajas.get(i).controlContadorProductos(-1);
							barajas.remove(i);
							pExiste = true;
							Pintar.borradoCorrecto();
							break;
						}
					}
				}
				if (!pExiste) {
					for (int i = 0; i < velas.size(); i++) {
						if (velas.get(i) != null) {
							if (velas.get(i).getCodigo().equalsIgnoreCase(codigo)) {
								velas.get(i).controlContadorProductos(-1);
								velas.remove(i);
								pExiste = true;
								Pintar.borradoCorrecto();
								break;
							}
						}
					}
					if (!pExiste) {
						for (int i = 0; i < cristales.size(); i++) {
							if (cristales.get(i) != null) {
								if (cristales.get(i).getCodigo().equalsIgnoreCase(codigo)) {
									cristales.get(i).controlContadorProductos(-1);
									cristales.remove(i);
									pExiste = true;
									Pintar.borradoCorrecto();
									break;
								}
							}
						}
					}
					if (!pExiste) {
						Pintar.noHayCoincidencias();
					}
				}
			} catch (NullPointerException e) {
				Pintar.error();
			}
		}
	}

}
