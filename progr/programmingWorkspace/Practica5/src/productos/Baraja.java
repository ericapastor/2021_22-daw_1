package productos;

import enums.TipoBaraja;
import programa.Pintar;

/**
 * 
 * Baraja tiene un atributo enum que se refiere al tipo de baraja (oraculo o
 * tarot), y otro atributo de cartas, en el que recibe un array de objetos de la
 * clase Carta. La razon de usar un array y no un ArrayList en este caso es que
 * una baraja siempre tendra un numero de cartas definido, y no vamos a darlas
 * de alta por teclado. No hay necesidad de crear un objeto ArrayList.
 * 
 * Ademas tiene un magic number que es un final estatico (NUM_BAR_BASE) que
 * definira el numero de barajas a la hora de rellenarlas. Si tuviera que hacer
 * otra baraja, aumentaria ese numero sin necesidad de tener que cambiarlo en
 * todos los demas sitios del programa.
 * 
 * Es una clase final porque con ella termina la herencia (no tiene clases por
 * debajo suya jerarquicamente hablando).
 * 
 * @author erica
 */
public final class Baraja extends Producto {

	public static final int NUM_BAR_BASE = 2;
	public static int numBarajas = 0;
	private Carta[] cartas;
	private TipoBaraja tipo;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor para crear barajas que usa el constructor vacio de Producto. No
	 * tengo necesidad de mas constructores.
	 * 
	 * @see Public Producto()
	 */
	public Baraja() {
		super();
		iva = 15F;
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return Carta[] cartas
	 */
	public Carta[] getCartas() {
		return cartas;
	}

	/**
	 * @param Carta[] cartas
	 */
	public void setCartas(Carta[] cartas) {
		this.cartas = cartas;
	}

	/**
	 * @return TipoBaraja tipo
	 */
	public TipoBaraja getTipoBaraja() {
		return tipo;
	}

	/**
	 * @param TipoBaraja tipo
	 */
	public void setTipoBaraja(TipoBaraja tipo) {
		this.tipo = tipo;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return super.toString() + "|           Tipo de baraja: " + this.tipo + "\n|           Numero de cartas: "
				+ this.cartas.length + "\n|           IVA aplicado: " + this.iva + "%\n|           Descuento aplicado: "
				+ this.precio.getDescuento() + "\n|";
	}

	/**
	 * Calcular el precio de un producto dependiendo del descuento que se le
	 * aplique. Si no coincide con ninguno, solo se le aplica el IVA. Para baraja el
	 * descuento de primavera es del 5.55%, el de Navidad del 7.12% y el de San
	 * Valentin del 8.57%.
	 */
	@Override
	public void calcularPrecioNeto() {
		float precioB = this.getPrecio().getPrecioBruto();
		float precioN;
		switch (this.precio.getDescuento()) {
		case PRIMAVERA:
			precioN = precioB * (1 - iva / 100) * (1 + 5.55F / 100);
		case NAVIDAD:
			precioN = precioB * (1 - iva / 100) * (1 + 7.12F / 100);
		case SAN_VALENTIN:
			precioN = precioB * (1 - iva / 100) * (1 + 8.57F / 100);
		default:
			precioN = precioB * (1 - iva / 100);
		}
		this.precio.setPrecioNeto(precioN);
	}

	/**
	 * Metodo abstracto de la clase Producto para asignar el codigo de producto
	 * automaticamente segun el numero de barajas existente.
	 */
	@Override
	public void calcularCodigo() {
		if (numBarajas < 10) {
			this.setCodigo("BAR00" + numBarajas);
		} else if (numBarajas >= 10 && numBarajas < 100) {
			this.setCodigo("BAR0" + numBarajas);
		} else if (numBarajas >= 100 && numBarajas < 1000) {
			this.setCodigo("BAR" + numBarajas);
		}
	}

	/**
	 * Metodo abstracto de la clase producto que me sirve para sumar el numero que
	 * entre (sea positivo o negativo) sin que pase de los numeros especulados en el
	 * metodo calcularCodigo (de 0 a 1000).
	 */
	@Override
	public void controlContadorProductos(int n) {
		if (numBarajas + n > 1000) {
			numBarajas = 0;
		} else if (numBarajas + n < 0) {
			numBarajas = 0;
		} else {
			numBarajas += n;
		}
	}

	/**
	 * Recibe un numero y en base a este va dando de alta las barajas que
	 * previamente he rellenado a mano.
	 * 
	 * @param int n
	 * 
	 * @see altaOraculoTWUASD()
	 * @see altaTarotOSHO()
	 */
	@Override
	public void altaProducto(int n) {
		switch (n) {
		case 0:
			this.altaOraculoTWUASD();
			break;
		case 1:
			this.altaTarotOSHO();
			break;
		}
	}

	/**
	 * Seleccionar un numero entre las posiciones del array de cartas.
	 * 
	 * @return int carta
	 * 
	 */
	public int elegirCartaAleatoria() {
		int carta = (int) (Math.round(Math.random() * this.cartas.length));
		if (carta >= this.cartas.length) {
			carta = cartas.length - 1;
		} else if (carta < 0) {
			carta = 0;
		}
		return carta;
	}

	/**
	 * Seleccionar 0 o 1 para posteriormente saber si una carta sale del derecho o
	 * del reves.
	 * 
	 * @return boolean posicion
	 * 
	 */
	public boolean elegirPosicionAleatoria() {
		int posicion = (int) (Math.round(Math.random()));
		if (posicion > 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Elegir tres cartas del derecho o del reves aleatoriamente y luego mostrarlas.
	 * 
	 * @see elegirCartaAleatoria()
	 * @see elegirPosicionAleatoria()
	 */
	public void lecturaTresCartas() {
		Pintar.cartaPasado();
		try {
			Carta carta = cartas[this.elegirCartaAleatoria()];
			carta.setDelDerecho(this.elegirPosicionAleatoria());
			System.out.println(carta);
			Pintar.cartaPresente();
			carta = cartas[this.elegirCartaAleatoria()];
			carta.setDelDerecho(this.elegirPosicionAleatoria());
			System.out.println(cartas[this.elegirCartaAleatoria()]);
			Pintar.cartaFuturo();
			carta = cartas[this.elegirCartaAleatoria()];
			carta.setDelDerecho(this.elegirPosicionAleatoria());
			System.out.println(cartas[this.elegirCartaAleatoria()]);
		} catch (java.lang.StringIndexOutOfBoundsException e) {
			Pintar.algoFueMal();
		} catch (java.lang.NullPointerException n) {
			Pintar.noHayCartas();
		}
	}

	/**
	 * Dar de alta el Oraculo The Wild Unknown Animal Spirit Deck (TWUASD).
	 */
	private void altaOraculoTWUASD() {
		this.setNombre("The Wild Unknown Animal Spirit Deck");
		this.setPeso(227.81F);
		this.setPrecio(new Precio(28.97F));
		this.precio.calcularDescuento();
		this.calcularPrecioNeto();
		this.calcularCodigo();
		this.setTipoBaraja(TipoBaraja.ORACULO);
		this.cartas = new Carta[63];
		this.cartas[0] = new Carta("oso", "tierra", "fuerza interior,ansias de crecimiento", "retiro,letargo,pesadez",
				"movimiento,ejercicio");
		this.cartas[1] = new Carta("gusano de tierra", "tierra", "formal,inteligente,valioso",
				"demasiado consciente de si mismo,aprensivo", "habla claro,arriesgate a pasar verg�enza");
		this.cartas[2] = new Carta("raton", "tierra", "organizado,ingenioso,preparado", "ocupado sin proposito",
				"lleva a cabo un proyecto significativo para ti");
		this.cartas[3] = new Carta("conejo", "tierra", "sensible,resolutivo,buen oyente",
				"sobreexplicaciones,hablar demasiado rapido", "un dia de silencio");
		this.cartas[4] = new Carta("mapache", "tierra", "amigo generoso,artista excepcional",
				"competitivo,artista hambriento", "haz nuevos proyectos");
		this.cartas[5] = new Carta("zorro", "tierra", "maestro ingenioso,magia,monogamia",
				"furtivo,huidizo,inseguridad de la propia identidad", "asociacion,conexion,camaraderia");
		this.cartas[6] = new Carta("serpiente", "tierra", "prosperidad,creatividad,carisma",
				"empezar y abandonar muchas cosas", "kundalini yoga,reexplora tus capacidades");
		this.cartas[7] = new Carta("bufalo", "tierra", "presencia pura,seguridad,certeza",
				"inquietud,falta de gratitud", "conecta con tus creencias");
		this.cartas[8] = new Carta("cordero", "tierra", "conocimiento,paz interna", "silencio,timidez,preocupacion",
				"medita,escucha tu verdad interna");
		this.cartas[9] = new Carta("alce", "tierra", "apoyo a los demas,amabilidad,consistente,consecuente",
				"pretencioso,alto y poderoso", "come y bebe mas conscientemente");
		this.cartas[10] = new Carta("cierva", "tierra", "receptividad,compasion,nutre a los demas",
				"preocupacion,sobreproteccion del otro", "naturaleza,ni�os o seres queridos");
		this.cartas[11] = new Carta("lobo", "tierra", "confiable,democratico,audaz", "dominante,critico",
				"practica dejar ir");
		this.cartas[12] = new Carta("ara�a", "tierra", "agradecido,entusiasta,prospero",
				"descorazonado,cansado,desamparado", "juega con tu creatividad");
		this.cartas[13] = new Carta("caballo", "tierra", "logra cualquier cosa,nunca se rinde",
				"huidizo,sentimiento de debilidad", "entrenamiento de fuerza");
		this.cartas[14] = new Carta("cocodrilo", "agua", "sabio,paciente,potente energia silenciosa",
				"energia atascada,darse de golpes con uno mismo", "descansar,sumergerse,recolectar energia,enfriarse");
		this.cartas[15] = new Carta("manta marina", "agua", "entusiasta,deseo de crecer,la \"espina\"",
				"echar la culpa a otros,rendirse",
				"enfrentate a situaciones incomodas,desarrolla confianza en ti mismo");
		this.cartas[16] = new Carta("pez", "agua", "adaptable,viajero", "distraido,opiniones cambiantes",
				"ponte algun reto menor y alcanzalo,no te pierdas en la corriente");
		this.cartas[17] = new Carta("estrella de mar", "agua", "bello,seductor,artistico,expresivo",
				"cotillear,sentirse vacio", "rodeate de amigos positivos");
		this.cartas[18] = new Carta("pulpo", "agua", "interes en todo,comprometido,inteligente",
				"anhelo,falta de limites,falta de direccion,pegajoso,falta de coraje", "date espacio a ti mismo");
		this.cartas[19] = new Carta("castor", "agua", "contento,trabajador,leal,incansable,trabajo significativo",
				"quemado,sentimiento de inutilidad", "labor fisca,servicio desinteresado");
		this.cartas[20] = new Carta("ostra", "agua",
				"paciente,guarda de secretos,esconde tesoros interiores,sentimiento de bienaventuranza,genoroso",
				"reacio,agarre,se \"cierra\"", "comparte algo");
		this.cartas[21] = new Carta("tortuga", "agua",
				"pacifico,aventurero,productivo,alma antigua,confiado,casa en uno mismo",
				"detenerse,bloquearse,dejar de avanzar", "busca una aventura");
		this.cartas[22] = new Carta("rana", "agua", "energia clara,entusiasmo por la vida,sanador",
				"agotado,quedarse vacio", "visita: un lago,un oceano,un rio");
		this.cartas[23] = new Carta("nutria", "agua", "alegria sin obstaculos,jugueton,satisfaccion",
				"sombrio,busca excusas", "ve a una fiesta de baile o a una celebracion");
		this.cartas[24] = new Carta("tiburon", "agua", "intrigante,cautivador,directo,expuesto,misterioso",
				"furtivo,destructivo", "se honesto");
		this.cartas[25] = new Carta("cisne", "agua",
				"poder de creacion infinito,creatividad sin esfuerzo,sensible a lo mistico",
				"agitado,insolente,falto de vision", "pasa tiempo solo,escribe");
		this.cartas[26] = new Carta("delfin", "agua",
				"sanador activo,fuerte practica espiritual,inteligente de manera innata,luz",
				"subestima su propio poder", "busca un espiritu afin");
		this.cartas[27] = new Carta("ballena", "agua", "calmado,compasion desmedida,sabiduria antigua,paz profunda",
				"pesado,se repite en antiguas historias", "realiza cuidados de ti mismo");
		this.cartas[28] = new Carta("hormiga de fuego", "fuego", "disciplinado,reflexivo",
				"agresivo,solo sigue ordenes,pensamiento rigido,cotillear,discute",
				"pasa tiempo a solas,pasea de noche");
		this.cartas[29] = new Carta("hyena", "fuego", "ingenioso,divertido,encantador",
				"deshilvanado,mezquino,receloso", "moderate,busca el pensamiento racional y la modestia");
		this.cartas[30] = new Carta("escorpion", "fuego", "apasionado,competitivo,salvaje,libre,fiero",
				"celoso,tiende a aislarse,resentido,irresoluto", "se honesto,trabaja en el perdon");
		this.cartas[31] = new Carta("lagarto", "fuego", "instintivo,sensible a lo sutil,so�ador,artistico,espiritual",
				"elusivo,evasivo,no se compromete,escamoso", "realiza un proyecto creativo");
		this.cartas[32] = new Carta("pantera", "fuego", "bravo,productivo,destruye lo innecesario,purgador",
				"auto-destructivo", "deshazte de las cargas innecesarias");
		this.cartas[33] = new Carta("tarantula", "fuego", "sigue su intuicion,reclama su objetivo de vida",
				"duda,sobre-intelectualiza", "comienza o prosigue un diario");
		this.cartas[34] = new Carta("camello", "fuego", "con recursos,calmado,se conoce a si mismo,independiente",
				"deshidratado,falta de vitalidad", "peregrinacion");
		this.cartas[35] = new Carta("gacela", "fuego", "elegante,perceptivo,artistico,alta consciencia",
				"alergias alimenticias,imsomnio,mente inquieta,vulnerabilidad",
				"regula tu alimentacion,medita o haz yoga,busca ambientes acogedores");
		this.cartas[36] = new Carta("guepardo", "fuego", "fuerza solar,accion,logros,energia sin limite",
				"impaciente,competitivo", "reconecta con tus propositos");
		this.cartas[37] = new Carta("tigre", "fuego", "fuerza lunar,comidad en la oscuridad,apasionado,fuerte,sensual",
				"sobre-estimulado,", "practica el \"trataka\"");
		this.cartas[38] = new Carta("cobra", "fuego", "pausado,paciente,maestro interno,aprendiz,humilde,sabio",
				"sabelotodo,egocentrico", "toma una clase,estudia");
		this.cartas[39] = new Carta("zebra", "fuego", "excentrico,,creativo,visionario,entusiasta,mundano",
				"hastiado,malhumorado,vanidoso", "una aventura epica,arte");
		this.cartas[40] = new Carta("leon", "fuego", "paciente,maestro,epitome de paz y fuerza,",
				"desligado,retirado,demasiada seriedad",
				"haz meditaciones diarias,manten contacto frecuente con tus amistades");
		this.cartas[41] = new Carta("elefante", "fuego",
				"imparable,propicio,sabio,generoso,enfocado en su objetivo,lleno de amor", "malinterpreta el destino",
				"confia");
		this.cartas[42] = new Carta("polilla", "aire", "entusiasta,caprichoso", "idealizacion de los demas,nerviosismo",
				"termina un proyecto");
		this.cartas[43] = new Carta("mariposa", "aire", "transformacion interna,alegria",
				"transformacion interna,fragil,frustrado", "concentrate en tu rutina diaria");
		this.cartas[44] = new Carta("murcielago", "aire", "aceptacion,adaptacion,renacimiento",
				"negacion a dejar ir,recrearse", "observa un nuevo amanecer");
		this.cartas[45] = new Carta("luciernaga", "aire", "inspirador,creativo,lluvias de ideas",
				"fugaz,apagado,aburrido", "escribe un poema,dibuja");
		this.cartas[46] = new Carta("abeja", "aire", "trabajador,energia vibrante,activo", "exceso de trabajo,irritado",
				"unas mini-vacaciones");
		this.cartas[47] = new Carta("colibri", "aire", "entusiasta,ingenioso,lleno de curiosidad",
				"demasiado exigente,insistente,brusco", "toma clases de algo que te guste");
		this.cartas[48] = new Carta("buitre", "aire", "purificacion,reequilibracion,sabiduria revelada",
				"dramatico,agresivo", "limpia tu espacio,infusiones de salvia");
		this.cartas[49] = new Carta("cuervo", "aire", "espiritualmente fuerte,mente clara,creatividad",
				"desarraigado,hipersensitivo", "practica meditacion diaria");
		this.cartas[50] = new Carta("buho", "aire", "abundancia,clarividencia,generosidad,confianza",
				"peleas de dinero,escasez", "haz una ofrenda");
		this.cartas[51] = new Carta("libelula", "aire", "vision clara,alegria,magia",
				"incapaz de concentrarse,mente bulliciosa", "concentrate en la respiracion");
		this.cartas[52] = new Carta("ruise�or", "aire", "voz intrepida,palabras libres con amabilidad,canto",
				"timidez,bulto en la garganta", "musica");
		this.cartas[53] = new Carta("pavo real", "aire",
				"seguro de si mismo,belleza interna,compasion,asimila cualquier cosa",
				"incapacidad para asimilar situaciones", "manipura chakra:,meditacion en el chakra del ombligo");
		this.cartas[54] = new Carta("halcon", "aire", "vigilante,intuitivo,vision clara", "ve demasiado,receloso",
				"cambia de perspectiva");
		this.cartas[55] = new Carta("aguila", "aire", "busqueda de la verdad,brillante y radiante,busqueda de retos",
				"controlador", "da un paso hacia lo desconocido");
		this.cartas[56] = new Carta("fenix",
				"espiritu\n|            Primer chakra (Muladhara)\n|            o \"chakra raiz\"",
				"transformacion del pasado,quemar el peso muerto,reencarnacion,empezar a vivir conscientemente,alcanzar claridad mental",
				"sufrimiento prolongado,vivir incoscientemente,escapar de uno mismo,cargar peso muerto,estancamiento,",
				"rodeate de rojo,haz estiramientos de cadera,inciensos de jengibre,reconecta con la naturaleza,haz ejercicio");
		this.cartas[57] = new Carta("serpiente marina",
				"espiritu\n|            Segundo chakra (Svadhisthana)\n|            o \"chakra del cuenco pelvico\"",
				"transformacion de las emociones,expresion audaz,emociones,creatividad,sensualidad y deseo,relaciones significativas y duraderas,aceptacion y perdon de uno mismo",
				"emociones y creatividad desequilibradas,areas de la vida estancadas y otras \"inundadas\",irritabilidad y amargura,problemas sexuales",
				"rodeate de naranja,baila,practica actividades que disfrutes");
		this.cartas[58] = new Carta("dragon",
				"espiritu\n|            Tercer chakra (Manipura)\n|            o \"chakra ombligo\"",
				"transformacion interna,lleno de coraje,valeroso,visionario,consciente de la consciencia,energia omnipotente,ver belleza en todas las cosas",
				"incapacidad para ver la belleza,resistencia a dejarse guiar,fuego interno apagado,problemas digestivos,dificultad para percibir con claridad\n la dimension interna y la externa,dificultad para percibir la propia intuicion,problemas hablando en publico",
				"rodeate de amarillo,practica el matra RAM,pasa tiempo al sol,cristal recomendado: ojo de tigre,yoga kundalini,");
		this.cartas[59] = new Carta("huevo dorado",
				"espiritu\n|            Cuarto chakra (Anahata)\n|            o \"chakra corazon\"",
				"equilibrio de cuerpo con mente,compasion y aceptacion hacia los demas,satisfaccion emocional,alta auto-estima,capcidad de intimidad,conexion con los seres y la naturaleza,capacidad de gratitud y perdon",
				"necesidad de controlar,posesividad,miedo al abandono,tencia a juzgar a otros,tristeza,desconfianza,miedo al compromiso",
				"rodeate de verde,yoga nidra,meditacion,toma un tiempo de introspeccion,escuchate desde tu vulnerabilidad,ejercicios de gratitud,ejercicios de perdon,interactua con animales,voluntariado");
		this.cartas[60] = new Carta("huevo negro",
				"espiritu\n|            Quinto chakra (Vishuddha)\n|            o \"chakra garganta\"",
				"capacidad creativa,voz interior,busqueda de la verdad,honestidad,facilidad verbal,capacidad para escuchar,capacidad para interpretar situaciones\n|             desde la perspectiva espiritual,liderazgo",
				"dificultad de expresion verbal,problemas de aprendizaje,tendencia a mentir o exagerar,ansiedad sobre el proposito de la vida,tendencia a juzgar y discriminar,arrogancia,competitividad,ifecciones recurrrentes de garganta y/o oido",
				"rodeate de azul claro,ejercita tu creatividad,canta o incluso grita\n|             manten activas tus cuerdas vocales,cristal recomendado: lapislazuli o turquesa,toma una clase de debate,reconciliate con tu verdad,escribe diariamente tus emociones del dia");
		this.cartas[61] = new Carta("unicornio",
				"espiritu\n|            Sexto chakra (Ajna)\n|            o \"chackra del tercer ojo",
				"chakras alineados,conexion con la divinidad,estar preparado para abrir la intuicion\n|             extrasensorial,despertar en la realidad mistica,desprendimiento del ego,conciencia pura,soltar y aceptar,confiar y recibir,fluir y expandirse",
				"crisis existencial con todos los chakras,espiritualidad abrumadora,dificultad en la sexualidad y las emociones,dificultad para una vision clara,desconexion de la mente y el cuerpo,comunicacion verbal dificil",
				"rodeate de azul oscuro (indigo),trabaja en tus cinco chakras inferiores,medita en oscuridad,conecta con tu esencia,reiki");
		this.cartas[62] = new Carta("huevo cosmico",
				"espiritu\n|            Septimo chakra (Sahasrara)\n|            o \"chakra coronilla\"",
				"despliegue de la consciencia cosmica,reconexion con el proposito,contentamiento,sensacion de unidad,identidad encontrada,conciencia universal,expresion mas sagrada de uno mismo",
				"sensacion de nunca llegar a objetivos,sensacion de viajar solo,desconexcion de los demas,fragmentacion de la vida cotidiana",
				"rodeate de violeta y blanco o dorado,trabaja en tus chakras inferiores,mantente constante y comprometido\n con tus practicas de yoga y meditacion");
	}

	/**
	 * Dar de alta el Tarot OSHO Zen (OSHO). Las cartas han quedado sin escribir
	 * porque no tuve tiempo para mas.
	 */
	private void altaTarotOSHO() {
		this.setNombre("Tarot OSHO Zen");
		this.setPeso(301.12F);
		this.setPrecio(new Precio(24.70F));
		this.calcularCodigo();
		this.setTipoBaraja(TipoBaraja.TAROT);
		this.cartas = new Carta[72];
	}
}
