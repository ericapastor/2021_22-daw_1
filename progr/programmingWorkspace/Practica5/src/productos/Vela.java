package productos;

import enums.AromaVela;

/**
 * Vela tiene un contador estatico numVelas para auto calcular el codigo de
 * producto.
 * 
 * Ademas tiene los atributos String color, float diametro, float altura e int
 * unidades.
 * 
 * Las unidades hacen referencia a cuantas hay de ese tipo de vela.
 * 
 * Por ultimo tiene un atributo del Enum AromaVela, y segun el aroma que una
 * vela reciba, tendra un nombre y un color pre-definidos.
 * 
 * Es una clase final porque con ella termina la herencia (no tiene hijos).
 * 
 * @author erica
 *
 */

public final class Vela extends Producto {

	public static int numVelas = 0;
	private String color;
	private float diametro;
	private float altura;
	private int unidades;
	private AromaVela aroma;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor vacio
	 */
	public Vela() {
		super();
		iva = 10.7F;
		this.aroma = AromaVela.NINGUNO;
	}

	/**
	 * Aqui Vela usa el constructor que no recibe nombre de la clase Producto (el de
	 * dos atributos).
	 * 
	 * @param float     peso
	 * @param float     precio
	 * @param float     diametro
	 * @param float     altura
	 * @param int       unidades
	 * @param AromaVela aroma
	 * 
	 */
	public Vela(float peso, float precio, float diametro, float altura, int unidades, AromaVela aroma) {
		super(peso, precio);
		this.diametro = diametro;
		this.altura = altura;
		this.unidades = unidades;
		this.aroma = aroma;
		iva = 10.7F;
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return String color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param String color
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return float tamRadio
	 */
	public float getDiametro() {
		return diametro;
	}

	/**
	 * @param float tamRadio
	 */
	public void setDiametro(float diametro) {
		this.diametro = diametro;
	}

	/**
	 * @return float tamAltura
	 */
	public float getAltura() {
		return altura;
	}

	/**
	 * @param float tamAltura
	 */
	public void setAltura(float altura) {
		this.altura = altura;
	}

	/**
	 * @return int unidades
	 */
	public int getUnidades() {
		return unidades;
	}

	/**
	 * @param int unidades
	 */
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}

	/**
	 * @return AromaVela aroma
	 */
	public AromaVela getAroma() {
		return aroma;
	}

	/**
	 * @param AromaVela aroma
	 */
	public void setAroma(AromaVela aroma) {
		this.aroma = aroma;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return super.toString() + "|           Aroma: " + this.aroma + "\n|           Color: " + this.color
				+ "\n|           Medidas: " + this.diametro + " cm de diametro y " + this.altura
				+ " cm de altura\n|           Unidades disponibles: " + this.unidades + "\n|           Peso: "
				+ this.peso + "g\n|           IVA aplicado: " + this.iva + "%\n|           Descuento aplicado: "
				+ this.precio.getDescuento() + "\n|";
	}

	/**
	 * Recibe un numero, y obteniendo el valor numerico del Enum AromaVela inicia un
	 * switch para ponerle un nombre y un color a la vela segun el aroma de la vela.
	 * 
	 * @param int n
	 * 
	 */
	@Override
	public void altaProducto(int n) {
		AromaVela aroma = AromaVela.values()[n];
		switch (aroma) {
		case VAINILLA:
			this.setNombre("Caricias de Algodon");
			this.setColor("#FFFCF0");
			break;
		case LIMON:
			this.setNombre("Frescura Vespertina");
			this.setColor("#FDFF90");
			break;
		case FRUTOS_ROJOS:
			this.setNombre("Pasion Encendida");
			this.setColor("#B41E2B");
			break;
		case CANELA:
			this.setNombre("Actividad Matutina");
			this.setColor("#CB9943");
			break;
		default:
			this.setNombre("Vela Estandar para Rituales");
			this.setColor("#000000");
			break;
		}
	}

	/**
	 * Calcular el precio de un producto dependiendo del descuento que se le
	 * aplique. Si no coincide con ninguno, solo se le aplica el IVA. Para vela el
	 * descuento de primavera es del 2.10%, el de Navidad del 2.76% y el de San
	 * Valentin del 7.5%.
	 */
	@Override
	public void calcularPrecioNeto() {
		float precioB = this.precio.getPrecioBruto();
		float precioN;
		switch (this.precio.getDescuento()) {
		case PRIMAVERA:
			precioN = precioB * (1 - iva / 100) * (1 + 2.1F / 100);
		case NAVIDAD:
			precioN = precioB * (1 - iva / 100) * (1 + 2.76F / 100);
		case SAN_VALENTIN:
			precioN = precioB * (1 - iva / 100) * (1 + 7.5F / 100);
		default:
			precioN = precioB * (1 - iva / 100);
		}
		this.precio.setPrecioNeto(precioN);
	}

	/**
	 * Auto asigna un codigo segun el numero de velas existentes al producto vela.
	 */
	@Override
	public void calcularCodigo() {
		if (numVelas < 10) {
			this.setCodigo("VEL00" + numVelas);
		} else if (numVelas >= 10 && numVelas < 100) {
			this.setCodigo("VEL0" + numVelas);
		} else if (numVelas >= 100 && numVelas < 1000) {
			this.setCodigo("VEL" + numVelas);
		}
	}

	/**
	 * Suma el numero que entre (sea positivo o negativo) sin que pase de los
	 * numeros especulados en el metodo calcularCodigo (de 0 a 1000).
	 * 
	 * @see calcularCodigo()
	 */
	@Override
	public void controlContadorProductos(int n) {
		if (numVelas + n > 1000) {
			numVelas = 0;
		} else if (numVelas + n < 0) {
			numVelas = 0;
		} else {
			numVelas += n;
		}
	}

}