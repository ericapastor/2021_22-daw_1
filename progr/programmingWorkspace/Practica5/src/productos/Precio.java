package productos;

import java.time.LocalDate;

import enums.Descuento;

/**
 * La clase Precio ser un objeto de la clase Producto. Tiene un Enum de
 * Descuento, un float como precio bruto y otro float como precio neto.
 * 
 * El descuento se calculara en el constructor de Precio automaticamente cada
 * vez que se cree un nuevo objeto, y dependera de la fecha del dia de hoy.
 * 
 * El precio bruto sera rellenado por teclado o manualmente.
 * 
 * El precio neto sera calculado por cada producto dependiendo si tiene
 * descuento y de su IVA propio.
 * 
 * @author erica
 *
 */

public class Precio {

	private Descuento descuento;
	private float precioBruto;
	private float precioNeto;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor vacio que inicializa un precio a 0 y calcular el descuento de la
	 * epoca actual.
	 */
	public Precio() {
		this.precioBruto = 0.0F;
		this.descuento = this.calcularDescuento();
	}

	/**
	 * Constructor con el unico atributo que puede recibir, que calcula el descuento
	 * que de la epoca actual.
	 * 
	 * @param precio
	 */
	public Precio(float precio) {
		this.precioBruto = precio;
		this.descuento = this.calcularDescuento();
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return Descuento descuento
	 */
	public Descuento getDescuento() {
		return descuento;
	}

	/**
	 * @param Descuento descuento
	 */
	public void setDescuento(Descuento descuento) {
		this.descuento = descuento;
	}

	/**
	 * @return float precioBruto
	 */
	public float getPrecioBruto() {
		return precioBruto;
	}

	/**
	 * @param float precioBruto
	 */
	public void setPrecioBruto(float precioBruto) {
		this.precioBruto = precioBruto;
	}

	/**
	 * @return float precioNeto
	 */
	public float getPrecioNeto() {
		return precioNeto;
	}

	/**
	 * @param float precioNeto
	 */
	public void setPrecioNeto(float precioNeto) {
		this.precioNeto = precioNeto;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "Precio [precioBruto=" + precioBruto + ", descuento=" + descuento + "]";
	}

	/**
	 * Segun el dia actual se auto calcula un descuento que se aplicara a los
	 * productos. Este metodo entra en el constructor de Precio.
	 * 
	 * Del 14 de enero al 15 de febrero es el descuento de San Valentin
	 * 
	 * Del 15 de marzo al 30 de abril es el descuento de Primavera
	 * 
	 * Del 10 de noviembre al 26 de diciembre es el descuento de Navidad
	 * 
	 * @return Descuento descuento
	 * 
	 */
	public Descuento calcularDescuento() {
		// descuento de san valentin del 14 de enero al 15 de febrero
		if (LocalDate.now().getDayOfYear() > 14 && LocalDate.now().getDayOfYear() < 46) {
			return Descuento.SAN_VALENTIN;
			// descuento de privamera del 15 de marzo al 30 de abril
		} else if (LocalDate.now().getDayOfYear() > 74 && LocalDate.now().getDayOfYear() < 121) {
			return Descuento.PRIMAVERA;
			// descuento de navidad del 10 de noviembre al 26 de diciembre
		} else if (LocalDate.now().getDayOfYear() > 313 && LocalDate.now().getDayOfYear() < 361) {
			return Descuento.NAVIDAD;
		} else {
			return Descuento.NINGUNO;
		}
	}

}
