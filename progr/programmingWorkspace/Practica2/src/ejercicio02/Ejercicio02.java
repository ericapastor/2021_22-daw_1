package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	static final String INICIO = "INICIO DEL PROGRAMA\n";
	static final String FIN = "\nFIN DEL PROGRAMA";

	public static void main(String[] args) {

		// Abro el programa
		System.out.println(INICIO);
		Scanner program = new Scanner(System.in);

		// Declaro variables
		String numeroString;
		String primeraParte;
		String segundaParte;
		String segundaParteAlReves = "";
		double acumulador = 0;
		double numeroDouble;
		int contador = 0;
		int num;

		System.out.println("Dime cuantos numeros quieres comprobar.");
		num = program.nextInt();

		// compruebo si es positivo
		if (num > 0) {

			// limpio el buffer
			program.nextLine();

			// pido tantos numeros como he indicado anteriormente
			for (int i = 0; i < num; i++) {
				contador++;
				System.out.println("Dime el numero " + contador);
				numeroString = program.nextLine();
				segundaParteAlReves = "";

				// compruebo si es par o impar

				if (numeroString.length() % 2 == 0) {
					// divido el numero en dos mitades
					primeraParte = numeroString.substring(0, numeroString.length() / 2);
					segundaParte = numeroString.substring(numeroString.length() / 2, numeroString.length());

					// le doy la vuelta a cualquiera de las partes
					for (int j = segundaParte.length() - 1; j >= 0; j--) {
						segundaParteAlReves = segundaParteAlReves + "" + segundaParte.charAt(j);
					}

					// compruebo si son iguales
					if (primeraParte.equals(segundaParteAlReves)) {
						System.out.println("Es capicua.");
					} else {
						System.out.println("No es capicua.");
					}

				} else { // si es impar
					// divido el numero en dos mitades y a la segunda le quito el primer numero
					primeraParte = numeroString.substring(0, numeroString.length() / 2);
					segundaParte = numeroString.substring((numeroString.length() / 2) + 1, numeroString.length());

					// le doy la vuelta a cualquiera de las partes
					for (int j = segundaParte.length() - 1; j >= 0; j--) {
						segundaParteAlReves = segundaParteAlReves + "" + segundaParte.charAt(j);
					}

					// compruebo si son iguales
					if (primeraParte.equals(segundaParteAlReves)) {
						System.out.println("Es capicua.");
					} else {
						System.out.println("No es capicua.");
					}
				}
				// lo convierto a double
				numeroDouble = Double.parseDouble(numeroString);
				// lo a�ado al acumulador
				acumulador = acumulador + numeroDouble;
			}

		} else {
			System.out.println("El numero debe ser positivo");
		}
		if (contador > 0) {
			System.out.println("La media es " + (acumulador / num));
		}

		// Ciero el programa
		program.close();
		System.out.println(FIN);

	}

}
