package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	static final String INICIO = "INICIO DEL PROGRAMA\n";
	static final String FIN = "\nFIN DEL PROGRAMA";

	public static void main(String[] args) {

		// Abro el programa
		System.out.println(INICIO);

		Scanner program = new Scanner(System.in);

		// Declaro variables

		String cadena;
		String password;
		int opcion;
		int contadorAcentos;
		int contadorMayus;
		int contadorMinus;
		int contadorDigit;

		// Muestro el menu de opciones

		do {

			// pido seleccionar una opcion
			System.out.println("************************");
			System.out.println("1 - Contar acentos.");
			System.out.println("2 - Contrase�a correcta.");
			System.out.println("3 - Salir.");
			System.out.println("************************");
			opcion = program.nextInt();

			switch (opcion) {

			// creo un default por si el numero seleccionado no fuera valida
			default:
				System.out.println("Opcion no contemplada.");
				break;
			case 1:
				// declaro aqui el contador a 0 para que se resetee cada vez que la opcion es
				// elegida
				contadorAcentos = 0;
				System.out.println("\nCONTAR ACENTOS\n");
				// limpio el buffer
				program.nextLine();
				// pido cadena
				System.out.println("Dime una frase.");
				cadena = program.nextLine();
				// comparo cada caracter con un for
				for (int i = 0; i < cadena.length(); i++) {
					if (cadena.charAt(i) == '�' || cadena.charAt(i) == '�' || cadena.charAt(i) == '�'
							|| cadena.charAt(i) == '�' || cadena.charAt(i) == '�' || cadena.charAt(i) == '�'
							|| cadena.charAt(i) == '�' || cadena.charAt(i) == '�' || cadena.charAt(i) == '�'
							|| cadena.charAt(i) == '�') {
						contadorAcentos++;
					}
					System.out.println("Numero de acentos: " + contadorAcentos);
				}
				break;
			case 2:
				/*
				 * Para que sea v�lida debe contener al menos 8 caracteres, 1 may�scula, 1
				 * min�scula y 1 digito.
				 */
				// limpio el buffer
				program.nextLine();

				// reseteo los contadores
				contadorMayus = 0;
				contadorMinus = 0;
				contadorDigit = 0;

				System.out.println("Dime la contrase�a:");
				password = program.nextLine();

				// compruebo si al menos hay 8 caracteres
				if (password.length() >= 8) {
					// comparo cada caracter con un for
					for (int i = 0; i < password.length(); i++) {
						if (password.charAt(i) >= 'a' && password.charAt(i) <= 'z') {
							contadorMinus++;
						}
						if (password.charAt(i) >= 'A' && password.charAt(i) <= 'Z') {
							contadorMayus++;
						}
						if (password.charAt(i) >= '0' && password.charAt(i) <= '9') {
							contadorDigit++;
						}
					}
					if (contadorMinus >= 1 && contadorMayus >= 1 && contadorDigit >= 1) {
						System.out.println("Contrase�a v�lida");
					} else {
						System.out.println("Contrase�a no v�lida");
					}
				} else {
					System.out.println("La contrase�a debe tener al menos 8 caracteres.");
				}
				break;
			case 3:
				System.out.println("Gracias. El programa se cerrara.");
				break;
			}
		} while (opcion != 3); // cuando la opcion sea 3 el menu dejara de ejecutarse

		// cierro el programa

		program.close();

		System.out.println(FIN);
	}

}
