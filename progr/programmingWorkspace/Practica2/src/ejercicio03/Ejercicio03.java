package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	static final String INICIO = "INICIO DEL PROGRAMA\n";
	static final String FIN = "\nFIN DEL PROGRAMA";

	public static void main(String[] args) {

		// inicio el programa
		System.out.println(INICIO);
		Scanner program = new Scanner(System.in);

		// declaro variables
		String cadenaFinal;
		String cadena;
		int contadorVocales = 0;
		int contadorMayus = 0;
		int contadorMinus = 0;
		int contadorArrobas = 0;

		// pido la cadena de finalizacion
		System.out.println("Dime la cadena de finalizacionn");
		cadenaFinal = program.nextLine();

		do { // pido mas cadenas
			System.out.println("Dime otra cadena.");
			cadena = program.nextLine();

			for (int i = 0; i < cadena.length(); i++) { // repaso todos los char de la cadena con un for
				// para que los contadores no tengan en cuenta la cadena final, rompo el bucle
				// si esta fuera introducida
				if (cadena.equals(cadenaFinal)) {
					break;
				}
				// comparo cada caracter y en caso de ser los adecuados a�ado +1 al contador
				if (cadena.charAt(i) >= 'A' && cadena.charAt(i) <= 'Z') {
					contadorMayus++;
				}
				if (cadena.charAt(i) >= 'a' && cadena.charAt(i) <= 'z') {
					contadorMinus++;
				}
				if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'e' || cadena.charAt(i) == 'i'
						|| cadena.charAt(i) == 'o' || cadena.charAt(i) == 'u' || cadena.charAt(i) == 'A'
						|| cadena.charAt(i) == 'E' || cadena.charAt(i) == 'I' || cadena.charAt(i) == 'O'
						|| cadena.charAt(i) == 'U') {
					contadorVocales++;
				}
				if (cadena.charAt(i) == '@') {
					contadorArrobas++;
				}
			}

		} while (!cadena.equals(cadenaFinal)); // hasta que la de finalizacion se introduzca

		System.out.println("Estadisticas:\nVocales: " + contadorVocales + "\nLetras mayusculas: " + contadorMayus
				+ "\nLetras minusculas: " + contadorMinus + "\nArrobas: " + contadorArrobas);

		// cierro el programa
		program.close();
		System.out.println(FIN);

	}

}
