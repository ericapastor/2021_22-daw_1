package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	static final String INICIO = "INICIO DEL PROGRAMA\n";
	static final String FIN = "\nFIN DEL PROGRAMA";

	public static void main(String[] args) {

		// Abro el programa
		System.out.println(INICIO);
		Scanner program = new Scanner(System.in);

		// Declaro variables

		int filas;
		int contador;

		System.out.println("Dime el numero de filas");
		filas = program.nextInt();

		// Primero separo los numeros pares de los impares
		if (filas % 2 == 0) {
			filas /= 2;
			contador = 1;
			// primera parte
			for (int i = 0; i < filas; i++) {
				for (int j = 0; j < contador; j++) {
					System.out.print("*");
				}
				contador++;
				System.out.println();
			}
			// segunda parte
			for (int i = filas; i > 0; i--) {
				contador--;
				for (int j = contador; j > 0; j--) {
					System.out.print("*");
				}
				System.out.println();
			}

		} else {
			filas /= 2;
			contador = 1;
			// primera parte
			for (int i = 0; i < filas + 1; i++) {
				for (int j = 0; j < contador; j++) {
					System.out.print("*");
				}
				contador++;
				System.out.println();
			}
			// segunda parte
			contador--;
			for (int i = filas; i > 0; i--) {
				contador--;
				for (int j = contador; j > 0; j--) {
					System.out.print("*");
				}
				System.out.println();
			}
		}

		// Cierro el programa
		program.close();
		System.out.println(FIN);

	}

}
