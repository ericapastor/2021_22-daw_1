package ej02;

import java.util.Scanner;

public class Profesor {

	static Scanner input = new Scanner(System.in);

	/**
	 * @author effy
	 * @version jdk1.8.0_191
	 * @since jdk 1.8
	 */

	String nombre;
	String apellidos;
	int edad;
	String ciclo;

	/**
	 * Declaracion de constructor vacio
	 */

	public Profesor() {
		nombre = "";
		apellidos = "";
		edad = 0;
		ciclo = "";
	}

	/**
	 * Declaracion de constructor con todos los parametros
	 * 
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 */

	public Profesor(String a, String b, int c, String d) {
		this.nombre = a;
		this.apellidos = b;
		this.edad = c;
		this.ciclo = d;
	}

	/**
	 * Declaracion de Getters y Setters
	 * 
	 * @return nombre
	 * @return apellidos
	 * @return edad
	 * @return ciclo
	 */

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	/**
	 * 
	 * En lugar de usar metodos para mostrar profesor vamos a usar el metodo
	 * toString.
	 * 
	 * Cuando un metodo ya existe dentro de Java, como es el toString,
	 * tengo que escribir @Override (importante la O mayuscula) antes de modificarlo.
	 * 
	 * Si no creo el toString() al imprimir el objeto me mostrara algo como esto:
	 * syso(profesor2) --> ej01.Profesor@1b6d3586
	 * 
	 * @Override toString
	 * 
	 */

	@Override
	public String toString() {
		return "Profesor:/nNombre: " + nombre + "\nApellidos: " + apellidos + "\nEdad: " + edad
				+ "\nCiclo que imparte: " + ciclo;
	}

}
