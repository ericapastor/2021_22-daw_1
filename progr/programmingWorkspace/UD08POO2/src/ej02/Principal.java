package ej02;

import ej01.Profesor;

public class Principal {

	public static void main(String[] args) {
		Profesor profe1 = new Profesor("Elena", "Jimenez", 42, "DAM");
		Profesor profe2 = new Profesor("David", "Orera", 34, "DAW");
		Profesor profe3 = new Profesor("Paco", "Martinez Lopez", 37, "DAW");
		Profesor profe4 = new Profesor("Alejandra", "Allue Ramirez", 29, "DAM");
		/**
		 * Muestro los datos del ejercicio anterior, esta vez simplemente usando el
		 * metodo toString Sin embargo, no es realmente necesario escribir toString,
		 * pues Java ya entiende que al tratarse de un objeto estamos llamando a
		 * toString. Vemos abajo que es lo mismo ponerlo de una manera que de otra.
		 */
		System.out.println("Datos del profesor 1:");
		System.out.println(profe1.toString() + "\n");
		System.out.println("Datos del profesor 2:");
		System.out.println(profe2.toString() + "\n");
		System.out.println("Datos del profesor 3:");
		System.out.println(profe3 + "\n");
		System.out.println("Datos del profesor 4:");
		System.out.println(profe4 + "\n");
	}

}
