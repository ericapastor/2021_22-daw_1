package ej04paraLeerNoLoHiceYo;

public class Profesor {
	// atributos
	private String nombre;
	private String apellidos;
	private int edad;
	private String ciclo;

	// constructores
	public Profesor() {
	}

	public Profesor(String nombre, String apellidos, int edad, String ciclo) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.ciclo = ciclo;
	}

	// setter y getter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	@Override
	public String toString() {
		return "Nombre y apellidos " + nombre + " " + apellidos + "\nEdad " + edad + "\nCiclo " + ciclo;
	}

	public void mostrarDatosProfesor(Profesor profesor) {
		System.out.println("Los datos del profesor son ");
		System.out.println(profesor);
	}

}
