package ej04paraLeerNoLoHiceYo;

import ej03.Profesor;

public class Principal {

	public static void main(String[] args) {
		// creamos profesores
		Profesor profesor1 = new Profesor("Juan","Fernandez",33, "DAM");
		Profesor profesor2 = new Profesor("Jose","Garcia",38, "DAW");
		Profesor profesor3 = new Profesor("Carlota","Suarez",29, "DAW");
		Profesor profesor4 = new Profesor("Raquel","Lopez",25, "DAM");
		
		//mostramos profesores
		System.out.println("Profesor1");
		profesor1.mostrarProfesor(profesor1);
		System.out.println("Profesor2");
		profesor2.mostrarProfesor(profesor2);
		System.out.println("Profesor3");
		profesor3.mostrarProfesor(profesor3);
		System.out.println("Profesor4");
		profesor4.mostrarProfesor(profesor4);
		
		//creamos profesor titular
		ProfesorTitular profesorTitular1 = new ProfesorTitular("Maria","Jimenez",45,"DAM",5);
		//mostrarmos datos
		System.out.println("ProfesorTitular1");
		profesorTitular1.mostrarDatosProfesor(profesorTitular1);
		
		//creamos profesor interino
		ProfesorInterino profesorInterino1 = new ProfesorInterino("Alejandro","Martin",25,"DAW",3);
		//mostramos datos
		System.out.println("ProfesorInterino1");
		profesorInterino1.mostrarDatosProfesor(profesorInterino1);
		
	}
}