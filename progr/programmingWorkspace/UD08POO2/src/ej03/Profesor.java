package ej03;

import java.util.Scanner;

public class Profesor {

	static Scanner input = new Scanner(System.in);

	/**
	 * @author effy
	 * @version jdk1.8.0_191
	 * @since jdk 1.8
	 */

	String nombre;
	String apellidos;
	int edad;
	String ciclo;

	/**
	 * Declaracion de constructor vacio
	 */

	public Profesor() {
		nombre = "";
		apellidos = "";
		edad = 0;
		ciclo = "";
	}

	/**
	 * Declaracion de constructor con todos los parametros
	 * 
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 */

	public Profesor(String a, String b, int c, String d) {
		this.nombre = a;
		this.apellidos = b;
		this.edad = c;
		this.ciclo = d;
	}

	/**
	 * Declaracion de Getters y Setters
	 * 
	 * @return nombre
	 * @return apellidos
	 * @return edad
	 * @return ciclo
	 */

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	/**
	 * 
	 * En este caso vamos a usar de nuevo el metodo to String, pero vamos a
	 * integrarlo en un metodo.
	 * 
	 * @Override toString
	 * 
	 */

	@Override
	public String toString() {
		return "Profesor:/nNombre: " + nombre + "\nApellidos: " + apellidos + "\nEdad: " + edad
				+ "\nCiclo que imparte: " + ciclo;
	}

	public void mostrarProfesor(Profesor profesor) {
		System.out.println(profesor);
	}

}
