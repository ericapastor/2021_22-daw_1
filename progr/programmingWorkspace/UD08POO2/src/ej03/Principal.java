package ej03;

import ej01.Profesor;

public class Principal {

	public static void main(String[] args) {
		Profesor profe1 = new Profesor("Elena", "Jimenez", 42, "DAM");
		Profesor profe2 = new Profesor("David", "Orera", 34, "DAW");
		Profesor profe3 = new Profesor("Paco", "Martinez Lopez", 37, "DAW");
		Profesor profe4 = new Profesor("Alejandra", "Allue Ramirez", 29, "DAM");

		System.out.println("Profesor 1:");
		profe1.mostrarProfesor(profe1);
		System.out.println();
		System.out.println("Profesor 2:");
		profe1.mostrarProfesor(profe2);
		System.out.println();
		System.out.println("Profesor 3:");
		profe1.mostrarProfesor(profe3);
		System.out.println();
		System.out.println("Profesor 4:");
		profe1.mostrarProfesor(profe4);
		System.out.println();
	}

}
