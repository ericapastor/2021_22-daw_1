package ej05noEstaHecho;

public class ProfesorInterino extends Profesor {
	//atributos
	private int anyosInterino;
	
	//constructores
	public ProfesorInterino() {
		
	}
	
	public ProfesorInterino(String nombre, String apellidos, int edad,
			String ciclo, int anyosInterino) {
		super(nombre, apellidos, edad, ciclo);
		this.anyosInterino=anyosInterino;
	}
	

	//setter y getter
	public int getAnyosInterino() {
		return anyosInterino;
	}


	public void setAnyosInterino(int anyosInterino) {
		this.anyosInterino = anyosInterino;
	}

	@Override
	public String toString() {
		return "ProfesorInterino " +super.toString() +" \nanyosInterino=" + anyosInterino;
	}
	
	
}