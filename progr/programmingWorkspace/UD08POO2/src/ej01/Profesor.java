package ej01;

import java.util.Scanner;

public class Profesor {

	static Scanner input = new Scanner(System.in);

	/**
	 * @author effy
	 * @version jdk1.8.0_191
	 * @since jdk 1.8
	 */

	String nombre;
	String apellidos;
	int edad;
	String ciclo;

	/**
	 * Declaracion de constructor vacio
	 */

	public Profesor() {
		nombre = "";
		apellidos = "";
		edad = 0;
		ciclo = "";
	}

	/**
	 * Declaracion de constructor con todos los parametros
	 * 
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 */

	public Profesor(String a, String b, int c, String d) {
		this.nombre = a;
		this.apellidos = b;
		this.edad = c;
		this.ciclo = d;
	}

	/**
	 * Declaracion de Getters y Setters
	 * 
	 * @return nombre
	 * @return apellidos
	 * @return edad
	 * @return ciclo
	 */

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	/**
	 * Metodos para rellenar y mostrar profesor
	 * 
	 * @param profe
	 */

	public void rellenarProfesor(Profesor profe) {
		System.out.println("Dime el nombre");
		this.setNombre(input.nextLine());
		System.out.println("Dime los apellidos");
		this.setApellidos(input.nextLine());
		System.out.println("Dime la edad");
		this.setEdad(input.nextInt());
		input.nextLine();
		System.out.println("Dime que ciclo imparte");
		this.setCiclo(input.nextLine());
	}

	public void mostrarProfesor(Profesor profe) {
		System.out.println("Nombre: " + profe.getNombre());
		System.out.println("Apellidos: " + profe.getApellidos());
		System.out.println("Edad: " + profe.getEdad());
		System.out.println("Ciclo que imparte: " + profe.getCiclo());
	}

}
