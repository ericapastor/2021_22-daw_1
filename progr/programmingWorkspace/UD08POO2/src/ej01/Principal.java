package ej01;

public class Principal {

	public static void main(String[] args) {
		Profesor profe1 = new Profesor();
		profe1.rellenarProfesor(profe1);
		Profesor profe2 = new Profesor();
		profe2.rellenarProfesor(profe2);
		Profesor profe3 = new Profesor("Paco", "Martinez Lopez", 37, "DAW");
		Profesor profe4 = new Profesor("Alejandra", "Allue Ramirez", 29, "DAM");
		System.out.println("Datos del profesor 1:");
		profe1.mostrarProfesor(profe1);
		System.out.println();
		System.out.println("Datos del profesor 2:");
		profe2.mostrarProfesor(profe2);
		System.out.println();
		System.out.println("Datos del profesor 3:");
		profe3.mostrarProfesor(profe3);
		System.out.println();
		System.out.println("Datos del profesor 4:");
		profe4.mostrarProfesor(profe4);
		System.out.println();
	}

}
