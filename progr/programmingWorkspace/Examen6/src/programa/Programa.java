package programa;

import clases.Ikea;

public class Programa {

	public static void main(String[] args) {

		Ikea ikea = new Ikea();
		// altas
		System.out.println("Alta de trabajadores y mostrar:");
		ikea.altaTrabajador("12312312Z", "Pepito", "Conductor de carritos");
		ikea.altaTrabajador("12341234R", "Luisa", "Cajera");
		ikea.altaTrabajador("12312345J", "Raquel", "Manitas");
		ikea.listarTrabajadores();
		System.out.println("\nAlta de jefes de seccion y mostrar:");
		ikea.altaJefeSeccion("34534534O", "Juana", "4B", 200);
		ikea.altaJefeSeccion("12345678P", "Xander", "3A", 150);
		ikea.altaJefeSeccion("78345013T", "Urtecho", "1C", 450);
		ikea.listarJefesDeSeccion();
		System.out.println("\nAlta departamentos y mostrar departamentos:");
		ikea.altaDepartamento("D012", "Decoracion", "34534534O");
		ikea.altaDepartamento("D005", "Muebles de cocina", "12345678P");
		ikea.listarDepartamentos();
		System.out.println("\nAsignar trabajadores a departamentos y mostrar departamentos de nuevo:");
		ikea.registrarDepartamentoTrabajador("D012", "12312312Z");
		ikea.registrarDepartamentoTrabajador("D012", "12341234R");
		ikea.registrarDepartamentoTrabajador("D005", "12312312Z");
		ikea.registrarDepartamentoTrabajador("D005", "12312345J");
		ikea.listarDepartamentos();
		// pruebas
		System.out.println("\nInsertar trabajador con DNI existente:");
		ikea.altaTrabajador("34534534O", "Kim", "Guia turistica");
		System.out.println("\nInsertar jefe de seccion con DNI existente:");
		ikea.altaJefeSeccion("34534534O", "Horacia", "I9", 100);
		System.out.println("\nInsertar un departamento con codigo ya existente:");
		ikea.altaDepartamento("D012", "Decoracion", "34534534O");
		System.out.println("\nInsertar un departamente con DNI inexistente:");
		ikea.altaDepartamento("D09", "Ropa de cama", "wskjenf89234");
		System.out.println("\nInsertar un departamento con DNI de un trabajador:");
		ikea.altaDepartamento("A8", "Iluminacion", "12312312Z");
		System.out.println("\nAsignar un jefe de seccion a la lista de trabajadores de un departamento:");
		ikea.registrarDepartamentoTrabajador("D012", "34534534O");
		System.out.println("\nAsignar un trabajador a un departamento inexistente:");
		ikea.registrarDepartamentoTrabajador("E003", "12341234R");
		System.out.println("\nAsignar a un departamento un DNI inexistente");
		ikea.registrarDepartamentoTrabajador("D012", "289347kjadncw");
		// ficheros
		System.out.println("\nGuardando en fichero...");
		ikea.guardarDatosFicheros();
		System.out.println("\nCargando desde fichero...");
		ikea.cargarDatosFicheros();
		// base de datos
		/*System.out.println("\nConectar con base de datos, insertar datos");
		ikea.conectarBBDD();
		ikea.insertarTrabajadoresBBDD();
		ikea.insertarJefesSeccionBBDD();
		System.out.println("\nMostrar trabajadores");
		ikea.mostrarTrabajadoresBBDD();
		System.out.println("\nBorrar trabajador \"Raquel\" y volver a mostrar");
		ikea.eliminarTrabajadores("12312345J");
		ikea.mostrarTrabajadoresBBDD();
		System.out.println("\nBorrar trabajador\"Pepito\" y volver a mostrar");
		ikea.eliminarTrabajadores("12312312Z");
		ikea.mostrarTrabajadoresBBDD();
		*/
	}

}
