package clases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class Ikea implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String datosDepartamentos = "src/datosDepartamentos.dat";
	private final String datosPersonal = "src/datosPersonal.dat";
	private Connection conexion;

	private ArrayList<Personal> personal;
	private ArrayList<Departamento> departamentos;

	public Ikea() {
		personal = new ArrayList<Personal>();
		departamentos = new ArrayList<Departamento>();
	}

	public ArrayList<Personal> getPersonal() {
		return personal;
	}

	public void setPersonal(ArrayList<Personal> personal) {
		this.personal = personal;
	}

	public ArrayList<Departamento> getDepartamentos() {
		return departamentos;
	}

	public void setDepartamentos(ArrayList<Departamento> departamentos) {
		this.departamentos = departamentos;
	}

	// buscar

	private Personal buscarPersonal(String dni) {
		for (Personal p : personal) {
			if (p != null && p.getDni().equalsIgnoreCase(dni)) {
				return p;
			}
		}
		return null;
	}

	private Departamento buscarDepartamento(String codigo) {
		for (Departamento t : departamentos) {
			if (t != null && t.getCodigo().equalsIgnoreCase(codigo)) {
				return t;
			}
		}
		return null;
	}

	// altas

	public void altaTrabajador(String dni, String nombre, String categoria) {
		if (buscarPersonal(dni) != null) {
			System.out.println("Ya existe alguien con ese dni");
		} else {
			personal.add(new Trabajador(dni, nombre, categoria));
			Collections.sort(personal);
		}
	}

	public void altaJefeSeccion(String dni, String nombre, String seccion, int suplemento) {
		if (buscarPersonal(dni) != null) {
			System.out.println("Ya existe alguien con ese dni");
		} else {
			personal.add(new JefeDeSeccion(dni, nombre, seccion, suplemento));
			Collections.sort(personal);
		}
	}

	public void altaDepartamento(String codigo, String nombre, String dni) {
		if (buscarDepartamento(codigo) != null) {
			System.out.println("Ya existe un departamento con ese codigo");
		} else if (buscarPersonal(dni) == null) {
			System.out.println("No existe nadie con ese dni");
		} else if (!(buscarPersonal(dni) instanceof JefeDeSeccion)) {
			System.out.println("El DNI facilitado no corresponde a un jefe de seccion");
		} else {
			departamentos.add(new Departamento(codigo, nombre, (JefeDeSeccion) buscarPersonal(dni)));
			Collections.sort(departamentos);
		}
	}

	// listar

	public void listarTrabajadores() {
		for (Personal p : personal) {
			if (p != null && p instanceof Trabajador) {
				System.out.println(p);
			}
		}
	}

	public void listarJefesDeSeccion() {
		for (Personal p : personal) {
			if (p != null && p instanceof JefeDeSeccion) {
				System.out.println(p);
			}
		}
	}

	public void listarDepartamentos() {
		for (Departamento t : departamentos) {
			System.out.println(t);
		}

	}

	// registrar un trabajador en departamento

	public void registrarDepartamentoTrabajador(String codigo, String dni) {
		if (buscarDepartamento(codigo) == null) {
			System.out.println("No existe ningun departamento con ese codigo");
		} else if (buscarPersonal(dni) == null) {
			System.out.println("No existe nadie con ese DNI");
		} else if (!(buscarPersonal(dni) instanceof Trabajador)) {
			System.out.println("El DNI introducido no corresponde a un trabajador");
		} else {
			buscarDepartamento(codigo).getTrabajadores().add((Trabajador) buscarPersonal(dni));
			Collections.sort(buscarDepartamento(codigo).getTrabajadores());
		}
	}

	/*
	 * FICHEROS
	 */

	// guardar datos

	private void guardarFicheroPersonal() throws FileNotFoundException, IOException {
		ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(datosPersonal));
		escritor.writeObject(personal);
		escritor.close();
	}

	private void guardarFicheroDepartamentos() throws FileNotFoundException, IOException {
		ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(datosDepartamentos));
		escritor.writeObject(departamentos);
		escritor.close();
	}

	public void guardarDatosFicheros() {
		try {
			guardarFicheroPersonal();
			guardarFicheroDepartamentos();
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	// cargar datos de fichero

	@SuppressWarnings("unchecked")
	private void cargarFicheroPersonal() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream lector = new ObjectInputStream(new FileInputStream(datosPersonal));
		personal = (ArrayList<Personal>) lector.readObject();
		lector.close();
	}

	@SuppressWarnings("unchecked")
	private void cargarFicheroDepartamentos() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream lector = new ObjectInputStream(new FileInputStream(datosDepartamentos));
		departamentos = (ArrayList<Departamento>) lector.readObject();
		lector.close();
	}

	public void cargarDatosFicheros() {
		try {
			cargarFicheroPersonal();
			cargarFicheroDepartamentos();
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	// base de datos
	public void conectarBBDD() {
		String servidor = "jdbc:mysql://localhost:3306/ikea";
		try {
			conexion = DriverManager.getConnection(servidor, "root", "");
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void insertarTrabajadoresBBDD() {
		String query = "INSERT INTO trabajadores (dni, nombre, categoria) VALUES (?, ?, ?);";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			for (Personal p : personal) {
				if (p != null && p instanceof Trabajador) {
					sentencia.setString(1, p.getDni());
					sentencia.setString(2, p.getNombre());
					sentencia.setString(3, ((Trabajador) p).getCategoria());
					sentencia.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void insertarJefesSeccionBBDD() {
		String query = "INSERT INTO jefeSeccion (dni, nombre, seccion, suplemento) VALUES (?, ?, ?, ?);";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			for (Personal p : personal) {
				if (p != null && p instanceof JefeDeSeccion) {
					sentencia.setString(1, p.getDni());
					sentencia.setString(2, p.getNombre());
					sentencia.setString(3, ((JefeDeSeccion) p).getSeccion());
					sentencia.setInt(4, ((JefeDeSeccion) p).getSuplemento());
					sentencia.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void mostrarTrabajadoresBBDD() {
		String query = "SELECT * FROM trabajadores;";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				System.out.println("DNI: " + resultado.getString(2) + ", nombre: " + resultado.getString(3)
						+ ", categoria: " + resultado.getString(4));
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void eliminarTrabajadores(String dni) {
		String query = "DELETE FROM trabajadores WHERE dni=?;";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			for (Personal p : personal) {
				if (p.getDni().equalsIgnoreCase(dni) && p instanceof Trabajador) {
					sentencia.setString(1, p.getDni());
					sentencia.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
