package clases;

import java.io.Serializable;
import java.util.ArrayList;

public class Departamento implements Comparable<Departamento>, Serializable {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private String nombre;
	private JefeDeSeccion jefe;
	private ArrayList<Trabajador> trabajadores;

	public Departamento(String codigo, String nombre, JefeDeSeccion jefe) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.jefe = jefe;
		trabajadores = new ArrayList<Trabajador>();
	}

	@Override
	public int compareTo(Departamento o) {
		return codigo.compareTo(o.codigo);
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "Departamento [codigo=" + codigo + ", nombre=" + nombre + ", jefe=" + jefe + ", trabajadores="
				+ (trabajadores != null ? trabajadores.subList(0, Math.min(trabajadores.size(), maxLen))
						: "departamento sin trabajadores")
				+ "]";
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public JefeDeSeccion getJefe() {
		return jefe;
	}

	public void setJefe(JefeDeSeccion jefe) {
		this.jefe = jefe;
	}

	public ArrayList<Trabajador> getTrabajadores() {
		return trabajadores;
	}

	public void setTrabajadores(ArrayList<Trabajador> trabajadores) {
		this.trabajadores = trabajadores;
	}

}
