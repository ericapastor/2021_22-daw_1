package clases;

import java.io.Serializable;

public class JefeDeSeccion extends Personal implements Serializable {

	private static final long serialVersionUID = 1L;

	private String seccion;
	private int suplemento;

	public JefeDeSeccion(String dni, String nombre, String seccion, int suplemento) {
		super(dni, nombre);
		this.seccion = seccion;
		this.suplemento = suplemento;
		this.incentivo = 300;
		this.salario = calcularSalario();
	}

	@Override
	public String toString() {
		return "JefeDeSeccion [seccion=" + seccion + ", suplemento=" + suplemento + super.toString() + "]";
	}

	@Override
	public int compareTo(Personal o) {
		return dni.compareTo(o.dni);
	}

	@Override
	int calcularSalario() {
		return salarioBase + incentivo + suplemento;
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public int getSuplemento() {
		return suplemento;
	}

	public void setSuplemento(int suplemento) {
		this.suplemento = suplemento;
	}

}
