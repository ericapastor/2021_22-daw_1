package clases;

import java.io.Serializable;

public class Trabajador extends Personal implements Serializable {

	private static final long serialVersionUID = 1L;

	private String categoria;

	public Trabajador(String dni, String nombre, String categoria) {
		super(dni, nombre);
		this.categoria = categoria;
		this.incentivo = 100;
		this.salario = calcularSalario();
	}

	@Override
	public String toString() {
		return "Trabajador [categoria=" + categoria + super.toString() + "]";
	}

	@Override
	public int compareTo(Personal o) {
		return dni.compareTo(o.dni);
	}

	@Override
	int calcularSalario() {
		return salarioBase + incentivo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

}
