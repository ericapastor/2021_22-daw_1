package clases;

import java.io.Serializable;

public abstract class Articulo implements Comparable<Articulo>, Serializable {

	private static final long serialVersionUID = 1L;

	private String titulo;
	private String editorial;
	private String isbn;

	public Articulo(String titulo, String editorial, String isbn) {
		this.titulo = titulo;
		this.editorial = editorial;
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	@Override
	public String toString() {
		return "Articulo [titulo=" + titulo + ", editorial=" + editorial + ", isbn=" + isbn + "]";
	}

	abstract int puntos();

}
