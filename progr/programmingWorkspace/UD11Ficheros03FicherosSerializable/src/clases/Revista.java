package clases;

import java.io.Serializable;

public class Revista extends Articulo implements Serializable {

	private static final long serialVersionUID = 1L;

	static final int PUNTOS = 2;
	private boolean online;

	public Revista(String titulo, String editorial, String isbn, boolean online) {
		super(titulo, editorial, isbn);
		this.online = online;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	@Override
	public String toString() {
		return "Revista [online=" + online + "]";
	}

	@Override
	public int compareTo(Articulo o) {
		return getIsbn().compareTo(o.getIsbn());
	}

	@Override
	int puntos() {
		return PUNTOS;
	}

}
