package clases;

import java.io.Serializable;
import java.time.LocalDate;

public class Socio implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String nombre;
	private LocalDate fechaAlta;

	public Socio(int id, String nombre, LocalDate fechaAlta) {
		this.id = id;
		this.nombre = nombre;
		this.fechaAlta = fechaAlta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Override
	public String toString() {
		return "Socio [id=" + id + ", nombre=" + nombre + ", fechaAlta=" + fechaAlta + "]";
	}

}
