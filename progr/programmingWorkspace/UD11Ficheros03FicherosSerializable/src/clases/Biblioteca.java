package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class Biblioteca implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final int DIAS_PRESTAMO = 15;
	static final String datos = "src/datos.dat";

	private ArrayList<Articulo> articulos;
	private ArrayList<Socio> socios;
	private ArrayList<Prestamo> prestamos;

	public Biblioteca() {
		articulos = new ArrayList<Articulo>();
		socios = new ArrayList<Socio>();
		prestamos = new ArrayList<Prestamo>();
	}

	public void altaSocio(String nombre) {
		socios.add(new Socio(socios.size() + 1, nombre, LocalDate.now()));
	}

	// alta libros
	public void altaArticulo(String isbn, String titulo, String editorial, String autor) {
		articulos.add(new Libro(isbn, titulo, editorial, autor));
		Collections.sort(articulos);
	}

	// alta revistas
	public void altaArticulo(String isbn, String titulo, String editorial, boolean online) {
		articulos.add(new Revista(isbn, titulo, editorial, online));
		Collections.sort(articulos);
	}

	public void crearPrestamosSocio(int id) {
		if (!socioExiste(id)) {
			System.out.println("El socio no existe");
		} else {
			prestamos.add(new Prestamo(prestamos.size() + 1, LocalDate.now(), LocalDate.now().plusDays(DIAS_PRESTAMO),
					obtenerSocio(id)));
		}
	}

	public void mostrarPrestamosSocio(int id) {
		for (Prestamo p : prestamos) {
			if (p.getSocio().equals(obtenerSocio(id))) {
				System.out.println(p);
			}
		}
	}

	public void nuevoArticuloPrestamo(int id, String isbn) {
		if (!prestamoExiste(id)) {
			System.out.println("El prestamo no existe.");
		} else {
			if (!articuloExiste(isbn)) {
				System.out.println("El articulo no existe.");
			} else {
				if (!obtenerPrestamo(id).comprobarArticulos(isbn)) {
					obtenerPrestamo(id).articulos.add(obtenerArticulo(isbn));
				} else {
					System.out.println("El articulo ya esta prestado.");
				}
			}
		}
	}

	public void listarPrestamos() {
		for (Prestamo p : prestamos) {
			System.out.println(p);
		}
	}

	private Socio obtenerSocio(int id) {
		for (Socio s : socios) {
			if (s.getId() == id) {
				return s;
			}
		}
		return null;
	}

	private Prestamo obtenerPrestamo(int id) {
		for (Prestamo p : prestamos) {
			if (p.getId() == id) {
				return p;
			}
		}
		return null;
	}

	private Articulo obtenerArticulo(String isbn) {
		for (Articulo a : articulos) {
			if (a.getIsbn().equals(isbn)) {
				return a;
			}
		}
		return null;
	}

	private boolean socioExiste(int id) {
		for (Socio s : socios) {
			if (s.getId() == id) {
				return true;
			}
		}
		return false;
	}

	private boolean prestamoExiste(int id) {
		for (Prestamo p : prestamos) {
			if (p.getId() == id) {
				return true;
			}
		}
		return false;
	}

	private boolean articuloExiste(String isbn) {
		for (Articulo a : articulos) {
			if (a.getIsbn().equals(isbn)) {
				return true;
			}
		}
		return false;
	}

	// cargar y guardar datos
	void guardarDatos() {
		try {
			ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(new File(datos)));
			f.writeObject(articulos);
			f.writeObject(prestamos);
			f.writeObject(socios);
			f.close();
		} catch (IOException e) {
			System.out.println("Error de entrada de datos");
			System.exit(0);
		}
	}

	@SuppressWarnings("unchecked")
	void cargarDatos() {
		try {
			ObjectInputStream f = new ObjectInputStream(new FileInputStream(new File(datos)));
			articulos = (ArrayList<Articulo>) f.readObject();
			prestamos = (ArrayList<Prestamo>) f.readObject();
			socios = (ArrayList<Socio>) f.readObject();
			f.close();
		} catch (IOException e) {
			System.out.println("Error de entrada de datos");
			System.exit(0);
		} catch (ClassNotFoundException e) {
			System.out.println("Clase no encontrada");
		}
	}

}
