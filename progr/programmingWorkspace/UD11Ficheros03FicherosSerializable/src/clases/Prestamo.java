package clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Prestamo implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private LocalDate fechaPrestamo;
	private LocalDate fechaDevolucion;
	private Socio socio;
	protected ArrayList<Articulo> articulos;

	public Prestamo(int idPrestamo, LocalDate fechaPrestamo, LocalDate fechaDevolucion, Socio socio) {
		this.id = idPrestamo;
		this.fechaPrestamo = fechaPrestamo;
		this.fechaDevolucion = fechaDevolucion;
		this.socio = socio;
		articulos = new ArrayList<Articulo>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(LocalDate fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	public LocalDate getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(LocalDate fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public ArrayList<Articulo> getArticulos() {
		return articulos;
	}

	public void setArticulos(ArrayList<Articulo> articulos) {
		this.articulos = articulos;
	}

	public int calcularPuntos() {
		int puntos = 0;
		for (Articulo a : articulos) {
			puntos += a.puntos();
		}
		return puntos;
	}

	public boolean comprobarArticulos(String isbn) {
		for (Articulo a : articulos) {
			if (a.getIsbn().equals(isbn)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "Prestamos [idPrestamo=" + id + ", fechaPrestamo=" + fechaPrestamo + ", fechaDevolucion="
				+ fechaDevolucion + ", socio=" + socio + ", articulos="
				+ (articulos != null ? articulos.subList(0, Math.min(articulos.size(), maxLen)) : null) + "]";
	}

}
