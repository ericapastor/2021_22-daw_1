package clases;

import java.io.Serializable;

public class Libro extends Articulo implements Serializable {

	private static final long serialVersionUID = 1L;

	static final int PUNTOS = 5;
	private String autor;

	public Libro(String titulo, String editorial, String isbn, String autor) {
		super(titulo, editorial, isbn);
		this.autor = autor;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	@Override
	public String toString() {
		return "Libro [autor=" + autor + super.toString() + "]";
	}

	@Override
	public int compareTo(Articulo o) {
		return getIsbn().compareTo(o.getIsbn());
	}

	@Override
	int puntos() {
		return PUNTOS;
	}
}
