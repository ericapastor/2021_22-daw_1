package ej04;


import java.util.Scanner;

import ej03.Ej03;

public class Ej04 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("How many characters you wish to ask for?");
		int opt = program.nextInt();
		char randomChars[] = new char[opt];
		randomChars = askForChar(opt);
		Ej03.vowelsChange(randomChars);
		System.out.println();
		for (int i = 0; i < randomChars.length; i++) {
			System.out.print( randomChars[i]);
		}
		program.close();
	}

	static char[] askForChar(int n) {
		char array[] = new char[n];
		for (int i = 0; i < array.length; i++) {
			array[i] = (char) ((Math.random() * ('z' - 'a' + 1)) + 'a');
			System.out.print(array[i] + " ");
		}
		return array;
	}

}
