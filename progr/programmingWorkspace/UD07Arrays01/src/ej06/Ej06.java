package ej06;

import java.util.Scanner;

public class Ej06 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Programa que pida por teclado n�meros y rellene dos arrays de enteros de 10
		 * elementos (20 n�meros). Posteriormente llamar� a un m�todo que recibe ambos
		 * arrays y que crear� y devolver� un array de 10 elementos organizados de la
		 * siguiente forma: El elemento 0 es la suma de los elementos 0 de ambos arrays,
		 * el elemento 1 es la suma de los elementos 1 de ambos arrays, etc. Para
		 * obtener el array con la suma de los otros 2 debo usar un bucle.
		 */
		int[] num1 = new int[10];
		int[] num2 = new int[10];
		int[] total = new int[10];
		for (int i = 0; i < num1.length; i++) {
			System.out.println("Dame el valor de la posicion " + (i + 1));
			num1[i] = input.nextInt();
		}
		for (int i = 0; i < num2.length; i++) {
			System.out.println("Dame el valor de la posicion " + (i + 1));
			num2[i] = input.nextInt();
		}
		total = dosArrays(num1, num2);
		for (int i = 0; i < total.length; i++) {
			System.out.println("La posicion " + (i + 1) + " es " + total[i]);
		}
		input.close();
	}

	static int[] dosArrays(int[] array1, int[] array2) {
		int[] arrayTotal = new int[10];
		for (int i = 0; i < arrayTotal.length; i++) {
			arrayTotal[i] = array1[i] + array2[i];
		}
		return arrayTotal;
	}

}
