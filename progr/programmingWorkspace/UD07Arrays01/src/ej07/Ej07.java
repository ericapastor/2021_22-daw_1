package ej07;

import java.util.Scanner;

public class Ej07 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Programa que pide un tama�o de array al usuario y posteriormente rellena ese
		 * array con Strings pidi�ndolas al usuario. Despu�s llama al m�todo
		 * desplazar(String[] array, int desplazamiento) y mueve cada string tantas
		 * posiciones adelante como indica desplazamiento. Por ejemplo, si
		 * desplazamiento es 3, el string de la posici�n 0 pasar� a la posici�n 3, y as�
		 * sucesivamente. Cuando nos pasamos de la longitud del array, debemos
		 * colocarlos al principio. Despu�s el programa principal lo debe mostrar por
		 * pantalla.
		 */
		System.out.println("Como de largo quieres hacer el array?");
		int option = input.nextInt();
		String[] array = new String[option];
		input.nextLine();
		array = leerArray(option);
		System.out.println("Cuantas posiciones lo quieres desplazar?");
		int desplazamiento = input.nextInt();
		array = desplazar(array, desplazamiento);
		input.close();
	}

	static String[] leerArray(int n) {
		String[] array = new String[n];
		for (int i = 0; i < array.length; i++) {
			System.out.println("Dime la frase " + (i + 1));
			array[i] = input.nextLine();
		}
		return array;
	}

	// encontrar mejor metodo que el de la profesora
	static String[] desplazar(String[] array, int desplazamiento) {
		for (int i = 0; i < array.length; i++) {
		}
		return array;
	}

}
