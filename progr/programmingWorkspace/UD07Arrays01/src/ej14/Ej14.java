package ej14;

import java.util.Scanner;

public class Ej14 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Program that asks for a students name and age (5 students). These should be
		 * stored in a 2x5 matrix. Afterwards, data will be shown.
		 */
		String[][] matrix = new String[2][5];
		String option;
		do {
			matrix = askForData();
			showData(matrix);
			System.out.println("Read more students? Type 'no' to stop the program.");
			option = input.nextLine();
		} while (!option.equals("no"));
		input.close();
	}

	static String[][] askForData() {
		String[][] matrix = new String[2][5];
		for (int i = 0; i < 5; i++) {
			System.out.println("Name of the student number " + (i + 1));
			matrix[0][i] = input.nextLine();
			System.out.println("Age of the student number " + (i + 1));
			matrix[1][i] = input.nextLine();
		}
		return matrix;
	}

	static void showData(String[][] matrix) {
		for (int i = 0; i < 5; i++) {
			System.out.print("Nombre: " + matrix[0][i] + " - ");
			System.out.println("Edad: " + matrix[1][i] + ".");
		}
	}

}
