package ej00;

public class Methods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static void showMatrix(int[][] matrix) {
		System.out.println();
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("| ");
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] < 10) {
					System.out.print(" " + matrix[i][j] + " | ");
				} else {
					System.out.print(matrix[i][j] + " | ");
				}
			}
			System.out.println();
		}
	}

	public static void showMatrixUnder100(int[][] matrix) {
		for (int i = 0; i < matrix.length + 1; i++) {

			if (i > 0) {
				System.out.print(" [j] ");
			} else {
				System.out.print("     ");
			}

		}
		System.out.println();
		for (int i = 0; i < matrix.length + 1; i++) {

			if (i > 0) {
				System.out.print("-----");
			} else {
				System.out.print("     ");
			}

		}
		System.out.println();
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[i] " + "| ");
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] < 10) {
					System.out.print(" " + matrix[i][j] + " | ");
				} else {
					System.out.print(matrix[i][j] + " | ");
				}
			}
			System.out.println();
			for (int k = 0; k < matrix.length + 1; k++) {

				if (k > 0) {
					System.out.print("-----");
				} else {
					System.out.print("     ");
				}

			}
			System.out.println();
		}
	}

}
