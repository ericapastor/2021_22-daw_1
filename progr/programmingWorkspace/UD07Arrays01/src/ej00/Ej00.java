package ej00;

import java.util.Scanner;

public class Ej00 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {

		int myArray[] = new int[5];
		for (int i = 0; i < myArray.length; i++) {
			System.out.println("Give me number " + (i + 1));
			myArray[i] = program.nextInt();
		}
		int sum = sum(myArray);
		double average = average(sum);
		int max = max(myArray);
		int min = min(myArray);
		System.out.println("Average value is: " + average);
		System.out.println("Maximum value is " + max);
		System.out.println("Minimum value is " + min);
		program.close();
	}

	static int sum(int array[]) {
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		System.out.println("Sum is: " + sum);
		return sum;
	}

	static double average(int sum) {
		double average = 0;
		average = sum / 5;
		return average;
	}

	static int max(int[] array) {
		int max = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}

	static int min(int[] array) {
		int min = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}
		return min;
	}

}
