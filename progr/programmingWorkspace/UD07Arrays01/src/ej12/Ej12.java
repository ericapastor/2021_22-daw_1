package ej12;

import java.util.Scanner;

import ej00.Methods;

public class Ej12 {

	// Program that asks for a number to create a squared matrix (5x5, 4x4, 7x7)
	// Afterwards it calls for a method that receives the matrix, 
	// filling it by scanner and then showing it in print
	// Later on, switch columns for rows and show the matrix again
	
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("How many sides should there be in the matrix?");
		int n = input.nextInt();
		int matrix[][] = new int[n][n];
		fillMatrix(matrix);
		System.out.println("Show matrix as default.");
		Methods.showMatrixUnder100(matrix);
		matrix = switchMatrix(matrix, n);
		System.out.println("Show matrix with rows and columns switched.");
		Methods.showMatrixUnder100(matrix);
		input.close();
	}

	static void fillMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.println("Give me a value to fill this");
				matrix[i][j] = input.nextInt();
			}
		}
	}

	static int[][] switchMatrix(int[][] matrix, int n) {
		int[][] newMatrix = new int[n][n];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				newMatrix[i][j] = matrix[j][i];
			}
		}
		return newMatrix;
	}

}
