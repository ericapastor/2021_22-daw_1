
/* Program that asks for numbers and fills 2 arrays of 10 numbers (20 total) */
/* It will call for a method that returns an array of 10 numbers: */
/*
 * Element 0 is the sum between elements 0 and 9, element 1 the sum between
 * elements 1 and 8, etc.
 */

package ej08;

import java.util.Scanner;

public class Ej08 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int[] num1 = new int[10];
		int[] num2 = new int[10];
		int[] numT = new int[10];
		System.out.println("Primera cadena de numeros:");
		for (int i = 0; i < num1.length; i++) {
			System.out.println("Dame el numero de posicion " + (i + 1));
			num1[i] = input.nextInt();
		}
		System.out.println("Segunda cadena de numeros:");
		for (int i = 0; i < num2.length; i++) {
			System.out.println("Dame el numero de posicion " + (i + 1));
			num2[i] = input.nextInt();
		}
		numT = num(num1, num2);
		for (int i = 0; i < numT.length; i++) {
			System.out.println("La posicion " + (i + 1) + " de la suma de estos arrays tiene el valor: " + numT[i]);
		}
		input.close();
	}

	static int[] num(int[] num1, int[] num2) {
		int[] numT = new int[10];
		for (int i = 0; i < num2.length; i++) {
			numT[i] = num1[i] + num2[num2.length - 1 - i];
		}
		return numT;
	}
}
