package ej01;

import java.util.Scanner;

public class Ej01 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {

		/*
		 * Create a program that asks for 10 whole numbers and then shows them on a
		 * line, from the last one to the first one.
		 */

		int option = option();
		int nums[] = numbers(option);
		int result[] = upsideDown(nums);
		System.out.println("Array:");
		printArray(nums);
		System.out.println("\nArray upside down:");
		printArray(result);

		program.close();
	}

	static int option() {
		int op;
		System.out.println("Choose how many numbers you wish to input.");
		op = program.nextInt();
		return op;
	}

	static int[] numbers(int n) {
		int[] numbers = new int[n];
		for (int i = 0; i < numbers.length; i++) {
			System.out.println("Input number " + (i + 1));
			numbers[i] = program.nextInt();
		}
		return numbers;
	}

	static int[] upsideDown(int array[]) {
		int[] magic = new int[array.length];
		int counter = 0;
		for (int i = (array.length - 1); i >= 0; i--) {
			magic[counter] = array[i];
			counter++;
		}
		return magic;
	}

	static void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]);
		}
	}

}
