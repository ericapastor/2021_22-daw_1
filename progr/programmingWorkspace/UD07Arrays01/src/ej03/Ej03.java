package ej03;

public class Ej03 {

	public static void main(String[] args) {
		/* Show characters from a to k and then switch vowels for the character '*' */
		char letras[] = new char[11];
		char character = 'a';
		System.out.println("The array without previous change is:");
		for (int i = 0; i < letras.length; i++) {
			letras[i] = character++;
			System.out.print(letras[i]);
		}
		System.out.println("\nThe array with vowels changed is: ");
		letras = vowelsChange(letras);
		for (int i = 0; i < letras.length; i++) {
			System.out.println(letras[i]);
		}
	}

	public static char[] vowelsChange(char[] charac) {
		for (int i = 0; i < charac.length; i++) {
			if (charac[i] == 'a' || charac[i] == 'e' || charac[i] == 'i' || charac[i] == 'o' || charac[i] == 'u') {
				charac[i] = '*';
			}
		}
		return charac;
	}
}
