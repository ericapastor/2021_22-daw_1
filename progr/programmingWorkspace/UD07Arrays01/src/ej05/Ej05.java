package ej05;

import java.util.Scanner;

public class Ej05 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Crear un programa que pida 10 n�meros enteros y los guarde en un array de 10
		 * elementos. Posteriormente se llamar� a un m�todo que recibe dicho array, y
		 * nos mostrar� por pantalla una serie de estad�sticas: cu�l es el mayor, y cu�l
		 * es el menor, la media de los positivos y de los negativos, y la media de
		 * todos.
		 */

		int[] numeros = new int[10];
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Dame el valor de la posicion " + (i + 1));
			numeros[i] = input.nextInt();
		}
		compareValues(numeros);

		input.close();
	}

	static void compareValues(int[] array) {
		int bigger = 0;
		int smaller = 999999999;
		int countN = 0;
		int sumaN = 0;
		int countP = 0;
		int sumaP = 0;
		int countT = 0;
		int sumaT = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] > bigger) {
				bigger = array[i];
			}
			if (array[i] < smaller) {
				smaller = array[i];
			}
			if (array[i] > 0) {
				sumaP += array[i];
				countP++;
			} else if (array[i] < 0) {
				array[i] = -array[i];
				sumaN += array[i];
				countN++;
			}
			sumaT += array[i];
			countT++;
		}

		System.out.println("El numero mas grande es " + bigger);
		System.out.println("El numero mas peque�o es " + smaller);
		if (countP != 0) {
			System.out.println("La media de positivos es " + ((double) sumaP / (double) countP));
		} else {
			System.out.println("No hay numeros positivos.");
		}
		if (countN != 0) {
			System.out.println("La media de negativos es " + (-((double) sumaN / (double) countN)));
		} else {
			System.out.println("No hay numeros negativos.");
		}
		System.out.println("La media total es " + (double) (sumaT / countT));

	}

}
