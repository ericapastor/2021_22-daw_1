package ej11;

import java.util.Scanner;

public class Ej11 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// statement of 3x3 matrix
		int[][] matrix = new int[3][3];
		// fill the matrix
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.println("Give the number in position i: [" + i + "] \n and position j: [" + j + "]");
				matrix[i][j] = input.nextInt();

			}
		}
		showStats(matrix);
		input.close();
	}

	private static void showStats(int[][] matrix) {
		int bigger = matrix[0][0];
		int smaller = matrix[2][2];
		int sum = 0;
		int product = 1;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] < smaller) {
					smaller = matrix[i][j];
				}
				if (matrix[i][j] > bigger) {
					bigger = matrix[i][j];
				}
				sum += matrix[i][j];
				product *= matrix[i][j];
			}
		}
		System.out.println("Biggest is: " + bigger);
		System.out.println("Smallest is: " + smaller);
		System.out.println("Sum is: "+sum);
		System.out.println("Average value is: " + (sum / (matrix.length * matrix[0].length)));
		System.out.println("Product is: "+product);
	}

}
