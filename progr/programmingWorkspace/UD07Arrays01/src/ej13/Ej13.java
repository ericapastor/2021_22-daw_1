package ej13;

import java.util.Scanner;

import ej00.Methods;

public class Ej13 {

	/*
	 * Program that fills a 10 x 10 matrix with random numbers between 0 and 49,
	 * then asks for a number to look for it inside the matrix. Afterwards, it calls
	 * for a method that that receives this matrix and the number asked. This method
	 * must look for the given number and show its position inside the matrix ([row,
	 * column]), as many times as it appeared. It will not show if it did not
	 * appear.
	 */

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int[][] matrix = new int[10][10];
		matrix = randomFill();
		System.out.println("What number do you want to find?");
		int n = input.nextInt();
		findNumber(matrix, n);
		Methods.showMatrixUnder100(matrix);
		input.close();
	}

	static int[][] randomFill() {
		int[][] matrix = new int[10][10];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = (int) (Math.round(Math.random() * 49));
			}
		}
		return matrix;
	}

	static void findNumber(int[][] matrix, int n) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == n) {
					System.out.println("The position [" + i + "][" + j + "] is the number " + n);
				}
			}
		}
		System.out.println();
	}

}
