package ej02;

public class Ej02 {

	public static void main(String[] args) {
		/*
		 * Program that shows 10 numbers in an Array. We create and fill the Array in
		 * the void main with random numbers from 4 to 15 It will call afterwards to a
		 * method that receives this array and only shows it on screen (doesn't return
		 * anything).
		 */

		int[] array = new int[10];
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) (Math.round((Math.random() * 11) + 4));
		}
		showArray(array);
	}

	static void showArray(int[] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			System.out.println(numbers[i]);
		}
	}

}