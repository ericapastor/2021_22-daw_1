
// Program that asks for characters or numbers and fills a 10 element array
// Afterwards it will show this array
// Later on it will call for a method that orders the array from smaller to bigger
// Finally the array is shown again

package ej10;

import java.util.Scanner;

public class Ej10 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		char[] characters = new char[10];
		// ask for array
		for (int i = 0; i < characters.length; i++) {
			System.out.println("Give me value number " + i);
			characters[i] = input.nextLine().charAt(0);
		}
		// show array
		System.out.println("Array in normal behaviour:");
		showArray(characters);
		// order array
		orderChars(characters);
		// show array again
		System.out.println("Array in order:");
		showArray(characters);
		input.close();
	}

	static void showArray(char[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
	}

	static void orderChars(char[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[j] < array[i]) {
					char aux = array[i];
					array[i] = array[j];
					array[j] = aux;
				}

			}
		}
	}

}
