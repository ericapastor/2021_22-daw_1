// Program that fills a 10 element array with ordered numbers (no need for Scanner)
// Then program must ask for a number to the user
// This number must be inserted into the array in the correct position

package ej09;

import java.util.Scanner;

public class Ej09 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int[] array = new int[10];
		array = fillArray();
		System.out.println("What number do you add?");
		int num = input.nextInt();
		int[] newArray = new int[10];
		newArray = changeArray(array, num);
		System.out.println("My method");
		showArray(newArray);
		System.out.println("Teachs' method.");
		arrayChange(array, num);
		showArray(array);
		input.close();
	}

	static int[] fillArray() {
		int[] array = new int[10];
		for (int i = 0; i < array.length; i++) {
			array[i] = i * 10;
		}
		return array;
	}

	static int[] changeArray(int[] array, int num) {
		int[] changeArray = new int[10];
		for (int i = 0; i < changeArray.length; i++) {
			if (array[i] < num && array[i + 1] > num) {
				changeArray[i] = num;
			} else {
				changeArray[i] = array[i];
			}
		}
		return changeArray;
	}

	// another method for this (used by the teacher)
	static void arrayChange(int[] array, int num) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] > num) {
				array[i] = num;
				return;
			}
		}
		array[10] = num;
	}

	static void showArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("Posicion " + i + " del array es: " + array[i]);
		}
	}

}
