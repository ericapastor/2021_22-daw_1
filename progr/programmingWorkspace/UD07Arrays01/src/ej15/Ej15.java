package ej15;

import java.util.Scanner;

import ej00.Methods;

public class Ej15 {

	/*
	 * Program that will ask for the number of rows and columns that a matrix will
	 * have and then fills it with random numbers between -50 and +49. The matrix
	 * will be shown, get ordered from the smallest number to the biggest by a
	 * method called orderMatrixNumbers and then the matrix will be shown again.
	 */

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("How many rows?");
		int r = input.nextInt();
		System.out.println("How many columns?");
		int c = input.nextInt();
		int[][] matrix = new int[r][c];
		matrix = fillRandomNumbersMatrix(r, c);
		System.out.println("Unordered matrix:");
		Methods.showMatrix(matrix);
		matrix = orderMatrix(matrix);
		System.out.println("Ordered matrix:");
		Methods.showMatrix(matrix);

		input.close();
	}

	static int[][] fillRandomNumbersMatrix(int r, int c) {
		int[][] matrix = new int[r][c];
		int i;
		int j;
		for (i = 0; i < r; i++) {
			for (j = 0; j < c; j++) {
				matrix[i][j] = (int) Math.round((Math.random() * 100) - 50);
			}
		}
		return matrix;
	}

	static int[][] orderMatrix(int[][] matrix) {
		int aux;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				for (int k = 0; k < matrix.length; k++) {
					for (int l = 0; l < matrix[k].length; l++) {
						if (matrix[i][j] < matrix[k][l]) {
							aux = matrix[i][j];
							matrix[i][j] = matrix[k][l];
							matrix[k][l] = aux;
						}
					}
				}
			}
		}
		return matrix;
	}
}
