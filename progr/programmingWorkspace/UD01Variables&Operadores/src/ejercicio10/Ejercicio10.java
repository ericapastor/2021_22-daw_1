package ejercicio10;

public class Ejercicio10 {

	public static void main(String[] args) {

		// Programa que declare una variable a de tipo entero y as�gnale un valor
		// A continuaci�n, muestra 3 mensajes:
		// 1. uno indicando si el valor de A es m�ltiplo de 5 o no
		// 2. otro indicando si es m�ltiplo de 10
		// 3.otro indicando si es mayor, menor, o igual a 100 (3 posibilidades,
		// 1 solo mensaje). Consideraremos el 0 como positivo. Si no, que me muestre
		// otro mensaje (p.e. �No se cumplen las condiciones�)

		// Utiliza el operador condicional ( ? : ) dentro de cada println para
		// resolverlo.

		int num;
		boolean a;
		boolean b;
		boolean c;
		boolean d;

		num = -8;
		a = num % 5 == 0;
		b = num % 10 == 0;
		c = num >= 100;
		d = num == 100;

		System.out.println(a ? "It is multiple of 5" : "Conditions are not met");
		System.out.println(b ? "It is multiple of 10" : "Conditions are not met");
		System.out.println(c ? (d ? "Number equals 100" : "Number's higher than 100") : "Number is lower than 100");

	}

}
