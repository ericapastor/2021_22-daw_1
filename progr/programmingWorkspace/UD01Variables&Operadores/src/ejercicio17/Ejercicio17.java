package ejercicio17;

public class Ejercicio17 {

	public static void main(String[] args) {

		/*
		 * Mediante el operador ? y creando dos variables tipo String, debemos
		 * inicializarlas con 2 cadenas de texto. El programa debe comparar ambas
		 * cadenas y mostrarme uno de los siguientes mensajes, dependiendo del valor de
		 * las cadenas:
		 */
		// -�cadena1� es mayor que �cadena2�
		// -�cadena2� es mayor que �cadena1�
		// -�cadena1� es igual que �cadena2�
		/*
		 * Donde �cadena1� y �cadena2� son los valores que he almacenado en ambas
		 * variables. Hay un m�todo de la clase String que nos indica si dos cadenas son
		 * iguales, una mayor a la otra, o menor que la otra.
		 */

		String chain1;
		String chain2;
		boolean condition1;
		boolean condition2;
		boolean condition3;

		chain1 = "Hello world!";
		chain2 = "I am a String";
		condition1 = chain1.length() >= chain2.length();
		condition2 = chain1.length() == chain2.length();
		condition3 = chain1.equalsIgnoreCase(chain2);

		System.out.println("First strings." + "\n");
		System.out.println(
				condition1
						? (condition2
								? (condition3 ? "Strings are the same"
										: "Strings are not the same but they are the same length")
								: "String 1 is bigger")
						: "String 2 is bigger");

		System.out.println("\n");
		
		System.out.println("Second Strings" + "\n");
		chain1 = "Hello world!!";
		chain2 = "I am a String";
		condition1 = chain1.length() >= chain2.length();
		condition2 = chain1.length() == chain2.length();
		condition3 = chain1.equalsIgnoreCase(chain2);

		System.out.println(
				condition1
						? (condition2
								? (condition3 ? "Strings are the same"
										: "Strings are not the same but they are the same length")
								: "String 1 is bigger")
						: "String 2 is bigger");

	}

}
