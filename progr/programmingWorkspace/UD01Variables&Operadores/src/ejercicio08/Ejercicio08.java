package ejercicio08;

public class Ejercicio08 {

	public static void main(String[] args) {

		/*
		 * Programa que declare una variableA de tipo entero y as�gnale un valor. A
		 * continuaci�n muestra un mensaje indicando si A es par o impar. Utiliza el
		 * operador condicional ( ? : ) dentro del println para resolverlo. En otro
		 * mensaje indicar si A es a la vez m�ltiplo de 2 (par) y m�ltiplo de 3. Si no,
		 * que me muestre otro mensaje (p.e. �No se cumplen las condiciones�)
		 */

		int varA;
		boolean condA;

		varA = 8;
		condA = varA % 2 == 0;

		System.out.println(condA ? "The number is a pair." : "Number is impair");

	}

}
