package ejercicio03;

public class Ejercicio03 {

	public static void main(String[] args) {

		/*
		 * Programa Java que declare una variable �num� de tipo int y as�gnale un valor.
		 * A continuaci�n escribe las instrucciones que realicen lo siguiente:
		 * Incrementar num en 77, decrementarla en 3, Duplicar su valor. Despu�s de cada
		 * operaci�n el valor de la variable se debe haber modificado
		 */

		int num = 6;
		System.out.println("The number is " + num);
		num += 77;
		System.out.println("The number is " + num);
		num -= 3;
		System.out.println("The number is " + num);
		num *= 2;
		System.out.println("The number is " + num);
		num /= 4;
		System.out.println("The number is " + num);

	}

}
