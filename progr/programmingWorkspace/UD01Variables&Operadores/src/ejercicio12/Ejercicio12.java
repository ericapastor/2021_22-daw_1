package ejercicio12;

public class Ejercicio12 {

	public static void main(String[] args) {

		/*
		 * Crea una variable de tipo float y otra variable de tipo double. Asigna la
		 * variable double a la variable float. Explica qu� has hecho
		 */

		float bitDec;
		double longDec;

		longDec = 5.456465456;
		bitDec = (float) longDec;

		System.out.println("The long decimal is " + longDec);
		System.out.println("and converted to tiny is " + bitDec);

	}

}
