package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {

		/*
		 * Programa java que realice lo siguiente: declarar una variable de cada tipo de
		 * datos primitivo y asignar a cada una un valor literal (excepto boolean). A
		 * continuación, realiza una serie de operaciones entre ellas(una por cada
		 * variable) y muestra por pantalla su resultado. (suma, resta, multiplicación,
		 * división y resto).
		 */

		long largeNumber;
		int number;
		short shortNumber;
		byte bitNumber;
		float tinyDec;
		double largeDec;
		char charac;

		largeNumber = 9834672938456295L;
		number = 984536432;
		shortNumber = 28345;
		bitNumber = 22;
		tinyDec = 456.4F;
		largeDec = 7854.345;
		charac = '8';

		System.out.println("Long minus int: " + (largeNumber - number));
		System.out.println("Short plus double " + (shortNumber + largeDec));
		System.out.println("Char divided by float: " + (charac / tinyDec));
		System.out.println("Byte multiplied by char: " + (bitNumber * charac));

	}

}
