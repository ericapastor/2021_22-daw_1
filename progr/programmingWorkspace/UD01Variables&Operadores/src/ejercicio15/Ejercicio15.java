package ejercicio15;

public class Ejercicio15 {

	static final String INICIO = "COMIENZA EL PROGRAMA" + "\n";
	static final String FIN = "\n" + "TERMINA EL PROGRAMA";

	public static void main(String[] args) {

		/*
		 * Crea una variable de tipo String que contenga una cadena de texto larga (m�s
		 * de 20 caracteres). Utiliza el m�todo substring de la clase String, para
		 * obtener una cadena intermedia, dentro de la cadena principal. Adem�s se deben
		 * crear 2 constantes tipo String. Una con el mensaje �Comienza el programa� y
		 * la otra con �Termina el programa�. Mostrar cada una antes y despu�s del resto
		 * del c�digo.
		 */

		System.out.println(INICIO);

		String chain = "Hello, this is a long chain of words, a String, to be more accurate.";

		System.out.println("This is the entire String:" + "\n");
		System.out.println(chain);
		System.out.println("\n");
		System.out.println("This is a part of the String:" + "\n");
		System.out.println(chain.substring(7, 36));

	}

}
