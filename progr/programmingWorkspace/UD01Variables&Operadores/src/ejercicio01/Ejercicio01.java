package ejercicio01;

public class Ejercicio01 {

	public static void main(String[] args) {

		/*
		 * Programa java que realice lo siguiente: declarar una variable de tipo int,
		 * una variable de tipo double y una variable de tipo char y asigna a cada una
		 * un valor. A continuaci�n, muestra por pantalla: El valor de cada variable, la
		 * suma de int + double, la diferencia de double - int, el valor num�rico
		 * correspondiente al car�cter que contiene la variable char (conversi�n cast)
		 */
		
		int number;
		double decimal;
		char character;
		
		number=9;
		decimal=8.67;
		character='h';

		System.out.println("The value of the integer is "+number);
		System.out.println("The value of the decimal is "+decimal);
		System.out.println("The value of the character is "+character);
		System.out.println("The addition of int plus double is "+(number+decimal));
		System.out.println("The difference between double minus int is "+(decimal-number));
		System.out.println("The numerical value of the character is "+(int)character);

	}

}
