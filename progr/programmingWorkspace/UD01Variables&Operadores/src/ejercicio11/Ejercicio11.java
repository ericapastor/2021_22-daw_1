package ejercicio11;

public class Ejercicio11 {

	public static void main(String[] args) {

		/*
		 * Crea 2 variables de tipo short, y 2 de tipo long. Dales a las variables de
		 * tipo long el m�ximo mayor positivo, y el m�nimo negativo. Luego asigna cada
		 * una de esas variables long a las variables de tipo short. Utiliza la
		 * conversi�n expl�cita (cast) y muestra las 4 variables por pantalla. Adem�s
		 * muestra un mensaje indicando qu� es lo que ocurre en ambos casos.
		 */

		short bit1;
		short bit2;
		long large1;
		long large2;

		large1 = 3879453984573487574L;
		large2 = -3879453984573487574L;
		bit1 = (short) large1;
		bit2 = (short) large2;

		System.out.println("Long numbers are " + large1 + " and " + large2 + ".");
		System.out.println("Shorted, they are " + bit1 + " and " + bit2 + ".");

	}

}
