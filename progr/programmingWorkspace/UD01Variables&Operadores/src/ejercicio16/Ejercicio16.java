package ejercicio16;

public class Ejercicio16 {

	public static void main(String[] args) {

		/*
		 * Creamos solo una variable de tipo String, la inicializamos con un texto que
		 * represente un n�mero entero (Ej: �1234�). Posteriormente con el operador ?:
		 * vamos a mostrar un mensaje: SI la cadena tiene 5 o m�s cifras, mostramos el
		 * valor num�rico de dicha cadena (conversi�n a int) y si no se cumple, debe
		 * mostrar un mensaje diciendo la cadena tiene menos de 5 cifras
		 */

		String chain;
		boolean condition;
		int number;

		chain = "34567245";
		condition = chain.length() >= 5;

		System.out.println(condition ? (number = Integer.parseInt(chain)) + number : "Chain has less than 5 numbers.");

		System.out.println("\n");

		chain = "397";
		condition = chain.length() >= 5;

		System.out.println(condition ? (number = Integer.parseInt(chain)) + number : "Chain has less than 5 numbers.");

	}

}
