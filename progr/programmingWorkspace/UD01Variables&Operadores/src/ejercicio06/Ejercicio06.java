package ejercicio06;

public class Ejercicio06 {

	public static void main(String[] args) {

		/*
		 * Programa que declare una variable variableA de tipo entero y asígnale un
		 * valor. A continuación, muestra un mensaje indicando si el valor de A es
		 * positivo o negativo. Consideraremos el 0 como positivo. Utiliza el operador
		 * condicional (? : ) dentro del println para resolverlo.
		 */

		int a;
		a = 9;
		System.out.println(a >= 0 ? "It is a positive number" : "It is a negative number");

	}

}
