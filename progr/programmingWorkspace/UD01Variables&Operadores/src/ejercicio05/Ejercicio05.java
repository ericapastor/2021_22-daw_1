package ejercicio05;

public class Ejercicio05 {

	public static void main(String[] args) {

		/*
		 * Dadas las variables de tipo int con valores a = 5, b = 3, c = -12 indicar si
		 * la evaluaci�n de estas expresiones dar�a como resultado verdadero o falso
		 * (mostrarlo por pantalla). Para evaluarlas asignamos la expresi�n a una
		 * variable de tipo boolean, y la mostramos por pantalla:
		 */
		// a) a > 3
		// b) a > c
		// c) a < c
		// d) b < c
		// e) b != c
		// f) a == 3
		// g) a * b == 15
		// h) c / b == -4
		// i) c / b < a
		// j) c / b == -10
		// k) a % b == 2
		// l) a + b + c == 5
		// m) (a+b == 8) && (a-b == 2)
		// n) (a+b == 8) || (a-b == 6)
		// o) a > 3 && b > 3 && c < 3
		// p) a > 3 && b >= 3 && c < -3

		int num1;
		int num2;
		int num3;
		num1 = 5;
		num2 = 3;
		num3 = -12;

		boolean a;
		boolean b;
		boolean c;
		boolean d;
		boolean e;
		boolean f;
		boolean g;
		boolean h;
		boolean i;
		boolean j;
		boolean k;
		boolean l;
		boolean m;
		boolean n;
		boolean o;
		boolean p;

		a = num1 > 3;
		b = num1 > num3;
		c = num1 < num3;
		d = num2 < num3;
		e = num2 != num3;
		f = num1 == 3;
		g = num1 * num2 == 15;
		h = num3 / num2 == -4;
		i = num3 / num2 < num1;
		j = num3 / num2 == -10;
		k = num1 % num2 == 2;
		l = num1 + num2 + num3 == 5;
		m = (num1 + num2 == 8) && (num1 - num2 == 2);
		n = (num1 + num2 == 8) || (num1 - num2 == 6);
		o = num1 > 3 && num2 > 3 && num3 > 3;
		p = num1 > 3 && num2 >= 3 && num3 < -3;

		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		System.out.println(e);
		System.out.println(f);
		System.out.println(g);
		System.out.println(h);
		System.out.println(i);
		System.out.println(j);
		System.out.println(k);
		System.out.println(l);
		System.out.println(m);
		System.out.println(n);
		System.out.println(o);
		System.out.println(p);

	}

}
