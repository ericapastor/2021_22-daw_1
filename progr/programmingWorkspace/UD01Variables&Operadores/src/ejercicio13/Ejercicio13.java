package ejercicio13;

public class Ejercicio13 {

	public static void main(String[] args) {

		/*
		 * Crea una variable de tipo String, dale valor y muestrala por pantalla.
		 * Muestra en un mensaje con println la longitud de la cadena de texto. (Anexo
		 * clase String)
		 */

		String chain = "Hello there.";
		System.out.println(chain);
		System.out.println("The length of the String is " + chain.length()+".");

	}

}
