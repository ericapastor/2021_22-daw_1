package ejercicio04;

public class Ejercicio04 {

	public static void main(String[] args) {

		/*
		 * Programa que declare, al menos, cuatro variables de tipo int variableA,
		 * variableB, variableC y variableD, as�gnale un valor a cada una y muestra su
		 * valor por consola. A continuaci�n, realiza las asignaciones necesarias para
		 * que: A tome el valor de B, B tome el valor de C, C tome el valor de D, D tome
		 * el valor de A. Muestra de nuevo su valor para comprobarlo. �Qu� m�s
		 * necesitamos si no queremos perder ning�n valor?
		 */

		int num1;
		int num2;
		int num3;
		int num4;
		int save;

		num1 = 13;
		num2 = 42;
		num3 = 34;
		num4 = 21;
		save = num1;

		System.out.println("First values:");
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
		System.out.println(num4);

		num1 = num2;
		num2 = num3;
		num3 = num4;
		num4 = save;

		System.out.println("Second values:");
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
		System.out.println(num4);

	}

}