package ejercicio07;

public class Ejercicio07 {

	public static void main(String[] args) {

		/*
		 * Utiliza el operador condicional ( ? : ) dentro del println para que me
		 * indique si el valor de una variable int (le asignamos un valor) es positivo y
		 * par. Si no, que me muestre otro mensaje (p.e. “No se cumplen las
		 * condiciones”)
		 */

		int a;
		boolean b;
		a = -8;
		b = a % 2 == 0 && a >= 0;

		System.out.println(b ? "The number is positive and pair" : "Conditions are not fitted.");

	}

}
