package ejercicio09;

public class Ejercicio09 {

	public static void main(String[] args) {

		/*
		 * Utiliza el operador condicional ( ? : ) dentro del println para que me
		 * indique si el valor de una variable int (le asignamos un valor) es o m�ltiplo
		 * de 3 o m�ltiplo de 7, y adem�s tiene que ser positivo. En caso afirmativo nos
		 * dir� que cumple las caracter�sticas nombradas, y si no, que me muestre otro
		 * mensaje (p.e. �No se cumplen las condiciones�) .
		 */
		int a;
		boolean condA;

		a = 21;
		condA = (a % 7 == 0) && (a % 3 == 0) && (a >= 0);

		System.out.println(
				condA ? "Variable 'a' is multiple of 3, 7 and it is a positive number" : "Conditions are not met");

	}

}
