package ejercicio14;

public class Ejercicio14 {

	public static void main(String[] args) {

		/*
		 * Crea 4 variables String y les introducimos una cadena que represente un
		 * n�mero decimal, un n�mero entero largo (long), un n�mero tipo byte, y un
		 * n�mero int. Ahora crea 4 variables de los tipos anteriores, y asigna el valor
		 * de las variables String a las variables del tipo concreto usando los m�todos
		 * necesarios para convertir los datos
		 */

		String decS;
		String longS;
		String byteS;
		String intS;
		double decN;
		long longN;
		byte byteN;
		int intN;

		decS = "45.98";
		longS = "345624534634563646";
		byteS = "123";
		intS = "23453456";

		System.out.println(decS);
		System.out.println(longS);
		System.out.println(byteS);
		System.out.println(intS);

		decN = Double.parseDouble(decS);
		longN = Long.parseLong(longS);
		byteN = Byte.parseByte(byteS);
		intN = Integer.parseInt(intS);

		System.out.println(decN);
		System.out.println(longN);
		System.out.println(byteN);
		System.out.println(intN);

	}

}
