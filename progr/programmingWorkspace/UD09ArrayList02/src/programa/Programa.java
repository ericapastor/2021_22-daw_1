package programa;

import clases.Cine;

public class Programa {

	public static void main(String[] args) {
		System.out.println("Generamos cine y rellenamos");
		Cine cine1 = new Cine();
		cine1.generarUnCine();

		System.out.println("Visualizar cine");
		cine1.visualizarCine();
		System.out.println("");

		System.out.println("Modificamos pelicula");
		cine1.modificarPelicula();
		System.out.println("");

		System.out.println("Ordenamos");
		cine1.ordenarDesPeliculas();
		System.out.println("");

		System.out.println();
		System.out.println("Visualizamos despues de ordenar");
		cine1.visualizarCine();
	}

}