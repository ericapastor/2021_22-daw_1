package clases;

import java.util.ArrayList;
import java.util.Scanner;

public class Pelicula {
	private String titulo;
	private double pvp;
	private ArrayList<Actor> actores;

	public static Scanner sc = new Scanner(System.in);

	public Pelicula() {
		this.titulo = "";
		this.pvp = 0.0;
		this.actores = new ArrayList<Actor>();
	}

	public Pelicula(String titulo, double pvp, ArrayList<Actor> actores) {
		this.titulo = titulo;
		this.pvp = pvp;
		this.actores = actores;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getPvp() {
		return pvp;
	}

	public void setPvp(double pvp) {
		this.pvp = pvp;
	}

	public ArrayList<Actor> getActores() {
		return actores;
	}

	public void setActores(ArrayList<Actor> actores) {
		this.actores = actores;
	}

	public void rellenarPelicula() {
		int precioPVP = -1;
		String nombreActor;
		System.out.println("Por favor, introduzca el titulo de la pelicula");
		this.setTitulo(sc.nextLine());
		System.out.println("Por favor, introduzca el precio PVP de la pelicula");
		while (precioPVP < 0) {
			try {
				precioPVP = sc.nextInt();
				this.setPvp(precioPVP);
				sc.nextLine();
			} catch (Exception e) {
				System.out.println("Se ha producido un error al introducir el precio");
				precioPVP = sc.nextInt();
				this.setPvp(precioPVP);
				sc.nextLine();
			}
		}
		while (true) {
			Actor actor = new Actor();
			System.out.println("Por favor, introduzca el nombre del actor (* para salir)");
			nombreActor = sc.nextLine();
			actor.setNombre(nombreActor);
			if (actor.getNombre().equalsIgnoreCase("*")) {
				break;
			}
			System.out.println("Por favor, introduzca si es protagonista (true o false)");
			actor.setProtagonista(sc.nextBoolean());
			sc.nextLine();
			actores.add(actor);
		}
	}

}