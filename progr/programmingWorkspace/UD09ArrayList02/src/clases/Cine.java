package clases;

import java.util.ArrayList;
import java.util.Scanner;

public class Cine {

	public static Scanner sc = new Scanner(System.in);

	private String nombre;
	private ArrayList<Pelicula> peliculas;

	public Cine() {
		this.nombre = "";
		this.peliculas = new ArrayList<Pelicula>();
	}

	public Cine(String nombre, ArrayList<Pelicula> peliculas) {
		this.nombre = nombre;
		this.peliculas = peliculas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Pelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(ArrayList<Pelicula> peliculas) {
		this.peliculas = peliculas;
	}

	public void visualizarCine() {
		int contadorPeli = 1;
		int contadorProta = 0;
		System.out.println();
		System.out.println("Nombre del cine" + this.nombre);
		for (Pelicula p : this.peliculas) {
			System.out.println("Peliculas numero " + contadorPeli);
			System.out.println("______________________");
			System.out.println("Titulo " + p.getTitulo());
			System.out.println("Precio PVP " + p.getPvp());
			System.out.println("Lista de actores");
			for (Actor a : p.getActores()) {
				System.out.println("Nombre " + a.getNombre());
				System.out.println("Es protagonista: " + a.isProtagonista());
				if (a.isProtagonista() == true) {
					contadorProta++;
				}
			}
			if (contadorProta > 2) {
				System.out.println("Genero familiar.");
			} else {
				System.out.println("Genero romantico.");
			}
		}
	}

	public void generarUnCine() {
		String seleccion;
		String nombreCine;
		System.out.println("Por favor, introduzca el nombre del cine.");
		nombreCine = sc.nextLine();
		ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();
		while (true) {
			Pelicula pelicula = new Pelicula();
			pelicula.rellenarPelicula();
			peliculas.add(pelicula);
			System.out.println("Quieres introducir mas peliculas (si/no)");
			seleccion = sc.nextLine();
			if (seleccion.equalsIgnoreCase("no")) {
				break;
			}
		}
		this.setNombre(nombreCine);
		this.setPeliculas(peliculas);
	}

	public void modificarPelicula() {
		String tituloPeli;
		String nombreActor;
		System.out.println("Por favor, introduzca el nombre de la pelicula que desea buscar");
		tituloPeli = sc.nextLine();
		System.out.println("Por favor, introduzca el nombre del actor que desea modificar");
		nombreActor = sc.nextLine();
		for (Pelicula p : this.getPeliculas()) {
			if (p.getTitulo().equals(tituloPeli)) {
				for (Actor a : p.getActores()) {
					if (nombreActor.equalsIgnoreCase(a.getNombre())) {
						System.out.println("Dame el nuevo nombre de actor");
						a.setNombre(sc.nextLine());
						System.out.println("Dime si es protagonista (true o false)");
						a.setProtagonista(sc.nextBoolean());
						break;
					}
				}
				break;
			}
		}
	}

	public void ordenarDesPeliculas() {
		ArrayList<Pelicula> peliculas = this.getPeliculas();
		Pelicula pelis = new Pelicula();
		for (int i = 0; i < peliculas.size() - 1; i++) {
			for (int j = 0; j < peliculas.size() - 1 - i; j++) {
				if (peliculas.get(j).getTitulo().compareTo(peliculas.get(j + 1).getTitulo()) < 0) {
					pelis = peliculas.get(j);
					peliculas.set(j, peliculas.get(j + 1));
					peliculas.set(j + 1, pelis);
				}
			}
		}

	}

}