package clasesMascota;

import enumsMascota.Sexo;
import interfacesMascota.Mascota;

public class Perro extends Animal implements Mascota{

	private String codigo;
	
	public Perro(Sexo s, String c) {
		super(s);
		this.codigo=c;
	}
	
	@Override
	public String getCodigo() {
		return this.codigo;
	}

	@Override
	public void hazRuido() {
		this.ladra();
	}
	
	public void ladra() {
		System.out.println("guau guau");
	}

	@Override
	public void come(String comida) {
		if (comida.equals("carne")) {
			super.come();
			System.out.println("mmmm gracias");
		} else {
			System.out.println("lo siento, solo como carne");
		}
	}

	@Override
	public void peleaCon(Animal contrincante) {
		if (contrincante.getClass().getSimpleName().equals("Perro")) {
			System.out.println("te vas a enterar");
		} else {
			System.out.println("no me gusta pelear");
		}
		
	}

}