package clasesMascota;

import enumsMascota.Sexo;
import interfacesMascota.Mascota;

public class Gato extends Animal implements Mascota {

	private String codigo;

	public Gato(Sexo s, String codigo) {
		super(s);
		this.codigo = codigo;
	}

	@Override
	public String getCodigo() {
		return this.codigo;
	}

	@Override
	public void hazRuido() {
		this.maulla();
		this.ronronea();
	}

	public void maulla() {
		System.out.println("miauuuu");
	}

	public void ronronea() {
		System.out.println("rrrrrrr");
	}

	@Override
	public void come(String comida) {
		if (comida.equals("pescado")) {
			super.come();
			System.out.println("mmmm gracias");
		} else {
			System.out.println("lo siento, solo como pescado");
		}
	}

	@Override
	public void peleaCon(Animal contrincante) {
		if (this.getSexo() == Sexo.HEMBRA) {
			System.out.println("no me gusta pelear");
		} else {
			System.out.println("te vas a enterar");
		}

	}

}