package clasesMascota;

import enumsMascota.Sexo;

public abstract class Animal {
	private Sexo sexo;
	
	public Animal() {
		sexo=Sexo.MACHO;
	}
	
	public Animal(Sexo x) {
		sexo=x;
	}
	
	public Sexo getSexo() {
		return sexo;
	}

	@Override
	public String toString() {
		return "Animal [sexo=" + sexo + "]";
	}
	
	//completamos los metodos
	public void duerme() {
		System.out.println("zzzzzzzz");
	}
	public void come() {
		System.out.println("Que rico");
	}
	public void come(String comida) {
		System.out.println("Que rico, me gusta comer "+comida);
	}

}
