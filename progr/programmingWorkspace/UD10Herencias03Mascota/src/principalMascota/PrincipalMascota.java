package principalMascota;

import clasesMascota.Gato;
import clasesMascota.Perro;
import enumsMascota.Sexo;
import interfacesMascota.Mascota;

public class PrincipalMascota {

	public static void main(String[] args) {
		// no puedo instanciar Mascota
		Mascota garfield = new Gato(Sexo.MACHO, "3214");
		Mascota lisa = new Gato(Sexo.HEMBRA, "2145");
		Mascota kuki = new Perro(Sexo.HEMBRA, "2568");
		Mascota minu = new Perro(Sexo.MACHO, "5478");

		System.out.println("Mostramos codigos");
		System.out.println("Garfield " + garfield.getCodigo());
		System.out.println("Lisa " + lisa.getCodigo());
		System.out.println("kuki " + kuki.getCodigo());
		System.out.println("minu " + minu.getCodigo());

		System.out.println("Metodos comer");
		System.out.println("Gato garfield");
		garfield.come("pescado");
		System.out.println("Gato lisa");
		lisa.come("hamburguesa");
		System.out.println("Perro kuki");
		kuki.come("pescado");

		System.out.println("Metodos pelear");
		System.out.println("Lisa pelea con Garfield");
		lisa.peleaCon((Gato)garfield);
		System.out.println("Minu pelea con kuki");
		minu.peleaCon((Perro)kuki);
		

	}

}