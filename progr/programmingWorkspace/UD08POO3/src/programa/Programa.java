package programa;

import clases.Vehiculo;

public class Programa {

	public static void main(String[] args) {
		System.out.println("1.- Crear una instancia de llamada taller con 4 albaranes");
		int maxVehiculos = 4;
		Vehiculo taller = new Vehiculo(maxVehiculos);
		System.out.println("Instancia creada");

		System.out.println("2.- Dar de alta 4 albaranes distintos");
		System.out.println("Doy de alta A1-A2-A3-A4");
		taller.altaAlbaran("A1", 20, "V1");
		taller.altaAlbaran("A2", 23.10, "V1");
		taller.altaAlbaran("A3", 25.99, "V1");
		taller.altaAlbaran("A4", 16.99, "V2");

		System.out.println("3.- Listar los albaranes");
		taller.listarAlbaranes();

		System.out.println("4.- Buscar albaran");
		System.out.println("Buscamos albaran A2");
		System.out.println(taller.buscarAlbaran("A2"));

		System.out.println("5.- Eliminar albaran");
		System.out.println("Eliminamos el albaran A3");
		taller.eliminarAlbaran("A3");
		taller.listarAlbaranes();

		System.out.println("6.- Dar de alta nuevo albaran");
		System.out.println("Doy de alta A5");
		taller.altaAlbaran("A5", 27.33, "V2");
		taller.listarAlbaranes();

		System.out.println("7.- Modificar el codVehiculo de un albaran");
		System.out.println("Modificamos el albaran A1 de V1 a V2");
		taller.cambiarAlbaran("A1", "V2");
		taller.listarAlbaranes();

		System.out.println("8.- Listar solo los albaranes de un vehiculo");
		System.out.println("Listamos los albaranes del vehiculo V2");
		taller.listarAlbaranesPorVehiculo("V2");
	}

}
