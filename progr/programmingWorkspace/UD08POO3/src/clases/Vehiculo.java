package clases;

import java.time.LocalDate;

public class Vehiculo {
	// atributo
	private Albaran[] albaranes;

	// constructor
	public Vehiculo(int maxAlbaranes) {
		this.albaranes = new Albaran[maxAlbaranes];
	}

	// alta albaran
	public void altaAlbaran(String codAlbaran, double precio, String codVehiculo) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] == null) {
				albaranes[i] = new Albaran(codAlbaran);
				albaranes[i].setPrecio(precio);
				albaranes[i].setCodVehiculo(codVehiculo);
				albaranes[i].setFecha(LocalDate.now());
				break;
			}
		}
	}

	// buscar albaranes
	public Albaran buscarAlbaran(String codAlbaran) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
				if (albaranes[i].getCodAlbaran().equals(codAlbaran)) {
					return albaranes[i];
				}
			}
		}
		return null;
	}

	// listar albaranes
	public void listarAlbaranes() {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
				System.out.println(albaranes[i]);
			}
		}
	}

	// eliminar albaran
	public void eliminarAlbaran(String codAlbaran) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
				if (albaranes[i].getCodAlbaran().equals(codAlbaran)) {
					albaranes[i] = null;
				}
			}
		}
	}

	// cambiar albaran
	public void cambiarAlbaran(String codAlbaran, String codVehiculo2) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
				if (albaranes[i].getCodAlbaran().equals(codAlbaran)) {
					albaranes[i].setCodVehiculo(codVehiculo2);
				}
			}
		}
	}

	// listar albaranes vehiculos
	public void listarAlbaranesPorVehiculo(String codVehiculo) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
				if (albaranes[i].getCodVehiculo().equals(codVehiculo)) {
					System.out.println(albaranes[i]);
				}
			}
		}
	}
}
