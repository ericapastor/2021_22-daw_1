package ejemplo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class Cliente {
	private String nombre;
	private String apellido;
	private String dni;
	private String codigo;
	private LocalDate fechaNacimiento;
	private LocalTime horaNacimiento;
	private Signo signo = new Signo();
	private ArrayList<Pedido> pedido;

	public Cliente(ArrayList<Pedido> pedido) {
		this.pedido = pedido;
	}
}
