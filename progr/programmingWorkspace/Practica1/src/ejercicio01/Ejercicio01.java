package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	static final String INDEX = "INICIO DEL PROGRAMA\n";
	static final String END = "\nFIN DEL PROGRAMA";
	static final float RECARGO = 4.95F;
	static final String DESCUENTO1 = "FDTY";
	static final String DESCUENTO2 = "HTRG";

	public static void main(String[] args) {

		/**/

		System.out.println(INDEX + "\n");

		Scanner program = new Scanner(System.in);

		int opcion;

		// ej1
		float compraOnline;
		String opcionDescuento;

		// ej2
		String apodo1;
		String apodo2;
		String apodo3;
		String nombre1;
		String nombre2;
		String nombre3;
		int puntuacion1;
		int puntuacion2;
		int puntuacion3;
		String jugador1;
		String jugador2;
		String jugador3;

		// ej3
		String cadenaEj3;
		int counterA = 0;
		int counterE = 0;
		int counterI = 0;
		int counterO = 0;
		int counterU = 0;

		// ej4
		float saldoInicial;
		float movimiento;
		float acumulator = 0;
		int counter = 0;

		do {
			System.out.println("\n*********************************\n");
			System.out.println("1-Calcular importe compra");
			System.out.println("2-Ordena jugadores");
			System.out.println("3-Frecuencia vocales");
			System.out.println("4-Dinero en cuenta");
			System.out.println("5-Salir");
			System.out.println("\n*********************************\n");
			System.out.println("Introduzca el numero de opcion");

			opcion = program.nextInt();

			switch (opcion) {
			
			default:
				System.out.println("Opcion no valida.");
				break;
				
			case 1:
				do {
					System.out.println("\nIntroduce la cantidad correspondiente a la compra.");
					compraOnline = program.nextFloat();
					if (compraOnline > 0) {
						compraOnline += RECARGO;
						System.out.println("\nLa compra tiene un recargo de " + RECARGO + ", lo que hace un total de "
								+ (compraOnline));
						System.out.println("\nIntroduzca codigo de descuento");
						program.nextLine();
						// lo siguiente no me da error pero por alguna razon no lo lee asi que no se
						// imprime el descuento
						opcionDescuento = program.nextLine();
						if (opcionDescuento.equals(DESCUENTO1)) {
							compraOnline = compraOnline * 85 / 100;
							System.out.println("La compra le costara ahora " + compraOnline + " euros.\n");
						}
						if (opcionDescuento.equals(DESCUENTO2)) {
							compraOnline -= RECARGO;
							System.out.println("La compra le costara ahora " + compraOnline + " euros.\n");
						}
					} else {
						System.out.println("La compra no puede valer cero o negativo.\n");
					}
				} while (compraOnline <= 0);
				break;
				
			case 2:
				do {
					System.out.println("BIENVENIDO AL ORDEN DE JUGADORES");
					program.nextLine();
					System.out.println("Nombre 1.");
					nombre1 = program.nextLine();
					System.out.println("Nombre 2.");
					nombre2 = program.nextLine();
					System.out.println("Nombre 3.");
					nombre3 = program.nextLine();
					System.out.println("Apodo 1.");
					apodo1 = program.nextLine();
					System.out.println("Apodo 2.");
					apodo2 = program.nextLine();
					System.out.println("Apodo 3.");
					apodo3 = program.nextLine();
					System.out.println("Primera puntuacion.");
					puntuacion1 = program.nextInt();
					System.out.println("Segunda puntuacion.");
					puntuacion2 = program.nextInt();
					System.out.println("Tercera puntuacion.");
					puntuacion3 = program.nextInt();

					jugador1 = apodo1 + " - " + nombre1 + " (" + puntuacion1 + ")";
					jugador2 = apodo2 + " - " + nombre2 + " (" + puntuacion2 + ")";
					jugador3 = apodo3 + " - " + nombre3 + " (" + puntuacion3 + ")";

					if (apodo2.compareTo(apodo1) > apodo1.compareTo(apodo2)
							&& apodo3.compareTo(apodo1) > apodo1.compareTo(apodo3)) {
						if (apodo3.compareTo(apodo2) > apodo2.compareTo(apodo3)) {
							System.out.println(jugador1 + "\n" + jugador2 + "\n" + jugador3);
						} else if (apodo2.compareTo(apodo3) > apodo3.compareTo(apodo2)) {
							System.out.println(jugador1 + "\n" + jugador3 + "\n" + jugador2);
						}
					} else if (apodo1.compareTo(apodo2) > apodo2.compareTo(apodo1)
							&& apodo3.compareTo(apodo2) > apodo2.compareTo(apodo3)) {
						if (apodo3.compareTo(apodo1) > apodo1.compareTo(apodo3)) {
							System.out.println(jugador2 + "\n" + jugador1 + "\n" + jugador3);
						} else if (apodo1.compareTo(apodo3) > apodo3.compareTo(apodo1)) {
							System.out.println(jugador2 + "\n" + jugador3 + "\n" + jugador1);
						}
					} else if (apodo1.compareTo(apodo3) > apodo3.compareTo(apodo1)
							&& apodo2.compareTo(apodo3) < apodo3.compareTo(apodo2)) {
						if (apodo2.compareTo(apodo1) > apodo1.compareTo(apodo2)) {
							System.out.println(jugador3 + "\n" + jugador1 + "\n" + jugador2);
						} else if (apodo1.compareTo(apodo2) > apodo2.compareTo(apodo1)) {
							System.out.println(jugador3 + "\n" + jugador2 + "\n" + jugador1);
						}
					}
					System.out.println("\n1.-Repetir proceso\n2.-Volver al menu principal.");
					opcion = program.nextInt();
				} while (opcion == 1);
				break;

			case 3:
				do {
					System.out.println("Dime una frase de varias palabras.");
					program.nextLine();
					cadenaEj3 = program.nextLine().toLowerCase();
					for (int i = 0; i < cadenaEj3.length(); i++) {
						if (cadenaEj3.charAt(i) == 'a') {
							counterA++;
						}
						if (cadenaEj3.charAt(i) == 'e') {
							counterE++;
						}
						if (cadenaEj3.charAt(i) == 'i') {
							counterI++;
						}
						if (cadenaEj3.charAt(i) == 'o') {
							counterO++;
						}
						if (cadenaEj3.charAt(i) == 'u') {
							counterU++;
						}
					}

					for (int j = 0; j < 1; j++) {
						System.out.print(" a " + counterA + " ");
						for (int k = 0; k < counterA; k++) {
							System.out.print("*");
						}
						System.out.println();
					}
					for (int j = 0; j < 1; j++) {
						System.out.print(" e " + counterE + " ");
						for (int k = 0; k < counterE; k++) {
							System.out.print("*");
						}
						System.out.println();
					}
					for (int j = 0; j < 1; j++) {
						System.out.print(" i " + counterI + " ");
						for (int k = 0; k < counterI; k++) {
							System.out.print("*");
						}
						System.out.println();
					}
					for (int j = 0; j < 1; j++) {
						System.out.print(" o " + counterO + " ");
						for (int k = 0; k < counterO; k++) {
							System.out.print("*");
						}
						System.out.println();
					}
					for (int j = 0; j < 1; j++) {
						System.out.print(" u " + counterU + " ");
						for (int k = 0; k < counterU; k++) {
							System.out.print("*");
						}
						System.out.println();
					}

					System.out.println("/1.-Repetir proceso.\n2.-Volver al menu principal.");
					opcion = program.nextInt();
				} while (opcion == 1);
				break;

			case 4:
				do {
					do {
						System.out.println("Cuanto dinero tienes en la cuenta" + "\n");
						saldoInicial = program.nextFloat();
						if (saldoInicial > 0) {
							acumulator += saldoInicial;
							do {
								System.out.println("\n"
										+ "Cuanto dinero ingresas/retiras?\nNumero positivo para ingresar, negativo para retirar.");
								movimiento = program.nextFloat();
								acumulator += movimiento;
								counter++;
								if (acumulator > 0) {
									System.out
											.println("\nEl nuevo balance de la cuenta es: " + acumulator + " euros\n");
								} else if (acumulator == 0) {
									System.out.println("Has agotado tu credito.");
								} else {
									counter--;
									acumulator -= movimiento;
									System.out.println(
											"No puedes introducir una cantidad que deje la cuenta en negativo.");
								}
							} while (acumulator > 0);
							System.out.println("El numero de movimientos realizados ha sido: " + (counter)
									+ ". Gracias por su tiempo.\n");
						} else {
							System.out.println("El saldo no puede ser cero o negativo.");
						}
					} while (saldoInicial <= 0);
					System.out.println("/1.-Repetir proceso.\n2.-Volver al menu principal.");
					opcion = program.nextInt();
				} while (opcion == 1);
				break;
			case 5:
				System.out.println("Gracias por utilizar nuestras opciones. El programa terminara.");
				break;
			}
		} while (opcion != 5);

		program.close();

		System.out.println("\n" + END);

	}

}
