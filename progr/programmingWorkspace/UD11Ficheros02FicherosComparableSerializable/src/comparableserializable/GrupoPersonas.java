package comparableserializable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class GrupoPersonas implements Serializable {

	private static final long serialVersionUID = -1429504306914757180L;
	private ArrayList<Persona> listaPersonas;

	public GrupoPersonas() {
		listaPersonas = new ArrayList<Persona>();
	}

	// DAR ALTA PERSONA
	public void altaPersona(String nombre, int altura, int edad) {
		listaPersonas.add(new Persona(nombre,altura, edad));
		Collections.sort(listaPersonas);
	}

	// LISTAR PERSONAS
	public void listarPersonas() {
		for (Persona per : listaPersonas) {
			System.out.println(per);
		}
	}

	// GUARDAR DATOS
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaPersonas);
			escritor.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// CARGAR DATOS
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream escritor = new ObjectInputStream(new FileInputStream(new File("src/datos.dat")));
			listaPersonas = (ArrayList<Persona>) escritor.readObject();
			escritor.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
