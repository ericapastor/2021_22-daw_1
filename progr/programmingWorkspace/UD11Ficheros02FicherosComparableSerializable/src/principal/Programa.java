package principal;

import comparableserializable.GrupoPersonas;

public class Programa {

	public static void main(String[] args) {
		
		GrupoPersonas grupo = new GrupoPersonas();
/*
		grupo.altaPersona("Mario", 187, 22);
		grupo.altaPersona("Pepe", 176, 52);
		grupo.altaPersona("Manuel", 158, 27);
		grupo.altaPersona("David", 164, 25);
		grupo.altaPersona("Alberto", 184, 80);

		System.out.println();
		System.out.println("Guardando datos .....");
		grupo.guardarDatos();*/
		System.out.println("Cargando datos .....");
		grupo.cargarDatos();
		
		System.out.println();
		System.out.println("Mostramos personas");
		grupo.listarPersonas();	

	}
}