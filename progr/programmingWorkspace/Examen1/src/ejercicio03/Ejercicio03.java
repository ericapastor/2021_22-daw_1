package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {

		/*
		 * Ejercicio 3 (3 ptos) Realizar un programa que permite validar un c�digo de
		 * descuento solicitando el nombre de usuario y un c�digo. Se crear� un String
		 * para el usuario, otro String para el c�digo de descuento y se les dar�
		 * valores. El programa comprobar� que el usuario y los c�digos son iguales a
		 * los creados (coincidencia completa: may�sculas/min�sculas). En caso de ser
		 * introducir los datos correctos (ambos, usuario y c�digo), se mostrar� el
		 * mensaje por pantalla �El descuento es v�lido�. En caso contrario, mostrar� el
		 * mensaje �Nombre de usuario o descuento Incorrecto�.
		 */

		Scanner input = new Scanner(System.in);

		// creo usuario y codigo validos
		String uvalid;
		String cvalid;
		uvalid = "ernesto lopez";
		cvalid = "345Xt4";

		// solicito un nombre de usuario y un codigo

		System.out.println("Dame un nombre de usuario.");
		String usuario = input.nextLine();
		System.out.println("Dame un codigo.");
		String codigo = input.nextLine();

		boolean valid1 = uvalid.equalsIgnoreCase(usuario);
		boolean valid2 = cvalid.equalsIgnoreCase(codigo);

		if (valid1 && valid2) {
			System.out.println("El descuento es valido.");
		} else {
			System.out.println("Nombre de usuario o descuento: Incorrecto.");
		}

		input.close();

	}

}
