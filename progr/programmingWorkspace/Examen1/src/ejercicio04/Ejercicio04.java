package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {

		/*
		 * Ejercicio 4 (3 ptos) Crear un programa que muestre un men� y pida seleccionar
		 * una opci�n (n�mero entero). El men� tendr� 3 opciones: 1 � Comprobar n�mero 2
		 * � Comprobar car�cter 3 - Salir a) (1 pto)
		 *
		 * Opci�n 1: La opci�n 1 pide un n�mero entero. Si el n�mero introducido es
		 * negativo, lo indica y no hace nada m�s. Si es positivo el programa indica si
		 * es m�ltiplo de 5 o no. b) (1 pto)
		 * 
		 * 
		 * Opci�n 2: La opci�n 2 pide una cadena. Si he introducido m�s de un car�cter
		 * lo indica por mensaje y no hace nada m�s. Si he introducido un solo car�cter,
		 * indica si es una letra, un n�mero o un signo.
		 * 
		 * c) (1 pto) Usar un switch para ejecutar la opci�n pedida. Si selecciono otra
		 * opci�n no contemplada el programa me avisar�. El men� se mostrar� una sola
		 * vez. Si selecciono Salir, el programa termina.
		 */

		Scanner input = new Scanner(System.in);

		// muestro las opciones
		System.out.println("Selecciona una opcion escribiendo el numero:");
		System.out.println("1-Comprobar numero");
		System.out.println("2-Comprobar caracter");
		System.out.println("3-Salir");

		int opcion = input.nextInt();

		// hago un switch para cada opcion

		switch (opcion) {
		case 1:

			// pido numero
			System.out.println("Dime el numero que quieres comprobar.");
			int numero = input.nextInt();

			// declaro booleans
			boolean false1;
			boolean mult5;
			false1 = numero < 0;
			mult5 = numero % 5 == 0;

			// indico si es negativo, o si es positivo y multiplo de 5
			if (false1) {
				System.out.println("El numero no puede ser negativo.");
			} else if (mult5) {
				System.out.println("El numero es multiplo de 5.");
			} else {
				System.out.println("El numero no es multiplo de 5.");
			}
			break;

		case 2:

			// pido una cadena, y limpio el buffer

			System.out.println("Dame una cadena o caracter");
			input.nextLine();
			String cadena = input.nextLine();

			// declaro booleans
			boolean chain;
			boolean character;
			char charvalue;
			chain = cadena.length() > 1;
			character = cadena.length() == 1;
			charvalue = cadena.charAt(0);

			// indico si tengo mas de un caracter o el valor de si solo fuera uno:
			if (chain) {
				System.out.println("La cadena tiene mas de un caracter");
			} else if (character) {
				System.out.println("El valor del caracter es: " + (int) charvalue + ".");
			} else {
				System.out.println("La opcion no es valida.");
			}
			break;

		case 3:
			System.out.println("Fin del programa.");
			break;

		default:
			System.out.println("Las opciones solo son entre 1 y 3.");
		}

		input.close();

	}

}
