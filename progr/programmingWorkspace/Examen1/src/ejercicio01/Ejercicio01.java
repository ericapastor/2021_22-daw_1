package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// Ejercicio 1 (2 ptos) Programa que pide tres notas y calcula su promedio,
		// sabiendo que la 1� y
		// 2� tienen un 15% de peso cada una y la 3� un 70% de peso

		Scanner input = new Scanner(System.in);

		// Pido las 3 notas
		System.out.println("Primera nota.");
		double nota1 = input.nextDouble();
		System.out.println("Segunda nota.");
		double nota2 = input.nextDouble();
		System.out.println("Tercera nota.");
		double nota3 = input.nextDouble();

		// calculos
		double promedio;
		promedio = (nota1 * 0.15) + (nota2 * 0.15) + (nota3 * 0.7);

		// doy el resultado
		System.out.println("El promedio de estas notas es: " + promedio + ".");

		input.close();

	}

}
