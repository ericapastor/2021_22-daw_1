package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {

		/*
		 * Ejercicio 2 (2 ptos) Programa que pide un primer n�mero correspondiente a la
		 * cantidad de hombres, un segundo n�mero correspondiente a la cantidad de
		 * mujeres y muestra los siguientes mensajes: La cantidad de hombres es: La
		 * cantidad de mujeres es: El total es: El porcentaje de hombres es: El
		 * porcentaje de mujeres es:
		 */
		Scanner input = new Scanner(System.in);

		// pido los datos
		System.out.println("Dime el numero de hombres.");
		int hombres = input.nextInt();
		System.out.println("Dime el numero de mujeres.");
		int mujeres = input.nextInt();

		// muestro las cantidades:

		System.out.println("La candidad de hombres es: " + hombres + ".");
		System.out.println("La cantidad de mujeres es: " + mujeres + '.');

		// hago calculos para el total y los porcentajes

		int total;
		total = hombres + mujeres;

		double porcentajeh;
		double porcentajem;
		porcentajeh = (hombres * 100) / total;
		porcentajem = (mujeres * 100) / total;

		// muestro los calculos

		System.out.println("El total es: " + total + ".");

		System.out.println("El porcentaje de hombres es: " + porcentajeh + ".");

		System.out.println("El porcentaje de mujeres es: " + porcentajem + ".");

		input.close();
	}

}
