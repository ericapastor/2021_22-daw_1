package ejs07ej09;

public class Ej07 {
	
	public static void main(String[] args) {
		
		Empleado empleado = new Empleado("Raquel");
		Operario operario = new Operario("Juan");
		Director director = new Director("Maria");
		Oficial oficial = new Oficial("Andrea");
		Tecnico tecnico = new Tecnico("Carlos");

		System.out.println(empleado);
		System.out.println(operario);
		System.out.println(director);
		System.out.println(oficial);
		System.out.println(tecnico);
		
	}

}
