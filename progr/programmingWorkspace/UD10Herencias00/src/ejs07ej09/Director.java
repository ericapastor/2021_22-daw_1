package ejs07ej09;

public class Director extends Empleado {

	public Director(String nombre) {
		super(nombre);
	}

	@Override
	public String toString() {
		return "[" + super.toString() + "] -> Director";
	}

}
