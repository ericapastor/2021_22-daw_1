package ej08;

public class ElementNotFoundRuntimeException extends Exception {
	
	/**
	 * tuneamos ElementNotFoundRuntimeException
	 */
	private static final long serialVersionUID = 1L;

	public ElementNotFoundRuntimeException(String matricula) {
		super("No existe el vehiculo con matricula "+matricula);
	}

}
