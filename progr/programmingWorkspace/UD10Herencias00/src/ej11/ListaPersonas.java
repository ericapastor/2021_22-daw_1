package ej11;

import java.util.ArrayList;

//si yo quiero sobreescribir un metodo que pertenece a otra clase
//en este caso voy a sobreescribir add que es de ArrayList
//tengo que hacer dos cosas
//heredar de esa clase -> extends
//@override del metodo que quiero tunear (sobreescribir)

//nuevamente, cuando heredas ciertos metodos
//pedira incorporar el serial

public class ListaPersonas extends ArrayList<Persona> {

	/**
	 * tuneamos el metodo add
	 */
	private static final long serialVersionUID = 1L;

	// para sobreescribir metodos, una vez esta el extends
	// source -> override

	// opcion1 a�adir persona (sobrescribimos)
	@Override
	public void add(int indice, Persona persona) {
		if (persona == null || (persona != null && !existeDni(persona.getDni()))) {
			super.add(indice, persona);
		}
	}

	// opcion2 a�adir persona (sobrescribimos)
	@Override
	public boolean add(Persona persona) {
		if (persona == null || (persona != null && !existeDni(persona.getDni()))) {
			super.add(persona);
			return true;
		}
		return false;
	}

	// metodo estandar existeDni
	// al sobreescribir los add ya no tengo que comprobar si existe o no dni
	// cada vez que se inserta una persona
	public boolean existeDni(String dni) {
		for (Persona persona : this) {
			if (persona != null && persona.getDni() != null && persona.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}

}
