package ej03ej04ej05ej06;

public class Coche extends Vehiculo {

private double km;
	
	public Coche(String nombre, String marca, int plazas, double km) {
		super(nombre, marca, plazas);
		this.km=km;
	}

	public double getKm() {
		return km;
	}

	public void setKm(double km) {
		this.km = km;
	}

	//toString usando el toString de Vehiculo
	@Override
	public String toString() {
		return "Coche [km=" + km + ", toString()=" + super.toString() + "]";
	}
	
}
