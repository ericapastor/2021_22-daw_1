package ej03ej04ej05ej06;

public class Ej03Ej04Ej05Ej06 {
	
	public static void main(String[] args) {
		Barco barco = new Barco("1234","Zodiac",7,"Juanjo",false);
		System.out.println("BARCO");
		System.out.println(barco);
		System.out.println("nombre capitan "+barco.getNombreCapitan());
		System.out.println("tiene vela "+ barco.isTieneVela());
		
		System.out.println();
		Avion avion = new Avion("12345F","Boeing",2,2);
		System.out.println("AVION");
		System.out.println(avion);
		System.out.println("Misiles "+avion.getNumMisiles());

		System.out.println();
		Coche coche =new Coche("FDD-1244","Peugeot",5,120.7);
		System.out.println("COCHE");
		System.out.println(coche);
		System.out.println("Km "+coche.getKm());
		
		//si un metodo lo hago static, no se hereda, se usa desde la clase padre
		System.out.println();
		System.out.println("Vehiculo.getCantidadInstanciasStatic()");
		System.out.println(Vehiculo.getCantidadInstanciasStatic());
		System.out.println("barco.getCantidadInstancias())");
		System.out.println(barco.getCantidadInstancias());
		System.out.println("coche.getCantidadInstancias())");
		System.out.println(coche.getCantidadInstancias());
		System.out.println("avion.getCantidadInstancias())");
		System.out.println(avion.getCantidadInstancias());
		

	}

}
