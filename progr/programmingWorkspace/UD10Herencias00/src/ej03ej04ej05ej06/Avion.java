package ej03ej04ej05ej06;

public class Avion extends Vehiculo {
	
	private int numMisiles;

	public Avion(String matricula, String marca, int plazas, int numMisiles) {
		super(matricula, marca, plazas);
		this.numMisiles = numMisiles;
	}

	public int getNumMisiles() {
		return numMisiles;
	}

	public void setNumMisiles(int numMisiles) {
		this.numMisiles = numMisiles;
	}

	public void disparar() {
		if (numMisiles > 0) {
			numMisiles--;
		}
	}

	// toString usando el toString heredado
	@Override
	public String toString() {
		return "Avion [numMisiles=" + numMisiles + ", toString()=" + super.toString() + "]";
	}


}
