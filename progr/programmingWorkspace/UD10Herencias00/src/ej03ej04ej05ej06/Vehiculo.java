package ej03ej04ej05ej06;

public class Vehiculo  {
	
	//public para todas las clases
		//private la clase en la que estoy
		//nada o package en el paquete
		//protected clase y heredadas
		
		//static de clase
		protected static int cantidadInstancias;
		
		protected String matricula;
		protected String marca;
		protected int plazas;
		
		public Vehiculo(String matricula, String marca, int plazas) {
			this.matricula = matricula;
			this.marca = marca;
			this.plazas = plazas;
			Vehiculo.cantidadInstancias++;
		}

		//getter static
		public static int getCantidadInstanciasStatic() {
			return cantidadInstancias;
		}
		
		//getter no static
		public int getCantidadInstancias() {
			return cantidadInstancias;
		}

		//no hay setter
		
		public String getMatricula() {
			return matricula;
		}

		public void setMatricula(String matricula) {
			this.matricula = matricula;
		}

		public String getMarca() {
			return marca;
		}

		public void setMarca(String marca) {
			this.marca = marca;
		}

		public int getPlazas() {
			return plazas;
		}

		public void setPlazas(int plazas) {
			this.plazas = plazas;
		}

		@Override
		public String toString() {
			return "Vehiculo [matricula=" + matricula + ", marca=" + marca + ", plazas=" + plazas + "]";
		}

}
