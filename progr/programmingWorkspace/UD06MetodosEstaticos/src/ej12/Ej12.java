package ej12;

import java.util.Scanner;

import ej00.Methods;

public class Ej12 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Create a program that shows a String using the method readCharacters(). It
		 * will read characters until digit 0 is introduced, putting together all
		 * previous characters separating them by a space
		 */
		System.out.println("The chain is " + Methods.readCharacters());
		program.close();
	}

}
