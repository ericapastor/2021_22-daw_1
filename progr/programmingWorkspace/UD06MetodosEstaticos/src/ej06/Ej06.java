package ej06;

import java.util.Scanner;

import ej00.Methods;

public class Ej06 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * App which uses the static method showTableAscii() to show all character in
		 * the ASCII table, indicating its code by the side
		 */
		Methods.showTableAscii();
		program.close();
	}

}
