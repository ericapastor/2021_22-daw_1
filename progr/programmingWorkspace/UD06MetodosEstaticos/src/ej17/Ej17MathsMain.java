package ej17;

import java.util.Scanner;

public class Ej17MathsMain {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Create a class called Mathematics which does not have a main method. We're
		 * going to create in it several public methods, and only Random from class Math
		 * can be used to solve them
		 */
		// (Absolute value of 2 and -2 is 2)
		// int absolute(int a): Returns the absolute value of a
		// double absolute(double a)
		// int maximum(int a, int b) : Returns the biggest absolute value
		// double maximum(double a, double b)
		// int minimum(int a, int b)
		// double minimum(double a, double b)
		// int round(double a) : Round a decimal depending on its first one
		// int roundUp(double a) : Round up a decimal
		// int roundDown(double a) : Round down a decimal
		// int power(int base, int exponent) : Returns the power
		// int random(int final) : Randomly generates a number between 0 and final
		// int random(int start, int final) : Random number between start and final
		/*
		 * Create another class called Exercise17 with the main method showing the use
		 * of all others from Mathematics class
		 */
		int option;
		System.out.println("==========================");
		System.out.println(" 1 - Absolute values.");
		System.out.println(" 2 - Maximum and minimum.");
		System.out.println(" 3 - Rounding numbers.");
		System.out.println(" 4 - Power.");
		System.out.println(" 5 - Random numbers.");
		System.out.println("==========================");
		option = program.nextInt();
		switch (option) {
		case 1:
			Ej17Methods.first();
			break;
		case 2:
			Ej17Methods.second();
			break;
		case 3:
			Ej17Methods.third();
			break;
		case 4:
			Ej17Methods.fourth();
			break;
		case 5:
			Ej17Methods.fifth();
			break;
		default:
			System.out.println("Option is not valid.");
			break;
		}
		program.close();
	}

}
