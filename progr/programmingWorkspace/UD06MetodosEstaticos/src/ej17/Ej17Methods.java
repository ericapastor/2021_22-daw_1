package ej17;

import java.util.Scanner;

public class Ej17Methods {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Create a class called Mathematics which does not have a main method. We're
		 * going to create in it several public methods, and only Random from class Math
		 * can be used to solve them
		 */
		// (Absolute value of 2 and -2 is 2)
		// int absolute(int a): Returns the absolute value of a
		// double absolute(double a)
		// int maximum(int a, int b) : Returns the biggest absolute value
		// double maximum(double a, double b)
		// int minimum(int a, int b)
		// double minimum(double a, double b)
		// int round(double a) : Round a decimal depending on its first one
		// int roundUp(double a) : Round up a decimal
		// int roundDown(double a) : Round down a decimal
		// int power(int base, int exponent) : Returns the power
		// int random(int final) : Randomly generates a number between 0 and final
		// int random(int start, int final) : Random number between start and final
		/*
		 * Create another class called Exercise17 with the main method showing the use
		 * of all others from Mathematics class
		 */
		program.close();
	}

	public static void first() {
		int number;
		double decimal;
		System.out.println("*******************\nAbsolute values.\n*******************");
		System.out.println("Type a whole number.");
		number = program.nextInt();
		System.out.println("Type a decimal number.");
		decimal = program.nextDouble();
		System.out.println("Absolute whole number value is: " + absolute(number));
		System.out.println("Absolute decimal value is: " + absolute(decimal));
	}

	public static void second() {
		int number;
		int num;
		double decimal;
		double dec;
		System.out.println("********************\nMaximum and minimum.\n********************");
		System.out.println("Type first whole number.");
		number = program.nextInt();
		System.out.println("Type the second whole number.");
		num = program.nextInt();
		System.out.println("Type first decimal number.");
		decimal = program.nextDouble();
		System.out.println("Type the second decimal number.");
		dec = program.nextDouble();
		System.out.println("Biggest whole number is: " + maximum(number, num));
		System.out.println("Biggest decimal number is: " + maximum(decimal, dec));
		System.out.println("Smallest whole number is: " + minimum(number, num));
		System.out.println("Smallest decimal number is: " + minimum(decimal, dec));
	}

	public static void third() {
		double decimal;
		System.out.println("*******************\nRounding numbers.\n*******************");
		System.out.println("Type a decimal to round.");
		decimal = program.nextDouble();
		System.out.println("Rounded depending on the first decimal: " + round(decimal));
		System.out.println("Type three more decimals.");
		System.out.println("1");
		decimal = program.nextDouble();
		roundUp(decimal);
		roundDown(decimal);
		System.out.println("2");
		decimal = program.nextDouble();
		roundUp(decimal);
		roundDown(decimal);
		System.out.println("3");
		decimal = program.nextDouble();
		roundUp(decimal);
		roundDown(decimal);
	}

	public static void fourth() {
		int base;
		int exponent;
		System.out.println("*******************\nPowers: base and exponent.\n*******************");
		System.out.println("Type the base.");
		base = program.nextInt();
		System.out.println("Type the exponent.");
		exponent = program.nextInt();
		System.out.println("The power is: " + power(base, exponent));
	}

	public static void fifth() {
		int finalNum;
		int startNum;
		System.out.println("Type a number.");
		finalNum = program.nextInt();
		System.out.println("Random number between that one and zero: " + random(finalNum));
		System.out.println("Type one more number.");
		startNum = program.nextInt();
		System.out.println("Random number between the two numbers you typed: " + random(startNum, finalNum));
	}

	public static int absolute(int a) {
		a = (a < 0) ? a = a - a * 2 : a;
		return a;
	}

	public static double absolute(double a) {
		int b;
		b = (a < 0) ? (int) (a = a - (a * 2)) : (int) a;
		a = (double) b;
		return a;
	}

	public static int maximum(int a, int b) {
		int c;
		c = (a > b) ? a : b;
		return c;
	}

	public static double maximum(double a, double b) {
		double c;
		c = (a > b) ? a : b;
		return c;
	}

	public static int minimum(int a, int b) {
		int c;
		c = (a < b) ? a : b;
		return c;
	}

	public static double minimum(double a, double b) {
		double c;
		c = (a < b) ? a : b;
		return c;
	}

	public static double round(double a) {
		int b;
		a *= 10;
		b = (int) a;
		a = (double) b / 10;
		return a;
	}

	public static double roundUp(double a) {
		double b = a * 10;
		int c = (int) b % 10;
		if (c >= 5) {
			c = (int) a + 1;
			a = (double) c;
			System.out.println("Rounded up: " + a);
		} else {
			a = 0;
		}
		return a;
	}

	public static double roundDown(double a) {
		double b = a * 10;
		int c = (int) b % 10;
		if (c < 5) {
			c = (int) a;
			a = (double) c;
			System.out.println("Rounded down: " + a);
		} else {
			a = 0;
		}
		return a;
	}

	public static int power(int a, int b) {
		int c = a;
		for (int i = 1; i < b; i++) {
			c *= a;
		}
		return c;
	}

	public static int random(int f) {
		f = (int) Math.round(Math.random() * f);
		return f;
	}

	public static int random(int s, int f) {
		if (f > s) {
			f = (int) Math.round(Math.random() * (f - s) + s);
		} else {
			f = (int) Math.round(Math.random() * (s - f) + f);
		}
		return f;
	}

}
