package ej05;

import java.util.Scanner;

import ej00.Methods;

public class Ej05 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Create an App which generates a random whole number between 0 and the number
		 * given by the use of a static method called random(int finalNumber)
		 */
		int finalNumber;
		System.out.println("Type a whole number.");
		finalNumber = program.nextInt();
		System.out.println(Methods.random(finalNumber));
		program.close();
	}

}
