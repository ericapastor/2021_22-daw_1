package ej01;

import java.util.Scanner;

import ej00.Methods;

public class Ej01 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Program that asks for your age and checks your adultness based on the method
		 * "adult"
		 */

		int age;
		boolean isAdult;

		System.out.println("What is your age?");
		age = program.nextInt();
		isAdult = Methods.adult(age);
		if (isAdult == true) {
			System.out.println("Your an adult, older than 18.");
		} else {
			System.out.println("You're not an adult.");
		}
		program.close();
	}

}
