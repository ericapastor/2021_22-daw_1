package ej00;

import java.util.Scanner;

public class Methods {

	// EXERCISE 1
	/*
	 * Create a method called "adult", which receives a whole number (age) and
	 * returns a boolean, depending on if it's bigger than 18 or smaller.
	 */

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		program.close();
	}

	public static boolean adult(int numero) {
		boolean isAdult = false;
		if (numero >= 18) {
			isAdult = true;
		}
		return isAdult;
	}

	// EXERCISE 2
	/*
	 * Create two methods in the class Methods, maximum and minimum. Receiving two
	 * whole numbers the program will return the bigger number, and the smaller
	 * number.
	 */
	public static int maximum(int num1, int num2) {
		int bigger = 0;
		if (num1 > num2) {
			System.out.println(num1 + " is bigger.");
			bigger = num1;
		} else if (num2 > num1) {
			System.out.println(num2 + " is bigger.");
			bigger = num2;
		} else {
			System.out.println("Both numbers are the same.");
		}
		return bigger;
	}

	public static int minimum(int num1, int num2) {
		int smaller = 0;
		if (num1 < num2) {
			System.out.println(num1 + " is smaller.");
			smaller = num1;
		} else if (num2 < num1) {
			System.out.println(num2 + " is smaller.");
			smaller = num2;
		} else {
			System.out.println("Both numbers are the same.");
		}
		return smaller;
	}

	// EXERCISE 3
	/*
	 * EJERCICIO 3 Program that returns true or false by checking the the char given
	 * is a character or a digit
	 */
	public static boolean isCharacter(char character) {
		boolean condition = false;
		if ((character >= 'A' && character <= 'Z') || (character >= 'a' && character <= 'z')) {
			condition = true;
		}
		return condition;
	}

	// EXERCISE 4
	/*
	 * Create an App which uses an isDigit static Method to check if a character is
	 * a digit or not.
	 */
	public static boolean isPrime(int number) {
		boolean condition = true;
		for (int i = 2; i < number; i++) {
			if (number % i == 0) {
				condition = false;
				break;
			}
		}
		return condition;
	}

	// EXERCISE 5
	/*
	 * Create an App which generates a random whole number between 0 and the number
	 * given by the use of a static method called random(int finalNumber)
	 */
	public static int random(int number) {
		int numRandom;
		numRandom = (int) Math.round(Math.random() * number);
		return numRandom;
	}

	// EXERCISE 6
	/*
	 * App which uses the static method showTableAscii() to show all character in
	 * the ASCII table, indicating its code by the side
	 */
	public static void showTableAscii() {
		String asciiTable = "decimal " + "|| ASCII \n";
		for (int i = 0; i < 256; i++) {
			asciiTable = asciiTable + i + "      |  " + (char) i + "\n";
		}
		System.out.println(asciiTable);
	}

	// EXERCISE 7
	/*
	 * App that calls the method alphabet(), which shows on screen the upper case
	 * letters from A to Z in the next way: AAA, AAB, AAC, ... ZZZ.
	 */
	public static void alphabet() {
		for (int i = 65; i <= 90; i++) {
			for (int j = 65; j <= 90; j++) {
				for (int k = 65; k <= 90; k++) {
					System.out.println((char) i + "" + (char) j + "" + (char) k);
				}
			}
		}
	}

	// EXERCISE 8
	/*
	 * App that generates a random whole number, for which it uses a method called
	 * random(int start,int end). Between these numbers the random number is created
	 */
	public static int readNum() {
		int number;
		number = program.nextInt();
		return number;
	}

	public static int randomBetweenTwo(int max, int min) {
		int random;
		random = (int) Math.round(Math.random() * (max - min) + min);
		if (min > max) {
			random = (int) Math.round(Math.random() * (min - max) + max);
		}
		return random;
	}

	// EXERCISE 9
	/*
	 * App that uses a method called randomWord (int lenght), which receives how
	 * many characters we want the chain to have and it returns a String of the same
	 * lenght with random letters between A-Z in upper case.
	 */
	public static String randomWord(int lenght) {
		String random = "";
		for (int i = 0; i < lenght; i++) {
			random = random + "" + (char) Methods.randomBetweenTwo(65, 90);
		}
		return random;
	}

	// EXERCISE 10
	/*
	 * App that checks a String and says if it's a whole number or not, depending on
	 * if it encounters a non digit character
	 */
	public static String readString() {
		String chain;
		chain = program.nextLine();
		return chain;
	}

	public static boolean isWhole(String number) {
		boolean isWhole = true;
		if ((number.charAt(0) == '-' && number.length() > 1) || (number.charAt(0) >= '0' && number.charAt(0) <= '9'))
			for (int i = 0; i < number.length(); i++) {
				if (number.charAt(i) < '0' || number.charAt(i) > '9') {
					isWhole = false;
					break;
				}
			}
		return isWhole;
	}

	public static boolean positive(String number) {
		boolean isPositive = true;
		for (int i = 0; i < number.length(); i++) {
			if (number.charAt(i) == '-') {
				isPositive = false;
				break;
			}
		}
		return isPositive;
	}

	// EXERCISE 11
	/*
	 * App that asks for a lower case word and it uses the method cypher(String
	 * chain, int gap), which returns a String that switches every character of the
	 * chain for the character in as many positions as the gap indicates. For
	 * example: chain="abcd"; gap=2; chainGap="cdef"; If we get to 'z' we start
	 * again from 'a'
	 */
	public static String cypher(String chain, int gap) {
		String gapChain = "";
		int character;
		/*
		 * gap = gap % 26; for (int i = 0; i < chain.length(); i++) { character = (char)
		 * (chain.charAt(i) + gap); if (character > 'z') { character = (char) ('a' + gap
		 * - 1); }
		 */
		for (int i = 0; i < chain.length(); i++) {
			character = chain.charAt(i);
			if (chain.charAt(i) + gap > 122) {
				character = character - 26;
			}
			if (chain.charAt(i) + gap < 97) {
				character = character + 26;
			}
			gapChain = gapChain + (char) (character + gap);
		}
		return gapChain;

	}

	// EXERCISE 12
	/*
	 * Create a program that shows a String using the method readCharacters(). It
	 * will read characters until digit 0 is introduced, putting together all
	 * previous characters separating them by a space
	 */
	public static String readCharacters() {
		String chain = "";
		char character;
		do {
			System.out.println("Type the character.");
			character = program.nextLine().charAt(0);
			if (character == '0') {
				break;
			}
			chain = chain + character + " ";
		} while (character != '0');
		return chain;
	}

	// EXERCISE 13
	/*
	 * Same as the previous exercise, but with a method called readChains() which
	 * reads Strings until we introduce the chain "end". It returns a String,
	 * showing all Strings separated by the character ':'
	 */
	public static String readChains() {
		String chains = "";
		String chain = "";
		do {
			System.out.println("Type the sentence.");
			chain = program.nextLine();
			if (chain.equals("end")) {
				chains = chains.substring(0, chains.lastIndexOf(':'));
				break;
			}
			chains = chains + chain + ":";
		} while (!chain.equals("end"));
		return chains;
	}

	// EXERCISE 14
	/*
	 * App that asks for a chain and through the method 'fromStringToInt' it returns
	 * an int with the right value. Method 'Integer.parseInt' cannot be used. If the
	 * parameter is not a whole number I must return '0', using the method isWhole()
	 * from exercise 10.
	 */
	public static int fromStringToInt(String number) {
		int num = 0;
		int exponent = 0;
		if (Methods.isWhole(number) == true) {
			for (int i = number.length() - 1; i >= 0; i--) {
				if (number.charAt(i) == '-') {
					num = -num;
					break;
				}
				char character = number.charAt(i);
				num = num + ((character - 48) * (int) Math.pow(10, exponent));
				exponent++;
			}
		} else {
			System.out.println("That's not a whole number.");
		}
		return num;
	}

	// EXERCISE 15
	/*
	 * Program that reads a String compound by words separated by a space.
	 * Afterwards, it gives that String to the method longestWord(String chain),
	 * which returns a String with the longest word.
	 */
	public static String longestWord(String st) {
		String longest = "";
		String acumulator = "";
		for (int i = 0; i < st.length(); i++) {
			if (st.charAt(i) == ' ') {
				if (acumulator.length() > longest.length()) {
					longest = acumulator;
					acumulator = "";
					System.out.println("New longest: " + longest);
				} else {
					acumulator = "";
				}
			}

			acumulator = acumulator + "" + st.charAt(i);
		}
		if (acumulator.length() > longest.length()) {
			longest = acumulator;
		}
		return longest;
	}

	// EXERCISE 16
	/*
	 * Program that reads a String and uses the method inverString (String chain) to
	 * return a String with the character order inverted from the end to the start
	 */
	public static String invertString(String st) {
		String neu = "";
		for (int i = st.length() - 1; i >= 0; i--) {
			neu = neu + "" + st.charAt(i);
		}
		return neu;
	}

	// EXERCISE 17
	/* Is in each own class */
}
