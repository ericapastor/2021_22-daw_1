package ej02;

import java.util.Scanner;

import ej00.Methods;

public class Ej02 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Create two methods in the class Methods, maximum and minimum. Receiving two
		 * whole numbers the program will return the bigger number, and the smaller
		 * number.
		 */
		int num1;
		int num2;
		System.out.println("Tell me a number.");
		num1 = program.nextInt();
		System.out.println("Tell me another number");
		num2 = program.nextInt();
		Methods.maximum(num1, num2);
		Methods.minimum(num1, num2);
		program.close();
	}

}
