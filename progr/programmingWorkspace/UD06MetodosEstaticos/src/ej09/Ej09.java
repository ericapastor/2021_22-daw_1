package ej09;

import java.util.Scanner;

import ej00.Methods;

public class Ej09 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * App that uses a method called randomWord (int lenght), which receives how
		 * many characters we want the chain to have and it returns a String of the same
		 * lenght with random letters between A-Z in upper case.
		 */
		int number;
		System.out.println("How many characters do you want to chain to have?");
		number = Methods.readNum();
		System.out.println("Here is your randomized chain: ");
		System.out.println(Methods.randomWord(number));
		program.close();
	}

}
