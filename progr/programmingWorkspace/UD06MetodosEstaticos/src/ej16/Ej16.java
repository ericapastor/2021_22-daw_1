package ej16;

import java.util.Scanner;

import ej00.Methods;

public class Ej16 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Program that reads a String and uses the method inverString (String chain) to
		 * return a String with the character order inverted from the end to the start
		 */
		String word;
		System.out.println("Type the word.");
		word = program.nextLine();
		System.out.println("The word inverted: " + Methods.invertString(word));
		program.close();
	}

}
