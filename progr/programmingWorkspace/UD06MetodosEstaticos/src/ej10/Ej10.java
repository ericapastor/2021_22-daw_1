package ej10;

import java.util.Scanner;

import ej00.Methods;

public class Ej10 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * App that checks a String and says if it's a whole number or not and if it's
		 * positive, depending on if it encounters a non digit character
		 */
		String chain;
		do {
			System.out.println("******************************************************************\n"
					+ "Type a number and I check if it's a whole and/or positive number.\n"
					+ "******************************************************************");
			chain = Methods.readString();
			if (Methods.isWhole(chain) == true) {
				System.out.println("It is a whole number.");
			} else {
				System.out.println("It's not a whole number.");
			}
			if (Methods.positive(chain) == true) {
				System.out.println("It's a positive number.");
			} else {
				System.out.println("It's a negative number.");
			}
		} while (!chain.equals("end"));
		program.close();
	}

}
