package ej14;

import java.util.Scanner;

import ej00.Methods;

public class Ej14 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * App that asks for a chain and through the method 'fromStringToInt' it returns
		 * an int with the right value. Method 'Integer.parseInt' cannot be used. If the
		 * parameter is not a whole number I must return '0', using the method isWhole()
		 * from exercise 10.
		 */
		String number;
		System.out.println("Give me a number.");
		number = program.nextLine();
		System.out.println("This is the number parsed to int: " + Methods.fromStringToInt(number));
		program.close();
	}

}
