package ej08;

import java.util.Scanner;

import ej00.Methods;

public class Ej08 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * App that generates a random whole number, for which it uses a method called
		 * random(int start,int end). Between these numbers the random number is created
		 */
		int num1;
		int num2;
		do {
			System.out.println("Type a number.");
			num1 = Methods.readNum();
			System.out.println("Type another number.");
			num2 = Methods.readNum();
			System.out.println("Random number between those two is: " + Methods.randomBetweenTwo(num1, num2));
		} while (num1 >= 0 || num2 >= 0);
		program.close();
	}

}
