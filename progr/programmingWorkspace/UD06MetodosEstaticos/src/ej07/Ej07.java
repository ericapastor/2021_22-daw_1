package ej07;

import java.util.Scanner;

import ej00.Methods;

public class Ej07 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * App that calls the mathod alphabet(), which shows on screen the upper case
		 * letters from A to Z in the next way: AAA, AAB, AAC, ... ZZZ.
		 */
		Methods.alphabet();
		program.close();
	}

}
