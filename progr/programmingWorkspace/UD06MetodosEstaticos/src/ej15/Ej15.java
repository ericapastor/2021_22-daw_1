package ej15;

import java.util.Scanner;

import ej00.Methods;

public class Ej15 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Program that reads a String compound by words separated by a space.
		 * Afterwards, it gives that String to the method longestWord(String chain),
		 * which returns a String with the longest word.
		 */
		String chain;
		System.out.println("Type several words and I will say which one is the longest one.");
		chain = Methods.readString();
		System.out.println("The longest word is: " + Methods.longestWord(chain));
		program.close();
	}

}
