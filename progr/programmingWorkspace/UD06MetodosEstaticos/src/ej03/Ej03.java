package ej03;

import java.util.Scanner;

import ej00.Methods;

public class Ej03 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Create an App which uses an isDigit static Method to check if a character is
		 * a digit or not.
		 */
		char character;
		System.out.println("Type a character");
		character = program.nextLine().charAt(0);
		if (Methods.isCharacter(character) == true) {
			System.out.println("It's a character.");
		} else {
			System.out.println("It's not a character it's a digit.");
		}
		program.close();
	}

}
