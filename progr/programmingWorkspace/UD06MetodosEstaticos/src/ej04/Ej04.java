package ej04;

import java.util.Scanner;

import ej00.Methods;

public class Ej04 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Program which checks how many primes are between 1 and the number given by
		 * using the boolean Method isPrime
		 */
		int number;
		String acumulator = "";
		System.out.println("Type a number.");
		number = program.nextInt();
		for (int i = 2; i <= number; i++) {
			if (Methods.isPrime(i) == true) {
				acumulator = acumulator + i + " ";
			}
		}
		System.out.println("Son primos:\n" + acumulator);
		program.close();
	}

}
