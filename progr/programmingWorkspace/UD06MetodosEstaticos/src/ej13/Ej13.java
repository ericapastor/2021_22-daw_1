package ej13;

import java.util.Scanner;

import ej00.Methods;

public class Ej13 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Same as the previous exercise, but with a method called readChains() which
		 * reads Strings until we introduce the chain "end". It returns a String,
		 * showing all Strings separated by the character ':'
		 */
		System.out.println(
				"This programs reads a series of sentences and then unifies them by the separation \nof the character ':'. Type 'end' to finish the reading.");
		System.out.println("\n" + Methods.readChains());
		program.close();
	}

}
