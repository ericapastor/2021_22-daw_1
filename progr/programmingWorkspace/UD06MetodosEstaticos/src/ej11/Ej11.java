package ej11;

import java.util.Scanner;

import ej00.Methods;

public class Ej11 {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * App that asks for a lower case word and it uses the method cypher(String
		 * chain, int gap), which returns a String that switches every character of the
		 * chain for the character in as many positions as the gap indicates. For
		 * example: chain="abcd"; gap=2; chainGap="cdef"; If we get to 'z' we start
		 * again from 'a'
		 */
		String chain;
		int gap;
		System.out.println("Type the sentence.");
		chain = Methods.readString();
		chain = chain.toLowerCase();
		System.out.println("Type the gap.");
		gap = Methods.readNum();
		System.out.println(Methods.cypher(chain, gap));
		program.close();
	}

}
