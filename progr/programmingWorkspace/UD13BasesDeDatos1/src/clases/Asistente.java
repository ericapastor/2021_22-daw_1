package clases;

import java.io.Serializable;
import java.sql.Date;

public class Asistente extends Personal implements Serializable {

	private static final long serialVersionUID = 1L;
	private Date fechaNacimiento;
	private String nacionalidad;

	public Asistente(String dni, String nombre, String fechaNacimiento, String nacionalidad) {
		super(dni, nombre);
		this.fechaNacimiento = Date.valueOf(fechaNacimiento);
		this.nacionalidad = nacionalidad;
	}

	@Override
	public String toString() {
		return super.toString() + "Asistente [fechaNacimiento=" + fechaNacimiento + ", nacionalidad=" + nacionalidad
				+ "]";
	}

	public int compareTo(Personal o) {
		return getDni().compareTo(o.getDni());
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

}
