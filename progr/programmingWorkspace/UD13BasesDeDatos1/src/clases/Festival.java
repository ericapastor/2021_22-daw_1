package clases;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class Festival implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Concierto> listaConciertos;
	private ArrayList<Personal> listaPersonal;
	private Connection conexion;

	public Festival() {
		this.listaConciertos = new ArrayList<Concierto>();
		this.listaPersonal = new ArrayList<Personal>();
	}

	public ArrayList<Concierto> getListaConciertos() {
		return listaConciertos;
	}

	public void setListaConciertos(ArrayList<Concierto> listaConciertos) {
		this.listaConciertos = listaConciertos;
	}

	public ArrayList<Personal> getListaPersonal() {
		return listaPersonal;
	}

	public void setListaPersonal(ArrayList<Personal> listaPersonal) {
		this.listaPersonal = listaPersonal;
	}

	public Connection getConexion() {
		return conexion;
	}

	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}

	// ALTA ARTISTA
	public void altaArtista(String dni, String nombre, String estiloMusica, String cache) {
		listaPersonal.add(new Artista(dni, nombre, estiloMusica, Float.valueOf(cache)));
		Collections.sort(listaPersonal);
	}

	// ALTA ASISTENTE
	public void altaAsistente(String dni, String nombre, String fechaNacimiento, String nacionalidad) {
		listaPersonal.add(new Asistente(dni, nombre, fechaNacimiento, nacionalidad));
		Collections.sort(listaPersonal);
	}

	// ALTA CONCIERTO
	public void altaConcierto(String codigo, String nombre, String horaInicio, String dniArtista) {
		if (compruebaConcierto(codigo) == false && compruebaArtista(dniArtista)) {
			listaConciertos.add(new Concierto(codigo, nombre, horaInicio, devuelveArtista(dniArtista)));
			Collections.sort(listaConciertos);
		} else {
			System.out.println("Ha ocurrido un error");
		}
	}

	// COMPROBAR CONCIERTO
	public boolean compruebaConcierto(String codigo) {
		for (Concierto c : listaConciertos) {
			if (c != null && c.getCodigo().equalsIgnoreCase(codigo)) {
				return true;
			}
		}
		return false;
	}

	// COMPROBAR ARTISTAS
	public boolean compruebaArtista(String dni) {
		for (Personal p : listaPersonal) {
			if (p != null && p instanceof Artista && p.getDni().equalsIgnoreCase(dni)) {
				return true;
			}
		}
		return false;
	}

	// COMPROBAR ASISTENTE
	public boolean compruebaAsistente(String dni) {
		for (Personal p : listaPersonal) {
			if (p != null && p instanceof Asistente && p.getDni().equalsIgnoreCase(dni)) {
				return true;
			}
		}
		return false;
	}

	// DEVOLVER ARTISTAS
	public Artista devuelveArtista(String dni) {
		for (Personal p : listaPersonal) {
			if (p != null && p instanceof Artista && p.getDni().equalsIgnoreCase(dni)) {
				return (Artista) p;
			}
		}
		return null;
	}

	// DEVOLVER ASISTENTES
	public Asistente devuelveAsistente(String dni) {
		for (Personal p : listaPersonal) {
			if (p != null && p instanceof Asistente && p.getDni().equalsIgnoreCase(dni)) {
				return (Asistente) p;
			}
		}
		return null;
	}

	// DEVOLVER CONCIERTOS
	public Concierto devuelveConcierto(String codigo) {
		for (Concierto c : listaConciertos) {
			if (c != null && c.getCodigo().equalsIgnoreCase(codigo)) {
				return c;
			}
		}
		return null;
	}

	// REGISTRAR ASISTENTES EN CONCIERTO
	public void registrarAsistenteConcierto(String codigoConcierto, String dniAsistente) {
		if (compruebaConcierto(codigoConcierto) && compruebaAsistente(dniAsistente)) {
			devuelveConcierto(codigoConcierto).getAsistentes().add(devuelveAsistente(dniAsistente));
		} else {
			System.out.println("Ha ocurrido un error");
		}
	}

	// LISTAR ARTISTAS
	public void listarArtistas() {
		for (Personal p : listaPersonal) {
			if (p instanceof Artista) {
				System.out.println(p);
			}
		}
	}

	// LISTAR ASISTENTES
	public void listarAsistentes() {
		for (Personal a : listaPersonal) {
			if (a instanceof Asistente) {
				System.out.println(a);
			}
		}
	}

	// LISTAR CONCIERTOS
	public void listarConciertos() {
		for (Concierto c : listaConciertos) {
			System.out.println(c);
		}
	}

	// GUARDAR DATOS FICHERO
	public void guardarDatos() {
		try {
			ObjectOutputStream e = new ObjectOutputStream(new FileOutputStream("src/datos.dat"));
			e.writeObject(listaConciertos);
			e.writeObject(listaPersonal);
			e.close();
		} catch (IOException e) {
			System.out.println("Algo ha ido mal con el fichero");
		}
	}

	// CONECTAR BASE DE DATOS
	public void conectarBBDD() {
		String servidor = "jdbc:mysql://localhost:3306/festival3ev";
		try {
			conexion = DriverManager.getConnection(servidor, "root", "");
		} catch (SQLException e) {
			System.out.println("Problema con la base de datos.");
		}
	}

	// GUARDAR ARTISTAS BASE DEDATOS
	public void guardarArtistasBBDD() {
		String query = "INSERT INTO artistas(dni, nombre, estilo, cache) VALUES (?,?,?,?)";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			for (Personal p : listaPersonal) {
				if (p instanceof Artista) {
					sentencia.setString(1, p.getDni());
					sentencia.setString(2, p.getNombre());
					sentencia.setString(3, ((Artista) p).getEstiloMusica());
					sentencia.setFloat(4, ((Artista) p).getCache());
					sentencia.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.out.println("Problema al insertar artistas en la base de datos");
		}
	}

	// CARGAR ARTISTAS DE BASE DE DATOS
	public void cargarArtistasBBDD() {
		String query = "SELECT * FROM artistas";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				System.out.println(resultado.getString(2) + " " + resultado.getString(3) + " " + resultado.getString(4)
						+ " " + resultado.getFloat(5));
			}
		} catch (SQLException e) {
			System.out.println("Problemas cargando artista de la base de datos");
		}
	}

	// CARGAR ASISTENTES DE BASE DE DATOS
	public void cargarAsistentesBBDD() {
		String query = "SELECT * FROM asistentes";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			ResultSet resultado = sentencia.executeQuery();
			while (resultado.next()) {
				System.out.println(resultado.getString(2) + " " + resultado.getString(3) + " " + resultado.getString(4)
						+ " " + resultado.getString(5));
			}
		} catch (SQLException e) {
			System.out.println("Problemas cargando artista de la base de datos");
		}
	}

}
