package programa;

import java.sql.SQLException;

import clases.Festival;

public class Programa {

	public static void main(String[] args) throws SQLException {
		Festival festival = new Festival();
		System.out.println("Alta artistas");
		festival.altaArtista("1234", "Jesus", "Pop", "1234");
		festival.altaArtista("5678", "Maria", "Rock", "5678");
		System.out.println("Alta asistente");
		festival.altaAsistente("2222", "Asistente1", "2001-11-04", "Spain");
		System.out.println("Alta concierto");
		festival.altaConcierto("1111", "Concierto1", "07:11:00", "1234");
		System.out.println("Listar artistas");
		festival.listarArtistas();
		System.out.println("Listar asistentes");
		festival.listarAsistentes();
		System.out.println("Listar conciertos");
		festival.listarConciertos();
		System.out.println();

		System.out.println("Registrar asistente concierto");
		festival.registrarAsistenteConcierto("1111", "2222");
		System.out.println("Listar artistas");
		festival.listarArtistas();
		System.out.println("Listar asistentes");
		festival.listarAsistentes();
		System.out.println("Listar conciertos");
		festival.listarConciertos();
		System.out.println();

		System.out.println("Guardar datos en fichero");
		festival.guardarDatos();
		System.out.println();

		System.out.println("Conectamos BBDD");
		festival.conectarBBDD();
		System.out.println();
		System.out.println("Guardamos artistas en BBDD");
		festival.guardarArtistasBBDD();
		System.out.println("Mostramos artistas de BBDD");
		festival.cargarArtistasBBDD();
		System.out.println();
		System.out.println("Cargamos asistentes de BBDD");
		festival.cargarAsistentesBBDD();

	}

}
