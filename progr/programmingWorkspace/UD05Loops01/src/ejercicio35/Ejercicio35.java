package ejercicio35;

import java.util.Scanner;

public class Ejercicio35 {

	public static void main(String[] args) {

		/*
		 * Realizar un programa que nos pida un número, y nos diga cuantos números hay
		 * entre 1 y n que son primos.
		 */

		Scanner program = new Scanner(System.in);

		int number;
		int counter;
		String primes = "";
		boolean itisPrime = false;

		System.out.println("Type a number.");
		number = program.nextInt();

		for (int i = 1; i <= number; i++) {
			counter = 0;
			for (int j = 1; j <= i; j++) {
				if (i % j == 0) {
					counter++;
				}
			}
			if (counter == 2) {
				itisPrime = true;
				primes = primes + i + " ";
			}
		}
		
		if (itisPrime == true) {
			System.out.println("There are primes in between.\nThese are:\n" + primes);
		} else {
			System.out.println("There are no primes in between.");
		}

		program.close();

	}

}
