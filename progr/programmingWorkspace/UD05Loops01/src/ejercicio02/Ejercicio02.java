package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	static final String FIN = "FIN DEL PROGRAMA";

	public static void main(String[] args) {

		/*
		 * Programa que muestre los n�meros del 100 al 1 utilizando la instrucci�n
		 * while. Realizar el ejercicio mostrando los n�meros cada uno en una l�nea. Y
		 * despu�s mostr�ndolos en la misma l�nea separados por un espacio. Mostrar un
		 * mensaje que indique el final del programa.
		 */

		Scanner program = new Scanner(System.in);

		int num;
		int counter;
		num = 100;
		counter = num;

		while (num >= 1) {
			System.out.println(num);
			num--;
		}

		num = counter;

		while (num >= 1) {
			System.out.print(num + " ");
			num--;
		}

		program.close();

		System.out.println("\n" + FIN);

	}

}
