package ejercicio30;

import java.util.Scanner;

public class Ejercicio30 {

	public static void main(String[] args) {

		/*
		 * Programa que me pide un n�mero entero positivo y me muestra por consola todos
		 * los divisores de ese n�mero desde el numero 1 hasta el numero introducido,
		 * ambos incluidos.
		 */

		Scanner program = new Scanner(System.in);

		int number;
		boolean itIsDivisor = false;
		String divisors = "";

		System.out.println("Type a number.");
		number = program.nextInt();
		if (number > 0) {
			for (int i = 2; i < number; i++) {
				if (number % i == 0) {
					itIsDivisor = true;
					divisors = divisors + i + "\n";
				}
			}
		}
		if (itIsDivisor == true) {
			System.out.println("Divisors are:\n" + divisors + "\n");
		} else {
			System.out.println("It is a prime number.");
		}
		program.close();

	}

}
