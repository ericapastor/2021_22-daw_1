package ejercicio14;

import java.util.Scanner;

public class Ejercicio14Spanish {

	static final String FIN = "\n" + "\n" + "FIN DEL PROGRAMA";

	public static void main(String[] args) {

		/*
		 * Programa para controlar el nivel de un dep�sito de agua. El programa
		 * inicialmente nos pedir� el volumen total en litros del dep�sito.
		 * Posteriormente nos pide constantemente la cantidad de litros que metemos al
		 * dep�sito o que retiramos (n�meros positivos echamos, negativos retiramos).
		 * Despu�s de cada cantidad que introducimos, nos debe mostrar la cantidad
		 * restante de litros que hace falta para llenarlo completamente. El programa
		 * deja de pedirnos cantidades cuando se ha llegado o superado el l�mite del
		 * dep�sito, indic�ndonoslo por pantalla.
		 */

		Scanner program = new Scanner(System.in);

		float maxCapacidad;
		float litrosMovimiento;
		float acumulador;

		System.out.println("Cual es la capacidad maxima de este deposito?" + "\n");
		maxCapacidad = program.nextFloat();
		acumulador = 0;

		do {
			System.out.println("\n" + "Cuantos litros insertas o retiras?" + "\n"
					+ "Numero positivo para insertar, negativo para retirar." + "\n");
			litrosMovimiento = program.nextFloat();
			acumulador = acumulador + litrosMovimiento;
			if (acumulador >= 0) {
				System.out.println("\n" + "El nuevo balance de la capacidad maxima de " + maxCapacidad + " l es: "
						+ acumulador + " l." + "\n");
			} else {
				System.out.println("\n" + "No puedes retirar dejando el deposito por debajo de cero." + "\n");
				acumulador = acumulador - litrosMovimiento;
				System.out.println("El balance sigue siendo " + acumulador + "l." + "\n");
			}
			if (acumulador >= maxCapacidad) {
				System.out.println("\n" + "Has llegado a la capacidad maxima. El programa terminara." + "\n");
			} else {
				System.out.println("Sigues teniendo espacio para " + (maxCapacidad - acumulador) + " litros.");
			}
		} while (acumulador < maxCapacidad);

		program.close();

		System.out.println(FIN);

	}

}
