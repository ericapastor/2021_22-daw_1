package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	static final String END = "\n" + "\n" + "END OF PROGRAM";

	public static void main(String[] args) {

		/*
		 * Programa para controlar el nivel de un dep�sito de agua. El programa
		 * inicialmente nos pedir� el volumen total en litros del dep�sito.
		 * Posteriormente nos pide constantemente la cantidad de litros que metemos al
		 * dep�sito o que retiramos (n�meros positivos echamos, negativos retiramos).
		 * Despu�s de cada cantidad que introducimos, nos debe mostrar la cantidad
		 * restante de litros que hace falta para llenarlo completamente. El programa
		 * deja de pedirnos cantidades cuando se ha llegado o superado el l�mite del
		 * dep�sito, indic�ndonoslo por pantalla.
		 */

		Scanner program = new Scanner(System.in);

		float litersMax;
		float litersIn;
		float acumulator;

		System.out.println("What is the total capacity in litters?" + "\n");
		litersMax = program.nextFloat();
		acumulator = 0;

		do {
			System.out.println("\n" + "How many liters do you insert/withdraw?" + "\n");
			litersIn = program.nextFloat();
			acumulator = acumulator + litersIn;
			if (acumulator >= 0) {
				System.out.println("\n" + "The new balance for the maximum capacity of " + litersMax + "l is: "
						+ acumulator + " liters" + "\n");
			} else {
				System.out.println("\n" + "You can't withdraw under 0 value." + "\n");
				acumulator = acumulator - litersIn;
				System.out.println("The balance is still " + acumulator + "\n");
			}
			if (acumulator >= litersMax) {
				System.out.println("\n" + "You have reached full capacity. Program will end." + "\n");
			} else {
				System.out.println("You still have " + (litersMax - acumulator) + " liters left.");
			}
		} while (acumulator < litersMax);

		program.close();

		System.out.println(END);

	}

}
