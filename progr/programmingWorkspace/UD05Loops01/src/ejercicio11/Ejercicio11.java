package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {

		/*
		 * Programa que pide al usuario una contrase�a formada por una cadena con
		 * cualquier car�cter, y muestra los caracteres de la cadena de texto separados
		 * por un espacio
		 */

		Scanner program = new Scanner(System.in);

		String password;
		System.out.println("Type the password.");
		password = program.nextLine();
		for (int i = 0; i < password.length(); i++) {
			System.out.print(password.charAt(i) + " ");
		}

		program.close();

	}

}
