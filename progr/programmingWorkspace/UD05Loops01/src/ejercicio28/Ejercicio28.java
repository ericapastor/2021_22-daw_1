package ejercicio28;

import java.util.Scanner;

public class Ejercicio28 {

	public static void main(String[] args) {

		/*
		 * Programa que pida n�mero enteros, hasta introducir el n�mero 0. En ese
		 * momento indicar cu�l es el mayor, el menor y la cantidad de n�meros le�da,
		 * sin contar el 0.
		 */

		Scanner program = new Scanner(System.in);

		int number;
		int bigger = 0;
		int smaller = 999999999;
		int counter = 0;

		do {
			System.out.println("Type a number.");
			number = program.nextInt();
			if (number != 0) {
				counter++;
				if (number > bigger) {
					bigger = number;
				}
				if (number < smaller) {
					smaller = number;
				}
			}

		} while (number != 0);

		System.out.println("You have input a total of " + counter + " numbers.\nThe bigger is " + bigger
				+ ".\nThe smaller is " + smaller + ".");

		program.close();

	}

}
