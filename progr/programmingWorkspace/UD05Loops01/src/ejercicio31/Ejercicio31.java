package ejercicio31;

import java.util.Scanner;

public class Ejercicio31 {

	static final String END = "\nEND OF THE PROGRAM";

	public static void main(String[] args) {

		/*
		 * Comprobar si un numero introducido por teclado es perfecto. Un n�mero es
		 * perfecto si es igual a la suma de todos sus divisores positivos sin incluir
		 * el propio n�mero. (El 6 es perfecto tiene como divisores: 1, 2, 3 y 6 pero el
		 * 6 no se cuenta como divisor para comprobar si es perfecto: 1+2+3=6)
		 */

		Scanner program = new Scanner(System.in);

		int number;
		int sumDivisors = 0;

		System.out.println("Type a number to check if it's perfect.");
		number = program.nextInt();

		if (number > 0) {
			for (int i = 1; i < number; i++) {
				if (number % i == 0) {
					sumDivisors += i;
				}
			}
		} else {
			System.out.println("Thank you for using effyCalculator. Goodbye.");
		}
		if (sumDivisors == number) {
			System.out.println(number + " is perfect.\n");
		} else {
			System.out.println("Conditions not met. " + number + " is not perfect.\n");
		}

		program.close();

		System.out.println(END);

	}

}
