package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	static final String FIN = "FIN DEL PROGRAMA";

	public static void main(String[] args) {

		/*
		 * Programa que muestre los n�meros del 1 a un n�mero mayor introducido por
		 * teclado utilizando la instrucci�n for. Realizar el ejercicio mostrando los
		 * n�meros cada uno en una l�nea. Y despu�s mostr�ndolos en la misma l�nea
		 * separados por un espacio. Mostrar un mensaje que indique el final del
		 * programa.
		 */

		Scanner program = new Scanner(System.in);
		int num;
		System.out.println("Type a whole number." + "\n");
		num = program.nextInt();
		System.out.println();

		if (num <= 0) {

			System.out.println("Number can't be zero or negative.");

		} else {

			System.out.println("On a single line.");
			for (int i = 1; i < num + 1; i++) {
				System.out.print(i + " ");
			}

			System.out.println("\n");

			System.out.println("On different lines.");
			for (int i = 1; i < num + 1; i++) {
				System.out.println(i + " ");
			}
		}

		program.close();

		System.out.println("\n" + FIN);

	}

}
