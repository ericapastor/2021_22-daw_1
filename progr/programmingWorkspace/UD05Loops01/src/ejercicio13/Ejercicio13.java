package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {

		/*
		 * Programa que permite calcular la potencia de un n�mero. Pedir� por teclado la
		 * base y el exponente y mostrar� el resultado de la potencia. El exponente
		 * puede ser mayor o igual que 0. Sin usar la clase Math.
		 */

		Scanner program = new Scanner(System.in);

		int base;
		int exponent;
		int solution;

		do {
			System.out.println("Type the base." + "\n");
			base = program.nextInt();

			System.out.println("\n" + "Type the exponent" + "\n");
			exponent = program.nextInt();

			solution = 1;

			for (int i = 0; i < exponent; i++) {
				solution = solution * base;
			}

			System.out.println("\n" + "Solution is: " + solution + "." + "\n");
		} while (base > 0 && exponent >= 0);

		program.close();

	}

}
