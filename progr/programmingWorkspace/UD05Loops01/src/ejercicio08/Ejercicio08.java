package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {

		/* Programa que muestra el abecedario en castellano en may�sculas. */

		Scanner program = new Scanner(System.in);

		for (int i = 'A'; i <= 'Z'; i++) {
			System.out.println((char) i);
		}

		program.close();

	}

}
