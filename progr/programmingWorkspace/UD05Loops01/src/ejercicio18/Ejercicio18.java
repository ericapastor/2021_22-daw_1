package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {

		/*
		 * Pedir un n�mero entero que representa el tama�o del lado. Dibujar y rellenar
		 * un cuadrado con el car�cter �X�, con tantas filas y columnas como se ha
		 * indicado por teclado.
		 */

		Scanner program = new Scanner(System.in);

		int number;

		System.out.println("Size of the square:\n");
		number = program.nextInt();
		System.out.println("\n");

		for (int i = 0; i < number; i++) {
			for (int j = 0; j < number; j++) {
				System.out.print("x ");
			}
			System.out.println();
		}

		program.close();

	}

}
