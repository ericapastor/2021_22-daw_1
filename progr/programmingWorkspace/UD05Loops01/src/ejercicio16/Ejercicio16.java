package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	static final String END = "\nEND OF PROGRAM";

	public static void main(String[] args) {

		/*
		 * Pedir por teclado un número entero ‘cantidadSueldos’. Posteriormente pedir
		 * tantos sueldos como indica la variable ‘cantidadSueldos’. Finalmente mostrar
		 * el sueldo más alto. Utilizar el tipo de datos adecuado.
		 */

		Scanner program = new Scanner(System.in);

		int totalIncome;
		float income;
		int counter = 0;
		float acumulator = 0F;

		do {
			System.out.println("How many salaries do you want me to sum?\nType 0 to end the program.\n");
			totalIncome = program.nextInt();
			for (int i = 0; i < totalIncome; i++) {
				counter++;
				System.out.println("\nType income number " + counter + ":\n");
				income = program.nextFloat();
				if (income >= 0) {
					acumulator = acumulator + income;
				} else {
					System.out.println("\nNegatives numbers not allowed.\nStart again.\n");
					break;
				}
			}
			if (counter == totalIncome && counter != 0) {
				System.out.println("\nThe sum of salaries is: " + acumulator + ".\n");
			}
		} while (totalIncome != 0);

		program.close();

		System.out.println(END);

	}

}
