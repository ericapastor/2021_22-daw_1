package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {

		/*
		 * Mostrar el n�mero de cifras de un n�mero introducido por teclado. Los datos
		 * deben ser tratados como n�meros enteros, no como String (Se debe resolver sin
		 * los m�todos de la clase String).
		 */

		Scanner program = new Scanner(System.in);

		int num;
		int counter = 0;

		System.out.println("Type a number.");
		num = program.nextInt();

		while (num != 0) {
			num = num / 10;
			counter++;
		}

		System.out.println("Number of digits is " + counter);

		program.close();

	}

}
