package ejercicio20;

import java.util.Scanner;

public class Ejercicio20 {

	public static void main(String[] args) {

		/*
		 * Programa que me cuenta el n�mero de veces que aparece un car�cter en una
		 * cadena de texto. El car�cter se pedir� por teclado, y posteriormente se
		 * pedir� una cadena de texto.
		 */

		Scanner program = new Scanner(System.in);

		char charac;
		String chain;
		int counter = 0;

		System.out.println("Type the character you wish to count.\n");
		charac = program.nextLine().charAt(0);

		System.out.println("\nType the sentence from which you wish to count the previous character.\n");
		chain = program.nextLine();
		System.out.println();

		for (int i = 0; i < chain.length(); i++) {
			if (chain.charAt(i) == charac) {
				counter++;
			}
		}

		System.out.println("\nThe character appears " + counter + " times.");

		program.close();

	}

}
