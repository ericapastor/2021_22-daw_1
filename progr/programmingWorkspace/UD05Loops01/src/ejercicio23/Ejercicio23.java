package ejercicio23;

import java.util.Scanner;

public class Ejercicio23 {

	public static void main(String[] args) {

		/*
		 * Programa para corregir ex�menes. Se introducen las notas del alumno de cada
		 * examen (double) hasta que se introduzca un n�mero negativo, y calcular la
		 * nota media
		 */

		Scanner program = new Scanner(System.in);

		double grade;
		int counter = 0;
		double acumulator = 0;

		do {
			System.out.println("Type the grade");
			grade = program.nextDouble();
			if (grade < 0) {
				break;
			} else {
				counter++;
				acumulator = acumulator + grade;
			}
		} while (grade >= 0);

		if (counter != 0) {
			System.out.println("Media grade for the total " + acumulator + " and a total of " + counter + " grades is: "
					+ (acumulator / counter) + ".");
		}
		program.close();

	}

}
