package ejercicio21;

import java.util.Scanner;

public class Ejercicio21 {

	public static void main(String[] args) {

		/*
		 * Programa que pide un n�mero binario de 8 cifras y muestra su conversi�n a
		 * sistema decimal. Debemos crear un algoritmo que haga eso, sin usar clases
		 * nuevas. Solo tenemos que recordar las operaciones que debo hacer para pasar
		 * de binario a decimal, y encontrar el patr�n que se repite. Despu�s
		 * representar esa repetici�n mediante un bucle.
		 */

		Scanner program = new Scanner(System.in);

		String binaryString;
		int binary;
		int bit;
		int decimal = 0;

		System.out.println("Type an eight digit binary number.");
		binaryString = program.nextLine();

		if (binaryString.length() == 8) {
			binary = Integer.parseInt(binaryString);
			for (int i = 0; i < 8; i++) {
				bit = binary % 10;
				binary = binary / 10;
				bit = bit * (int) Math.pow(2, i);
				decimal = decimal + bit;
			}

			// asi lo haria sin bucle
//			decimal = ((binary / 10000000) * (int) Math.pow(2.0, 7.0))
//					+ (((binary % 10000000) / 1000000) * (int) Math.pow(2.0, 6.0))
//					+ (((binary % 1000000) / 100000) * (int) Math.pow(2.0, 5.0))
//					+ (((binary % 100000) / 10000) * (int) Math.pow(2.0, 4.0)) + (((binary % 10000) / 1000) * (int) Math.pow(2.0, 3.0))
//					+ (((binary % 1000) / 100) * (int) Math.pow(2.0, 2.0)) + (((binary % 100) / 10) * (int) Math.pow(2.0, 1.0)) + ((binary % 10) * (int) Math.pow(2.0, 0.0));

			System.out.println("Number is " + decimal);

		} else {
			System.out.println("Number needs to be eight digited for conversion.");
		}

		program.close();

	}

}
