package ejercicio37;

public class Ejercicio37 {

	public static void main(String[] args) {

		/* Mostrar los n�meros perfectos entre 1 y 1000. */

		int number = 0;
		String perfect = "";

		for (int i = 1; i <= 1000; i++) {
			for (int j = 1; j < i; j++) {
				// multiples must sum the number
				if (i / j == 0) {
					number = number + j;
				}
			}
			if (number == i) {
				perfect = perfect + i + " ";
			}
			number = 0;
		}
		
		System.out.println("Perfect numbers between 1 and 1000 are\n"+perfect);

	}

}
