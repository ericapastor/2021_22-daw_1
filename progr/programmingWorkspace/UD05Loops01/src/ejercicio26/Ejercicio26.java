package ejercicio26;

import java.util.Scanner;

public class Ejercicio26 {

	public static void main(String[] args) {

		/*
		 * Programa que pide un numero entero positivo desde el 0 a 255 y muestra su
		 * conversi�n a n�mero binario. Debemos crear nuestro propio algoritmo, sin usar
		 * clases nuevas. Podemos usar un String para almacenar los bits que voy
		 * obteniendo de las sucesivas divisiones.
		 */

		Scanner program = new Scanner(System.in);

		int decimal;
		int bit;
		String binary = "";

		System.out.println("Type a decimal number.");
		decimal = program.nextInt();

		while (decimal > 0) {
			bit = decimal % 2;
			decimal /= 2;
			binary = bit + binary;
		}

		System.out.println("Binary number would be " + binary);

		program.close();

	}

}
