package ejercicio29;

import java.util.Scanner;

public class Ejercicio29 {

	public static void main(String[] args) {

		/*
		 * Programa que pida cadenas de texto hasta introducir la cadena de texto �fin�.
		 * Posteriormente me dir� la cantidad de vocales (may�sculas o min�sculas) y el
		 * porcentaje de vocales respecto al total de caracteres. Adem�s me mostrar� la
		 * cadena resultante de unir todas las cadenas separadas por un espacio.
		 */

		Scanner program = new Scanner(System.in);

		String chain;
		String wholeChain = "";
		char leter;
		int counter = 0;
		int counterVowel = 0;
		int counterCharacter = 0;

		do {
			System.out.println("Introduce a sentence. Type 'end' to finish.");
			chain = program.nextLine().toLowerCase();
			if (!chain.equals("end")) {
				wholeChain = wholeChain + chain + " ";
				chain = chain.replaceAll(" ", ""); // para que no cuente los espacios
				counterCharacter = counterCharacter + chain.length();
				counter++;
				for (int i = 0; i < chain.length(); i++) {
					leter = chain.charAt(i);
					if (leter == 'a' || leter == 'e' || leter == 'i' || leter == 'o' || leter == 'u') {
						counterVowel++;
					}
				}
			}
		} while (!chain.equals("end"));

		System.out.println("\nThe number of sentences was " + counter + ".\n");

		if (counter != 0) {
			System.out.println("Whole chain of sentences is " + wholeChain + ".\n");
			System.out.println(
					"The ratio of vowels was " + ((double) counterVowel / wholeChain.length() * 100) + " %.\n");
			System.out.println("The total number of characters was " + counterCharacter + ".\n");
		}

		program.close();

	}

}
