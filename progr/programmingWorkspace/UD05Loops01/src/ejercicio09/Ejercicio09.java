package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {

		/*
		 * Generar un n�mero entero aleatorio entre 0 y 100 ayud�ndome del m�todo
		 * Math.random(). Posteriormente muestro por pantalla todos los n�meros desde el
		 * n�mero aleatorio hasta el -100, de 7 en 7.
		 */

		Scanner program = new Scanner(System.in);

		double num;

		num = Math.random() * 101;

		for (int i = (int) num; i >= -100; i -= 7) {
			System.out.println(i);
		}

		program.close();

	}

}
