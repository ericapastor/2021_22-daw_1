package ejercicio10;

import java.util.Scanner;

public class Ejercicio10_01 {

	public static void main(String[] args) {

		/* Pedir 10 n�meros reales por teclado y despu�s escribir la suma total. */

		Scanner program = new Scanner(System.in);

		int num;
		int acum = 0;
		int count = 0;

		do {
			System.out.println("Type a number");
			num = program.nextInt();
			acum = acum + num;
			count++;
		} while (count < 10);

		System.out.println("The sum of the 10 numbers is: " + acum);

		program.close();

	}

}
