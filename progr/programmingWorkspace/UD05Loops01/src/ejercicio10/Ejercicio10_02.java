package ejercicio10;

import java.util.Scanner;

public class Ejercicio10_02 {

	public static void main(String[] args) {

		/* Pedir 10 n�meros reales por teclado y despu�s escribir la suma total. */

		Scanner program = new Scanner(System.in);

		double num;
		double acum = 0; // un numero real puede ser decimal, no es lo mismo que un numero entero

		for (int i = 0; i < 10; i++) {
			System.out.println("Type a number.");
			num = program.nextDouble();
			if (num > 0) {
				acum = acum + num;
			} else {
				System.out.println("Can't be zero or negative.");
				break;
			}
		}

		System.out.println("The sum of the numbers is: " + acum);

		program.close();

	}

}
