package ejercicio19;

import java.util.Scanner;

public class Ejercicio19 {

	public static void main(String[] args) {

		/* Pedir 5 n�meros e indicar al final si alguno es m�ltiplo de 3. */

		Scanner program = new Scanner(System.in);

		int number;
		boolean x3 = false;
		String acumulator = "";

		for (int i = 0; i < 5; i++) {
			System.out.println("Type a number.");
			number = program.nextInt();
			if (number % 3 == 0) {
				x3 = true;
				acumulator = acumulator + number + " ";
			}
		}

		if (x3 == true) {
			System.out.println("\nThere was/were multiple(s) of 3.\nNumber(s) was/were: " + acumulator);
		}

		program.close();

	}

}
