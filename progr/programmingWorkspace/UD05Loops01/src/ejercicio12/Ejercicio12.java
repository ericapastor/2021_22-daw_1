package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {

		/*
		 * Programa que muestra un men� con 3 opciones. 1 � Opci�n 1, 2 � Opci�n 2, 3 �
		 * Salir. Mientras que se introduzcan las opciones 1 y 2 debe mostrar un mensaje
		 * indicando la opci�n seleccionada, y acto seguido vuelve a mostrar el men�. Si
		 * se selecciona salir, mostrar� un mensaje de despedida y terminar� el
		 * programa. Usar switch para obtener la selecci�n.
		 */

		Scanner program = new Scanner(System.in);

		int num;

		do {
			System.out.println("Type a number to pick the option:");
			System.out.println("1.-Option 1.");
			System.out.println("2.-Option 2.");
			System.out.println("3.-Finish program.");
			System.out.println();
			num = program.nextInt();
			switch (num) {
			case 1:
				System.out.println("Option 1 was selected." + "\n");
				break;
			case 2:
				System.out.println("Option 2 was selected." + "\n");
				break;
			case 3:
				System.out.println("End of program.");
				break;
			default:
				System.out.println("Option not recognized." + "\n");
				break;
			}
		} while (num != 3);

		program.close();

	}

}
