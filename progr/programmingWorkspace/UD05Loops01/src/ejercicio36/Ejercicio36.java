package ejercicio36;

import java.util.Scanner;

public class Ejercicio36 {

	public static void main(String[] args) {

		/*
		 * Programa que me cuenta el n�mero de palabras que tiene una cadena de texto.
		 * Entendemos que las palabras est�n separadas por un �nico espacio.
		 */

		Scanner program = new Scanner(System.in);

		String sentence;
		int counter = 0;

		System.out.println("Type the String.");
		sentence = program.nextLine();

		for (int i = 0; i < sentence.length(); i++) {
			if (sentence.charAt(i) == ' ') {
				counter++;
			}
		}

		if (counter > 0) {
			System.out.println("The String has " + (counter + 1) + " words.");
		} else {
			System.out.println("The String has only one word.");
		}
		program.close();

	}

}
