package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {

		/*
		 * Programa (bucle for) que recibe dos n�meros enteros (un inicio y un final) y
		 * despu�s un tercer n�mero que indica el salto. El programa debe mostrar por
		 * pantalla todos los n�meros desde el inicio hasta el final sumando o restando
		 * el salto (Debe comprobar si el inicio es mayor o menor al final). Ejemplo1:
		 * inicio 1, final 11, salto 2 (1,3,5,7,9,11). Ejemplo2: inicio 25, final 5,
		 * salto 6 (25, 19, 13, 7, 1)
		 */

		Scanner program = new Scanner(System.in);

		int num1;
		int num2;
		int jumpy;

		System.out.println("Type first number.");
		num1 = program.nextInt();

		System.out.println("Type second number.");
		num2 = program.nextInt();
		System.out.println("Type loop number.");
		jumpy = program.nextInt();
		if (jumpy >= 1) {
			if (num1 > num2) {

				for (int i = num1; i >= num2; i -= jumpy) {
					System.out.println(i);
				}

			} else if (num1 < num2) {

				for (int i = num1; i <= num2; i += jumpy) {
					System.out.println(i);
				}

			} else {
				System.out.println("Number one and number two can't be the same.");
			}
		} else {
			System.out.println("Loop can't be 0 or negative.");
		}
		program.close();

	}

}
