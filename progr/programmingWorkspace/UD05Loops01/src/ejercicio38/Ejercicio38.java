package ejercicio38;

import java.util.Scanner;

public class Ejercicio38 {

	public static void main(String[] args) {

		/*
		 * Realizar un juego para adivinar un n�mero. Para ello se genera un n�mero
		 * aleatorio de 1 al 100 y luego se va pidiendo n�meros indicando �mayor� o
		 * �menor� seg�n sea mayor o menor con respecto al n�mero aleatorio generado. El
		 * proceso termina cuando el usuario acierta
		 */

		// Math.random() -> puedo generar un numero aleatorio
		// *100 estar entre 0 y 100
		// si lo casteo sera entero (int)
		// Math.round -> redondeo
		// obtener un numero aleatorio entre 1 y 100
		Scanner input = new Scanner(System.in);

		int aleatorio = (int) (Math.round(Math.random() * 100));
		int numero;

		do {
			System.out.println("Adivina el numero");
			numero = input.nextInt();
			if (numero > aleatorio) {
				System.out.println("El numero que has introducido es mayor");
			} else if (numero < aleatorio) {
				System.out.println("El numero que has introducido es menor");
			} else {
				System.out.println("Has acertado");
			}
		} while (numero != aleatorio);

		input.close();

	}

}
