package ejercicio24;

import java.util.Scanner;

public class Ejercicio24 {

	public static void main(String[] args) {

		/*
		 * Programa que pida el largo y el ancho (valores enteros) y dibuje un
		 * rect�ngulo, con tantas �X� por alto y por ancho como se han introducido por
		 * teclado.
		 */

		Scanner program = new Scanner(System.in);

		int width;
		int height;

		System.out.println("Type the width.");
		width = program.nextInt();
		System.out.println("Type the height.");
		height = program.nextInt();

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				System.out.print("x ");
			}
			System.out.println();
		}

		program.close();

	}

}
