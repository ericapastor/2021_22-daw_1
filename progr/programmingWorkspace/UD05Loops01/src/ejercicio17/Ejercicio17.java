package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {

		/* Pedir 10 n�meros, y decir al final si se ha introducido alguno negativo */

		Scanner program = new Scanner(System.in);

		int num;
		boolean negative = false;

		for (int i = 0; i < 10; i++) {
			System.out.println("Type a number.");
			num = program.nextInt();
			if (num < 0) {
				negative = true;
			}
		}

		if (negative == true) {
			System.out.println("Some negative number(s) has/have been introduced.");
		} else {
			System.out.println("No negative numbers.");
		}

		program.close();

	}

}
