package ejercicio32;

import java.util.Scanner;

public class Ejercicio32_02 {

	public static void main(String[] args) {

		/*
		 * Crea una aplicaci�n que dibuje una escalera de n�meros, siendo cada l�nea
		 * n�meros empezando en uno y acabando en el n�mero de la l�nea. Este es un
		 * ejemplo, si introducimos un 5 como altura:
		 */

		// 1
		// 12
		// 123
		// 1234
		// 12345

		Scanner program = new Scanner(System.in);

		int height;

		System.out.println("Type a number for the height of the stair.");
		height = program.nextInt();

		for (int i = 1; i <= height; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("x ");
			}
			System.out.println();
		}

		program.close();

	}

}
