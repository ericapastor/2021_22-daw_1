package ejercicio33;

import java.util.Scanner;

public class Ejercicio33 {

	public static void main(String[] args) {

		/*
		 * Necesitamos mostrar un contador con 5 d�gitos (X-X-X-X-X), que muestre los
		 * n�meros del 0-0-0-0-0 al 9-9-9-9-9. Despu�s a�adirle una modificaci�n para
		 * que en lugar de mostrar el n�mero 3 lo cambie por la letra �E�.
		 */

		Scanner program = new Scanner(System.in);

		for (int i = 0; i <= 9; i++) {
			for (int j = 0; j <= 9; j++) {
				for (int k = 0; k <= 9; k++) {
					System.out.println(i + "-" + j + "-" + k);
				}
			}
		}

		program.close();

	}

}
