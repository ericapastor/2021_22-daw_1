package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {

		/*
		 * Mostrar la tabla de multiplicar de un numero introducido por teclado del 1 al
		 * 10
		 */

		Scanner program = new Scanner(System.in);

		int num;
		int counter = 0;

		System.out.println("Type a number between one and ten.");
		num = program.nextInt();

		if (num >= 1 && num <= 10) {
			for (int i = 0; i < 10; i++) {
				counter++;
				System.out.println(num + "*" + counter + "=" + (num * counter));
			}
		}

		program.close();

	}

}
