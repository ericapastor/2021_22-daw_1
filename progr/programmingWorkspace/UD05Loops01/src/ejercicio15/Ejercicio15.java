package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {

		/*
		 * Programa que pide n�meros enteros positivos y negativos por teclado hasta que
		 * introducimos el n�mero 0. Despu�s deber� indicar el n�mero total de n�meros
		 * le�dos y la suma total de sus valores.
		 */

		Scanner program = new Scanner(System.in);

		int num;
		int counter = 0;
		int acumulator = 0;

		do {
			System.out.println("Type a positive or a negative number.\n");
			System.out.println("Type 0 to end the program.\n");
			num = program.nextInt();
			if (num != 0) {
				counter++;
				acumulator = acumulator + num;
			}
			System.out.println();

		} while (num != 0);

		System.out.println("\nTotal of numbers: " + counter + ". \n");
		System.out.println("Sum of numbers: " + acumulator + ".");

		program.close();

	}

}
