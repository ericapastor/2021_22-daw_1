package ejercicio25;

import java.util.Scanner;

public class Ejercicio25 {

	final static int KELVIN = 273;
	final static String END = "\nEND OF PROGRAM";

	public static void main(String[] args) {

		/*
		 * Pasar de grados cent�grados a grados kelvin. El proceso de leer grados
		 * cent�grados y convertirlos se debe repetir mientras que se responda �S� a la
		 * pregunta: �Repetir proceso? (S/N). (Conversi�n: �K = �C + 273)
		 */

		Scanner program = new Scanner(System.in);

		char process;
		char check;
		int centigrade;
		int kelvin;

		do {
			System.out.println("Type the centigrades");
			centigrade = program.nextInt();
			kelvin = centigrade + KELVIN;
			System.out.println("The conversion is " + kelvin + " kelvin grades.");
			System.out.println("Repeat the process?\nY/N\nY for yes\nN for no");
			program.nextLine();
			process = program.nextLine().charAt(0);
			if (process == 'n' || process == 'N') {
				System.out.println("Would u wish to get out of the process?");
				check = program.nextLine().charAt(0);
				if (check == 'n' || check == 'N') {
					process = 'Y';
				} else {
					System.out.println("Thanks for using effyConverter. Goodbye.");
				}
			}
		} while (process == 'Y' || process == 'y');


		program.close();

		System.out.println(END);

	}

}
