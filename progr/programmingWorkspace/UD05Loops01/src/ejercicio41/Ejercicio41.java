package ejercicio41;

import java.util.Scanner;

public class Ejercicio41 {

	public static void main(String[] args) {

		/*
		 * Crea una aplicaci�n que dibuje una pir�mide de asteriscos. Nosotros le
		 * pasamos la altura de la pir�mide por teclado. Este es un ejemplo, si
		 * introducimos 5 de altura: No se trata de centrarse en pintar los asteriscos
		 * sino tambi�n espacios en blanco.
		 */

		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una altura");
		int altura = input.nextInt();

		// La i es la altura
		for (int i = 1; i <= altura; i++) {

			// Calcular el numero de espacios, en funcion de la altura
			int numEspacios = altura - i;
			for (int j = numEspacios; j > 0; j--) {
				System.out.print(" ");
			}

			// calcular el numero de asteriscos, en funcion de la altura
			int numAsteriscos = i * 2 - 1;
			for (int k = 0; k < numAsteriscos; k++) {
				System.out.print("*");
			}

			System.out.println();
		}

		input.close();

	}

}
