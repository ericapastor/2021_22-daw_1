package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	static final String FIN = "\n" + "FIN DEL PROGRAMA";

	public static void main(String[] args) {

		/*
		 * Programa que muestre los n�meros, descendiendo de uno en uno desde un n�mero
		 * introducido por teclado mayor al 1 hasta el n�mero 1 utilizando la
		 * instrucci�n do-while. Realizar el ejercicio mostrando los n�meros cada uno en
		 * una l�nea. Y despu�s mostr�ndolos en la misma l�nea separados por un espacio.
		 * Mostrar un mensaje que indique el final del programa.
		 */

		Scanner program = new Scanner(System.in);

		int num;
		int counter;

		System.out.println("Type a number bigger than 1." + "\n");
		num = program.nextInt();
		counter = num;
		if (num > 1) {
			System.out.println();
			do {
				System.out.println(num);
				num--;
			} while (num >= 1);
			System.out.println();
			num = counter;
			do {
				System.out.print(num + " ");
				num--;
			} while (num >= 1);
		} else {
			System.out.println("\n" + "Number must be bigger than 1.");
		}

		program.close();

	}

}
