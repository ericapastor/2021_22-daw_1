package ejercicio27;

import java.util.Scanner;

public class Ejercicio27_02 {

	final static String END = "\nEND OF PROGRAM";

	public static void main(String[] args) {

		/*
		 * Programa que pide al usuario una cadena de texto, la pasa a min�sculas, y
		 * muestra los caracteres de la cadena por separado. (Ejercicio 11) Adem�s
		 * quiero que me diga el porcentaje que hay de cada una de las vocales, respecto
		 * al total de caracteres de la cadena.
		 */

		Scanner program = new Scanner(System.in);

		System.out.println("Type a sentence");

		String chain = program.nextLine().toLowerCase();

		char charac;

		int counterA = 0;
		int counterE = 0;
		int counterI = 0;
		int counterO = 0;
		int counterU = 0;

		for (int i = 0; i < chain.length(); i++) {
			charac = chain.charAt(i);
			if (charac == 'a') {
				counterA++;
			}
			if (charac == 'e') {
				counterE++;
			}
			if (charac == 'i') {
				counterI++;
			}
			if (charac == 'o') {
				counterO++;
			}
			if (charac == 'u') {
				counterU++;
			}
		}

		System.out.println(chain);

		System.out.println("The percentage of A is " + ((double) counterA / chain.length() * 100) + " %");
		System.out.println("The percentage of E is " + ((double) counterE / chain.length() * 100) + " %");
		System.out.println("The percentage of I is " + ((double) counterI / chain.length() * 100) + " %");
		System.out.println("The percentage of O is " + ((double) counterO / chain.length() * 100) + " %");
		System.out.println("The percentage of U is " + ((double) counterU / chain.length() * 100) + " %");

		program.close();

		System.out.println(END);

	}

}
