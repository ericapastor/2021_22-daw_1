package ejercicio27;

import java.util.Scanner;

public class Ejercicio27_03 {

	final static String END = "\nEND OF PROGRAM";

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// pido una cadena y la paso a minusculas
		System.out.println("Introduce una cadena");
		String cadena = input.nextLine().toLowerCase();
		char caracter;

		int contadorA = 0;
		int contadorE = 0;
		int contadorI = 0;
		int contadorO = 0;
		int contadorU = 0;

		for (int i = 0; i < cadena.length(); i++) {
			caracter = cadena.charAt(i);
			if (caracter == 'a') {
				contadorA++;
			} else if (caracter == 'e') {
				contadorE++;
			} else if (caracter == 'i') {
				contadorI++;
			} else if (caracter == 'o') {
				contadorO++;
			} else if (caracter == 'u') {
				contadorU++;
			}
		}

		System.out.println("Ratio de A " + ((double) contadorA / cadena.length() * 100));
		System.out.println("Ratio de E " + ((double) contadorE / cadena.length() * 100));
		System.out.println("Ratio de I " + ((double) contadorI / cadena.length() * 100));
		System.out.println("Ratio de O " + ((double) contadorO / cadena.length() * 100));
		System.out.println("Ratio de U " + ((double) contadorU / cadena.length() * 100));

		input.close();

	}

}
