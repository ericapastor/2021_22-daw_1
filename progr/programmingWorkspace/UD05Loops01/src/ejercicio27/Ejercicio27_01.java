package ejercicio27;

import java.util.Scanner;

import ejercicio25.Ejercicio25;

public class Ejercicio27_01 {

	public static void main(String[] args) {

		/*
		 * Programa que pide al usuario una cadena de texto, la pasa a min�sculas, y
		 * muestra los caracteres de la cadena por separado. (Ejercicio 11) Adem�s
		 * quiero que me diga el porcentaje que hay de cada una de las vocales, respecto
		 * al total de caracteres de la cadena.
		 */

		Scanner program = new Scanner(System.in);

		String chain;
		int charac;
		int counter = 0;
		String sentence = "";

		System.out.println("Type a sentence");
		chain = program.nextLine();
		for (int i = 0; i < chain.length(); i++) {
			charac = chain.charAt(i);
			if (chain.charAt(i) >= 65 && chain.charAt(i) <= 90) {
				charac = charac + 32;
			}
			if (charac == 97 || charac == 101 || charac == 105 || charac == 111 || charac == 117) {
				counter++;
			}
			sentence = sentence + (char) charac + " ";
		}

		System.out.println(
				sentence + "\nThe percentage of vowels is " + ((double) counter / chain.length() * 100) + "%");

		System.out.println("End of program.");

		program.close();

	}

}
