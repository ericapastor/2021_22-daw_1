package ejercicio22;

import java.util.Scanner;

public class Ejercicio22 {

	public static void main(String[] args) {

		/*
		 * Mismo ejercicio anterior pero que permita convertir un n�mero binario de
		 * cualquier cantidad de cifras. Usar tipo long.
		 */

		Scanner program = new Scanner(System.in);

		String binary;
		int exponent = 0;
		long resultado = 0;

		System.out.println("Type a binary number.");
		binary = program.nextLine();

		for (int i = binary.length() - 1; i >= 0; i--) {
			if (binary.charAt(i) == '1') {
				resultado = resultado + (long) Math.pow(2, exponent);
			}
			exponent++;
		}

		System.out.println("Conversion is " + resultado + ".");

		program.close();

	}

}
