package ejercicio34;

import java.util.Scanner;

public class Ejercicio34 {

	public static void main(String[] args) {

		/*
		 * Comprobar si un n�mero introducido por teclado es primo. Un n�mero primo es
		 * aquel que solo es divisible entre s� mismo y entre 1. Un n�mero es divisible
		 * entre otro si el resto es 0
		 */

		Scanner program = new Scanner(System.in);

		int number;
		boolean itisPrime = true;

		System.out.println("Type a number.");
		number = program.nextInt();

		for (int i = 2; i < number; i++) {
			if (number % i == 0) {
				itisPrime = false;
				break;
			}
		}

		if (itisPrime == true) {
			System.out.println("Number is a prime.");
		} else {
			System.out.println("It's not a prime number.");
		}

		program.close();

	}

}
