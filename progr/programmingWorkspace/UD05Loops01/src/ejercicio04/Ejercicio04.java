package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	final static String FIN = "\n" + "FIN DEL PROGRAMA";

	public static void main(String[] args) {

		/*
		 * Programa que muestre los n�meros desde un n�mero mayor introducido por
		 * teclado hasta otro n�mero menor introducido por teclado, utilizando la
		 * instrucci�n for. Realizar el ejercicio mostrando los n�meros cada uno en una
		 * l�nea. Y despu�s mostr�ndolos en la misma l�nea separados por un espacio.
		 * Mostrar un mensaje que indique el final del programa
		 */

		Scanner program = new Scanner(System.in);

		int bigNum;
		int smallNum;

		System.out.println("Type a number.");
		bigNum = program.nextInt();
		System.out.println("Type a smaller number now.");
		smallNum = program.nextInt();

		if (bigNum > smallNum) {
			System.out.println();
			for (int i = bigNum; i >= smallNum; i--) {
				System.out.println(i);
			}
			System.out.println();
			for (int i = bigNum; i >= smallNum; i--) {
				System.out.print(i + " ");
			}

		} else {
			System.out.println("\n" + "First number must be bigger.");
		}

		program.close();

		System.out.println("\n" + FIN);

	}

}
