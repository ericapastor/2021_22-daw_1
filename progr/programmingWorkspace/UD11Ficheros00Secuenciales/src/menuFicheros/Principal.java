package menuFicheros;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Principal {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws IOException {

		// 1.- Pido el nombre del archivo
		System.out.println("Dame el nombre del archivo");
		String nombreA = input.nextLine();
		// 2.- Cro un archivo usando OperacionesArchivos
		Archivo miArchivo = new Archivo(nombreA);
		int opcion;
		do {
			// 3.- Llamo al menu
			opcion = menu();
			switch (opcion) {
			case 1:
				miArchivo.crearArchivo();
				break;
			case 2:
				miArchivo.visualizarArchivo();
				break;
			case 3:
				miArchivo.convertirMayuscMinus(1);
				break;
			case 4:
				miArchivo.convertirMayuscMinus(2);
				break;
			case 5:
				int cantidadesLineas = miArchivo.numeroLineas();
				break;
			case 6:
				System.out.println("Introduce la palabra a buscar");
				String palabra = input.nextLine();
				miArchivo.buscarPalabra(palabra);
				break;
			case 7:
				input.close();
				break;
			}
		} while (opcion < 7);
	}

	public static int menu() {
		int opcion = 0;
		boolean error = true;
		do {
			try {
				System.out.println("MENU");
				System.out.println("1.- Crear archivo desde teclado");
				System.out.println("2.- Visualizar archivo");
				System.out.println("3.- Convertir a mayusculas");
				System.out.println("4.- Convertir a minusculas");
				System.out.println("5.- Numero de lineas");
				System.out.println("6.- Buscar palabra en un fichero");
				System.out.println("7.- Salir");
				opcion = input.nextInt();
				input.nextLine();
				if (opcion < 1 || opcion > 7) {
					System.out.println("El valor debe estar entre 1 y 7");
				} else {
					error = false;
				}
			} catch (InputMismatchException e) {
				System.out.println("Introduce solamente datos numericos");
				input.nextLine();
			} catch (Exception e) {
				System.out.println("Error de acceso a la informacion del teclado");
				input.nextLine();
				System.exit(0);
			}
		} while (error);
		return opcion;
	}

}
