package menuFicheros;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;

public class Archivo {

	private String archivo;

	public Archivo(String archivo) {
		this.archivo = archivo;
	}

	// metodos para trabajar con archivos

	// crear el archivo desde teclado
	public void crearArchivo() throws IOException {
		System.out.println("CREAR ARCHIVO");
		// 1.- Abro lectura y entrada de teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// 2.-Abro escritura y buffer de lectura
		PrintWriter fuente = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		// 3.- Recorro linea a linea hasta el asterisco *
		String linea = in.readLine();
		while (!linea.equalsIgnoreCase("*")) {
			fuente.println(linea);
			linea = in.readLine();
		}
		System.out.println("Archivo creado");
		// 4.-Cerrar archivos
		fuente.close();

	}

	// leer informacion archivo y mostrar por pantalla
	public void visualizarArchivo() throws IOException {
		String linea;
		System.out.println("VISUALIZAR ARCHIVO " + this.archivo);
		// 1.- abro para lectura
		BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
		// 2.- recorro linea a linea hasta null
		linea = fuente.readLine();
		while (linea != null) {
			System.out.println(linea);
			linea = fuente.readLine();
		}

		// 3.- cierro el archivo
		fuente.close();
	}

	// convertir mayusculas minusculas
	public void convertirMayuscMinus(int opcionElegida) {
		String linea;
		System.out.println("CONVERTIR MAYUSCULAS/MINUSCULAS");
		if (opcionElegida == 1) {
			System.out.println("Mayusculas");
		} else {
			System.out.println("Minusculas");
		}
		// declaro ArrayList
		ArrayList<String> vectorMayMin = new ArrayList<String>();
		try {
			// 1.- conecto en modo lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));

			// 2.- guardo las lineas en el vector
			linea = fuente.readLine();
			while (linea != null) {
				vectorMayMin.add(linea);
				linea = fuente.readLine();
			}

			// 3.- cerrar el archivo
			fuente.close();

			// 4.- leo el vector y lo paso a mayusculas o minusculas
			PrintWriter fuenteMayMin = new PrintWriter(new FileWriter(this.archivo));
			for (int i = 0; i < vectorMayMin.size(); i++) {
				if (opcionElegida == 1) {
					fuenteMayMin.println(vectorMayMin.get(i).toUpperCase());
				} else {
					fuenteMayMin.println(vectorMayMin.get(i).toLowerCase());
				}
			}

			// 5.- cerrar archivo
			fuenteMayMin.close();
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

	// contar lineas
	public int numeroLineas() {
		System.out.println("CONTAR LINEAS");
		String linea;
		int contadorLineas = 0;
		try {
			// 1.- abro para lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			// 2.- recorro linea a linea hasta null y contar
			linea = fuente.readLine();
			while (linea != null) {
				contadorLineas++;
				linea = fuente.readLine();
			}
			// 3.- cierro el archivo
			fuente.close();

		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}
		return contadorLineas;
	}

	// buscarPalabra
	public void buscarPalabra(String valorABuscar) {
		System.out.println("BUSCAR PALABRA EN VECTOR");
		String linea;
		// declaramos un vector
		Vector<String> vectorBuscar = new Vector<String>();
		try {
			// 1.- abrimos para leer
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));

			// 2.- recorro linea a linea y metemos las palabras en el vector
			linea = fuente.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				// extraemos una a una las letras
				// y vamos acumulando en palabras
				// al finalizar cada vuelta de for tendremos una palabra
				for (int i = 0; i < linea.length(); i++) {
					letra = linea.substring(i, i + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
					}
				}
				vectorBuscar.add(palabra);
				palabra = "";
				linea = fuente.readLine();
			}
			System.out.println("Palabra buscada");
			System.out.println(valorABuscar);
			System.out.println("Vector a buscar");
			System.out.println(vectorBuscar);

			// 3.- buscamos palabra en el vector
			int cuentaPalabras = 0;
			for (int j = 0; j < vectorBuscar.size(); j++) {
				String valorEnVector = vectorBuscar.elementAt(j).toString();
				if (valorEnVector.equalsIgnoreCase(valorABuscar) == true) {
					cuentaPalabras++;
				}
			}
			if (cuentaPalabras == 0) {
				System.out.println("La palabra no se encuentra en el archivo");
			} else {
				System.out.println("La palabra se encuentra en el archivo " + cuentaPalabras + " veces");
			}

			// 4.- cerrar el archivo
			fuente.close();
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}
	}
}
