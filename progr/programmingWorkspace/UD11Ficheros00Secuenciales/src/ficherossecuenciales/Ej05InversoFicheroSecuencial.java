package ficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Ej05InversoFicheroSecuencial {

	public static final String ARCHIVO1 = "ej1.txt";
	public static final String ARCHIVO2 = "ej5.txt";

	public static void main(String[] args) throws IOException {

		// 1.-Abrir archivo para leer y crear un ArrayList
		BufferedReader origen = new BufferedReader(new FileReader(ARCHIVO1));
		ArrayList<String> v = new ArrayList<String>();

		// 2.-Leo el archivo y escribo en el vector
		String linea = "";
		linea = origen.readLine();
		while (linea != null) {
			v.add(linea);
			linea = origen.readLine();
		}

		// 3.-Cierro el archivo
		origen.close();

		// 4.-Abro el segundo archivo para escribir
		PrintWriter destino = new PrintWriter(new FileWriter(ARCHIVO2, false));

		// 5.-Leo el vector del reves y voy escribiendo
		for (int i = v.size() - 1; i >= 0; i--) {
			destino.println(v.get(i));
		}

		// 6.-Cierro el archivo
		destino.close();

		// 7.-Mostrar archivo origen
		visualizarArchivo(ARCHIVO1);

		// 8.-Mostrar archivo destino
		visualizarArchivo(ARCHIVO2);

	}

	public static void visualizarArchivo(String nombreArchivo) {

		try {

			// 1.-Abro para leer
			BufferedReader f = new BufferedReader(new FileReader(nombreArchivo));

			// 2.-Recorro linea a linea hasta null
			String linea = "";
			linea = f.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = f.readLine();
			}

			// 3.-Cierro fichero
			f.close();

		} catch (FileNotFoundException e) {
			System.out.println("No se ha podido leer el archivo.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error al intentar leer el archivo.");
			System.exit(0);
			// System.exitt(0) cierra completamente la aplicacion
		}
	}

}
