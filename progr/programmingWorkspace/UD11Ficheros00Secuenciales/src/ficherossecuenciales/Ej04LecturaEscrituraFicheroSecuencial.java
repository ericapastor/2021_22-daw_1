package ficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Ej04LecturaEscrituraFicheroSecuencial {

	public static void main(String[] args) throws IOException {

		// 1.-Abrir el fichero para leer y el segundo para escribir
		BufferedReader origen = new BufferedReader(new FileReader("ej3.txt"));
		PrintWriter destino = new PrintWriter(new FileWriter("ej4.txt"));

		// 2.-Leo del primero y escribo en el segundo en mayusculas
		String linea = "";
		linea = origen.readLine();
		while (linea != null) {
			destino.println(linea.toUpperCase());
			linea = origen.readLine();
		}

		// 3.-Cerrar los archivos y el Scanner
		origen.close();
		destino.close();

		// 4.-Visualizar el archivo orgien
		System.out.println("Visualizo fichero origen");
		visualizarArchivo("ej3.txt");

		// 5.-Visualizar el archivo destino
		System.out.println("Visualizo fichero destino");
		visualizarArchivo("ej4.txt");
	}

	public static void visualizarArchivo(String nombreArchivo) {

		try {

			// 1.-Abro para leer
			BufferedReader f = new BufferedReader(new FileReader(nombreArchivo));

			// 2.-Recorro linea a linea hasta null
			String linea = "";
			linea = f.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = f.readLine();
			}

			// 3.-Cierro fichero
			f.close();

		} catch (FileNotFoundException e) {
			System.out.println("No se ha podido leer el archivo.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error al intentar leer el archivo.");
			System.exit(0);
			// System.exitt(0) cierra completamente la aplicacion
		}
	}

}
