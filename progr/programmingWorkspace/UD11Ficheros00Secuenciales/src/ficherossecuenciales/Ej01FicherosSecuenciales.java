package ficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Ej01FicherosSecuenciales {

	// cualquier flujo de informacion en java necesita un Stream (flujo)
	// flujo conexion entre el programa y el dispositivio de entrada o salida
	// **Tipos de flujo:
	// caracteres (texto)
	// bytes (binario)
	// **Tipos de archivos:
	// textos (no enriquecidos), los que abrimos con bloc de notas, wordpad...
	// binarios (enriquecidos), los que contienen video, texto con imagenes...
	// **Clases caracteres
	// InputStreamReader, OutputStreamReader
	// **Clases bytes
	// Reader, Writer
	// **Modos de acceso (secuencial, aleatorio)
	// Modos de acceso (lectura BufferedReader) (escritura PrintWriter)

	public static void main(String[] args) {
		try {

			// 1.-Crear un puntero al archivo
			BufferedReader f = new BufferedReader(new FileReader("ej1.txt"));
			// si tuviera que ponerle una ruta, por ejemplo:
			// "c:\\documents\\datos.txt" -> doble barra necesaria

			// 2.-Recorrer el archivo linea a linea
			String nombre = "";
			nombre = f.readLine();
			// el proceso de leer un fichero secuencialmente
			// finaliza cuando se lee el fin del fichero (EOF)
			// al final del fichero encuentra (null)
			while (nombre != null) {
				System.out.println(nombre);
				nombre = f.readLine();
			}

			// 3.-Cerrrar el archivo
			f.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("No se encontro el archivo.");
		} catch (IOException e) {
			System.out.println("El fichero no es accesible.");
		}
	}

}
