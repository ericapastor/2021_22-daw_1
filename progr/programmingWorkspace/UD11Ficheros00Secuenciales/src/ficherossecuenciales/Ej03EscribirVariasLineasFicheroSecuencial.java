package ficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Ej03EscribirVariasLineasFicheroSecuencial {

	public static String archivo = "ej3.txt";

	public static void main(String[] args) throws IOException {

		// 1.-Abrir el archivo para escribir
		PrintWriter f = new PrintWriter(new FileWriter(archivo, false));
		Scanner input = new Scanner(System.in);

		// 2.-Escribir en el archivo linea a linea hasta escribir fin
		// leer linea a linea de teclado mientras no sea 'fin'
		// escribir linea en el archivo
		System.out.println("Escribiendo lineas en un archivo (fin para acabar)");
		String linea = "";
		linea = input.nextLine();
		while (!linea.equalsIgnoreCase("fin")) {
			f.println(linea);
			linea = input.nextLine();
		}

		// 3.-Cerrar archivo
		f.close();
		input.close();

		BufferedReader read = new BufferedReader(new FileReader(archivo));
		linea = read.readLine();
		while (linea != null) {
			System.out.println(linea);
			linea = read.readLine();
		}
		read.close();

	}

}
