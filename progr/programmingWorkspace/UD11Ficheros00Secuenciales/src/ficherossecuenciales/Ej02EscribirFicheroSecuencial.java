package ficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Ej02EscribirFicheroSecuencial {
	// creamos una variable para guardar el nombre del archivo
	public static String archivo = "ej2.txt";

	public static void main(String[] args) throws IOException {
		// 1.-Abrir el fichero para escribir
		PrintWriter w = new PrintWriter(new FileWriter(archivo, false));
		Scanner input = new Scanner(System.in);
		String linea = "";
		// true escribira al final
		// false escribira al principio, sobreescribiendo todo

		// 2.- Escribo en el fichero
		System.out.println("Dame el texto que quieres guardar en el fichero");
		linea = input.nextLine();
		w.print(linea + "\n");
		// 3.-Cerrar
		input.close();
		w.close();

		// 1.-Abrir fichero lectura
		BufferedReader r = new BufferedReader(new FileReader(archivo));

		// 2.-Leer el fichero
		linea = r.readLine();
		while (linea != null) {
			System.out.println(linea);
			linea = r.readLine();
		}

		// 3.-Cerrar el fichero
		r.close();
	}

}
