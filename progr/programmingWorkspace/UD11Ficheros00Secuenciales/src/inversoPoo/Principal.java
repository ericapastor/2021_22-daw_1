package inversoPoo;

public class Principal {

	public static void main(String[] args) {

		// crear un objeto de clase archivo
		Archivo archivo1 = new Archivo("datos.txt");

		// visualizarlo
		archivo1.visualizarArchivo("datos.txt");

		// darle la vuelta
		archivo1.crearArchivoInvertido();
		archivo1.visualizarArchivo("datos.txt");
	}

}
