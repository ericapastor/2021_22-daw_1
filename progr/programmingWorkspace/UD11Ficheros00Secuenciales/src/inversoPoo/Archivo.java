package inversoPoo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Archivo {

	// 1.-Atributos(nombre archivo)
	String nombreArchivo;

	// 2-.Constructores
	public Archivo() {
		this.nombreArchivo = "";
	}

	public Archivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	// 3.-Metodo visualizar archivo
	public void visualizarArchivo(String nombreArchivo) {
		try {
			// 1.-Abrir para lectura
			BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));
			// 2.-Recorro linea a linea hasta null
			String linea = "";
			linea = origen.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = origen.readLine();
			}
			// 3.-Cerrar el archivo
			origen.close();
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el archivo.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Archivo inaccesible.");
			System.exit(0);
		}
	}

	// 4.-Metodo invertir archivo
	public void crearArchivoInvertido() {
		try {
			// 1.-Abrir el archivo para leer y crear un vector ArrayList
			BufferedReader origen = new BufferedReader(new FileReader(this.nombreArchivo));
			// 2.-Leo el archivo y escribo en el vector
			String linea = "";
			linea = origen.readLine();
			ArrayList<String> archivito = new ArrayList<String>();
			while (linea != null) {
				archivito.add(linea);
				linea = origen.readLine();
			}
			// 3.-Cierro el archivo
			origen.close();
			// 4.-Abrir archivo de escritura
			PrintWriter destino = new PrintWriter(new FileWriter(this.nombreArchivo, false));
			// 5.-Escribo en el archivo de destino
			for (int i = archivito.size() - 1; i >= 0; i--) {
				destino.print(archivito.get(i) + "\n");
			}
			// 6.-Cierro el segundo archivo
			destino.close();
		} catch (FileNotFoundException e) {
			System.out.println("Archivo no encontrado.");
		} catch (IOException e) {
			System.out.println("Archivo inaccesible.");
		}
	}

}
