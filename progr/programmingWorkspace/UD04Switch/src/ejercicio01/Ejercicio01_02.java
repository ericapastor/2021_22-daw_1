package ejercicio01;

import java.util.Scanner;

public class Ejercicio01_02 {

	public static void main(String[] args) {

		/*
		 * 1a. Programa Java que lea un car�cter en min�scula (no hace falta
		 * comprobarlo) y, utilizando una sentencia switch, diga qu� vocal es, o en caso
		 * contrario, que indique que no es vocal, y muestre el car�cter en ambos casos.
		 * Probar el ejercicio usando la sentencia break en los cases. 1b Mismo
		 * ejercicio, pero sin usar sentencia break en los �cases�. Mostrar un mensaje
		 * al final del programa explicando qu� ocurre.
		 */

		Scanner program = new Scanner(System.in);

		char letter;
		System.out.println("Type a vowel in low case.");
		letter = program.nextLine().charAt(0);

		switch (letter) {
		case 'a':
			System.out.println("It's an a.");
		case 'e':
			System.out.println("It's an e.");
		case 'i':
			System.out.println("It's an i.");
		case 'o':
			System.out.println("It's an o.");
		case 'u':
			System.out.println("It's an u.");
		default:
			System.out.println("Not a vowel.");
		}

		// al no poner break, se imprimen todos los mensajes desde la vocal
		// que escribo tras ejecutar el programa

		program.close();

	}

}
