package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {

		/*
		 * Pedir una nota num�rica entera entre 0 y 10, y mostrar dicha nota de la
		 * forma: cero, uno, dos, tres... Comprobar si la opci�n introducida no es
		 * v�lida.
		 */

		Scanner program = new Scanner(System.in);

		int num;

		System.out.println("Type a number between 0 and 10" + "\n");
		num = program.nextInt();

		System.out.println(/* this makes an 'enter' */);

		switch (num) {
		case 0:
			System.out.println("Zero.");
			break;
		case 1:
			System.out.println("One.");
			break;
		case 2:
			System.out.println("Two");
			break;
		case 3:
			System.out.println("Three");
			break;
		case 4:
			System.out.println("Four");
			break;
		case 5:
			System.out.println("Five");
			break;
		case 6:
			System.out.println("Six");
			break;
		case 7:
			System.out.println("Seven");
			break;
		case 8:
			System.out.println("Eight");
			break;
		case 9:
			System.out.println("Nine");
			break;
		case 10:
			System.out.println("Ten");
			break;
		default:
			System.out.println("Number not valid.");
			break;
		}

		program.close();
	}

}
