package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {

		/*
		 * Programa que lea un mes en tipo String y compruebe si el valor corresponde a
		 * un mes de 30, 31 o 28 d�as. Se mostrar� adem�s el nombre del mes. Comprobar
		 * si la opci�n introducida no es v�lida. (No es necesario poner break; en todos
		 * los casos.)
		 */

		Scanner program = new Scanner(System.in);

		String month;
		System.out.println("Type a month." + "\n");
		month = program.nextLine();
		switch (month) {
		case "Januar":
		case "March":
		case "May":
		case "July":
		case "August":
		case "October":
		case "December":
			System.out.println("\n" + "The month is " + month + "." + "\n");
			System.out.println("Month has 31 days.");
			break;
		case "Februar":
			System.out.println("\n" + "The month is " + month + "." + "\n");
			System.out.println("Month has 28 days, or 29 depending on the year.");
			break;
		case "April":
		case "June":
		case "September":
		case "November":
			System.out.println("\n" + "The month is " + month + "." + "\n");
			System.out.println("Month has 30 days.");
			break;
		default:
			System.out.println(
					"Month not valid: must be in English in the following format: Upper case letter as the capital letter and then low case letters. Punctuation marks not necessary.");
		}

		program.close();
	}

}
