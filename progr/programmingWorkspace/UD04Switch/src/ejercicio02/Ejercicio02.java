package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {

		/*
		 * Programa que pida al usuario una cantidad de gramos y posteriormente pida
		 * seleccionar una opci�n de un men� con opciones num�ricas (opciones del men�:
		 * 1-Kilogramo, 2- Hectogramos, 3-Decagramos, 4-Decigramos, 5-Centigramos,
		 * 6-Miligramos). Al seleccionar una opci�n mostrar la conversi�n de gramos a
		 * dicha unidad. Comprobar si la opci�n del men� introducida no es v�lida. Si la
		 * cantidad de gramos es un valor negativo, no hacer nada.
		 */

		Scanner program = new Scanner(System.in);

		double grams;
		int option;

		System.out.println("Type a quantity in grams.");
		grams = program.nextDouble();

		if (grams >= 0) {

			System.out.println("\n" + "Select the option to convert, by typing the number of the option: " + "\n");
			System.out.println("1.- Kilograms");
			System.out.println("2.- Hectograms");
			System.out.println("3.- Decagrams");
			System.out.println("4.- Decigrams");
			System.out.println("5.- Centigrams");
			System.out.println("6.- Miligrams");
			System.out.println();

			option = program.nextInt();

			System.out.println();

			switch (option) {
			case 1:
				System.out.println(grams + " grams are " + (grams / 1000) + " kilograms.");
				break;
			case 2:
				System.out.println(grams + " grams are " + (grams / 100) + " hectograms.");
				break;
			case 3:
				System.out.println(grams + " grams are " + (grams / 10) + " decagrams.");
				break;
			case 4:
				System.out.println(grams + " grams are " + (grams * 10) + " decigrams.");
				break;
			case 5:
				System.out.println(grams + " grams are " + (grams * 100) + " centigrams.");
				break;
			case 6:
				System.out.println(grams + " grams are " + (grams * 1000) + " miligrams.");
				break;
			default:
				System.out.println("Option not valid.");

			}
			
		} else {
			System.out.println("Grams can't be negative.");
		}

		program.close();
	}

}
