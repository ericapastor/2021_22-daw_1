package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {

		/*
		 * Pedir una nota num�rica entera entre 0 y 10 y mostrar el valor de la nota:
		 * Insuficiente, Suficiente, Bien, Notable o Sobresaliente. (No es necesario
		 * poner break; en todos los casos.)
		 */

		Scanner program = new Scanner(System.in);

		int num;

		System.out.println("Type your grade.");
		num = program.nextInt();
		if (num >= 0) {
			switch (num) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
				System.out.println("F");
				break;
			case 5:
				System.out.println("D");
				break;
			case 6:
				System.out.println("C");
				break;
			case 7:
			case 8:
				System.out.println("B");
				break;
			case 9:
			case 10:
				System.out.println("A");
				break;
			default:
				System.out.println("Not valid.");

			}
		}

		program.close();
	}

}
