package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {

		/*
		 * Programa que pida 2 n�meros float y a continuaci�n un car�cter de operaci�n
		 * aritm�tica (+, -, *, /, %). Mediante una sentencia switch escoger el caso
		 * referente al car�cter de operaci�n introducido. El programa mostrar� un
		 * mensaje con el resultado de la operaci�n escogida, entre los 2 n�meros.
		 * Indicar si el car�cter introducido no es v�lido.
		 */

		Scanner program = new Scanner(System.in);

		float num1;
		float num2;
		char charac;

		System.out.println("Type a number. I can include decimals." + "\n");
		num1 = program.nextFloat();

		System.out.println("\n" + "Type anohter number. I can include decimals." + "\n");
		num2 = program.nextFloat();

		System.out.println("\n" + "What operation would you wish to make?" + "\n");
		System.out.println('+');
		System.out.println('-');
		System.out.println('*');
		System.out.println('/');
		System.out.println();
		program.nextLine();
		charac = program.nextLine().charAt(0);
		System.out.println();
		switch (charac) {
		case '+':
			System.out.println("The operation is " + num1 + "+" + num2 + "=" + (num1 + num2) + ".");
			break;
		case '-':
			System.out.println("The operation is " + num1 + "-" + num2 + "=" + (num1 - num2) + ".");
			break;
		case '*':
			System.out.println("The operation is " + num1 + "*" + num2 + "=" + (num1 * num2) + ".");
			break;
		case '/':
			System.out.println("The operation is " + num1 + "/" + num2 + "=" + (num1 / num2) + ".");
			break;
		default:
			System.out.println("Operator not recognized.");
			break;
		}

		program.close();
	}

}
