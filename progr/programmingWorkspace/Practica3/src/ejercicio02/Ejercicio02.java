package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {
		// lleno el array pueblos
		String[] pueblos = { "Biota", "Tauste", "Gallur", "Tarazona", "Trasmoz", "Ateca", "Jaraba", "Anento",
				"Gallocanta", "Belchite", "Cadrete", "Gelsa" };
		// lleno el array comarcas
		String[] comarcas = { "Ejea de los Caballeros", "Ejea de los Caballeros", "Borja", "Borja", "Borja",
				"Calatayud", "Calatayud", "Daroca", "Daroca", "Zaragoza", "Zaragoza", "Zaragoza" };
		mostrarPueblos(pueblos, comarcas);
	}

	static void mostrarPueblos(String[] pueblo, String[] comarca) {
		for (int i = 0; i < pueblo.length; i++) {
			System.out.println("El pueblo " + pueblo[i] + " pertenece a la comarca " + comarca[i] + ".");
		}
	}
}
