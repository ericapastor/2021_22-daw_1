package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int[] leerHabitantes = new int[10];
		// lleno el vector
		for (int i = 0; i < leerHabitantes.length; i++) {
			System.out.println("Dame el numero de habitantes del pueblo numero " + (i + 1));
			leerHabitantes[i] = input.nextInt();
		}
		// llamo a los metodos
		mediaHabitantes(leerHabitantes);
		maximoHabitantes(leerHabitantes);
		minimoHabitantes(leerHabitantes);
		input.close();
	}

	static void mediaHabitantes(int[] array) {
		int suma = 0;
		double media = 0;
		// hago la suma recorriendo el for
		for (int i = 0; i < array.length; i++) {
			suma = suma + array[i];
		}
		// la muestro por pantalla
		System.out.println("La suma de habitantes es: " + suma);
		// calculo la media convirtiendo la suma en double y la muestro
		media = ((double) suma) / 10;
		System.out.println("La media de habitantes es: " + media);
	}

	static void maximoHabitantes(int[] array) {
		int max = 0;
		// recorro el vector comparando para encontrar el maximo numero
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		// muestro cual es el maximo
		System.out.println("El maximo de habitantes es: " + max);
	}

	static void minimoHabitantes(int[] array) {
		int min = 999999999;
		// recorro el vector comparando para encontrar el minimo numero
		for (int i = 0; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}
		// muestro cual es el minimo
		System.out.println("El minimo de habitantes es: " + min);
	}
}
