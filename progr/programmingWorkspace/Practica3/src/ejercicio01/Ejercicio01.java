package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// leo el pueblo
		System.out.println("Que pueblo quieres buscar?");
		String pueblo = input.nextLine();
		// declaro la comarca
		String comarca;
		comarca = mostrarComarca(pueblo);
		System.out.println("El pueblo " + pueblo + " pertenece a la comarca de " + comarca + ".");
		input.close();
	}

	static String mostrarComarca(String pueblo) {
		String comarca = "";
		if (pueblo.equalsIgnoreCase("Biota") || pueblo.equalsIgnoreCase("Tauste")) {
			comarca = "Ejea de los Caballeros";
		} else if (pueblo.equalsIgnoreCase("Fabara")) {
			comarca = "Caspe";
		} else if (pueblo.equalsIgnoreCase("Anento") || pueblo.equalsIgnoreCase("Gallocanta")) {
			comarca = "Daroca";
		} else if (pueblo.equalsIgnoreCase("Calatorao") || pueblo.equalsIgnoreCase("Codos")) {
			comarca = "La Almunia de Do�a Godina";

		} else if (pueblo.equalsIgnoreCase("Gallur") || pueblo.equalsIgnoreCase("Tarazona")) {
			comarca = "Comarca Borja";

		} else if (pueblo.equalsIgnoreCase("Cadrete") || pueblo.equalsIgnoreCase("Pedrola")
				|| pueblo.equalsIgnoreCase("Quinto") || pueblo.equalsIgnoreCase("Zuera")) {
			comarca = "Zaragoza";

		} else if (pueblo.equalsIgnoreCase("Ateca") || pueblo.equalsIgnoreCase("Calatayud")
				|| pueblo.equalsIgnoreCase("Jaraba")) {
			comarca = "Calatayud";
		}
		return comarca;
	}

}
