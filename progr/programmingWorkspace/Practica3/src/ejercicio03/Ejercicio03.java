package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// declaro variables
		String pueblo;
		boolean encontrado;
		String[] pueblos = { "Biota", "Tauste", "Gallur", "Tarazona", "Trasmoz", "Ateca", "Jaraba", "Anento",
				"Gallocanta", "Belchite", "Cadrete", "Gelsa" };
		// pido el nombre del pueblo
		System.out.println("Introduce un pueblo");
		pueblo = input.nextLine();
		// devuelvo el boolean y lo guardo
		encontrado = encontrado(pueblos, pueblo);
		if (encontrado == true) {
			System.out.println("El pueblo esta dentro del array.");
		} else {
			System.out.println("El pueblo no esta dentro del array.");
		}

		input.close();
	}

	static boolean encontrado(String[] array, String pueblo) {
		boolean si = false;
		for (int i = 0; i < array.length; i++) {
			if (array[i].equalsIgnoreCase(pueblo)) {
				si = true;
				break;
			}
		}
		return si;
	}

}
