package programa;

import clases.GestorTrabajos;

public class Programa {

	public static void main(String[] args) {
		System.out.println("Crear instancia de gestor de trabajos");
		GestorTrabajos gestor = new GestorTrabajos();
		
		System.out.println("Alta de 3 responsables");
		gestor.altaResponsable("54321", "responsable1");
		gestor.altaResponsable("12345", "responsable2");
		gestor.altaResponsable("12345", "responsable3");
		
		System.out.println("Listar responsables");
		gestor.listarResponsables();
		
		System.out.println("Buscar un responsable y uno que no exista");
		System.out.println(gestor.buscarResponsable("12345"));
		System.out.println(gestor.buscarResponsable("123456"));
		
		System.out.println("Dar de alta 3 trabajos");
		gestor.altaTrabajo("trabajo1", "Cliente1", 10.0, "2019-04-02");
		gestor.altaTrabajo("trabajo2", "Cliente2", 10.0, "2019-05-02");
		gestor.altaTrabajo("trabajo3", "Cliente3", 10.0, "2020-04-02");
		
		System.out.println("Asignamos responsable");
		gestor.asignarResponsable("12345", "trabajo1");
		gestor.asignarResponsable("12345", "trabajo2");
		
		System.out.println("Listamos trabajos de un responsable");
		gestor.listarTrabajosResponsable("12345");
		
		System.out.println("Listar trabajos 2019");
		gestor.listarTrabajosAnno(2019);
		
		System.out.println("Eliminamos un trabajo");
		gestor.eliminarTrabajo("trabajo2");
		System.out.println("Listar trabajos de responsable");
		gestor.listarTrabajosResponsable("12345");
		
		System.out.println("Hacemos un ere");
		gestor.ere();
		gestor.listarResponsables();
		
		

	}

}