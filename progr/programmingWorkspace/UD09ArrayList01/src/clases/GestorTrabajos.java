package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorTrabajos {
	private ArrayList<Trabajo> listaTrabajos;
	private ArrayList<Responsable> listaResponsables;

	public GestorTrabajos() {
		listaTrabajos = new ArrayList<Trabajo>();
		listaResponsables = new ArrayList<Responsable>();
	}

	// alta responsable
	public void altaResponsable(String dni, String nombre) {
		if (!existeResponsable(dni)) {
			Responsable nuevoResponsable = new Responsable(dni, nombre);
			nuevoResponsable.setFechaContratacion(LocalDate.now());
			listaResponsables.add(nuevoResponsable);
		} else {
			System.out.println("El responsable ya existe");
		}
	}

	// alta trabajo
	public void altaTrabajo(String nombre, String cliente, double presupuesto, String fechaConcesion) {
		Trabajo nuevoTrabajo = new Trabajo(nombre, cliente, presupuesto, fechaConcesion);
		listaTrabajos.add(nuevoTrabajo);
	}

	// buscar responsable
	public Responsable buscarResponsable(String dni) {
		for (Responsable jefe : listaResponsables) {
			if (jefe != null && jefe.getDni().equals(dni)) {
				return jefe;
			}
		}
		return null;
	}

	// buscarResponsableExperto
	public Responsable buscarResponsableExperto() {
		LocalDate fechaAntigua = null;
		for (int i = 0; i < listaResponsables.size(); i++) {
			Responsable responsableActual = listaResponsables.get(i);
			if (responsableActual != null && i == 0) {
				fechaAntigua = responsableActual.getFechaContratacion();
			} else {
				if (responsableActual != null && responsableActual.getFechaContratacion().isAfter(fechaAntigua)) {
					fechaAntigua = responsableActual.getFechaContratacion();
				}
			}
		}
		for (Responsable jefe : listaResponsables) {
			if (jefe != null && jefe.getFechaContratacion().equals(fechaAntigua)) {
				return jefe;
			}
		}
		return null;
	}

	// buscarTrabajo
	public Trabajo buscarTrabajo(String nombreTrabajo) {
		for (Trabajo trabajo : listaTrabajos) {
			if (trabajo != null && trabajo.getNombre().equals(nombreTrabajo)) {
				return trabajo;
			}
		}
		return null;
	}

	// existe responsable
	public boolean existeResponsable(String dni) {
		for (Responsable responsable : listaResponsables) {
			if (responsable != null && responsable.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}

	// eliminar trabajo
	public void eliminarTrabajo(String nombre) {
		Iterator<Trabajo> iteradorTrabajos = listaTrabajos.iterator();
		while (iteradorTrabajos.hasNext()) {
			Trabajo trabajo = iteradorTrabajos.next();
			if (trabajo.getNombre().equals(nombre)) {
				iteradorTrabajos.remove();
			}
		}
	}

	// asignar responsable
	public void asignarResponsable(String dni, String nombreTrabajo) {
		if (buscarResponsable(dni) != null && buscarTrabajo(nombreTrabajo) != null) {
			Responsable jefe = buscarResponsable(dni);
			Trabajo trabajo = buscarTrabajo(nombreTrabajo);
			trabajo.setResponsableTrabajo(jefe);
		}
	}

	// asignar responsable experto
	public void asignarResponsableExperto(String nombreTrabajo) {
		Trabajo trabajo = buscarTrabajo(nombreTrabajo);
		if (trabajo != null) {
			trabajo.setResponsableTrabajo(buscarResponsableExperto());
		}
	}

	// listar responsable
	public void listarResponsables() {
		for (Responsable responsable : listaResponsables) {
			if (responsable != null) {
				System.out.println(responsable);
			}
		}
	}

	// listrar trabajos a�o
	public void listarTrabajosAnno(int anno) {
		for (Trabajo trabajo : listaTrabajos) {
			if (trabajo.getFechaConcesion().getYear() == anno) {
				System.out.println(trabajo);
			}
		}
	}

	// listar trabajos responsable
	public void listarTrabajosResponsable(String dni) {
		for (Trabajo trabajo : listaTrabajos) {
			if (trabajo.getResponsableTrabajo() != null && trabajo.getResponsableTrabajo().getDni().equals(dni)) {
				System.out.println(trabajo);
			}
		}
	}

	// ere
	public void ere() {
		Iterator<Responsable> iteradorResponsables = listaResponsables.iterator();
		while (iteradorResponsables.hasNext()) {
			Responsable responsableActual = iteradorResponsables.next();
			boolean estaEnTrabajo = false;
			for (Trabajo trabajo : listaTrabajos) {
				if (trabajo.getResponsableTrabajo() != null
						&& trabajo.getResponsableTrabajo().getDni().equals(responsableActual.getDni())) {
					estaEnTrabajo = true;
				}
			}
			if (!estaEnTrabajo) {
				iteradorResponsables.remove();
			}
		}
	}

}