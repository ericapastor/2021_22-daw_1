package clases;

import java.time.LocalDate;

public class Trabajo {
	private String nombre;
	private String cliente;
	private double presupuesto;
	private LocalDate fechaConcesion;
	private Responsable responsableTrabajo;

	public Trabajo(String nombre, String cliente, double presupuesto, String fechaConcesion) {
		this.nombre = nombre;
		this.cliente = cliente;
		this.presupuesto = presupuesto;
		this.fechaConcesion = LocalDate.parse(fechaConcesion);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public double getPresupuesto() {
		return presupuesto;
	}

	public void setPresupuesto(double presupuesto) {
		this.presupuesto = presupuesto;
	}

	public LocalDate getFechaConcesion() {
		return fechaConcesion;
	}

	public void setFechaConcesion(LocalDate fechaConcesion) {
		this.fechaConcesion = fechaConcesion;
	}

	public Responsable getResponsableTrabajo() {
		return responsableTrabajo;
	}

	public void setResponsableTrabajo(Responsable responsableTrabajo) {
		this.responsableTrabajo = responsableTrabajo;
	}

	@Override
	public String toString() {
		return "Trabajo [nombre=" + nombre + ", cliente=" + cliente + ", presupuesto=" + presupuesto
				+ ", fechaConcesion=" + fechaConcesion + ", responsableTrabajo=" + responsableTrabajo + "]";
	}

}