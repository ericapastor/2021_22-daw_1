package ej02;

import java.util.Scanner;

import methods.Methods;

public class Ej02 {

	public static void main(String[] args) {
		// ask for array size
		Scanner input = new Scanner(System.in);
		System.out.println("What size does the array have?");
		int n = input.nextInt();
		int[] random = new int[n];
		// generate random numbers from 0 to 9
		// to the fill array
		random = Methods.fillArrayRandom0to9(random);
		
		// the method called here from another class can't be private
		// but the method called here from another class
		// can call a private method within its own class
		
		// show every position on screen and its value
		Methods.showArray(random);
		input.close();
	}

}
