package ej12;

import java.util.Scanner;

public class Ej12 {

	/* Program that will keep the values of an array and then invert them */
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("What length will the array have?");
		int n = input.nextInt();
		int[] a = new int[n];
		int[] b = new int[n];
		fillArray(a);
		invertArray(a, b);
		showArray(a, b);
		input.close();
	}

	static void fillArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("Value " + i + " of the array: ");
			array[i] = input.nextInt();
		}
	}

	static void invertArray(int[] a, int[] b) {
		for (int i = 0, j = 1; i < a.length; i++, j++) {
			b[i] = a[a.length - j];
		}
	}

	static void showArray(int[] a, int[] b) {
		System.out.println("First array: ");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println();
		for (int i = 0; i < b.length; i++) {
			System.out.print(b[i] + " ");
		}
	}
}
