package ej01;

import methods.Methods;

public class Ej01 {

	public static void main(String[] args) {
		// Array is filled in class Methods
		// I show it by another method in class Methods
		// Using String.split to separate them
		String[] frases = Methods.fillArray();
		Methods.showWords(frases);
	}

}
