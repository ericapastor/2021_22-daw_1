package ej10;

import java.util.Scanner;

import methods.Methods;

public class Ej10 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Create a number array and a String array of 10 positions. We put there grades
		 * from 0 to 10 in decimals in the number array, then ask for students names in
		 * the String array. Afterwards, create a String array where we put the grade in
		 * words:
		 */
		// Between 0 y 4,99, fail.
		// Between 5 y 6,99, passed.
		// Between 7 y 8,99 remarkable.
		// Between 9 y 10, outstanding.
		/*
		 * Show the student and the grade in words, with the methods you feel like
		 * using.
		 */
		final int SIZE = 10;
		double[] grades = new double[SIZE];
		String[] names = new String[SIZE];
		// method one, ask for each thing in different methods -> buffer works unclean
		// grades = Ud07A00Methods.askForGrades();
		// input.nextLine();
		// names = Ud07A00Methods.askForNames();
		// showData(grades, names);*/

		// method two, ask for data in the same method -> buffer works clean
		Methods.askForData(grades, names);
		showData(grades, names);

		input.close();
	}

	private static void showData(double[] grades, String[] names) {
		for (int i = 0; i < grades.length; i++) {
			switch ((int) grades[i]) {
			case 1:
			case 2:
			case 3:
			case 4:
				System.out.println("The grade for the student " + names[i] + " is D. Fail.");
				break;
			case 5:
			case 6:
				System.out.println("The grade for the student " + names[i] + " is C. Passed.");
				break;
			case 7:
			case 8:
				System.out.println("The grade for the student " + names[i] + " is B. Remarkable.");
				break;
			case 9:
			case 10:
				System.out.println("The grade for the student " + names[i] + " is A. Outstanding.");
				break;
			}
		}
	}
}
