package methods;

import java.util.Arrays;
import java.util.Scanner;

public class Methods {

	static Scanner input = new Scanner(System.in);

	public static String[] fillArray() {
		System.out.println("Fill the array, 4 sentences.");
		String[] frases = new String[4];
		for (int i = 0; i < frases.length; i++) {
			System.out.println("Sentence " + (i + 1) + ":");
			frases[i] = input.nextLine();
		}
		return frases;
	}

	public static void showWords(String[] array) {
		System.out.println("Introduce una componente entre 1 y 4.");
		int componente = input.nextInt();
		componente -= 1;
		System.out.println("La frase de la componente " + (componente+1) + " es " + array[componente]);
		int cantidadEspacios = 0;
		for (int i = 0; i < array[componente].length(); i++) {
			if (array[componente].charAt(i) == ' ') {
				cantidadEspacios++;
			}
		}
		String[] palabras = array[componente].split(" ");
		for (int i = 0; i < cantidadEspacios + 1; i++) {
			System.out.println(palabras[i]);
		}
	}

	// generate random numbers from 0 to 9
	private static int random0to9() {
		int n = (int) (Math.round(Math.random() * 9));
		return n;
	}

	public static int[] fillArrayRandom0to9(int[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] = random0to9();
		}
		return array;
	}

	// generate random numbers from 1 to 100

	private static int random1to100() {
		int n = (int) (Math.round(Math.random() * 99) + 1);
		return n;
	}

	public static int[] fillArrayRandom1to100(int[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] = random1to100();
		}
		return array;
	}

	public static void showArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("Position " + i + ": " + array[i] + ".");
		}
	}

	public static void showArray(char[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]);
		}
	}

	public static double[] askForGrades() {
		double[] array = new double[10];
		for (int i = 0; i < array.length; i++) {
			System.out.println("Nota " + (i + 1));
			array[i] = input.nextDouble();
		}
		return array;
	}

	public static String[] askForNames() {
		String[] array = new String[10];
		for (int i = 0; i < array.length; i++) {
			System.out.println("Name " + (i + 1));
			array[i] = input.nextLine();
		}
		return array;
	}

	public static void askForData(double[] grades, String[] names) {
		for (int i = 0; i < grades.length; i++) {
			System.out.println("Name of the student " + (i + 1) + ":");
			names[i] = input.nextLine();
			do {
				System.out.println("Grade of this student?");
				grades[i] = input.nextInt();
			} while (grades[i] >= 0 && grades[i] <= 10);
			input.nextLine();
		}
	}

	public static void showMatrix(String[][] matrix) {
		int i;
		int j;
		for (i = 0; i < matrix.length; i++) {
			for (j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j]);
			}
			System.out.println();
		}
	}

	public static void createRandomNumArrays(int[] array, int start, int end) {
		for (int i = 0; i < array.length; i++) {
			if (start < end) {
				array[i] = (int) (Math.round((Math.random() * end - start) + start));
			} else {
				array[i] = (int) (Math.round((Math.random() * start - end) + end));
			}
		}
	}

	public static int[] parseNumberIntoArray(String n, int l) {
		int[] d = new int[l];
		for (int i = 0; i < n.length(); i++) {
			d[i] = Character.getNumericValue(n.charAt(i));
		}
		return d;
	}

	public static boolean isPalindrome(int[] array) {
		int[] a = new int[array.length];
		for (int i = 0, j = 1; i < array.length; i++, j++) {
			a[i] = array[array.length - j];
		}
		if (Arrays.equals(array, a)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isPrime(int z) {
		int c = 0;
		for (int i = 1; i < z; i++) {
			if (z % i == 0) {
				c++;
			} else if (c > 1) {
				return false;
			}
		}
		return true;
	}
}
