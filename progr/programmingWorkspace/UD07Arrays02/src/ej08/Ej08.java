package ej08;

import java.util.Scanner;

import methods.Methods;

public class Ej08 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
		 * n�meros aleatorios entre 1 y 300 y mostrar aquellos n�meros que acaben en un
		 * d�gito que nosotros le indiquemos por teclado (debes controlar que se
		 * introduce un numero correcto), estos deben guardarse en un nuevo array. Por
		 * ejemplo, en un array de 10 posiciones e indicamos mostrar los n�meros
		 * acabados en 5, podr�a salir 155, 25, etc.
		 */
		int l;
		do {
			System.out.println("Dime la largura del array.");
			l = input.nextInt();
			if (l != 0) {
				int array[] = new int[l];
				System.out.println("Dime un numero a buscar, entre 0 y 9.");
				int n;
				do {
					n = input.nextInt();
					System.out.println(n >= 10 || n < 0
							? "No puede ser un numero de dos cifras o mas ni negativo. Intentalo de nuevo"
							: "");
				} while (n >= 10 || n < 0);
				Methods.fillArrayRandom1to100(array);
				buscarNumero(n, array);
			}
		} while (l != 0);
		input.close();
	}

	static void buscarNumero(int n, int[] array) {
		String a = "";
		String b = "";
		int g;
		for (int i = 0; i < array.length; i++) {
			g = array[i];
			g %= 10;
			if (g == n) {
				a = a + array[i] + " ";
			}
		}
		if (a.equals(b)) {
			System.out.println("No habia coincidencias.");
		} else {
			System.out.println("Estos numeros terminan en " + n + ", que era el numero deseado para buscar:\n" + a);
		}
	}
}