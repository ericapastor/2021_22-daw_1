package ej09;

import java.util.Scanner;

public class Ej09 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		/*
		 * Calcula la letra de un DNI, pediremos el DNI por teclado y nos devolver� el
		 * DNI completo. Para calcular la letra, cogeremos el resto de dividir nuestro
		 * dni entre 23, el resultado debe estar entre 0 y 22. Haz un m�todo donde seg�n
		 * el resultado de la anterior formula busque en un array de caracteres la
		 * posici�n que corresponda a la letra. Por ejemplo, si introduzco 70588387, el
		 * resultado ser� de 7 que corresponde a �F�.
		 */

		System.out.println("Dame tu DNI");
		int n = input.nextInt();
		final int DIVISOR = 23;
		int m = n % DIVISOR;
		calcularLetra(n, m);
		input.close();
	}

	static void calcularLetra(int n, int m) {
		char letras[] = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H',
				'L', 'C', 'K', 'E' };
		for (int i = 0; i < letras.length; i++) {
			if (i == m) {
				System.out.println("Tu DNI es " + n + "" + letras[i]);
			}
		}
	}

}
