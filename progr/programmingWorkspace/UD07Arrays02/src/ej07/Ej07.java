package ej07;

import java.util.Scanner;

import methods.Methods;

public class Ej07 {

	/*
	 * Create two arrays of numbers with a length asked by Scanner One of them will
	 * be filled with random numbers The other will point the first array Create
	 * again the first array and fill it again with random numbers Finally: create a
	 * method that receives both arrays and returns one that will be the multiple of
	 * position 0 of array 1 and 2 and so on Show the content of each array
	 */

	public static void main(String[] args) {
		// declare
		int i;
		// ask for length for arrays
		Scanner input = new Scanner(System.in);
		System.out.println("Dime la largura de los arrays.");
		int n = input.nextInt();
		// declare both arrays
		int[] array1 = new int[n];
		int[] array2 = new int[n];
		// fill 1st array and show
		array1 = Methods.fillArrayRandom0to9(array1);
		System.out.println("Array 1:");
		Methods.showArray(array1);
		// fill 2nd array and show
		for (i = 0; i < array1.length; i++) {
			array2[i] = array1[i];
		}
		System.out.println("Array 2:");
		Methods.showArray(array2);
		// fill array 1 again with random numbers, then show
		array1 = new int[n];
		Methods.createRandomNumArrays(array1, 10, 100);
		System.out.println("Array 1 again:");
		Methods.showArray(array1);
		// call for the method that will multiply positions, then show
		array1 = multiplyArrays(array1, array2);
		System.out.println("Show array 1 again, multiplied with array 2.");
		Methods.showArray(array1);
		input.close();
	}

	static int[] multiplyArrays(int[] array1, int[] array2) {
		for (int i = 0; i < array1.length; i++) {
			array1[i] = array1[i] * array2[i];
		}
		return array1;
	}
}
