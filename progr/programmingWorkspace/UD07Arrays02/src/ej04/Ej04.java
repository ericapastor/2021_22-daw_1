package ej04;

public class Ej04 {

	public static void main(String[] args) {
		// create a 100 positions array with numbers from 1 to 100
		// show the sum of them all and its average value
		int[] n = new int[100];
		int s = 0;
		for (int i = 0; i < 100; i++) {
			n[i] = i+1;
			s = s + i;
		}
		int a = s / n.length;
		System.out.println("Sum is " + s);
		System.out.println("Average value is " + a);
	}

}
