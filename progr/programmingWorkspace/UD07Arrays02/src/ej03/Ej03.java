package ej03;

import java.util.Scanner;

import methods.Methods;

public class Ej03 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// create an array which size is asked by Scanner
		// this array contains random prime numbers
		// (random numbers must be checked before going into the array)
		// then show which number is the biggest
		System.out.println("What length will the array have?");
		int n = input.nextInt();
		int[] a = new int[n];
		int b = 0;
		fillArray(a);
		Methods.showArray(a);
		bigger(a, b);
		input.close();
	}

	static void fillArray(int[] a) {
		for (int i = 0; i < a.length; i++) {
			int z = 0;
			do {
				z = (int) (Math.round((Math.random() * 99) + 1));
				System.out.println(z);
			} while (Methods.isPrime(z) == false);
			a[i] = z;
		}
	}

	static void bigger(int[] a, int b) {
		for (int i = 0; i < a.length; i++) {
			if (a[i] > b) {
				b = a[i];
			}
		}
		System.out.println("The biggest number is " + b + ".");
	}

}
