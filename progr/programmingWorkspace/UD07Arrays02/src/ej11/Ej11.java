package ej11;

import java.util.Scanner;

import methods.Methods;

public class Ej11 {

	/*
	 * Program that will ask for a number and then check if it is a palindrome
	 * number. You can use this: Character.getNumericValue(string.charAt(position))
	 * to turn the number into an array
	 */

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// Introduce number
		System.out.println("Type a number.");
		int nn = input.nextInt();
		// Parse it to an array
		String sn = nn + "";
		int[] d = Methods.parseNumberIntoArray(sn, sn.length());
		if (Methods.isPalindrome(d)) {
			System.out.println("The number " + nn + " is a palindrome.");
		} else {
			System.out.println("The number " + sn + " is not a palindrome.");
		}
		input.close();
	}
}
