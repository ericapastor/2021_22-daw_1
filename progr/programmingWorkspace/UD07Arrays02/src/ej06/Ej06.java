package ej06;

import java.util.Scanner;

import methods.Methods;

public class Ej06 {

	public static void main(String[] args) {
		/*
		 * Pide al usuario por teclado una frase y pasa sus caracteres a un array de
		 * caracteres. Puedes hacer con o sin m�todos de String.
		 */
		Scanner input = new Scanner(System.in);
		System.out.println("Escribe una frase.");
		String frase = input.nextLine();
		char[] array = new char[frase.length()];
		for (int i = 0; i < array.length; i++) {
			array[i] = frase.charAt(i);
		}
		Methods.showArray(array);
		input.close();
	}

}
