package ej05;

import java.util.Scanner;

public class Ej05 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		// create an array of chars containing upper case letters of the alphabet
		// ask for positions by Scanner
		// If the position is correct, it will be added to another array
		// this new array will be shown at the end
		// if the position is incorrect, program must say it
		// introducing number -1 ends the program

		char[] alphabet = new char[26];
		for (int i = 65, j = 0; i <= 90; i++, j++) {
			alphabet[j] = (char) i; // this is the same as making a counter
			// as seen right now:
			// int n = 'A';
			// alphabet[i] = (char) n;
			// n++;
		}

		String chain = "";
		int choice = -1;

		do {
			System.out.println("Choose an index between 0 and 25.");
			choice = input.nextInt();
			if (choice < -1 || choice > alphabet.length - 1) {
				System.out.println("Number is not valid.");
			} else {
				if (choice != -1) {
					chain += alphabet[choice];
				} else {
					System.out.println("Program will end.");
					break;
				}
			}
		} while (choice != -1);

		System.out.println(chain);
		
		input.close();
	}

}
