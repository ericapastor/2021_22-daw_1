package programa;

import java.util.Scanner;
import clases.Calculos;
import clases.Pintar;
import clases.Starset;

public class Programa {

	public static Scanner input = new Scanner(System.in);

	static final String ADMIN = "";
	static final String PASSW = "";

	/**
	 * 
	 * @author effy
	 * @since jdk1.8.0_191
	 *
	 *        NOTA: La lectura de signos del zodiaco recogera fecha y hora de
	 *        nacimiento y comprueba que estos sean correctos. El programa esta
	 *        preparado para calcular signos lunares del 1927 al 2010, y rechazara
	 *        cualquier otro rango de fecha.
	 */

	/**
	 * Aqui entra la clase principal
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// variables y/o objetos que necesitan inicializarse al principio
		int n = -1;
		String codigo = "";
		Starset starset = new Starset(100);

		// mensaje de bienvenida
		Pintar.inicio();

		// dar valor a datos iniciales con los que probar el codigo
		valoresIniciales(starset);

		/**
		 * ########################################################################
		 * ########################## TERRENO DE PRUEBAS ##########################
		 * ########################################################################
		 */

		// menu inicial (loggin)
		do {
			Pintar.loggin();
			n = Calculos.elegirOpcion();
			switch (n) {
			case 0:
				// mensaje de despedida
				Pintar.despedida();
				input.close();
				break;
			case 1:
				// menu usuario normal
				codigo = menuInvitado(n, codigo, starset);
				break;
			case 2:
				// menu administrador
				menuAdmin(n, starset);
				break;
			default:
				System.out.println("##  opcion no contemplada");
				break;
			}

		} while (n != 0);
	}

	/**
	 * ########################################################################
	 * ######################### METODOS DE PROGRAMA ##########################
	 * ########################################################################
	 */

	/**
	 * 
	 * @param starset
	 */
	private static void valoresIniciales(Starset starset) {
		starset.datosIniciales("Kim", "Possible", "01234567L", "1990-08-15", "09:01");
		starset.datosIniciales("Maria", "La Portuguesa", "10293847J", "2000-02-29", "21:34");
		starset.datosIniciales("Juan", "Arco", "01928374P", "1965-07-02", "11:57");
		starset.datosIniciales("Mario", "NoBro", "56473829C", "1997-03-21", "14:13");
	}

	/**
	 * menuInvitado
	 * 
	 * @param n
	 * @param starset
	 * @return starset
	 */
	private static String menuInvitado(int n, String codigo, Starset starset) {
		do {
			// mostrar menu
			Pintar.menuInvitado();
			n = Calculos.elegirOpcion();
			switch (n) {
			case 1:
				codigo = starset.altaCliente();
				break;
			case 2:
				if (!codigo.equals("")) {
					starset.mostrarUnCliente(codigo);
				} else {
					Pintar.dateDeAlta();
				}
				break;
			case 3:
				codigo = starset.modificarClienteInvitado(n, codigo);
				break;
			default:
			}

		} while (n != 0);
		return codigo;
	}

	/**
	 * validarAdmin
	 * 
	 * @return boolean
	 * 
	 *         Devuelvo true o false segun el usuario y contrasenya introducidos por
	 *         teclado, comprobando si coinciden o no con los declarados como static
	 *         final String al principio del programa.
	 */
	private static boolean validarAdmin() {
		String user = "";
		String passw = "";
		do {
			System.out.println("##  introduce nombre de usuario");
			user = input.nextLine();
			if (user.equals(ADMIN)) {
				System.out.println("##  introduce la contraseņa");
				passw = input.nextLine();
				if (passw.equals(PASSW)) {
					return true;
				} else if (passw.equals("0")) {
					Pintar.retrocediendo();
					break;
				} else {
					System.out.println("##  contraseņa incorrecta");
				}
			} else if (user.equals("0")) {
				Pintar.retrocediendo();
				break;
			} else {
				System.out.println("##  usuario incorrecto");
			}
		} while (!user.equals(ADMIN) && !passw.equals(PASSW));
		return false;
	}

	/**
	 * menuAdmin
	 * 
	 * @param n
	 * @param starset
	 * @return starset
	 */
	private static Starset menuAdmin(int n, Starset starset) {
		boolean userCorrecto = validarAdmin();
		if (userCorrecto) {
			do {
				// mostrar menu
				Pintar.menuAdministrador();
				n = Calculos.elegirOpcion();
				switch (n) {
				case 0:
					Pintar.retrocediendo();
					break;
				case 1:
					// introducir nuevos datos
					starset.altaCliente(n);
					break;
				case 2:
					// mostrar clientes actuales
					starset.listarClientes();
					break;
				case 3:
					// buscar un cliente en especifico
					starset.mostrarUnCliente(n);
					break;
				case 4:
					// modificar datos
					starset.modificarClienteAdmin(n);
					break;
				case 5:
					// eliminar arrays
					starset.eliminarCliente(n);
					break;
				case 6:
					// listar segun un atributo
					starset.listarClientesSegunAtributo(n);
					break;
				default:
					System.out.println("##  opcion no valida, elige un numero del 1 al 6.");
					break;
				}
			} while (n != 0);
		} else {
			System.out.println("##\n##  los datos no pudieron ser validados");
		}
		return starset;
	}

}
