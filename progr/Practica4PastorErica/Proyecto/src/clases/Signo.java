package clases;

import java.time.LocalDate;
import java.time.LocalTime;

public class Signo {

	/**
	 * ##############################################################################
	 * 
	 * La clase CartaAstral contiene todos los atributos final de los signos del
	 * zodiaco y ademas los atributos variables necesarios para el calculo
	 * personalizado de la carta natal. Un cliente podra darse de alta en la
	 * aplicacion y guardar su carta natal para verla y consultarla mas adelante.
	 * 
	 * ##############################################################################
	 */

	/**
	 * Los signos son algo fijo que voy a usar para las lecturas
	 */
	private final String[] SIGNO = { "Aries", "Tauro", "Geminis", "Cancer", "Leo", "Virgo", "Libra", "Escorpio",
			"Sagitario", "Capricornio", "Acuario", "Piscis" };
	private final String[] ELEMENTO = { "Fuego", "Tierra", "Aire", "Agua", "Fuego", "Tierra", "Aire", "Agua", "Fuego",
			"Tierra", "Aire", "Agua", "Fuego", "Tierra", "Aire", "Agua" };
	private final String[] MODALIDAD = { "Cardinal", "Fijo", "Mutable", "Cardinal", "Fijo", "Mutable", "Cardinal",
			"Fijo", "Mutable", "Cardinal", "Fijo", "Mutable" };
	private final String[] PLANETAREGENTE = { "Marte", "Venus", "Mercurio", "Luna", "Sol", "Mercurio", "Venus",
			"Pluton", "Jupiter", "Saturno", "Urano", "Neptuno" };
	private final String[] INTERPRETACIONSOLAR = { Pintar.interpretacionSolar(0), Pintar.interpretacionSolar(1),
			Pintar.interpretacionSolar(2), Pintar.interpretacionSolar(3), Pintar.interpretacionSolar(4),
			Pintar.interpretacionSolar(5), Pintar.interpretacionSolar(6), Pintar.interpretacionSolar(7),
			Pintar.interpretacionSolar(8), Pintar.interpretacionSolar(9), Pintar.interpretacionSolar(10),
			Pintar.interpretacionSolar(11) };
	private final String[] INTERPRETACIONLUNAR = { Pintar.interpretacionLunar(0), Pintar.interpretacionLunar(1),
			Pintar.interpretacionLunar(2), Pintar.interpretacionLunar(3), Pintar.interpretacionLunar(4),
			Pintar.interpretacionLunar(5), Pintar.interpretacionLunar(6), Pintar.interpretacionLunar(7),
			Pintar.interpretacionLunar(8), Pintar.interpretacionLunar(9), Pintar.interpretacionLunar(10),
			Pintar.interpretacionLunar(11) };

	/**
	 * Por otro lado, la fecha y hora de nacimiento,
	 */
	private String solar;
	private String lunar;
	private String ascendente;

	/**
	 * constructor
	 */
	public Signo() {
	}

	/**
	 * Getters para atributos fijos (final). No necesitan Setters.
	 * 
	 * @return signo
	 * @return elemento
	 * @return casa
	 * @return planetaRegente
	 */

	public String[] getSIGNO() {
		return SIGNO;
	}

	public String[] getELEMENTO() {
		return ELEMENTO;
	}

	public String[] getMODALIDAD() {
		return MODALIDAD;
	}

	public String[] getPLANETAREGENTE() {
		return PLANETAREGENTE;
	}

	public String[] getINTERPRETACIONSOLAR() {
		return INTERPRETACIONSOLAR;
	}

	public String[] getINTERPRETACIONLUNAR() {
		return INTERPRETACIONLUNAR;
	}

	/**
	 * Getters y Setters para los atributos no fijos.
	 * 
	 * @return solar
	 * @return lunar
	 * @return ascendente
	 */
	public String getSolar() {
		return solar;
	}

	public void setSolar(String solar) {
		this.solar = solar;
	}

	public String getLunar() {
		return lunar;
	}

	public void setLunar(String lunar) {
		this.lunar = lunar;
	}

	public String getAscendente() {
		return ascendente;
	}

	public void setAscendente(String ascendente) {
		this.ascendente = ascendente;
	}

	/**
	 * ######### METODOS ###########
	 */

	/**
	 * calcularSolar
	 * 
	 * @param fecha
	 */
	void calcularSolar(LocalDate fecha) {
		LocalDate fechaNacimiento = fecha;
		// obtengo el valor numerico del dia de la fecha de nacimiento
		int diaNacimiento = fechaNacimiento.getDayOfYear();
		// si es bisiesto y posterior al 28 de febrero, le resto uno
		if (diaNacimiento % 4 == 0 && diaNacimiento >= 60) {
			diaNacimiento -= 1;
		}
		/*
		 * Compruebo el rango de fechas que corresponden a cada signo
		 */
		// aries 21 marzo - 20 abril # 30 dias
		boolean signo1 = diaNacimiento >= 80 && diaNacimiento <= 110;
		// tauro 21 abril - 20 mayo # 29 dias
		boolean signo2 = diaNacimiento >= 111 && diaNacimiento <= 140;
		// geminis 21 mayo - 21 junio # 31 dias
		boolean signo3 = diaNacimiento >= 141 && diaNacimiento <= 172;
		// cancer 22 junio - 21 julio # 29 dias
		boolean signo4 = diaNacimiento >= 173 && diaNacimiento <= 202;
		// leo 22 julio - 23 agosto # 32 dias
		boolean signo5 = diaNacimiento >= 203 && diaNacimiento <= 235;
		// virgo 24 agosto - 23 septiembre # 30 dias
		boolean signo6 = diaNacimiento >= 236 && diaNacimiento <= 266;
		// libra 24 septiembre - 22 octubre # 28 dias
		boolean signo7 = diaNacimiento >= 267 && diaNacimiento <= 295;
		// escorpio 23 octubre - 22 noviembre # 30 dias
		boolean signo8 = diaNacimiento >= 296 && diaNacimiento <= 326;
		// sagitario 23 noviembre - 21 diciembre # 28 dias
		boolean signo9 = diaNacimiento >= 327 && diaNacimiento <= 355;
		// capricornio 22 diciembre - 20 enero # 30 dias
		boolean signo10 = diaNacimiento >= 356 && diaNacimiento <= 365 || diaNacimiento >= 1 && diaNacimiento <= 19;
		// acuario 21 enero - 19 febrero # 30 dias
		boolean signo11 = diaNacimiento >= 20 && diaNacimiento <= 50;
		// piscis 20 febrero - 20 marzo # 28 dias
		boolean signo12 = diaNacimiento >= 51 && diaNacimiento <= 79;

		/*
		 * Creo un array de todos los boolean para rellenar el signo solar en un simple
		 * bucle for (para no matarme a escribir 12 if(){}
		 */
		boolean[] sSolar = { signo1, signo2, signo3, signo4, signo5, signo6, signo7, signo8, signo9, signo10, signo11,
				signo12 };
		for (int i = 0; i < sSolar.length; i++) {
			if (sSolar[i] == true) {
				this.setSolar(this.SIGNO[i]);
			}
		}
		if (this.SIGNO == null) {
			Pintar.algoHaIdoMal();
		}
	}

	/**
	 * calcularLunar
	 * 
	 * @param SIGNO
	 * 
	 *              La lectura del signo lunar funciona del 1927 al 2010 por falta
	 *              de mas informacion, en rango de otras fechas dara un mensaje de
	 *              error, pero eso ya lo hemos comprobado en un metodo anterior.
	 * 
	 */
	void calcularLunar(LocalDate fecha) {
		int anyo = fecha.getYear();
		int mes = fecha.getMonthValue();
		int dia = fecha.getDayOfMonth();
		// cambio el valor del a�o
		switch (anyo) {
		case 1947:
			anyo = 0;
			break;
		case 1928:
		case 1966:
		case 1985:
			anyo = 1;
			break;
		case 1939:
		case 1958:
			anyo = 2;
			break;
		case 1931:
		case 1977:
		case 1996:
		case 2004:
			anyo = 3;
			break;
		case 1950:
		case 1969:
		case 2007:
			anyo = 4;
			break;
		case 1942:
		case 1988:
		case 1999:
			anyo = 5;
			break;
		case 1961:
		case 1980:
			anyo = 6;
			break;
		case 1934:
		case 1953:
		case 1991:
		case 2010:
			anyo = 7;
			break;
		case 1972:
		case 1983:
		case 2002:
			anyo = 8;
			break;
		case 1945:
		case 1964:
		case 1994:
			anyo = 9;
			break;
		case 1937:
		case 1975:
			anyo = 10;
			break;
		case 1956:
		case 1967:
		case 1986:
			anyo = 11;
			break;
		case 1929:
		case 1948:
		case 1959:
		case 1978:
		case 2005:
			anyo = 12;
			break;
		case 1940:
		case 1951:
		case 1970:
		case 1997:
			anyo = 13;
			break;
		case 1932:
		case 1943:
		case 1989:
			anyo = 14;
			break;
		case 1962:
		case 2008:
			anyo = 15;
			break;
		case 1935:
		case 1981:
			anyo = 16;
			break;
		case 1927:
		case 1954:
		case 1973:
		case 2000:
			anyo = 17;
			break;
		case 1946:
		case 1992:
			anyo = 18;
			break;
		case 1965:
		case 1984:
		case 2003:
			anyo = 19;
			break;
		case 1938:
		case 1957:
		case 1995:
			anyo = 20;
			break;
		case 1930:
		case 1976:
		case 1987:
			anyo = 21;
			break;
		case 1949:
		case 1968:
		case 1998:
		case 2006:
			anyo = 22;
			break;
		case 1941:
		case 1979:
			anyo = 23;
			break;
		case 1960:
		case 1971:
		case 1990:
			anyo = 24;
			break;
		case 1933:
		case 1952:
		case 1963:
		case 1982:
		case 2009:
			anyo = 25;
			break;
		case 1944:
		case 1955:
		case 1974:
		case 2001:
			anyo = 26;
			break;
		case 1936:
		case 1993:
			anyo = 27;
			break;
		default:
			anyo = -1;
			break;
		}
		// si el a�o se ha calculado correctamente, cambio el valor del mes
		switch (mes) {
		case 1:
			mes = 0;
			break;
		case 2:
		case 3:
			mes = 4;
			break;
		case 4:
			mes = 8;
			break;
		case 5:
			mes = 11;
			break;
		case 6:
			mes = 14;
			break;
		case 7:
			mes = 17;
			break;
		case 8:
			mes = 21;
			break;
		case 9:
			mes = 24;
			break;
		case 10:
			mes = 27;
			break;
		case 11:
			mes = 3;
			break;
		case 12:
			mes = 6;
			break;
		default:
			mes = -1;
			break;
		}
		// los sumo al valor del dia
		int suma = anyo + mes + dia;
		// dejo suma en un numero menor de 27
		if (suma >= 29 && suma <= 54) {
			suma -= 27;
		} else if (suma >= 55 && suma <= 81) {
			suma -= 55;
		} else if (suma > 81) {
			suma -= 82;
		}
		// saco el valor de la suma total
		switch (suma) {
		case 0:
		case 1:
		case 27:
		case 28:
			suma = 0;
			break;
		case 2:
		case 3:
		case 4:
			suma = 1;
			break;
		case 5:
		case 6:
			suma = 2;
			break;
		case 7:
		case 8:
			suma = 3;
			break;
		case 9:
		case 10:
			suma = 4;
			break;
		case 11:
		case 12:
		case 13:
			suma = 5;
			break;
		case 14:
		case 15:
			suma = 6;
			break;
		case 16:
		case 17:
			suma = 7;
			break;
		case 18:
		case 19:
			suma = 8;
			break;
		case 20:
		case 21:
		case 22:
			suma = 9;
			break;
		case 23:
		case 24:
			suma = 10;
			break;
		case 25:
		case 26:
			suma = 11;
			break;
		default:
			suma = -1;
			break;
		}
		// compruebo quesuma no valga un valor distinto entre 0 y 11
		if (suma >= 0 && suma <= 11) {
			for (int i = 0; i < 12; i++) {
				if (suma == i) {
					this.setLunar(this.SIGNO[i]);
					break;
				}
			}
		} else {
			Pintar.algoHaIdoMal();
		}
	}

	/**
	 * calcularAscendente
	 * 
	 * @param SIGNO
	 * 
	 *              Este metodo usa dos bucles for(){} anidados para comparar el
	 *              parametro i con el signo solar del objeto entrante con los
	 *              signos del array fijo (final) y luego recorrer las horas del dia
	 *              segun este solar.
	 * 
	 */
	void calcularAscendente(LocalTime h) {
		int hora = h.getHour();
		for (int i = 0; i < 12; i++) {
			if (this.getSolar().equalsIgnoreCase(this.SIGNO[i])) {
				for (int j = i, k = 6; j < i + 12; j++, k += 2) {
					if (k == 24) {
						k = 0;
					}
					if (hora == k || hora == (k - 1)) {
						if (j > 11) {
							j -= 12;
						}
						this.setAscendente(this.SIGNO[j]);
						break;
					}
				}
			}
		}
		if (this.getAscendente() == null) {
			Pintar.algoHaIdoMal();
		}
	}

	boolean signoExiste(String signo) {
		for (int i = 0; i < this.SIGNO.length; i++) {
			if (signo.equalsIgnoreCase(this.SIGNO[i])) {
				return true;
			}
		}
		return false;
	}

}
