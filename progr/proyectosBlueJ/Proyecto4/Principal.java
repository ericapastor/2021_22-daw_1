public class Principal
{
    public static void main(String[] args) {
        // creo objeto
        MisSumas objetoSuma = new MisSumas();
        System.out.println("ATRIBUTOS");
        System.out.println(objetoSuma.numInt1);
        System.out.println(objetoSuma.numInt2);
        System.out.println(objetoSuma.numDouble1);
        System.out.println(objetoSuma.numDouble2);
        System.out.println("METODOS");
        System.out.println(objetoSuma.suma(3,4));
        System.out.println(objetoSuma.suma(3.4,5.6));
    }
}