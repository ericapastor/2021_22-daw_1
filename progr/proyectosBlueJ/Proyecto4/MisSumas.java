public class MisSumas
{
    // atributos
    int numInt1;
    int numInt2;
    double numDouble1;
    double numDouble2;
    
    // constructor:
    // es un metodo
    // se llama como la clase
    // no tiene devolucion
    public MisSumas() {
        numInt1 = 2;
        numInt2 = 3;
        numDouble1 = 2.3;
        numDouble2 = 4.5;
    }
    
    // metodos:
    // sobrecargados
    // se llaman igual pero sus parametros son distintos
    public int suma(int x, int y) {
        return x + y;
    }
    public double suma(double x, double y) {
        return x + y;
    }
}