public class Conversion
{
    public void eurosPesetas() {
        double pesetas = 325;
        double euros;
        System.out.println("La cantidad en pesetas es " + pesetas + ".");
        euros = pesetas / 166.386;
        System.out.println("La cantidad en euros es " + euros + ".");
    }
    public void pesetasEuros() {
        double euros = 3.85;
        double pesetas;
        System.out.println("La cantidad en euros es " + euros + ".");
        pesetas = euros * 166.386;
        System.out.println("La cantidad en pesetas es " + pesetas + ".");
    }
}