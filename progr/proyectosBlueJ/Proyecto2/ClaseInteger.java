public class ClaseInteger
{
    public void crearInteger() {
        
        System.out.println("Creo un numero Int y lo muestro");
        int x = 3;
        System.out.println("El numero es "+x);
        
        System.out.println("Creo un String y lo muestro");
        String cadena = "Hola";
        System.out.println("La cadena es "+cadena);
        
        // clase objeto = new constructor
        // uso el constructor de la clase Integer
        System.out.println("Creo un Integer y lo muestro");
        Integer miInteger = new Integer(3);
        System.out.println("El Integer es ");
    }
}
