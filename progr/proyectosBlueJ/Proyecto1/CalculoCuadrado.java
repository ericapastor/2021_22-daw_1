public class CalculoCuadrado
{
    public static void main(String[]args) {
        /* Creo objeto
        clase nombreObjeto = new constructor
        el constructor por defecto cuando yo
        no he creado ninguno es nombreClase()*/
        SinMetodoMain smm = new SinMetodoMain();
        /* cuando un metodo es static no necesita
           crear objetos, se le llama directamente
           como clase.metodo */
        int resultado = smm.calcularCuadrado(15);
        System.out.print(resultado);
        Mensaje mensajito = new Mensaje();
        mensajito.mostrarMensaje();
    }
}