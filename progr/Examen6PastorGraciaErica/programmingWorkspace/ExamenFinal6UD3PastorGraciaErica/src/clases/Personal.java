package clases;

import java.io.Serializable;

public abstract class Personal implements Comparable<Personal>, Serializable {

	private static final long serialVersionUID = 1L;

	final int salarioBase = 750;

	String dni;
	String nombre;
	int salario;
	int incentivo;

	public Personal(String dni, String nombre) {
		this.dni = dni;
		this.nombre = nombre;
	}

	abstract int calcularSalario();

	@Override
	public String toString() {
		return "Personal [dni=" + dni + ", nombre=" + nombre + "salarioBase=" + salarioBase + ", salario=" + salario
				+ ", incentivo=" + incentivo + "]";
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}

	public int getIncentivo() {
		return incentivo;
	}

	public void setIncentivo(int incentivo) {
		this.incentivo = incentivo;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

}
