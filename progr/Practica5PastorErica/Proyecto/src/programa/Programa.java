package programa;

import java.util.Scanner;

import gestiones.GestionProductos;

/**
 * Clase Principal. El main tiene una sola linea que llama al metodo programa(),
 * el cual da de alta un objeto de gestion y unos productos base.
 * 
 * Despues va llamando a otros metodos estaticos de esta clase segun la opcion
 * que se elija en el menu.
 * 
 * @author Effy
 *
 */

public class Programa {

	/**
	 * Scanner unico para todo el programa. Es publico para que sea accesible desde
	 * otras clases.
	 */
	static public Scanner input = new Scanner(System.in);

	/**
	 * main
	 * 
	 * @param String[] args
	 */
	public static void main(String[] args) {
		programa();
	}

	/**
	 * Pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se termina el programa.
	 * Del 1 al 6 se reconduce a otros metodos de esta clase.
	 */
	private static void programa() {
		Pintar.saludo();
		GestionProductos gestionP = new GestionProductos();
		gestionP.altaProductosBase();
		int contadorLectura = 0;
		while (true) {
			Pintar.menuInicio();
			String opcion = pedirDatosString();
			switch (opcion) {
			case "0":
				Pintar.despedida();
				input.close();
				break;
			case "1":
				opcion1(gestionP);
				break;
			case "2":
				opcion2(gestionP);
				break;
			case "3":
				opcion3(gestionP);
				break;
			case "4":
				opcion4(gestionP);
				break;
			case "5":
				opcion5(gestionP);
				break;
			case "6":
				contadorLectura = opcion6(gestionP, contadorLectura);
				break;
			default:
				Pintar.opcionNoValida();
				break;
			}
			if (opcion.equals("0")) {
				break;
			}
		}
	}

	/**
	 * Se pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se vuelve al menu
	 * principal. Del 1 al 2 se reconduce a metodos de Gestion.
	 */
	private static void opcion1(GestionProductos gestion) {
		while (true) {
			Pintar.menuAlta();
			String opcion = pedirDatosString();
			switch (opcion) {
			case "0":
				Pintar.retrocediendo();
				break;
			case "1":
				gestion.pedirDatosVelas();
				break;
			case "2":
				gestion.pedirDatosCristal();
				break;
			default:
				Pintar.opcionNoValida();
				break;
			}
			if (opcion.equals("0")) {
				break;
			}
		}
	}

	/**
	 * Se pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se vuelve al menu
	 * principal. Del 1 al 4 se reconduce a metodos de Gestion.
	 */
	private static void opcion2(GestionProductos gestion) {
		while (true) {
			Pintar.menuBuscar();
			String opcion = pedirDatosString();
			switch (opcion) {
			case "0":
				Pintar.retrocediendo();
				break;
			case "1":
				gestion.buscarCarta();
				break;
			case "2":
				gestion.buscarVela();
				break;
			case "3":
				gestion.buscarCristal();
				break;
			case "4":
				gestion.buscarProducto();
				break;
			default:
				Pintar.opcionNoValida();
				break;
			}
			if (opcion.equals("0")) {
				break;
			}
		}
	}

	/**
	 * Se pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se vuelve al menu
	 * principal. Del 1 al 3 se reconduce a metodos de Gestion.
	 */
	private static void opcion3(GestionProductos gestion) {
		while (true) {
			Pintar.menuListar();
			String opcion = pedirDatosString();
			switch (opcion) {
			case "0":
				Pintar.retrocediendo();
				break;
			case "1":
				gestion.listarBarajas();
				break;
			case "2":
				gestion.listarVelas();
				break;
			case "3":
				gestion.listarCristales();
				break;
			default:
				Pintar.opcionNoValida();
				break;
			}
			if (opcion.equals("0")) {
				break;
			}
		}
	}

	/**
	 * Se pide el codigo del producto a modificar.
	 */
	private static void opcion4(GestionProductos gestion) {
		Pintar.menuModificar();
		gestion.modificarProducto();
	}

	/**
	 * Se pide el codigo del producto a borrar.
	 */
	private static void opcion5(GestionProductos gestion) {
		Pintar.menuBorrar();
		gestion.borrarProducto();
	}

	/**
	 * Recibe un contador del menu principal y devuelve un numero. Si el contador
	 * que recibe es superior a 0, no permite acceder a la lectura.
	 * 
	 * Si el contador es 0, redirige al metodo de lectura de la clase baraja.
	 * 
	 * Anteriormente tenia aqui un menu para elegir la baraja con la que quieres la
	 * tirada, pero no me dio tiempo a rellenar una segunda baraja asi que lo deje
	 * sin opciones, redirigiendote a la unica de la que las cartas estan rellenas.
	 */
	private static int opcion6(GestionProductos gestion, int c) {
		if (c > 0) {
			Pintar.noMasLecturas();
		} else {
			Pintar.menuTirada();
			gestion.getBarajas().get(0).lecturaTresCartas();

		}
		return 1;
	}

	/**
	 * Pedir un dato String, comprobando que se escriba algo. Devuelve lo que sea
	 * que se escriba.
	 * 
	 * @return String datos
	 */
	public static String pedirDatosString() {
		String datos = "";
		while (datos.equals("")) {
			datos = input.nextLine();
			if (!datos.equals("")) {
				break;
			} else {
				Pintar.noHasEscritoNada();
			}
		}
		return datos;
	}

	/**
	 * Pedir un dato int, con comprobacion de excepciones.
	 * 
	 * @return int datos
	 */
	public static int pedirDatosInt() {
		int datos = -1;
		while (datos == -1) {
			try {
				datos = input.nextInt();
				input.nextLine();
				if (datos == -1) {
					Pintar.noHasEscritoNada();
				}
				return datos;
			} catch (Exception InputMismatchException) {
				Pintar.valorIncorrecto();
				Pintar.noDecimales();
				input.nextLine();
			}
		}
		return datos;
	}

	/**
	 * Pedir un dato float, con comprobacion de excepciones.
	 * 
	 * @return int datos
	 */
	public static float pedirDatosFloat() {
		float datos = -1F;
		while (datos == -1F) {
			try {
				datos = input.nextFloat();
				input.nextLine();
				if (datos == -1) {
					Pintar.noHasEscritoNada();
				}
				return datos;
			} catch (Exception InputMismatchException) {
				Pintar.valorIncorrecto();
				Pintar.decimales();
				input.nextLine();
			}
		}
		return datos;
	}

	/**
	 * Pide una cadena de texto hasta que se introduzca si o no, devolviendo true o
	 * false respectivamente.
	 * 
	 * @return boolean bool
	 */
	public static boolean pedirDatosBoolean() {
		String bool = "";
		do {
			bool = pedirDatosString();
			bool = bool.toUpperCase();
			switch (bool) {
			case "SI":
				return true;
			case "NO":
				return false;
			default:
				Pintar.booleanSINO();
			}
		} while (!bool.equalsIgnoreCase("si") && !bool.equalsIgnoreCase("no"));
		return false;
	}

	/**
	 * Recibe un frase, capitaliza la primera letra, y pone en minusculas las
	 * siguientes, devolviendo asi una frase.
	 * 
	 * @param String frase
	 * @return String frase
	 */
	public static String toSentenceCase(String s) {
		return s.toUpperCase().charAt(0) + s.toLowerCase().substring(1);
	}

}
