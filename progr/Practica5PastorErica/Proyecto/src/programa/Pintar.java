package programa;

/**
 * La clase Pintar es estatica y me sirve dejar el codigo mas limpio
 * 
 * @author Effy
 *
 */

public class Pintar {

	/**
	 * Pintar inicio
	 */
	static void saludo() {
		System.out.println("\n\n");
		System.out.println("|                                    *                       *  |");
		System.out.println("|        *                                  *             *     |");
		System.out.println("|                 *                                *            |");
		System.out.println("|           *                                         *         |");
		System.out.println("|                    *                                       *  |");
		System.out.println("|  *       *                                       *            |");
		System.out.println("|                            #  #  #                            |");
		System.out.println("|                        #  # #  #     #                *  *    |");
		System.out.println("|    *                #  #  #  #         #                      |");
		System.out.println("|                   #  #  #  #             #                    |");
		System.out.println("|                  #  #  #  #               #                   |");
		System.out.println("|                 #  #  #  #   Bienvenido    #            *     |");
		System.out.println("|                 #  #  #  #   a starset*    #                  |");
		System.out.println("| *               #  #  #  #                 #           *      |");
		System.out.println("|                  #  #  #  #               #               *   |");
		System.out.println("|                   #  #  #  #             #       *            |");
		System.out.println("|                     #  #  #  #         #                      |");
		System.out.println("|        *               #  # #  #     #             *          |");
		System.out.println("|     *                      #  #  #           *             *  |");
		System.out.println("|                                                               |");
		System.out.println("|     *      *                                    *             |");
		System.out.println("|         *        *                            *      *    **  |");
		System.out.println("|                               *        **                *    |");
		System.out.println("|       *                                       *             * |");
		System.out.println("|_______________________________________________________________|");
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
	}

	/**
	 * Pintar despedida
	 */
	static void despedida() {
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|_______________________________________________________________|");
		System.out.println("|                                    *                       *  |");
		System.out.println("|        *                                  *             *     |");
		System.out.println("|                 *                                *            |");
		System.out.println("|           *                                         *         |");
		System.out.println("|                    *                                       *  |");
		System.out.println("|  *       *                                       *            |");
		System.out.println("|                            #  #  #                            |");
		System.out.println("|                        #  # #  #     #                *  *    |");
		System.out.println("|    *                #  #  #  #         #                      |");
		System.out.println("|                   #  #  #  #             #                    |");
		System.out.println("|                  #  #  #  #               #                   |");
		System.out.println("|                 #  #  #  #   Gracias,      #            *     |");
		System.out.println("|                 #  #  #  #   hasta         #                  |");
		System.out.println("| *               #  #  #  #   pronto        #           *      |");
		System.out.println("|                  #  #  #  #               #               *   |");
		System.out.println("|                   #  #  #  #             #       *            |");
		System.out.println("|                     #  #  #  #         #                      |");
		System.out.println("|        *               #  # #  #     #             *          |");
		System.out.println("|     *                      #  #  #           *             *  |");
		System.out.println("|                                                               |");
		System.out.println("|     *      *                                    *             |");
		System.out.println("|         *        *                            *      *    **  |");
		System.out.println("|                               *        **                *    |");
		System.out.println("|       *                                       *             * |");
	}

	/**
	 * Mostrar menu de loggeo
	 */
	static void menuInicio() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *         MENU DE INICIO                         |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           *****************************************           |");
		System.out.println("|                                            *      *           |");
		System.out.println("|                                             *                 |");
		System.out.println("|           1 - Dar de alta productos                           |");
		System.out.println("|           2 - Buscar un producto                              |");
		System.out.println("|           3 - Listar productos                                |");
		System.out.println("|           4 - Modificar nombre de un producto                 |");
		System.out.println("|           5 - Borrar productos                                |");
		System.out.println("|           6 - Servicio semanal: tirada de cartas              |");
		System.out.println("|                                                               |");
		System.out.println("|    *                                                          |");
		System.out.println("|           *****************************************           |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres terminar el programa, pulsa 0            |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
		System.out.println("|                                                        *      |");
	}

	/**
	 * Mostrar mensaje de opcion no contemplada
	 */
	static void opcionNoValida() {
		System.out.println("|                                              *                |");
		System.out.println("|           Opcion no contemplada                               |");
		System.out.println("|          *                                                    |");
	}

	/**
	 * Mostrar mensaje de retrocediendo
	 */
	public static void retrocediendo() {
		System.out.println("|                                                 *             |");
		System.out.println("|           Retrocediendo...                         *          |");
		System.out.println("|      *                                       *                |");
	}

	/**
	 * Mostrar mensaje de "no has escrito nada"
	 */
	static void noHasEscritoNada() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No has escrito nada                         *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para indicar que un valor es incorrecto
	 */
	public static void valorIncorrecto() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Has introducido un valor incorrecto         *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje que pide no introducir decimales
	 */
	static void noDecimales() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Recuerda no introducir decimales            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para indicar que un valor introducido deberia ser numerico
	 */
	static void decimales() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Recuerda separar los decimales con          *       |");
		System.out.println("|           una coma, nunca con un punto                        |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para indicar que se introduzca solamente SI o NO
	 */

	static void booleanSINO() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Por favor escribe solamente SI o NO         *       |");
		System.out.println("|  *                                                  *         |");
	}

	public static void intentaloDeNuevo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Intentemoslo de nuevo:                      *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar menu alta productos
	 */
	static void menuAlta() {
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Dar de alta velas                               |");
		System.out.println("|           2 - Dar de alta cristales                           |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
		System.out.println("|                                                        *      |");
	}

	/**
	 * Mostrar mensaje de alta de productos
	 */
	public static void datosAltas() {
		System.out.println("|                                                        *      |");
		System.out.println("|                                                               |");
		System.out.println("|           Bienvenido al proceso de alta. Se te pe-            |");
		System.out.println("|           dira una confirmacion antes de cada alta.       *   |");
		System.out.println("|                                                               |");
		System.out.println("|           Si pulsas 0 volveras hacia atras.                   |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Mensaje para informar de la no alta de un producto
	 */
	public static void altaCancelada() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           El alta ha quedado cancelada                *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Preguntar si se quiere repetir otro alta de vela
	 */
	public static void repetirAltaVela() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Quieres introducir otra vela [SI/NO]        *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Preguntar si se quiere repetir otro alta de cristal
	 */
	public static void repetirAltaCristal() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Quieres introducir otro cristal [SI/NO]     *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar datos instrucidos de vela
	 */
	public static void confirmacionVela(String aroma, int unidades, float precio, float peso, float diametro,
			float altura) {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Estos son los datos introducidos:           *       |");
		System.out.println("|                                                               |");
		System.out.println("|           * Aroma: " + aroma);
		System.out.println("|           * Unidades: " + unidades);
		System.out.println("|           * Precio bruto: " + precio + "euros");
		System.out.println("|           * Peso: " + peso + "g");
		System.out.println("|           * Diametro: " + diametro + "cm");
		System.out.println("|           * Altura: " + altura + "cm");
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Confirme si son correctos: [SI/NO]                  |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mostrar datos instrucidos de cristal
	 */
	public static void confirmacionCristal(String nombre, float peso, float precio, boolean isPulido, String forma) {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Estos son los datos introducidos:           *       |");
		System.out.println("|                                                               |");
		System.out.println("|           * Nombre: " + nombre);
		System.out.println("|           * Peso: " + peso);
		System.out.println("|           * Precio bruto: " + precio);
		System.out.println("|           * Esta pulido: " + (isPulido ? "Si" : "No"));
		System.out.println("|           * Forma: " + forma);
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Confirme si son correctos: [SI/NO]                  |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mostrar mensaje de fin de proceso de altas
	 */
	public static void altasFinalizadas() {
		System.out.println("|                                                        *      |");
		System.out.println("|                                                               |");
		System.out.println("|           El proceso de alta ha terminado. Se te              |");
		System.out.println("|           redirigira al menu principal.                   *   |");
		System.out.println("|    *                                                          |");
		System.out.println("|        *                                                      |");
	}

	/**
	 * Mensaje pedir precio bruto
	 */
	public static void pedirPrecio() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el precio bruto del producto      *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir peso de un producto
	 */
	public static void pedirPeso() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el peso del producto              *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Ir mostrando por que vela vamos al ir insertando nuevas
	 */
	public static void altaVelaNum(int n) {
		System.out.println("|      *                                                        |");
		System.out.println("|           Vamos con la vela numero " + (n + 1) + "                  *       |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mensaje pedir aroma de la vela
	 */
	public static void pedirAromaVela() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el aroma de la vela               *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje que muestra las opciones de aromas
	 */
	public static void opcionesAromas() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Las opciones de aromas que hay son:         *       |");
		System.out.println("|           * Vainilla                                          |");
		System.out.println("|           * Limon                                             |");
		System.out.println("|           * Frutos rojos                                      |");
		System.out.println("|           * Canela                                            |");
		System.out.println("|           * Ninguno                                           |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir unidades
	 */
	public static void pedirUnidadesVela() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce cuantas unidades vienen           *       |");
		System.out.println("|  *        en el paquete de velas                    *         |");
		System.out.println("|                                       *                       |");
	}

	/**
	 * Mensaje pedir diametro de una vela
	 */
	public static void pedirDiametro() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el diametro de la vela            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir altura de una vela
	 */
	public static void pedirAltura() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce la altura de la vela              *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Ir mostrando por que cristal vamos al ir insertando nuevos
	 */
	public static void altaCristalNum(int n) {
		System.out.println("|      *                                                        |");
		System.out.println("|           Vamos con el cristal numero " + (n + 1) + "                *      |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mensaje pedir nombre
	 */
	public static void pedirNombreCristal() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el nombre del cristal             *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje preguntar si un cristal esta pulido o no
	 */
	public static void pedirPulido() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Di si el cristal esta pulido: [SI/NO]       *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir forma del cristal
	 */
	public static void pedirForma() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce la forma del cristal              *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para pedir nombre de una carta
	 */
	public static void pedirNombreCarta() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el nombre de la carta             *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para pedir el codigo de un producto en el menu de busqueda por codigo
	 */
	public static void pedirCodigo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el codigo del producto            *       |");
		System.out.println("|           Recuerda que tiene seis caracteres:                 |");
		System.out.println("|           Tres letras (BAR, VEL o CRI) y                      |");
		System.out.println("|           tres numeros                                        |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para pedir el nuevo nombre de un producto en el menu modificar
	 */
	public static void nuevoNombre() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el nuevo nombre que quie-         *       |");
		System.out.println("|           res darle al producto:                              |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para comunicar que el nombre ha sido modificado
	 */
	public static void nombreModificado() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           El nombre ha sido modificado                *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para informar de que las barajas no se pueden borrar
	 */
	public static void noSePuedenBorrarBarajas() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No se pueden borrar las barajas             *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar menu buscar producto
	 */
	static void menuBuscar() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *      MENU BUSCAR                               |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Buscar cartas por nombre                        |");
		System.out.println("|           2 - Buscar velas por aroma                          |");
		System.out.println("|           3 - Buscar cristales por forma                      |");
		System.out.println("|           4 - Buscar producto por codigo                      |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
		System.out.println("|                                                        *      |");
	}

	/**
	 * Menu listar productos
	 */
	static void menuListar() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *          MENU LISTAR                           |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Listar barajas                                  |");
		System.out.println("|           2 - Listar velas                                    |");
		System.out.println("|           3 - Listar cristales                                |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
		System.out.println("|                                                        *      |");
	}

	/**
	 * Mostrar menu modificar
	 */
	static void menuModificar() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Escribe el codigo del producto              *       |");
		System.out.println("|           que quieres modificar                       *       |");
		System.out.println("|  *                                                  *         |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
		System.out.println("|                                                        *      |");
	}

	/**
	 * Escribir el menu de borrar
	 */
	static void menuBorrar() {
		System.out.println("|      *                                           *            |");
		System.out.println("|   *       Escribe el codigo del producto              *       |");
		System.out.println("|           que quieres eliminar                                |");
		System.out.println("|  *                                                  *         |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
		System.out.println("|                                                        *      |");
	}

	/**
	 * Escribir el menu de servicio tirada tres cartas
	 */
	static void menuTirada() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Esta es la baraja disponible para           *       |");
		System.out.println("| *         la tirada: Mazo de los Espiritus               *    |");
		System.out.println("|    *      Animales Salvajes Desconocidos.           *         |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Esta es la lectura que ha salido:                   |");
		System.out.println("|                                                        *      |");
	}

	/**
	 * Escribir que un numero de carta no existe
	 */
	public static void cartaNoExiste() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           La carta no existe                          *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Escribir que la baraja no ha sido encontrada
	 */
	public static void barajaNoExiste() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No se ha encontrado la baraja               *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Escribir que no se ha encontrado ningun producto con el codigo introducido
	 */
	public static void productoNoExiste() {
		System.out.println("|      *                                                        |");
		System.out.println("|           No se ha encontrado ningun producto    *            |");
		System.out.println("|           con ese codigo                              *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Escribir que no se encontraron coincidencias
	 */
	public static void noHayCoincidencias() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No se han encontrado coincidencias          *       |");
		System.out.println("|  *                                                  *         |");
	}

	public static void esoEsTodo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Esas son todas las coincidencias            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Escribir que se ha llevado a cabo el borrado correctamente
	 */
	public static void borradoCorrecto() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Se ha eliminado el producto correctamente   *       |");
		System.out.println("|  *                                                  *         |");

	}

	/**
	 * Mostrar mensaje para luego mostrar la primera carta en una lectura
	 */
	public static void cartaPasado() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Donde solias estar:                         *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje para luego mostrar la segunda carta en una lectura
	 */
	public static void cartaPresente() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Donde estas ahora:                          *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje para luego mostrar la tercera carta en una lectura
	 */
	public static void cartaFuturo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Donde estaras pronto:                       *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje indicando que solamente se admite una lectura al dia
	 */
	public static void noMasLecturas() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Solo puedes acceder a una lectura           *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje indicando que algo ha salido mal en la lectura
	 */
	public static void algoFueMal() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Algo ha salido mal en la lectura            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje indicando que no hay cartas en la baraja
	 */
	public static void noHayCartas() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No hay cartas en esta baraja todavia        *       |");
		System.out.println("|  *                                                  *         |");
	}

	public static void error() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Vaya, parece que ha habido un error...       *      |");
		System.out.println("|  *                                                  *         |");
	}

}
