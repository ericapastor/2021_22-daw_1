package productos;

/**
 * Cristal tiene un static final para definir el numero de cristales base que se
 * van a dar de alta, y un contador estatico numCristales que servira para auto
 * calcular el codigo de producto.
 * 
 * Ademas tiene un atributo boolean que se refiere a si el cristal esta pulido o
 * no, y otro String que se refiere a la forma del cristal.
 * 
 * Es una clase final porque con ella termina la herencia (no tiene hijos).
 * 
 * @author erica
 *
 */

public final class Cristal extends Producto {

	public static final int NUM_CRI_BASE = 5;
	public static int numCristales = 0;
	private boolean pulido;
	private String forma;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor vacio
	 */
	public Cristal() {
		super();
		iva = 6.66F;
	}

	/**
	 * Constructor con todos los parametros que usa el constructor de tres atributos
	 * de la clase Producto.
	 * 
	 * @param String  nombre
	 * @param float   peso
	 * @param float   precio
	 * @param boolean pulido
	 * @param String  forma
	 */
	public Cristal(String nombre, float peso, float precio, boolean pulido, String forma) {
		super(nombre, peso, precio);
		this.pulido = pulido;
		this.forma = forma;
		iva = 6.66F;
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return boolean isPulido
	 */
	public boolean isPulido() {
		return pulido;
	}

	/**
	 * @param boolean pulido
	 */
	public void setPulido(boolean pulido) {
		this.pulido = pulido;
	}

	/**
	 * @return String forma
	 */
	public String getForma() {
		return forma;
	}

	/**
	 * @param String forma
	 */
	public void setForma(String forma) {
		this.forma = forma;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return super.toString() + "|           Pulido: " + (this.pulido ? "Si" : "No") + "\n|           Forma: "
				+ this.forma + "\n|           IVA aplicado: " + this.iva + "%\n|           Descuento aplicado: "
				+ this.precio.getDescuento() + "\n|";
	}

	/**
	 * Recibe un numero y en base a este da de alta una serie de cristales
	 * predefinidos. Estos productos los uso para tener algunos de base al
	 * inicializar el programa.
	 * 
	 * @param int n
	 */
	@Override
	public void altaProducto(int n) {
		switch (n) {
		case 0:
			this.setNombre("Obsidiana");
			this.setPeso(3.6F);
			this.precio.setPrecioBruto(4.9F);
			this.setPulido(true);
			this.setForma("Obelisco");
			break;
		case 1:
			this.setNombre("Cuarzo rosa");
			this.setPeso(0.9F);
			this.precio.setPrecioBruto(1.00F);
			this.setPulido(false);
			this.setForma("Ovalada irregular");
			break;
		case 2:
			this.setNombre("Agata");
			this.setPeso(1.5F);
			this.precio.setPrecioBruto(9.5F);
			this.setPulido(true);
			this.setForma("Ovalo perfecto");
			break;
		case 3:
			this.setNombre("Agua marina");
			this.setPeso(4.3F);
			this.precio.setPrecioBruto(5F);
			this.setPulido(false);
			this.setForma("Diamante irregular");
			break;
		case 4:
			this.setNombre("Ojo de tigre");
			this.setPeso(10.18F);
			this.precio.setPrecioBruto(15.00F);
			this.setPulido(true);
			this.setForma("Corazon");
			break;
		default:
			break;
		}
	}

	/**
	 * Calcular el precio de un producto dependiendo del descuento que se le
	 * aplique. Si no coincide con ninguno, solo se le aplica el IVA. Para baraja el
	 * descuento de primavera es del 1.2%, el de Navidad del 3.3% y el de San
	 * Valentin del 2.78%.
	 */
	@Override
	public void calcularPrecioNeto() {
		float precioB = this.precio.getPrecioBruto();
		float precioN;
		switch (this.precio.getDescuento()) {
		case PRIMAVERA:
			precioN = precioB * (1 - iva / 100) * (1 + 1.2F / 100);
		case NAVIDAD:
			precioN = precioB * (1 - iva / 100) * (1 + 3.3F / 100);
		case SAN_VALENTIN:
			precioN = precioB * (1 - iva / 100) * (1 + 2.78F / 100);
		default:
			precioN = precioB * (1 - iva / 100);
		}
		this.precio.setPrecioNeto(precioN);
	}

	/**
	 * Asignar el codigo de producto automaticamente segun el numero de cristales
	 * existente.
	 */
	@Override
	public void calcularCodigo() {
		if (numCristales < 10) {
			this.setCodigo("CRI00" + numCristales);
		} else if (numCristales >= 10 && numCristales < 100) {
			this.setCodigo("CRI0" + numCristales);
		} else if (numCristales >= 100 && numCristales < 1000) {
			this.setCodigo("CRI" + numCristales);
		}
	}

	/**
	 * Suma el numero que entre (sea positivo o negativo) sin que pase de los
	 * numeros especulados en el metodo calcularCodigo (de 0 a 1000).
	 * 
	 * @see calcularCodigo()
	 */
	@Override
	public void controlContadorProductos(int n) {
		if (numCristales + n > 1000) {
			numCristales = 0;
		} else if (numCristales + n < 0) {
			numCristales = 0;
		} else {
			numCristales += n;
		}
	}

}
