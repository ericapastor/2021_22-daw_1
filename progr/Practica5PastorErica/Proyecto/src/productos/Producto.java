package productos;

/**
 * Producto es una clase abstracta que inicia una herencia de producto: Barajas,
 * Velas y Cristales. De aqui estas clases heredan los atributos de nombre,
 * peso, precio, iva y codigo.
 * 
 * El precio sera un objeto de clase Precio con sus propios atributos.
 * 
 * El iva sera redefinido (sobreescrito) en cada constructor de cada hijo. Su
 * valor por defecto es del 21%.
 * 
 * El codigo sera auto calculado cada vez que se haga un alta de un producto
 * segun el tipo de producto y el contador de productos propio de cada clase.
 * 
 * @author erica
 *
 */

public abstract class Producto {

	String nombre;
	float peso;
	Precio precio;
	float iva = 21F;
	String codigo;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor vacio que instancia el atributo Precio precio vacio, utilizando
	 * el constructor de Precio que no recibe atributos. Sera utilizado en el alta
	 * de barajas.
	 * 
	 * @see Public Precio()
	 */
	public Producto() {
		this.precio = new Precio();
	}

	/**
	 * Constructor que recibe dos atributos, instanciando precio con el constructor
	 * que recibe un atributo. Sera utilizado en el alta de velas, dado que las
	 * velas tienen su propio nombre asignado segun el aroma.
	 * 
	 * @param String peso
	 * @param float  precio
	 * 
	 * @see Public Precio(float precio)
	 * 
	 */
	public Producto(float peso, float precio) {
		this.peso = peso;
		this.precio = new Precio(precio);
	}

	/**
	 * Constructor que recibe tres atributos, instanciando precio con el constructor
	 * que recibe un atributo. Sera utilizado en el alta de cristales.
	 * 
	 * @param String nombre
	 * @param float  peso
	 * @param float  precioBruto
	 * 
	 * @see Public Precio(float precio)
	 * 
	 */
	public Producto(String nombre, float peso, float precioBruto) {
		this.nombre = nombre;
		this.peso = peso;
		this.precio = new Precio(precioBruto);
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return String nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param String nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return float peso
	 */
	public float getPeso() {
		return peso;
	}

	/**
	 * @param float peso
	 */
	public void setPeso(float peso) {
		this.peso = peso;
	}

	/**
	 * @return float precio
	 */
	public Precio getPrecio() {
		return precio;
	}

	/**
	 * @param float precio
	 */
	public void setPrecio(Precio precio) {
		this.precio = precio;
	}

	/**
	 * @return float iva
	 */
	public float getIVA() {
		return iva;
	}

	/**
	 * @param float iva
	 */
	public void setIVA(float iva) {
		this.iva = iva;
	}

	/**
	 * @return String codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param String codigo
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "\n|           Nombre: " + this.nombre + "\n|           Codigo " + this.codigo + "\n|           Peso: "
				+ this.peso + "\n|           Precio bruto: " + this.precio.getPrecioBruto()
				+ "\n|           Precio neto: " + this.precio.getPrecioNeto() + "\n";
	}

	/**
	 * Calcular el precio dependiendo del descuento aplicado y del IVA.
	 */
	abstract void calcularPrecioNeto();

	/**
	 * Controlara que si se da de alta un producto se sume un numero a su contador
	 * de productos y si se borra se reste.
	 * 
	 * @param n
	 */
	abstract void controlContadorProductos(int n);

	/**
	 * Calculara el codigo de producto automaticamente.
	 */
	abstract void calcularCodigo();

	/**
	 * Dara de alta productos en el caso de Baraja y Cristal, y en el caso de Vela,
	 * le asignara un nombre y un color segun el aroma.
	 * 
	 * @param int n
	 * 
	 */
	abstract void altaProducto(int n);

}
