package ProblemaArray;

import java.util.Arrays;
import java.util.List;

public class EjercicioArrays {

	public static void main(String[] args) {
		int numAlumnos = 40;
		// generar notas aleatorias
		int[] notas = generarNotas(numAlumnos);
		// buscar el mayor
		int maxNota = maxNota(notas);
		// buscar el menor
		int minNota = minNota(notas);
		int indMaxNota;
		int indMinNota;
		// Empezamos el uso de listas para facilitar la tarea de indices.
		List<int[]> notasas = Arrays.asList(notas);
		indMinNota = notasas.indexOf(minNota) + 1;
		indMaxNota = notasas.lastIndexOf(maxNota) + 1;

		// Comprobamos el resultado del ejercicio
		System.out.println("Minimo es: " + minNota);
		System.out.println("Maximo es: " + maxNota);
		System.out.println("Indice del minimo es : " + indMinNota);
		System.out.println("Indice del maximo es : " + indMaxNota);
		System.out.print("Lista de clase :");
		for (int i = 0; i < notas.length; i++) {
			System.out.print(i + 1);
		}
		System.out.println("Array de notas :" + notasas);

		// creamos el array de notas "practicas"
		int[] practicas = new int[numAlumnos];
		for (int i = 0; i < practicas.length; i++) {
			practicas[i] = (int) (Math.random() * 11);
		}
		// Creamos el vector calificaciones
		float[] calificaciones = new float[numAlumnos];
		for (int i = 0; i < notas.length; i++) {
			calificaciones[i] = (((float) notas[i] + (float) practicas[i]) / 2);
		}
		System.out.println("Practicas      :" + Arrays.toString(practicas));
		System.out.println("Calificaciones :" + Arrays.toString(calificaciones));

		// Sacamos la estadistica de calificaciones
		// hacemos un array de 10 para la estadistica.
		float[] estadistica = new float[10];

		for (int i = 0; i < 10; i++) {
			float count = 0;
			float sum = 0;
			for (int j = 0; j < notas.length; j++) {
				if ((i < calificaciones[j]) && ((i + 1) >= calificaciones[j])) {
					sum += calificaciones[j];
					count += 1;
				}
			}
			if (count != 0) {
				estadistica[i] = ((float) count / numAlumnos);
			} else {
				estadistica[i] = 0;
			}
			double sol = (Math.round(estadistica[i] * 10000.0)) / 100.0;
			System.out.println("Estadística nota tramo <=" + (i + 1) + " = " + sol + "%");
		}
		// Aprobados y suspensos
		int[] aprobados = new int[numAlumnos];
		int[] suspensos = new int[numAlumnos];
		int countAprobados = 0;
		int countSuspensos = 0;
		for (int i = 0; i < numAlumnos; i++) {
			if (calificaciones[i] < 5) {
				aprobados[i] = i;
				countAprobados += 1;
			} else {
				suspensos[i] = i;
				countSuspensos += 1;
			}
		}
		System.out.println("Relacion de aprobados por num de lista: " + Arrays.toString(aprobados));
		System.out.println("Relacion de suspensos por num de lista: " + Arrays.toString(suspensos));
		// Resumen de aprobados y suspensos
		int i = 0;
		int x = 0;
		int[] a = new int[countAprobados];
		int[] s = new int[countSuspensos];
		while (i < aprobados.length) {
			if (aprobados[i] != 0) {
				a[x] = aprobados[i];
				i++;
				x++;
			} else {
				i++;
			}
		}

		i = x = 0;
		while (i < suspensos.length) {
			if (suspensos[i] != 0) {
				s[x] = suspensos[i];
				i++;
				x++;
			} else {
				i++;
			}
		}
		System.out.println("Resumen  de aprobados por num de lista: " + Arrays.toString(a));
		System.out.println("Resumen  de aprobados por num de lista: " + Arrays.toString(s));

		/*
		 * 6. Suponer un vector de Calificaciones de tamaño 40 (máximo de alumnos por
		 * clase), pero que solo almacena las notas de 31 alumnos. Realizar un programa
		 * que permita insertar en la posición 4 del vector la calificación de un
		 * nuevo alumno en clase al que supuestamente le corresponde como nota un 6.
		 */
		double[] calif = new double[40];
		for (int j = 0; j < 31; j++) {
			calif[j] = (int) (Math.random() * 11);
		}
		System.out.println("Nota antigua alumno num4: " + calif[3]);
		calif[3] = 6;
		System.out.println("Nota nueva   alumno num4: " + calif[3]);
	}

	static int[] generarNotas(int numAlumnos) {
		// vector con las notas generadas
		int[] notasAleatorias = new int[numAlumnos];
		// Genera notas random entre 1 y 10
		for (int i = 0; i < notasAleatorias.length; i++) {
			notasAleatorias[i] = (int) (Math.random() * 9) + 1;
		}
		return notasAleatorias;
	}

	static int maxNota(int[] notas) {
		int max = 0;
		for (int i = 0; i < notas.length; i++) {
			if (notas[i] > max) {
				max = notas[i];
			}
		}
		return max;
	}

	static int minNota(int[] notas) {
		int min = 11;
		for (int i = 0; i < notas.length; i++) {
			if (notas[i] < min) {
				min = notas[i];
			}
		}
		return min;
	}
}