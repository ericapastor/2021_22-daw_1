package PuesSiJaja;

import java.util.Scanner;

public class EjercicioDivisas {

	/*
	 * ERRORES DE REFACTORIZACION
	 */
	// 1. Los static final estan declarados al final del codigo
	// 2. El array de moendas calculadas nunca se utiliza para nada
	// 3. La variable local String que sirve como opcion1 esta en mayusculas
	// 4. La variable local String que sirve como opcion2 tiene un nombre
	// no auto-aclarativo
	// 5. La variable float MONEDA esta en mayusculas y tiene un guion
	// 6. El nombre del package es poco aclarativo y esta en mayusculas
	// 7. 84.13F es un magic number que deberia ser un FINAL correspondiente
	// al valor de las rupias en esta App
	// 8. La variable valorConversion hace referencia a el valor de la moneda
	// ya convertida segun las opciones elegidas pero nunca es usado,
	// por lo que es codigo muerto
	// 9. Las variables locales String OPCION_1 y rugidoEnsordecedor
	// estan declaradas en una sola linea
	// 10. Tabulaciones incorrectas en todo el codigo

public static void main(String[] args) {

Scanner input = new Scanner(System.in);
String OPCION_1 = "", rugidoEnsordecedor = "";
float MONEDA = 0.0F;

System.out.println("Desde que divisa quieres transformar:");
System.out.println("1 - Euro");
System.out.println("2 - Dolar");
System.out.println("3 - Libra");
System.out.println("4 - Yen");
System.out.println("5 - Rupias");
System.out.println("6 - Francos suizos");
while (true) {
OPCION_1 = input.nextLine();
if (!OPCION_1.equals("")) {
switch (OPCION_1) {
case "1":
System.out.println("A que divisa quieres transformar:");
System.out.println("1 - Dolares");
System.out.println("2 - Libras");
System.out.println("3 - Yenes");
System.out.println("4 - Rupias");
System.out.println("5 - Francos suizos");
while (true) {
rugidoEnsordecedor = input.nextLine();
if (!rugidoEnsordecedor.equals("")) {
switch (rugidoEnsordecedor) {
case "1":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * dolar;
}
System.out.println(MONEDA + " euros son " + (MONEDA * dolar)
+ " dolares.");
break;
case "2":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * LIBRA;
}
System.out.println(MONEDA + " euros son " + (MONEDA * LIBRA)
+ " libras.");
break;
case "3":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * YEN;
}
System.out.println(MONEDA + " euros son "
+ (MONEDA * YEN) + " yenes.");
break;
case "4":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * 84.13F;
}
System.out.println(
MONEDA + " euros son " + (MONEDA * 84.13F) + " rupias.");
break;
case "5":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * FRANCO;
}
System.out.println(MONEDA + " euros son " + (MONEDA * FRANCO)
+ " francos suizos.");
break;
default:
System.out.println("La opcion elegida no es valida.");
break;
}
} else {
System.out.println("No has escrito nada, vuelve a intentarlo.");
}
if (rugidoEnsordecedor.equals("1") || rugidoEnsordecedor.equals("2")
|| rugidoEnsordecedor.equals("3") || rugidoEnsordecedor.equals("4")
|| rugidoEnsordecedor.equals("5")) {
break;
}
}
break;
case "2":
System.out.println("A que divisa quieres transformar:");
System.out.println("1 - Euros");
System.out.println("2 - Libras");
System.out.println("3 - Yenes");
System.out.println("4 - Rupias");
System.out.println("5 - Francos suizos");
while (true) {
rugidoEnsordecedor = input.nextLine();
if (!rugidoEnsordecedor.equals("")) {
switch (rugidoEnsordecedor) {
case "1":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA / dolar;
}
System.out.println(MONEDA + " dolares son " + (MONEDA / dolar)
+ " euros.");
break;
case "2":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * LIBRA / dolar;
}
System.out.println(MONEDA + " dolares son "
+ (MONEDA * LIBRA / dolar) + " libras.");
break;
case "3":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * YEN / dolar;
}
System.out.println(MONEDA + " dolares son "
+ (MONEDA * YEN / dolar) + " yenes.");
break;
case "4":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * 84.13F / dolar;
}
System.out.println(MONEDA + " dolares son "
+ (MONEDA * 84.13F / dolar) + " rupias.");
break;
case "5":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * FRANCO / dolar;
}
System.out.println(MONEDA + " dolares son "
+ (MONEDA * FRANCO / dolar) + " francos suizos.");
break;
default:
System.out.println("La opcion elegida no es valida.");
break;
}
} else {
System.out.println("No has escrito nada, vuelve a intentarlo.");
}
if (rugidoEnsordecedor.equals("1") || rugidoEnsordecedor.equals("2")
|| rugidoEnsordecedor.equals("3") || rugidoEnsordecedor.equals("4")
|| rugidoEnsordecedor.equals("5")) {
break;
}
}
break;
case "3":
System.out.println("A que divisa quieres transformar:");
System.out.println("1 - Euros");
System.out.println("2 - Dolares");
System.out.println("3 - Yenes");
System.out.println("4 - Rupias");
System.out.println("5 - Francos suizos");
while (true) {
rugidoEnsordecedor = input.nextLine();
if (!rugidoEnsordecedor.equals("")) {
switch (rugidoEnsordecedor) {
case "1":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA / LIBRA;
}
System.out.println(MONEDA + " libras son " + (MONEDA / LIBRA)
+ " euros.");
break;
case "2":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * dolar / LIBRA;
}
System.out.println(MONEDA + " libras son "
+ (MONEDA * dolar / LIBRA) + " dolares.");
break;
case "3":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * YEN / LIBRA;
}
System.out.println(MONEDA + " libras son "
+ (MONEDA * YEN / LIBRA) + " yenes.");
break;
case "4":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * 84.13F / LIBRA;
}
System.out.println(MONEDA + " libras son "
+ (MONEDA * 84.13F / LIBRA) + " rupias.");
break;
case "5":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * FRANCO / LIBRA;
}
System.out.println(MONEDA + " libras son "
+ (MONEDA * FRANCO / LIBRA) + " francos suizos.");
break;
default:
System.out.println("La opcion elegida no es valida.");
break;
}
} else {
System.out.println("No has escrito nada, vuelve a intentarlo.");
}
if (rugidoEnsordecedor.equals("1") || rugidoEnsordecedor.equals("2")
|| rugidoEnsordecedor.equals("3") || rugidoEnsordecedor.equals("4")
|| rugidoEnsordecedor.equals("5")) {
break;
}
}
break;
case "4":
System.out.println("A que divisa quieres transformar:");
System.out.println("1 - Euros");
System.out.println("2 - Dolares");
System.out.println("3 - Libras");
System.out.println("4 - Rupias");
System.out.println("5 - Francos suizos");
while (true) {
rugidoEnsordecedor = input.nextLine();
if (!rugidoEnsordecedor.equals("")) {
switch (rugidoEnsordecedor) {
case "1":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA / YEN;
}
System.out.println(MONEDA + " yenes son "
+ (MONEDA / YEN) + " euros.");
break;
case "2":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * dolar / YEN;
}
System.out.println(MONEDA + " yenes son "
+ (MONEDA * dolar / YEN) + " dolares.");
break;
case "3":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * LIBRA / YEN;
}
System.out.println(MONEDA + " yenes son "
+ (MONEDA * LIBRA / YEN) + " libras.");
break;
case "4":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * 84.13F / YEN;
}
System.out.println(MONEDA + " yenes son "
+ (MONEDA * 84.13F / YEN) + " rupias.");
break;
case "5":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * FRANCO / YEN;
}
System.out.println(MONEDA + " yenes son "
+ (MONEDA * FRANCO / YEN)
+ " francos suizos.");
break;
default:
System.out.println("La opcion elegida no es valida.");
break;
}
} else {
System.out.println("No has escrito nada, vuelve a intentarlo.");
}
if (rugidoEnsordecedor.equals("1") || rugidoEnsordecedor.equals("2")
|| rugidoEnsordecedor.equals("3") || rugidoEnsordecedor.equals("4")
|| rugidoEnsordecedor.equals("5")) {
break;
}
}
break;
case "5":
System.out.println("A que divisa quieres transformar:");
System.out.println("1 - Euros");
System.out.println("2 - Dolares");
System.out.println("3 - Libras");
System.out.println("4 - Yenes");
System.out.println("5 - Francos suizos");
while (true) {
rugidoEnsordecedor = input.nextLine();
if (!rugidoEnsordecedor.equals("")) {
switch (rugidoEnsordecedor) {
case "1":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA / 84.13F;
}
System.out.println(
MONEDA + " rupias son " + (MONEDA / 84.13F) + " euros.");
break;
case "2":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * dolar / 84.13F;
}
System.out.println(MONEDA + " rupias son "
+ (MONEDA * dolar / 84.13F) + " dolares.");
break;
case "3":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * LIBRA / 84.13F;
}
System.out.println(MONEDA + " rupias son "
+ (MONEDA * LIBRA / 84.13F) + " libras.");
break;
case "4":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * YEN / 84.13F;
}
System.out.println(MONEDA + " rupias son "
+ (MONEDA * YEN / 84.13F) + " yenes.");
break;
case "5":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * FRANCO / 84.13F;
}
System.out.println(MONEDA + " rupias son "
+ (MONEDA * FRANCO / 84.13F) + " francos suizos.");
break;
default:
System.out.println("La opcion elegida no es valida.");
break;
}
} else {
System.out.println("No has escrito nada, vuelve a intentarlo.");
}
if (rugidoEnsordecedor.equals("1") || rugidoEnsordecedor.equals("2")
|| rugidoEnsordecedor.equals("3") || rugidoEnsordecedor.equals("4")
|| rugidoEnsordecedor.equals("5")) {
break;
}
}
break;
case "6":
System.out.println("A que divisa quieres transformar:");
System.out.println("1 - Euros");
System.out.println("2 - Dolares");
System.out.println("3 - Libras");
System.out.println("4 - Yenes");
System.out.println("5 - Rupias");
while (true) {
rugidoEnsordecedor = input.nextLine();
if (!rugidoEnsordecedor.equals("")) {
switch (rugidoEnsordecedor) {
case "1":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA / FRANCO;
}
System.out.println(MONEDA + " francos suizos son "
+ (MONEDA / FRANCO) + " euros.");
break;
case "2":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * dolar / FRANCO;
}
System.out.println(MONEDA + " francos suizos son "
+ (MONEDA * dolar / FRANCO) + " dolares.");
break;
case "3":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * LIBRA / FRANCO;
}
System.out.println(MONEDA + " francos suizos son "
+ (MONEDA * LIBRA / FRANCO) + " libras.");
break;
case "4":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * YEN / FRANCO;
}
System.out.println(MONEDA + " francos suizos son "
+ (MONEDA * YEN / FRANCO) + " yenes.");
break;
case "5":
System.out.println("Introduce la cantidad que quieres transformar");
while (true) {
MONEDA = input.nextFloat();
input.nextLine();
if (MONEDA > 0) {
break;
} else {
System.out.println("La cantidad no puede ser negativa.");
}
float valorConversion = MONEDA * 84.13F / FRANCO;
}
System.out.println(MONEDA + " francos suizos son "
+ (MONEDA * 84.13F / FRANCO) + " rupias.");
break;
default:
System.out.println("La opcion elegida no es valida.");
break;
}
} else {
System.out.println("No has escrito nada, vuelve a intentarlo.");
}
if (rugidoEnsordecedor.equals("1") || rugidoEnsordecedor.equals("2")
|| rugidoEnsordecedor.equals("3") || rugidoEnsordecedor.equals("4")
|| rugidoEnsordecedor.equals("5")) {
break;
}
}
break;
default:
System.out.println("La opcion elegida no es valida.");
break;
}
} else {
System.out.println("No has escrito nada, vuelve a intentarlo.");
}
if (OPCION_1.equals("1") || OPCION_1.equals("2") || OPCION_1.equals("3")
|| OPCION_1.equals("4") || OPCION_1.equals("5")
|| OPCION_1.equals("6")) {
break;
}
}
input.close();
}

	static final float dolar = 1.1F;
	static final float LIBRA = 0.83F;
	static final float YEN = 131.72F;
	static final float FRANCO = 1.04F;
}
