package suma;

import java.util.Scanner;

public class ClaseDepuracion2 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int num1;
		int num2;
		System.out.println("Dame un numero");
		num1 = input.nextInt();
		System.out.println("Dame otro numero");
		num2 = input.nextInt();
		suma(num1, num2);
		input.close();
	}

	static void suma(int num1, int num2) {
		System.out.println("La suma es: " + (num1 + num2));
	}

}