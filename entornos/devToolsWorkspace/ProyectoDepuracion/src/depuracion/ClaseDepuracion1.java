package depuracion;

import java.util.Scanner;

public class ClaseDepuracion1 {
	
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int num;
		for (int i = 0; i < 5; i++) {
			System.out.println("Dame el numero");
			num = input.nextInt();
			System.out.println("El numero leido es: " + num + ".");
		}
		System.out.println("El programa ha finalizado.");
		input.close();
	}

}
