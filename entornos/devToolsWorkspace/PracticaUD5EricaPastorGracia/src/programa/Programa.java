package programa;

import clases.Starset;

public class Programa {

	public static void main(String[] args) {
		// creo instancia de tienda
		Starset starset = new Starset();
		// alta clientes
		System.out.println("Alta cliente 1");
		starset.altaCliente("CLI001", "Kim Possible");
		System.out.println("Alta cliente 2");
		starset.altaCliente("CLI002", "Qweryt Gonzales");
		System.out.println("Alta cliente 3");
		starset.altaCliente("CLI001", "Samba");
		// listar clientes
		System.out.println("\nListar clientes:");
		starset.listarClientes();
		// buscar clientes
		System.out.println("Buscar cliente CLI001");
		if (starset.buscarCliente("CLI001") != null) {
			System.out.println(starset.buscarCliente("CLI001"));
		} else {
			System.out.println("El cliente no existe");
		}
		System.out.println("Buscar cliente CLI003");
		if (starset.buscarCliente("CLI003") != null) {
			System.out.println(starset.buscarCliente("CLI003"));
		} else {
			System.out.println("El cliente no existe");
		}
		// alta carta astral
		System.out.println("\nAlta carta astral 1");
		starset.altaCartaAstral("CAR001", "1995-02-13T17:00");
		System.out.println("Alta carta astral 2");
		starset.altaCartaAstral("CAR002", "1998-12-18T01:00");
		System.out.println("Alta carta astral 3 (con el codigo de la primera)");
		starset.altaCartaAstral("CAR001", "1994-05-31T07:00");
		// asignar cartas a clientes
		System.out.println("\nAsignar carta 001 a cliente 001");
		starset.asignarCartaAstralCliente("CLI001", "CAR001");
		System.out.println("Asignar carta 002 a cliente 002");
		starset.asignarCartaAstralCliente("CLI002", "CAR002");
		System.out.println("Asignar cliente 003 a carta 001 (el cliente no existe)");
		starset.asignarCartaAstralCliente("CLI003", "CAR001");
		System.out.println("Asignar cliente 001 a carta 003 (la carta no existe)");
		starset.asignarCartaAstralCliente("CLI001", "CAR003");
		System.out.println("\nListar cartas astrales");
		starset.listarCartas();
		System.out.println("\nListar por signo solar \"Acuario\"");
		starset.listarCartaPorSolar("Acuario");
	}

}
