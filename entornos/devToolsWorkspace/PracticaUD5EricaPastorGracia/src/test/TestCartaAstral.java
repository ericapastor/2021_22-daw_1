package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDateTime;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.CartaAstral;
import clases.Starset;

class TestCartaAstral {

	static Starset starset = new Starset();

	@BeforeAll
	public static void altaCartasAstrales() {
		System.out.print("**** Before all, doy de alta 4 cartas astrales ****");
		starset.altaCartaAstral("CAR000", "1996-05-01T16:43");
		starset.altaCartaAstral("CAR001", "1987-07-30T10:17");
		starset.altaCartaAstral("CAR002", "2001-02-15T21:01");
		starset.altaCartaAstral("CAR003", "1993-10-07T01:57");
		System.out.println("**** Listo cartas ****");
		starset.listarCartas();
		System.out.println();
	}

	@Test
	public void calcularCartaAstralTest1() {
		// sabiendo que la carta astral CAR000 es solar tauro, lunar libra, ascendente
		// libra con fecha y hora de nacimiento 1996-05-01T16:43
		System.out.println("**** Test 1 calcular carta astral ****");
		System.out.println(
				"**** Guardo el solar y la fecha y hora de nacimeinto de la CAR000 en una variable local ****");
		System.out.println("**** Cambio fecha y hora de nacimiento a CAR000 ****");
		LocalDateTime nacimientoTemporal = starset.buscarCartaAstral("CAR000").getNacimiento();
		String solarTemporal = starset.buscarCartaAstral("CAR000").getSolar();
		// la carta astral se calcula en el setter de la fecha y hora de nacimiento,
		// por tanto deberia cambiar tras cambiar este dato
		starset.buscarCartaAstral("CAR000").setNacimiento("1995-02-13T17:01");
		System.out.println("**** Comparo el signo solar antiguo con el nuevo con assert not equals ****");
		assertNotEquals(solarTemporal, starset.buscarCartaAstral("CAR000").getSolar());
		System.out.println("**** Compruebo por pantalla ****");
		System.out.println("Fecha y hora antiguos: " + nacimientoTemporal);
		System.out.println("Fecha y hora nuevos: " + starset.buscarCartaAstral("CAR000").getNacimiento());
		System.out.println("Signo solar antiguo: " + solarTemporal);
		System.out.println("Signo solar nuevo: " + starset.buscarCartaAstral("CAR000").getSolar());
	}

	@Test
	public void buscarCartasAstralesTest1() {
		System.out.println("**** Test 2 buscar carta astral ****");
		System.out.println("**** Creo una carta nueva CAR004 ****");
		CartaAstral carta = new CartaAstral("CAR004", "2000-06-15T14:31");
		starset.getCartasAstrales().add(carta);
		System.out.println("**** Comparo con assert equals: ****");
		assertEquals(carta, starset.buscarCartaAstral("CAR004"));
		System.out.println("**** Compruebo por pantalla: ****");
		System.out.println(carta);
		System.out.println(starset.buscarCartaAstral("CAR004"));
		System.out.println();
	}

	@Test
	public void buscarCartasAstralesTest2() {
		System.out.println("**** Test 3 buscar carta astral ****");
		System.out.println("**** Creo una carta que ya exista: ****");
		starset.altaCartaAstral("CAR001", "1999-09-27T12:05");
		System.out.println("**** Creo una carta astral que no exista ****");
		CartaAstral carta = new CartaAstral("CAR005", "1999-09-27T12:05");
		starset.getCartasAstrales().add(carta);
		System.out.println("**** Comparo con assert not equals ****");
		assertNotEquals(carta, starset.buscarCartaAstral("CAR001"));
		System.out.println("**** Compruebo por pantalla: ****");
		System.out.println(carta);
		System.out.println(starset.buscarCartaAstral("CAR001"));
		System.out.println();
	}

	@AfterAll
	public static void mostrarCartas() {
		System.out.println("**** After all, muestro todas las cartas astrales de nuevo ****");
		starset.listarCartas();
		System.out.println("**** Fin de pruebas de cartas astrales ****");
	}

}
