package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Starset;

class TestStarset {

	static Starset starset = new Starset();

	@BeforeAll
	public static void altaClientesCartas() {
		System.out.println("**** Dar de alta clientes y cartas y mostrarlos (before all) ****");
		starset.altaCliente("CLI000", "Paco");
		starset.altaCliente("CLI001", "Ortiga");
		starset.altaCliente("CLI002", "Ivan");
		System.out.println("**** los clientes ****");
		starset.listarClientes();
		starset.altaCartaAstral("CAR000", "1996-05-01T16:43");
		starset.altaCartaAstral("CAR001", "1987-07-30T10:17");
		starset.altaCartaAstral("CAR002", "2001-02-15T21:01");
		System.out.println("**** Muestro las cartas astrales ****");
		starset.listarCartas();
		System.out.println();
	}

	@Test
	public void asignarClienteCartaTest1() {
		System.out.println("**** Test 1 asignar cliente a una carta astral ****");
		System.out.println("Asigno el cliente CLI000 a la carta CAR000");
		starset.asignarCartaAstralCliente("CLI000", "CAR000");
		System.out.println("Compruebo con assert equals");
		assertEquals(starset.buscarCliente("CLI000"), starset.buscarCartaAstral("CAR000").getCliente());
		System.out.println("**** Compruebo mostrando por pantalla ****");
		System.out.println("Cliente desde \"buscarCliente\"" + starset.buscarCliente("CLI000"));
		System.out.println(
				"Cliente desde \"buscarCartaAstral.getCliente\"" + starset.buscarCartaAstral("CAR000").getCliente());
	}

	@Test
	public void asignarClienteCartaTest2() {
		System.out.println("**** Test 2 asignar cliente a una carta astral ****");
		System.out.println("Asigno el cliente CLI001 a la carta CAR001");
		starset.asignarCartaAstralCliente("CLI001", "CAR001");
		System.out.println("Compruebo con assert not equals");
		assertNotEquals(starset.buscarCliente("CLI000"), starset.buscarCartaAstral("CAR001").getCliente());
		System.out.println("Cliente desde \"buscarCliente\"" + starset.buscarCliente("CLI000"));
		System.out.println(
				"Cliente desde \"buscarCartaAstral.getCliente\"" + starset.buscarCartaAstral("CAR001").getCliente());
	}

	@Test
	public void desasignarClienteCartaTest() {
		System.out.println("**** Test 3 desasignar cliente de carta astral ****");
		System.out.println(
				"Comprobar con assertNull si se ha desasignado correctamente el cliente CLI001 de la carta astral CAR001");
		starset.desasignarClienteCarta("CLI001");
		assertNull(starset.buscarCartaAstral("CAR001").getCliente());
		System.out.println("Compruebo por pantalla:");
		System.out.println("Cliente de carta CAR001: " + starset.buscarCartaAstral("CAR001").getCliente());
	}

	@Test
	public void borrarClienteTest() {
		System.out.println("**** Test 4 borrar cliente ****");
		System.out.println(
				"Comprobar con assertTrue si se ha desasignado el cliente CLI000 de la carta astral CAR000 al borrarlo");
		starset.borrarCliente("CLI000");
		assertNull(starset.buscarCartaAstral("CAR000").getCliente());
		System.out.println("**** Compruebo por pantalla: ****");
		System.out.println("Cliente CLI000: " + starset.buscarCliente("CLI000"));
		System.out.println(
				"Cliente buscado desde la carta astral CAR000: " + starset.buscarCartaAstral("CAR001").getCliente());
	}

	@AfterAll
	public static void listarClientes() {
		System.out.println("**** Listo de nuevo todos los clientes: ****");
		starset.listarClientes();
		System.out.println("**** Listo de nuevo todas las cartas astrales: ****");
		starset.listarCartas();
	}

}