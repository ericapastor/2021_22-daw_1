package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Starset;

class TestCliente {

	static Starset starset = new Starset();

	@BeforeAll
	public static void altaClientes() {
		System.out.println("**** Dar de alta clientes y mostrarlos (before all) ****");
		starset.altaCliente("CLI000", "Paco");
		starset.altaCliente("CLI001", "Ortiga");
		starset.altaCliente("CLI002", "Ivan");
		System.out.println();
		starset.listarClientes();
		System.out.println();
	}

	@Test
	public void buscarClienteTest1() {
		System.out.println("**** Test 1 buscar clientes ****");
		System.out.println("**** Creo un cliente CLI003 y compruebo con assertEquals ****");
		Cliente cliente = new Cliente("CLI003", "Andres");
		starset.getClientes().add(cliente);
		assertEquals(cliente, starset.buscarCliente("CLI003"));
		System.out.println("**** Compruebo mostrando por pantalla: ****");
		System.out.println("Cliente variable local: " + cliente);
		System.out.println("Cliente del array CLI003: " + starset.buscarCliente("CLI003"));
		System.out.println();
	}

	@Test
	public void buscarClienteTest2() {
		System.out.println("**** Test 2 buscar clientes ****");
		System.out.println("**** Creo un cliente CLI004 y compruebo con assertNotEquals cliente CLI001 ****");
		Cliente cliente = new Cliente("CLI004", "Manuela");
		starset.getClientes().add(cliente);
		assertNotEquals(cliente, starset.buscarCliente("CLI001"));
		System.out.println("**** Compruebo mostrando por pantalla: ****");
		System.out.println("Cliente variable local: " + cliente);
		System.out.println("Cliente del array CLI001: " + starset.buscarCliente("CLI001"));

	}

	@Test
	public void clienteExisteTest1() {
		System.out.println("**** Test 3 existe cliente, assert true ****");
		boolean cExiste = starset.buscarCliente("CLI000") != null;
		assertTrue(cExiste);
		System.out.println("**** Compruebo: ****");
		System.out.println("Existe el cliente CLI000: " + (cExiste ? "si" : "no"));
		System.out.println();
	}

	@Test
	public void clienteExisteTest2() {
		System.out.println("**** Test 4 existe cliente, assert false ****");
		boolean cExiste = starset.buscarCliente("CLI009") != null;
		assertFalse(cExiste);
		System.out.println("**** Compruebo *****");
		System.out.println("Existe el cliente CLI009: " + (cExiste ? "si" : "no"));
		System.out.println();
	}
	
	@Test
	public void eliminarClienteTest() {
		System.out.println("**** Test 5 eliminar cliente, assert null ****");
		System.out.println("Comprobar la eliminacion correcta de un cliente");
		starset.borrarCliente("CLI000");
		assertNull(starset.buscarCliente("CLI000"));
		System.out.println("Compruebo por pantalla:");
		System.out.println(starset.buscarCliente("CLI000"));
	}

	@AfterAll
	public static void listarClientes() {
		System.out.println("**** Listo de nuevo todos los clientes: ****");
		starset.listarClientes();
	}

}
