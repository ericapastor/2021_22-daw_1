package clases;

import java.time.LocalDate;

public class Cliente {
	private String id;
	private String nombre;
	private LocalDate fechaAlta;

	public Cliente(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
		this.fechaAlta = LocalDate.now();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Override
	public String toString() {
		return "Cliente: nombre: " + nombre + ", ID de cliente: " + id + ", fecha de alta: " + fechaAlta;
	}

}
