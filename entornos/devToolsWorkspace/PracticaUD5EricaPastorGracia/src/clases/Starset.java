package clases;

import java.util.ArrayList;
import java.util.Iterator;

public class Starset {

	private ArrayList<Cliente> clientes;
	private ArrayList<CartaAstral> cartasAstrales;

	public Starset() {
		clientes = new ArrayList<Cliente>();
		cartasAstrales = new ArrayList<CartaAstral>();
	}

	public ArrayList<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(ArrayList<Cliente> clientes) {
		this.clientes = clientes;
	}

	public ArrayList<CartaAstral> getCartasAstrales() {
		return cartasAstrales;
	}

	public void setCartasAstrales(ArrayList<CartaAstral> cartasAstrales) {
		this.cartasAstrales = cartasAstrales;
	}

	public void altaCliente(String id, String nombre) {
		if (buscarCliente(id) == null) {
			Cliente c = new Cliente(id, nombre);
			clientes.add(c);
		} else {
			System.out.println("El cliente ya existe.");
		}
	}

	public void listarClientes() {
		for (Cliente c : clientes) {
			if (c != null) {
				System.out.println(c);
			}
		}
	}

	public Cliente buscarCliente(String id) {
		for (Cliente c : clientes) {
			if (c != null && c.getId().equals(id)) {
				return c;
			}
		}
		return null;
	}

	public void altaCartaAstral(String id, String nacimiento) {
		if (buscarCartaAstral(id) == null) {
			CartaAstral c = new CartaAstral(id, nacimiento);
			cartasAstrales.add(c);
		} else {
			System.out.println("Ya existe el ID para esa carta astral.");
		}
	}

	public void asignarCartaAstralCliente(String idCliente, String idCartaAstral) {
		if (buscarCliente(idCliente) != null) {
			if (buscarCartaAstral(idCartaAstral) != null) {
				Cliente cliente = buscarCliente(idCliente);
				CartaAstral carta = buscarCartaAstral(idCartaAstral);
				carta.setCliente(cliente);
			} else {
				System.out.println("La carta astral no existe.");
			}
		} else {
			System.out.println("El cliente no existe.");
		}
	}

	public void desasignarClienteCarta(String id) {
		for (CartaAstral carta : cartasAstrales) {
			if (carta.getCliente().equals(buscarCliente(id))) {
				carta.setCliente(null);
				break;
			}
		}
	}

	public CartaAstral buscarCartaAstral(String id) {
		for (CartaAstral c : cartasAstrales) {
			if (c != null && c.getId().equals(id)) {
				return c;
			}
		}
		return null;
	}

	public void listarCartaPorSolar(String signo) {
		for (CartaAstral c : cartasAstrales) {
			if (c.getSolar().equalsIgnoreCase(signo)) {
				System.out.println(c);
			}
		}
	}

	public void listarCartas() {
		for (CartaAstral c : cartasAstrales) {
			System.out.println(c);
		}
	}

	public void borrarCliente(String id) {
		if (buscarCliente(id) != null) {
			Iterator<Cliente> it = clientes.iterator();
			boolean cExiste = false;
			while (it.hasNext()) {
				Cliente cliente = it.next();
				if (cliente.getId().equalsIgnoreCase(id)) {
					cExiste = true;
					desasignarClienteCarta(id);
					it.remove();
					System.out.println("Cliente borrado correctamente.");
					break;
				}
			}
			if (!cExiste) {
				System.out.println("No habia ningun cliente con ese codigo.");
			}
		}
	}
}
