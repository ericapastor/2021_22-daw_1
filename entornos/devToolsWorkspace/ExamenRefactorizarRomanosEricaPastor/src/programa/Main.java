package programa;

import java.util.InputMismatchException;
import java.util.Scanner;

import clases.Numero;

public class Main {

	// 1. Se declara una static final String INICIO pero no se usa en el codigo
	// 2. El objeto Scanner se abre pero no se cierra
	// 3. Las variables num unidades y decenas estan declaradas en linea
	// 4. Las variables num unidades, decenas, centenas y millares deberian
	// declararse donde estan inicializadas
	// 5. La variable Centenas debe escribirse en lowerCamelCase, pero esta en
	// CamelCase
	// 6. El import java.util.* es erroneo, hay que importar cada cosa por separado
	// 7. El "if (unidades >=0 && unidades < 10)" no sigue las convenciones de
	// estilo, el corchete que abre la declaracion esta en la siguiente linea
	// 8. El nombre del Scanner esta declarado en minusculas, y las variables deben
	// escribirse en lowerCamelCase. Asi, en lugar de vamosaleer, seria vamosALeer

	public static final String INICIO = "Inicio del programa";
	static Scanner input = new Scanner(System.in);

	public static void main(String args[]) {
		System.out.println(INICIO);
		System.out.println("Programa que convierte un n�mero ar�bigo a romano (1 - 3999)");
		System.out.println("Dame un numero");
		int numArabico = pedirNumero();
		Numero conversion = new Numero(numArabico);
		conversion.mostrarNumConvertido();
		input.close();
	}

	static int pedirNumero() {
		while (true) {
			try {
				int num = input.nextInt();
				input.nextLine();
				return num;
			} catch (InputMismatchException e) {
				System.out.println("Escribe solamente numeros");
				input.nextLine();
			}
		}
	}
}