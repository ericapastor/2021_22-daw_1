package clases;

public class Numero {
	private int numArabico;
	private String numRomano;

	public Numero(int numArabico) {
		this.numArabico = numArabico;
	}

	public void conversionNumRomano(int numArabico) {
		String numRomano = "";
		for (int i = 1; i < 1001; i *= 10) {
			int numActual = (numArabico / i) % 10;
			numRomano = numRomano + devolverNumRomano(numActual);
		}
		this.numRomano = numRomano;
	}

	private String devolverNumRomano(int numero) {
		if (numero >= 0 && numero < 10) {
			switch (numero) {
			case 1:
				return "I";

			case 2:
				return "II";

			case 3:
				return "III";

			case 4:
				return "IV";

			case 5:
				return "V";

			case 6:
				return "VI";

			case 7:
				return "VII";

			case 8:
				return "VIII";
			case 9:
				return "IX";
			}
		}
		return "";
	}

	public void mostrarNumConvertido() {
		if (numArabico > 0 && numArabico < 4000) {
			conversionNumRomano(numArabico);
			System.out.println("El n�mero romano es: " + numRomano);
		} else {
			System.out.println("N�mero inv�lido");
		}
	}

}
