package clases;

import programa.Programa;

public class MostrarMenu {

	public static int mostrarMenu() {
		System.out.println("Programa que genera passwords de la longitud\nindicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		System.out.println("Elige tipo de password: ");
		return Programa.leer.nextInt();
	}

	public static int longitudCadena() {
		System.out.println("Introduce la longitud de la cadena: ");
		return Programa.leer.nextInt();
	}

}
