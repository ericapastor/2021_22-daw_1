package clases;

import programa.Programa;

public class Password {
	
	private int longitud;
	private String passwd;
	
	/**
	 * constructor
	 * @param longitud
	 * @param passwd
	 */
	
	public Password() {
	}
	
	/**
	 * getters and setters
	 * @return int
	 * @return String
	 */

	public int getLongitud() {
		return longitud;
	}



	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}



	public String getPasswd() {
		return passwd;
	}



	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}



	public void generarCaracteres(int longitud) {
		String passwd="";
		for (int i = 0; i < longitud; i++) {
			char letra1;
			letra1 = (char) ((Math.random() * Programa.ASCII_26) + Programa.ASCII_65);
			passwd += letra1;
		}
		this.setPasswd(passwd);
	}

	public void generarNumeros(int longitud) {
		String passwd="";
		for (int i = 0; i < longitud; i++) {
			int numero2;
			numero2 = (int) (Math.random() * Programa.DIEZ);
			passwd += numero2;
		}
		this.setPasswd(passwd);
	}

	public void generarLetrasCaracteres(int longitud) {
		String passwd="";
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * Programa.DOS);
			if (n == 1) {
				char letra3;
				letra3 = (char) ((Math.random() * Programa.ASCII_26) + Programa.ASCII_65);
				passwd += letra3;
			} else {
				char caracter3;
				caracter3 = (char) ((Math.random() * Programa.ASCII_15) + Programa.ASCII_33);
				passwd += caracter3;
			}
		}
		this.setPasswd(passwd);
	}

	public void generarLetrasNumerosCaracteres(int longitud) {
		String passwd="";
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * Programa.TRES);
			if (n == 1) {
				char letra4;
				letra4 = (char) ((Math.random() * Programa.ASCII_26) + Programa.ASCII_65);
				passwd += letra4;
			} else if (n == 2) {
				char caracter4;
				caracter4 = (char) ((Math.random() * Programa.ASCII_15) + Programa.ASCII_33);
				passwd += caracter4;
			} else {
				int numero4;
				numero4 = (int) (Math.random() * Programa.DIEZ);
				passwd += numero4;
			}
		}
		this.setPasswd(passwd);
	}

}
