package programa;

import java.util.Scanner;

import clases.MostrarMenu;
import clases.Password;

public class Programa {

	public static final int DOS = 2;
	public static final int TRES = 3;
	public static final int DIEZ = 10;
	public static final int ASCII_15 = 15;
	public static final int ASCII_33 = 33;
	public static final int ASCII_65 = 65;
	public static final int ASCII_26 = 26;
	public static Scanner leer = new Scanner(System.in);

	public static void main(String[] args) {
		int opcion = MostrarMenu.mostrarMenu();
		int longitud = MostrarMenu.longitudCadena();
		Password passwd = new Password();
		passwd.setLongitud(longitud);
		switch (opcion) {
		case 1:
			passwd.generarCaracteres(longitud);
			break;
		case 2:
			passwd.generarNumeros(longitud);
			break;
		case 3:
			passwd.generarLetrasCaracteres(longitud);
			break;
		case 4:
			passwd.generarLetrasNumerosCaracteres(longitud);
			break;
		}
		System.out.println(passwd.getPasswd());
		leer.close();
	}

}
