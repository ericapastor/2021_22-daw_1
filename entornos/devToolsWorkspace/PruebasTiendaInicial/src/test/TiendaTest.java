package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Tienda;

public class TiendaTest {
	
	static Tienda unaTienda= new Tienda();
	
	@BeforeAll
	public static void insertarClientesFacturas() {
		System.out.println("Before All");
		System.out.println("Insertamos clientes");
		unaTienda.altaCliente("1111", "Maria");
		unaTienda.altaCliente("2222", "Patricia");
		unaTienda.altaCliente("3333", "Jacinto");
		unaTienda.listarClientes();
		System.out.println();
		unaTienda.altaFactura("Factura1", 10.1, "2021-04-02");
		unaTienda.altaFactura("Factura2", 20.2, "2021-05-02");
		unaTienda.altaFactura("Factura3", 30.3, "2021-06-02");
		unaTienda.listarFacturas();
		System.out.println();
	}
	
	@Test
	public void asignarClienteFacturaTest() {
		System.out.println("Asignar cliente factura");
		unaTienda.asignarClienteFactura("1111", "Factura1");
		assertEquals(unaTienda.buscarCliente("1111"), unaTienda.buscarFactura("Factura1").getCliente());
		System.out.println("comprobar");
		System.out.println(unaTienda.buscarCliente("1111"));
		System.out.println(unaTienda.buscarFactura("Factura1").getCliente());
		System.out.println();
	}
	
	@Test
	public void asignarClienteFacturaTest2() {
		System.out.println("Asignar cliente factura 2");
		unaTienda.asignarClienteFactura("2222", "Factura2");
		assertNotEquals(unaTienda.buscarCliente("2222"), unaTienda.buscarFactura("Factura1").getCliente());
		System.out.println("comprobar");
		System.out.println(unaTienda.buscarCliente("2222"));
		System.out.println(unaTienda.buscarFactura("Factura1").getCliente());
		System.out.println();
	}
	
	@AfterAll
	public static void mostratClientesFactura() {
		System.out.println("afterAll");
		unaTienda.listarClientes();
		unaTienda.listarFacturas();
		System.out.println("mensaje Fin");
	}
}
