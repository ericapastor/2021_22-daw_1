package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Tienda;

public class ClienteTest {

	static Tienda unaTienda = new Tienda();

	@BeforeAll
	public static void insertarClientes() {
		// Se realiza antes de empezar los test
		System.out.println("Before All");
		System.out.println("Insertamos clientes");

		unaTienda.altaCliente("1111", "Maria");
		unaTienda.altaCliente("2222", "Patricia");
		unaTienda.altaCliente("3333", "Jacinto");
		unaTienda.listarClientes();
		System.out.println();

	}

	@Test
	public void existeClientesTest() {
		System.out.println("Test existe clientes");
		boolean resultado = unaTienda.existeCliente("1111");
		assertTrue(resultado);
		// esto ya no es necesario
		System.out.println("Comprueba assertTrue");
		System.out.println(resultado);
		System.out.println();
	}

	@Test
	public void existeClientesTest2() {
		System.out.println("Test existe clientes");
		boolean resultado = unaTienda.existeCliente("4444");
		assertFalse(resultado);
		System.out.println("Comprueba assertFalse");
		System.out.println(resultado);
		System.out.println();
	}

	@Test
	public void buscarClientetest() {
		System.out.println("Test buscar clientes");
		Cliente cli = new Cliente("4444", "Andres");
		unaTienda.getListaClientes().add(cli);
		assertEquals(cli, unaTienda.buscarCliente("5555"));
		System.out.println("Compruebo");
		System.out.println(cli);
		System.out.println(unaTienda.buscarCliente("5555"));
		System.out.println();
	}

	@Test
	public void buscarClientetest2() {
		System.out.println("Test buscar clientes");
		Cliente cli = new Cliente("5555", "Manolo");
		unaTienda.getListaClientes().add(cli);
		assertNotEquals(cli, unaTienda.buscarCliente("55554"));
		System.out.println("Compruebo");
		System.out.println(cli);
		System.out.println(unaTienda.buscarCliente("55554"));
		System.out.println();
	}

	@AfterAll
	public static void mostrarClientes() {
		System.out.println("afeterall");
		unaTienda.listarClientes();
		System.out.println("mensaje Fin");
	}
}
