package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.NotasSwitch;

class NotasSwitchTest {// cuando se repite mucho algo (como la creacion de objetos, apertura de
	// ficheros...) se ponen en un @Before

	static NotasSwitch miNota;

	@BeforeEach
	public void setUpBeforeTest() {
		miNota = new NotasSwitch();
	}

	@Test
	public void testNotas1() {
		System.out.println("Test notas1");
		int nota = 3;
		String esperado = "insuficiente";
		String resultado = miNota.notas(nota);
		assertEquals(esperado, resultado);
	}

	@Test
	public void testNotas2() {
		System.out.println("Test notas2");
		int nota = 15;
		String esperado = "La opcion introducida no es valida";
		String resultado = miNota.notas(nota);
		assertEquals(esperado, resultado);
	}
	
	// si lo que se repite es un cierre (como cerrar ficheros) se pone en un @After

}
