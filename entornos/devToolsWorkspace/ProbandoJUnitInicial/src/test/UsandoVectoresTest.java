package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import clases.UsandoVectores;

class UsandoVectoresTest {
	
	@Test
	public void testSumaVector() {
		System.out.println("Test usando vectores suma 1");
		UsandoVectores miSuma = new UsandoVectores();
		Double miVector[] = { 4.0, 5.0, 6.0, 3.3, 5.2 };
		double esperado = 23.5;
		double resultado = miSuma.sumaVector(miVector);
		assertEquals(esperado, resultado);
	}

}
