package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import clases.ComparaNumeros;

class ComparaNumerosTest {

	@Test
	public void ComparaMayorTest1() {
		System.out.println("Test 1 numeroMayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado = 0;
		int resultado = compara.numeroMayor(0, 0, 0);
		assertEquals(esperado, resultado);
	}

	@Test
	public void ComparaMayorTest2() {
		System.out.println("Test 2 numeroMayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado = 3;
		int resultado = compara.numeroMayor(1, 2, 3);
		assertEquals(esperado, resultado);
	}

	@Test
	public void ComparaMayorTest3() {
		System.out.println("Test 3 numeroMayor");
		ComparaNumeros compara = new ComparaNumeros();
		int esperado = -2;
		int resultado = compara.numeroMayor(-2, -3, -5);
		assertEquals(esperado, resultado);
	}

	@Test
	public void ComparaMayorTest4() {
		System.out.println("Test 4 numeroMayor");
		ComparaNumeros compara = new ComparaNumeros();
		int noEsperado = -2;
		int resultado = compara.numeroMayor(-2, -2, 5);
		assertNotEquals(noEsperado, resultado);
	}

}
