package enums;

public enum TipoDivisa {
	EURO, LIBRA, DOLAR, FRANCO, YEN;

	public String obtenerStringDivisa() {
		switch (this) {
		case EURO:
			return "Euro";
		case LIBRA:
			return "Libra";
		case DOLAR:
			return "Dolar";
		case FRANCO:
			return "Franco";
		case YEN:
			return "Yen";
		default:
			return "";
		}
	}
}
