package programa;

import gestion.Transaccion;

public class Programa {

	// Fallo 1. Import de java.util..function.BinaryOperator que nunca se utiliza

	// Fallo 2. En el menu "Que moneda quieres cambiar?" se guarda el nombre de la
	// divisa que se quiere cambiar en un String, y para ello se utiliza un switch.
	// En el switch se separa por casos del 1 al 5, pero todos hacen la misma
	// operacion. Ademas hay formas mas efectivas de dar esa orden, sin usar switch
	// Es codigo muerto.

	// Fallo 3. Las variables estaticas de valor de la moneda (VALOREURO,
	// VALORLIBRA, VALORDOLAR, VALORFRANCO, VALORYEN) por convencion de nomenclatura
	// deberian estar declaradas en lowerCamelCase.
	// en su lugar deberian ver se asi: valorEuro, valorLibra, valorDolar,
	// valorFranco, valorYen

	// Fallo 4. El objeto Scanner se abre pero nunca se cierra.

	// Fallo 5. El array monedas tiene, igual que el anterior, los corchetes
	// declarados tras el nombre.
	// En lugar de double monedas[] debe ser double[] monedas.

	// Fallo 6. La variable double cantidadPerder esta declarada muy lejos de donde
	// se utiliza. Debe declararse e inicializarse mas cerca.

	// Fallo 8. En lugar de usar un for para ir pidiendo el dinero disponible al
	// principio del codigo se usa un contador posicion++ y un bucle
	// do{}while(posicion < divisas.length)

	// Fallo 9. La variable boolean que decide el fin del bucle por el que se
	// repite el programa es excesivamente larga y no es descriptiva de lo que hace.
	// En lugar de
	// 'noTengoMuyClaroComoLamarAEstaVariableLaVerdadHoyEstoyPocoCreativoYAdemasEstoyJugandoAlWowDeMientrasEtoyHaciendoEstaPracticaEspecialmenteParaTiPersonitaQueNoSeQuienSerasPeroQueSepasQueNoMeImportaQuienSeasPorqueAlFinalNoSomosNadieEnEsteUniversoInfinitoBuenoSiLoPiensasUnPocoMasEnMenorEscalaSomosAmasijosDeCarneYHuesosQueSabenHablarQueCosaMasAsquerosaSiTePonesAImaginarteEstoPeroEnLugarDeConFormHumanaConFormaDeCucharachaPfffffBuenoCreoQueEstoYaEsLoSuficientementeLArgoAsiQueAleSuerteConLosErrores'
	// Yo la llamaria 'fin', por ejemplo, o 'finPrograma'
	// En lugar de eso he optado por eliminarla dado que no es necesaria: con un
	// while(true) que se rompe si se introduce la opcion 6 es suficiente.

	// Fallo 10. Las variables cambioValorMoneda y aumentarBajarValor estan
	// declaradas en linea.
	// * En lugar de:
	// int cambioValorMoneda,aumentarBajarValor;
	// cambioValorMoneda= (int) (Math.random() * 5) + 1;
	// aumentarBajarValor= (int) (Math.random() * 2);
	// * Seria:
	// int cambioValorMoneda = (int) (Math.random() * 5) + 1;
	// int aumentarBajarValor = (int) (Math.random() * 2);

	public static void main(String[] args) {
		Transaccion transaccion = new Transaccion();
		transaccion.altaDivisas();
		transaccion.rellenarBolsillo();
		transaccion.mostrarDineros();
		while (true) {
			// pedir desde que moneda quiero cambiar
			PedirDatos.monedaOrigen();
			int opcionOrigen = PedirDatos.opciones();
			if (opcionOrigen == 6) {
				PedirDatos.teclado.close();
				break;
			} else {
				opcionOrigen -= 1;
				// pedir datos cuanto dinero quiero cambiar
				double cantidadPerder = transaccion.getBolsillo()[opcionOrigen].cantidadCambio();
				// pedir a que moneda quiero cambiar
				PedirDatos.monedaDestino();
				int opcionDestino = PedirDatos.opciones();
				if (opcionDestino == 6) {
					PedirDatos.teclado.close();
					break;
				} else {
					opcionDestino -= 1;
					transaccion.transaccion(opcionOrigen, opcionDestino, cantidadPerder);
				}
			}
			// calcular aleatoriamente el cambio de valor de las monedas
			transaccion.cambiarValorMoneda();
			transaccion.mostrarDineros();
		}
		System.out.println("Adioz");
	}
}