package programa;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PedirDatos {

	static Scanner teclado = new Scanner(System.in);

	public static String texto() {
		String frase = "";
		while (frase.equals("")) {
			frase = teclado.nextLine();
		}
		return frase;
	}

	public static double decimales() {
		double decimal = -1;
		while (decimal < 0) {
			try {
				decimal = teclado.nextDouble();
				teclado.nextLine();
				if (decimal < 0) {
					System.out.println("No escribir un negativo crack");
				}
			} catch (InputMismatchException e) {
				System.out.println("El valor introducido tiene que ser un numero decimal");
				teclado.nextLine();
			} catch (Exception e) {
				System.out.println("Error de acceso a la informacion del teclado");
				teclado.nextLine();
				System.exit(0);
			}
		}
		return decimal;
	}

	public static int enteros() {
		while (true) {
			try {
				int enteros = teclado.nextInt();
				teclado.nextLine();
				if (enteros < 0) {
					System.out.println("No puedes escribir un negativo crack");
				} else {
					return enteros;
				}
			} catch (InputMismatchException e) {
				System.out.println("El valor introducido tiene que ser un numero entero");
				teclado.nextLine();
			} catch (Exception e) {
				System.out.println("Error de acceso a la informacion del teclado");
				teclado.nextLine();
				System.exit(0);
			}
		}
	}

	public static void inicio() {
		System.out.println("Bienvenid@ a nuestro maravitupendo conversor de divisas");
		System.out.println("Nos quedaremos amablemente con un 15% de tus transacciones");
		System.out.println("Introduce la cantidad que tienes de cada divisa\n");
	}

	public static void monedaOrigen() {
		System.out.println("\n�Que Moneda Quieres cambiar?");
		System.out.println("1-Euros");
		System.out.println("2-Libras");
		System.out.println("3-Dolares");
		System.out.println("4-Francos");
		System.out.println("5-Yenes");
		System.out.println("6-Nada, quiero irme de aqui pls");
	}

	public static void monedaDestino() {
		System.out.println("\n�A que quieres convertirlo?");
		System.out.println("1-Euros");
		System.out.println("2-Libra");
		System.out.println("3-Dolar");
		System.out.println("4-Franco");
		System.out.println("5-Yen");
		System.out.println("6-Nada, quiero irme de aqui pls");
	}

	public static int opciones() {
		while (true) {
			int opcion = PedirDatos.enteros();
			if (opcion > 6) {
				System.out.println("Un graciosillo eeee");
			} else {
				return opcion;
			}
		}
	}

}
