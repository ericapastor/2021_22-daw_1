package gestion;

import dinerito.Bolsillo;
import dinerito.Divisa;
import enums.TipoDivisa;
import programa.PedirDatos;

public class Transaccion {

	// Creo un nuevo static final VALOR_CONVERSION, dado que son magic numbers, y
	// para automatizar el proceso lo hago una matriz de 5x5
	// Esta matriz sera utilizada en el metodo transaccion()
	static final double[][] VALOR_CONVERSION = { { 1.0, 0.5, 0.8, 1.3, 3.5 }, { 1.5, 1.0, 1.6, 2.2, 4.2 },
			{ 0.75, 0.24, 1.0, 1.1, 4.3 }, { 0.65, 0.48, 0.9, 1.0, 3.4 }, { 0.15, 0.08, 0.20, 0.28, 1.0 } };
	// El interes bancario es private y solamente sera accesible desde metodos de
	// esta clase. No tiene getter publico.
	private static final double INTERES_BANCARIO = 0.15;
	// Un array de divisas
	private Divisa[] divisas = new Divisa[5];
	// Una lista del dinero que tenemos en bolsillo. Su tama�o correspondera siempre
	// a la largura del array de divisas.
	private Bolsillo[] bolsillo = new Bolsillo[divisas.length];

	public Transaccion() {
	}

	public Divisa[] getDivisas() {
		return divisas;
	}

	public void setDivisas(Divisa[] divisas) {
		this.divisas = divisas;
	}

	public Bolsillo[] getBolsillo() {
		return bolsillo;
	}

	public void setBolsillo(Bolsillo[] bolsillo) {
		this.bolsillo = bolsillo;
	}

	public static double[][] getValorConversion() {
		return VALOR_CONVERSION;
	}

	public void altaDivisas() {
		divisas[0] = new Divisa(TipoDivisa.EURO, 1);
		divisas[1] = new Divisa(TipoDivisa.LIBRA, 1);
		divisas[2] = new Divisa(TipoDivisa.DOLAR, 1);
		divisas[3] = new Divisa(TipoDivisa.FRANCO, 1);
		divisas[4] = new Divisa(TipoDivisa.YEN, 1);
	}

	public void rellenarBolsillo() {
		PedirDatos.inicio();
		for (int i = 0; i < bolsillo.length; i++) {
			System.out.println("�Cuantos \"" + divisas[i].getTipo().obtenerStringDivisa().toLowerCase() + "\" tienes?");
			double dineritos = PedirDatos.decimales();
			Bolsillo bolsillo = new Bolsillo(dineritos);
			this.bolsillo[i] = bolsillo;
		}
	}

	public Bolsillo obtenerBolsillo(int n) {
		Bolsillo bolsillo = new Bolsillo();
		try {
			bolsillo = this.bolsillo[n];
			return bolsillo;
		} catch (NullPointerException e) {
			System.out.println("Vaya, parece que el programador es un poco torpe.\nEl objeto esta vacio...");
			System.exit(0);
		} catch (java.lang.ArrayIndexOutOfBoundsException e) {
			System.out.println("Vaya, parece que el programador es un poco torpe.\nObjeto fuera de limites...");
			System.exit(0);
		}
		return bolsillo;
	}

	public Divisa obtenerDivisa(int n) {
		Divisa divisa = new Divisa();
		try {
			divisa = this.divisas[n];
			return divisa;
		} catch (NullPointerException e) {
			System.out.println("Vaya, parece que el programador es un poco torpe.\nEl objeto esta vacio...");
			System.exit(0);
		} catch (java.lang.ArrayIndexOutOfBoundsException e) {
			System.out.println("Vaya, parece que el programador es un poco torpe.\nObjeto fuera de limites...");
			System.exit(0);
		}
		return divisa;
	}

	public void transaccion(int n1, int n2, double cantidad) {
		TipoDivisa origen = this.obtenerDivisa(n1).getTipo();
		TipoDivisa destino = this.obtenerDivisa(n2).getTipo();
		if (origen == destino) {
			System.out.println("�te das cuenta de que quieres pasar " + origen.obtenerStringDivisa().toLowerCase()
					+ " a " + destino.obtenerStringDivisa().toLowerCase()
					+ " verdad?\no va a pasar nada asi que buena esa");
		} else {
			// cambio de monedas de un bolsillo a otro
			// 1.-Quito las monedas de la divisa 1
			this.obtenerBolsillo(n1).setDineritos(this.obtenerBolsillo(n1).getDineritos() - cantidad);
			// 2.-Se las doy a la divisa 2
			this.obtenerBolsillo(n2).setDineritos(this.obtenerBolsillo(n2).getDineritos()
					// uso la matriz VALOR_CONVERSION para transformar la cantidad de dinero de una
					// divisa a otra
					+ cantidad * VALOR_CONVERSION[n1][n2] * this.obtenerDivisa(n1).getValor() * (1 - INTERES_BANCARIO));
		}
	}

	public void cambiarValorMoneda() {
		int cambioValorMoneda = (int) (Math.round((Math.random() * 4)));
		int aumentarBajarValor = (int) (Math.round((Math.random() * 2)));
		String tipoDivisa = divisas[cambioValorMoneda].getTipo().obtenerStringDivisa();
		if (aumentarBajarValor > 0) {
			this.divisas[cambioValorMoneda].setValor(this.divisas[cambioValorMoneda].getValor() + Math.random() * 0.2);
			System.out.println("El " + tipoDivisa + " ha subido de valor");
		} else {
			this.divisas[cambioValorMoneda].setValor(this.divisas[cambioValorMoneda].getValor() - Math.random() * 0.2);
			System.out.println("El " + tipoDivisa + " ha bajado de valor");
			if (this.divisas[cambioValorMoneda].getValor() <= 0) {
				this.divisas[cambioValorMoneda].setValor(0.01);
			}
		}
	}

	public void mostrarDineros() {
		System.out.println("\nTienes:");
		for (int i = 0; i < bolsillo.length; i++) {
			if (bolsillo[i] != null && divisas[i] != null) {
				System.out.println(divisas[i] + " = " + bolsillo[i]);
			}
		}
	}

}
