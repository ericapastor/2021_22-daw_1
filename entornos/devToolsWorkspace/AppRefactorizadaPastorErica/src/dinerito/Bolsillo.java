package dinerito;

import programa.PedirDatos;

public class Bolsillo {

	private double dineritos;

	public Bolsillo() {
	}

	public Bolsillo(double dineritos) {
		this.dineritos = dineritos;
	}

	public double getDineritos() {
		return dineritos;
	}

	public void setDineritos(double dineritos) {
		this.dineritos = dineritos;
	}

	@Override
	public String toString() {
		return "" + dineritos;
	}

	public double cantidadCambio() {
		while (true) {
			System.out.println("�Cuanto quieres cambiar?");
			double cantidad = PedirDatos.decimales();
			if (cantidad > this.getDineritos()) {
				System.out.println("No puedes cambiar mas de lo que tienes");
			} else {
				return cantidad;
			}
		}
	}

}
