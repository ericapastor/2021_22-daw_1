package dinerito;

import enums.TipoDivisa;

public class Divisa {

	TipoDivisa tipo;
	private double valor;

	public Divisa() {
	}

	public Divisa(TipoDivisa tipo, double valor) {
		this.tipo = tipo;
		this.valor = valor;
	}

	public TipoDivisa getTipo() {
		return tipo;
	}

	public void setTipo(TipoDivisa tipo) {
		this.tipo = tipo;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		String tipo = this.tipo.obtenerStringDivisa();
		return "" + tipo;
	}

}
