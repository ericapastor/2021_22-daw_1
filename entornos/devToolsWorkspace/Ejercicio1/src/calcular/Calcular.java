package calcular;

import java.util.Scanner;

public class Calcular {

	static Scanner program = new Scanner(System.in);

	public static void main(String[] args) {
		int base;
		int exponent;
		System.out.println("Base: ");
		base = program.nextInt();
		System.out.println("Exponent: ");
		exponent = program.nextInt();
		System.out.println("The result is: " + (int) Math.pow(base, exponent));
		program.close();
	}
	
}
